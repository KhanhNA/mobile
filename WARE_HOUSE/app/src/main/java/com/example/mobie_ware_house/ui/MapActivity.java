package com.example.mobie_ware_house.ui;

import android.*;
import android.content.*;
import android.content.pm.*;
import android.location.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.support.v4.content.*;
import android.support.v7.app.*;
import android.widget.*;

import com.example.mobie_ware_house.Adapter.*;
import com.example.mobie_ware_house.R;
import com.example.mobie_ware_house.model.dto.*;
import com.example.mobie_ware_house.util.*;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import java.util.*;


public class MapActivity extends AppCompatActivity implements
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleMap.OnInfoWindowClickListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private GoogleMap mMap;

    Marker mLeVanThiem1;
    Marker mLeVanThiem2;
    Marker mLeVanThiem3;
    Marker mLeVanThiem4;
    Marker mLeVanThiem5;
    Marker mLeVanThiem6;
    Marker mLeVanThiem7;

    private ArrayList<Marker> lstMarker = new ArrayList<>();


    private static final LatLng leVanThiem1 = new LatLng(20.999122, 105.794374);

    private static final LatLng leVanThiem2 = new LatLng(20.992711, 105.796198);

    private static final LatLng leVanThiem3 = new LatLng(21.000391, 105.802096);

    private static final LatLng leVanThiem4 = new LatLng(20.998333, 105.803480);

    private static final LatLng leVanThiem5 = new LatLng(21.010395, 105.805291);

    private static final LatLng leVanThiem6 = new LatLng(21.006243, 105.794768);

    private static final LatLng leVanThiem7 = new LatLng(20.985855, 105.801254);

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page_fragment);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull final Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }



    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        enableMyLocation();
        addMarkerToMap();
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(this));
    }

    private void addMarkerToMap() {
        mLeVanThiem1 = mMap.addMarker(new MarkerOptions()
                .position(leVanThiem1)
                .title("KHO PHẠM BIÊN")
                .snippet("Lê Văn Thiêm, gần quán thịt chó!")
                .flat(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse1)));

        mLeVanThiem2 = mMap.addMarker(new MarkerOptions()
                .position(leVanThiem2)
                .title("KHO HOÀNG DỰ")
                .snippet("Lê Văn Thiêm, gần quán cháo lòng!")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse1)));

        mLeVanThiem3 = mMap.addMarker(new MarkerOptions()
                .position(leVanThiem3)
                .title("KHO CUỐC NGÁO")
                .snippet("Củ chi, gần quán tiết canh vịt!")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse1)));
        mLeVanThiem4 = mMap.addMarker(new MarkerOptions()
                .position(leVanThiem4)
                .title("KHO HẢI DỚ")
                .snippet("Mỹ Tho, gần quán Hải Hói!")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse1)));
        mLeVanThiem5 = mMap.addMarker(new MarkerOptions()
                .position(leVanThiem5)
                .title("KHO MR HIẾU")
                .snippet("HẢI HÓI 2, KHÔNG SAY KHÔNG VỀ!")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse1)));
        mLeVanThiem6 = mMap.addMarker(new MarkerOptions()
                .position(leVanThiem6)
                .title("KHO SƠN 1")
                .snippet("HẢI HÓI 3, SAY RỒI THÌ NGỦ!")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse1)));
        mLeVanThiem7 = mMap.addMarker(new MarkerOptions()
                .position(leVanThiem7)
                .title("KHO DỰ 2")
                .snippet("Địa chỉ: Bưu chính viễn thông!")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse1)));
        lstMarker.add(mLeVanThiem1);
        lstMarker.add(mLeVanThiem2);
        lstMarker.add(mLeVanThiem3);
        lstMarker.add(mLeVanThiem4);
        lstMarker.add(mLeVanThiem5);
        lstMarker.add(mLeVanThiem6);
        lstMarker.add(mLeVanThiem7);
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }


    @Override
    public void onInfoWindowClick(final Marker marker) {
        Intent intent = new Intent(this,DetailRegisterWareHouseActivity.class);
        startActivity(intent);
    }
}
