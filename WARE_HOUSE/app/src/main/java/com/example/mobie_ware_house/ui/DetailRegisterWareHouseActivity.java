package com.example.mobie_ware_house.ui;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;

import com.daimajia.slider.library.*;
import com.daimajia.slider.library.Animations.*;
import com.daimajia.slider.library.SliderTypes.*;
import com.example.mobie_ware_house.R;
import com.example.mobie_ware_house.viewmodel.*;
import com.tsolution.base.*;

import java.text.*;
import java.util.*;

public class DetailRegisterWareHouseActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener {
    DetailRegisterWareHouseVM detailRegisterWareHouseVM;
    SliderLayout sliderWareHouse;
    EditText edFromDate;
    EditText edToDate;
    private Calendar myCalendar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detailRegisterWareHouseVM = (DetailRegisterWareHouseVM) viewModel;
        myCalendar = Calendar.getInstance();
        initView();
        initBanner();
    }
    private void initBanner() {
        HashMap<String,Integer> file_maps = new HashMap<>();
        file_maps.put("Ảnh mặt ngoài",R.drawable.ba1);
        file_maps.put("Ảnh parcel",R.drawable.ba_2);
        file_maps.put("Ảnh trong kho",R.drawable.ba_3);
        file_maps.put("Ảnh sân", R.drawable.ba_4);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            sliderWareHouse.addSlider(textSliderView);
        }
        sliderWareHouse.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderWareHouse.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderWareHouse.setCustomAnimation(new DescriptionAnimation());
        sliderWareHouse.setDuration(3600);
    }

    private void initView() {
        sliderWareHouse = findViewById(R.id.sliderWareHouse);
        edFromDate = findViewById(R.id.edFromDate);
        edToDate = findViewById(R.id.edToDate);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.detail_register_warehouse;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DetailRegisterWareHouseVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
    private void updateLabel(EditText editText) {

    }
    private void showDatePicker(EditText editText){


    }
    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.edFromDate) {
             showDatePicker(edFromDate);
            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "MM/dd/yyyy"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                    edFromDate.setText(sdf.format(myCalendar.getTime()));
//                updateLabel(editText);
                }

            };
            new DatePickerDialog(this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    }

    @Override
    public void onSliderClick(final BaseSliderView slider) {

    }
}
