package com.example.mobie_ware_house.Adapter;

import android.app.*;
import android.support.v4.app.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.example.mobie_ware_house.ui.*;

public class MyPagerAdapter extends FragmentPagerAdapter {
    private long lvType;

    public MyPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return lvType == 1 ? 5 : 4;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new  HomeActivity();
            case 1:
                return new Mefragment();
            case 2:
                return new Mefragment();
            case 3:
                return new Mefragment();
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}