package com.example.mobie_ware_house.Adapter;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.support.v7.app.*;
import android.text.*;
import android.text.style.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import com.example.mobie_ware_house.R;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter, View.OnClickListener {
    TextView titleUi;
    TextView snippetUi;
    TextView tvNumberParcel;
    TextView tvPriceParcel;
    TextView tvPhone;
    ImageView imgWareHouse;
    Button btnRentWareHouse;

    Context context;

    public CustomInfoWindowAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(final Marker marker) {
        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_info_window, null);
        init(view);
        render(marker, view);
        return view;
    }

    private void render(Marker marker, View view) {
        imgWareHouse.setImageResource(R.drawable.ba_4);
        //
        tvPhone.setText("0336269646");
        //
        SpannableString tvNumberParcelSpan = new SpannableString("30 parcel trống");
        tvNumberParcelSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0,tvNumberParcelSpan.length() , 0);
        tvNumberParcel.setText(tvNumberParcelSpan);
        //
        SpannableString tvPriceParcelSpan = new SpannableString("2.000.000 VND/parcel");
        tvPriceParcelSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0,tvPriceParcelSpan.length() , 0);
        tvPriceParcel.setText(tvPriceParcelSpan);
        //
        String title = marker.getTitle();
        if (title != null) {
            SpannableString titleText = new SpannableString(title);
            titleText.setSpan(new ForegroundColorSpan(Color.BLUE), 0, titleText.length(), 0);
            titleUi.setText(titleText);
        } else {
            titleUi.setText("");
        }

        String snippet = marker.getSnippet();

        if (snippet != null && snippet.length() > 12) {
            SpannableString snippetText = new SpannableString(snippet);
            snippetText.setSpan(new ForegroundColorSpan(Color.BLACK), 0, snippet.length(), 0);
            snippetUi.setText(snippetText);
        } else {
            snippetUi.setText("");
        }
    }

    private void init(View view) {
        titleUi = view.findViewById(R.id.titleWareHouse);
        snippetUi = view.findViewById(R.id.snippetWareHouse);
        tvNumberParcel = view.findViewById(R.id.tvNumberParcel);
        tvPriceParcel = view.findViewById(R.id.tvPriceParcel);
        tvPhone = view.findViewById(R.id.tvPhone);
        imgWareHouse = view.findViewById(R.id.imgWareHouse);
        btnRentWareHouse = view.findViewById(R.id.btnRentWareHouse);
        btnRentWareHouse.setOnClickListener(this);
    }

    @Override
    public View getInfoContents(final Marker marker) {
//        render(marker, mWindow);
        return null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnRentWareHouse){
            Log.e("aaaaaaaa","Click Success");
        }
    }
}
