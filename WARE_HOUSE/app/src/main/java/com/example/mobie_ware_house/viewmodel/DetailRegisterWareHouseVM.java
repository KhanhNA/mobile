package com.example.mobie_ware_house.viewmodel;

import android.app.*;
import android.support.annotation.*;
import android.widget.*;

import com.example.mobie_ware_house.model.dto.*;
import com.example.mobie_ware_house.ui.*;
import com.tsolution.base.*;

import java.text.*;
import java.util.*;

public class DetailRegisterWareHouseVM extends BaseViewModel<InfoWareHouseDTO> {
    public DetailRegisterWareHouseVM(@NonNull final Application application) {
        super(application);
    }
    public void showDatePicker(){
         Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                updateLabel();
            }

        };
//        new DatePickerDialog(, date, myCalendar
//                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
//    private void updateLabel() {
//        String myFormat = "MM/dd/yy"; //In which you need put here
//        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
//
//        edittext.setText(sdf.format(myCalendar.getTime()));
//    }
}
