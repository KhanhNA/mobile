package com.example.mobie_ware_house.model.dto;


import com.tsolution.base.*;

import lombok.*;

@Getter
@Setter
public class InfoWareHouseDTO extends BaseModel {
    String wareHouseId;
    String image;
    String nameWareHouse;
    String AddressWareHouse;
    String numberParcel;

}
