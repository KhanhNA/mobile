package com.example.mobie_ware_house.ui;

import android.content.*;
import android.os.*;
import android.support.annotation.*;
import android.view.*;

import com.example.mobie_ware_house.R;
import com.tsolution.base.*;

public class HomeActivity extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void action(final View view, final BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        if (view.getId()== R.id.btnRentWareHouseMain){
            Intent intent = new Intent(getActivity(),DetailRegisterWareHouseActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.home_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
