package com.example.mobie_ware_house.ui;


import com.example.mobie_ware_house.R;
import com.tsolution.base.*;

public class Mefragment extends BaseFragment {
    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
