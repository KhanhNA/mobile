package com.tsolution.ecommerce.view.chat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.MessageAdapter;
import com.tsolution.ecommerce.model.Contact;
import com.tsolution.ecommerce.model.Product;
import com.tsolution.ecommerce.view.ProductDetails;

import butterknife.BindView;

public class ChatMessageActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private EditText editText;
    Contact contact;

    private MessageAdapter messageAdapter;
    private ListView messagesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i =getIntent();
        setContentView(R.layout.activity_chat_message);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(i.getStringExtra("title_chat"));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();

        messagesView = findViewById(R.id.messages_view);
        editText = (EditText) findViewById(R.id.editText);
        editText.setText(i.getStringExtra("sms_body"));
        messageAdapter = new MessageAdapter(this);
        messagesView.setAdapter(messageAdapter);
        initToolbar();

    }

    private void initToolbar() {


        Intent i = getIntent();
        toolbar.setTitleTextColor(Color.GRAY);
        contact = (Contact) i.getSerializableExtra("contact");

//        setTitle(i.getStringExtra("name"));
        if(contact != null) {

            TextView poit = findViewById(R.id.txt_contact_poit);
            poit.setText("$ " + contact.getContact_poit());

            String imgUrl = contact.getUrlImg();
            ImageView img = findViewById(R.id.backdrop);
            Picasso.with(ChatMessageActivity.this).load(imgUrl).into(img);


        }
    }
    public void sendMessage(View view) {
        String message = editText.getText().toString();
        if (message.length() > 0) {
            messageAdapter.add(new Message(message, true));
            messageAdapter.add(new Message("Masan........", false));
            editText.getText().clear();
            messagesView.setSelection(messageAdapter.getCount() - 1);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:break;
        }

        return super.onOptionsItemSelected(item);
    }


}


