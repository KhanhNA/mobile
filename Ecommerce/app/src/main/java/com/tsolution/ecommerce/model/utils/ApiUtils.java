package com.tsolution.ecommerce.model.utils;


import com.tsolution.ecommerce.model.data.RetrofitClient;
import com.tsolution.ecommerce.model.data.SOService;

public class ApiUtils {

    public static final String BASE_URL = "http://192.168.1.98:8080/api/";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }
}