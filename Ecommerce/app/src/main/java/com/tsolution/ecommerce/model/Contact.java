package com.tsolution.ecommerce.model;

import java.io.Serializable;

public class Contact implements Serializable {
    private String urlImg;
    private String contact_name;
    private int contact_poit;
    private String contact_phone;
    private String contact_email;
    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Contact(String urlImg, String contact_name, int contact_poit, String contact_phone, String email) {
        this.urlImg = urlImg;
        this.contact_name = contact_name;
        this.contact_poit = contact_poit;
        this.contact_phone = contact_phone;
        this.contact_email = contact_email;
        this.email = email;
    }
    public Contact(String urlImg, String contact_name, int contact_poit, String contact_phone, String email,int status) {
        this.urlImg = urlImg;
        this.contact_name = contact_name;
        this.contact_poit = contact_poit;
        this.contact_phone = contact_phone;
        this.contact_email = contact_email;
        this.email = email;
        this.status = status;
    }


    public Contact(){

    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public int getContact_poit() {
        return contact_poit;
    }

    public void setContact_poit(int contact_poit) {
        this.contact_poit = contact_poit;
    }
}
