package com.tsolution.ecommerce.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tsolution.ecommerce.fragment.Mefragment;
import com.tsolution.ecommerce.fragment.SampleFragment;
import com.tsolution.ecommerce.fragment.SubProductFragment;

/**
 * Created by KaKa on 5/16/2017.
 */

public class CategoriesPagerAdapter extends FragmentPagerAdapter {
    private Mefragment categoryFragment;
    private SubProductFragment subProductFragment;
    private SampleFragment sampleFragment;
    private String[] TITLES;

    public CategoriesPagerAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        //Show Fragment
        switch (position) {
            case 0:
                subProductFragment = new SubProductFragment();
                return subProductFragment;
            case 1:
                subProductFragment = new SubProductFragment();
                return subProductFragment;
            case 2:
                subProductFragment = new SubProductFragment();
                return subProductFragment;
            case 3:
                subProductFragment = new SubProductFragment();
                return subProductFragment;



        }
        return null;
    }
}
