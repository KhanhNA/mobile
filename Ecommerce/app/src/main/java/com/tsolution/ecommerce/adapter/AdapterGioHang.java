package com.tsolution.ecommerce.adapter;

import android.app.*;
import android.content.*;
import android.support.annotation.*;
import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import com.tsolution.ecommerce.*;
import com.tsolution.ecommerce.view.Order.*;
import com.tsolution.ecommerce.view.Order.SanPhamDTO.*;

import java.text.*;
import java.util.*;

public class AdapterGioHang extends RecyclerView.Adapter<AdapterGioHang.ViewHolderGioHang> {
    Context context;
    List<SanPham> sanPhamList;
    ModelGioHang modelGioHang;
    private Dialog dialog;


    public AdapterGioHang(Context context, List<SanPham> sanPhamList){
        this.context = context;
        this.sanPhamList = sanPhamList;
        modelGioHang = new ModelGioHang();
        modelGioHang.MoKetNoiSQL(context);
    }

    public class ViewHolderGioHang extends RecyclerView.ViewHolder {
        //liet ke du lieu can
        TextView txtTenTieuDeGioHang,txtGiaTienGioHang,txtSoLuongSanPham,txtsoluong,txtChinhSua;
        ImageView imHinhGioHang,imXoaSanPhamGioHang;
        ImageButton imTangSoLuongSPGioHang,imGiamSoLuongSPGioHang;
        LinearLayout layout_KhuyenMai;
        View view1,view2;
        public ViewHolderGioHang(@NonNull final View itemView) {
            super(itemView);
            txtTenTieuDeGioHang = (TextView) itemView.findViewById(R.id.txtTieuDeGioHang);
            txtGiaTienGioHang = (TextView) itemView.findViewById(R.id.txtGiaGioHang);
            txtSoLuongSanPham = (TextView) itemView.findViewById(R.id.txtSoLuongSanPham);
            txtChinhSua = itemView.findViewById(R.id.txtChinhSua);
            imHinhGioHang = (ImageView) itemView.findViewById(R.id.imHinhGioHang);
            imXoaSanPhamGioHang = (ImageView) itemView.findViewById(R.id.imXoaSanPhamGioHang);
            imGiamSoLuongSPGioHang = (ImageButton) itemView.findViewById(R.id.imGiamSoLuongSPTrongGioHang);
            imTangSoLuongSPGioHang = (ImageButton) itemView.findViewById(R.id.imTangSoLuongSPTrongGioHang);
            layout_KhuyenMai = itemView.findViewById(R.id.layout_KhuyenMai);
            view1 = itemView.findViewById(R.id.view1);
            view2 = itemView.findViewById(R.id.view2);
            txtsoluong = itemView.findViewById(R.id.txtsoluong);

        }
    }
    @NonNull
    @Override
    public ViewHolderGioHang onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout_giohang,parent,false);
        ViewHolderGioHang viewHolderGioHang= new ViewHolderGioHang(view);
        return viewHolderGioHang;
    }

    @Override
    public void onBindViewHolder( final ViewHolderGioHang holder, final int position) {
        final SanPham sanPham =sanPhamList.get(position);
        holder.txtTenTieuDeGioHang.setText(sanPham.getTENSP());

        NumberFormat numberFormat = new DecimalFormat("###,###");
        String gia = numberFormat.format(sanPham.getGIA()).toString();
        holder.txtGiaTienGioHang.setText(gia + " VNĐ ");

        //convert byte -> image
        byte[] hinhsanpham = sanPham.getHinhgiohang();
//        Bitmap bitmapHinhGioHang = BitmapFactory.decodeByteArray(hinhsanpham,0,hinhsanpham.length);
//        holder.imHinhGioHang.setImageBitmap(bitmapHinhGioHang);

        holder.imXoaSanPhamGioHang.setTag(sanPham.getMASP());

        holder.imXoaSanPhamGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModelGioHang modelGioHang = new ModelGioHang();
                modelGioHang.MoKetNoiSQL(context);
                modelGioHang.XoaSanPhamTrongGioHang((int)v.getTag());
                sanPhamList.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.txtSoLuongSanPham.setText(String.valueOf(sanPham.getSOLUONG()));
        holder.imTangSoLuongSPGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int soluong = Integer.parseInt(holder.txtSoLuongSanPham.getText().toString());
                soluong++;
                holder.txtSoLuongSanPham.setText(String.valueOf(soluong));
                int soluongtonkho = sanPham.getSOLUONGTONKHO();

                if(soluong < soluongtonkho){
                    soluong++;
                }else{
                    Toast.makeText(context,"Số lượng sản phẩm bạn mua quá số lượng có trong kho của cửa hàng !",Toast.LENGTH_SHORT).show();
                }

                modelGioHang.CapNhatSoLuongSanPhamGioHang(sanPham.getMASP(),soluong);

            }
        });
        holder.imGiamSoLuongSPGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int soluong = Integer.parseInt(holder.txtSoLuongSanPham.getText().toString());
                if(soluong > 1){
                    soluong--;
                }
                modelGioHang.CapNhatSoLuongSanPhamGioHang(sanPham.getMASP(),soluong);
                holder.txtSoLuongSanPham.setText(String.valueOf(soluong));
            }
        });
        holder.layout_KhuyenMai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                showDialog();
            }
        });
        holder.txtChinhSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (holder.txtChinhSua.getText().toString().equals("Hoàn Thành"))
                {
                    holder.txtChinhSua.setText("Sửa");
                    holder.view1.setVisibility(View.GONE);
                    holder.view2.setVisibility(View.GONE);
                    holder.txtsoluong.setVisibility(View.VISIBLE);
                    holder.imTangSoLuongSPGioHang.setVisibility(View.GONE);
                    holder.imGiamSoLuongSPGioHang.setVisibility(View.GONE);

                }else if (holder.txtChinhSua.getText().toString().equals("Sửa")){
                    holder.txtChinhSua.setText("Hoàn Thành");
                    holder.view1.setVisibility(View.VISIBLE);
                    holder.view2.setVisibility(View.VISIBLE);
                    holder.txtsoluong.setVisibility(View.GONE);
                    holder.imTangSoLuongSPGioHang.setVisibility(View.VISIBLE);
                    holder.imGiamSoLuongSPGioHang.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void showDialog() {
        dialog = new Dialog(context);
        dialog.setTitle("Thông tin chi tiết");
        dialog.setContentView(R.layout.dialog_chuong_trinh_khuyen_mai);
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return sanPhamList.size();
    }


}
