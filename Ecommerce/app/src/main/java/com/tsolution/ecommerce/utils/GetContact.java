package com.tsolution.ecommerce.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import com.tsolution.ecommerce.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class GetContact {
    Cursor getPhoneNumber;
    ContentResolver resolver ;

    Context context;

    public GetContact(Context context) {
        this.context = context;
    }

    public List<Contact> getData() {
        List<Contact> data = new ArrayList<>();
        String[] contactProjection = new String[]{
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME
        };

        String contactSelection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '1'";
        getPhoneNumber = context.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                contactProjection,
                contactSelection,
                null,
                ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");

        if (getPhoneNumber != null) {
            Log.e("count", "" + getPhoneNumber.getCount());
            if (getPhoneNumber.getCount() == 0) {
                Toast.makeText(context, "No contacts in your contact list.", Toast.LENGTH_LONG).show();
            }

            while (getPhoneNumber.moveToNext()) {
                String id = getPhoneNumber.getString(getPhoneNumber.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                String name = getPhoneNumber.getString(getPhoneNumber.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = getPhoneNumber.getString(getPhoneNumber.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String EmailAddr = getPhoneNumber.getString(getPhoneNumber.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA2));
                String image_thumb = getPhoneNumber.getString(getPhoneNumber.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));

                Contact contact = new Contact();
                contact.setUrlImg(image_thumb);
                contact.setContact_name(name);
                contact.setContact_phone(phoneNumber);
                contact.setEmail(EmailAddr);
                contact.setEmail(id);
                data.add(contact);
            }
        } else {
            Log.e("Cursor close 1", "----");
        }

        getPhoneNumber.close();

        return data;
    }
}
