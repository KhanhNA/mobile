package com.tsolution.ecommerce.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.view.ListOrderActivity;
import com.tsolution.ecommerce.view.TranferPoitActivity;

import butterknife.BindView;

public class Mefragment extends Fragment implements View.OnClickListener {
    private View rootView;
    TextView btnDonMua, btnDonMuaCap2, btnDoiDiem, btnVi, btnShareFB, btnSetting, btnHelp;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView= inflater.inflate(R.layout.layout_me_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //create control
        btnDonMua = getView().findViewById(R.id.btnDonMua);
        btnDonMuaCap2 = getView().findViewById(R.id.btnDonMuaCap2);
        btnDoiDiem = getView().findViewById(R.id.btnDoiDiem);
        btnVi = getView().findViewById(R.id.btnVi);
        btnShareFB = getView().findViewById(R.id.btnShareFB);
        btnSetting = getView().findViewById(R.id.btnSetting);
        btnHelp = getView().findViewById(R.id.btnHelp);
        //set onclick
        btnDonMua.setOnClickListener(this);
        btnDonMuaCap2.setOnClickListener(this);
        btnDoiDiem.setOnClickListener(this);
        btnVi.setOnClickListener(this);
        btnShareFB.setOnClickListener(this);
        btnSetting.setOnClickListener(this);
        btnHelp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){

            case R.id.btnDonMua:
                 i = new Intent(getActivity(), ListOrderActivity.class);
                 i.putExtra("index", 0);
                getContext().startActivity(i);
                break;
            case R.id.btnDonMuaCap2:
                i = new Intent(getActivity(), ListOrderActivity.class);
                i.putExtra("index", 1);
                getContext().startActivity(i);
                break;
            case R.id.btnDoiDiem:
                i = new Intent(getActivity(), TranferPoitActivity.class);
                getContext().startActivity(i);
                break;
            case R.id.btnVi:
                break;
            case R.id.btnShareFB:
                break;
            case R.id.btnSetting:
                break;
            case R.id.btnHelp:
                break;
        }
    }
}
