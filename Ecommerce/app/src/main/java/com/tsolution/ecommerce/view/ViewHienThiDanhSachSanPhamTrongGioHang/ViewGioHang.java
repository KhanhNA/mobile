package com.tsolution.ecommerce.view.ViewHienThiDanhSachSanPhamTrongGioHang;

import com.tsolution.ecommerce.view.Order.SanPhamDTO.*;

import java.util.*;

public interface ViewGioHang {
    void HienThiDanhSachSanPhamTrongGioHang(List<SanPham> sanPhamList);
}
