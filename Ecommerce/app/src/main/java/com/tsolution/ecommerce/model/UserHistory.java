package com.tsolution.ecommerce.model;

import java.util.ArrayList;

public class UserHistory {
    Order order;
    int nimusPoit;
    String date;
    String description;

    public UserHistory(Order order, int nimusPoit, String date, String description) {
        this.order = order;
        this.nimusPoit = nimusPoit;
        this.date = date;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNimusPoit() {
        return nimusPoit;
    }

    public void setNimusPoit(int nimusPoit) {
        this.nimusPoit = nimusPoit;
    }





    public UserHistory() {
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
