package com.tsolution.ecommerce.view;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.tsolution.ecommerce.R;

public class ForgetPassword_Comfirm extends AppCompatActivity implements View.OnClickListener {

    EditText txtOTP;
    TextView txtMsgTypeOTP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forget_password__comfirm);
        txtMsgTypeOTP = findViewById(R.id.txtTypeOTP);
        txtOTP = findViewById(R.id.txtOTPCode);
        setKeyboardVisibilityListener();
    }

    public void comfirm(View view) {
        if(txtOTP.getText().toString().equals("123456a@A")){
            Intent i = new Intent(ForgetPassword_Comfirm.this, ChangePassActivity.class);
            startActivity(i);
        }else{
            txtMsgTypeOTP.setText("Mã OTP không chính xác");
        }
    }




    private void setKeyboardVisibilityListener() {
        final View parentView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
        parentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private boolean alreadyOpen;
            private final int defaultKeyboardHeightDP = 100;
            private final int EstimatedKeyboardDP = defaultKeyboardHeightDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);
            private final Rect rect = new Rect();

            @Override
            public void onGlobalLayout() {
                int estimatedKeyboardHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, parentView.getResources().getDisplayMetrics());
                parentView.getWindowVisibleDisplayFrame(rect);
                int heightDiff = parentView.getRootView().getHeight() - (rect.bottom - rect.top);
                boolean isShown = heightDiff >= estimatedKeyboardHeight;
                // Gets linearlayout
                LinearLayout layout = findViewById(R.id.LnLLogo);
                // Gets the layout params that will allow you to resize the layout
                ViewGroup.LayoutParams params = layout.getLayoutParams();
                // Changes the height and width to the specified *pixels*

                if (isShown == alreadyOpen) {
                    params.height = 1300;
                    layout.setLayoutParams(params);
                    return;
                } else {
                    params.height = 700;
                    layout.setLayoutParams(params);
                    return;
                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnComfirm:

        }
    }
}
