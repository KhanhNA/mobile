package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.listener.OnClickOrder;
import com.tsolution.ecommerce.model.Order;
import com.tsolution.ecommerce.model.Product;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ListOrderAdapter extends RecyclerView.Adapter<ListOrderAdapter.MyViewHolder> {
    private List<Order> arrData;
    private Context mContext;
private OnClickOrder onClickOrder;
    public ListOrderAdapter(Context mContext, List<Order> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }
    public void setOnClickOrder(OnClickOrder onClick)
    {
        this.onClickOrder = onClick;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_order, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListOrderAdapter.MyViewHolder holder, int i) {
        if (arrData != null && arrData.size() > 0) {
            Order order = arrData.get(i);

            Picasso.with(mContext).load(order.getUrlProduct()).into(holder.imgProduct);
            holder.txtNameProduct.setText(order.getProductName());
            holder.txtQuantity.setText("x "+order.getQuantity());
            holder.txtPrice.setText("đ " + (order.getQuantity() * order.getPrice()));

        }

    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNameProduct, txtQuantity, txtPrice;
        public ImageView imgProduct;

        public MyViewHolder(View view) {
            super(view);
            txtNameProduct = view.findViewById(R.id.txtNameProduct);
            txtQuantity = view.findViewById(R.id.txtQuantity);
            txtPrice = view.findViewById(R.id.txtPrice);
            imgProduct = view.findViewById(R.id.imgProduct);
            imgProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                    public void onClick(View v) {
                    if(onClickOrder!=null)
                    {
                        onClickOrder.onClickOrder();
                    }
                }
            });
        }
    }
}
