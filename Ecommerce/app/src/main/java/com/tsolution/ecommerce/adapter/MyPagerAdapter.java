package com.tsolution.ecommerce.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tsolution.ecommerce.fragment.Mefragment;
import com.tsolution.ecommerce.fragment.Contact_Fragment;
import com.tsolution.ecommerce.fragment.NotificationFragment;
import com.tsolution.ecommerce.fragment.ProductFragment;

/**
 * Created by PhamBien on 5/16/2017.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {
    private Mefragment mefragment;
    private Contact_Fragment contact_fragment;
    private ProductFragment productFragment;
    private NotificationFragment notificationFragment;
    private String[] TITLES;

    public MyPagerAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        //Show Fragment
        switch (position) {
            case 0:
                productFragment = new ProductFragment();
                return productFragment;

            case 1:
                contact_fragment = new Contact_Fragment();
                return contact_fragment;

            case 2:
                notificationFragment = new NotificationFragment();
                return notificationFragment;

            case 3:
                mefragment = new Mefragment();
                return mefragment;


        }
        return null;
    }
}
