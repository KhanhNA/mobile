package com.tsolution.ecommerce.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Order;
import com.tsolution.ecommerce.model.OrderDao;
import com.tsolution.ecommerce.view.AppController;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

@SuppressLint("ValidFragment")
public class BottomBuyDialogFragment extends BottomSheetDialogFragment {


    private View rootView;
    private Order mOrder;
    private OrderDao orderDao;
    EditText txtCount;
    int count;

    public BottomBuyDialogFragment(Order order) {
        // Required empty public constructor
        this.mOrder = order;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderDao = AppController.getInstance().getDaoSession().getOrderDao();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.layout_bottom_buy, container, false);
        ButterKnife.bind(this,rootView);

        return rootView;
    }

    @OnClick(R.id.btnBuy)
    public void buyProduct() {
        if (mOrder != null) {
            mOrder.setQuantity(Integer.parseInt(txtCount.getText().toString()));
            orderDao.insert(mOrder);
            dismiss();
            Toast.makeText(getContext(), "Mua hàng thành công", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        count = 1;
        txtCount = getView().findViewById(R.id.count);
        txtCount.setText("1");
    }


    @OnClick(R.id.add)
    public void add(){
        count++;
        txtCount.setText(String.valueOf(count));
    }
    @OnClick(R.id.minus)
    public void minus(){
        if(count > 1){
            count--;
        }
        txtCount.setText(String.valueOf(count));
    }
    @OnTextChanged(R.id.count)
    public void changed()
    {
        if(txtCount.getText().toString().equals("")){
            count = 1;
        }

        if(txtCount.length() > 6){
            txtCount.setText("1");
        }
        else {
            count = Integer.parseInt(txtCount.getText().toString());
        }
    }
}
