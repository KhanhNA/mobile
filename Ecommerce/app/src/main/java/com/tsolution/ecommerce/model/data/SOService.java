package com.tsolution.ecommerce.model.data;

import com.tsolution.ecommerce.model.Category;
import com.tsolution.ecommerce.model.Product;
import com.tsolution.ecommerce.model.ProductDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface SOService {

    @GET("v1/products")
    Call<SOAnswersResponse> getProducts();

    @GET("v1/categories")
    Call<Category> getCategories();

    @GET("v1/products/{id}")
    Call<ProductDetail> getProductById(@Path("id") String id);


}