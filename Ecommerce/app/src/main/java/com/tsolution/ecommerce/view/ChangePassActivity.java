package com.tsolution.ecommerce.view;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.tsolution.ecommerce.R;

public class ChangePassActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        setKeyboardVisibilityListener();
    }

    private void setKeyboardVisibilityListener() {
        final View parentView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
        parentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private boolean alreadyOpen;
            private final int defaultKeyboardHeightDP = 100;
            private final int EstimatedKeyboardDP = defaultKeyboardHeightDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);
            private final Rect rect = new Rect();

            @Override
            public void onGlobalLayout() {
                int estimatedKeyboardHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, parentView.getResources().getDisplayMetrics());
                parentView.getWindowVisibleDisplayFrame(rect);
                int heightDiff = parentView.getRootView().getHeight() - (rect.bottom - rect.top);
                boolean isShown = heightDiff >= estimatedKeyboardHeight;
                // Gets linearlayout
                LinearLayout layout = findViewById(R.id.LnLLogo);
                LinearLayout lnlButton = findViewById(R.id.lnlButton);
                // Gets the layout params that will allow you to resize the layout
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
                LinearLayout.LayoutParams button_params = (LinearLayout.LayoutParams) lnlButton.getLayoutParams();
                // Changes the height and width to the specified *pixels*

                if (isShown == alreadyOpen) {
                    params.weight = 6f;
                    layout.setLayoutParams(params);
                    return;
                }else{
                    params.weight = 1f;
                    button_params.weight = 6;
                    layout.setLayoutParams(params);
                    lnlButton.setLayoutParams(button_params);
                    return;
                }

            }
        });
    }
}
