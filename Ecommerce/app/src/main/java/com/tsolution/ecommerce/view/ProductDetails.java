package com.tsolution.ecommerce.view;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ListProductAdapter;
import com.tsolution.ecommerce.adapter.SubProductAdapter;
import com.tsolution.ecommerce.fragment.BottomBuyDialogFragment;
import com.tsolution.ecommerce.fragment.SubProductFragment;
import com.tsolution.ecommerce.model.Description;
import com.tsolution.ecommerce.model.ProductDetail;
import com.tsolution.ecommerce.model.data.SOAnswersResponse;
import com.tsolution.ecommerce.model.data.SOService;
import com.tsolution.ecommerce.model.utils.ApiUtils;
import com.tsolution.ecommerce.view.Order.Presenter.PresenterLogicChiTietSanPham;
import com.tsolution.ecommerce.view.Order.SanPhamDTO.SanPham;
import com.tsolution.ecommerce.view.Order.View.ViewChiTietSanPham;
import com.tsolution.ecommerce.view.ViewHienThiDanhSachSanPhamTrongGioHang.*;
import com.tsolution.ecommerce.view.chat.ChatActivity;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetails extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewChiTietSanPham, ViewPagerEx.OnPageChangeListener{
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.slider)
    SliderLayout mDemoSlider;
    private SOService mService;
    private String imgUrl = "";
    private String namePro = "";
    private long productId = 0;
    private long price = 1200;
    private ProductDetail product;
    PresenterLogicChiTietSanPham presenterLogicChiTietSanPham;
    TextView txtGioHang;
    boolean onPause = false;
    private SubProductAdapter subProductAdapter;
    @BindView(R.id.rcRelativeProduct)
    RecyclerView rcRelativeProduct;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_product_detail);
        ButterKnife.bind(this);

        //Du
        presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham(this);
        initToolbar();
    }

    private void initToolbar() {
        final HashMap<String, String> url_maps = new HashMap();
        url_maps.put("Masan", "http://giavichinsu.com/wp-content/uploads/2018/04/bo-nuoc-mam-chin-su-thuong-hang.jpg");
        url_maps.put("chisu", "https://www.masanconsumer.com/images/imgchinsu/banner.jpg");
        url_maps.put("fast food", "http://1.bp.blogspot.com/-e8kQyAb4koM/VNxYL1Ebh3I/AAAAAAAAAEE/KIZlqXWKo44/s1600/banner%2Bnuoc%2Bmam%2Bphu%2Bquoc%2Bhong%2Btuyet.jpg");
        url_maps.put("B'Fast", "https://masanfood-cms-production.s3-ap-southeast-1.amazonaws.com/iblock/623/62326d58273e62c4692f773c9892fd0e.jpg");

        sliderCollection(url_maps);
        setSupportActionBar(toolbar);
        mService = ApiUtils.getSOService();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent i = getIntent();
        toolbar.setTitleTextColor(Color.GRAY);
        namePro = i.getStringExtra("name");
        setTitle(namePro);


        final RatingBar ratingBar = findViewById(R.id.ratingBar);
        final TextView name = findViewById(R.id.txtNameProduct);
        final TextView price = findViewById(R.id.txtPrice);
        final TextView numberRating = findViewById(R.id.number_rate);
        final TextView priceSale = findViewById(R.id.txtPriceSale);
        final TextView ratingCount = findViewById(R.id.ratingCount);
        priceSale.setPaintFlags(priceSale.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        final ExpandableTextView expTv1 = findViewById(R.id.sample1)
                .findViewById(R.id.expand_text_view);


        final String productId = i.getStringExtra("product_id");
        product = new ProductDetail();
        if(productId != null && !"".equals(productId) ) {
            mService.getProductById(productId).enqueue(new Callback<ProductDetail>() {
                @Override
                public void onResponse(Call<ProductDetail> call, Response<ProductDetail> response) {
                    if(response.isSuccessful()) {
                        product = response.body();
                        Description description = product.getDescription();
                        if(description != null) {
                            name.setText(description.getTitle());
                            //expTv1.setText(description.getDescription());
                            expTv1.setText(getString(R.string.product_des));
                        }
                        //priceSale.setText(product.getOriginalPrice().toString());
                        numberRating.setText(product.getRating().toString());
                        ratingBar.setRating(product.getRating());
                        price.setText("$" + product.getPrice());
                        ratingCount.setText(product.getRatingCount()+ "");
                        String[] images = product.getImgaes();
                        if(images != null){
                            for(int i = 0; i < images.length; i++){
                                url_maps.put(" ", images[i]);
                            }
                        }
                        Log.e("ProductDetail", "posts load success from API");
                    }else {
                        int statusCode  = response.code();
                        // handle request errors depending on status code
                        Log.e("ProductDetail", "posts load false from API");
                    }
                }

                @Override
                public void onFailure(Call<ProductDetail> call, Throwable t) {
                    Log.e("Failure", "error loading from API" + t);
                }
            });
            mService.getProducts().enqueue(new Callback<SOAnswersResponse>() {
                @Override
                public void onResponse(Call<SOAnswersResponse> call, Response<SOAnswersResponse> response) {
                    if (response.isSuccessful()) {
                        ArrayList arrData = (ArrayList) response.body().getItems();
                        //relative
                        subProductAdapter = new SubProductAdapter(ProductDetails.this, arrData);
                        GridLayoutManager layoutPromotion = new GridLayoutManager(ProductDetails.this, 1, GridLayoutManager.HORIZONTAL, false);
                        rcRelativeProduct.setLayoutManager(layoutPromotion);
                        rcRelativeProduct.setAdapter(subProductAdapter);
                    } else {
                        int statusCode = response.code();
                        // handle request errors depending on status code
                        Log.e("SubProducFragment", "posts load false from API");
                    }
                }

                @Override
                public void onFailure(Call<SOAnswersResponse> call, Throwable t) {
                    Log.e("Failure", "error loading from API:" + t);
                }
            });


        }

    }
    // sliderCollection
    private void sliderCollection(HashMap<String, String> url_maps) {
        for (String title : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(ProductDetails.this);
            // initialize a SliderLayout
            textSliderView
                    .description(title)
                    .image(url_maps.get(title))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", title);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(2600);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
    }

    @OnClick(R.id.imgCart)
    public void openCart() {
        startActivity(new Intent(ProductDetails.this, CartActivity.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        MenuItem iGioHang =menu.findItem(R.id.action_cart);
        View giaoDienCustomGioHang = MenuItemCompat.getActionView(iGioHang);
        txtGioHang =(TextView) giaoDienCustomGioHang.findViewById(R.id.txtSoLuongSanPhamGioHang);
        txtGioHang.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
        giaoDienCustomGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iGioHang = new Intent(ProductDetails.this, CartActivity.class);
                startActivity(iGioHang);
            }
        });
        return true;
    }

    @OnClick(R.id.txtBuy)
    public void showBottomSheetDialogFragment() {
        SanPham sanPhamGioHang = new SanPham();
        sanPhamGioHang.setMASP(1111);
        sanPhamGioHang.setTENSP("Iphone XS");
        sanPhamGioHang.setGIA(10000);
        sanPhamGioHang.setHinhgiohang(null);
        sanPhamGioHang.setSOLUONG(1);
        sanPhamGioHang.setSOLUONGTONKHO(5);
        presenterLogicChiTietSanPham.ThemGioHang(sanPhamGioHang,this);
//        //
//        Order order = new Order();
//        order.setId(null);
//        order.setProductId(productId);
//        order.setUrlProduct(imgUrl);
//        order.setProductName(namePro);
//        order.setPrice(price);
//        //
//        BottomBuyDialogFragment bottomBuyDialogFragment = new BottomBuyDialogFragment(order);
//        bottomBuyDialogFragment.show(getSupportFragmentManager(), bottomBuyDialogFragment.getTag());

//        presenterLogicChiTietSanPham.ThemGioHang(sanPhamGioHang,this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(onPause){
            PresenterLogicChiTietSanPham presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();
            txtGioHang.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        onPause = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    @OnClick(R.id.imgChat)
    public void openChatMes()
    {
        startActivity(new Intent(ProductDetails.this, ChatActivity.class));
    }
    @Override
    public void onStop() {
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }
    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void ThemGioHangThanhCong() {
        Toast.makeText(this,"Sản phẩm đã được thêm vào giỏ hàng !",Toast.LENGTH_SHORT).show();
        Log.e("soGIoHang2",String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
        txtGioHang.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
    }

    @Override
    public void ThemGiohangThatBai() {
        Toast.makeText(this,"Sản phẩm đã có trong giỏ hàng !",Toast.LENGTH_SHORT).show();
    }
}
