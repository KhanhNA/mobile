package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Product;

import java.util.ArrayList;



public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.MyViewHolder>  {
    private ArrayList<Product> arrData;
    private Context mContext;

    public BrandAdapter(Context mContext, ArrayList<Product> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_brand, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BrandAdapter.MyViewHolder holder, int i) {
        if(arrData!=null && arrData.size()>0)
        {
            Product movie = arrData.get(i);
            Picasso.with(mContext).load(arrData.get(i).getImage()).into(holder.imgProduct);
            holder.namePro.setText(movie.getDescription().getTitle());

        }

    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView namePro;
        public ImageView imgProduct;
        public MyViewHolder(View view) {
            super(view);
            imgProduct = view.findViewById(R.id.imgProduct);
            namePro =  view.findViewById(R.id.txtNameProduct);


        }
    }
}
