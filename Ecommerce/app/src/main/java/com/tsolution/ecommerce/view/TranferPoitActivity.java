package com.tsolution.ecommerce.view;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;


import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.TransferPoitPapersAdapter;
import com.tsolution.ecommerce.view.chat.ChatActivity;
import com.tsolution.ecommerce.widget.SlidingTabLayout;

public class TranferPoitActivity extends AppCompatActivity {

    Toolbar toolbar;
    SlidingTabLayout tabs;
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tranfer_poit);
        toolbar = findViewById(R.id.toolbar);
        tabs = findViewById(R.id.sliding_tabs);
        pager = findViewById(R.id.pager);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(4);

        //TabLayout
        tabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        tabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));

        tabs.setDistributeEvenly(true);
        // View Pager
        TransferPoitPapersAdapter myPagerAdapter = new TransferPoitPapersAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.arr_title_request_transfer_poit));
        pager.setAdapter(myPagerAdapter);
        pager.setOffscreenPageLimit(2);
        pager.setHorizontalScrollBarEnabled(false);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabs.setViewPager(pager);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);

        //set curren page
        Intent i = getIntent();
        int index = i.getIntExtra("index", 0);
        pager.setCurrentItem(index);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_chat:
                Intent i = new Intent(TranferPoitActivity.this, ChatActivity.class);
                startActivity(i);

                return true;

            default:break;
        }

        return super.onOptionsItemSelected(item);
    }
}
