package com.tsolution.ecommerce.Constant;

public interface IntentContants {
    public static final String INTENT_USER_NAME = "username";
    public static final String INTENT_LANGUAGE = "language";
}
