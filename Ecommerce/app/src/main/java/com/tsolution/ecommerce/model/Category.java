package com.tsolution.ecommerce.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Category implements Serializable {

    @SerializedName("title")
    @Expose
    String categoryName;

    @SerializedName("urlImage")
    @Expose
    String categoryIcon;

    @SerializedName("categoryId")
    @Expose
    String categoryId;

    @SerializedName("content")
    @Expose
    private List<Category> items = null;

    public void setItems(List<Category> items) {
        this.items = items;
    }

    public List<Category> getItems() {
        return items;
    }

    public Category() {
    }

    public Category(String categoryName, String categoryIcon, String categoryId) {
        this.categoryName = categoryName;
        this.categoryIcon = categoryIcon;
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}

