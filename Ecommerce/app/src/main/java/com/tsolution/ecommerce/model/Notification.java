package com.tsolution.ecommerce.model;

public class Notification {
    private int typeNotification;//0: ds đơn hàng, 1: sản phẩm, 2 cập nhật, 3 tin nhắn hệ thống
    private  String urlImg;
    private String title;
    private String description;

    public Notification(String urlImg, String title, String description){
        this.urlImg = urlImg;
        this.title = title;
        this.description = description;
    }
    public Notification(String urlImg, String title, String description, int typeNotification){
        this.urlImg = urlImg;
        this.title = title;
        this.description = description;
        this.typeNotification = typeNotification;
    }

    public int getTypeNotification() {
        return typeNotification;
    }

    public void setTypeNotification(int typeNotification) {
        this.typeNotification = typeNotification;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
