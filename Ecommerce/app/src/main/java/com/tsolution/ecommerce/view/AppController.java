package com.tsolution.ecommerce.view;

import android.app.Application;

import com.tsolution.ecommerce.model.DaoMaster;
import com.tsolution.ecommerce.model.DaoSession;

import org.greenrobot.greendao.database.Database;

public class AppController extends Application {
    public static final boolean ENCRYPTED = true;
    private static AppController mInstance;
    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "e-commerce"); //The users-db here is the name of our database.
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public DaoSession getDaoSession() {
        return daoSession;
    }
}
