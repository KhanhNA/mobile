package com.tsolution.ecommerce.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tsolution.ecommerce.fragment.Contact_Fragment;
import com.tsolution.ecommerce.fragment.ListOrderedFragment;
import com.tsolution.ecommerce.fragment.UserHistoryFragment;


public class TransferPoitPapersAdapter extends FragmentPagerAdapter {
    private String[] TITLES;
    private UserHistoryFragment sampleFragment;
    private Contact_Fragment contact_fragment;

    public TransferPoitPapersAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        //Show Fragment
        switch (position) {
            case 0:
               sampleFragment = new UserHistoryFragment();
               return sampleFragment;
            case 1:
                contact_fragment = new Contact_Fragment();
                return contact_fragment;

        }
        return null;
    }
}
