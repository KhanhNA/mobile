package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Product;
import com.tsolution.ecommerce.view.ProductDetails;

import java.util.ArrayList;
import java.util.List;

public class SubProductAdapter extends RecyclerView.Adapter<SubProductAdapter.CustomViewHolder> {
    private List<Product> arrData;
    private Context mContext;


    public SubProductAdapter(Context context, ArrayList<Product> posts) {
        arrData = posts;
        mContext = context;

    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_product, parent, false);

        return new CustomViewHolder(itemView);
    }
    public void updateProducts(List<Product> items) {
        arrData = items;
        notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        Product item  = arrData.get(position);
       // holder.name.setText(item.getDescription().getTitle());
        holder.price.setText("Giá: " + item.getPrice());
        holder.setIndex(position);
//        holder.rating.setRating(item.getRating());
//        holder.txtQuantity.setText("Số lượng: "  + item.getQuantity());
        String img = item.getImage();
        if(img != null) {
            Picasso.with(mContext).load(img).into(holder.image);
        }


    }


    @Override
    public int getItemCount() {
        return arrData==null?0:arrData.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView name, price, txtQuantity;
        public ImageView image;
        public LinearLayout layoutRoot;
        RatingBar rating;
        private int index;

        /**
         * Constructor to initialize the Views
         *
         * @param itemView
         */
        public CustomViewHolder(View itemView) {
            super(itemView);
            txtQuantity = itemView.findViewById(R.id.txtQuantity);
            name =  itemView.findViewById(R.id.pizzaName);
            price =  itemView.findViewById(R.id.pizzaPrice);
            image =  itemView.findViewById(R.id.pizzaImage);
            layoutRoot = itemView.findViewById(R.id.layoutRoot);
            rating = itemView.findViewById(R.id.ratingBar);


            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, ProductDetails.class);
                    i.putExtra("name", name.getText().toString());
                    i.putExtra("img", arrData.get(getAdapterPosition()).getImage());
                    i.putExtra("product_id", arrData.get(index).getId().toString());
                    notifyDataSetChanged();
                    mContext.startActivity(i);
                }
            });

        }


        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }


    }

}
class LoadingViewHolder extends RecyclerView.ViewHolder
{

    public ProgressBar progressBar;

    public LoadingViewHolder(View itemView) {
        super(itemView);
        progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);
    }
}
