package com.tsolution.ecommerce.view.ViewHienThiDanhSachSanPhamTrongGioHang;

import android.content.*;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.*;
import android.support.v4.view.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.*;

import com.tsolution.ecommerce.*;
import com.tsolution.ecommerce.adapter.*;
import com.tsolution.ecommerce.view.Order.Presenter.*;
import com.tsolution.ecommerce.view.Order.SanPhamDTO.*;
import com.tsolution.ecommerce.view.Order.ThanhToan.ViewThanhToan.*;
import com.tsolution.ecommerce.view.PresenterGioHang.*;

import java.util.List;


public class CartActivity extends AppCompatActivity implements ViewGioHang, View.OnClickListener {
//    @BindView(R.id.rcOrder)
////    RecyclerView rcOrder;
//    @BindView(R.id.toolbar)
    Toolbar toolbar;
////    private ListOrderAdapter listOrderAdapter;
////    private OrderDao orderDao;
////    List<Order> arrData = new ArrayList<>();
    Button btnThanhToan, btnTinhKM, btnThanhToanTrucTiep;
    TextView txtEdit;


    RecyclerView recyclerView;
    PresenterLogicGioHang presenterLogicGioHang;

    BottomSheetBehavior sheetBehavior;

    LinearLayout layoutBottomSheet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_giohang);

        recyclerView = findViewById(R.id.recyclerGioHang);
        toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

//        btnMuaNgay = findViewById(R.id.btnMuaNgay);
//        btnMuaNgay.setOnClickListener(this);
        presenterLogicGioHang = new PresenterLogicGioHang(this);
        presenterLogicGioHang.LayDanhSachSanPhamTrongGioHang(this);

        btnTinhKM = findViewById(R.id.btnTinhKM);
        btnTinhKM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
//                showBottomSheetDialog();
                Intent intent = new Intent(CartActivity.this, ThanhToanActivity.class);
                startActivity(intent);
            }
        });
//        btnThanhToanTrucTiep = findViewById(R.id.btnThanhToanTrucTiep);
//        btnThanhToanTrucTiep.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View v) {
//                Intent intent = new Intent(CartActivity.this, ThanhToanActivity.class);
//                startActivity(intent);
//            }
//        });
        layoutBottomSheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

    }

    public void showBottomSheetDialog() {
        View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_dialog_khuyen_mai, null);
        btnThanhToan = view.findViewById(R.id.btnThanhToan);
        btnThanhToan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(CartActivity.this, ThanhToanActivity.class);
                startActivity(intent);
            }
        });
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void HienThiDanhSachSanPhamTrongGioHang( List<SanPham> sanPhamList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        AdapterGioHang adapterGioHang = new AdapterGioHang(this,sanPhamList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterGioHang);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
////        MenuInflater menuInflater = getMenuInflater();
////        menuInflater.inflate(R.menu.edit,menu);
////
////        return true;
        getMenuInflater().inflate(R.menu.edit, menu);
        MenuItem iGioHang =menu.findItem(R.id.actin_edit);
        View giaoDienCustomGioHang = MenuItemCompat.getActionView(iGioHang);
        txtEdit =(TextView) giaoDienCustomGioHang.findViewById(R.id.txtEdit);
        txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (txtEdit.getText().toString().equals("Hoàn Thành"))
                {
                    txtEdit.setText("Sửa");
                }else if (txtEdit.getText().toString().equals("Sửa")){
                    txtEdit.setText("Hoàn Thành");
                }

            }
        });
////        PresenterLogicChiTietSanPham presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();
////        txtGioHang.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
////        giaoDienCustomGioHang.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                Intent iGioHang = new Intent(MainActivity.this, CartActivity.class);
////                startActivity(iGioHang);
////            }
////        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int res_id = item.getItemId();
        if (res_id == R.id.actin_edit);
        {
            txtEdit.setText("Hoàn Thành");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(final View v) {

    }
}
