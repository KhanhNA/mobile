package com.tsolution.ecommerce.view.Order.Presenter;

import android.content.Context;

import com.tsolution.ecommerce.view.Order.IPresenterChiTietSanPham;
import com.tsolution.ecommerce.view.Order.ModelGioHang;
import com.tsolution.ecommerce.view.Order.SanPhamDTO.SanPham;
import com.tsolution.ecommerce.view.Order.View.ViewChiTietSanPham;

import java.util.*;

public class PresenterLogicChiTietSanPham implements IPresenterChiTietSanPham {
    ModelGioHang modelGioHang;
    ViewChiTietSanPham viewChiTietSanPham;

    public PresenterLogicChiTietSanPham() {
        modelGioHang = new ModelGioHang();
    }

    public PresenterLogicChiTietSanPham(ViewChiTietSanPham viewChiTietSanPham) {
        this.viewChiTietSanPham = viewChiTietSanPham;
        modelGioHang = new ModelGioHang();
    }

    @Override
    public void ThemGioHang(SanPham sanPham, Context context) {
        modelGioHang.MoKetNoiSQL(context);
        boolean kiemtra = modelGioHang.ThemGioHang(sanPham);
        if (kiemtra) {
            viewChiTietSanPham.ThemGioHangThanhCong();
        } else {
            viewChiTietSanPham.ThemGiohangThatBai();
        }
    }

    public int DemSanPhamCoTrongGioHang(Context context){
        modelGioHang.MoKetNoiSQL(context);
        List<SanPham> sanPhamList =modelGioHang.LayDanhSachSanPhamTrongGioHang();
        int dem =sanPhamList.size();
        return dem;
    }
}
