package com.tsolution.ecommerce.adapter;



import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter  extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>  {
    private List<Notification> listData;
    private Context c;

    public NotificationAdapter(List<Notification> listData, Context c) {
        this.listData = listData;
        this.c = c;
    }
    public NotificationAdapter(){

    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.ViewHolder viewHolder, int i) {

        viewHolder.notification_title.setText(listData.get(i).getTitle());
        viewHolder.description.setText(listData.get(i).getDescription());
        Picasso.with(c).load(listData.get(i).getUrlImg()).into(viewHolder.img);

        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

                Toast.makeText(view.getContext(), "show description notification", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return 20;
    }




    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public ImageView img;
        public TextView notification_title;
        public TextView description;
        public ItemClickListener itemClickListener;

        public ViewHolder(final View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.notification_img);
            notification_title = itemView.findViewById(R.id.txt_vp_item_list);
            description = itemView.findViewById(R.id.txt_description_notification);
            itemView.setOnClickListener(this);

        }

        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition());
            return true;
        }
    }
    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
