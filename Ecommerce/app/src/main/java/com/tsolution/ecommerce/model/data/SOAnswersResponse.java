package com.tsolution.ecommerce.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tsolution.ecommerce.model.Product;

import java.util.List;

public class SOAnswersResponse {

    @SerializedName("content")
    @Expose
    private List<Product> items = null;


    public List<Product> getItems() {
        return items;
    }

    public void setItems(List<Product> items) {
        this.items = items;
    }


}
