package com.tsolution.ecommerce.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.SubProductAdapter;
import com.tsolution.ecommerce.model.Product;
import com.tsolution.ecommerce.model.data.SOAnswersResponse;
import com.tsolution.ecommerce.model.data.SOService;
import com.tsolution.ecommerce.model.utils.ApiUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubProductFragment extends Fragment {
    private SOService mService;
    @BindView(R.id.rcSubProduct)
    RecyclerView rcSubProduct;
    private View rootView;
    private SubProductAdapter subProductAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_sub_product_fragment, container, false);
        return rootView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, rootView);
        mService = ApiUtils.getSOService();
        subProductAdapter = new SubProductAdapter(getContext(), new ArrayList<Product>(0));
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rcSubProduct.setLayoutManager(mLayoutManager);
        rcSubProduct.setItemAnimator(new DefaultItemAnimator());
        rcSubProduct.setAdapter(subProductAdapter);
        loadAnswers();
    }

    public void loadAnswers() {

        mService.getProducts().enqueue(new Callback<SOAnswersResponse>() {
            @Override
            public void onResponse(Call<SOAnswersResponse> call, Response<SOAnswersResponse> response) {
                if (response.isSuccessful()) {
                    subProductAdapter.updateProducts(response.body().getItems());
                    Log.e("SubProducFragment", "posts loaded from API");

                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                    Log.e("SubProducFragment", "posts load false from API");
                }
            }

            @Override
            public void onFailure(Call<SOAnswersResponse> call, Throwable t) {
                Log.e("Failure", "error loading from API:" + t);
            }
        });

    }
}
