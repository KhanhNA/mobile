package com.tsolution.ecommerce.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;


@Entity(nameInDb = "Data")
public class Data {
    @Id
    private long id;
    private String type;

    @Generated(hash = 2106500334)
    public Data(long id, String type) {
        this.id = id;
        this.type = type;
    }

    @Generated(hash = 2135787902)
    public Data() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
