package com.tsolution.ecommerce.view.Order.ThanhToan.ViewThanhToan;

import android.content.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;

import com.tsolution.ecommerce.*;
import com.tsolution.ecommerce.view.Order.SanPhamDTO.*;
import com.tsolution.ecommerce.view.Order.View.*;

import java.util.*;

public class ThanhToanActivity extends AppCompatActivity {
    Spinner spPhuongThucVanChuyen,spChonKho;
    TextView diaChiNhanHang;
    LinearLayout lnThanhToan;
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_thanh_toan_new);
        spChonKho = findViewById(R.id.spChonKho);
        spPhuongThucVanChuyen = findViewById(R.id.spPhuongThucVanChuyen);
        diaChiNhanHang = findViewById(R.id.diaChiNhanHang);
        diaChiNhanHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent i = new Intent(ThanhToanActivity.this, LocationActivity.class);
                startActivity(i);
            }
        });
        lnThanhToan=findViewById(R.id.lnThanhToan);
        lnThanhToan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(ThanhToanActivity.this,PhuongThucThanhToanActivity.class);
                startActivity(intent);
            }
        });
        //spChonKho
        List<String> list = new ArrayList<>();
        list.add("Kho DC1");
        list.add("Kho DC2");
        list.add("Kho DC3");
        list.add("Kho DC4");
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,list);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spChonKho.setAdapter(adapter);
        spChonKho.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //
        List<String> list2 = new ArrayList<>();
        list2.add("Lấy hàng trực tiếp");
        list2.add("Ship");
        ArrayAdapter<String> adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item,list2);
        adapter2.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spPhuongThucVanChuyen.setAdapter(adapter2);
        spPhuongThucVanChuyen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
