package com.tsolution.ecommerce;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.*;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.tsolution.ecommerce.adapter.MyPagerAdapter;
import com.tsolution.ecommerce.fragment.NotificationFragment;
import com.tsolution.ecommerce.view.ViewHienThiDanhSachSanPhamTrongGioHang.CartActivity;
import com.tsolution.ecommerce.view.Order.Presenter.*;
import com.tsolution.ecommerce.view.ProductDetails;
import com.tsolution.ecommerce.view.chat.ChatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import devlight.io.library.ntb.NavigationTabBar;
import retrofit2.http.*;

public class MainActivity extends AppCompatActivity implements NotificationFragment.OnFragmentInteractionListener, View.OnClickListener {
    @BindView(R.id.main_layout)
    ViewPager viewPager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.search)
    EditText editText;
    @BindView(R.id.imgCardTest)
    ImageView imgCartTest;
    TextView txtGioHang;
    boolean onPause = false;

    //@BindView(R.id.toolbar)
    //Toolbar toolbar;
   // @BindView(R.id.search)
    //EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
//        ImageView imgCartTest = findViewById(R.id.imgCardTest);
        imgCartTest.setOnClickListener(this);
        //setSupportActionBar(toolbar);
        initUI();

    }

    private void initViewPager() {
        // View Pager
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.arr_title));
        viewPager.setAdapter(myPagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
        viewPager.setCurrentItem(0);
    }

    private void initUI() {
        initViewPager();



        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_first),
                        Color.parseColor(getResources().getString(R.color.main_color)))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_sixth))
                        .title(getResources().getString(R.string.menu_product))
                        .build()
        );
        models.get(0).hideBadge();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_second),
                        Color.parseColor(getResources().getString(R.color.main_color)))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title(getResources().getString(R.string.menu_management_agent))
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_notifications_active_black_24dp),
                        Color.parseColor(getResources().getString(R.color.main_color)))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title(getResources().getString(R.string.menu_notification))
                        .badgeTitle("50")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_third),
                        Color.parseColor(getResources().getString(R.color.main_color)))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_seventh))
                        .title(getResources().getString(R.string.MENU_ME))
                        .badgeTitle("state")
                        .build()
        );




        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 0);

        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {
            }
        });
        models.get(0).hideBadge();
        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem iGioHang =menu.findItem(R.id.action_cart);
        View giaoDienCustomGioHang = MenuItemCompat.getActionView(iGioHang);
        txtGioHang =(TextView) giaoDienCustomGioHang.findViewById(R.id.txtSoLuongSanPhamGioHang);
        PresenterLogicChiTietSanPham presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();
        txtGioHang.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
        giaoDienCustomGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iGioHang = new Intent(MainActivity.this, CartActivity.class);
                startActivity(iGioHang);
            }
        });
        return true;
//

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_chat:
                Intent i = new Intent(MainActivity.this, ChatActivity.class);
                startActivity(i);
                break;
            case R.id.action_cart:
                startActivity(new Intent(MainActivity.this, CartActivity.class));
                break;


        }
        return super.onOptionsItemSelected(item);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(onPause){
            PresenterLogicChiTietSanPham presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();
            txtGioHang.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        onPause = true;//khi chuyen trang thi nhay vao onPause

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public void onClick(View v) {
        if (v == imgCartTest){
            Intent i = new Intent(MainActivity.this, ProductDetails.class);
            startActivity(i);
        }
    }
}
