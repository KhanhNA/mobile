package com.tsolution.ecommerce.view.chat;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.tsolution.ecommerce.MainActivity;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ChatAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatActivity extends AppCompatActivity {
    private ArrayList<Integer> icons;
    private ArrayList<String> titles;
    private ArrayList<String> description;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    RecyclerView recyclerView;

    public void generateData(){
        icons = new ArrayList<>();
        icons.add(R.drawable.vietnam);
        icons.add(R.drawable.phone);
        icons.add(R.drawable.england);
        icons.add(R.drawable.earth);

        titles = new ArrayList<>();
        titles.add("admin");
        titles.add("Văn Biên");
        titles.add("Trọng Hải");
        titles.add("Hoàng Dự");

        description = new ArrayList<>();
        description.add("xong báo cáo chưa cu");
        description.add("vẫn phải làm lại!");
        description.add("báo cáo gì giờ này nữa!");
        description.add("mặt hàng này còn không ?");

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        generateData();
        recyclerView = findViewById(R.id.recycler_view_chat);
        GridLayoutManager grid = new GridLayoutManager(ChatActivity.this, 1);
        grid.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(grid);
        recyclerView.setAdapter(new ChatAdapter(icons, titles, description, ChatActivity.this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:break;
        }

        return super.onOptionsItemSelected(item);
    }

}
