package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Product;
import com.tsolution.ecommerce.view.ProductDetails;

import java.util.ArrayList;

public class ListProductAdapter extends RecyclerView.Adapter<ListProductAdapter.CustomViewHolder> {
    private ArrayList<Product> arrData;
    private Context mContext;

    public ListProductAdapter(Context mContext, ArrayList<Product> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_promotion, parent, false);

        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        Product item  = arrData.get(position);
        holder.name.setText(item.getDescription().getTitle());
        holder.price.setText("" + item.getPrice());
        holder.setIndex(position);
        Picasso.with(mContext).load(arrData.get(position).getImage()).into(holder.image);


    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView name, price;
        public ImageView image;
        public LinearLayout layoutRoot;
        private int index;
        /**
         * Constructor to initialize the Views
         *
         * @param itemView
         */
        public CustomViewHolder(final View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.txtProductNameSale);
            price =  itemView.findViewById(R.id.pizzaPrice);
            image =  itemView.findViewById(R.id.imgProductSale);
            layoutRoot = itemView.findViewById(R.id.layoutRoot);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, ProductDetails.class);
                    i.putExtra("name", name.getText().toString());
                    i.putExtra("img", arrData.get(getAdapterPosition()).getImage());
                    i.putExtra("product", arrData.get(index));

                    mContext.startActivity(i);
                }
            });
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }
}
