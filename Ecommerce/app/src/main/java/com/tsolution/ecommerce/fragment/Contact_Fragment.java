package com.tsolution.ecommerce.fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ContactAdapter;
import com.tsolution.ecommerce.model.Contact;
import com.tsolution.ecommerce.utils.GetContact;
import com.tsolution.ecommerce.view.chat.ChatMessageActivity;


import java.util.ArrayList;
import java.util.List;

import static android.support.v4.content.ContextCompat.checkSelfPermission;

public class Contact_Fragment extends Fragment implements ContactAdapter.ItemClickListener {
    List<Contact> list = new ArrayList<>();
    ArrayList<String> img;
    GetContact resource_contact;
    ContactAdapter adapter;
    ImageView imgDimisSelect;
    RecyclerView recyclerView;
    LinearLayout checkContact;
    CheckBox chkContact, chkCheckAll;
    SearchView search;
    CardView dialogContact;


    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private List<Contact> contacts;

    public Contact_Fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imgDimisSelect = view.findViewById(R.id.imgDimisSelect);
        resource_contact = new GetContact(getContext());
        recyclerView = view.findViewById(R.id.contacts_list);
        contacts = getContactNames();
        adapter = new ContactAdapter(getActivity(), contacts);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

        checkContact = getView().findViewById(R.id.checkContact);
        chkCheckAll = getView().findViewById(R.id.chkListContact);
        dialogContact = getView().findViewById(R.id.dialogContact);


        adapter.setItemClickListener(this);
        
        imgDimisSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setShow(false);
                adapter.notifyDataSetChanged();
                checkContact.setVisibility(View.GONE);
                dialogContact.setVisibility(View.GONE);
            }
        });
        chkCheckAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
               @Override
               public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                       for(int i = 0; i < contacts.size(); i++){
                           contacts.get(i).setStatus(isChecked ? 1 : 0);
                       }
                       adapter.notifyDataSetChanged();
               }
           }
        );

    }
    private List<Contact> getContactNames() {
        ArrayList<Contact> temp = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            // Get the ContentResolver
            ContentResolver cr = getContext().getContentResolver();
            // Get the Cursor of all the contacts
            Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            // Move the cursor to first. Also check whether the cursor is empty or not.
            if (cursor.moveToFirst()) {
                // Iterate through the cursor
                do {
                    // Get the contacts name
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String img = "https://cdn.pixabay.com/photo/2018/08/14/13/23/ocean-3605547__340.jpg";

                    String phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DISPLAY_NAME));

                    temp.add(new Contact(img, name, 100, phone, email, 1));
                } while (cursor.moveToNext());
            }
            // Close the curosor
            cursor.close();
        }
        return temp;
    }

    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);

        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            ContactAdapter adapter = new ContactAdapter(getActivity(), contacts);
            recyclerView.setAdapter(adapter);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(getContext(), "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
            if(isLongClick)
            {
                adapter.setShow(true);
                adapter.notifyDataSetChanged();
                checkContact.setVisibility(View.VISIBLE);
                dialogContact.setVisibility(View.VISIBLE);
            }
            else
            {

                final Contact contact = contacts.get(position);
                Intent smsIntent = new Intent(getContext(), ChatMessageActivity.class);
                smsIntent.putExtra("contact", contact);
                smsIntent.putExtra("title_chat", contact.getContact_name());
                smsIntent.putExtra("sms_body","Cài đặt app để được hưởng nhiều ưu đãi. Link: ......");
                getContext().startActivity(smsIntent);
            }
    }
}
