package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.listener.OnClickOrder;
import com.tsolution.ecommerce.model.UserHistory;

import java.util.List;

public class UserHistoryAdapter extends RecyclerView.Adapter<UserHistoryAdapter.MyViewHolder> {
    private List<UserHistory> arrData;
    private Context mContext;
    private OnClickOrder onClickOrder;
    public void setOnClickOrder(OnClickOrder onClick)
    {
        this.onClickOrder = onClick;
    }
    public UserHistoryAdapter(Context mContext, List<UserHistory> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_user_history_transfer, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        if (arrData != null && arrData.size() > 0) {
            UserHistory userHistory = arrData.get(i);

            Picasso.with(mContext).load(userHistory.getOrder().getUrlProduct()).into(holder.imgProduct);
            holder.txtNameProduct.setText(userHistory.getOrder().getProductName());
            holder.txtQuantity.setText("x "+ userHistory.getOrder().getQuantity());
            holder.txtPrice.setText("đ " + (userHistory.getOrder().getQuantity() * userHistory.getOrder().getPrice()));
            holder.txtMinusPoint.setText("$" + userHistory.getNimusPoit());
            holder.txtDescription.setText(userHistory.getDescription());

        }

    }


    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNameProduct, txtQuantity, txtPrice, txtMinusPoint, txtDate, txtDescription;
        public ImageView imgProduct;

        public MyViewHolder(View view) {
            super(view);
            txtNameProduct = view.findViewById(R.id.txtNameProduct);
            txtQuantity = view.findViewById(R.id.txtQuantity);
            txtPrice = view.findViewById(R.id.txtPrice);
            imgProduct = view.findViewById(R.id.imgProduct);
            txtMinusPoint = view.findViewById(R.id.txtMinusPoint);
            txtDate = view.findViewById(R.id.txtDate);
            txtDescription = view.findViewById(R.id.txtDescription);
            imgProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onClickOrder!=null)
                    {
                        onClickOrder.onClickOrder();
                    }
                }
            });
        }
    }
}
