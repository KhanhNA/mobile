package com.tsolution.ecommerce.view.PresenterGioHang;

import android.content.*;

import com.tsolution.ecommerce.view.Order.*;
import com.tsolution.ecommerce.view.Order.SanPhamDTO.*;
import com.tsolution.ecommerce.view.ViewHienThiDanhSachSanPhamTrongGioHang.*;

import java.util.*;

public class PresenterLogicGioHang implements IPresenterGioHang {
    ModelGioHang modelGioHang;
    ViewGioHang viewGioHang;

    public PresenterLogicGioHang(ViewGioHang viewGioHang){
        modelGioHang = new ModelGioHang();
        this.viewGioHang = viewGioHang;
    }
    @Override
    public void LayDanhSachSanPhamTrongGioHang(Context context) {
        modelGioHang.MoKetNoiSQL(context);
        List<SanPham> sanPhamList = modelGioHang.LayDanhSachSanPhamTrongGioHang();
        if(sanPhamList.size() > 0){
            viewGioHang.HienThiDanhSachSanPhamTrongGioHang(sanPhamList);
        }
    }
}
