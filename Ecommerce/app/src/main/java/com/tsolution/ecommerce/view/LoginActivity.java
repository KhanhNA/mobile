package com.tsolution.ecommerce.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.tsolution.ecommerce.Constant.IntentContants;
import com.tsolution.ecommerce.MainActivity;
import com.tsolution.ecommerce.R;

import java.util.Locale;

public class LoginActivity extends AppCompatActivity {

    public static String filename = "user_info";

    //--------------------------------
    CheckBox checkBox;
    EditText txtUsername;
    EditText txtPassword;
    TextView btnForgetPass;
    TextView txtLoginFail;
    Button btnLogin;


    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        setKeyboardVisibilityListener();

        //Khởi tạo controll
        checkBox = findViewById(R.id.chkSave);
        txtLoginFail = findViewById(R.id.txtLoginFail);
        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        btnForgetPass = findViewById(R.id.btnForgetPass);
        btnLogin = findViewById(R.id.btnLogin);



        btnForgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgetPassword.class);
                i.putExtra(IntentContants.INTENT_USER_NAME, txtUsername.getText().toString());
                startActivity(i);
            }
        });


    }


    private void setKeyboardVisibilityListener() {
        final View parentView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
        parentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private boolean alreadyOpen;
            private final int defaultKeyboardHeightDP = 100;
            private final int EstimatedKeyboardDP = defaultKeyboardHeightDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);
            private final Rect rect = new Rect();

            @Override
            public void onGlobalLayout() {
                int estimatedKeyboardHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, parentView.getResources().getDisplayMetrics());
                parentView.getWindowVisibleDisplayFrame(rect);
                int heightDiff = parentView.getRootView().getHeight() - (rect.bottom - rect.top);
                boolean isShown = heightDiff >= estimatedKeyboardHeight;
                // Gets linearlayout
                LinearLayout layout = findViewById(R.id.LnLLogo);
                LinearLayout lnlButton = findViewById(R.id.lnlButton);
                // Gets the layout params that will allow you to resize the layout
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
                LinearLayout.LayoutParams button_params = (LinearLayout.LayoutParams) lnlButton.getLayoutParams();
                // Changes the height and width to the specified *pixels*

                if (isShown == alreadyOpen) {
                    params.weight = 3;
                    button_params.weight = 0;
                    layout.setLayoutParams(params);
                    return;
                }else{
                    params.weight = 1;
                    button_params.weight = 0.5f;
                    layout.setLayoutParams(params);
                    lnlButton.setLayoutParams(button_params);
                    return;
                }

            }
        });
    }


    public void login(View view) {
        Log.e("username", String.valueOf(txtUsername.getText()));
        Log.e("password", String.valueOf(txtPassword.getText()));
        if("admin".equals(txtUsername.getText().toString()) && "123456a@A".equals(txtPassword.getText().toString())){
            Intent i = new Intent(LoginActivity.this, MainActivity.class);

            SharedPreferences preferences = getSharedPreferences(filename,MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("username", txtUsername.getText().toString());
            if(checkBox.isChecked()) {
                editor.putString("password", txtPassword.getText().toString());
                editor.putBoolean("chkSave", true);
            }else{
                editor.putBoolean("chkSave", false);
                editor.putString("password", "");
            }
            //xác nhận lưu trữ
            editor.commit();


            i.putExtra("name", txtUsername.getText().toString());
            startActivity(i);
        }
        else {
            txtPassword.setText("");
            txtLoginFail.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        SharedPreferences preferences = getSharedPreferences(filename,MODE_PRIVATE);
        String lang = preferences.getString(IntentContants.INTENT_LANGUAGE, "");

        txtUsername.setText(preferences.getString("username", ""));
        txtPassword.setText(preferences.getString("password", ""));
        checkBox.setChecked(true);


    }


}
