package com.tsolution.ecommerce.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.NotificationAdapter;
import com.tsolution.ecommerce.model.Notification;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import devlight.io.library.ntb.NavigationTabBar;


public class NotificationFragment extends Fragment{
    private FragmentManager fm;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    List<Notification> list = new ArrayList<>();
    ArrayList<String> img;
    private OnFragmentInteractionListener mListener;

    public NotificationFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        genData();
        initUI();

    }

    public void getImg(){
        img = new ArrayList<>();
        img.add("https://cf.shopee.vn/file/07a993c25b004acb8ca4bb68992fcc6c_tn");
        img.add("https://cf.shopee.vn/file/cf8b21e16959cca617e509d0e22c6d9c_tn");
        img.add("https://cf.shopee.vn/file/07a993c25b004acb8ca4bb68992fcc6c_tn");
        img.add("https://cf.shopee.vn/file/2eb9f2c993babb863facd9156bdae34d_tn");
        img.add("https://cf.shopee.vn/file/3c00258d6b39e4116e79264e1d543027_tn");
        img.add("https://cf.shopee.vn/file/209e7a1d6c5f279a90066c4bc6eb31f0_tn");
        img.add("https://cf.shopee.vn/file/c58b5ca95f32416d4b3f6b2188f1ed6b_tn");
        img.add("https://cf.shopee.vn/file/de50ba8b024d839e3cc4d746d5852e89_tn");
        img.add("https://cf.shopee.vn/file/3bda99e21aef90e300df0a2ced51a189_tn");
        img.add("https://cf.shopee.vn/file/ec95c51882de3b68883b52d9ae8e5cd1_tn");
        img.add("https://cf.shopee.vn/file/07a993c25b004acb8ca4bb68992fcc6c_tn");
        img.add("https://cf.shopee.vn/file/07a993c25b004acb8ca4bb68992fcc6c_tn");
        img.add("https://cf.shopee.vn/file/2eb9f2c993babb863facd9156bdae34d_tn");
        img.add("https://cf.shopee.vn/file/3c00258d6b39e4116e79264e1d543027_tn");
        img.add("https://cf.shopee.vn/file/209e7a1d6c5f279a90066c4bc6eb31f0_tn");
        img.add("https://cf.shopee.vn/file/c58b5ca95f32416d4b3f6b2188f1ed6b_tn");
        img.add("https://cf.shopee.vn/file/cf8b21e16959cca617e509d0e22c6d9c_tn");
        img.add("https://cf.shopee.vn/file/de50ba8b024d839e3cc4d746d5852e89_tn");
        img.add("https://cf.shopee.vn/file/3bda99e21aef90e300df0a2ced51a189_tn");
        img.add("https://cf.shopee.vn/file/ec95c51882de3b68883b52d9ae8e5cd1_tn");
    }
    private void genData() {
        list = new ArrayList<>();
        getImg();
        for(int i = 0; i <img.size(); i++){
            list.add(new Notification(img.get(i), "Thông báo " + i,
                    "Một đơn hàng đang được giao đến dịa chỉ yêu cầu, Quý khách vui lòng sắp xếp thời gian đón hàng"));
        }
    }

    private void initUI() {
        Log.e("init", "initUI");

        final ViewPager viewPager = getView().findViewById(R.id.vp_horizontal_ntb);
        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 4;
            }

            @Override
            public boolean isViewFromObject(final View view, final Object object) {
                return view.equals(object);
            }
            @Override
            public void destroyItem(final View container, final int position, final Object object) {
                ((ViewPager) container).removeView((View) object);
            }


            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {
                final View view = LayoutInflater.from(
                        getContext()).inflate(R.layout.notification_list, null, false);

                final RecyclerView recyclerView =  view.findViewById(R.id.rv);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(
                                getContext(), LinearLayoutManager.VERTICAL, false
                        )
                );
                recyclerView.setAdapter(new NotificationAdapter(list, getContext()));

                container.addView(view);
                return view;
            }
        });

        final String[] colors = getResources().getStringArray(R.array.default_preview);

        final NavigationTabBar navigationTabBar = getView().findViewById(R.id.ntb_horizontal);

        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.notification),
                        Color.parseColor(colors[0]))
                        .title(getResources().getString(R.string.menu_order_list))
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.cart),
                        Color.parseColor(colors[1]))
                        .title(getResources().getString(R.string.menu_product))
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_menu_share),
                        Color.parseColor(colors[2]))
                        .title(getResources().getString(R.string.update))
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.england),
                        Color.parseColor(colors[4]))
                        .title(getResources().getString(R.string.sys_message))
                        .build()
        );
        navigationTabBar.setIsSwiped(true);
        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 0);

        navigationTabBar.post(new Runnable() {
            @Override
            public void run() {
                final View viewPager = getView().findViewById(R.id.vp_horizontal_ntb);
                ((ViewGroup.MarginLayoutParams) viewPager.getLayoutParams()).topMargin =
                        (int) -navigationTabBar.getBadgeMargin();
                viewPager.requestLayout();
            }
        });
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
//        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
//            @Override
//            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {
//
//            }
//
//            @Override
//            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
//                model.hideBadge();
//            }
//        });

        getView().findViewById(R.id.mask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final String title = String.valueOf(new Random().nextInt(15));
                            if (!model.isBadgeShowed()) {
                                model.setBadgeTitle(title);
                                model.showBadge();
                            } else model.updateBadgeTitle(title);
                        }
                    }, i *10);
                }
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
