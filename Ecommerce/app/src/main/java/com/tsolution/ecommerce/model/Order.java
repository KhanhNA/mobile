package com.tsolution.ecommerce.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "tblOrder")
public class Order {
    @Id(autoincrement = true)
    private Long  id;
    private Long productId;
    private Integer quantity;
    private String createDate;
    private Integer status;
    private String urlProduct;
    private String productName;
    private Long price;
    private String providerName;
    private String urlProviderImg;

    public Order(String urlProduct, String productName, Long price, String providerName, String urlProviderImg,int quantity, int status){
        this.urlProduct = urlProduct;
        this.productName = productName;
        this.price = price;
        this.providerName = providerName;
        this.urlProviderImg = urlProviderImg;
        this.quantity = quantity;
        this.status = status;

    }

    @Generated(hash = 1371921098)
    public Order(Long id, Long productId, Integer quantity, String createDate,
            Integer status, String urlProduct, String productName, Long price,
            String providerName, String urlProviderImg) {
        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.createDate = createDate;
        this.status = status;
        this.urlProduct = urlProduct;
        this.productName = productName;
        this.price = price;
        this.providerName = providerName;
        this.urlProviderImg = urlProviderImg;
    }
    @Generated(hash = 1105174599)
    public Order() {

    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getProductId() {
        return this.productId;
    }
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    public Integer getQuantity() {
        return this.quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public String getCreateDate() {
        return this.createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public Integer getStatus() {
        return this.status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getUrlProduct() {
        return this.urlProduct;
    }
    public void setUrlProduct(String urlProduct) {
        this.urlProduct = urlProduct;
    }
    public Long getPrice() {
        return this.price;
    }
    public void setPrice(Long price) {
        this.price = price;
    }
    public String getProductName() {
        return this.productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProviderName() {
        return this.providerName;
    }
    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
    public String getUrlProviderImg() {
        return this.urlProviderImg;
    }
    public void setUrlProviderImg(String urlProviderImg) {
        this.urlProviderImg = urlProviderImg;
    }

   
}
