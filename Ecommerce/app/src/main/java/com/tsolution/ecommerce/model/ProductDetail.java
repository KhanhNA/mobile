package com.tsolution.ecommerce.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductDetail implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("price")
    @Expose
    private Double price;

    @SerializedName("description")
    @Expose
    private Description description;

    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    @SerializedName("rating")
    @Expose
    private Float rating;

    @SerializedName("imgaes")
    @Expose
    private String[] imgaes;

    @SerializedName("ratingCount")
    @Expose
    private int ratingCount;

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }
    //    @SerializedName("originalPrice")
//    @Expose
//    private Double originalPrice;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String[] getImgaes() {
        return imgaes;
    }

    public void setImgaes(String[] imgaes) {
        this.imgaes = imgaes;
    }

    //    public Double getOriginalPrice() {
//        return originalPrice;
//    }
//
//    public void setOriginalPrice(Double originalPrice) {
//        this.originalPrice = originalPrice;
//    }
}
