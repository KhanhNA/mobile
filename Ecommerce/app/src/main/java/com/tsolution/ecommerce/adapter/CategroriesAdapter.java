package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Category;
import com.tsolution.ecommerce.model.Product;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CategroriesAdapter extends RecyclerView.Adapter<CategroriesAdapter.MyViewHolder> {
    private ArrayList<Category> arrData;
    private Context mContext;

    public CategroriesAdapter(Context mContext, ArrayList<Category> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }


    public void updateCategories(ArrayList<Category> items) {
        arrData = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_cate, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategroriesAdapter.MyViewHolder holder, int i) {
        if (arrData != null && arrData.size() > 0) {
            Category movie = arrData.get(i);
            if (null != movie.getCategoryIcon()) {
                Picasso.with(mContext).load(arrData.get(i).getCategoryIcon()).into(holder.imgCategory);
            }
            holder.nameCategory.setText(movie.getCategoryName());

        }

    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nameCategory;
        public CircleImageView imgCategory;

        public MyViewHolder(View view) {
            super(view);
            imgCategory = view.findViewById(R.id.imgCategory);
            nameCategory = view.findViewById(R.id.txtNameCategory);


        }
    }
}
