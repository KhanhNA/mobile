package com.tsolution.ecommerce.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ListOrderAdapter;
import com.tsolution.ecommerce.fragment.MapViewDialogFragment;
import com.tsolution.ecommerce.listener.OnClickOrder;
import com.tsolution.ecommerce.model.Order;
import com.tsolution.ecommerce.model.OrderDao;

import java.util.ArrayList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartActivity extends AppCompatActivity implements OnClickOrder {
    @BindView(R.id.rcOrder)
    RecyclerView rcOrder;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private ListOrderAdapter listOrderAdapter;
    private OrderDao orderDao;
    List<Order> arrData = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cart_activity);
        ButterKnife.bind(this);
        //
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Giỏ hàng");
        // Load data
        orderDao = AppController.getInstance().getDaoSession().getOrderDao();
        arrData = orderDao.queryBuilder().orderDesc(OrderDao.Properties.Id).build().listLazy();
        listOrderAdapter = new ListOrderAdapter(CartActivity.this, arrData);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcOrder.setLayoutManager(layoutManager);
        rcOrder.setAdapter(listOrderAdapter);
        //
        listOrderAdapter.setOnClickOrder(this);
    }

    @Override
    public void onClickOrder() {
        FragmentManager fm = getSupportFragmentManager();
        MapViewDialogFragment userInfoDialog = new MapViewDialogFragment();
        userInfoDialog.show(fm, null);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
