package com.tsolution.ecommerce.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tsolution.ecommerce.fragment.ListOrderedFragment;


public class ListOrderPapersAdapter extends FragmentPagerAdapter {
    private String[] TITLES;
    private ListOrderedFragment sampleFragment;

    public ListOrderPapersAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        //Show Fragment
        switch (position) {
            case 0:
               sampleFragment = new ListOrderedFragment();
               return sampleFragment;
            case 1:
                sampleFragment = new ListOrderedFragment();
                return sampleFragment;

        }
        return null;
    }
}
