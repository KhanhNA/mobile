package com.tsolution.ecommerce.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.BrandAdapter;
import com.tsolution.ecommerce.adapter.CategoriesPagerAdapter;
import com.tsolution.ecommerce.adapter.CategroriesAdapter;
import com.tsolution.ecommerce.adapter.ListProductAdapter;
import com.tsolution.ecommerce.model.Category;
import com.tsolution.ecommerce.model.Description;
import com.tsolution.ecommerce.model.Product;
import com.tsolution.ecommerce.model.data.SOService;
import com.tsolution.ecommerce.model.utils.ApiUtils;
import com.tsolution.ecommerce.widget.SlidingTabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    private static ProductFragment mIntance;
    private View rootView;
    @BindView(R.id.slider)
    SliderLayout mDemoSlider;
    @BindView(R.id.rcDanhMucSp)
    RecyclerView rcDanhMucSp;
    @BindView(R.id.rcBrand)
    RecyclerView rcBrand;
    @BindView(R.id.rcPromotion)
    RecyclerView rcPromotion;
    @BindView(R.id.sliding_tabs)
    SlidingTabLayout tabs;
    @BindView(R.id.pager)
    ViewPager pager;


    //Service call api
    private SOService mService;

    //adapter
    private CategroriesAdapter categroriesAdapter;
    private BrandAdapter brandAdapter;
    private ListProductAdapter listProductAdapter;

    ArrayList<String> img = new ArrayList<>();
    private ArrayList<Product> arrData = new ArrayList<>();
    ArrayList<String> imgBrand = new ArrayList<>();
    private ArrayList<Product> dataBrand = new ArrayList<>();
    public ProductFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        // Inflate the layout for this fragment

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void getUrlImg(){
//        img.add("http://mamnamngu.com/wp-content/uploads/2018/04/nuoc-mam-chin-su-2-chai-ca-hoi.jpg");
//        img.add("http://cafefcdn.com/thumb_w/650/2017/1447392151985-4107234-1492158270186-crop-1492158331431-1492229100663.jpg");
//        img.add("http://1.bp.blogspot.com/-6C2JX1OLQf0/UJM_mNzkO1I/AAAAAAAABeI/jS57aWW-ZEA/s1600/mi-omachi-an-khong-so-nong.jpg");//
//        img.add("http://cafefcdn.com/thumb_w/650/2017/vietnam-vinacafe-marten-wake-up-instant-coffee-mix-3-in-1-export-quality-17g-18-sticks-1507188765040-crop-1507188769930-1507194079899.jpg");
//        img.add("https://pbs.twimg.com/media/DBv4eOzXcAEl3Ik.jpg");
        img.add("https://cf.shopee.vn/file/ec14dd4fc238e676e43be2a911414d4d_tn");
        img.add("https://cf.shopee.vn/file/74ca517e1fa74dc4d974e5d03c3139de_tn");
        img.add("https://cf.shopee.vn/file/7abfbfee3c4844652b4a8245e473d857_tn");
        img.add("https://cf.shopee.vn/file/6cb7e633f8b63757463b676bd19a50e4_tn");
        img.add("https://cf.shopee.vn/file/3fb459e3449905545701b418e8220334_tn");
        img.add("https://cf.shopee.vn/file/75ea42f9eca124e9cb3cde744c060e4d_tn");
        img.add("https://cf.shopee.vn/file/24b194a695ea59d384768b7b471d563f_tn");
        img.add("https://cf.shopee.vn/file/fa6ada2555e8e51f369718bbc92ccc52_tn");
        img.add("https://cf.shopee.vn/file/c432168ee788f903f1ea024487f2c889_tn");
        img.add("https://cf.shopee.vn/file/b0f78c3136d2d78d49af71dd1c3f38c1_tn");
        img.add("https://cf.shopee.vn/file/b0f78c3136d2d78d49af71dd1c3f38c1_tn");
        img.add("https://cf.shopee.vn/file/36013311815c55d303b0e6c62d6a8139_tn");
        img.add("https://cf.shopee.vn/file/74ca517e1fa74dc4d974e5d03c3139de_tn");
        img.add("https://cf.shopee.vn/file/7abfbfee3c4844652b4a8245e473d857_tn");

        imgBrand.add("https://www.brandsvietnam.com/upload/news/480px/2014/Masan_1392600440.JPG");
        imgBrand.add("https://cf.shopee.vn/file/c91a51e7a67252073789eb99004c5580");
        imgBrand.add("http://nguoivietonhat.com/wp-content/uploads/2018/02/07/co-gi-dac-biet-o-thoi-son-vavachi-thoi-son-dang-khien-hang-loat-sao-viet-phat-sot-tinnuocnhat-info-thumb.jpeg");
        imgBrand.add("https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Dell_logo_2016.svg/1200px-Dell_logo_2016.svg.png");
        imgBrand.add("https://tragop.online/uploads/products/rJLva2B0Z/slider/iphone-x-64g-tra-gop-2.png");
        imgBrand.add("https://cf.shopee.vn/file/81c42e9ab723e53ee7b7b363b036bdc8");
        imgBrand.add("https://cf.shopee.vn/file/76cf62284e4dda72a969e7d5364659e8");
        imgBrand.add("https://cf.shopee.vn/file/01b0d760bdcaca9e473e067ea91f979a");
        imgBrand.add("https://cf.shopee.vn/file/7fbef06de8ae485c147677fc3cd8e59b");
        imgBrand.add("https://cf.shopee.vn/file/436f5a789320e6f95e8daf6e1f3ceff2");
        imgBrand.add("https://cf.shopee.vn/file/f7b875963b3ef8dcc635beeb99b9fc2b");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list_product, container, false);
        ButterKnife.bind(this, rootView);
        mService = ApiUtils.getSOService();

        // DUMP DATA SLIDE
        getUrlImg();
        HashMap<String, String> url_maps = new HashMap<String, String>();
        url_maps.put("Masan", "http://giavichinsu.com/wp-content/uploads/2018/04/bo-nuoc-mam-chin-su-thuong-hang.jpg");
        url_maps.put("chisu", "https://www.masanconsumer.com/images/imgchinsu/banner.jpg");
        url_maps.put("fast food", "http://1.bp.blogspot.com/-e8kQyAb4koM/VNxYL1Ebh3I/AAAAAAAAAEE/KIZlqXWKo44/s1600/banner%2Bnuoc%2Bmam%2Bphu%2Bquoc%2Bhong%2Btuyet.jpg");
        url_maps.put("B'Fast", "https://masanfood-cms-production.s3-ap-southeast-1.amazonaws.com/iblock/623/62326d58273e62c4692f773c9892fd0e.jpg");

        sliderCollection(url_maps);


        //data categories
        categroriesAdapter = new CategroriesAdapter(getActivity(), new ArrayList<Category>());
        GridLayoutManager layoutDanhMuc
                = new GridLayoutManager(getActivity(), 2, GridLayoutManager.HORIZONTAL, false);

        rcDanhMucSp.setLayoutManager(layoutDanhMuc);
        rcDanhMucSp.setAdapter(categroriesAdapter);

        //brand
        brandAdapter = new BrandAdapter(getActivity(), dataBrand);
        GridLayoutManager layoutBrand = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        rcBrand.setLayoutManager(layoutBrand);
        rcBrand.setAdapter(brandAdapter);
        //promotion
        listProductAdapter = new ListProductAdapter(getActivity(), arrData);
        GridLayoutManager layoutPromotion = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);

        rcPromotion.setLayoutManager(layoutPromotion);

        rcPromotion.setAdapter(listProductAdapter);

        //------------------------- get data -------------------
        getCategoryData();





        //------------------------------------------------------


        //TabLayout
        tabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        tabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        tabs.setDistributeEvenly(false);

        // View Pager
        CategoriesPagerAdapter myPagerAdapter = new CategoriesPagerAdapter(getFragmentManager(), getResources().getStringArray(R.array.arr_title));
        pager.setAdapter(myPagerAdapter);
        pager.setOffscreenPageLimit(3);
        pager.setHorizontalScrollBarEnabled(false);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabs.setViewPager(pager);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        pager.setCurrentItem(0);

        return rootView;
    }

    private void getCategoryData() {
        mService.getCategories().enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                if(response.isSuccessful()){
                    categroriesAdapter.updateCategories((ArrayList<Category>) response.body().getItems());
                }else {
                    Log.e("onResponse", "Load fail category");
                }
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                Log.e("onFailure", "Error:" + t);
            }
        });
    }

    // sliderCollection
    private void sliderCollection(HashMap<String, String> url_maps) {
        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(2600);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onStop() {
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
