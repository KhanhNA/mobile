package com.ts.notification.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationCenterDataDTO {
    private Long id;
    private String type;
    private String title;
    private String description;
    private String actionTitle;
    private String imgUrl;
    private String actionClick;
    private String packageName;
    private String notificationDate;
}
