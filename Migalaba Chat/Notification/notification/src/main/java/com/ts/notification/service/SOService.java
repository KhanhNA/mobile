package com.ts.notification.service;


import com.ts.notification.model.NotificationResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SOService {
    @GET("notification/getDiscussion")
    Call<NotificationResponse> getNotification(@Query("roomId") String roomId, @Query("page") Integer page);
}
