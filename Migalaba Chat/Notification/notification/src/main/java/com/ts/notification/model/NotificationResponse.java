package com.ts.notification.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationResponse {
    private List<Notification> messages;
    private Integer total;
    private Integer totalPage;
}
