package com.ts.notification.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.ts.notification.R;
import com.ts.notification.model.MessageType;
import com.ts.notification.model.Notification;
import com.ts.notification.service.AdapterClickListener;
import com.ts.notification.utils.ToastUtils;

public class MainActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NotificationFragment fragment = NotificationFragment.newInstance( "33FCA8XHndH694MdK", new AdapterClickListener() {
            @Override
            public void onItemClick(Notification o) {
                ToastUtils.showToast("click");
            }
        });
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.commonFrag, fragment);
        ft.commit();
    }


}
