package com.ts.notification.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ts.notification.R;
import com.ts.notification.base.NotificationApplication;
import com.ts.notification.model.MessageType;
import com.ts.notification.model.Notification;
import com.ts.notification.model.NotificationResponse;
import com.ts.notification.service.AdapterClickListener;
import com.ts.notification.service.ClientApi;
import com.ts.notification.service.SOService;
import com.ts.notification.utils.NetworkUtils;
import com.ts.notification.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private String roomId = "";
    private boolean loadMore;
    private NotificationAdapter notificationAdapter;
    private SOService soService;
    private AdapterClickListener listener;
    private List<Notification> lstNotification;
    public static String INTENT_ROOM_ID = "INTENT_ROOM_ID";

    private SwipeRefreshLayout swRefresh;
    private int currentPage, totalPage;


    private NotificationFragment(AdapterClickListener listener){
        this.listener = listener;
    }

    public static NotificationFragment newInstance(String roomId, AdapterClickListener listener) {
        NotificationFragment fragment = new NotificationFragment(listener);
        Bundle args = new Bundle();
        args.putString(INTENT_ROOM_ID, roomId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        soService = ClientApi.getClient().create(SOService.class);
        Bundle bundle = getArguments();
        if (bundle != null) {
            roomId = bundle.getString(INTENT_ROOM_ID);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification_list, container, false);

        initView(view);
        getNotification();
        return view;
    }

    private void initView(View view) {
        // Set the adapter
        lstNotification = new ArrayList<>();
        notificationAdapter = new NotificationAdapter(getContext(), lstNotification, listener);
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(notificationAdapter);
        LinearLayoutManager layoutManager = ((LinearLayoutManager)recyclerView.getLayoutManager());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                assert layoutManager != null;
                int lastVisiblePosition = layoutManager.findLastVisibleItemPosition();

                if (lastVisiblePosition == notificationAdapter.getItemCount() - 2) {
                    if (loadMore) {
                        loadMore = false;
                        getMoreNotification();
                    }
                }
            }
        });



        swRefresh = view.findViewById(R.id.swRefresh);
        swRefresh.setRefreshing(true);
        swRefresh.setOnRefreshListener(this);
    }

    private void getNotification() {
        Call<NotificationResponse> call = soService.getNotification(roomId, 0);
        if (NetworkUtils.isNetworkConnected(getContext())) {
            loadMore = false;
            call.enqueue(new Callback<NotificationResponse>() {
                @Override
                public void onResponse(@NonNull Call<NotificationResponse> call, @NonNull Response<NotificationResponse> response) {
                    if (response.body() != null) {
                        currentPage = 0;
                        totalPage = response.body().getTotalPage();
                        lstNotification = response.body().getMessages();
                        notificationAdapter.notifyDataSetChanged(lstNotification);
                        loadMore=true;
                    }
                    swRefresh.setRefreshing(false);
                }

                @Override
                public void onFailure(Call<NotificationResponse> call, Throwable t) {
                    swRefresh.setRefreshing(false);
                    Toast.makeText(getContext(), R.string.fail_to_load_notification, Toast.LENGTH_SHORT).show();
                    call.cancel();
                }
            });
        } else {
            ToastUtils.showToast(R.string.not_connect_server);
        }
    }
    private void getMoreNotification() {
        if(currentPage <= totalPage) {
            currentPage++;
            Call<NotificationResponse> call = soService.getNotification(roomId, currentPage);
            if (NetworkUtils.isNetworkConnected(getContext())) {
                loadMore = false;
                swRefresh.setRefreshing(true);
                call.enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<NotificationResponse> call, @NonNull Response<NotificationResponse> response) {
                        if (response.body() != null) {
                            lstNotification.addAll(response.body().getMessages());
                            notificationAdapter.notifyDataSetChanged(lstNotification);
                            loadMore = true;
                        }
                        swRefresh.setRefreshing(false);
                    }

                    @Override
                    public void onFailure(@NonNull Call<NotificationResponse> call, @NonNull Throwable t) {
                        swRefresh.setRefreshing(false);
                        loadMore = true;
                        currentPage--;
                        Toast.makeText(getContext(), R.string.fail_to_load_notification, Toast.LENGTH_SHORT).show();
                        call.cancel();
                    }
                });
            } else {
                currentPage--;
                ToastUtils.showToast(R.string.not_connect_server);
            }
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


//    private void openAction(Notification o) {
//            if (o.getContent().getPackageName() != null && !"".equals(o.getContent().getPackageName())) {
//            Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(o.getContent().getPackageName());
//            if (launchIntent != null) {
//                Bundle bundle = new Bundle();
//                if (!"".equals(o.getContent().getActionClick())) {
//                    bundle.putString("actionId", "" + o.getContent().getId());
//                    bundle.putString("click_action", o.getContent().getActionClick());
//                }
//                launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                launchIntent.putExtras(bundle);
//                getActivity().startActivity(launchIntent);
//            }
//        } else {
//            Toast.makeText(getActivity(), "No packagename", Toast.LENGTH_LONG).show();
//        }
//
//    }

    @Override
    public void onRefresh() {
        getNotification();
    }
}
