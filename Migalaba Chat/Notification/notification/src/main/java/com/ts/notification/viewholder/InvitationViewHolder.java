package com.ts.notification.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ts.notification.R;
import com.ts.notification.base.NotificationApplication;
import com.ts.notification.model.Notification;

import androidx.recyclerview.widget.RecyclerView;

public class InvitationViewHolder extends RecyclerView.ViewHolder {
    private TextView txtTitle;
    private TextView txtDescription;
    private TextView txtNotificationDate;
    public LinearLayout layoutRoot;

    public InvitationViewHolder(View itemView) {
        super(itemView);
        // Initiate view
        txtTitle = itemView.findViewById(R.id.txtTitle);
        txtDescription = itemView.findViewById(R.id.txtDescription);
        txtNotificationDate = itemView.findViewById(R.id.txtNotificationDate);
        layoutRoot = itemView.findViewById(R.id.layoutRoot);
    }

    public void bindView(Notification notification) {
        txtTitle.setText(notification.getContent().getTitle());
        txtDescription.setText(notification.getContent().getDescription());
        txtNotificationDate.setText(notification.getContent().getNotificationDate());
    }
}
