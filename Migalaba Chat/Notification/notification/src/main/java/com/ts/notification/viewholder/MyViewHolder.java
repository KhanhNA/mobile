package com.ts.notification.viewholder;

import android.view.View;

import com.ts.notification.model.Notification;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class MyViewHolder extends RecyclerView.ViewHolder {
    public Notification data;
    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    void bind(Notification data){
        this.data = data;
        bindViews(data);
    }

    abstract void bindViews(Notification data);
}
