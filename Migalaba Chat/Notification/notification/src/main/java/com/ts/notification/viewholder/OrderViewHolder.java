package com.ts.notification.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ts.notification.R;
import com.ts.notification.base.NotificationApplication;
import com.ts.notification.model.Notification;

import androidx.recyclerview.widget.RecyclerView;

public class OrderViewHolder extends RecyclerView.ViewHolder {
    private TextView txtTitle;
    private TextView txtDescription;
    private TextView txtNotificationDate;
    private ImageView imgProduct;
    public LinearLayout layoutRoot;
    public OrderViewHolder(View itemView) {
        super(itemView);
        // Initiate view
        txtTitle= itemView.findViewById(R.id.txtTitle);
        txtDescription= itemView.findViewById(R.id.txtDescription);
        txtNotificationDate= itemView.findViewById(R.id.txtNotificationDate);
        imgProduct = itemView.findViewById(R.id.imgProduct);
        layoutRoot = itemView.findViewById(R.id.layoutRoot);
    }
    public void bindView(Notification notification){
        txtTitle.setText(notification.getContent().getTitle());
        txtDescription.setText(notification.getContent().getDescription());
        txtNotificationDate.setText(notification.getContent().getNotificationDate());
        Glide.with(itemView.getContext())
                .load(NotificationApplication.BASE_URL
                + "files"
                + notification.getContent().getImgUrl())
                .error(R.drawable.ic_stub)
                .into(imgProduct);
    }
}
