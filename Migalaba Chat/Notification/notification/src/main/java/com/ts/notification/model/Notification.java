package com.ts.notification.model;

import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Notification {
    private String _id;
    private String msg;
    private NotificationCenterDataDTO content;

    public NotificationCenterDataDTO getContent() {
        if(content == null){
            content = new Gson().fromJson(msg, NotificationCenterDataDTO.class);
        }
        return content;
    }
}
