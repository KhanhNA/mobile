package com.ts.notification.viewholder;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.ts.notification.R;
import com.ts.notification.base.NotificationApplication;
import com.ts.notification.model.Notification;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;

public class PromotionViewHolder extends RecyclerView.ViewHolder {
    private TextView txtTitle;
    private TextView txtDescription;
    private TextView txtAction;
    private ImageView imgPromotion;
    public LinearLayout layoutRoot;


    public PromotionViewHolder(View itemView) {
        super(itemView);
        // Initiate view
        txtTitle = itemView.findViewById(R.id.txtTitle);
        txtDescription = itemView.findViewById(R.id.txtDescription);
        txtAction = itemView.findViewById(R.id.txtAction);
        imgPromotion = itemView.findViewById(R.id.imgPromotion);
        layoutRoot = itemView.findViewById(R.id.layoutRoot);
    }

    public void bindView(Notification notification) {
        txtTitle.setText(notification.getContent().getTitle());
        txtTitle.setSelected(true);
        txtDescription.setText(notification.getContent().getDescription());
        txtAction.setText(notification.getContent().getActionTitle());
        txtAction.setSelected(true);
        Glide.with(itemView.getContext())
                .asBitmap()
                .load(NotificationApplication.BASE_URL
                        + "files/"
                        + notification.getContent().getImgUrl())
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        imgPromotion.setImageBitmap(resource);
                        Palette.from(resource).generate(new Palette.PaletteAsyncListener() {
                            @Override
                            public void onGenerated(@Nullable Palette palette) {
                                if (palette != null) {
                                    int darkColor = palette.getDarkMutedColor(itemView.getContext()
                                            .getResources()
                                            .getColor(R.color.colorPrimaryDark));

                                    int lightColor = palette.getMutedColor(itemView.getContext()
                                            .getResources()
                                            .getColor(R.color.colorPrimary));
                                    Drawable background = txtAction.getBackground();
                                    ((GradientDrawable) background)
                                            .setColors(new int[]{darkColor, lightColor});
                                }
                            }
                        });
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }
}
