package com.ts.notification.base;


public class NotificationApplication extends MyApplication {
    private static NotificationApplication mInstance;
    public static String DOMAIN_NAME = "http://dev.nextsolutions.com.vn";
    public static String BASE_URL = DOMAIN_NAME + ":8234/api/v1/";
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
    public static synchronized NotificationApplication getInstance() {
        return mInstance;
    }
}
