package com.ts.notification.service;

import com.ts.notification.model.Notification;

public interface AdapterClickListener {
    void onItemClick(Notification o);
}
