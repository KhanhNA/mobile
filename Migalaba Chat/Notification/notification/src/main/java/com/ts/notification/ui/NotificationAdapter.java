package com.ts.notification.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ts.notification.R;
import com.ts.notification.model.MessageType;
import com.ts.notification.model.Notification;
import com.ts.notification.service.AdapterClickListener;
import com.ts.notification.viewholder.ExampleViewHolder;
import com.ts.notification.viewholder.InvitationViewHolder;
import com.ts.notification.viewholder.OrderViewHolder;
import com.ts.notification.viewholder.PromotionViewHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<Notification> mData;
    private AdapterClickListener listener;

    NotificationAdapter(Context context, List<Notification> data, AdapterClickListener listener) {
        this.context = context;
        this.mData = data;
        this.listener = listener;
    }

    void notifyDataSetChanged(List<Notification> data) {
        this.mData = data;
        notifyDataSetChanged();
    }

    public int getItemViewType(int position) {
        //1- promotion, 2- order, 3- invitationC
        if (mData.get(position).getContent().getType().equals(MessageType.PROMOTION.getValue())) {
            return 1;
        }
        if (mData.get(position).getContent().getType().equals(MessageType.ORDER.getValue())) {
            return 2;
        }
        if (mData.get(position).getContent().getType().equals(MessageType.INVITATION.getValue())) {
            return 3;
        }
        return -1;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //1- promotion, 2- order, 3- invitation
        int layout;
        switch (viewType) {
            case 1:
                layout = R.layout.item_notification_promotion;
                View promotionView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
                return new PromotionViewHolder(promotionView);
            case 2:
                layout = R.layout.item_notification_order;
                View orderView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
                return new OrderViewHolder(orderView);
            case 3:
                layout = R.layout.item_notification_invitation;
                View invitationView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
                return new InvitationViewHolder(invitationView);
        }
        layout = R.layout.item_notification_example;
        View exampleView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new ExampleViewHolder(exampleView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();
        switch (viewType) {
            case 1:
                final Notification notification = mData.get(position);
                ((PromotionViewHolder) holder).bindView(notification);
                ((PromotionViewHolder) holder).layoutRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(notification);
                    }
                });
                break;
            case 2:
                final Notification order = mData.get(position);
                ((OrderViewHolder) holder).bindView(order);
                ((OrderViewHolder) holder).layoutRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(order);
                    }
                });
                break;
            case 3:
                final Notification invitation = mData.get(position);
                ((InvitationViewHolder) holder).bindView(invitation);
                ((InvitationViewHolder) holder).layoutRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(invitation);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mData != null && mData.size() > 0 ? mData.size() : 0;
    }
}

