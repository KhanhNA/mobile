package chat.ts.android.emoji.internal

import chat.ts.android.emoji.Emoji

fun Emoji.isCustom(): Boolean = this.url != null
