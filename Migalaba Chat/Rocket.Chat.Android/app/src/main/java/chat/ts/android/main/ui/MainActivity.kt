package chat.ts.android.main.ui

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import chat.ts.android.R
import chat.ts.android.app.RocketChatApplication
import chat.ts.android.authentication.domain.model.DEEP_LINK_INFO_KEY
import chat.ts.android.authentication.domain.model.DeepLinkInfo
import chat.ts.android.chatrooms.ui.ChatRoomsFragment
import chat.ts.android.chatrooms.ui.TAG_CHAT_ROOMS_FRAGMENT
import chat.ts.android.core.behaviours.AppLanguageView
import chat.ts.android.main.presentation.MainPresenter
import chat.ts.android.server.ui.INTENT_CHAT_ROOM_ID
import chat.ts.android.server.ui.INTENT_USER_NAME
import chat.ts.android.util.extensions.isNotNullNorEmpty
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import java.util.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasActivityInjector,
        HasSupportFragmentInjector, AppLanguageView {
    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var presenter: MainPresenter
    private var deepLinkInfo: DeepLinkInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        deepLinkInfo = intent.getParcelableExtra(DEEP_LINK_INFO_KEY)

        with(presenter) {
            connect()
            getAppLanguage()
            removeOldAccount()
            saveNewAccount()
            registerPushNotificationToken()
            val userName = intent.getStringExtra(INTENT_USER_NAME)
            if(userName.isNotNullNorEmpty()){
                initMainFragment(userName!!, "", deepLinkInfo)
            }else {
                if (intent.getStringExtra(INTENT_CHAT_ROOM_ID).isNotNullNorEmpty()) {
                    clearNotificationsForChatRoom(intent.getStringExtra(INTENT_CHAT_ROOM_ID))
                    initMainFragment("", intent.getStringExtra(INTENT_CHAT_ROOM_ID), deepLinkInfo)
                }else{
                    initMainFragment("", "", deepLinkInfo)
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent?.getStringExtra(INTENT_USER_NAME).isNotNullNorEmpty()) {
            intent?.getStringExtra(INTENT_USER_NAME)?.let { userName ->
                (supportFragmentManager.findFragmentByTag(TAG_MAIN_FRAGMENT) as? MainFragment)
                        ?.goToChatRoom(userName)
            }
        } else {
            intent?.getParcelableExtra<DeepLinkInfo>(DEEP_LINK_INFO_KEY)?.let { deepLinkInfo ->
                (supportFragmentManager.findFragmentByTag(TAG_CHAT_ROOMS_FRAGMENT) as? ChatRoomsFragment)
                        ?.processDeepLink(deepLinkInfo)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        clearAppNotifications()
    }

    override fun activityInjector(): AndroidInjector<Activity> =
            activityDispatchingAndroidInjector

    override fun supportFragmentInjector(): AndroidInjector<Fragment> =
            fragmentDispatchingAndroidInjector

    override fun updateLanguage(language: String, country: String?) {
        val locale: Locale = if (country != null) {
            Locale(language, country)
        } else {
            Locale(language)
        }

        Locale.setDefault(locale)

        val config = Configuration()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocales(LocaleList(locale))
        } else {
            config.locale = locale
        }

        // TODO We need to check out a better way to use createConfigurationContext
        // instead of updateConfiguration here since it is deprecated.
        resources.updateConfiguration(config, resources.displayMetrics)
    }

    private fun clearAppNotifications() =
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancelAll()
}
