package chat.ts.android.main.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import chat.ts.android.R
import chat.ts.android.app.RocketChatApplication
import chat.ts.android.authentication.domain.model.DeepLinkInfo
import chat.ts.android.chatrooms.ui.ChatRoomsFragment
import chat.ts.android.main.presentation.MainPresenter
import chat.ts.android.server.ui.INTENT_USER_NAME
import chat.ts.android.util.extensions.ifNotNullNotEmpty
import chat.ts.android.util.extensions.inflate
import chat.ts.android.util.extensions.isNotNullNorEmpty
import chat.ts.android.widget.BottomNavigationBehavior
import com.ns.chat.views.activity.FriendPostsFragment
import com.ts.notification.model.Notification
import com.ts.notification.ui.NotificationFragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_main_v2.*
import kotlinx.android.synthetic.main.app_bar_chat_rooms.*
import kotlinx.android.synthetic.main.conversation_actionbar.*
import javax.inject.Inject

internal const val TAG_MAIN_FRAGMENT = "MainFragment"
private const val BUNDLE_CHAT_ROOM_ID = "BUNDLE_CHAT_ROOM_ID"

fun newMainFragmentInstance(userName: String?, chatRoomId: String?, deepLinkInfo: DeepLinkInfo?): Fragment =
        MainFragment().apply {
            arguments = Bundle(1).apply {
                putString(BUNDLE_CHAT_ROOM_ID, chatRoomId)
                putString(INTENT_USER_NAME, userName)
                putParcelable(
                        chat.ts.android.authentication.domain.model.DEEP_LINK_INFO_KEY,
                        deepLinkInfo
                )
            }
        }

class MainFragment : Fragment() {
    @Inject
    lateinit var presenter: MainPresenter

    private var chatRoomId: String? = null
    private var userName: String? = null
    private var deepLinkInfo: DeepLinkInfo? = null

    private var prevItem: MenuItem? = null

    private var chats: ChatRoomsFragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)

        arguments?.run {
            chatRoomId = getString(BUNDLE_CHAT_ROOM_ID)
            userName = getString(INTENT_USER_NAME)

            chatRoomId.ifNotNullNotEmpty {
                chatRoomId = null
            }
            deepLinkInfo =
                    getParcelable(chat.ts.android.authentication.domain.model.DEEP_LINK_INFO_KEY)

        }


    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = container?.inflate(R.layout.activity_main_v2)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupListener()
        setupViewPager(frame_container)
    }

    private fun setupListener() {
        image_avatar.setOnClickListener { presenter.toSettings() }
    }

    private fun setupViewPager(viewPager: View) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        chats = chat.ts.android.chatrooms.ui.newInstance(userName, chatRoomId, deepLinkInfo)
        val post = FriendPostsFragment()
        val bundle = Bundle()
        bundle.putString(com.ns.chat.views.activity.profile.ProfileFragment.USER_ID_EXTRA_KEY, RocketChatApplication.current_user_firebase_id)
        post.arguments = bundle
        val notification = NotificationFragment.newInstance("33FCA8XHndH694MdK") {
            openNotificationDetail(it)
        }
        adapter.addFragment(chats)
        adapter.addFragment(post)
        adapter.addFragment(notification)
        (viewPager as ViewPager).adapter = adapter

        navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_chats -> viewPager.currentItem = 0
                R.id.navigation_chats_group -> viewPager.currentItem = 1
                R.id.navigation_profile -> viewPager.currentItem = 2
            }

            // attaching bottom sheet behaviour - hide / show on scroll
            val layoutParams = navigation.layoutParams as CoordinatorLayout.LayoutParams
            layoutParams.behavior = BottomNavigationBehavior()

            false
        }


        viewPager.offscreenPageLimit = 3
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                if (prevItem != null) prevItem?.isChecked = false else navigation.menu.getItem(0).isChecked = false
                navigation.menu.getItem(position).isChecked = true
                prevItem = navigation.menu.getItem(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

    }

    private fun openNotificationDetail(it: Notification?) {
        if(it?.content?.packageName.isNotNullNorEmpty()){
            val launchIntent = it?.content?.packageName?.let { packageName -> activity?.packageManager?.getLaunchIntentForPackage(packageName) }
            if(launchIntent != null){
                val bundle = Bundle()
                if(it.content.actionClick.isNotNullNorEmpty()){
                    bundle.putString("actionId", "" + it.content.id)
                    bundle.putString("click_action", it.content.actionClick)
                }
                launchIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                launchIntent.putExtras(bundle)
                activity?.startActivity(launchIntent)
            }else{
                Toast.makeText(activity, "No package name", Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(activity, "No package name", Toast.LENGTH_LONG).show()
        }
    }


    fun goToChatRoom(userName: String) {
        chats?.loadRoomFromUserName(userName)
    }

    fun setupToolbar() {
        with((activity as AppCompatActivity)) {
            setSupportActionBar(toolbar)
            val urlImg = presenter.getAvatarUser()
            image_avatar.setImageURI(urlImg)
            supportActionBar?.setDisplayShowHomeEnabled(false)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setHomeButtonEnabled(false)
        }
        text_server_name?.text = RocketChatApplication.account?.userName
    }


}