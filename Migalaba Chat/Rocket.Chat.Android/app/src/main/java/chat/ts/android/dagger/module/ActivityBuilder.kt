package chat.ts.android.dagger.module

import chat.ts.android.authentication.di.AuthenticationModule
import chat.ts.android.authentication.login.di.LoginFragmentProvider
import chat.ts.android.authentication.loginoptions.di.LoginOptionsFragmentProvider
import chat.ts.android.authentication.onboarding.di.OnBoardingFragmentProvider
import chat.ts.android.authentication.registerusername.di.RegisterUsernameFragmentProvider
import chat.ts.android.authentication.resetpassword.di.ResetPasswordFragmentProvider
import chat.ts.android.authentication.server.di.ServerFragmentProvider
import chat.ts.android.authentication.signup.di.SignupFragmentProvider
import chat.ts.android.authentication.twofactor.di.TwoFAFragmentProvider
import chat.ts.android.authentication.ui.AuthenticationActivity
import chat.ts.android.chatdetails.di.ChatDetailsFragmentProvider
import chat.ts.android.chatinformation.di.MessageInfoFragmentProvider
import chat.ts.android.chatinformation.ui.MessageInfoActivity
import chat.ts.android.chatroom.di.ChatRoomFragmentProvider
import chat.ts.android.chatroom.di.ChatRoomModule
import chat.ts.android.chatroom.ui.ChatRoomActivity
import chat.ts.android.chatrooms.di.ChatRoomsFragmentProvider
import chat.ts.android.createchannel.di.CreateChannelProvider
import chat.ts.android.dagger.scope.PerActivity
import chat.ts.android.directory.di.DirectoryFragmentProvider
import chat.ts.android.draw.main.di.DrawModule
import chat.ts.android.draw.main.ui.DrawingActivity
import chat.ts.android.favoritemessages.di.FavoriteMessagesFragmentProvider
import chat.ts.android.files.di.FilesFragmentProvider
import chat.ts.android.inviteusers.di.InviteUsersFragmentProvider
import chat.ts.android.main.di.MainFragmentProvider
import chat.ts.android.main.di.MainModule
import chat.ts.android.main.ui.MainActivity
import chat.ts.android.members.di.MembersFragmentProvider
import chat.ts.android.mentions.di.MentionsFragmentProvider
import chat.ts.android.pinnedmessages.di.PinnedMessagesFragmentProvider
import chat.ts.android.profile.di.ProfileFragmentProvider
import chat.ts.android.server.di.ChangeServerModule
import chat.ts.android.server.ui.ChangeServerActivity
import chat.ts.android.servers.di.ServersBottomSheetFragmentProvider
import chat.ts.android.settings.di.SettingsFragmentProvider
import chat.ts.android.settings.password.di.PasswordFragmentProvider
import chat.ts.android.settings.password.ui.PasswordActivity
import chat.ts.android.sortingandgrouping.di.SortingAndGroupingBottomSheetFragmentProvider
import chat.ts.android.userdetails.di.UserDetailsFragmentProvider
import chat.ts.android.videoconference.di.VideoConferenceModule
import chat.ts.android.videoconference.ui.VideoConferenceActivity
import chat.ts.android.webview.adminpanel.di.AdminPanelWebViewFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @PerActivity
    @ContributesAndroidInjector(
        modules = [AuthenticationModule::class,
            OnBoardingFragmentProvider::class,
            ServerFragmentProvider::class,
            LoginOptionsFragmentProvider::class,
            LoginFragmentProvider::class,
            RegisterUsernameFragmentProvider::class,
            ResetPasswordFragmentProvider::class,
            SignupFragmentProvider::class,
            TwoFAFragmentProvider::class
        ]
    )
    abstract fun bindAuthenticationActivity(): AuthenticationActivity

    @PerActivity
    @ContributesAndroidInjector(
        modules = [MainModule::class,
            MainFragmentProvider::class,
            ChatRoomsFragmentProvider::class,
            ServersBottomSheetFragmentProvider::class,
            SortingAndGroupingBottomSheetFragmentProvider::class,
            CreateChannelProvider::class,
            ProfileFragmentProvider::class,
            SettingsFragmentProvider::class,
            AdminPanelWebViewFragmentProvider::class,
            DirectoryFragmentProvider::class
        ]
    )
    abstract fun bindMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector(
        modules = [ChatRoomModule::class,
            ChatRoomFragmentProvider::class,
            UserDetailsFragmentProvider::class,
            ChatDetailsFragmentProvider::class,
            MembersFragmentProvider::class,
            InviteUsersFragmentProvider::class,
            MentionsFragmentProvider::class,
            PinnedMessagesFragmentProvider::class,
            FavoriteMessagesFragmentProvider::class,
            FilesFragmentProvider::class
        ]
    )
    abstract fun bindChatRoomActivity(): ChatRoomActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [PasswordFragmentProvider::class])
    abstract fun bindPasswordActivity(): PasswordActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ChangeServerModule::class])
    abstract fun bindChangeServerActivity(): ChangeServerActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [MessageInfoFragmentProvider::class])
    abstract fun bindMessageInfoActiviy(): MessageInfoActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [DrawModule::class])
    abstract fun bindDrawingActivity(): DrawingActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [VideoConferenceModule::class])
    abstract fun bindVideoConferenceActivity(): VideoConferenceActivity
}
