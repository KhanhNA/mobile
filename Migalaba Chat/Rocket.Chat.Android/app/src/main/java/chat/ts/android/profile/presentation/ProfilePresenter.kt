package chat.ts.android.profile.presentation

import android.graphics.Bitmap
import android.net.Uri
import chat.ts.android.chatroom.domain.UriInteractor
import chat.ts.android.core.behaviours.showMessage
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.helper.UserHelper
import chat.ts.android.main.presentation.MainNavigator
import chat.ts.android.server.domain.GetCurrentServerInteractor
import chat.ts.android.server.domain.TokenRepository
import chat.ts.android.server.infrastructure.RocketChatClientFactory
import chat.ts.android.util.extension.compressImageAndGetByteArray
import chat.ts.android.util.extension.launchUI
import chat.ts.android.util.extensions.avatarUrl
import chat.ts.android.util.retryIO
import chat.rocket.common.RocketChatException
import chat.rocket.common.model.UserStatus
import chat.rocket.common.util.ifNull
import chat.rocket.core.RocketChatClient
import chat.rocket.core.internal.realtime.setDefaultStatus
import chat.rocket.core.internal.rest.me
import chat.rocket.core.internal.rest.resetAvatar
import chat.rocket.core.internal.rest.setAvatar
import chat.rocket.core.internal.rest.updateProfile
import java.util.*
import javax.inject.Inject
import javax.inject.Named

class ProfilePresenter @Inject constructor(
    private val view: ProfileView,
    private val strategy: CancelStrategy,
    private val navigator: MainNavigator,
    private val uriInteractor: UriInteractor,
    val userHelper: UserHelper,
    serverInteractor: GetCurrentServerInteractor,
    factory: RocketChatClientFactory,
    tokenRepository: TokenRepository
) {
    private val serverUrl = serverInteractor.get()!!
    private val client: RocketChatClient = factory.get(serverUrl)
    private val user = userHelper.user()
    private val token = tokenRepository.get(serverUrl)

    fun loadUserProfile() {
        launchUI(strategy) {
            view.showLoading()
            try {
                val me = retryIO(description = "me", times = 5) {
                    client.me()
                }

                view.showProfile(
                    me.status.toString(),
                    serverUrl.avatarUrl(me.username!!, token?.userId, token?.authToken),
                    me.name ?: "",
                    me.username ?: "",
                    me.emails?.getOrNull(0)?.address ?: ""
                )
            } catch (exception: RocketChatException) {
                view.showMessage(exception)
            } finally {
                view.hideLoading()
            }
        }
    }

    fun updateUserProfile(email: String, name: String, username: String) {
        launchUI(strategy) {
            view.showLoading()
            try {
                user?.id?.let { id ->
                    retryIO {
                        client.updateProfile(
                            userId = id,
                            email = email,
                            name = name,
                            username = username
                        )
                    }
                    view.onProfileUpdatedSuccessfully(email, name, username)
                    view.showProfile(
                        user.status.toString(),
                        serverUrl.avatarUrl(user.username!!, token?.userId, token?.authToken),
                        name,
                        username,
                        email
                    )
                }
            } catch (exception: RocketChatException) {
                exception.message?.let {
                    view.showMessage(it)
                }.ifNull {
                    view.showGenericErrorMessage()
                }
            } finally {
                view.hideLoading()
            }
        }
    }

    fun updateAvatar(uri: Uri) {
        launchUI(strategy) {
            view.showLoading()
            try {
                retryIO {
                    client.setAvatar(
                        uriInteractor.getFileName(uri) ?: uri.toString(),
                        uriInteractor.getMimeType(uri)
                    ) {
                        uriInteractor.getInputStream(uri)
                    }
                }
                user?.username?.let {
                    view.reloadUserAvatar(
                        serverUrl.avatarUrl(
                            it,
                            token?.userId,
                            token?.authToken
                        )
                    )
                }
            } catch (exception: RocketChatException) {
                exception.message?.let {
                    view.showMessage(it)
                }.ifNull {
                    view.showGenericErrorMessage()
                }
            } finally {
                view.hideLoading()
            }
        }
    }

    fun preparePhotoAndUpdateAvatar(bitmap: Bitmap) {
        launchUI(strategy) {
            view.showLoading()
            try {
                val byteArray = bitmap.compressImageAndGetByteArray("image/png")

                retryIO {
                    client.setAvatar(
                        UUID.randomUUID().toString() + ".png",
                        "image/png"
                    ) {
                        byteArray?.inputStream()
                    }
                }

                user?.username?.let {
                    view.reloadUserAvatar(
                        serverUrl.avatarUrl(
                            it,
                            token?.userId,
                            token?.authToken
                        )
                    )
                }
            } catch (exception: RocketChatException) {
                exception.message?.let {
                    view.showMessage(it)
                }.ifNull {
                    view.showGenericErrorMessage()
                }
            } finally {
                view.hideLoading()
            }
        }
    }

    fun resetAvatar() {
        launchUI(strategy) {
            view.showLoading()
            try {
                user?.id?.let { id ->
                    retryIO { client.resetAvatar(id) }
                }
                user?.username?.let {
                    view.reloadUserAvatar(
                        serverUrl.avatarUrl(
                            it,
                            token?.userId,
                            token?.authToken
                        )
                    )
                }
            } catch (exception: RocketChatException) {
                exception.message?.let {
                    view.showMessage(it)
                }.ifNull {
                    view.showGenericErrorMessage()
                }
            } finally {
                view.hideLoading()
            }
        }
    }

    fun updateStatus(status: UserStatus) {
        launchUI(strategy) {
            try {
                client.setDefaultStatus(status)
            } catch (exception: RocketChatException) {
                exception.message?.let {
                    view.showMessage(it)
                }.ifNull {
                    view.showGenericErrorMessage()
                }
            }
        }
    }

    fun toProfileImage() = user?.username?.let { username ->
        navigator.toProfileImage(serverUrl.avatarUrl(username, token?.userId, token?.authToken))
    }
}