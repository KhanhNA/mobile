package chat.ts.android.authentication.login.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.authentication.login.presentation.LoginView
import chat.ts.android.authentication.login.ui.LoginFragment
import chat.ts.android.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class LoginFragmentModule {

    @Provides
    @PerFragment
    fun loginView(frag: LoginFragment): LoginView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: LoginFragment): LifecycleOwner = frag
}