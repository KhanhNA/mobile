package chat.ts.android.main.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.main.ui.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentProvider {

    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    @PerFragment
    abstract fun provideMainFragment(): MainFragment
}