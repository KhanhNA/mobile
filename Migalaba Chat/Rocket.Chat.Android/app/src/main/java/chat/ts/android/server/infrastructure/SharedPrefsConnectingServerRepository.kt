package chat.ts.android.server.infrastructure

import android.content.SharedPreferences
import chat.ts.android.app.RocketChatApplication
import chat.ts.android.server.domain.CurrentServerRepository

class SharedPrefsConnectingServerRepository(private val preferences: SharedPreferences) : CurrentServerRepository {

    override fun save(url: String) {
        preferences.edit().putString(CONNECTING_SERVER_KEY, url).apply()
    }

    override fun get(): String? {
        return preferences.getString(CONNECTING_SERVER_KEY, RocketChatApplication.server_url)
    }

    companion object {
        private const val CONNECTING_SERVER_KEY = "connecting_server"
    }

    override fun clear() {
        preferences.edit().remove(CONNECTING_SERVER_KEY).apply()
    }
}