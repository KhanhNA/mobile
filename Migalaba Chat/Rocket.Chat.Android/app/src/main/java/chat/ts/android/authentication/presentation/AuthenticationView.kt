package chat.ts.android.authentication.presentation

interface AuthenticationView {
    fun showServerInput()
}