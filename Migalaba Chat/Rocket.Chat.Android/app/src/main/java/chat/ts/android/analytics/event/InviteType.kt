package chat.ts.android.analytics.event

enum class InviteType(val inviteTypeName: String) {
    ViaApp("viaApp")
}