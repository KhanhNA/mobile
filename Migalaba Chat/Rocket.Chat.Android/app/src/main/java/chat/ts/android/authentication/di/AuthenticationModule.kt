package chat.ts.android.authentication.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.authentication.presentation.AuthenticationNavigator
import chat.ts.android.authentication.ui.AuthenticationActivity
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.dagger.scope.PerActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class AuthenticationModule {

    @Provides
    @PerActivity
    fun provideAuthenticationNavigator(activity: AuthenticationActivity) =
        AuthenticationNavigator(activity)

    @Provides
    @PerActivity
    fun provideJob(): Job = Job()

    @Provides
    @PerActivity
    fun provideLifecycleOwner(activity: AuthenticationActivity): LifecycleOwner = activity

    @Provides
    @PerActivity
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy =
        CancelStrategy(owner, jobs)
}