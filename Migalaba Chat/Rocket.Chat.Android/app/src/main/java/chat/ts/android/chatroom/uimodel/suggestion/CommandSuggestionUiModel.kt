package chat.ts.android.chatroom.uimodel.suggestion

import chat.ts.android.suggestions.model.SuggestionModel

class CommandSuggestionUiModel(
    text: String,
    val description: String,
    searchList: List<String>
) : SuggestionModel(text, searchList)