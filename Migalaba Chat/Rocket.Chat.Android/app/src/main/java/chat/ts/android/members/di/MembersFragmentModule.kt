package chat.ts.android.members.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.members.presentation.MembersView
import chat.ts.android.members.ui.MembersFragment
import dagger.Module
import dagger.Provides

@Module
class MembersFragmentModule {

    @Provides
    @PerFragment
    fun membersView(frag: MembersFragment): MembersView {
        return frag
    }
}