package chat.ts.android.inviteusers.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.inviteusers.presentation.InviteUsersView
import chat.ts.android.inviteusers.ui.InviteUsersFragment
import dagger.Module
import dagger.Provides

@Module
class InviteUsersFragmentModule {

    @Provides
    @PerFragment
    fun inviteUsersView(frag: InviteUsersFragment): InviteUsersView = frag
}