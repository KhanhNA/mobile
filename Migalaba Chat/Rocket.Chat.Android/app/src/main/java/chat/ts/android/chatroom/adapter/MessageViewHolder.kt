package chat.ts.android.chatroom.adapter

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.Spannable
import android.text.method.LinkMovementMethod
import android.text.style.ImageSpan
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.children
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import chat.ts.android.R
import chat.ts.android.chatroom.uimodel.MessageUiModel
import chat.ts.android.emoji.EmojiReactionListener
import chat.rocket.core.model.MessageType
import chat.rocket.core.model.isSystemMessage
import chat.ts.android.app.RocketChatApplication
import com.bumptech.glide.load.resource.gif.GifDrawable
import kotlinx.android.synthetic.main.avatar.view.*
import kotlinx.android.synthetic.main.fragment_authentication_log_in.view.*
import kotlinx.android.synthetic.main.item_message.view.*
import kotlinx.android.synthetic.main.item_message.view.button_join_video_call
import kotlinx.android.synthetic.main.item_message.view.day
import kotlinx.android.synthetic.main.item_message.view.day_marker_layout
import kotlinx.android.synthetic.main.item_message.view.image_star_indicator
import kotlinx.android.synthetic.main.item_message.view.layout_avatar
import kotlinx.android.synthetic.main.item_message.view.new_messages_notif
import kotlinx.android.synthetic.main.item_message.view.read_receipt_view
import kotlinx.android.synthetic.main.item_message.view.text_content
import kotlinx.android.synthetic.main.item_message.view.text_edit_indicator
import kotlinx.android.synthetic.main.item_message.view.text_message_time
import kotlinx.android.synthetic.main.item_message.view.text_sender
import kotlinx.android.synthetic.main.item_my_message.view.*

class MessageViewHolder(
    itemView: View,
    listener: ActionsListener,
    reactionListener: EmojiReactionListener? = null,
    private val avatarListener: (String) -> Unit,
    private val joinVideoCallListener: (View) -> Unit
) : BaseViewHolder<MessageUiModel>(itemView, listener, reactionListener), Drawable.Callback {

    init {
        with(itemView) {
            setupActionMenu(my_message_container)
            text_content.movementMethod = LinkMovementMethod()
        }
    }

    override fun bindViews(data: MessageUiModel) {
        with(itemView) {
            day.text = data.currentDayMarkerText
            day_marker_layout.isVisible = data.showDayMarker

            new_messages_notif.isVisible = data.isFirstUnread

            text_message_time.text = data.time
            text_sender.text = data.senderName

            if (data.content is Spannable) {
                val spans = data.content.getSpans(0, data.content.length, ImageSpan::class.java)
                spans.forEach {
                    if (it.drawable is GifDrawable) {
                        it.drawable.callback = this@MessageViewHolder
                        (it.drawable as GifDrawable).start()
                    }
                }
            }

            text_content.text_content.text = data.content

            button_join_video_call.isVisible = data.message.type is MessageType.JitsiCallStarted
            button_join_video_call.setOnClickListener { joinVideoCallListener(it) }

            image_avatar.setImageURI(data.avatar)
            text_content.setTextColor(if (data.isTemporary) Color.GRAY else Color.BLACK)

            data.message.let {
                text_edit_indicator.isVisible = !it.isSystemMessage() && it.editedBy != null
                image_star_indicator.isVisible = it.starred?.isNotEmpty() ?: false
            }

            if (data.unread == null) {
                read_receipt_view.isVisible = false
            } else {
                read_receipt_view.setImageResource(
                    if (data.unread == true) {
                        R.drawable.ic_check_unread_24dp
                    } else {
                        R.drawable.ic_check_read_24dp
                    }
                )
                read_receipt_view.isVisible = true
            }

            image_avatar.setOnClickListener {
                data.message.sender?.id?.let { userId ->
                    avatarListener(userId)
                }
            }

            if(data.message.sender?.username == RocketChatApplication.account?.userName){
                layout_message.layoutDirection = LinearLayout.LAYOUT_DIRECTION_RTL
                my_message_header.layoutDirection = LinearLayout.LAYOUT_DIRECTION_RTL
                text_content.setTextColor(resources.getColor(R.color.color_white))
                text_content.background = itemView.context.getDrawable(R.drawable.bg_my_item_message)
            }else{
                layout_message.layoutDirection = LinearLayout.LAYOUT_DIRECTION_LTR
                my_message_header.layoutDirection = LinearLayout.LAYOUT_DIRECTION_LTR
                text_content.setTextColor(resources.getColor(R.color.colorPrimaryText))
                text_content.background = itemView.context.getDrawable(R.drawable.bg_border_user_details_avatar)
            }

        }
    }

    override fun unscheduleDrawable(who: Drawable, what: Runnable) {
        with(itemView) {
            text_content.removeCallbacks(what)
        }
    }

    override fun invalidateDrawable(p0: Drawable) {
        with(itemView) {
            text_content.invalidate()
        }
    }

    override fun scheduleDrawable(who: Drawable, what: Runnable, w: Long) {
        with(itemView) {
            text_content.postDelayed(what, w)
        }
    }
}
