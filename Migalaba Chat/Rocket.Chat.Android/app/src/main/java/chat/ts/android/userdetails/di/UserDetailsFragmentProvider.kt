package chat.ts.android.userdetails.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.userdetails.ui.UserDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = [UserDetailsFragmentModule::class])
    @PerFragment
    abstract fun provideUserDetailsFragment(): UserDetailsFragment
}
