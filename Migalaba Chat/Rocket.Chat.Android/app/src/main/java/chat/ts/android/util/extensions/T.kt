package chat.ts.android.util.extensions

val <T> T.exhaustive: T
    get() = this