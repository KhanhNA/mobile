package chat.ts.android.settings.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.settings.ui.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SettingsFragmentProvider {

    @ContributesAndroidInjector(modules = [SettingsFragmentModule::class])
    @PerFragment
    abstract fun provideSettingsFragment(): SettingsFragment
}
