package chat.ts.android.authentication.presentation

import chat.ts.android.app.RocketChatApplication
import chat.ts.android.authentication.domain.model.DeepLinkInfo
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.infrastructure.LocalRepository
import chat.ts.android.server.domain.GetAccountInteractor
import chat.ts.android.server.domain.GetConnectingServerInteractor
import chat.ts.android.server.domain.GetCurrentServerInteractor
import chat.ts.android.server.domain.SettingsRepository
import chat.ts.android.server.domain.TokenRepository
import chat.ts.android.util.extension.launchUI
import chat.ts.android.util.extensions.privacyPolicyUrl
import chat.ts.android.util.extensions.termsOfServiceUrl
import javax.inject.Inject

class AuthenticationPresenter @Inject constructor(
        private val strategy: CancelStrategy,
        private val navigator: AuthenticationNavigator,
        private val getCurrentServerInteractor: GetCurrentServerInteractor,
        private val getAccountInteractor: GetAccountInteractor,
        private val settingsRepository: SettingsRepository,
        private val localRepository: LocalRepository,
        private val tokenRepository: TokenRepository,
        private val serverInteractor: GetConnectingServerInteractor
) {

    fun loadCredentials(newServer: Boolean, callback: (isAuthenticated: Boolean) -> Unit) {
        launchUI(strategy) {
            val currentServer = getCurrentServerInteractor.get()
            val serverToken = currentServer?.let { tokenRepository.get(currentServer) }
            val settings = currentServer?.let { settingsRepository.get(currentServer) }
            val account = currentServer?.let { getAccountInteractor.get(currentServer) }

            account?.let {
                localRepository.save(LocalRepository.CURRENT_USERNAME_KEY, account.userName)
            }

            if (newServer || currentServer == null ||
                    serverToken == null ||
                    settings == null ||
                    account?.userName == null
            ) {
                callback(false)
            } else {
                RocketChatApplication.account = account
                callback(true)
            }
        }
    }

    fun termsOfService(toolbarTitle: String) =
            serverInteractor.get()?.let { navigator.toWebPage(it.termsOfServiceUrl(), toolbarTitle) }

    fun privacyPolicy(toolbarTitle: String) =
            serverInteractor.get()?.let { navigator.toWebPage(it.privacyPolicyUrl(), toolbarTitle) }

    fun toOnBoarding() = navigator.toOnBoarding()

    fun toSignInToYourServer(deepLinkInfo: DeepLinkInfo? = null) =
            navigator.toSignInToYourServer(deepLinkInfo, false)

    fun saveDeepLinkInfo(deepLinkInfo: DeepLinkInfo) = navigator.saveDeepLinkInfo(deepLinkInfo)

    fun toChatList() = navigator.toChatList()

    fun toChatList(userName: String?) = navigator.toChatListByUserName(userName)

    fun toChatList(deepLinkInfo: DeepLinkInfo) = navigator.toChatList(deepLinkInfo)
}
