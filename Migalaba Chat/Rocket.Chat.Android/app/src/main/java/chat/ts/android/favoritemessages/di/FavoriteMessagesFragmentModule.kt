package chat.ts.android.favoritemessages.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.favoritemessages.presentation.FavoriteMessagesView
import chat.ts.android.favoritemessages.ui.FavoriteMessagesFragment
import dagger.Module
import dagger.Provides

@Module
class FavoriteMessagesFragmentModule {

    @Provides
    @PerFragment
    fun provideFavoriteMessagesView(frag: FavoriteMessagesFragment): FavoriteMessagesView {
        return frag
    }
}