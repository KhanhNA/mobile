package chat.ts.android.util

import android.content.Context
import android.content.Intent
import android.os.Bundle

class TsUtil {

    companion object {
        fun startActivity(context: Context, clazz: Class<*>, bundle: Bundle? = null) {

            val intent = Intent(context, clazz)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
            bundle?.let { it1 -> intent.putExtras(it1) }
            context.startActivity(intent)

        }
    }
}
