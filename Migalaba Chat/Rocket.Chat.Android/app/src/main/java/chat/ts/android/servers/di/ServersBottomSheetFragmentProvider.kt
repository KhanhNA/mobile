package chat.ts.android.servers.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.servers.ui.ServersBottomSheetFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServersBottomSheetFragmentProvider {

    @ContributesAndroidInjector(modules = [ServersBottomSheetFragmentModule::class])
    @PerFragment
    abstract fun provideServersBottomSheetFragment(): ServersBottomSheetFragment
}