package chat.ts.android.widget

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager



class CustomViewPager(context: Context?, attrs: AttributeSet?) : ViewPager(context!!, attrs) {
    var swipeAble = true

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (swipeAble) {
            super.onTouchEvent(event)
        } else false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (swipeAble) {
            super.onInterceptTouchEvent(event)
        } else false
    }


}