package chat.ts.android.mentions.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.mentions.ui.MentionsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MentionsFragmentProvider {

    @ContributesAndroidInjector(modules = [MentionsFragmentModule::class])
    @PerFragment
    abstract fun provideMentionsFragment(): MentionsFragment
}