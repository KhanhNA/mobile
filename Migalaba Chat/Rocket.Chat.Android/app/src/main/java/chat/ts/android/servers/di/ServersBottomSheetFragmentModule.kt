package chat.ts.android.servers.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.servers.presentation.ServersView
import chat.ts.android.servers.ui.ServersBottomSheetFragment
import dagger.Module
import dagger.Provides

@Module
class ServersBottomSheetFragmentModule {

    @Provides
    @PerFragment
    fun serversView(frag: ServersBottomSheetFragment): ServersView = frag
}