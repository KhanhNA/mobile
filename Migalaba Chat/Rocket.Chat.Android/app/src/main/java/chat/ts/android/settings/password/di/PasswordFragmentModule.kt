package chat.ts.android.settings.password.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.settings.password.presentation.PasswordView
import chat.ts.android.settings.password.ui.PasswordFragment
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class PasswordFragmentModule {

    @Provides
    @PerFragment
    fun provideJob(): Job = Job()

    @Provides
    @PerFragment
    fun passwordView(frag: PasswordFragment): PasswordView {
        return frag
    }

    @Provides
    @PerFragment
    fun settingsLifecycleOwner(frag: PasswordFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}
