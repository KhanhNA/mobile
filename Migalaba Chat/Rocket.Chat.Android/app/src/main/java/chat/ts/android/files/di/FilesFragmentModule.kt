package chat.ts.android.files.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.files.presentation.FilesView
import chat.ts.android.files.ui.FilesFragment
import dagger.Module
import dagger.Provides

@Module
class FilesFragmentModule {

    @Provides
    @PerFragment
    fun provideFilesView(frag: FilesFragment): FilesView {
        return frag
    }
}