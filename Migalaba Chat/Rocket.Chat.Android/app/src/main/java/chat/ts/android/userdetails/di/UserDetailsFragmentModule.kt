package chat.ts.android.userdetails.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.userdetails.presentation.UserDetailsView
import chat.ts.android.userdetails.ui.UserDetailsFragment
import dagger.Module
import dagger.Provides

@Module
class UserDetailsFragmentModule {

    @Provides
    @PerFragment
    fun provideUserDetailsView(frag: UserDetailsFragment): UserDetailsView = frag
}