package chat.ts.android.push

import com.google.gson.annotations.SerializedName


data class EJson(
        @SerializedName("messageType") val messageType: String,
        @SerializedName("rid") val rid: String
)