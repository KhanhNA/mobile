package chat.ts.android.chatrooms.adapter

interface ItemHolder<T> {
    val data: T
}