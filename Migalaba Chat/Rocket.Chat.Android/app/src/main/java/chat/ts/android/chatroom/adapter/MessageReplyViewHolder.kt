package chat.ts.android.chatroom.adapter

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import chat.ts.android.app.RocketChatApplication
import chat.ts.android.chatroom.uimodel.MessageReplyUiModel
import chat.ts.android.emoji.EmojiReactionListener
import kotlinx.android.synthetic.main.item_message_reply.view.*
import kotlinx.android.synthetic.main.message_url_preview.view.*

class MessageReplyViewHolder(
    itemView: View,
    listener: ActionsListener,
    reactionListener: EmojiReactionListener? = null,
    private val replyCallback: (roomName: String, permalink: String) -> Unit
) : BaseViewHolder<MessageReplyUiModel>(itemView, listener, reactionListener) {

    init {
        setupActionMenu(itemView)
    }

    override fun bindViews(data: MessageReplyUiModel) {
        with(itemView) {
            button_message_reply.setOnClickListener {
                with(data.rawData) {
                    replyCallback.invoke(roomName, permalink)
                }
            }

            if(data.message.sender?.username == RocketChatApplication.account?.userName){
                message_container.layoutDirection = ConstraintLayout.LAYOUT_DIRECTION_RTL
            }else{
                message_container.layoutDirection = ConstraintLayout.LAYOUT_DIRECTION_LTR
            }
        }
    }
}