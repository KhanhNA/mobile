package chat.ts.android.chatdetails.adapter

import chat.ts.android.chatdetails.domain.Option
import chat.ts.android.chatrooms.adapter.ItemHolder

data class OptionItemHolder(override val data: Option): ItemHolder<Option>