package chat.ts.android.chatroom.uimodel

interface BaseAttachmentUiModel<out T> : BaseUiModel<T> {
    val attachmentUrl: String
}