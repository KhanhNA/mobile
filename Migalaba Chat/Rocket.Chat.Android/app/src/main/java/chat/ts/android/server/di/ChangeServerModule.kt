package chat.ts.android.server.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.dagger.scope.PerActivity
import chat.ts.android.server.presentation.ChangeServerNavigator
import chat.ts.android.server.presentation.ChangeServerView
import chat.ts.android.server.ui.ChangeServerActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class ChangeServerModule {

    @Provides
    @PerActivity
    fun provideJob(): Job = Job()

    @Provides
    @PerActivity
    fun provideChangeServerNavigator(activity: ChangeServerActivity) = ChangeServerNavigator(activity)

    @Provides
    @PerActivity
    fun ChangeServerView(activity: ChangeServerActivity): ChangeServerView {
        return activity
    }

    @Provides
    fun provideLifecycleOwner(activity: ChangeServerActivity): LifecycleOwner = activity

    @Provides
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy = CancelStrategy(owner, jobs)
}