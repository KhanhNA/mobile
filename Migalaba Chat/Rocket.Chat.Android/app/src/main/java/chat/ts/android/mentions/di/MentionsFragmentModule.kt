package chat.ts.android.mentions.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.mentions.presentention.MentionsView
import chat.ts.android.mentions.ui.MentionsFragment
import dagger.Module
import dagger.Provides

@Module
class MentionsFragmentModule {

    @Provides
    @PerFragment
    fun provideMentionsView(frag: MentionsFragment): MentionsView {
        return frag
    }
}