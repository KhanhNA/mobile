package chat.ts.android.chatdetails.di

import chat.ts.android.chatdetails.presentation.ChatDetailsView
import chat.ts.android.chatdetails.ui.ChatDetailsFragment
import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.db.ChatRoomDao
import chat.ts.android.db.DatabaseManager
import dagger.Module
import dagger.Provides

@Module
class ChatDetailsFragmentModule {

    @Provides
    @PerFragment
    fun chatDetailsView(frag: ChatDetailsFragment): ChatDetailsView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideChatRoomDao(manager: DatabaseManager): ChatRoomDao = manager.chatRoomDao()
}