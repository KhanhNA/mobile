package chat.ts.android.favoritemessages.presentation

import chat.ts.android.chatroom.uimodel.BaseUiModel
import chat.ts.android.core.behaviours.LoadingView
import chat.ts.android.core.behaviours.MessageView

interface FavoriteMessagesView : MessageView, LoadingView {

    /**
     * Shows the list of favorite messages for the current room.
     *
     * @param favoriteMessages The list of favorite messages to show.
     */
    fun showFavoriteMessages(favoriteMessages: List<BaseUiModel<*>>)
}