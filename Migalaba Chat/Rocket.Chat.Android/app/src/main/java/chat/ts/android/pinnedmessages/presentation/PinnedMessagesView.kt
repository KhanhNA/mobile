package chat.ts.android.pinnedmessages.presentation

import chat.ts.android.chatroom.uimodel.BaseUiModel
import chat.ts.android.core.behaviours.LoadingView
import chat.ts.android.core.behaviours.MessageView

interface PinnedMessagesView : MessageView, LoadingView {

    /**
     * Show list of pinned messages for the current room.
     *
     * @param pinnedMessages The list of pinned messages.
     */
    fun showPinnedMessages(pinnedMessages: List<BaseUiModel<*>>)
}