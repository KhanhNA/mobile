package chat.ts.android.chatroom.domain

data class MessageReply(val roomName: String, val permalink: String)