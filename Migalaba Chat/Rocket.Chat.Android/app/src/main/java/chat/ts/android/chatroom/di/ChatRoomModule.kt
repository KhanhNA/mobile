package chat.ts.android.chatroom.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.chatroom.presentation.ChatRoomNavigator
import chat.ts.android.chatroom.ui.ChatRoomActivity
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.dagger.scope.PerActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class ChatRoomModule {

    @Provides
    @PerActivity
    fun provideChatRoomNavigator(activity: ChatRoomActivity) = ChatRoomNavigator(activity)

    @Provides
    @PerActivity
    fun provideJob(): Job = Job()

    @Provides
    @PerActivity
    fun provideLifecycleOwner(activity: ChatRoomActivity): LifecycleOwner = activity

    @Provides
    @PerActivity
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}