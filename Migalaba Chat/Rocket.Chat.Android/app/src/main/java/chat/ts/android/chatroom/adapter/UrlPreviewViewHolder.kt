package chat.ts.android.chatroom.adapter

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import chat.ts.android.app.RocketChatApplication
import chat.ts.android.chatroom.uimodel.UrlPreviewUiModel
import chat.ts.android.emoji.EmojiReactionListener
import chat.ts.android.util.extensions.content
import chat.ts.android.util.extensions.openTabbedUrl
import chat.ts.android.R
import kotlinx.android.synthetic.main.message_url_preview.view.*

class UrlPreviewViewHolder(
    itemView: View,
    listener: ActionsListener,
    reactionListener: EmojiReactionListener? = null
) : BaseViewHolder<UrlPreviewUiModel>(itemView, listener, reactionListener) {

    init {
        setupActionMenu(itemView.url_preview_layout)
    }

    override fun bindViews(data: UrlPreviewUiModel) {
        with(itemView) {
            if (data.thumbUrl.isNullOrEmpty()) {
                image_preview.isVisible = false
            } else {
                image_preview.setImageURI(data.thumbUrl)
                image_preview.isVisible = true
            }
            text_host.content = data.hostname
            text_title.content = data.title
            text_description.content = data.description ?: ""

            url_preview_layout.setOnClickListener(onClickListener)
            text_host.setOnClickListener(onClickListener)
            text_title.setOnClickListener(onClickListener)
            image_preview.setOnClickListener(onClickListener)
            text_description.setOnClickListener(onClickListener)

            if(data.message.sender?.username == RocketChatApplication.account?.userName){

                url_preview_layout.layoutDirection = ConstraintLayout.LAYOUT_DIRECTION_RTL
                text_title.setTextColor(resources.getColor(R.color.color_white))
                text_host.setTextColor(resources.getColor(R.color.whitesmoke))
            }else{
                url_preview_layout.layoutDirection = ConstraintLayout.LAYOUT_DIRECTION_LTR
                text_title.setTextColor(resources.getColor(R.color.colorAccent))
                text_host.setTextColor(resources.getColor(R.color.colorSecondaryText))
            }
        }
    }

    private val onClickListener = { view: View ->
        if (data != null) {
            view.openTabbedUrl(data!!.rawData.url)
        }
    }
}