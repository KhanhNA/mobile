package chat.ts.android.authentication.twofactor.di

import chat.ts.android.authentication.twofactor.ui.TwoFAFragment
import chat.ts.android.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TwoFAFragmentProvider {

    @ContributesAndroidInjector(modules = [TwoFAFragmentModule::class])
    @PerFragment
    abstract fun provideTwoFAFragment(): TwoFAFragment
}
