package chat.ts.android.authentication.registerusername.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.authentication.registerusername.presentation.RegisterUsernameView
import chat.ts.android.authentication.registerusername.ui.RegisterUsernameFragment
import chat.ts.android.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class RegisterUsernameFragmentModule {

    @Provides
    @PerFragment
    fun registerUsernameView(frag: RegisterUsernameFragment): RegisterUsernameView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: RegisterUsernameFragment): LifecycleOwner = frag
}