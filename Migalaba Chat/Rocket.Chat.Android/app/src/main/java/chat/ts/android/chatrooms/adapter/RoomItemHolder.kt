package chat.ts.android.chatrooms.adapter

import chat.ts.android.chatrooms.adapter.model.RoomUiModel

data class RoomItemHolder(override val data: RoomUiModel) : ItemHolder<RoomUiModel>