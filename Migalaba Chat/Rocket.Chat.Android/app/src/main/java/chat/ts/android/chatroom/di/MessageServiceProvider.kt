package chat.ts.android.chatroom.di

import chat.ts.android.chatroom.service.MessageService
import chat.ts.android.dagger.module.AppModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class MessageServiceProvider {

    @ContributesAndroidInjector(modules = [AppModule::class])
    abstract fun provideMessageService(): MessageService
}