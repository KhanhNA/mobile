package chat.ts.android.pinnedmessages.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.pinnedmessages.presentation.PinnedMessagesView
import chat.ts.android.pinnedmessages.ui.PinnedMessagesFragment
import dagger.Module
import dagger.Provides

@Module
class PinnedMessagesFragmentModule {

    @Provides
    @PerFragment
    fun providePinnedMessagesView(frag: PinnedMessagesFragment): PinnedMessagesView {
        return frag
    }
}