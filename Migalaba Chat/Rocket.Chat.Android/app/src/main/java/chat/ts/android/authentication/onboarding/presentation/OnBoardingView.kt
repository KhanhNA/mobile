package chat.ts.android.authentication.onboarding.presentation

import chat.ts.android.core.behaviours.LoadingView
import chat.ts.android.core.behaviours.MessageView

interface OnBoardingView : LoadingView, MessageView