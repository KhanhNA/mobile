package chat.ts.android.settings.password.presentation

import chat.ts.android.analytics.AnalyticsManager
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.helper.UserHelper
import chat.ts.android.server.domain.GetCurrentServerInteractor
import chat.ts.android.server.infrastructure.RocketChatClientFactory
import chat.ts.android.util.extension.launchUI
import chat.ts.android.util.retryIO
import chat.rocket.common.RocketChatException
import chat.rocket.core.RocketChatClient
import chat.rocket.core.internal.rest.updateProfile
import javax.inject.Inject

class PasswordPresenter @Inject constructor(
    private val view: PasswordView,
    private val strategy: CancelStrategy,
    private val analyticsManager: AnalyticsManager,
    private val userHelp: UserHelper,
    serverInteractor: GetCurrentServerInteractor,
    factory: RocketChatClientFactory
) {
    private val serverUrl = serverInteractor.get()!!
    private val client: RocketChatClient = factory.get(serverUrl)

    fun updatePassword(password: String) {
        launchUI(strategy) {
            try {
                view.showLoading()
                userHelp.user()?.id?.let { userId ->
                    retryIO("updateProfile()") {
                        client.updateProfile(userId, null, null, password, null)
                    }
                    analyticsManager.logResetPassword(true)
                    view.showPasswordSuccessfullyUpdatedMessage()
                }
            } catch (exception: RocketChatException) {
                analyticsManager.logResetPassword(false)
                exception.message?.let { errorMessage ->
                    view.showPasswordFailsUpdateMessage(errorMessage)
                }
            } finally {
                view.hideLoading()
            }
        }
    }
}