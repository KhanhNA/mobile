package chat.ts.android.directory.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.directory.ui.DirectoryFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DirectoryFragmentProvider {

    @ContributesAndroidInjector(modules = [DirectoryFragmentModule::class])
    @PerFragment
    abstract fun provideDirectoryFragment(): DirectoryFragment

}