package chat.ts.android.chatinformation.presentation

import chat.ts.android.chatinformation.viewmodel.ReadReceiptViewModel
import chat.ts.android.core.behaviours.LoadingView

interface MessageInfoView : LoadingView {

    fun showGenericErrorMessage()

    fun showReadReceipts(messageReceipts: List<ReadReceiptViewModel>)
}
