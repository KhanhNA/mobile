package chat.ts.android.core.behaviours

interface LoadingView {

    fun showLoading()

    fun hideLoading()
}