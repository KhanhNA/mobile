package chat.ts.android.sortingandgrouping.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.sortingandgrouping.ui.SortingAndGroupingBottomSheetFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SortingAndGroupingBottomSheetFragmentProvider {

    @ContributesAndroidInjector(modules = [SortingAndGroupingBottomSheetFragmentModule::class])
    @PerFragment
    abstract fun provideSortingAndGroupingBottomSheetFragment(): SortingAndGroupingBottomSheetFragment

}