package chat.ts.android.main.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.main.ui.MainFragment
import dagger.Module
import dagger.Provides

@Module
class MainFragmentModule {
    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: MainFragment): LifecycleOwner {
        return frag
    }
}