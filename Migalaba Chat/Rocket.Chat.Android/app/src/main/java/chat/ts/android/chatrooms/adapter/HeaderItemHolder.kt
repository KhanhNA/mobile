package chat.ts.android.chatrooms.adapter

data class HeaderItemHolder(override val data: String) : ItemHolder<String>