package chat.ts.android.settings.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.settings.presentation.SettingsView
import chat.ts.android.settings.ui.SettingsFragment
import dagger.Module
import dagger.Provides

@Module
class SettingsFragmentModule {

    @Provides
    @PerFragment
    fun settingsView(frag: SettingsFragment): SettingsView {
        return frag
    }
    @Provides
    @PerFragment
    fun settingsLifecycleOwner(fragment: SettingsFragment): LifecycleOwner {
        return fragment
    }
}
