package chat.ts.android.chatrooms.di

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import chat.ts.android.chatrooms.adapter.RoomUiModelMapper
import chat.ts.android.chatrooms.domain.FetchChatRoomsInteractor
import chat.ts.android.chatrooms.presentation.ChatRoomsView
import chat.ts.android.chatrooms.ui.ChatRoomsFragment
import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.db.ChatRoomDao
import chat.ts.android.db.DatabaseManager
import chat.ts.android.db.UserDao
import chat.ts.android.server.domain.GetCurrentUserInteractor
import chat.ts.android.server.domain.PermissionsInteractor
import chat.ts.android.server.domain.PublicSettings
import chat.ts.android.server.domain.SettingsRepository
import chat.ts.android.server.domain.TokenRepository
import chat.ts.android.server.infrastructure.ConnectionManager
import chat.ts.android.server.infrastructure.ConnectionManagerFactory
import chat.ts.android.server.infrastructure.RocketChatClientFactory
import chat.rocket.core.RocketChatClient
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class ChatRoomsFragmentModule {

    @Provides
    @PerFragment
    fun chatRoomsView(frag: ChatRoomsFragment): ChatRoomsView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ChatRoomsFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideRocketChatClient(
        factory: RocketChatClientFactory,
        @Named("currentServer") currentServer: String?
    ): RocketChatClient {
        return currentServer?.let { factory.get(it) }!!
    }

    @Provides
    @PerFragment
    fun provideChatRoomDao(manager: DatabaseManager): ChatRoomDao = manager.chatRoomDao()

    @Provides
    @PerFragment
    fun provideUserDao(manager: DatabaseManager): UserDao = manager.userDao()

    @Provides
    @PerFragment
    fun provideConnectionManager(
        factory: ConnectionManagerFactory,
        @Named("currentServer") currentServer: String?
    ): ConnectionManager {
        return currentServer?.let { factory.create(it) }!!
    }

    @Provides
    @PerFragment
    fun provideFetchChatRoomsInteractor(
        client: RocketChatClient,
        dbManager: DatabaseManager
    ): FetchChatRoomsInteractor {
        return FetchChatRoomsInteractor(client, dbManager)
    }

    @Provides
    @PerFragment
    fun providePublicSettings(
        repository: SettingsRepository,
        @Named("currentServer") currentServer: String?
    ): PublicSettings {
        return currentServer?.let { repository.get(it) }!!
    }

    @Provides
    @PerFragment
    fun provideRoomMapper(
        context: Application,
        settingsRepository: SettingsRepository,
        userInteractor: GetCurrentUserInteractor,
        tokenRepository: TokenRepository,
        @Named("currentServer") currentServer: String?,
        permissionsInteractor: PermissionsInteractor
    ): RoomUiModelMapper {
        return currentServer?.let {
            RoomUiModelMapper(
                context,
                settingsRepository.get(it),
                userInteractor,
                tokenRepository,
                it,
                permissionsInteractor
            )
        }!!
    }

    @Provides
    @PerFragment
    fun provideGetCurrentUserInteractor(
        tokenRepository: TokenRepository,
        @Named("currentServer") currentServer: String?,
        userDao: UserDao
    ): GetCurrentUserInteractor {
        return currentServer?.let { GetCurrentUserInteractor(tokenRepository, it, userDao) }!!
    }
}