package chat.ts.android.authentication.server.di

import chat.ts.android.authentication.server.ui.ServerFragment
import chat.ts.android.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServerFragmentProvider {

    @ContributesAndroidInjector(modules = [ServerFragmentModule::class])
    @PerFragment
    abstract fun provideServerFragment(): ServerFragment
}