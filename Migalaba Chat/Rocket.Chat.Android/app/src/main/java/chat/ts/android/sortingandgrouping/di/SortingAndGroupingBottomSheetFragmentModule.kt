package chat.ts.android.sortingandgrouping.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.sortingandgrouping.presentation.SortingAndGroupingView
import chat.ts.android.sortingandgrouping.ui.SortingAndGroupingBottomSheetFragment
import dagger.Module
import dagger.Provides

@Module
class SortingAndGroupingBottomSheetFragmentModule {

    @Provides
    @PerFragment
    fun sortingAndGroupingView(frag: SortingAndGroupingBottomSheetFragment): SortingAndGroupingView =
        frag
}