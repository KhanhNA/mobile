package chat.ts.android.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import chat.ts.android.db.model.AttachmentActionEntity
import chat.ts.android.db.model.AttachmentEntity
import chat.ts.android.db.model.AttachmentFieldEntity
import chat.ts.android.db.model.ChatRoomEntity
import chat.ts.android.db.model.MessageChannels
import chat.ts.android.db.model.MessageEntity
import chat.ts.android.db.model.MessageFavoritesRelation
import chat.ts.android.db.model.MessageMentionsRelation
import chat.ts.android.db.model.MessagesSync
import chat.ts.android.db.model.ReactionEntity
import chat.ts.android.db.model.UrlEntity
import chat.ts.android.db.model.UserEntity
import chat.ts.android.emoji.internal.db.StringListConverter

@Database(
    entities = [
        UserEntity::class,
        ChatRoomEntity::class,
        MessageEntity::class,
        MessageFavoritesRelation::class,
        MessageMentionsRelation::class,
        MessageChannels::class,
        AttachmentEntity::class,
        AttachmentFieldEntity::class,
        AttachmentActionEntity::class,
        UrlEntity::class,
        ReactionEntity::class,
        MessagesSync::class
    ],
    version = 15,
    exportSchema = true
)
@TypeConverters(StringListConverter::class)
abstract class RCDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun chatRoomDao(): ChatRoomDao
    abstract fun messageDao(): MessageDao
}
