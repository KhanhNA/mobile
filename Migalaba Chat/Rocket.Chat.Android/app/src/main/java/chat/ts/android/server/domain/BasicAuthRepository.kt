package chat.ts.android.server.domain

import chat.ts.android.server.domain.model.BasicAuth

interface BasicAuthRepository {
    fun save(basicAuth: BasicAuth)
    fun load(): List<BasicAuth>
}
