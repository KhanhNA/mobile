package chat.ts.android.authentication.ui

import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Bundle
import android.view.Window
import androidx.fragment.app.FragmentActivity
import chat.ts.android.util.extensions.isNotNullNorEmpty
import chat.ts.android.videoconference.ui.videoConferenceIntent
import kotlinx.android.synthetic.main.layout_incom_call.*
import android.media.Ringtone
import com.dropbox.core.v2.teamlog.ActorLogInfo.app
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T


class IncomingPopupActivity : FragmentActivity() {
    var r: Ringtone? = null
    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(chat.ts.android.R.layout.layout_incom_call)
        //
        try {
            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            r = RingtoneManager.getRingtone(applicationContext, notification)
            r?.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        //
        val roomId = intent.getStringExtra("roomId")
        val roomType = intent.getStringExtra("roomType")
        val title = intent.getStringExtra("title")
        if (title.isNotNullNorEmpty()) {
            text_caller_name.text = title
        }

        image_button_accept_call.setOnClickListener {
            if (roomId.isNotNullNorEmpty() && roomType.isNotNullNorEmpty()) {
                startActivity(videoConferenceIntent(roomId, roomType))
                r?.stop()
                finish()
            }

        }
        image_button_reject_call.setOnClickListener {
            if (roomId.isNotNullNorEmpty() && roomType.isNotNullNorEmpty()) {
                r?.stop()
                finish()
            }

        }
    }

}