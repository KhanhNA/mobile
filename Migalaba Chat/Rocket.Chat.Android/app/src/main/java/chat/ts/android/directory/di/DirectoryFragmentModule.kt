package chat.ts.android.directory.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.directory.presentation.DirectoryView
import chat.ts.android.directory.ui.DirectoryFragment
import dagger.Module
import dagger.Provides

@Module
class DirectoryFragmentModule {

    @Provides
    @PerFragment
    fun directoryView(frag: DirectoryFragment): DirectoryView = frag
}