package chat.ts.android.chatrooms.adapter

import android.annotation.SuppressLint
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.TextView
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import chat.rocket.common.model.RoomType
import chat.rocket.common.model.UserStatus
import chat.ts.android.R
import chat.ts.android.app.RocketChatApplication
import chat.ts.android.chatrooms.adapter.model.RoomUiModel
import chat.ts.android.util.extension.setTextViewAppearance
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_chat.view.*

class RoomViewHolder(itemView: View, private val listener: (RoomUiModel, View) -> Unit) :
        ViewHolder<RoomItemHolder>(itemView) {
    private val resources: Resources = itemView.resources
    private val channelIcon: Drawable = resources.getDrawable(R.drawable.ic_hashtag_12dp, null)
    private val groupIcon: Drawable = resources.getDrawable(R.drawable.ic_lock_12_dp, null)
    private val discussionIcon: Drawable = resources.getDrawable(R.drawable.ic_discussion_20dp, null)
    private val onlineIcon: Drawable = resources.getDrawable(R.drawable.ic_status_online_12dp, null)
    private val awayIcon: Drawable = resources.getDrawable(R.drawable.ic_status_away_12dp, null)
    private val busyIcon: Drawable = resources.getDrawable(R.drawable.ic_status_busy_12dp, null)
    private val offlineIcon: Drawable = resources.getDrawable(R.drawable.ic_status_invisible_12dp, null)

    @SuppressLint("SetTextI18n")
    override fun bindViews(data: RoomItemHolder) {
        val room = data.data
        with(itemView) {
            Glide.with(image_avatar.context)
                    .load(room.avatar)
                    .apply(RequestOptions.bitmapTransform(RoundedCorners(100)))
                    .into(image_avatar)

            text_chat_name.text = room.name

            if (room.isDiscussion) {
//                if(room.parentId == RocketChatApplication.parentId){
//                    itemView.visibility = View.GONE
//                }
                image_chat_icon.setImageDrawable(discussionIcon)
            } else if (room.status != null && room.type is RoomType.DirectMessage) {
                image_chat_icon.setImageDrawable(getStatusDrawable(room.status))
            } else {
                image_chat_icon.setImageDrawable(getRoomDrawable(room.type))
            }

            if (room.lastMessage != null) {
                text_last_message.text = room.lastMessage
                text_last_message.isVisible = true
            } else {
                text_last_message.isGone = true
            }

            if (room.date != null) {
                text_timestamp.text = room.date
                text_timestamp.isVisible = true
            } else {
                text_timestamp.isInvisible = true
            }

            if (room.alert) {
                val textTotalUnreadMessages = text_total_unread_messages as TextView
                if (room.unread == null) textTotalUnreadMessages.text = "!"
                if (room.unread != null) textTotalUnreadMessages.text = room.unread
                if (room.mentions) textTotalUnreadMessages.text = "@${room.unread}"
                text_chat_name.setTextViewAppearance(context, R.style.ChatList_ChatName_Unread_TextView)
                text_timestamp.setTextViewAppearance(context, R.style.ChatList_Timestamp_Unread_TextView)
                text_last_message.setTextViewAppearance(context, R.style.ChatList_LastMessage_Unread_TextView)
                textTotalUnreadMessages.isVisible = true
            } else {
                text_chat_name.setTextViewAppearance(context, R.style.ChatList_ChatName_TextView)
                text_timestamp.setTextViewAppearance(context, R.style.ChatList_Timestamp_TextView)
                text_last_message.setTextViewAppearance(context, R.style.ChatList_LastMessage_TextView)
                text_total_unread_messages.isInvisible = true
            }

            action_hide_conversation.setOnClickListener{listener(room, action_hide_conversation)}
            action_unread.setOnClickListener{listener(room, action_unread)}
            action_favorite.setOnClickListener{listener(room, action_favorite)}
            content.setOnClickListener { listener(room, content) }
        }
    }

    private fun getRoomDrawable(type: RoomType): Drawable? = when (type) {
        is RoomType.Channel -> channelIcon
        is RoomType.PrivateGroup -> groupIcon
        else -> null
    }

    private fun getStatusDrawable(status: UserStatus): Drawable = when (status) {
        is UserStatus.Online -> onlineIcon
        is UserStatus.Away -> awayIcon
        is UserStatus.Busy -> busyIcon
        else -> offlineIcon
    }
}