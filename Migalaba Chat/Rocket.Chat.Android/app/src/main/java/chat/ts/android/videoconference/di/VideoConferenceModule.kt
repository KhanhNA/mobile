package chat.ts.android.videoconference.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.dagger.scope.PerActivity
import chat.ts.android.videoconference.presenter.JitsiVideoConferenceView
import chat.ts.android.videoconference.ui.VideoConferenceActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class VideoConferenceModule {

    @Provides
    @PerActivity
    fun provideVideoConferenceView(activity: VideoConferenceActivity): JitsiVideoConferenceView {
        return activity
    }

    @Provides
    @PerActivity
    fun provideJob(): Job = Job()

    @Provides
    @PerActivity
    fun provideLifecycleOwner(activity: VideoConferenceActivity): LifecycleOwner = activity

    @Provides
    @PerActivity
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy =
        CancelStrategy(owner, jobs)
}