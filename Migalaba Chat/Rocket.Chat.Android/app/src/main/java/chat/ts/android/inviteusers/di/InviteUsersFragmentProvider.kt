package chat.ts.android.inviteusers.di

import chat.ts.android.dagger.scope.PerFragment
import chat.ts.android.inviteusers.ui.InviteUsersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class InviteUsersFragmentProvider {

    @ContributesAndroidInjector(modules = [InviteUsersFragmentModule::class])
    @PerFragment
    abstract fun provideInviteUsersFragment(): InviteUsersFragment
}