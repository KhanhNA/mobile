package chat.ts.android.chatinformation.di

import chat.ts.android.chatinformation.ui.MessageInfoFragment
import chat.ts.android.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MessageInfoFragmentProvider {

    @ContributesAndroidInjector(modules = [MessageInfoFragmentModule::class])
    @PerFragment
    abstract fun provideMessageInfoFragment(): MessageInfoFragment
}
