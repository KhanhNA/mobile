package chat.ts.android.authentication.loginoptions.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.authentication.loginoptions.presentation.LoginOptionsView
import chat.ts.android.authentication.loginoptions.ui.LoginOptionsFragment
import chat.ts.android.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class LoginOptionsFragmentModule {

    @Provides
    @PerFragment
    fun loginOptionsView(frag: LoginOptionsFragment): LoginOptionsView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: LoginOptionsFragment): LifecycleOwner = frag
}