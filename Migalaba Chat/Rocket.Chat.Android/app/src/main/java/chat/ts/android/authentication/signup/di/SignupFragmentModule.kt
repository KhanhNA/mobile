package chat.ts.android.authentication.signup.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.authentication.signup.presentation.SignupView
import chat.ts.android.authentication.signup.ui.SignupFragment
import chat.ts.android.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class SignupFragmentModule {

    @Provides
    @PerFragment
    fun signupView(frag: SignupFragment): SignupView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: SignupFragment): LifecycleOwner = frag
}