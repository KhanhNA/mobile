package chat.ts.android.push.di

import chat.ts.android.push.RocketChatMessagingService
import chat.ts.android.dagger.module.AppModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FirebaseMessagingServiceProvider {

    @ContributesAndroidInjector(modules = [AppModule::class])
    abstract fun provideRocketChatMessagingService(): RocketChatMessagingService
}