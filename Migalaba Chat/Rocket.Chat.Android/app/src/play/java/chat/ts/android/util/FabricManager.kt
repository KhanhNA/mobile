package chat.ts.android.util

import android.content.Context
import chat.ts.android.infrastructure.installCrashlyticsWrapper
import chat.ts.android.BuildConfig
import chat.ts.android.app.RocketChatApplication
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.core.CrashlyticsCore
import io.fabric.sdk.android.Fabric

fun setupFabric(context: Context) {
    val core = CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()
    Fabric.with(
            context,
            Crashlytics.Builder()
                    .core(core) // For Crashlytics
                    .answers(Answers()) // For Answers
                    .build()
    )

    installCrashlyticsWrapper(
            context as RocketChatApplication,
            context.getCurrentServerInteractor,
            context.settingsInteractor,
            context.accountRepository,
            context.localRepository
    )
}
