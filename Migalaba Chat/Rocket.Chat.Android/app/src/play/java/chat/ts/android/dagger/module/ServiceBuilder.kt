package chat.ts.android.dagger.module

import chat.ts.android.push.RocketChatMessagingService
import chat.ts.android.push.di.FirebaseMessagingServiceProvider
import chat.ts.android.chatroom.di.MessageServiceProvider
import chat.ts.android.chatroom.service.MessageService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuilder {

    @ContributesAndroidInjector(modules = [FirebaseMessagingServiceProvider::class])
    abstract fun bindRocketChatMessagingService(): RocketChatMessagingService

    @ContributesAndroidInjector(modules = [MessageServiceProvider::class])
    abstract fun bindMessageService(): MessageService
}