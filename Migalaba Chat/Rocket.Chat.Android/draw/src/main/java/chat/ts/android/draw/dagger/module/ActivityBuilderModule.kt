package chat.ts.android.draw.dagger.module

import chat.ts.android.draw.main.di.DrawModule
import chat.ts.android.draw.main.ui.DrawingActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = [DrawModule::class])
    abstract fun contributeDrawingActivityInjector(): DrawingActivity
}