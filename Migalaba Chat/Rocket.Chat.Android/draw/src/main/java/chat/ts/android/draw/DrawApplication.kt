package chat.ts.android.draw

import chat.ts.android.draw.dagger.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class DrawApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        chat.ts.android.draw.dagger.DaggerAppComponent.builder().create(this)
}