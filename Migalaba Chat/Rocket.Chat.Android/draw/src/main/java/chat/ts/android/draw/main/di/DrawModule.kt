package chat.ts.android.draw.main.di

import androidx.lifecycle.LifecycleOwner
import chat.ts.android.core.lifecycle.CancelStrategy
import chat.ts.android.draw.main.presenter.DrawView
import chat.ts.android.draw.main.ui.DrawingActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class DrawModule {

    @Provides
    fun provideMainView(activity: DrawingActivity): DrawView = activity

    @Provides
    fun provideJob(): Job = Job()

    @Provides
    fun provideLifecycleOwner(activity: DrawingActivity): LifecycleOwner = activity

    @Provides
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy =
        CancelStrategy(owner, jobs)
}