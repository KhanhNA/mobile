package chat.ts.android.suggestions.repository

interface LocalSuggestionProvider {
    fun find(prefix: String)
}