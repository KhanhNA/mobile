package chat.ts.android.util.extension

fun Boolean?.orFalse(): Boolean = this ?: false