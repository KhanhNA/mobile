package chat.rocket.mingalabar.draw.dagger.module

import chat.rocket.mingalabar.draw.main.di.DrawModule
import chat.rocket.mingalabar.draw.main.ui.DrawingActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = [DrawModule::class])
    abstract fun contributeDrawingActivityInjector(): DrawingActivity
}