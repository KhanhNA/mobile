package chat.rocket.mingalabar.chatgroups.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import chat.rocket.mingalabar.chatrooms.adapter.RoomUiModelMapper
import chat.rocket.mingalabar.chatgroups.domain.FetchChatGroupsInteractor
import chat.rocket.mingalabar.chatgroups.infrastructure.ChatRoomsRepository
import chat.rocket.mingalabar.server.infrastructure.ConnectionManager
import javax.inject.Inject

class ChatGroupsViewModelFactory @Inject constructor(
        private val connectionManager: ConnectionManager,
        private val interactor: FetchChatGroupsInteractor,
        private val repository: ChatRoomsRepository,
        private val mapper: RoomUiModelMapper
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
            ChatGroupsViewModel(connectionManager, interactor, repository, mapper) as T
}