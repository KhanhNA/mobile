package chat.rocket.mingalabar.authentication.presentation

interface AuthenticationView {
    fun showServerInput()
}