package chat.rocket.mingalabar.chatroom.di

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.chatroom.presentation.ChatRoomView
import chat.rocket.mingalabar.chatroom.ui.ChatRoomFragment
import chat.rocket.mingalabar.chatrooms.adapter.RoomUiModelMapper
import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.db.ChatRoomDao
import chat.rocket.mingalabar.db.DatabaseManager
import chat.rocket.mingalabar.db.UserDao
import chat.rocket.mingalabar.server.domain.GetCurrentUserInteractor
import chat.rocket.mingalabar.server.domain.PermissionsInteractor
import chat.rocket.mingalabar.server.domain.SettingsRepository
import chat.rocket.mingalabar.server.domain.TokenRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class ChatRoomFragmentModule {

    @Provides
    @PerFragment
    fun chatRoomView(frag: ChatRoomFragment): ChatRoomView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ChatRoomFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideChatRoomDao(manager: DatabaseManager): ChatRoomDao = manager.chatRoomDao()

    @Provides
    @PerFragment
    fun provideUserDao(manager: DatabaseManager): UserDao = manager.userDao()

    @Provides
    @PerFragment
    fun provideGetCurrentUserInteractor(
        tokenRepository: TokenRepository,
        @Named("currentServer") currentServer: String?,
        userDao: UserDao
    ): GetCurrentUserInteractor {
        return currentServer?.let { GetCurrentUserInteractor(tokenRepository, it, userDao) }!!
    }

    @Provides
    @PerFragment
    fun provideRoomMapper(
        context: Application,
        repository: SettingsRepository,
        userInteractor: GetCurrentUserInteractor,
        tokenRepository: TokenRepository,
        @Named("currentServer") currentServer: String?,
        permissionsInteractor: PermissionsInteractor
    ): RoomUiModelMapper {
        return currentServer?.let {
            RoomUiModelMapper(
                context,
                repository.get(it),
                userInteractor,
                tokenRepository,
                it,
                permissionsInteractor
            )
        }!!
    }
}
