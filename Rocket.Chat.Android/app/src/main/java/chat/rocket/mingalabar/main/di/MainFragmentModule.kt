package chat.rocket.mingalabar.main.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.main.ui.MainFragment
import dagger.Module
import dagger.Provides

@Module
class MainFragmentModule {
    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: MainFragment): LifecycleOwner {
        return frag
    }
}