package chat.rocket.mingalabar.chatdetails.di

import chat.rocket.mingalabar.chatdetails.presentation.ChatDetailsView
import chat.rocket.mingalabar.chatdetails.ui.ChatDetailsFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.db.ChatRoomDao
import chat.rocket.mingalabar.db.DatabaseManager
import dagger.Module
import dagger.Provides

@Module
class ChatDetailsFragmentModule {

    @Provides
    @PerFragment
    fun chatDetailsView(frag: ChatDetailsFragment): ChatDetailsView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideChatRoomDao(manager: DatabaseManager): ChatRoomDao = manager.chatRoomDao()
}