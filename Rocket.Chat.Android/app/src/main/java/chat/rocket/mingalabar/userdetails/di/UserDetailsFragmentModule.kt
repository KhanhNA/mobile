package chat.rocket.mingalabar.userdetails.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.userdetails.presentation.UserDetailsView
import chat.rocket.mingalabar.userdetails.ui.UserDetailsFragment
import dagger.Module
import dagger.Provides

@Module
class UserDetailsFragmentModule {

    @Provides
    @PerFragment
    fun provideUserDetailsView(frag: UserDetailsFragment): UserDetailsView = frag
}