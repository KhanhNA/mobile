package chat.rocket.mingalabar.inviteusers.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.inviteusers.ui.InviteUsersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class InviteUsersFragmentProvider {

    @ContributesAndroidInjector(modules = [InviteUsersFragmentModule::class])
    @PerFragment
    abstract fun provideInviteUsersFragment(): InviteUsersFragment
}