package chat.rocket.mingalabar.authentication.registerusername.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.authentication.registerusername.presentation.RegisterUsernameView
import chat.rocket.mingalabar.authentication.registerusername.ui.RegisterUsernameFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class RegisterUsernameFragmentModule {

    @Provides
    @PerFragment
    fun registerUsernameView(frag: RegisterUsernameFragment): RegisterUsernameView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: RegisterUsernameFragment): LifecycleOwner = frag
}