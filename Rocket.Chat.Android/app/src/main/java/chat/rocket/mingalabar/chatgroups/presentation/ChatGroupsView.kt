package chat.rocket.mingalabar.chatgroups.presentation

import chat.rocket.mingalabar.core.behaviours.LoadingView
import chat.rocket.mingalabar.core.behaviours.MessageView

interface ChatGroupsView : LoadingView, MessageView {

    fun setupSortingAndGrouping(
        isSortByName: Boolean,
        isUnreadOnTop: Boolean
    )
}