package chat.rocket.mingalabar.settings.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.settings.presentation.SettingsView
import chat.rocket.mingalabar.settings.ui.SettingsFragment
import dagger.Module
import dagger.Provides

@Module
class SettingsFragmentModule {

    @Provides
    @PerFragment
    fun settingsView(frag: SettingsFragment): SettingsView {
        return frag
    }
    @Provides
    @PerFragment
    fun settingsLifecycleOwner(fragment: SettingsFragment): LifecycleOwner {
        return fragment
    }
}
