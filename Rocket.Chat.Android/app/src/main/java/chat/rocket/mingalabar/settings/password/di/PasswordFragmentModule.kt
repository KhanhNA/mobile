package chat.rocket.mingalabar.settings.password.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.core.lifecycle.CancelStrategy
import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.settings.password.presentation.PasswordView
import chat.rocket.mingalabar.settings.password.ui.PasswordFragment
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class PasswordFragmentModule {

    @Provides
    @PerFragment
    fun provideJob(): Job = Job()

    @Provides
    @PerFragment
    fun passwordView(frag: PasswordFragment): PasswordView {
        return frag
    }

    @Provides
    @PerFragment
    fun settingsLifecycleOwner(frag: PasswordFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}
