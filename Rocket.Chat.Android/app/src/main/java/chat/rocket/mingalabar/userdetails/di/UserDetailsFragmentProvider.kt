package chat.rocket.mingalabar.userdetails.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.userdetails.ui.UserDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = [UserDetailsFragmentModule::class])
    @PerFragment
    abstract fun provideUserDetailsFragment(): UserDetailsFragment
}
