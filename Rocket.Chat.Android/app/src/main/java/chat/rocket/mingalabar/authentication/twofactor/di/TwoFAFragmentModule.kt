package chat.rocket.mingalabar.authentication.twofactor.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.authentication.twofactor.presentation.TwoFAView
import chat.rocket.mingalabar.authentication.twofactor.ui.TwoFAFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class TwoFAFragmentModule {

    @Provides
    @PerFragment
    fun loginView(frag: TwoFAFragment): TwoFAView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: TwoFAFragment): LifecycleOwner = frag
}
