package chat.rocket.mingalabar.profile.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.profile.presentation.ProfileView
import chat.rocket.mingalabar.profile.ui.ProfileFragment
import dagger.Module
import dagger.Provides

@Module
class ProfileFragmentModule {

    @Provides
    @PerFragment
    fun profileView(frag: ProfileFragment): ProfileView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ProfileFragment): LifecycleOwner {
        return frag
    }
}