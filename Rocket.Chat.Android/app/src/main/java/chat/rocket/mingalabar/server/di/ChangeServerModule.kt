package chat.rocket.mingalabar.server.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.core.lifecycle.CancelStrategy
import chat.rocket.mingalabar.dagger.scope.PerActivity
import chat.rocket.mingalabar.server.presentation.ChangeServerNavigator
import chat.rocket.mingalabar.server.presentation.ChangeServerView
import chat.rocket.mingalabar.server.ui.ChangeServerActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class ChangeServerModule {

    @Provides
    @PerActivity
    fun provideJob(): Job = Job()

    @Provides
    @PerActivity
    fun provideChangeServerNavigator(activity: ChangeServerActivity) = ChangeServerNavigator(activity)

    @Provides
    @PerActivity
    fun ChangeServerView(activity: ChangeServerActivity): ChangeServerView {
        return activity
    }

    @Provides
    fun provideLifecycleOwner(activity: ChangeServerActivity): LifecycleOwner = activity

    @Provides
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy = CancelStrategy(owner, jobs)
}