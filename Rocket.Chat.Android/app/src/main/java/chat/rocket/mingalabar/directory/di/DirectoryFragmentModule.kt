package chat.rocket.mingalabar.directory.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.directory.presentation.DirectoryView
import chat.rocket.mingalabar.directory.ui.DirectoryFragment
import dagger.Module
import dagger.Provides

@Module
class DirectoryFragmentModule {

    @Provides
    @PerFragment
    fun directoryView(frag: DirectoryFragment): DirectoryView = frag
}