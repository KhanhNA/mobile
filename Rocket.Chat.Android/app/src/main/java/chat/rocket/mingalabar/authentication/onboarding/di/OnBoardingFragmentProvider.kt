package chat.rocket.mingalabar.authentication.onboarding.di

import chat.rocket.mingalabar.authentication.onboarding.ui.OnBoardingFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class OnBoardingFragmentProvider {

    @ContributesAndroidInjector(modules = [OnBoardingFragmentModule::class])
    @PerFragment
    abstract fun provideOnBoardingFragment(): OnBoardingFragment
}