package chat.rocket.mingalabar.directory.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.directory.ui.DirectoryFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DirectoryFragmentProvider {

    @ContributesAndroidInjector(modules = [DirectoryFragmentModule::class])
    @PerFragment
    abstract fun provideDirectoryFragment(): DirectoryFragment

}