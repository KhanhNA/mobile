package chat.rocket.mingalabar.chatinformation.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.chatinformation.presentation.MessageInfoView
import chat.rocket.mingalabar.chatinformation.ui.MessageInfoFragment
import chat.rocket.mingalabar.core.lifecycle.CancelStrategy
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class MessageInfoFragmentModule {

    @Provides
    @PerFragment
    fun provideJob(): Job = Job()

    @Provides
    @PerFragment
    fun messageInfoView(frag: MessageInfoFragment): MessageInfoView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: MessageInfoFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}
