package chat.rocket.mingalabar.members.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.members.presentation.MembersView
import chat.rocket.mingalabar.members.ui.MembersFragment
import dagger.Module
import dagger.Provides

@Module
class MembersFragmentModule {

    @Provides
    @PerFragment
    fun membersView(frag: MembersFragment): MembersView {
        return frag
    }
}