package chat.rocket.mingalabar.chatroom.domain

data class MessageReply(val roomName: String, val permalink: String)