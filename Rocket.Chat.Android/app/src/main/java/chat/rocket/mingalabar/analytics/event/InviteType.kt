package chat.rocket.mingalabar.analytics.event

enum class InviteType(val inviteTypeName: String) {
    ViaApp("viaApp")
}