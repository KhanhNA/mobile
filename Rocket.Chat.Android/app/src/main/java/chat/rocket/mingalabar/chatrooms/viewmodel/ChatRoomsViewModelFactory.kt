package chat.rocket.mingalabar.chatrooms.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import chat.rocket.mingalabar.chatrooms.adapter.RoomUiModelMapper
import chat.rocket.mingalabar.chatrooms.domain.FetchChatRoomsInteractor
import chat.rocket.mingalabar.chatrooms.infrastructure.ChatRoomsRepository
import chat.rocket.mingalabar.server.infrastructure.ConnectionManager
import javax.inject.Inject

class ChatRoomsViewModelFactory @Inject constructor(
    private val connectionManager: ConnectionManager,
    private val interactor: FetchChatRoomsInteractor,
    private val repository: ChatRoomsRepository,
    private val mapper: RoomUiModelMapper
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
            ChatRoomsViewModel(connectionManager, interactor, repository, mapper) as T
}