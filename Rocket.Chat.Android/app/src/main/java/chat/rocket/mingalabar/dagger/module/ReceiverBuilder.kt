package chat.rocket.mingalabar.dagger.module

import chat.rocket.mingalabar.push.DeleteReceiver
import chat.rocket.mingalabar.push.di.DeleteReceiverProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ReceiverBuilder {

    @ContributesAndroidInjector(modules = [DeleteReceiverProvider::class])
    abstract fun bindDeleteReceiver(): DeleteReceiver
}