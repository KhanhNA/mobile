package chat.rocket.mingalabar.server.domain

import chat.rocket.mingalabar.authentication.domain.model.TokenModel

interface MultiServerTokenRepository {
    fun get(server: String): TokenModel?

    fun save(server: String, token: TokenModel)

    fun clear(server: String)
}