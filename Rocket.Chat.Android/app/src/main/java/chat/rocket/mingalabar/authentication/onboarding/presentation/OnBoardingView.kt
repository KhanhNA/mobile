package chat.rocket.mingalabar.authentication.onboarding.presentation

import chat.rocket.mingalabar.core.behaviours.LoadingView
import chat.rocket.mingalabar.core.behaviours.MessageView

interface OnBoardingView : LoadingView, MessageView