package chat.rocket.mingalabar.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import chat.rocket.mingalabar.db.model.AttachmentActionEntity
import chat.rocket.mingalabar.db.model.AttachmentEntity
import chat.rocket.mingalabar.db.model.AttachmentFieldEntity
import chat.rocket.mingalabar.db.model.ChatRoomEntity
import chat.rocket.mingalabar.db.model.MessageChannels
import chat.rocket.mingalabar.db.model.MessageEntity
import chat.rocket.mingalabar.db.model.MessageFavoritesRelation
import chat.rocket.mingalabar.db.model.MessageMentionsRelation
import chat.rocket.mingalabar.db.model.MessagesSync
import chat.rocket.mingalabar.db.model.ReactionEntity
import chat.rocket.mingalabar.db.model.UrlEntity
import chat.rocket.mingalabar.db.model.UserEntity
import chat.rocket.mingalabar.emoji.internal.db.StringListConverter

@Database(
    entities = [
        UserEntity::class,
        ChatRoomEntity::class,
        MessageEntity::class,
        MessageFavoritesRelation::class,
        MessageMentionsRelation::class,
        MessageChannels::class,
        AttachmentEntity::class,
        AttachmentFieldEntity::class,
        AttachmentActionEntity::class,
        UrlEntity::class,
        ReactionEntity::class,
        MessagesSync::class
    ],
    version = 15,
    exportSchema = true
)
@TypeConverters(StringListConverter::class)
abstract class RCDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun chatRoomDao(): ChatRoomDao
    abstract fun messageDao(): MessageDao
}
