package chat.rocket.mingalabar.authentication.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.authentication.presentation.AuthenticationNavigator
import chat.rocket.mingalabar.authentication.ui.AuthenticationActivity
import chat.rocket.mingalabar.chatrooms.presentation.ChatRoomsPresenter
import chat.rocket.mingalabar.core.lifecycle.CancelStrategy
import chat.rocket.mingalabar.dagger.scope.PerActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Job

@Module
class AuthenticationModule {

    @Provides
    @PerActivity
    fun provideAuthenticationNavigator(activity: AuthenticationActivity) =
        AuthenticationNavigator(activity)

    @Provides
    @PerActivity
    fun provideJob(): Job = Job()

    @Provides
    @PerActivity
    fun provideLifecycleOwner(activity: AuthenticationActivity): LifecycleOwner = activity

    @Provides
    @PerActivity
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy =
        CancelStrategy(owner, jobs)
}