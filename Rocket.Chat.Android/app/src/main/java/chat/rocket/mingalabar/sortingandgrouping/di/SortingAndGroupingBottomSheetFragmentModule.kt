package chat.rocket.mingalabar.sortingandgrouping.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.sortingandgrouping.presentation.SortingAndGroupingView
import chat.rocket.mingalabar.sortingandgrouping.ui.SortingAndGroupingBottomSheetFragment
import dagger.Module
import dagger.Provides

@Module
class SortingAndGroupingBottomSheetFragmentModule {

    @Provides
    @PerFragment
    fun sortingAndGroupingView(frag: SortingAndGroupingBottomSheetFragment): SortingAndGroupingView =
        frag
}