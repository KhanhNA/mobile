package chat.rocket.mingalabar.createchannel.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.createchannel.presentation.CreateChannelView
import chat.rocket.mingalabar.createchannel.ui.CreateChannelFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class CreateChannelModule {

    @Provides
    @PerFragment
    fun createChannelView(fragment: CreateChannelFragment): CreateChannelView {
        return fragment
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(fragment: CreateChannelFragment): LifecycleOwner {
        return fragment
    }
}