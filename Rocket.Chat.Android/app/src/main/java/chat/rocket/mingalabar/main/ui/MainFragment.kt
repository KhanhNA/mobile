package chat.rocket.mingalabar.main.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import chat.rocket.mingalabar.R
import chat.rocket.mingalabar.app.RocketChatApplication
import chat.rocket.mingalabar.authentication.domain.model.DeepLinkInfo
import chat.rocket.mingalabar.chatgroups.ui.ChatGroupsFragment
import chat.rocket.mingalabar.chatrooms.ui.ChatRoomsFragment
import chat.rocket.mingalabar.main.presentation.MainPresenter
import chat.rocket.mingalabar.profile.ui.ProfileFragment
import chat.rocket.mingalabar.util.extensions.ifNotNullNotEmpty
import chat.rocket.mingalabar.util.extensions.inflate
import chat.rocket.mingalabar.widget.BottomNavigationBehavior
import chat.rocket.mingalabar.widget.CustomViewPager
import com.ns.chat.views.activity.FriendPostsFragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_main_v2.*
import kotlinx.android.synthetic.main.app_bar_chat_rooms.*
import kotlinx.android.synthetic.main.conversation_actionbar.*
import javax.inject.Inject

internal const val TAG_MAIN_FRAGMENT = "MainFragment"
private const val BUNDLE_CHAT_ROOM_ID = "BUNDLE_CHAT_ROOM_ID"

fun newMainFragmentInstance(userName: String?, chatRoomId: String?, deepLinkInfo: DeepLinkInfo?): Fragment =
        MainFragment().apply {
            arguments = Bundle(1).apply {
                putString(BUNDLE_CHAT_ROOM_ID, chatRoomId)
                putString("userName", userName)
                putParcelable(
                        chat.rocket.mingalabar.authentication.domain.model.DEEP_LINK_INFO_KEY,
                        deepLinkInfo
                )
            }
        }

class MainFragment : Fragment() {
    @Inject
    lateinit var presenter: MainPresenter

    private var chatRoomId: String? = null
    private var userName: String? = null
    private var deepLinkInfo: DeepLinkInfo? = null

    private var prevItem: MenuItem? = null

    private var chats: ChatRoomsFragment? = null
    private var chatsGroup: ChatGroupsFragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)

        arguments?.run {
            chatRoomId = getString(BUNDLE_CHAT_ROOM_ID)
            userName = getString("userName")

            chatRoomId.ifNotNullNotEmpty {
                chatRoomId = null
            }
            deepLinkInfo =
                    getParcelable(chat.rocket.mingalabar.authentication.domain.model.DEEP_LINK_INFO_KEY)

        }


    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = container?.inflate(R.layout.activity_main_v2)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupListener()
        setupViewPager(frame_container)
    }

    private fun setupListener() {
        image_avatar.setOnClickListener { presenter.toSettings() }

    }

    private fun setupViewPager(viewPager: View) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        chats = chat.rocket.mingalabar.chatrooms.ui.newInstance(userName,chatRoomId, deepLinkInfo) as ChatRoomsFragment
        chatsGroup = chat.rocket.mingalabar.chatgroups.ui.newInstance(chatRoomId, deepLinkInfo) as ChatGroupsFragment
        val post = FriendPostsFragment()
        val bundle = Bundle()
        bundle.putString(com.ns.chat.views.activity.profile.ProfileFragment.USER_ID_EXTRA_KEY, RocketChatApplication.current_user_firebase_id)
        post.arguments = bundle
//        val profile = ProfileFragment()
        adapter.addFragment(chats)
        adapter.addFragment(chatsGroup)
        adapter.addFragment(post)
        (viewPager as ViewPager).adapter = adapter

        navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_chats -> viewPager.currentItem = 0
                R.id.navigation_chats_group -> viewPager.currentItem = 1
                R.id.navigation_profile -> viewPager.currentItem = 2
            }

            // attaching bottom sheet behaviour - hide / show on scroll
            val layoutParams = navigation.layoutParams as CoordinatorLayout.LayoutParams
            layoutParams.behavior = BottomNavigationBehavior()

            false
        }


        viewPager.offscreenPageLimit = 3
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                if (prevItem != null) prevItem?.isChecked = false else navigation.menu.getItem(0).isChecked = false
                navigation.menu.getItem(position).isChecked = true
                prevItem = navigation.menu.getItem(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })


    }

    fun setupToolbar() {
        with((activity as AppCompatActivity)) {
            setSupportActionBar(toolbar)
            val urlImg = presenter.getAvatarUser()
            image_avatar.setImageURI(urlImg)
            supportActionBar?.setDisplayShowHomeEnabled(false)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setHomeButtonEnabled(false)
        }
        text_server_name?.text = RocketChatApplication.account?.userName
    }


}