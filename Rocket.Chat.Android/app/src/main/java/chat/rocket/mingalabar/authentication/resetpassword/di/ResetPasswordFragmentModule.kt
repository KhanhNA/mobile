package chat.rocket.mingalabar.authentication.resetpassword.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.authentication.resetpassword.presentation.ResetPasswordView
import chat.rocket.mingalabar.authentication.resetpassword.ui.ResetPasswordFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class ResetPasswordFragmentModule {

    @Provides
    @PerFragment
    fun resetPasswordView(frag: ResetPasswordFragment): ResetPasswordView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ResetPasswordFragment): LifecycleOwner = frag
}