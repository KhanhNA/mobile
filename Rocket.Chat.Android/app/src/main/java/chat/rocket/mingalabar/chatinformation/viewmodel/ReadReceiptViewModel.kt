package chat.rocket.mingalabar.chatinformation.viewmodel

data class ReadReceiptViewModel(
    val avatar: String,
    val name: String,
    val time: String
)