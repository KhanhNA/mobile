package chat.rocket.mingalabar.chatinformation.presentation

import chat.rocket.mingalabar.chatroom.uimodel.UiModelMapper
import chat.rocket.mingalabar.core.lifecycle.CancelStrategy
import chat.rocket.mingalabar.server.domain.GetCurrentServerInteractor
import chat.rocket.mingalabar.server.infrastructure.ConnectionManagerFactory
import chat.rocket.mingalabar.util.extension.launchUI
import chat.rocket.common.RocketChatException
import chat.rocket.core.internal.rest.getMessageReadReceipts
import timber.log.Timber
import javax.inject.Inject

class MessageInfoPresenter @Inject constructor(
    private val view: MessageInfoView,
    private val strategy: CancelStrategy,
    private val mapper: UiModelMapper,
    serverInteractor: GetCurrentServerInteractor,
    factory: ConnectionManagerFactory
) {
    private val currentServer = serverInteractor.get()!!
    private val manager = factory.create(currentServer)
    private val client = manager?.client

    fun loadReadReceipts(messageId: String) {
        launchUI(strategy) {
            try {
                view.showLoading()
                client?.getMessageReadReceipts(messageId = messageId)?.result?.let { readReceipts ->
                    view.showReadReceipts(mapper.map(readReceipts))
                }
            } catch (ex: RocketChatException) {
                Timber.e(ex)
                view.showGenericErrorMessage()
            } finally {
                view.hideLoading()
            }
        }
    }
}
