package chat.rocket.mingalabar.chatrooms.adapter

import chat.rocket.mingalabar.chatrooms.adapter.model.RoomUiModel

data class RoomItemHolder(override val data: RoomUiModel) : ItemHolder<RoomUiModel>