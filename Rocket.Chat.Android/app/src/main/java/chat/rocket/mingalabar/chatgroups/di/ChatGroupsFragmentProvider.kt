package chat.rocket.mingalabar.chatgroups.di

import chat.rocket.mingalabar.chatgroups.ui.ChatGroupsFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ChatGroupsFragmentProvider {

    @ContributesAndroidInjector(modules = [ChatGroupsFragmentModule::class])
    @PerFragment
    abstract fun provideChatGroupsFragment(): ChatGroupsFragment
}