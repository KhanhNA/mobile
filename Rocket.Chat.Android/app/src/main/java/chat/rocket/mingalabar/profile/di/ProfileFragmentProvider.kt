package chat.rocket.mingalabar.profile.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.profile.ui.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProfileFragmentProvider {

    @ContributesAndroidInjector(modules = [ProfileFragmentModule::class])
    @PerFragment
    abstract fun provideProfileFragment(): ProfileFragment
}