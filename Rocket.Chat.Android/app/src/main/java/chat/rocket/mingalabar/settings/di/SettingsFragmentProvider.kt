package chat.rocket.mingalabar.settings.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.settings.ui.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SettingsFragmentProvider {

    @ContributesAndroidInjector(modules = [SettingsFragmentModule::class])
    @PerFragment
    abstract fun provideSettingsFragment(): SettingsFragment
}
