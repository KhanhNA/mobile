package chat.rocket.mingalabar.main.presentation

import android.content.Intent
import chat.rocket.mingalabar.R
import chat.rocket.mingalabar.authentication.domain.model.DeepLinkInfo
import chat.rocket.mingalabar.authentication.ui.newServerIntent
import chat.rocket.mingalabar.chatgroups.ui.newInstance
import chat.rocket.mingalabar.chatroom.ui.chatRoomIntent
import chat.rocket.mingalabar.chatrooms.ui.TAG_CHAT_ROOMS_FRAGMENT
import chat.rocket.mingalabar.createchannel.ui.TAG_CREATE_CHANNEL_FRAGMENT
import chat.rocket.mingalabar.directory.ui.TAG_DIRECTORY_FRAGMENT
import chat.rocket.mingalabar.main.ui.MainActivity
import chat.rocket.mingalabar.main.ui.TAG_MAIN_FRAGMENT
import chat.rocket.mingalabar.profile.ui.TAG_IMAGE_DIALOG_FRAGMENT
import chat.rocket.mingalabar.profile.ui.TAG_PROFILE_FRAGMENT
import chat.rocket.mingalabar.server.ui.changeServerIntent
import chat.rocket.mingalabar.settings.ui.TAG_SETTINGS_FRAGMENT
import chat.rocket.mingalabar.util.extensions.addFragment
import chat.rocket.mingalabar.util.extensions.addFragmentBackStack
import chat.rocket.mingalabar.webview.adminpanel.ui.TAG_ADMIN_PANEL_WEB_VIEW_FRAGMENT
import chat.rocket.mingalabar.webview.ui.webViewIntent

class MainNavigator(internal val activity: MainActivity) {

    fun toChatList(chatRoomId: String? = null, deepLinkInfo: DeepLinkInfo? = null) {
        activity.addFragment(TAG_CHAT_ROOMS_FRAGMENT, R.id.fragment_container) {
            newInstance(chatRoomId, deepLinkInfo)
        }
    }

    fun toMainFragment(userName: String?, chatRoomId: String? = null, deepLinkInfo: DeepLinkInfo? = null) {
        activity.addFragment(TAG_MAIN_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.main.ui.newMainFragmentInstance(userName, chatRoomId, deepLinkInfo)
        }
    }

    fun toSettings() {
        activity.addFragmentBackStack(TAG_SETTINGS_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.settings.ui.newInstance()
        }
    }

    fun toDirectory() {
        activity.addFragmentBackStack(TAG_DIRECTORY_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.directory.ui.newInstance()
        }
    }

    fun toCreateChannel() {
        activity.addFragmentBackStack(TAG_CREATE_CHANNEL_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.createchannel.ui.newInstance()
        }
    }

    fun toProfile() {
        activity.addFragmentBackStack(TAG_PROFILE_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.profile.ui.newInstance()
        }
    }

    fun toProfileImage(avatarUrl: String) {
        activity.addFragmentBackStack(TAG_IMAGE_DIALOG_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.profile.ui.newInstance(avatarUrl)
        }
    }

    fun toAdminPanel(webPageUrl: String, userToken: String) {
        activity.addFragmentBackStack(TAG_ADMIN_PANEL_WEB_VIEW_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.webview.adminpanel.ui.newInstance(webPageUrl, userToken)
        }
    }

    fun toLicense(licenseUrl: String, licenseTitle: String) {
        activity.startActivity(activity.webViewIntent(licenseUrl, licenseTitle))
    }

    fun toChatRoom(
            chatRoomId: String,
            chatRoomName: String,
            chatRoomType: String,
            isReadOnly: Boolean?,
            chatRoomLastSeen: Long,
            isSubscribed: Boolean?,
            isCreator: Boolean,
            isFavorite: Boolean
    ) {
        activity.startActivity(
                activity.chatRoomIntent(
                        chatRoomId,
                        chatRoomName,
                        chatRoomType,
                        isReadOnly,
                        chatRoomLastSeen,
                        isSubscribed,
                        isCreator,
                        isFavorite
                )
        )
        activity.overridePendingTransition(R.anim.open_enter, R.anim.open_exit)
    }

    /**
     * Switches to a server, given a [serverUrl] or adds a new server (navigating to the
     * AuthenticationActivity) if the user server list only contains one server and the
     * user logs out from this server.
     * NOTE: If the user has more than one server and logs out from the current server, then it will
     * switch to the first server in the server list.
     *
     * @param serverUrl The server URL to switch from, or null in case user logs out from the
     * current server.
     */
    fun switchOrAddNewServer(serverUrl: String? = null) {
        activity.startActivity(activity.changeServerIntent(serverUrl = serverUrl))
        activity.finish()
    }

    fun toServerScreen() {
        activity.startActivity(activity.newServerIntent())
    }

    fun recreateActivity() {
        activity.startActivity(Intent(activity, MainActivity::class.java))
        activity.finish()
    }
}