package chat.rocket.mingalabar.favoritemessages.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.favoritemessages.presentation.FavoriteMessagesView
import chat.rocket.mingalabar.favoritemessages.ui.FavoriteMessagesFragment
import dagger.Module
import dagger.Provides

@Module
class FavoriteMessagesFragmentModule {

    @Provides
    @PerFragment
    fun provideFavoriteMessagesView(frag: FavoriteMessagesFragment): FavoriteMessagesView {
        return frag
    }
}