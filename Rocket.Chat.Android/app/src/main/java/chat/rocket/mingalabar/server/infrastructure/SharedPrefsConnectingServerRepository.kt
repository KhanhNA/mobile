package chat.rocket.mingalabar.server.infrastructure

import android.content.SharedPreferences
import chat.rocket.mingalabar.app.RocketChatApplication
import chat.rocket.mingalabar.server.domain.CurrentServerRepository

class SharedPrefsConnectingServerRepository(private val preferences: SharedPreferences) : CurrentServerRepository {

    override fun save(url: String) {
        preferences.edit().putString(CONNECTING_SERVER_KEY, url).apply()
    }

    override fun get(): String? {
        return preferences.getString(CONNECTING_SERVER_KEY, RocketChatApplication.SERVER_URL)
    }

    companion object {
        private const val CONNECTING_SERVER_KEY = "connecting_server"
    }

    override fun clear() {
        preferences.edit().remove(CONNECTING_SERVER_KEY).apply()
    }
}