package chat.rocket.mingalabar.chatroom.uimodel.suggestion

import chat.rocket.mingalabar.suggestions.model.SuggestionModel

class CommandSuggestionUiModel(
    text: String,
    val description: String,
    searchList: List<String>
) : SuggestionModel(text, searchList)