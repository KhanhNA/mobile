package chat.rocket.mingalabar.authentication.login.di

import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.authentication.login.presentation.LoginView
import chat.rocket.mingalabar.authentication.login.ui.LoginFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class LoginFragmentModule {

    @Provides
    @PerFragment
    fun loginView(frag: LoginFragment): LoginView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: LoginFragment): LifecycleOwner = frag
}