package chat.rocket.mingalabar.server.domain

import chat.rocket.mingalabar.server.domain.model.Account
import javax.inject.Inject

class SaveAccountInteractor @Inject constructor(val repository: AccountsRepository) {
    fun save(account: Account) = repository.save(account)
}