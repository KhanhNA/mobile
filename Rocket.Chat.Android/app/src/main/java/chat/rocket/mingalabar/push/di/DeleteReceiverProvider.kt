package chat.rocket.mingalabar.push.di

import chat.rocket.mingalabar.dagger.module.AppModule
import chat.rocket.mingalabar.push.DeleteReceiver
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DeleteReceiverProvider {
    @ContributesAndroidInjector(modules = [AppModule::class])
    abstract fun provideDeleteReceiver(): DeleteReceiver
}