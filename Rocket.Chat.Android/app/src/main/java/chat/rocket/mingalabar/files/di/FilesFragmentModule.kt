package chat.rocket.mingalabar.files.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.files.presentation.FilesView
import chat.rocket.mingalabar.files.ui.FilesFragment
import dagger.Module
import dagger.Provides

@Module
class FilesFragmentModule {

    @Provides
    @PerFragment
    fun provideFilesView(frag: FilesFragment): FilesView {
        return frag
    }
}