package chat.rocket.mingalabar.main.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.main.ui.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentProvider {

    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    @PerFragment
    abstract fun provideMainFragment(): MainFragment
}