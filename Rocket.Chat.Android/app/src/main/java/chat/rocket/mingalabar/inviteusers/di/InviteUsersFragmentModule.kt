package chat.rocket.mingalabar.inviteusers.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.inviteusers.presentation.InviteUsersView
import chat.rocket.mingalabar.inviteusers.ui.InviteUsersFragment
import dagger.Module
import dagger.Provides

@Module
class InviteUsersFragmentModule {

    @Provides
    @PerFragment
    fun inviteUsersView(frag: InviteUsersFragment): InviteUsersView = frag
}