package chat.rocket.mingalabar.chatinformation.presentation

import chat.rocket.mingalabar.chatinformation.viewmodel.ReadReceiptViewModel
import chat.rocket.mingalabar.core.behaviours.LoadingView

interface MessageInfoView : LoadingView {

    fun showGenericErrorMessage()

    fun showReadReceipts(messageReceipts: List<ReadReceiptViewModel>)
}
