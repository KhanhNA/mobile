package chat.rocket.mingalabar.settings.password.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.settings.password.ui.PasswordFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PasswordFragmentProvider {
    @ContributesAndroidInjector(modules = [PasswordFragmentModule::class])
    @PerFragment
    abstract fun providePasswordFragment(): PasswordFragment
}