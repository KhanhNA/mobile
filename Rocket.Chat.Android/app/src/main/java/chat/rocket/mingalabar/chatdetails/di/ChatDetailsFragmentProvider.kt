package chat.rocket.mingalabar.chatdetails.di

import chat.rocket.mingalabar.chatdetails.ui.ChatDetailsFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ChatDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = [ChatDetailsFragmentModule::class])
    @PerFragment
    abstract fun providesChatDetailsFragment(): ChatDetailsFragment
}