package chat.rocket.mingalabar.servers.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.servers.presentation.ServersView
import chat.rocket.mingalabar.servers.ui.ServersBottomSheetFragment
import dagger.Module
import dagger.Provides

@Module
class ServersBottomSheetFragmentModule {

    @Provides
    @PerFragment
    fun serversView(frag: ServersBottomSheetFragment): ServersView = frag
}