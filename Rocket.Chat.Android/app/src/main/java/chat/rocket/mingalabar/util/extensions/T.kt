package chat.rocket.mingalabar.util.extensions

val <T> T.exhaustive: T
    get() = this