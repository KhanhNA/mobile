package chat.rocket.mingalabar.main.presentation

import chat.rocket.mingalabar.app.RocketChatApplication
import chat.rocket.mingalabar.authentication.domain.model.DeepLinkInfo
import chat.rocket.mingalabar.core.behaviours.AppLanguageView
import chat.rocket.mingalabar.core.lifecycle.CancelStrategy
import chat.rocket.mingalabar.helper.UserHelper
import chat.rocket.mingalabar.push.GroupedPush
import chat.rocket.mingalabar.push.retrieveCurrentPushNotificationToken
import chat.rocket.mingalabar.server.domain.GetCurrentLanguageInteractor
import chat.rocket.mingalabar.server.domain.GetSettingsInteractor
import chat.rocket.mingalabar.server.domain.RefreshPermissionsInteractor
import chat.rocket.mingalabar.server.domain.RefreshSettingsInteractor
import chat.rocket.mingalabar.server.domain.RemoveAccountInteractor
import chat.rocket.mingalabar.server.domain.SaveAccountInteractor
import chat.rocket.mingalabar.server.domain.TokenRepository
import chat.rocket.mingalabar.server.domain.favicon
import chat.rocket.mingalabar.server.domain.model.Account
import chat.rocket.mingalabar.server.domain.siteName
import chat.rocket.mingalabar.server.domain.wideTile
import chat.rocket.mingalabar.server.infrastructure.ConnectionManagerFactory
import chat.rocket.mingalabar.server.infrastructure.RocketChatClientFactory
import chat.rocket.mingalabar.util.extension.launchUI
import chat.rocket.mingalabar.util.extensions.avatarUrl
import chat.rocket.mingalabar.util.extensions.serverLogoUrl
import javax.inject.Inject
import javax.inject.Named

class MainPresenter @Inject constructor(
        @Named("currentServer") private val currentServer: String?,
        private val strategy: CancelStrategy,
        private val mainNavigator: MainNavigator,
        private val appLanguageView: AppLanguageView,
        private val refreshSettingsInteractor: RefreshSettingsInteractor,
        private val refreshPermissionsInteractor: RefreshPermissionsInteractor,
        private val getSettingsInteractor: GetSettingsInteractor,
        private val connectionManagerFactory: ConnectionManagerFactory,
        private var getLanguageInteractor: GetCurrentLanguageInteractor,
        private val groupedPush: GroupedPush,
        private val tokenRepository: TokenRepository,
        private val userHelper: UserHelper,
        private val saveAccountInteractor: SaveAccountInteractor,
        private val removeAccountInteractor: RemoveAccountInteractor,
        private val factory: RocketChatClientFactory
) {

    private val token = currentServer?.let { tokenRepository.get(it) }

    fun connect() = currentServer?.let {
        refreshSettingsInteractor.refreshAsync(it)
        refreshPermissionsInteractor.refreshAsync(it)
        connectionManagerFactory.create(it)?.connect()
    }

    fun clearNotificationsForChatRoom(chatRoomId: String?) {
        if (chatRoomId == null) return
        groupedPush.hostToPushMessageList[currentServer].let { list ->
            list?.removeAll { it.info.roomId == chatRoomId }
        }
    }

    fun getAvatarUser(): String? {
        val temp = RocketChatApplication.account
        return currentServer?.avatarUrl(temp?.userName!!, token?.userId, temp.authToken)
    }

    fun toSettings() = mainNavigator.toSettings()

    fun getAppLanguage() {
        with(getLanguageInteractor) {
            getLanguage()?.let { language ->
                appLanguageView.updateLanguage(language, getCountry())
            }
        }
    }

    fun removeOldAccount() = currentServer?.let {
        removeAccountInteractor.remove(currentServer)
    }

    fun saveNewAccount() {
        currentServer?.let { currentServer ->
            with(getSettingsInteractor.get(currentServer)) {
                val icon = favicon()?.let {
                    currentServer.serverLogoUrl(it)
                }
                val logo = wideTile()?.let {
                    currentServer.serverLogoUrl(it)
                }
                val token = tokenRepository.get(currentServer)
                val thumb = currentServer.avatarUrl(
                        userHelper.username() ?: "",
                        token?.userId,
                        token?.authToken
                )

                val account = Account(
                        serverName = siteName() ?: currentServer,
                        serverUrl = currentServer,
                        serverLogoUrl = icon,
                        serverBackgroundImageUrl = logo,
                        userName = userHelper.username() ?: "",
                        userAvatarUrl = thumb,
                        authToken = token?.authToken,
                        userId = token?.userId
                )

                saveAccountInteractor.save(account)
            }
        }
    }

    fun registerPushNotificationToken() = launchUI(strategy) {
        currentServer?.let { retrieveCurrentPushNotificationToken(factory.get(it)) }
    }

    fun initMainFragment(userName: String, chatRoomId: String? = null, deepLinkInfo: DeepLinkInfo? = null) =
            mainNavigator.toMainFragment(userName,chatRoomId, deepLinkInfo)
}
