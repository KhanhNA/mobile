package chat.rocket.mingalabar.authentication.signup.di

import chat.rocket.mingalabar.authentication.signup.ui.SignupFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SignupFragmentProvider {

    @ContributesAndroidInjector(modules = [SignupFragmentModule::class])
    @PerFragment
    abstract fun provideSignupFragment(): SignupFragment
}