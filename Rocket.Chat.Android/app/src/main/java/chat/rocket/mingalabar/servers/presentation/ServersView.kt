package chat.rocket.mingalabar.servers.presentation

import chat.rocket.mingalabar.core.behaviours.MessageView
import chat.rocket.mingalabar.server.domain.model.Account

interface ServersView : MessageView {

    /**
     * Shows the server list.
     *
     * @param serverList The list of server to show.
     * @param currentServerUrl The current logged in server url.
     */
    fun showServerList(serverList: List<Account>, currentServerUrl: String)

    /**
     * Hides the servers view.
     */
    fun hideServerView()
}