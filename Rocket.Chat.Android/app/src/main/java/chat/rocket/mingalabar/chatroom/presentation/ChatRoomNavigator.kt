package chat.rocket.mingalabar.chatroom.presentation

import chat.rocket.mingalabar.R
import chat.rocket.mingalabar.chatdetails.ui.TAG_CHAT_DETAILS_FRAGMENT
import chat.rocket.mingalabar.chatinformation.ui.messageInformationIntent
import chat.rocket.mingalabar.chatroom.ui.ChatRoomActivity
import chat.rocket.mingalabar.chatroom.ui.bottomsheet.WebUrlBottomSheet
import chat.rocket.mingalabar.chatroom.ui.chatRoomIntent
import chat.rocket.mingalabar.favoritemessages.ui.TAG_FAVORITE_MESSAGES_FRAGMENT
import chat.rocket.mingalabar.files.ui.TAG_FILES_FRAGMENT
import chat.rocket.mingalabar.inviteusers.ui.TAG_INVITE_USERS_FRAGMENT
import chat.rocket.mingalabar.members.ui.TAG_MEMBERS_FRAGMENT
import chat.rocket.mingalabar.mentions.ui.TAG_MENTIONS_FRAGMENT
import chat.rocket.mingalabar.pinnedmessages.ui.TAG_PINNED_MESSAGES_FRAGMENT
import chat.rocket.mingalabar.profile.ui.TAG_IMAGE_DIALOG_FRAGMENT
import chat.rocket.mingalabar.server.ui.changeServerIntent
import chat.rocket.mingalabar.userdetails.ui.TAG_USER_DETAILS_FRAGMENT
import chat.rocket.mingalabar.util.extensions.addFragmentBackStack
import chat.rocket.mingalabar.videoconference.ui.videoConferenceIntent
import chat.rocket.mingalabar.webview.ui.webViewIntent

class ChatRoomNavigator(internal val activity: ChatRoomActivity) {

    fun toUserDetails(userId: String, chatRoomId: String) {
        activity.addFragmentBackStack(TAG_USER_DETAILS_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.userdetails.ui.newInstance(userId, chatRoomId)
        }
    }

    fun toVideoConference(chatRoomId: String, chatRoomType: String) {
        activity.startActivity(activity.videoConferenceIntent(chatRoomId, chatRoomType))
    }

    fun toChatRoom(
        chatRoomId: String,
        chatRoomName: String,
        chatRoomType: String,
        isReadOnly: Boolean,
        chatRoomLastSeen: Long,
        isSubscribed: Boolean?,
        isCreator: Boolean,
        isFavorite: Boolean
    ) {
        activity.startActivity(
            activity.chatRoomIntent(
                chatRoomId,
                chatRoomName,
                chatRoomType,
                isReadOnly,
                chatRoomLastSeen,
                isSubscribed,
                isCreator,
                isFavorite
            )
        )
        activity.overridePendingTransition(R.anim.open_enter, R.anim.open_exit)
    }

    fun toChatDetails(
        chatRoomId: String,
        chatRoomType: String,
        isChatRoomSubscribed: Boolean,
        isChatRoomFavorite: Boolean,
        isMenuDisabled: Boolean
    ) {
        activity.addFragmentBackStack(TAG_CHAT_DETAILS_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.chatdetails.ui.newInstance(
                chatRoomId,
                chatRoomType,
                isChatRoomSubscribed,
                isChatRoomFavorite,
                isMenuDisabled
            )
        }
    }

    fun toMembersList(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_MEMBERS_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.members.ui.newInstance(chatRoomId)
        }
    }

    fun toInviteUsers(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_INVITE_USERS_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.inviteusers.ui.newInstance(chatRoomId)
        }
    }

    fun toMemberDetails(userId: String, chatRoomId: String) {
        activity.addFragmentBackStack(TAG_USER_DETAILS_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.userdetails.ui.newInstance(userId, chatRoomId)
        }
    }

    fun toMentions(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_MENTIONS_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.mentions.ui.newInstance(chatRoomId)
        }
    }

    fun toPinnedMessageList(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_PINNED_MESSAGES_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.pinnedmessages.ui.newInstance(chatRoomId)
        }
    }

    fun toFavoriteMessageList(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_FAVORITE_MESSAGES_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.favoritemessages.ui.newInstance(chatRoomId)
        }
    }

    fun toFileList(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_FILES_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.files.ui.newInstance(chatRoomId)
        }
    }

    fun toNewServer() {
        activity.startActivity(activity.changeServerIntent())
        activity.finish()
    }

    fun toDirectMessage(
        chatRoomId: String,
        chatRoomName: String,
        chatRoomType: String,
        isChatRoomReadOnly: Boolean,
        chatRoomLastSeen: Long,
        isChatRoomSubscribed: Boolean?,
        isChatRoomCreator: Boolean,
        isChatRoomFavorite: Boolean,
        chatRoomMessage: String
    ) {
        activity.startActivity(
            activity.chatRoomIntent(
                chatRoomId,
                chatRoomName,
                chatRoomType,
                isChatRoomReadOnly,
                chatRoomLastSeen,
                isChatRoomSubscribed,
                isChatRoomCreator,
                isChatRoomFavorite,
                chatRoomMessage
            )
        )
        activity.overridePendingTransition(R.anim.open_enter, R.anim.open_exit)
    }

    fun toMessageInformation(messageId: String) {
        activity.startActivity(activity.messageInformationIntent(messageId = messageId))
        activity.overridePendingTransition(R.anim.open_enter, R.anim.open_exit)
    }

    fun toProfileImage(avatarUrl: String) {
        activity.addFragmentBackStack(TAG_IMAGE_DIALOG_FRAGMENT, R.id.fragment_container) {
            chat.rocket.mingalabar.profile.ui.newInstance(avatarUrl)
        }
    }

    fun toFullWebPage(roomId: String, url: String) {
        activity.startActivity(activity.webViewIntent(url,null))
        activity.overridePendingTransition(R.anim.open_enter, R.anim.open_exit)
    }

    fun toConfigurableWebPage(roomId: String, url: String, heightRatio: String) {
        val weburlbottomsheet = WebUrlBottomSheet.newInstance(url, roomId, heightRatio)
        weburlbottomsheet.show(activity.supportFragmentManager, null)
    }
}
