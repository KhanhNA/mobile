package chat.rocket.mingalabar.servers.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.servers.ui.ServersBottomSheetFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServersBottomSheetFragmentProvider {

    @ContributesAndroidInjector(modules = [ServersBottomSheetFragmentModule::class])
    @PerFragment
    abstract fun provideServersBottomSheetFragment(): ServersBottomSheetFragment
}