package chat.rocket.mingalabar.webview.adminpanel.di

import chat.rocket.mingalabar.webview.adminpanel.ui.AdminPanelWebViewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AdminPanelWebViewFragmentProvider {

    @ContributesAndroidInjector
    abstract fun provideAdminPanelWebViewFragment(): AdminPanelWebViewFragment
}