package chat.rocket.mingalabar.pinnedmessages.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.pinnedmessages.presentation.PinnedMessagesView
import chat.rocket.mingalabar.pinnedmessages.ui.PinnedMessagesFragment
import dagger.Module
import dagger.Provides

@Module
class PinnedMessagesFragmentModule {

    @Provides
    @PerFragment
    fun providePinnedMessagesView(frag: PinnedMessagesFragment): PinnedMessagesView {
        return frag
    }
}