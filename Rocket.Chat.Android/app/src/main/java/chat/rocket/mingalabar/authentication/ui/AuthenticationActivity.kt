package chat.rocket.mingalabar.authentication.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import chat.rocket.mingalabar.R
import chat.rocket.mingalabar.authentication.domain.model.DeepLinkInfo
import chat.rocket.mingalabar.authentication.presentation.AuthenticationPresenter
import chat.rocket.mingalabar.dynamiclinks.DynamicLinksForFirebase
import chat.rocket.mingalabar.util.extensions.getDeepLinkInfo
import chat.rocket.mingalabar.util.extensions.isDynamicLink
import chat.rocket.mingalabar.util.extensions.isSupportedLink
import chat.rocket.common.util.ifNull
import chat.rocket.mingalabar.app.RocketChatApplication
import chat.rocket.mingalabar.app.RocketChatApplication.Companion.toUserName
import chat.rocket.mingalabar.chatrooms.presentation.ChatRoomsPresenter
import chat.rocket.mingalabar.main.ui.MainActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class AuthenticationActivity : AppCompatActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var presenter: AuthenticationPresenter

    @Inject
    lateinit var dynamicLinksManager: DynamicLinksForFirebase

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        processIncomingIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let {
            //            processIncomingIntent(it)
        }
    }

    private fun processIncomingIntent(intent: Intent) {
        if (intent.hasExtra("chat_room_name")) {
            toUserName = intent.getStringExtra("chat_room_name")
            intent.removeExtra("chat_room_name")
        }
        if (intent.isSupportedLink(this)) {
            intent.data?.let { uri ->
                if (uri.isDynamicLink(this)) {
                    resolveDynamicLink(intent)
                } else {
                    uri.getDeepLinkInfo(baseContext)?.let {
                        routeDeepLink(it)
                    }.ifNull {
                        loadCredentials()
                    }
                }
            }
        } else {
            loadCredentials()
        }
    }

    private fun resolveDynamicLink(intent: Intent) {
        val deepLinkCallback = { returnedUri: Uri? ->
            returnedUri?.let {
                returnedUri.getDeepLinkInfo(baseContext)?.let {
                    routeDeepLink(it)
                }
            }.ifNull {
                loadCredentials()
            }
        }
        dynamicLinksManager.getDynamicLink(intent, deepLinkCallback)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val currentFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        currentFragment?.onActivityResult(requestCode, resultCode, data)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> =
            fragmentDispatchingAndroidInjector

    private fun routeDeepLink(deepLinkInfo: DeepLinkInfo) {
        presenter.loadCredentials(false) { isAuthenticated ->
            if (isAuthenticated) {
                showChatList(deepLinkInfo)
            } else {
                presenter.saveDeepLinkInfo(deepLinkInfo)
                if (getString(R.string.server_url).isEmpty()) {
                    presenter.toOnBoarding()
                } else {
                    presenter.toSignInToYourServer()
                }
            }
        }
    }

    private fun loadCredentials() {
        val newServer = intent.getBooleanExtra(INTENT_ADD_NEW_SERVER, false)
        presenter.loadCredentials(newServer) { isAuthenticated ->
            when {
                isAuthenticated -> {
                    if (intent.hasExtra("chat_room_name")) {
                        val name = intent.getStringExtra("chat_room_name")
                        intent.removeExtra("chat_room_name")
//                        showChatRoom(name)
                        val intent = Intent(this, MainActivity::class.java)
                        // To pass any data to next activity
                        intent.putExtra("user_name", toUserName)
                        // start your next activity
                        startActivity(intent)
                    } else {
                        showChatList()
                    }
                }
                getString(R.string.server_url).isEmpty() -> presenter.toOnBoarding()
                else -> presenter.toSignInToYourServer()
            }
        }
    }

    private fun showChatList() = presenter.toChatList()

    private fun showChatRoom(name: String) = presenter.loadChatRoom(name)


    private fun showChatList(deepLinkInfo: DeepLinkInfo) = presenter.toChatList(deepLinkInfo)

}

const val INTENT_ADD_NEW_SERVER = "INTENT_ADD_NEW_SERVER"

fun Context.newServerIntent(): Intent {
    return Intent(this, AuthenticationActivity::class.java).apply {
        putExtra(INTENT_ADD_NEW_SERVER, true)
        flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
    }
}
