package chat.rocket.mingalabar.sortingandgrouping.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.sortingandgrouping.ui.SortingAndGroupingBottomSheetFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SortingAndGroupingBottomSheetFragmentProvider {

    @ContributesAndroidInjector(modules = [SortingAndGroupingBottomSheetFragmentModule::class])
    @PerFragment
    abstract fun provideSortingAndGroupingBottomSheetFragment(): SortingAndGroupingBottomSheetFragment

}