package chat.rocket.mingalabar.authentication.resetpassword.presentation

import chat.rocket.mingalabar.authentication.presentation.AuthenticationNavigator
import chat.rocket.mingalabar.core.lifecycle.CancelStrategy
import chat.rocket.mingalabar.server.domain.GetConnectingServerInteractor
import chat.rocket.mingalabar.server.infrastructure.RocketChatClientFactory
import chat.rocket.mingalabar.util.extension.launchUI
import chat.rocket.mingalabar.util.retryIO
import chat.rocket.common.RocketChatException
import chat.rocket.common.RocketChatInvalidResponseException
import chat.rocket.common.util.ifNull
import chat.rocket.core.RocketChatClient
import chat.rocket.core.internal.rest.forgotPassword
import javax.inject.Inject

class ResetPasswordPresenter @Inject constructor(
    private val view: ResetPasswordView,
    private val strategy: CancelStrategy,
    private val navigator: AuthenticationNavigator,
    factory: RocketChatClientFactory,
    serverInteractor: GetConnectingServerInteractor
) {
    private val currentServer = serverInteractor.get()!!
    private val client: RocketChatClient = factory.get(currentServer)

    fun resetPassword(email: String) {
        launchUI(strategy) {
            view.showLoading()
            try {
                retryIO("forgotPassword(email = $email)") {
                    client.forgotPassword(email)
                }
                navigator.toPreviousView()
                view.emailSent()
            } catch (exception: RocketChatException) {
                if (exception is RocketChatInvalidResponseException) {
                    view.updateYourServerVersion()
                } else {
                    exception.message?.let {
                        view.showMessage(it)
                    }.ifNull {
                        view.showGenericErrorMessage()
                    }
                }
            } finally {
                view.hideLoading()
            }
        }
    }
}