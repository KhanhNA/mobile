package chat.rocket.mingalabar.servers.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class AddNewServerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)