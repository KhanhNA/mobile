package chat.rocket.mingalabar.authentication.presentation

import chat.rocket.core.internal.rest.createDirectMessage
import chat.rocket.mingalabar.app.RocketChatApplication
import chat.rocket.mingalabar.authentication.domain.model.DeepLinkInfo
import chat.rocket.mingalabar.core.lifecycle.CancelStrategy
import chat.rocket.mingalabar.infrastructure.LocalRepository
import chat.rocket.mingalabar.server.domain.*
import chat.rocket.mingalabar.server.infrastructure.ConnectionManager
import chat.rocket.mingalabar.util.extension.launchUI
import chat.rocket.mingalabar.util.extensions.privacyPolicyUrl
import chat.rocket.mingalabar.util.extensions.termsOfServiceUrl
import javax.inject.Inject

class AuthenticationPresenter @Inject constructor(
        private val strategy: CancelStrategy,
        private val navigator: AuthenticationNavigator,
        private val getCurrentServerInteractor: GetCurrentServerInteractor,
        private val getAccountInteractor: GetAccountInteractor,
        private val settingsRepository: SettingsRepository,
        private val localRepository: LocalRepository,
        private val tokenRepository: TokenRepository,
        private val serverInteractor: GetConnectingServerInteractor
        ) {

    fun loadCredentials(newServer: Boolean, callback: (isAuthenticated: Boolean) -> Unit) {
        launchUI(strategy) {
            val currentServer = getCurrentServerInteractor.get()
            val serverToken = currentServer?.let { tokenRepository.get(currentServer) }
            val settings = currentServer?.let { settingsRepository.get(currentServer) }
            val account = currentServer?.let { getAccountInteractor.get(currentServer) }

            account?.let {
                localRepository.save(LocalRepository.CURRENT_USERNAME_KEY, account.userName)

            }

            if (newServer || currentServer == null ||
                    serverToken == null ||
                    settings == null ||
                    account?.userName == null
            ) {
                callback(false)
            } else {
                RocketChatApplication.account = account
                callback(true)
            }
        }
    }


    fun termsOfService(toolbarTitle: String) =
            serverInteractor.get()?.let { navigator.toWebPage(it.termsOfServiceUrl(), toolbarTitle) }

    fun privacyPolicy(toolbarTitle: String) =
            serverInteractor.get()?.let { navigator.toWebPage(it.privacyPolicyUrl(), toolbarTitle) }

    fun toOnBoarding() = navigator.toOnBoarding()

    fun toSignInToYourServer(deepLinkInfo: DeepLinkInfo? = null) =
            navigator.toSignInToYourServer(deepLinkInfo, false)

    fun saveDeepLinkInfo(deepLinkInfo: DeepLinkInfo) = navigator.saveDeepLinkInfo(deepLinkInfo)

    fun toChatList() = navigator.toChatList()

    fun loadChatRoom(name: String){

        navigator.toChatRoom("Ehnp7iDSW66iMfKd5FtStYQAkBfJG7JWKA", "KBN8cs4GhZbnTvS5w", "d",
                false,1578474663207, isSubscribed = false, isCreator = false, isFavorite = false)
    }

    fun toChatList(deepLinkInfo: DeepLinkInfo) = navigator.toChatList(deepLinkInfo)


}
