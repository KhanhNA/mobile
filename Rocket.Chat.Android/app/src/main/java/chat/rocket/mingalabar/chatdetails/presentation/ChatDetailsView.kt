package chat.rocket.mingalabar.chatdetails.presentation

import chat.rocket.mingalabar.chatdetails.domain.ChatDetails
import chat.rocket.mingalabar.core.behaviours.MessageView

interface ChatDetailsView: MessageView {

    /**
     * Shows the corresponding favorite icon for a favorite or non-favorite chat room.
     *
     * @param isFavorite True if a chat room is favorite, false otherwise.
     */
    fun showFavoriteIcon(isFavorite: Boolean)

    /**
     * Shows the details of a chat room.
     */
    fun displayDetails(room: ChatDetails)
}