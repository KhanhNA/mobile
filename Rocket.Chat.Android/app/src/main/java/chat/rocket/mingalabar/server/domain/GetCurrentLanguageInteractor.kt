package chat.rocket.mingalabar.server.domain

import chat.rocket.mingalabar.server.infrastructure.CurrentLanguageRepository
import javax.inject.Inject

class GetCurrentLanguageInteractor @Inject constructor(
    private val repository: CurrentLanguageRepository
) {

    fun getLanguage(): String? = repository.getLanguage()
    fun getCountry(): String? = repository.getCountry()
}