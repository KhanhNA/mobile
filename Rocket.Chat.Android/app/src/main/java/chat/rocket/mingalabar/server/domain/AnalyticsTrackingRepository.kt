package chat.rocket.mingalabar.server.domain

interface AnalyticsTrackingRepository {
    fun save(isAnalyticsTrackingEnable: Boolean)
    fun get(): Boolean
}