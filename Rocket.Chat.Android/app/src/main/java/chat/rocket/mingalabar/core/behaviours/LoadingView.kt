package chat.rocket.mingalabar.core.behaviours

interface LoadingView {

    fun showLoading()

    fun hideLoading()
}