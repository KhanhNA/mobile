package chat.rocket.mingalabar.mentions.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.mentions.presentention.MentionsView
import chat.rocket.mingalabar.mentions.ui.MentionsFragment
import dagger.Module
import dagger.Provides

@Module
class MentionsFragmentModule {

    @Provides
    @PerFragment
    fun provideMentionsView(frag: MentionsFragment): MentionsView {
        return frag
    }
}