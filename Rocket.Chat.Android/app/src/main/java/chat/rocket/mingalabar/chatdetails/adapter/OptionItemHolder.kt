package chat.rocket.mingalabar.chatdetails.adapter

import chat.rocket.mingalabar.chatdetails.domain.Option
import chat.rocket.mingalabar.chatrooms.adapter.ItemHolder

data class OptionItemHolder(override val data: Option): ItemHolder<Option>