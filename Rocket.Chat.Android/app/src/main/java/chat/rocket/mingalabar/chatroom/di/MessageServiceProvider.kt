package chat.rocket.mingalabar.chatroom.di

import chat.rocket.mingalabar.chatroom.service.MessageService
import chat.rocket.mingalabar.dagger.module.AppModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class MessageServiceProvider {

    @ContributesAndroidInjector(modules = [AppModule::class])
    abstract fun provideMessageService(): MessageService
}