package chat.rocket.mingalabar.chatdetails.domain

data class ChatDetails(
    val name: String?,
    val fullName: String?,
    val type: String?,
    val topic: String?,
    val announcement: String?,
    val description: String?
)