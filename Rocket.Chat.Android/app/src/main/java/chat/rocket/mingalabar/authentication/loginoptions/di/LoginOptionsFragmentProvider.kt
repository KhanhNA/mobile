package chat.rocket.mingalabar.authentication.loginoptions.di

import chat.rocket.mingalabar.authentication.loginoptions.ui.LoginOptionsFragment
import chat.rocket.mingalabar.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class LoginOptionsFragmentProvider {

    @ContributesAndroidInjector(modules = [LoginOptionsFragmentModule::class])
    @PerFragment
    abstract fun providesLoginOptionFragment(): LoginOptionsFragment
}