package chat.rocket.mingalabar.chatroom.uimodel.suggestion

import chat.rocket.mingalabar.suggestions.model.SuggestionModel

class ChatRoomSuggestionUiModel(
    text: String,
    val fullName: String,
    val name: String,
    searchList: List<String>
) : SuggestionModel(text, searchList, false)
