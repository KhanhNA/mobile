package chat.rocket.mingalabar.app

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.work.Worker
import chat.rocket.mingalabar.BuildConfig
import chat.rocket.mingalabar.dagger.DaggerAppComponent
import chat.rocket.mingalabar.dagger.injector.HasWorkerInjector
import chat.rocket.mingalabar.dagger.qualifier.ForMessages
import chat.rocket.mingalabar.emoji.Emoji
import chat.rocket.mingalabar.emoji.EmojiRepository
import chat.rocket.mingalabar.emoji.Fitzpatrick
import chat.rocket.mingalabar.emoji.internal.EmojiCategory
import chat.rocket.mingalabar.helper.CrashlyticsTree
import chat.rocket.mingalabar.infrastructure.LocalRepository
import chat.rocket.mingalabar.server.domain.AccountsRepository
import chat.rocket.mingalabar.server.domain.GetCurrentServerInteractor
import chat.rocket.mingalabar.server.domain.GetSettingsInteractor
import chat.rocket.mingalabar.server.domain.TokenRepository
import chat.rocket.mingalabar.server.domain.model.Account
import chat.rocket.mingalabar.server.infrastructure.RocketChatClientFactory
import chat.rocket.mingalabar.util.retryIO
import chat.rocket.mingalabar.util.setupFabric
import chat.rocket.common.RocketChatException
import chat.rocket.core.internal.rest.getCustomEmojis
import com.facebook.drawee.backends.pipeline.DraweeConfig
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.jakewharton.threetenabp.AndroidThreeTen
import com.ns.chat.application.ChatApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasBroadcastReceiverInjector
import dagger.android.HasServiceInjector
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

class RocketChatApplication : Application(), HasActivityInjector, HasServiceInjector,
        HasBroadcastReceiverInjector, HasWorkerInjector {


    companion object {
        var context: WeakReference<Context>? = null
        var SERVER_URL: String = "https://mstore.rocket.chat/"
        var toUserName: String = ""
        var account: Account? = null
        var current_user_firebase_id: String = ""
    }

    @Inject
    lateinit var appLifecycleObserver: AppLifecycleObserver

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var serviceDispatchingAndroidInjector: DispatchingAndroidInjector<Service>

    @Inject
    lateinit var broadcastReceiverInjector: DispatchingAndroidInjector<BroadcastReceiver>

    @Inject
    lateinit var workerInjector: DispatchingAndroidInjector<Worker>

    @Inject
    lateinit var imagePipelineConfig: ImagePipelineConfig
    @Inject
    lateinit var draweeConfig: DraweeConfig

    // TODO - remove this from here when we have a proper service handling the connection.
    @Inject
    lateinit var getCurrentServerInteractor: GetCurrentServerInteractor
    @Inject
    lateinit var settingsInteractor: GetSettingsInteractor
    @Inject
    lateinit var tokenRepository: TokenRepository
    @Inject
    lateinit var localRepository: LocalRepository
    @Inject
    lateinit var accountRepository: AccountsRepository
    @Inject
    lateinit var factory: RocketChatClientFactory

    @Inject
    @field:ForMessages
    lateinit var messagesPrefs: SharedPreferences


    override fun onCreate() {
        super.onCreate()
        loginPost()
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)

        ProcessLifecycleOwner.get()
                .lifecycle
                .addObserver(appLifecycleObserver)

        context = WeakReference(applicationContext)

        AndroidThreeTen.init(this)

        setupFabric(this)
        setupFresco()
        setupTimber()

        // TODO - FIXME - we need to properly inject and initialize the EmojiRepository
        loadEmojis()

    }



    override fun activityInjector() = activityDispatchingAndroidInjector

    override fun serviceInjector() = serviceDispatchingAndroidInjector

    override fun broadcastReceiverInjector() = broadcastReceiverInjector

    override fun workerInjector() = workerInjector


    private fun setupFresco() = Fresco.initialize(this, imagePipelineConfig, draweeConfig)

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashlyticsTree())
        }
    }


    // TODO - FIXME - This is a big Workaround
    /**
     * Load all emojis for the current server. Simple emojis are always the same for every server,
     * but custom emojis vary according to the its url.
     */
    fun loadEmojis() {
        EmojiRepository.init(this)
        runBlocking {
            getCurrentServerInteractor.get()?.let { server ->
                EmojiRepository.setCurrentServerUrl(server)
                val customEmojis = retryIO("getCustomEmojis()") {
                    factory.get(server).getCustomEmojis().update
                }
                val customEmojiList = mutableListOf<Emoji>()
                try {
                    if (customEmojis != null) {
                        for (customEmoji in customEmojis) {
                            customEmojiList.add(
                                    Emoji(
                                            shortname = ":${customEmoji.name}:",
                                            category = EmojiCategory.CUSTOM.name,
                                            url = "$server/emoji-custom/${customEmoji.name}.${customEmoji.extension}",
                                            count = 0,
                                            fitzpatrick = Fitzpatrick.Default.type,
                                            keywords = customEmoji.aliases,
                                            shortnameAlternates = customEmoji.aliases,
                                            siblings = mutableListOf(),
                                            unicode = "",
                                            isDefault = true
                                    )
                            )
                        }
                    }
                    EmojiRepository.load(this@RocketChatApplication, customEmojis = customEmojiList)
                } catch (ex: RocketChatException) {
                    Timber.e(ex)
                    EmojiRepository.load(this@RocketChatApplication as Context)
                }
            }
        }
    }

    fun loginPost(){
        val postApplication = ChatApplication(this)
        postApplication.onCreate()
        val mAuth = FirebaseAuth.getInstance()
        //thay bằng user đăng nhập
        mAuth.signInWithEmailAndPassword("thonv2@gmail.com", "123456").addOnCompleteListener{
            if(it.isSuccessful){
                current_user_firebase_id = mAuth.currentUser!!.uid
            }
        }
    }

}
