package chat.rocket.mingalabar.files.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.files.ui.FilesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FilesFragmentProvider {

    @ContributesAndroidInjector(modules = [FilesFragmentModule::class])
    @PerFragment
    abstract fun provideFilesFragment(): FilesFragment
}