package chat.rocket.mingalabar.chatrooms.adapter

data class HeaderItemHolder(override val data: String) : ItemHolder<String>