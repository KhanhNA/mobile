package chat.rocket.mingalabar.chatgroups.di

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import chat.rocket.mingalabar.chatgroups.domain.FetchChatGroupsInteractor
import chat.rocket.mingalabar.chatgroups.presentation.ChatGroupsView
import chat.rocket.mingalabar.chatgroups.ui.ChatGroupsFragment
import chat.rocket.mingalabar.chatrooms.adapter.RoomUiModelMapper
import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.db.ChatRoomDao
import chat.rocket.mingalabar.db.DatabaseManager
import chat.rocket.mingalabar.db.UserDao
import chat.rocket.mingalabar.server.domain.*
import chat.rocket.mingalabar.server.infrastructure.ConnectionManager
import chat.rocket.mingalabar.server.infrastructure.ConnectionManagerFactory
import chat.rocket.mingalabar.server.infrastructure.RocketChatClientFactory
import chat.rocket.core.RocketChatClient
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class ChatGroupsFragmentModule {

    @Provides
    @PerFragment
    fun chatGroupsView(frag: ChatGroupsFragment): ChatGroupsView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ChatGroupsFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideRocketChatClient(
        factory: RocketChatClientFactory,
        @Named("currentServer") currentServer: String?
    ): RocketChatClient {
        return currentServer?.let { factory.get(it) }!!
    }

    @Provides
    @PerFragment
    fun provideChatGroupDao(manager: DatabaseManager): ChatRoomDao = manager.chatRoomDao()

    @Provides
    @PerFragment
    fun provideUserDao(manager: DatabaseManager): UserDao = manager.userDao()

    @Provides
    @PerFragment
    fun provideConnectionManager(
        factory: ConnectionManagerFactory,
        @Named("currentServer") currentServer: String?
    ): ConnectionManager {
        return currentServer?.let { factory.create(it) }!!
    }

    @Provides
    @PerFragment
    fun provideFetchChatGroupsInteractor(
        client: RocketChatClient,
        dbManager: DatabaseManager
    ): FetchChatGroupsInteractor {
        return FetchChatGroupsInteractor(client, dbManager)
    }

    @Provides
    @PerFragment
    fun providePublicSettings(
        repository: SettingsRepository,
        @Named("currentServer") currentServer: String?
    ): PublicSettings {
        return currentServer?.let { repository.get(it) }!!
    }

    @Provides
    @PerFragment
    fun provideGroupMapper(
        context: Application,
        settingsRepository: SettingsRepository,
        userInteractor: GetCurrentUserInteractor,
        tokenRepository: TokenRepository,
        @Named("currentServer") currentServer: String?,
        permissionsInteractor: PermissionsInteractor
    ): RoomUiModelMapper {
        return currentServer?.let {
            RoomUiModelMapper(
                context,
                settingsRepository.get(it),
                userInteractor,
                tokenRepository,
                it,
                permissionsInteractor
            )
        }!!
    }

    @Provides
    @PerFragment
    fun provideGetCurrentUserInteractor(
        tokenRepository: TokenRepository,
        @Named("currentServer") currentServer: String?,
        userDao: UserDao
    ): GetCurrentUserInteractor {
        return currentServer?.let { GetCurrentUserInteractor(tokenRepository, it, userDao) }!!
    }
}