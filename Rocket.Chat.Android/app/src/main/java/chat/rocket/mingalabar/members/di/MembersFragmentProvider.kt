package chat.rocket.mingalabar.members.di

import chat.rocket.mingalabar.dagger.scope.PerFragment
import chat.rocket.mingalabar.members.ui.MembersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MembersFragmentProvider {

    @ContributesAndroidInjector(modules = [MembersFragmentModule::class])
    @PerFragment
    abstract fun provideMembersFragment(): MembersFragment
}