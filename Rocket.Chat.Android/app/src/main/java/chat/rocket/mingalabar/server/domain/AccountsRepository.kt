package chat.rocket.mingalabar.server.domain

import chat.rocket.mingalabar.server.domain.model.Account

interface AccountsRepository {

    fun save(account: Account)
    fun load(): List<Account>
    fun remove(serverUrl: String)
}