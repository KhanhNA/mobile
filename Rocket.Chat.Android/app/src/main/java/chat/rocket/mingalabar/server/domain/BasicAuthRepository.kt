package chat.rocket.mingalabar.server.domain

import chat.rocket.mingalabar.server.domain.model.BasicAuth

interface BasicAuthRepository {
    fun save(basicAuth: BasicAuth)
    fun load(): List<BasicAuth>
}
