package chat.rocket.mingalabar.dagger.module

import chat.rocket.mingalabar.authentication.di.AuthenticationModule
import chat.rocket.mingalabar.authentication.login.di.LoginFragmentProvider
import chat.rocket.mingalabar.authentication.loginoptions.di.LoginOptionsFragmentProvider
import chat.rocket.mingalabar.authentication.onboarding.di.OnBoardingFragmentProvider
import chat.rocket.mingalabar.authentication.registerusername.di.RegisterUsernameFragmentProvider
import chat.rocket.mingalabar.authentication.resetpassword.di.ResetPasswordFragmentProvider
import chat.rocket.mingalabar.authentication.server.di.ServerFragmentProvider
import chat.rocket.mingalabar.authentication.signup.di.SignupFragmentProvider
import chat.rocket.mingalabar.authentication.twofactor.di.TwoFAFragmentProvider
import chat.rocket.mingalabar.authentication.ui.AuthenticationActivity
import chat.rocket.mingalabar.chatdetails.di.ChatDetailsFragmentProvider
import chat.rocket.mingalabar.chatgroups.di.ChatGroupsFragmentProvider
import chat.rocket.mingalabar.chatinformation.di.MessageInfoFragmentProvider
import chat.rocket.mingalabar.chatinformation.ui.MessageInfoActivity
import chat.rocket.mingalabar.chatroom.di.ChatRoomFragmentProvider
import chat.rocket.mingalabar.chatroom.di.ChatRoomModule
import chat.rocket.mingalabar.chatroom.ui.ChatRoomActivity
import chat.rocket.mingalabar.chatrooms.di.ChatRoomsFragmentProvider
import chat.rocket.mingalabar.createchannel.di.CreateChannelProvider
import chat.rocket.mingalabar.dagger.scope.PerActivity
import chat.rocket.mingalabar.directory.di.DirectoryFragmentProvider
import chat.rocket.mingalabar.draw.main.di.DrawModule
import chat.rocket.mingalabar.draw.main.ui.DrawingActivity
import chat.rocket.mingalabar.favoritemessages.di.FavoriteMessagesFragmentProvider
import chat.rocket.mingalabar.files.di.FilesFragmentProvider
import chat.rocket.mingalabar.inviteusers.di.InviteUsersFragmentProvider
import chat.rocket.mingalabar.main.di.MainFragmentProvider
import chat.rocket.mingalabar.main.di.MainModule
import chat.rocket.mingalabar.main.ui.MainActivity
import chat.rocket.mingalabar.members.di.MembersFragmentProvider
import chat.rocket.mingalabar.mentions.di.MentionsFragmentProvider
import chat.rocket.mingalabar.pinnedmessages.di.PinnedMessagesFragmentProvider
import chat.rocket.mingalabar.profile.di.ProfileFragmentProvider
import chat.rocket.mingalabar.server.di.ChangeServerModule
import chat.rocket.mingalabar.server.ui.ChangeServerActivity
import chat.rocket.mingalabar.servers.di.ServersBottomSheetFragmentProvider
import chat.rocket.mingalabar.settings.di.SettingsFragmentProvider
import chat.rocket.mingalabar.settings.password.di.PasswordFragmentProvider
import chat.rocket.mingalabar.settings.password.ui.PasswordActivity
import chat.rocket.mingalabar.sortingandgrouping.di.SortingAndGroupingBottomSheetFragmentProvider
import chat.rocket.mingalabar.userdetails.di.UserDetailsFragmentProvider
import chat.rocket.mingalabar.videoconference.di.VideoConferenceModule
import chat.rocket.mingalabar.videoconference.ui.VideoConferenceActivity
import chat.rocket.mingalabar.webview.adminpanel.di.AdminPanelWebViewFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @PerActivity
    @ContributesAndroidInjector(
        modules = [AuthenticationModule::class,
            OnBoardingFragmentProvider::class,
            ServerFragmentProvider::class,
            LoginOptionsFragmentProvider::class,
            LoginFragmentProvider::class,
            RegisterUsernameFragmentProvider::class,
            ResetPasswordFragmentProvider::class,
            SignupFragmentProvider::class,
            TwoFAFragmentProvider::class
        ]
    )
    abstract fun bindAuthenticationActivity(): AuthenticationActivity

    @PerActivity
    @ContributesAndroidInjector(
        modules = [MainModule::class,
            MainFragmentProvider::class,
            ChatGroupsFragmentProvider::class,
            ChatRoomsFragmentProvider::class,
            ServersBottomSheetFragmentProvider::class,
            SortingAndGroupingBottomSheetFragmentProvider::class,
            CreateChannelProvider::class,
            ProfileFragmentProvider::class,
            SettingsFragmentProvider::class,
            AdminPanelWebViewFragmentProvider::class,
            DirectoryFragmentProvider::class
        ]
    )
    abstract fun bindMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector(
        modules = [ChatRoomModule::class,
            ChatRoomFragmentProvider::class,
            UserDetailsFragmentProvider::class,
            ChatDetailsFragmentProvider::class,
            MembersFragmentProvider::class,
            InviteUsersFragmentProvider::class,
            MentionsFragmentProvider::class,
            PinnedMessagesFragmentProvider::class,
            FavoriteMessagesFragmentProvider::class,
            FilesFragmentProvider::class
        ]
    )
    abstract fun bindChatRoomActivity(): ChatRoomActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [PasswordFragmentProvider::class])
    abstract fun bindPasswordActivity(): PasswordActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ChangeServerModule::class])
    abstract fun bindChangeServerActivity(): ChangeServerActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [MessageInfoFragmentProvider::class])
    abstract fun bindMessageInfoActiviy(): MessageInfoActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [DrawModule::class])
    abstract fun bindDrawingActivity(): DrawingActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [VideoConferenceModule::class])
    abstract fun bindVideoConferenceActivity(): VideoConferenceActivity
}
