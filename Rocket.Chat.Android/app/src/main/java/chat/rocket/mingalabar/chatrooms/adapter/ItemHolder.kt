package chat.rocket.mingalabar.chatrooms.adapter

interface ItemHolder<T> {
    val data: T
}