package chat.rocket.mingalabar.analytics

import android.content.Context
import javax.inject.Inject

class GoogleAnalyticsForFirebase @Inject constructor(val context: Context) : Analytics