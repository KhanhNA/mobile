package chat.rocket.mingalabar.dagger.module

import chat.rocket.mingalabar.chatroom.di.MessageServiceProvider
import chat.rocket.mingalabar.chatroom.service.MessageService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class ServiceBuilder {
    @ContributesAndroidInjector(modules = [MessageServiceProvider::class])
    abstract fun bindMessageService(): MessageService
}
