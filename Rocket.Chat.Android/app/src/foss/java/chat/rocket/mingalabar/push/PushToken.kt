package chat.rocket.mingalabar.push

import chat.rocket.core.RocketChatClient

fun retrieveCurrentPushNotificationToken(
    rocketChatClient: RocketChatClient,
    shouldUnregister: Boolean = false
) {
    // Do nothing
}

fun registerPushNotificationToken(rocketChatClient: RocketChatClient, token: String) {
    // Do nothing
}