package chat.rocket.mingalabar.suggestions.repository

interface LocalSuggestionProvider {
    fun find(prefix: String)
}