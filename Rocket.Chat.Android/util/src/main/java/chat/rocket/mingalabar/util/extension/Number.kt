package chat.rocket.mingalabar.util.extension

fun Long.orZero(): Long {
    return if (this < 0) 0 else this
}