package chat.rocket.mingalabar.util.extension

fun Boolean?.orFalse(): Boolean = this ?: false