package chat.rocket.mingalabar.emoji.internal

import chat.rocket.mingalabar.emoji.Emoji

fun Emoji.isCustom(): Boolean = this.url != null
