package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ExportStatementDetail;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.models.ImportStatementDetail;
import com.tsolution.logistics.utils.AppUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import lombok.Getter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
public class ImportStatementFromExportReleaseDetailViewModel extends BaseViewModel {
    private ObservableField<ImportStatement> importStatement = new ObservableField<ImportStatement>();
    private HashMap<String, Integer> positionMap = new HashMap<>();

    public ImportStatementFromExportReleaseDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ImportStatement importStatement) {
        if (importStatement != null) {
            this.importStatement.set(importStatement);
            findImportStatementById(importStatement.getId());
        }
    }

    private void findImportStatementById(Long statementId) {
        if (statementId != null) {
            AppUtils.callService().findImportStatementById(statementId).enqueue(callBack(this::getImportStatementSuccess, ImportStatement.class, null));
        }
    }

    private void getImportStatementSuccess(Call call, Response response, Object body, Throwable throwable) {
        ImportStatement importStatement = (ImportStatement) body;
        this.importStatement.set(importStatement);
        List<ExportStatementDetail> exportStatementDetails = importStatement.getExportStatement().getExportStatementDetails();

        for (int i = 0; i < exportStatementDetails.size(); i++) {
            exportStatementDetails.get(i).setQuantity(importStatement.getImportStatementDetails().get(i).getQuantity());
            String code = exportStatementDetails.get(i).getStoreProductPackingDetail().getCode();
            positionMap.put(AppUtils.changeFormCode(code), i);
            i++;
        }
        setData(exportStatementDetails);
    }

    public void onCreate() {
        if (validate()) {
            ImportStatement importStatement = this.importStatement.get();
            List<ImportStatement> importStatements = new ArrayList<>();
            ImportStatement statement = new ImportStatement();
            statement.setCode(importStatement.getCode());
            statement.setId(importStatement.getId());
            statement.setStatus(ImportStatement.ImportStatementStatus.UPDATED_INVENTORY.getValue());
            importStatements.add(statement);
            AppUtils.callService().createImportStatement(importStatements).enqueue(callBack(this::createSuccess, ImportStatement.class, List.class));
        }
    }

    private void createSuccess(Call call, Response response, Object o, Throwable throwable) {
        if (o != null) {
            appException.setValue(new AppException(R.string.success, "success"));
        }
    }

    private boolean validate() {
        if (positionMap == null) {
            return false;
        }
        ImportStatement importStatement = this.importStatement.get();
        if (importStatement != null) {
            List<ExportStatementDetail> statementDetails = importStatement.getExportStatement().getExportStatementDetails();
            for (ExportStatementDetail ed : statementDetails) {
                if (ed.checked == null || !ed.checked) {
                    return false;
                }
            }
        }
        return true;
    }

    public void checkProduct(String result) {
        result = AppUtils.changeFormCode(result);
        Integer stt = positionMap.get(result);
        if (stt != null) {
            List lst = (List) baseModels.get();
            ImportStatementDetail sd = (ImportStatementDetail) lst.get(stt);
            sd.checked = true;
            lst.set(lst.indexOf(lst.get(stt)), sd);
            baseModels.notifyChange();
        }
    }
}
