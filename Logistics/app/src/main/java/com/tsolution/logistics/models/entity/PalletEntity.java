package com.tsolution.logistics.models.entity;

import com.tsolution.base.BaseModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Entity(tableName = "pallet", foreignKeys = {@ForeignKey(entity = StoreEntity.class,
        parentColumns = "id",
        childColumns = "store_id"), @ForeignKey(entity = PalletEntity.class,
        parentColumns = "id",
        childColumns = "parent_pallet_id")})
@Getter
@Setter
public class PalletEntity extends BaseModel {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;

    @ColumnInfo(name = "store_id")
    private Long storeId;

    @ColumnInfo(name = "code")
    private String code;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "step")
    private Integer step;

    @ColumnInfo(name = "parent_pallet_id")
    private Long parentPalletId;

}
