package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Pageable;
import com.tsolution.logistics.models.ProductDescription;
import com.tsolution.logistics.models.entity.ProductDescriptionEntity;
import com.tsolution.logistics.repository.ProductDescriptionRepository;
import com.tsolution.logistics.repository.ProductRepository;
import com.tsolution.logistics.utils.AppUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class ProductDialogViewModel extends BaseViewModel {
    private boolean isSearchByService;
    private List<Long> exceptId;
    private Integer pageSize = AppUtils.pageSize;
    private String textSearch = "";
    private ObservableField<String> currentPage = new ObservableField<>("1");
    private ProductDescriptionRepository productDescriptionRepository;
    private ProductRepository productRepository;

    private LiveData<List<ProductDescriptionEntity>> allProductDescription;
    //private LiveData<List<ProductEntity>> allProduct;

    public ProductDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(boolean isSearchByService, List<Long> exceptId) {
        productDescriptionRepository = new ProductDescriptionRepository(getApplication());
        productRepository = new ProductRepository(getApplication());
        this.exceptId = exceptId;
        //productRepository.search(exceptId, 1);
        this.isSearchByService = isSearchByService;
        productDescriptionRepository.searchAll(exceptId, 1);
        allProductDescription = productDescriptionRepository.getAllProductDescription();
        //allProduct = productRepository.getAllProduct();
    }

    public void search() {
        if (isSearchByService) {
            searchByService();
        } else {
            isSearchByLocal();
        }
    }


    private void isSearchByLocal() {
        String page = currentPage.get();
        if (page != null) {
            productDescriptionRepository.searchAll(exceptId, Integer.parseInt(page));
        }
        allProductDescription = productDescriptionRepository.getAllProductDescription();
    }

    private void searchByService() {
        String page = currentPage.get();
        if (page != null) {
            AppUtils.callService().searchProduct(textSearch, AppUtils.language, exceptId, Integer.parseInt(page), AppUtils.pageSize).enqueue(callBack(this::searchProductSuccess, Pageable.class, ProductDescription.class, null));
        }
    }

    private void searchProductSuccess(Call call, Response response, Object body, Throwable throwable) {
        Pageable productPageable = (Pageable) body;
        setPage(productPageable);
        List<ProductDescription> productList = productPageable.getContent();
        setData(productList);
    }

    public void setDataForView(List<ProductDescriptionEntity> productDescriptions) {
        setData(productDescriptions);
    }
}


