package com.tsolution.logistics.models.mapper;

import com.tsolution.logistics.models.ExportStatementDetail;
import com.tsolution.logistics.models.ProductDescription;
import com.tsolution.logistics.models.entity.ProductDescriptionEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ExportStatementDetailMapper {
    ExportStatementDetailMapper INSTANCE = Mappers.getMapper( ExportStatementDetailMapper.class );
    @Mapping(source = "code", target = "productCode")
    @Mapping(source = "name", target = "productName")
    @Mapping(source = "packingQuantity", target = "packingTypeQuantity")
    @Mapping(source = "productEntityId", target = "productId")
    @Mapping(source = "packingId", target = "packingTypeId")
    @Mapping(source = "quantity", target = "totalQuantity")
    @Mapping(target = "index", ignore = true)
    @Mapping(target = "checked", ignore = true)
    ExportStatementDetail productDescriptionToExportStatementDetail(ProductDescriptionEntity product);



    @Mapping(source = "productCode", target = "code")
    @Mapping(source = "productName", target = "name")
    @Mapping(source = "packingTypeQuantity", target = "packingQuantity")
    @Mapping(source = "productId", target = "productEntityId")
    @Mapping(source = "packingTypeId", target = "packingId")
    @Mapping(source = "totalQuantity", target = "quantity")
    @Mapping(target = "index", ignore = true)
    @Mapping(target = "checked", ignore = true)
    ProductDescriptionEntity statementDetailToProduct(ExportStatementDetail statementDetail);
}
