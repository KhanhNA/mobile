package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CountryDescription extends Base{
    private Language language;
    private Country country;
    private String name;
    private String title;
    private String description;

    public CountryDescription(String name, String title, String description) {
        this.name = name;
        this.title = title;
        this.description = description;
    }
}
