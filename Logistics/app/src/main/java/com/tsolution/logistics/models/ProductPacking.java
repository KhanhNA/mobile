package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductPacking extends Base {

    private String code;

    private String name;

    private Integer vat;

    private String uom;

    private String barcode;

    private Product product;

    private PackingType packingType;

    private Boolean status;

    private List<ProductPackingPrice> productPackingPrices;

    private List<PalletDetail> palletDetails;

}
