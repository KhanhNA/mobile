package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.viewmodels.ImportStatementViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CreateImportStatementFromExportFragment extends BaseFragment {
    ImportStatementViewModel importStatementViewModel;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        importStatementViewModel = (ImportStatementViewModel) viewModel;
        importStatementViewModel.init();
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_create_import_statement_item, viewModel, (view, baseModel) -> {
            ExportStatement exportStatement = (ExportStatement) baseModel;
            Intent intent = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", exportStatement);
            intent.putExtras(bundle);
            intent.putExtra("FRAGMENT", ImportStatementFromExportDetailFragment.class);
            startActivity(intent);
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if ("scanCode".equals(view.getTag())) {
            Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
            startActivityForResult(qrScan, 0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                importStatementViewModel.searchByCode(result);
            }
        }
        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_import_statement;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportStatementViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.exportStatements;
    }
}
