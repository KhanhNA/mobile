package com.tsolution.logistics.models.dao;

import com.tsolution.logistics.models.entity.PalletEntity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface PalletDao {
    @Query("SELECT p.* from pallet p LIMIT :limit OFFSET :offset")
    LiveData<List<PalletEntity>> getPalletByPage(int limit, int offset);

    @Query("SELECT p.* from pallet p")
    LiveData<List<PalletEntity>> getAllPallet();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(PalletEntity palletEntity);

    @Query("DELETE FROM pallet")
    public void deleteAll();
}
