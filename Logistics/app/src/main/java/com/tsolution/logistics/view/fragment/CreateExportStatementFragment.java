package com.tsolution.logistics.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.view.dialog.ProductDialog;
import com.tsolution.logistics.viewmodels.CreateExportStatementViewModel;


public class CreateExportStatementFragment extends BaseFragment {
    private CreateExportStatementViewModel createExportStatementViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  super.onCreateView(inflater, container, savedInstanceState);
        createExportStatementViewModel = (CreateExportStatementViewModel) viewModel;
        createExportStatementViewModel.init();
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_create_export_statement_item, viewModel, null, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return v;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        ProductDialog productDialog = new ProductDialog();
        productDialog.setExceptId(createExportStatementViewModel.getExceptId());
        productDialog.setSearchByService(false);
        productDialog.setResult(createExportStatementViewModel.getCurrentProduct());
        if(getFragmentManager()!=null){
            productDialog.show(getFragmentManager(), "searchProduct");
        }

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_export_statement;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateExportStatementViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.deltail;
    }
}
