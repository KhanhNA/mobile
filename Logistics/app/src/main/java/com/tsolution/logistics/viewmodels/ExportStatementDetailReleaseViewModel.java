package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.util.Log;

import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.models.ExportStatementDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class ExportStatementDetailReleaseViewModel extends BaseViewModel {
    private ExportStatement exportStatement = new ExportStatement();
    private HashMap<String, Integer> exportStatementDetails = new HashMap<>();

    public ExportStatementDetailReleaseViewModel(@NonNull Application application) {
        super(application);
    }

    public void checkProduct(String result) {
        result = AppUtils.changeFormCode(result);
//        if (statement.getValue() != null) {
        Integer stt = exportStatementDetails.get(result);
        if (stt != null) {
            List lst = (List) baseModels.get();
            ExportStatementDetail sd = (ExportStatementDetail) lst.get(stt);
            sd.checked = true;
            lst.set(lst.indexOf(lst.get(stt)), sd);
//            exportStatementDetails.remove(result);
            baseModels.notifyChange();
        }
//        }
    }

    public void findExportStatementById() {
        AppUtils.callService().findExportStatementById(exportStatement.getId()).enqueue(callBack(this::getExportStatementSuccess, ExportStatement.class, null));
    }

    private void getExportStatementSuccess(Call call, Response response, Object o, Throwable throwable) {
        exportStatement = (ExportStatement) o;
        List<ExportStatementDetail> statementDetails = exportStatement.getExportStatementDetails();
        setData(statementDetails);
        int stt = 0;
        for (ExportStatementDetail exportStatementDetail : statementDetails) {
            String code = exportStatementDetail.getStoreProductPackingDetail().getCode();
            exportStatementDetails.put(AppUtils.changeFormCode(code), stt);
            stt++;
        }
    }

    public void onCreate() {
        if (validate()) {
            List<ExportStatement> exportStatements = new ArrayList<>();
            ExportStatement statement = new ExportStatement();
            statement.setCode(exportStatement.getCode());
            statement.setId(exportStatement.getId());
            statement.setStatus(ExportStatement.ExportStatementStatus.RELEASED.getValue());
            exportStatements.add(statement);
            AppUtils.callService().exportStatement(exportStatements).enqueue(callBack(this::statementSuccess, ExportStatement.class, List.class));
        } else {
            Log.e("NMQ", "Thong bao loi");
            appException.setValue(new AppException(R.string.product_info_error, "product_info_error"));
            // TODO: 7/18/2019 Show message error
        }
    }

    private void statementSuccess(Call call, Response response, Object o, Throwable throwable) {
        Log.e("NMQ", "Success");
        appException.setValue(new AppException(R.string.success, "success"));
    }

    private boolean validate() {
        if (exportStatement == null) {
            return false;
        }
        List<ExportStatementDetail> statementDetails = exportStatement.getExportStatementDetails();
        for (ExportStatementDetail ed : statementDetails) {
            if (ed.checked == null || !ed.checked) {
                return false;
            }
        }
        return true;
    }

    public void onClickCheckBox(BaseModel baseModel) {

    }
}
