package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RepackingPlanningDetail extends Base {
    private RepackingPlanning repackingPlanning;

    private ProductPacking productPacking;

    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date expireDate;

    private Long quantity;

    //private List<RepackingPlanningDetailPallet> repackingPlanningDetailPallets;

    private List<RepackingPlanningDetailRepacked> repackingPlanningDetailRepackeds;

    private Long inventory;
}