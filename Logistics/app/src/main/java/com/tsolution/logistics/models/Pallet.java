package com.tsolution.logistics.models;

import android.view.Display;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.ValidateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

import androidx.room.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Pallet extends Base {
    public enum PalletStep {
        // Mới tạo lệnh nhập chưa thực nhập và chưa cộng kho
        IMPORT_PO_UPDATE_PALLET(1),
        // Đã tạo lệnh thực nhập và cộng kho
        IMPORT_STATEMENT_UPDATE_PALLET(2);

        private int value;

        PalletStep(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    private Store store;

    private String code;

    private String qRCode;

    private String name;

    private Integer step;

    private Pallet parentPallet;

    private List<Pallet> childPallets;

    private List<PalletDetail> palletDetails;

    private String displayName;

    public String getCode() {
        if (ValidateUtils.isNullOrEmpty(code)) {
            return "";
        }
        return code;
    }

}