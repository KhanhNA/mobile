package com.tsolution.logistics.repository;

import com.tsolution.logistics.models.Country;
import com.tsolution.logistics.models.CountryDescription;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.models.ImportPoDetailPallet;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.models.RepackingPlanningDetailRepacked;
import com.tsolution.logistics.models.Statement;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("/user/me")
    Call<ResponseBody> getUser();

    //  ========================LANGUGES========================
    @GET("/languages")
    Call<ResponseBody> getAllLanguages();

    //  ========================COUNTRIES========================
    @GET("/countries")
    Call<List<CountryDescription>> getAllLanguage();

    @GET("/countries")
    Call<ResponseBody> getAllCountriesDescription();

    @GET("/countries")
    Call<ResponseBody> getAllCountriesDescription(@Query("support") Boolean support,
                                                  @Query("language") Long language);

    @GET("/countries/{id}")
    Call<ResponseBody> getOneCountry(@Path("id") Long id);

    @PATCH("/countries/{id}")
    Call<ResponseBody> updateCountryDescription(@Path("id") Long id, @Body Country source);

    //  ========================PRODUCT========================
    @GET("/products")
    Call<ResponseBody> searchProduct(@Query("text") String text,
                                     @Query("language") Long language,
                                     @Query("exceptId") List<Long> exceptId,
                                     @Query("PageNumber") Integer pageNumber,
                                     @Query("PageSize") Integer PageSize);

    @GET("/products/packing-type")
    Call<ResponseBody> getPackingType(@Query("status") Boolean status);

    //  ========================STORES========================
    @GET("/stores/fc")
    Call<ResponseBody> getStoreFC();

    @GET("/stores/{id}")
    Call<ResponseBody> findStoreById(@Path("id") Long id);

    @GET("/stores")
    Call<ResponseBody> getFromStore(@Query("status") Boolean status,
                                    @Query("ignoreCheckPermission") Boolean ignoreCheckPermission,
                                    @Query("exceptIds") ArrayList<Long> exceptIds);

    @GET("/stores")
    Call<ResponseBody> getToStore(@Query("status") Boolean status,
                                  @Query("is_DC") Boolean is_DC,
                                  @Query("ignoreCheckPermission") Boolean ignoreCheckPermissiongetStoreFC,
                                  @Query("exceptIds") ArrayList<Long> exceptIds);

    @GET("/stores/all")
    Call<ResponseBody> getAllStore();


    //  ========================IMPORT STATEMENT========================
    @POST("/import-statements/import-fc")
    Call<ResponseBody> createImportStatementFC(@Body Statement statement);

    @GET("/import-statements/find")
    Call<ResponseBody> getAll(@Query("code") String code,
                              @Query("status") Integer status,
                              @Query("pageNumber") Integer pageNumber,
                              @Query("pageSize") Integer pageSize);

    @POST("/import-statements")
    Call<ResponseBody> createImportStatement(@Body List<ImportStatement> importStatements);

    @POST("/import-statements/from-export-statement")
    Call<ResponseBody> createFromExportStatement(@Query("code") Long exportStatementId);

    @GET("/import-statements/{id}")
    Call<ResponseBody> findImportStatementById(@Path("id") Long id);

    @POST("/import-statements/from-repacking-planning")
    Call<ResponseBody> createImportStatementFromRepackingPlanning(@Body ImportStatement importStatements);


    //    ========================MERCHANT ORDER========================
    @GET("/merchant-orders/search")
    Call<ResponseBody> getOrders();

    @GET("/merchant-orders/search")
    Call<ResponseBody> getStatementDetailFromMerchantOrder(@Query("code") String code);

    //    ========================EXPORT STATEMENT========================
    @POST("/export-statements")
    Call<ResponseBody> exportStatement(@Body List<ExportStatement> exportStatementList);

    @GET("/export-statements/get-by-code")
    Call<ResponseBody> getExportStatement(@Query("code") String code);

    @GET("/export-statements/{id}")
    Call<ResponseBody> findExportStatementById(@Path("id") Long id);

    @GET("/export-statements/from-merchant-order")
    Call<ResponseBody> createStatementFromMerChantOrder(@Query("code") String code);

    @GET("/export-statements")
    Call<ResponseBody> findImportStatementByCode(@Query("code") String code,
                                                 @Query("status") Integer status,
                                                 @Query("pageNumber") Integer pageNumber,
                                                 @Query("pageSize") Integer pageSize);

    //    ========================REPACKING PLANNING========================

    @GET("/repacking-plannings")
    Call<ResponseBody> findRepackingPlanning(@Query("text") String text,
                                             @Query("dateType") Integer dateType,
                                             @Query("fromDate") String fromDate,
                                             @Query("toDate") String toDate,
                                             @Query("pageNumber") Integer pageNumber ,
                                             @Query("pageSize") Integer pageSize,
                                             @Query("status") Integer status);

    @GET("/repacking-plannings/{id}")
    Call<ResponseBody> findRepackingPlanningById(@Path("id") Long id);




//    @POST("/repacking-plannings/repack/{id}")
//    Call<ResponseBody> repack(@Path("id") Long id, @Body List<RepackingPlanningDetailRepacked> repackingPlanningDetailRepackeds);

    //    ========================PALLET========================

    @GET("/pallets")
    Call<ResponseBody> findPallet(@Query("pageNumber") Integer pageNumber,
                                  @Query("pageSize") Integer pageSize,
                                  @Query("step") Integer step,
                                  @Query("text") String text);
    @GET("/pallets/all")
    Call<ResponseBody> findAllPallet();


    //    ========================IMPORT POS========================

    @GET("/import-pos")
    Call<ResponseBody> findImportPo(@Query("pageNumber") Integer pageNumber,
                                  @Query("pageSize") Integer pageSize,
                                  @Query("status") Integer status,
                                  @Query("text") String text);

    @GET("/import-pos/{id}")
    Call<ResponseBody> findById(@Path("id") Long id);

    @POST("/import-pos/update-pallet/{id}")
    Call<ResponseBody>  updatePallet(@Path("id") Long id, @Body List<ImportPoDetailPallet> importPoDetailPallets);

}
