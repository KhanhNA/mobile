package com.tsolution.logistics.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.base.BaseModel;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import lombok.Getter;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public abstract class SuperEntity extends BaseModel {


	private Long id;

//	@JsonFormat(pattern = AppUtils.dateFormat)
//	private Date createDate;


	private String createUser;

//	@JsonFormat(pattern = AppUtils.dateFormat)
//	private Date updateDate;


	private String updateUser;

}
