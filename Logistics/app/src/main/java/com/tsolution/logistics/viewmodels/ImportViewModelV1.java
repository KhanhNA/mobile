package com.tsolution.logistics.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;

import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.models.StatementDetail;

import java.util.ArrayList;
import java.util.List;

public class ImportViewModelV1 extends BaseViewModel {
    public ImportViewModelV1(@NonNull Application application) {
        super(application);
    }
    public void init(){
        List<StatementDetail> a = new ArrayList<>();
        a.add(new StatementDetail());
        a.add(new StatementDetail());
        setData(a);
    }
}
