package com.tsolution.logistics.view;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;

import com.amitshekhar.DebugDB;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.databinding.ActivityHomeBinding;
import com.tsolution.logistics.models.AppType;
import com.tsolution.logistics.models.Role;
import com.tsolution.logistics.utils.AppModel.MenuTitle;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.utils.ValidateUtils;
import com.tsolution.logistics.view.adapter.ExpandableListAdapter;
import com.tsolution.logistics.viewmodels.UserViewModel;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    private UserViewModel userViewModel;
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    Map<MenuTitle, SortedSet<MenuTitle>> mapMenu = new TreeMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityHomeBinding activityHomeBinding = (ActivityHomeBinding) binding;//DataBindingUtil.setContentView(this, R.layout.activity_home);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.nav_header_home,
                activityHomeBinding.navView, false);
        userViewModel = (UserViewModel) viewModel;
        userViewModel.createDataBaseLocal();


        activityHomeBinding.navView.addHeaderView(binding.getRoot());
//        nav.setVariable(BR.viewModel,viewModel);


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.gray_blue_dark));
        //Log.e("LOG", DebugDB.getAddressLog());


        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with showyour own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show());
//        fab.setOnClickListener(view -> userViewModel.test());
        expandableListView = findViewById(R.id.expandableListView);
//        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Log.e("NMQ Log", DebugDB.getAddressLog());

        userViewModel.getStores().observe(this, storeEntities -> {
            if(storeEntities!=null && storeEntities.size()==0){
                userViewModel.synchronizeData();
                Log.e("NMQ", storeEntities.size()+"");
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sync) {
            userViewModel.clearDataBase();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_share) {
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void populateExpandableList() {
        try {
            for (Role role : AppUtils.user.getUserAuthentication().getPrincipal().getRoles()) {
                if (ValidateUtils.isNullOrEmpty(role.getMenus())) {
                    continue;
                }
                for (com.tsolution.logistics.models.Menu menu : role.getMenus()) {
                    if (!AppType.MOBILE.getValue().equals(menu.getAppType())) {
                        continue;
                    }
                    MenuTitle menuTitle = AppUtils.findMenuByCode(menu.getCode());
                    if (menuTitle == null) {
                        continue;
                    }
                    if (menu.getParentMenu() == null && !mapMenu.containsKey(menuTitle)) {
                        mapMenu.put(menuTitle, new TreeSet<>());
                    }
                    if (menu.getParentMenu() != null) {
                        SortedSet<MenuTitle> menuTitleSet;
                        MenuTitle parentMenuTitle = AppUtils.findMenuByCode(menu.getParentMenu().getCode());
                        if (mapMenu.containsKey(parentMenuTitle)) {
                            menuTitleSet = mapMenu.get(parentMenuTitle);
                        } else {
                            menuTitleSet = new TreeSet<>();
                        }
                        menuTitleSet.add(menuTitle);
                        mapMenu.put(parentMenuTitle, menuTitleSet);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: 7/18/2019 Thong bao loi
        }
        expandableListAdapter = new ExpandableListAdapter(this, mapMenu.keySet(), mapMenu);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener((parent, v, groupPosition, id) -> false);
        expandableListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            Log.e("onChildClick", groupPosition + "" + "    " + childPosition);

            Set<MenuTitle> menuTitles = mapMenu.get(mapMenu.keySet().toArray()[groupPosition]);
            if (menuTitles != null) {
                MenuTitle menuTitle = menuTitles.toArray(new MenuTitle[menuTitles.size()])[childPosition];
                toolbar.setTitle(menuTitle.getTitle());
                if (!ValidateUtils.isNullOrEmpty(menuTitle.getTag())) {
                    Bundle bundle = new Bundle();
                    bundle.putString("fragment", menuTitle.getTag());
                    menuTitle.getFragment().setArguments(bundle);
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout, menuTitle.getFragment()).commit();
            }
            onBackPressed();
            return false;
        });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_home;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return UserViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
