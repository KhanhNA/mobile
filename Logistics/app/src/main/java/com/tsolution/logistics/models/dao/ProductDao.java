package com.tsolution.logistics.models.dao;

import com.tsolution.logistics.models.entity.ProductEntity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface ProductDao {
    @Query("SELECT p.* from product p WHERE p.id NOT IN(:exceptId) LIMIT :limit OFFSET :offset")
    LiveData<List<ProductEntity>> getAllProduct(long[] exceptId, int limit, int offset);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(ProductEntity productEntity);

    @Query("DELETE FROM product")
    void deleteAll();
}
