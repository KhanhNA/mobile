package com.tsolution.logistics.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.AppUser;
import com.tsolution.logistics.viewmodels.LoginActivityViewModel;


public class LoginActivity extends BaseActivity {
    TextInputLayout txtUsername;
    TextInputLayout txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        viewModel = ViewModelProviders.of(this).get(LoginActivityViewModel.class);
//        binding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.activity_main);
//

        txtUsername = binding.getRoot().findViewById(R.id.txtUsername);
        txtPassword = binding.getRoot().findViewById(R.id.txtPassword);
        binding.setLifecycleOwner(this);
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
//            case "resetLanguage":
//                setLocale("vn");
//                break;
            case "loginError":
                showErrorInput(((AppUser) viewModel.getModel().get()).getLoginStatus());
                break;
            case "startHome":
                Intent home = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(home);
        }
        closeProcess();
    }
    public void showErrorInput(int errorCode) {
        switch (errorCode) {
            case 1:
                txtUsername.setError("Field can't be empty");
                txtPassword.setError(null);
                break;
            case 2:
                txtPassword.setError("Field can't be empty");
                txtUsername.setError(null);
                break;
            default:
                txtPassword.setError(null);
                txtUsername.setError(null);
                break;
        }
    }

    //    public void setLocale(String lang) {
//        Locale myLocale = new Locale(lang);
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.locale = myLocale;
//        res.updateConfiguration(conf, dm);
//        Intent refresh = new Intent(this, LoginActivity.class);
//        startActivity(refresh);
//        finish();
//    }
    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginActivityViewModel.class;
    }
}
