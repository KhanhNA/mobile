package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.viewmodels.ExportStatementDetailReleaseViewModel;

public class ExportStatementDetailReleaseFragment extends BaseFragment {
    private ExportStatementDetailReleaseViewModel detailViewModel;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        detailViewModel = (ExportStatementDetailReleaseViewModel) viewModel;
        FragmentActivity activity = getActivity();
        if(activity!=null){
            Intent intent = activity.getIntent();
            if(intent!=null && intent.hasExtra("exportStatement")){
                ExportStatement exportStatement = (ExportStatement) intent.getSerializableExtra("exportStatement");
                detailViewModel.setExportStatement(exportStatement);
                detailViewModel.findExportStatementById();
            }
        }
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_create_export_statement_release_detail_item, viewModel, (view, baseModel) -> {
            detailViewModel.onClickCheckBox(baseModel);
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return v;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
//        super.action(view, baseViewModel);
        String tag = (String) view.getTag();
        if ("scanProduct".equals(tag)) {
            Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
            startActivityForResult(qrScan, 0);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                detailViewModel.checkProduct(result);
            }
        }
        closeProcess();
    }
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_export_statement_release_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ExportStatementDetailReleaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.lstProducts;
    }
}
