package com.tsolution.logistics.view.dialog;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseDialogFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.entity.ProductDescriptionEntity;
import com.tsolution.logistics.viewmodels.ProductDialogViewModel;

import java.util.List;

import lombok.Setter;

@Setter
public class ProductDialog extends BaseDialogFragment {

    private boolean isSearchByService;
    private List<Long> exceptId;
    private ObservableField<ProductDescriptionEntity> result;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ProductDialogViewModel dialogViewModel = (ProductDialogViewModel) viewModel;
        dialogViewModel.init(isSearchByService, exceptId);


        BaseAdapter baseAdapter = new BaseAdapter(R.layout.dialog_search_product_item, viewModel, (view, baseModel) -> {
            result.set((ProductDescriptionEntity) baseModel);
            dismiss();
        }, getBaseActivity());


        dialogViewModel.getAllProductDescription().observe(this, dialogViewModel::setDataForView);

        //dialogViewModel.getAllProduct().observe(this, products -> Log.e("products", products.size() + ""));

        recyclerView.setAdapter(baseAdapter);

        return binding.getRoot();
    }



    @Override
    public int getLayoutRes() {
        return R.layout.dialog_product;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ProductDialogViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.list_product;
    }
}
