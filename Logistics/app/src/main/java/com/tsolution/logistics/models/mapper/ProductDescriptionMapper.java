package com.tsolution.logistics.models.mapper;

import com.tsolution.logistics.models.ProductDescription;
import com.tsolution.logistics.models.entity.ProductDescriptionEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductDescriptionMapper {
    ProductDescriptionMapper INSTANCE = Mappers.getMapper( ProductDescriptionMapper.class );

    @Mapping(source = "product.id", target = "productEntityId")
    @Mapping(source = "language.id", target = "langId")
    @Mapping(source = "packingTypeId", target = "packingId")
    @Mapping(target = "quantity", ignore = true)
    @Mapping(target = "index", ignore = true)
    @Mapping(target = "checked", ignore = true)
    ProductDescriptionEntity productDescriptionDtoToProductDescription(ProductDescription product);
}
