package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.viewmodels.ImportStatementFromExportReleaseDetailViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

public class ImportStatementFromExportReleaseDetailFragment extends BaseFragment {
    private ImportStatementFromExportReleaseDetailViewModel mViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_import_statement_from_export_release_detail_item, viewModel, (view, baseModel) -> {
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Intent intent = activity.getIntent();
            if (intent != null && intent.hasExtra("model")) {
                ImportStatement importStatement = (ImportStatement) intent.getSerializableExtra("model");
                mViewModel = (ImportStatementFromExportReleaseDetailViewModel) viewModel;
                mViewModel.init(importStatement);
            }
        }
        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        String tag = (String) view.getTag();
        if ("scanProduct".equals(tag)) {
            Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
            startActivityForResult(qrScan, 0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                mViewModel.checkProduct(result);
            }
        }
        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_import_statement_from_export_release_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportStatementFromExportReleaseDetailViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.importStatements;
    }
}
