package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportPo;
import com.tsolution.logistics.models.ImportPoDetail;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.viewmodels.ImportPoSplitViewModel;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;

public class ImportPoSplitFragment extends BaseFragment {
    private ImportPoSplitViewModel importPoSplitViewModel;
    //toolbar
    private Toolbar toolbar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        importPoSplitViewModel = (ImportPoSplitViewModel) viewModel;
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_import_po_split_item, importPoSplitViewModel, (view, baseModel) -> {
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Intent intent = activity.getIntent();
            if (intent != null && intent.hasExtra("importPoDetail") && intent.hasExtra("importPo")) {
                ImportPoDetail importPoDetail = (ImportPoDetail) intent.getSerializableExtra("importPoDetail");
                ImportPo importPo = (ImportPo) intent.getSerializableExtra("importPo");
                importPoSplitViewModel.setImportPoDetail(importPoDetail);
                importPoSplitViewModel.setImportPo(importPo);
                importPoSplitViewModel.init();
            }
        }
        return binding.getRoot();
    }
    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if ("scanCode".equals(view.getTag())) {
            Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
            startActivityForResult(qrScan, 1);
        }
        if ("create".equals(view.getTag())) {
            if(importPoSplitViewModel.enoughQuantity()){
                Intent resultIntent = new Intent();
                resultIntent.putExtra("list result", (Serializable) importPoSplitViewModel.getBaseModelsE());
                getBaseActivity().setResult(Activity.RESULT_OK, resultIntent);
                getBaseActivity().finish();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                importPoSplitViewModel.setPallet(result);
            }
        }
        closeProcess();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = getBaseActivity().findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(R.string.split_pallet);
        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public int getLayoutRes() {
        return R.layout.fragment_import_po_split;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportPoSplitViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.splitDeltails;
    }
}
