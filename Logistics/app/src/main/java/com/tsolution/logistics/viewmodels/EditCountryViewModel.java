package com.tsolution.logistics.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.models.Country;
import com.tsolution.logistics.models.Language;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditCountryViewModel extends BaseViewModel {

    private ObservableList<Language> languages = new ObservableArrayList<>();
    private ObservableList<Country> country;

    public EditCountryViewModel(@NonNull Application application) {
        super(application);
    }
}
