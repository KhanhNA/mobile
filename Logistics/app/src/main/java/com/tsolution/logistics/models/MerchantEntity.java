package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class MerchantEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4572272641714394555L;

	// @Column(name = "country_id", nullable = false)
	// private Long countryId;

	private Country country;

	// @Column(name = "language_id", nullable = false)
	// private Long languageId;


	private Language language;

	// @Column(name = "currency_id", nullable = false)
	// private Long currencyId;


	private Currency currency;


	private String code;
	private String name;
	private String domainName;


//	private Date inBusinessSince;
	private String email;
	private String logo;
	private String address;
	private String city;
	private String phone;
	private String postalCode;
	private BigDecimal lat;
	private BigDecimal lng;

	/**
	 * 0: không hoạt động, 1: hoạt động
	 */

	private Boolean status;

}