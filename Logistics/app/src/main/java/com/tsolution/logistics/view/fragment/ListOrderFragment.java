package com.tsolution.logistics.view.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.viewmodels.ListOrderActivityViewModel;


public class ListOrderFragment extends BaseFragment {
//    private RecyclerView recyclerView;
//    private FragmentListOrderBinding orderBinding;
//    private ListOrderActivityViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
//        init(inflater, container, savedInstanceState, R.layout.fragment_list_order, ListOrderActivityViewModel.class, R.id.deltail);
//        showProcessing(R.string.wait);
//        ProgressDialog progressDialog = new ProgressDialog(getBaseActivity());
//        progressDialog.setTitle(R.string.wait);
//        progressDialog.setMessage("ABC");
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.show();

        ListOrderActivityViewModel listOrderActivityViewModel = (ListOrderActivityViewModel) viewModel;
        listOrderActivityViewModel.init();
        BaseAdapter orderAdapter = new BaseAdapter(R.layout.activity_order_info, viewModel, null);
        recyclerView.setAdapter(orderAdapter);

        //((ListOrderActivityViewModel) viewModel).refresh();
        return binding.getRoot();
    }

//    @Override
//    public void onClicked(View view, BaseViewModel vm) {
//        List lst = ((ListOrderActivityViewModel)viewModel).getCheckedItems();
//        if(lst != null) {
//            showAlertDialog(R.string.ConfirmApprove, (v, v1) -> {
//                ((ListOrderActivityViewModel) viewModel).approve(view, vm);
//            });
//        }
//
//    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if (t == null && "orderSearch".equals(action)) {
            recyclerView.getAdapter().notifyDataSetChanged();
        }
        closeProcess();
    }
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_list_order;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ListOrderActivityViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.deltail;
    }



}
