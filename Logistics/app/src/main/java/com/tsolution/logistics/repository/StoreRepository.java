package com.tsolution.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.tsolution.logistics.database.AppDatabase;
import com.tsolution.logistics.models.dao.StoreDao;
import com.tsolution.logistics.models.entity.ProductEntity;
import com.tsolution.logistics.models.entity.StoreEntity;
import com.tsolution.logistics.repository.callback.DatabaseCallback;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import lombok.Getter;
import rx.Completable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Getter
public class StoreRepository {
    private int CODE_RESPONSE_FROM_STORE = 1000;
    private StoreDao mStoreDao;
    private LiveData<List<StoreEntity>> allStore;

    public StoreRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        mStoreDao = db.storeDao();
        //allProduct = mProductDao.getAllProduct();
    }

    public void insert(StoreEntity store) {
        new StoreRepository.insertAsyncTask(mStoreDao).execute(store);
    }

    public void insert(List<StoreEntity> store) {
//        new insertAsyncTask(mProductDao).execute(product);
    }

    public void getAllStoreFromLocal() {
        allStore = mStoreDao.getAllStore();
//        Completable.fromAction(() -> {
//
//        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Completable.CompletableSubscriber() {
//            @Override
//            public void onCompleted() {
//                databaseCallback.onSuccess(CODE_RESPONSE_FROM_STORE);
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                databaseCallback.onError(CODE_RESPONSE_FROM_STORE, e);
//            }
//
//            @Override
//            public void onSubscribe(Subscription d) {
//            }
//        });
    }

    public void deleteAll() {
        Completable.fromAction(() -> {
            mStoreDao.deleteAll();
        }).subscribeOn(Schedulers.io()).subscribe();
    }

    private static class insertAsyncTask extends AsyncTask<StoreEntity, Void, Void> {

        private StoreDao mAsyncTaskDao;

        insertAsyncTask(StoreDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final StoreEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
