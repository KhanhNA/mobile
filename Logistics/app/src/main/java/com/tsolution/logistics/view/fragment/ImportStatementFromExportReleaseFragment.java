package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.viewmodels.ImportStatementFromExportReleaseViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ImportStatementFromExportReleaseFragment extends BaseFragment {
    private ImportStatementFromExportReleaseViewModel  mViewModel;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mViewModel = (ImportStatementFromExportReleaseViewModel) viewModel;
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_import_statement_from_export_release_item, viewModel, (view, baseModel) -> {
            ImportStatement importStatement = (ImportStatement) baseModel;
            Intent intent = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", importStatement);
            intent.putExtras(bundle);
            intent.putExtra("FRAGMENT", ImportStatementFromExportReleaseDetailFragment.class);
            startActivity(intent);
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.search();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if ("scanCode".equals(view.getTag())) {
            Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
            startActivityForResult(qrScan, 1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                mViewModel.setStatementCode(result);
            }
        }
        closeProcess();
    }
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_import_statement_from_export_release;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportStatementFromExportReleaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.lstImportStatementRequest;
    }
}
