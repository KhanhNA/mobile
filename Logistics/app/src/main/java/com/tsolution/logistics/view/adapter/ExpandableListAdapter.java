package com.tsolution.logistics.view.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.tsolution.logistics.utils.AppModel.MenuTitle;
import com.tsolution.logistics.R;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private Set<MenuTitle> listDataHeader;
    private Map<MenuTitle, SortedSet<MenuTitle>> listDataChild;

    public ExpandableListAdapter(Context context, Set<MenuTitle> listDataHeader,
                                 Map<MenuTitle, SortedSet<MenuTitle>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public MenuTitle getChild(int groupPosition, int childPosititon) {
        Set<MenuTitle> childs = this.listDataChild.get(listDataHeader.toArray()[groupPosition]);
        return childs.toArray(new MenuTitle[childs.size()])[childPosititon];
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = getChild(groupPosition, childPosition).getTitle();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_child, null);
        }

        TextView txtListChild = convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        if (this.listDataChild.get(listDataHeader.toArray()[groupPosition]) == null)
            return 0;
        else
            return this.listDataChild.get(listDataHeader.toArray()[groupPosition])
                    .size();
    }

    @Override
    public MenuTitle getGroup(int groupPosition) {
        return listDataHeader.toArray(new MenuTitle[0])[groupPosition];
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition).getTitle();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_header, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}