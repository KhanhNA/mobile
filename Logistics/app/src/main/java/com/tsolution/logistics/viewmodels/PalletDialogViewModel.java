package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Pageable;
import com.tsolution.logistics.models.Pallet;
import com.tsolution.logistics.utils.AppUtils;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class PalletDialogViewModel extends BaseViewModel {
    private ObservableField<String> textSearch = new ObservableField<>("");
    public PalletDialogViewModel(@NonNull Application application) {
        super(application);
    }
    public void init(){
        search();
    }

    public void search(){
        Integer page = Integer.parseInt((String) currentPage.get());
        AppUtils.callService().findPallet(page, AppUtils.pageSize, Pallet.PalletStep.IMPORT_STATEMENT_UPDATE_PALLET.getValue(), textSearch.get()).enqueue(callBack(this::getPalletSuccess, Pageable.class, Pallet.class, null));
    }


    private void getPalletSuccess(Call call, Response response, Object o, Throwable throwable) {
        Pageable pallets = (Pageable) o;
        setData(pallets.getContent());
    }
}
