package com.tsolution.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.tsolution.logistics.database.AppDatabase;
import com.tsolution.logistics.models.dao.PalletDao;
import com.tsolution.logistics.models.entity.PalletEntity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import lombok.Getter;
import rx.Completable;
import rx.schedulers.Schedulers;

@Getter
public class PalletRepository {
    private PalletDao palletDao;
    private LiveData<List<PalletEntity>> allPallet;
    private MutableLiveData<List<PalletEntity>> allPalletV2 = new MutableLiveData<>();
    public PalletRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        palletDao = db.palletDao();
    }

    public void getAllPalletFromLocal() {
        allPallet = palletDao.getAllPallet();
    }
//
    public void insert(PalletEntity entity) {
        new PalletRepository.insertAsyncTask(palletDao).execute(entity);
    }
    public void deleteAll(){
        Completable.fromAction(() -> {
            palletDao.deleteAll();
        }).subscribeOn(Schedulers.io()).subscribe();

    }
    //    public void deleteAll(DatabaseCallback databaseCallback){
//        Completable.fromAction(() -> {
//            productDescriptionDao.deleteAll();
//        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Completable.CompletableSubscriber() {
//            @Override
//            public void onCompleted() {
//                databaseCallback.onSuccess(0);
//            }
//            @Override
//            public void onError(Throwable e) {
//                databaseCallback.onError(0, e);
//            }
//
//            @Override
//            public void onSubscribe(Subscription d) {
//            }
//        });}
    private static class insertAsyncTask extends AsyncTask<PalletEntity, Void, Void> {

        private PalletDao mAsyncTaskDao;

        insertAsyncTask(PalletDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final PalletEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
