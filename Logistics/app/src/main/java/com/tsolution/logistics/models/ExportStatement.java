package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExportStatement extends Base{
    public enum ExportStatementStatus {
        // Mới tạo lệnh xuất chưa thực xuất
        REQUESTED(0),
        // Đã tạo lệnh thực xuất chưa có lệnh nhập tương ứng
        RELEASED(1),
        // Đang được thực thi
        // 2.1: Đã tạo lệnh nhập nhưng chưa thực nhập,
        // 2.2: Tạo lệnh từ Merchant_order
        PROCESSING(2),
        // Được hoàn thành thực nhập/thực xuất cho merchant
        COMPLETED(3),
        // Lệnh bị hủy
        CANCELED(4),
        // Lệnh bị trả lại từ phía nhận
        RETURNED(5);

        private int value;

        ExportStatementStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }
    private Store fromStore;
    private Store toStore;
    private String code;
    private String qRCode;
    private String description;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date importDate;
    private List<ExportStatementDetail> exportStatementDetails;
    private Long toStoreId;
    private String toStoreCode;
    private Long fromStoreId;
    private String fromStoreCode;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date exportDate;
    private Integer status;
}
