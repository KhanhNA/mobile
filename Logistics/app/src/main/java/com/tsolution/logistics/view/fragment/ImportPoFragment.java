package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportPo;
import com.tsolution.logistics.models.ImportPoDetail;
import com.tsolution.logistics.models.ImportPoDetailPallet;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.viewmodels.ImportPoViewModel;

import java.util.List;

public class ImportPoFragment extends BaseFragment {
    private ImportPoViewModel importPoViewModel;
    private int REQUEST_CODE_SPLIT = 1111;
    private int REQUEST_CODE_SCAN_CAMERA = 2222;

    //toolbar
    private Toolbar toolbar;
    BaseAdapter splitPalletAdapter;
    RecyclerView rcSplitQuantity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        importPoViewModel = (ImportPoViewModel) viewModel;

        //list splitPallet
        rcSplitQuantity = binding.getRoot().findViewById(R.id.rcSplitQuantity);
        splitPalletAdapter = new BaseAdapter(R.layout.split_quantity_item, importPoViewModel.getArrSplitPallets(), this::onPalletClick, getBaseActivity());
        GridLayoutManager layoutManager =
                new GridLayoutManager(getBaseActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        rcSplitQuantity.setLayoutManager(layoutManager);
        rcSplitQuantity.setAdapter(splitPalletAdapter);


        //list pallet
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_import_po_detail_item, importPoViewModel, (view, baseModel) -> {
            String tag = (String) view.getTag();
            ImportPoDetail importPoDetail = (ImportPoDetail) baseModel;
            importPoViewModel.setIndex(importPoDetail);
            switch (tag) {
                case "split":
                    Intent intent = new Intent(getContext(), CommonActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("importPoDetail", importPoDetail);
                    bundle.putSerializable("importPo", importPoViewModel.getImportPo());
                    intent.putExtras(bundle);
                    intent.putExtra("FRAGMENT", ImportPoSplitFragment.class);
                    startActivityForResult(intent, REQUEST_CODE_SPLIT);
                    break;
                case "scan_camera":
                    Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
                    startActivityForResult(qrScan, REQUEST_CODE_SCAN_CAMERA);
                    break;
                case "item_pallet":
                    importPoViewModel.getArrSplitPallets().setData(importPoViewModel.getSparseArray().get(importPoDetail.index -1));
                    break;

            }

        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Intent intent = activity.getIntent();
            if (intent != null && intent.hasExtra("model")) {
                ImportPo importPo = (ImportPo) intent.getSerializableExtra("model");
                importPoViewModel.setImportPo(importPo);
                importPoViewModel.init();
            }
        }

        importPoViewModel.getPallets().observe(this, palletEntities -> {
        });
        return binding.getRoot();
    }

    private void onPalletClick(View view, BaseModel baseModel) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = getBaseActivity().findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(R.string.import_po_detail);
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (REQUEST_CODE_SPLIT == requestCode) {
            if (resultCode == Activity.RESULT_OK) {
                List<ImportPoDetailPallet> result = (List<ImportPoDetailPallet>) data.getExtras().getSerializable("list result");
                importPoViewModel.onSplitResult(result);
            }
        }
        if (REQUEST_CODE_SCAN_CAMERA == requestCode) {
            if (resultCode == Activity.RESULT_OK && data!=null) {
                String result = data.getStringExtra("result");
                importPoViewModel.onSplitResult(result);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_import_po_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportPoViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.import_po_details;
    }


}
