package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;

import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.R;

import com.tsolution.logistics.models.MerchantOrderEntity;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ListOrderActivityViewModel extends BaseViewModel {
    public ListOrderActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public void refresh(){
        String code = null;
        Integer status = 0;
//        AppUtils.callService().getAllCountriesDescription().enqueue(new MyCallBack(this::handleSuccess, CountryDescription.class, List.class));
        //new BaseRepository<CountryDescription>().getAll(this, "getAllCountriesDescription", "handleSuccess", new CountryDescription());
//            ResponseBody response = AppUtils.callService().getOrders("",0).execute().body();
//        new BaseRepository<>().get(this,"getAllLanguage","handleSuccess");
        AppUtils.callService().getOrders().enqueue(callBack(this::handleSuccess, MerchantOrderEntity.class, List.class));
    }

    public void init(){
    }

    public void count(){
        MerchantOrderEntity order;
        List<MerchantOrderEntity> orders = new ArrayList<>();
        for(Object bm: (List)baseModels.get()){
            if(bm instanceof MerchantOrderEntity ){
                order = (MerchantOrderEntity)bm;
                if(order.checked != null && order.checked){
                    orders.add(order);
                }

            }
        }


    }
    public void handleSuccess(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t){
        try {
            if (t != null) {
                //processError("orderSearchErr", t);
                appException.setValue(t);
                return;
            }
//            baseModels.addAll((List) body);
            setData((List)body);
            view.action("orderSearch", null, this, null);
        }catch (Throwable t1){
            appException.setValue(t1);
        }
    }
//    public ObservableList getModels(){
//        return baseModels;
//    }
    public void approve(View view, BaseViewModel vm){

        List<BaseModel> lst = getCheckedItems();
        if(!validateData(lst))
            return;
        System.out.println("abc:");




    }
    public Boolean validateData(List lst){

        if(lst == null || lst.size() == 0){
            //loading.setValue(null);
            appException.setValue(new AppException(R.string.selectOneMore,"order"));

            return false;
        }
        if(lst.size()==1) {
            //loading.setValue(null);
            appException.setValue(new NullPointerException());
            return false;
        }
        //loading.setValue(null);
        return true;
    }

}
