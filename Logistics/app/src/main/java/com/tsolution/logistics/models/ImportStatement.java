package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImportStatement extends Base {
    public enum ImportStatementStatus {
        // Mới tạo lệnh nhập chưa thực nhập và chưa cộng kho
        NOT_UPDATE_INVENTORY(0),
        // Đã tạo lệnh thực nhập và cộng kho
        UPDATED_INVENTORY(1);

        private int value;

        ImportStatementStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    private Store fromStore;
    private Store toStore;
    private String code;
//    private String qRCode;
    private String description;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date importDate;
    private List<ImportStatementDetail> importStatementDetails;
    private Long toStoreId;
    private String toStoreCode;
    private Long fromStoreId;
    private String fromStoreCode;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date exportDate;
    private Integer status;
    private ExportStatement exportStatement;
    private Long repackingPlanningId;
}
