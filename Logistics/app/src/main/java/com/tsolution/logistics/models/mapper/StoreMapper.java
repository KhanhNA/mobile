package com.tsolution.logistics.models.mapper;

import com.tsolution.logistics.models.Store;
import com.tsolution.logistics.models.entity.StoreEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StoreMapper {
    StoreMapper INSTANCE = Mappers.getMapper( StoreMapper.class );

    @Mapping(target = "index", ignore = true)
    @Mapping(target = "checked", ignore = true)
    StoreEntity storeToStoreEntity(Store store);
}
