package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.viewmodels.ExportStatementReleaseViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CreateExportStatementReleaseFragment extends BaseFragment {
    private ExportStatementReleaseViewModel releaseViewModel;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  super.onCreateView(inflater, container, savedInstanceState);
        releaseViewModel = (ExportStatementReleaseViewModel) viewModel;
        releaseViewModel.init();
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_create_export_statement_release_item, viewModel, (view, baseModel) -> {
            ExportStatement exportStatement = (ExportStatement) baseModel;
            Intent intent = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("exportStatement", exportStatement);
            intent.putExtras(bundle);
            intent.putExtra("FRAGMENT", ExportStatementDetailReleaseFragment.class);
            startActivity(intent);
            //getBaseActivity().getSupportFragmentManager().beginTransaction().replace(R.id.framelayout, detailStatementFragment).commit();
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return v;
    }
    @Override
    public void action(View view, BaseViewModel baseViewModel) {
//        super.action(view, baseViewModel);
        String tag = (String) view.getTag();
        if("scanCode".equals(tag)){
            Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
            startActivityForResult(qrScan, 0);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                releaseViewModel.setStatementCode(result);
            }
        }
        closeProcess();
    }
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_export_statement_release;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ExportStatementReleaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.lstImportStatementRequest;
    }
}
