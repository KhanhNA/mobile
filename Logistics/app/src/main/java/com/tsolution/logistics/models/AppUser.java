package com.tsolution.logistics.models;

import androidx.annotation.StringRes;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class AppUser extends BaseModel {
    private Integer id;
    private String username;
    private String firstName;
    private String lastName;
    private String name;
    private Integer loginStatus;
    private String password;
    public AppUser() {
    }

    public void setLoginStatus(@StringRes Integer loginStatus) {
        this.loginStatus = loginStatus;
    }
}
