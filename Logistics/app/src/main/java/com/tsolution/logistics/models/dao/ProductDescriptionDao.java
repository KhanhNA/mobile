package com.tsolution.logistics.models.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.tsolution.logistics.models.entity.ProductDescriptionEntity;

import java.util.List;

@Dao
public interface ProductDescriptionDao {
    @Query("SELECT pd.*, p.quantity, p.packing_quantity " +
            "from product p " +
            "JOIN product_description pd ON p.id = pd.product_entity_id " +
            "WHERE lang_id = :langId AND product_entity_id NOT IN(:exceptId) " +
            "LIMIT :limit " +
            "OFFSET :offset")
    LiveData<List<ProductDescriptionEntity>> getProductDescription(Long langId, List<Long> exceptId, Integer limit , Integer offset );

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(ProductDescriptionEntity productDescription);

    @Query("DELETE FROM product_description")
    void deleteAll();
}
