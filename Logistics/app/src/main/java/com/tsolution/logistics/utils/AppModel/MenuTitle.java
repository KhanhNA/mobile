package com.tsolution.logistics.utils.AppModel;


import androidx.fragment.app.Fragment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MenuTitle implements Comparable<MenuTitle> {
    private String title;
    private Fragment fragment;
    private String tag;

    public MenuTitle(String title, Fragment fragment, String tag) {
        this.title = title;
        this.fragment = fragment;
        this.tag = tag;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof MenuTitle) {
            MenuTitle menuTitle = (MenuTitle) obj;
            return menuTitle.getTitle().equals(this.getTitle());
        }
        return false;
    }

    @Override
    public int compareTo(MenuTitle o) {
        if (o == null) {
            return -1;
        }
        return this.getTitle().compareTo(o.getTitle());
    }
}
