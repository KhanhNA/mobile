package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImportStatementDetail extends Base {
//    private Statement statement;
//    private ProductPacking productPacking;
//    private ProductPackingPrice productPackingPrice;
    private Long quantity;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date importDate;
    private Long productId;
    private String productCode;
    private Long packingTypeId;
    private Long productPackingId;
    private PackingType packingType;
    private String packingTypeCode;
    private String productPackingCode;
    private Integer packingTypeQuantity;
    private BigDecimal price;
    private String productName;
//    private StoreProductPackingDetail storeProductPackingDetail;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date expireDate;
    private String expireDateStr;
    private Long totalQuantity;
    private Pallet pallet;
    private Long palletId;

    public void setQuantityStr(String quantityStr) {
        this.quantity = Long.parseLong(quantityStr);
    }

    public String getQuantityStr() {
        return quantity == null ? "" : quantity.toString();
    }

    public String getExpireDateStr() {
        return expireDate == null ? "" : DateUtils.convertDateToString(expireDate, "dd/MM/yyyy");
    }
}
