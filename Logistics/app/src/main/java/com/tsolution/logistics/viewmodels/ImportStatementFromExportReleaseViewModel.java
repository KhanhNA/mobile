package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Pageable;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.utils.AppUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import retrofit2.Call;
import retrofit2.Response;

public class ImportStatementFromExportReleaseViewModel extends BaseViewModel {
    private ObservableField<String> importStatementCode = new ObservableField<>("");

    public ImportStatementFromExportReleaseViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(){

    }
    public void setStatementCode(String sCode){
        importStatementCode.set(sCode);
        search();
    }

    public void search(){
        // TODO: 8/14/2019 Call API get import statement ;
        AppUtils.callService()
                .getAll(importStatementCode.get(), ImportStatement.ImportStatementStatus.NOT_UPDATE_INVENTORY.getValue(), 1, AppUtils.pageSize)
                .enqueue(callBack(this::searchSuccess, Pageable.class, ImportStatement.class , null));
    }

    private void searchSuccess(Call call, Response response, Object o, Throwable throwable) {
        Pageable<ImportStatement> pageable = (Pageable<ImportStatement>) o;
        setPage(pageable);
        List<ImportStatement> importStatements = pageable.getContent();
        setData(importStatements);
    }

}
