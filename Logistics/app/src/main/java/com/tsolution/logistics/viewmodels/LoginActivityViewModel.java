package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.RetrofitClient;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.AppUser;
import com.tsolution.logistics.models.Language;
import com.tsolution.logistics.models.User;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class LoginActivityViewModel extends BaseViewModel<AppUser> {


    public ObservableList<Language> lstLanguage = new ObservableArrayList<>();
    public Integer selectedLanguagePosition;


    public LoginActivityViewModel(@NonNull Application application) {
        super(application);
        try {
//        TODO: 7/8/2019 xoa fix user password

            AppUser user = new AppUser();
            user.setUsername("admin");
            user.setPassword("abc@123");
            model.set(user);
            initLanguage();
        }catch (Throwable e){
            e.printStackTrace();
            if(appException != null){
                appException.postValue(e);
            }
        }

    }

    public Integer getSelectedLanguagePosition() {
        return selectedLanguagePosition;
    }



    public void login(View view) { //login
        model.get().setLoginStatus(0);
        if ("".equals(model.get().getUsername())) {
            model.get().setLoginStatus(R.string.userEmpty);
        }
        if ("".equals(model.get().getPassword())) {
            model.get().setLoginStatus(R.string.userEmpty);
        }
        if (model.get().getLoginStatus() != 0 && this.view != null) {
            appException.setValue(new AppException(model.get().getLoginStatus(), "loginError"));
            return;
        }
        processing.setValue(null);
//        AppUtils.requestLogin(appException, model, this::getRole);
        RetrofitClient.requestLogin(appException,model.get().getUsername(),model.get().getPassword(), this::getRole);
    }

    private void initLanguage() {
        AppUtils.callService().getAllLanguages().enqueue(callBack(this::success, Language.class, List.class));
    }

    private void getRole(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {

        AppUtils.getApiServiceAuth().getUser().enqueue(callBack(this::getRoleSuccess, User.class, null));
    }

    private void success(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {
        if(body!=null){
            lstLanguage.clear();
            lstLanguage.addAll((List) body);
        }
        else{
            appException.setValue(t);
        }
    }

    private void getRoleSuccess(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {
        try {
            AppUtils.user = (User) body;
            if (selectedLanguagePosition != null) {
                AppUtils.language = lstLanguage.get(selectedLanguagePosition).getId();
            }
            view.action("startHome", null, this, null);
        } catch (Throwable ex) {
            appException.setValue(ex);
        }
    }
}
