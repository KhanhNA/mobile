package com.tsolution.logistics.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.viewmodels.EditCountryDescriptionTabViewModel;

public class EditCountryDescriptionFragment extends BaseFragment {

    private EditCountryDescriptionTabViewModel editCountryDescriptionTabViewModel;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        editCountryDescriptionTabViewModel = (EditCountryDescriptionTabViewModel) viewModel;
        view = binding.getRoot();
        return view;
    }

    //    public EditCountryDescriptionFragment() {
//    }
//
//    public EditCountryDescriptionFragment(ObservableList<Country> country, int position) {
//        this.country = country;
//        this.position = position;
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        descriptionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tab_edit_country_description, container, false);
//        descriptionBinding.setLifecycleOwner(this);
//        viewModel = ViewModelProviders.of(this).get(EditCountryDescriptionTabViewModel.class);
//        viewModel.setCountry(country);
//        viewModel.setPosition(position);
//        init();
//        descriptionBinding.setViewModel(viewModel);
//        return descriptionBinding.getRoot();
//    }
//
//    private void init() {
//        this.viewModel.getCountry().observe(this, country -> {
//            if (viewModel.getCountry().getValue().getCountryDescriptions() != null) {
//                viewModel.getDescription().setValue(viewModel.getCountry().getValue().getCountryDescriptions().get(viewModel.getPosition()));
//            } else {
//                viewModel.getDescription().setValue(new CountryDescription("", "", ""));
//            }
//        });
//        this.viewModel.getDescription().observe(this, new Observer<CountryDescription>() {
//            @Override
//            public void onChanged(@Nullable CountryDescription countryDescription) {
//                if (viewModel.getCountry().getValue().getCountryDescriptions() != null) {
//                    viewModel.getCountry().getValue().getCountryDescriptions().get(viewModel.getPosition()).setTitle(countryDescription.getTitle());
//                    viewModel.getCountry().getValue().getCountryDescriptions().get(viewModel.getPosition()).setName(countryDescription.getName());
//                    viewModel.getCountry().getValue().getCountryDescriptions().get(viewModel.getPosition()).setDescription(countryDescription.getDescription());
//                }
//            }
//        });
//    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_tab_edit_country_description;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return EditCountryDescriptionTabViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

}
