package com.tsolution.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.tsolution.logistics.database.AppDatabase;
import com.tsolution.logistics.models.dao.ProductDao;
import com.tsolution.logistics.models.entity.ProductEntity;
import com.tsolution.logistics.repository.callback.DatabaseCallback;
import com.tsolution.logistics.utils.AppUtils;

import java.util.List;

import lombok.Getter;
import rx.Completable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Getter
public class ProductRepository {
    private ProductDao mProductDao;
    private LiveData<List<ProductEntity>> allProduct;
    public ProductRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        mProductDao = db.productDao();
        //allProduct = mProductDao.getAllProduct();
    }
    public void insert(ProductEntity product) {
        new insertAsyncTask(mProductDao).execute(product);
    }
    public void insert(List<ProductEntity> product) {
//        new insertAsyncTask(mProductDao).execute(product);
    }
    public void search(List<Long> exceptId, int page){
        long[] ids = new long[exceptId.size()];
        for(int i  = 0; i < exceptId.size(); i++){
            ids[i] = exceptId.get(i);
        }
        allProduct = mProductDao.getAllProduct(ids, AppUtils.pageSize, AppUtils.pageSize * (page - 1));
    }
    public void deleteAll(DatabaseCallback databaseCallback){
        Completable.fromAction(() -> mProductDao.deleteAll()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Completable.CompletableSubscriber() {
            @Override
            public void onCompleted() {
                databaseCallback.onSuccess(1);
            }
            @Override
            public void onError(Throwable e) {
                databaseCallback.onError(1, e);
            }

            @Override
            public void onSubscribe(Subscription d) {
            }
        });
    }
    private static class insertAsyncTask extends AsyncTask<ProductEntity, Void, Void> {

        private ProductDao mAsyncTaskDao;

        insertAsyncTask(ProductDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ProductEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

}
