package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import androidx.lifecycle.MutableLiveData;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.PackingType;
import com.tsolution.logistics.models.Product;
import com.tsolution.logistics.models.Statement;
import com.tsolution.logistics.models.StatementDetail;
import com.tsolution.logistics.models.Store;
import com.tsolution.logistics.models.StoreProductPacking;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class CreateStatementViewModel extends BaseViewModel {
    // du lieu luu lai
    private MutableLiveData<Statement> statement = new MutableLiveData<>();
    private Store fromStore = new Store();
    private Store toStore = new Store();
    private ObservableField<Integer> selectedPackingTypePosition = new ObservableField<>();
    private Integer selectedFromStorePosition;
    private Integer selectedToStorePosition;

    //du lieu da chon
    private List<Long> exceptId = new ArrayList<>();
    // hien thi tab
    private List<Product> products = new ArrayList<>();
    private ObservableField<StatementDetail> statementDetail = new ObservableField<>();
    // data from server
    private ObservableList<PackingType> lstPackingType = new ObservableArrayList<>();
    private ObservableList<Store> lstFromStore = new ObservableArrayList<>();
    private ObservableList<Store> lstToStore = new ObservableArrayList<>();

    private ObservableField<Integer> index = new ObservableField<>();

    // type
    private String type;

    public CreateStatementViewModel(@NonNull Application application) {
        super(application);
    }


    public void setSelectedFromStorePosition(Integer selectedFromStorePosition) {
        this.selectedFromStorePosition = selectedFromStorePosition;
        if (baseModels.get() != null)
            ((List) baseModels.get()).clear();
        getToStore();
    }

    private void clearData() {
//        baseModels.clear();
        if (baseModels.get() != null)
            ((List) baseModels.get()).clear();
        lstPackingType.clear();
        lstToStore.clear();
        lstFromStore.clear();
        selectedPackingTypePosition.set(0);
        selectedFromStorePosition = 0;
        selectedToStorePosition = 0;
    }

    public void init() {
        clearData();
        index.set(-1);
        statement.setValue(new Statement());
        exceptId = new ArrayList<>();
        exceptId.add(-1L);
        statementDetail.set(new StatementDetail());
        getPackingType();
        baseModels.set(new ArrayList<>());
        if (isImportFC() == 1) {
            getFromStock();
        } else {
            getStoreFC();
        }
    }

    private void getFromStock() {
        ArrayList<Long> exceptIds = new ArrayList<>();
        exceptIds.add(-1L);
        AppUtils.callService().getFromStore(true, true, exceptIds).enqueue(callBack(this::successFromStore, Store.class, List.class));
    }

    private void getStoreFC() {
        ArrayList<Long> exceptIds = new ArrayList<>();
        exceptIds.add(-1L);
        AppUtils.callService().getStoreFC().enqueue(callBack(this::success, Store.class, null));
    }

    private void getToStore() {
        if (fromStore != null) {
            ArrayList<Long> exceptIds = new ArrayList<>();
            exceptIds.add(-1L);
            fromStore = lstFromStore.get(selectedFromStorePosition);
            exceptIds.add(fromStore.getId());
            AppUtils.callService().getToStore(true, true, true, exceptIds).enqueue(callBack(this::successToStore, Store.class, List.class));
        }
    }

    private void getPackingType() {
        AppUtils.callService().getPackingType(true).enqueue(callBack(this::packingTypeSuccess, PackingType.class, List.class));
    }

    public Integer isImportFC() {
        if ("ExportStock".equals(type)) {
            return 1;
        }
        if ("ImportFC".equals(type)) {
            return 0;
        }
        return -1;
    }

    public void onCreate() {
        processing.setValue(R.string.wait);
        Log.e("NMQ", "NMQ");
        if (statement.getValue() != null) {
            statement.getValue().setToStoreId(toStore.getId());
            statement.getValue().setToStoreCode(toStore.getCode());
        }
        if ("ImportFC".equals(type)) {
            statement.getValue().setImportStatementDetails((List) baseModels.get());
            AppUtils.callService().createImportStatementFC(statement.getValue()).enqueue(callBack(this::createSuccess, Statement.class, null));
        } else if ("ExportStock".equals(type)) {
            fromStore = lstFromStore.get(selectedFromStorePosition);
            toStore = lstToStore.get(selectedToStorePosition);
            statement.getValue().setExportStatementDetails((List) baseModels.get());
            statement.getValue().setFromStoreId(fromStore.getId());
            statement.getValue().setFromStoreCode(fromStore.getCode());
            statement.getValue().setToStoreId(toStore.getId());
            statement.getValue().setToStoreCode(toStore.getCode());
            //AppUtils.callService().exportStatement(statement.getValue()).enqueue(callBack(this::exportStatementSuccess, Statement.class, null));
        }
        Log.e("NMQ", "NMQ");
    }

    private void exportStatementSuccess(Call call, Response response, Object o, Throwable throwable) {
        processing.setValue(null);
        Statement statementSuccess = (Statement) o;
        if (statementDetail != null && statementSuccess.getId() != null) {
            // TODO: 7/16/2019 Hien thi thong bao thanh cong
            appException.setValue(new AppException(R.string.success, "product_info_error"));
            Log.e("NMQ", "Thong bao thanh cong");
        }
    }

    private void createSuccess(Call call, Response response, Object o, Throwable throwable) {
        processing.setValue(null);
        Statement statementSuccess = (Statement) o;
        if (statementDetail != null && statementSuccess.getId() != null) {
            // TODO: 7/16/2019 Hien thi thong bao thanh cong

            appException.setValue(new AppException(R.string.success, "product_info_error"));
            Log.e("NMQ", "Thong bao thanh cong");
        }
    }

    public void setDate(Date date) {
        statementDetail.get().setExpireDate(date);
        statementDetail.notifyChange();
    }

    private void initProduct() {
        fromStore = lstFromStore.get(selectedFromStorePosition);
        if (fromStore != null) {
            products.clear();
            List<StoreProductPacking> storeProductPackings = fromStore.getStoreProductPackings();

            for (StoreProductPacking packing : storeProductPackings) {
                int i = findProductInList(packing);
                if (i == -1) {
                    Product product = new Product();
                    product.setTotalQuantity(packing.getTotalQuantity());
                    product.setPackingQuantity(packing.getProductPacking().getPackingType().getQuantity());
                    product.setPackingId(packing.getProductPacking().getPackingType().getId());
                    product.setProductDescriptions(packing.getProduct().getProductDescriptions());
                    product.setCode(packing.getProduct().getCode());
                    product.setName(packing.getProduct().getName());
                    product.setId(packing.getProduct().getId());
                    products.add(product);
                } else {
                    Product product = products.get(i);
                    product.setTotalQuantity(product.getTotalQuantity() + packing.getTotalQuantity());
                }
            }
        }
    }

    private void clearDatabase() {

    }

    private int findProductInList(StoreProductPacking productPacking) {
        Product product = productPacking.getProduct();
        int stt = 0;
        Long id = productPacking.getProductPacking().getPackingType().getId();
        for (Product p : products) {
            if (product.getId().equals(p.getId()) && id.equals(p.getPackingId())) {
                return stt;
            }
            stt++;
        }
        return -1;
    }


    //invoke function
    public void editItem(StatementDetail statementDetail) {
        processing.setValue(null);
        index.set(exceptId.indexOf(statementDetail.getProductId()));
        Integer position = index.get();
        if (position!=null && position != -1) {
            this.statementDetail.set(statementDetail);
            selectedPackingTypePosition.set(getPositionPackingType());
        }
    }

    //invoke function
    public void deleteItem(StatementDetail statementDetail) {
        processing.setValue(null);
        index.set(exceptId.indexOf(statementDetail.getProductId()));
        Integer position = index.get();
        if (position!=null && position != -1) {
            List list = (List) baseModels.get();
            if(list!=null){
                list.remove(position - 1);
//                updateModelInfo(list);
                baseModels.notifyChange();

            }
            exceptId.remove(position.intValue());
        }
    }

    private void changePackingType() {
        StatementDetail statementDetail = this.statementDetail.get();
        Integer position = selectedPackingTypePosition.get();
        if(statementDetail!=null && position !=null){
            statementDetail.setPackingType(lstPackingType.get(position));
            statementDetail.setPackingTypeCode(lstPackingType.get(position).getCode());
            statementDetail.setPackingTypeId(lstPackingType.get(position).getId());
        }

    }

    private boolean invalid() {
        StatementDetail sd = this.statementDetail.get();
        if (isImportFC() == 0) {
            return sd != null && sd.getProductId() != null && sd.getProductCode() != null && sd.getQuantity() != null && sd.getExpireDate() != null && sd.getQuantity() % sd.getPackingType().getQuantity() == 0;
        } else {
            return sd != null && sd.getProductId() != null && sd.getProductCode() != null && sd.getQuantity() != null && sd.getQuantity() <= sd.getTotalQuantity();
        }

    }

    // invokeFunc
    public void onAddOrUpdate() {
        StatementDetail statementDetail = this.statementDetail.get();
        if(statementDetail!=null){
            index.set(exceptId.indexOf(statementDetail.getProductId()));
            changePackingType();
            if (invalid()) {
                List listBaseModels = (List) baseModels.get();
                if(listBaseModels!=null){
                    Integer position = index.get();
                    if (position!=null && position == -1) {
                        listBaseModels.add(statementDetail);
//                        updateModelInfo(listBaseModels);
                        baseModels.notifyChange();
                        exceptId.add(statementDetail.getProductId());
                    } else {
                        //thonv
//                baseModels.set(baseModels.indexOf(statementDetail.get()), statementDetail.get());
                        listBaseModels.set(listBaseModels.indexOf(statementDetail), statementDetail);
                        baseModels.notifyChange();
                    }
                }
                this.statementDetail.set(new StatementDetail());
                selectedPackingTypePosition.set(0);
            } else {
                Log.e("NMQ", "Thong bao loi");
                appException.setValue(new AppException(R.string.product_info_error, "product_info_error"));
                // TODO: 7/18/2019 Show message error
            }
        }

    }

    private void successFromStore(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {
        if (body != null) {
            lstFromStore.clear();
            lstFromStore.addAll((List) body);
        } else {
            appException.setValue(new AppException(R.string.error_process, "product_info_error"));
        }
    }

    private void successToStore(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {
        lstToStore.clear();
        lstToStore.addAll((List) body);
        initProduct();
    }

    private void success(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {
        this.toStore = (Store) body;
    }

    private void packingTypeSuccess(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {
        lstPackingType.clear();
        lstPackingType.addAll((List) body);
    }

    private int getPositionPackingType() {
        int stt = 0;
        for (PackingType packingType : lstPackingType) {
            StatementDetail statementDetail = this.statementDetail.get();
            if(statementDetail!=null){
                if (packingType.getId().equals(statementDetail.getPackingTypeId())) {
                    return stt;
                }
                stt++;
            }

        }
        return 0;
    }

}

