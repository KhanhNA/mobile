package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Pageable;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.models.ExportStatement;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class ExportStatementReleaseViewModel extends BaseViewModel {
    private ObservableField<String> exportStatementCode = new ObservableField<>("");

    public ExportStatementReleaseViewModel(@NonNull Application application) {
        super(application);
    }

    public void setStatementCode(String sCode) {
        exportStatementCode.set(sCode);
        search();
    }

    public void search() {
        // TODO: 8/14/2019 Call API get statement export;

        Integer page = Integer.parseInt((String) currentPage.get());
        AppUtils.callService()
                .findImportStatementByCode(exportStatementCode.get(), ExportStatement.ExportStatementStatus.REQUESTED.getValue(), page, AppUtils.pageSize)
                .enqueue(callBack(this::searchSuccess, Pageable.class, ExportStatement.class, null));
    }

    private void searchSuccess(Call call, Response response, Object body, Throwable throwable) {
        Pageable<ExportStatement> statement = (Pageable<ExportStatement>) body;
        setPage(statement);
        List<ExportStatement> exportStatements = statement.getContent();
        setData(exportStatements);
    }
}
