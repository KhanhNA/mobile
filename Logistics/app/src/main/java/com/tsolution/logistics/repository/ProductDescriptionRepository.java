package com.tsolution.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.tsolution.logistics.database.AppDatabase;
import com.tsolution.logistics.models.dao.ProductDescriptionDao;
import com.tsolution.logistics.models.entity.ProductDescriptionEntity;
import com.tsolution.logistics.repository.callback.DatabaseCallback;
import com.tsolution.logistics.utils.AppUtils;

import java.util.List;

import androidx.lifecycle.LiveData;
import lombok.Getter;
import rx.Completable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Getter
public class ProductDescriptionRepository {
    private ProductDescriptionDao productDescriptionDao;
    private LiveData<List<ProductDescriptionEntity>> allProductDescription;

    public ProductDescriptionRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        productDescriptionDao = db.productDescriptionDao();
    }

    public void searchAll(List<Long> exceptId, Integer pageNumber){
        allProductDescription = productDescriptionDao.getProductDescription(AppUtils.language, exceptId, AppUtils.pageSize, AppUtils.pageSize*(pageNumber - 1));
    }
    public void insert(ProductDescriptionEntity entity) {
        new ProductDescriptionRepository.insertAsyncTask(productDescriptionDao).execute(entity);
    }
    public void deleteAll(DatabaseCallback databaseCallback){
        Completable.fromAction(() -> {
            productDescriptionDao.deleteAll();
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Completable.CompletableSubscriber() {
            @Override
            public void onCompleted() {
                databaseCallback.onSuccess(0);
            }
            @Override
            public void onError(Throwable e) {
                databaseCallback.onError(0, e);
            }

            @Override
            public void onSubscribe(Subscription d) {
            }
        });}
    private static class insertAsyncTask extends AsyncTask<ProductDescriptionEntity, Void, Void> {

        private ProductDescriptionDao mAsyncTaskDao;

        insertAsyncTask(ProductDescriptionDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ProductDescriptionEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
