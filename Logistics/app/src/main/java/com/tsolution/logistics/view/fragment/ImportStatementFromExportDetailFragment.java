package com.tsolution.logistics.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.viewmodels.ImportStatementFromExportDetailViewModel;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

public class ImportStatementFromExportDetailFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        FragmentActivity activity = getActivity();
        if(activity!=null){
            Intent intent = activity.getIntent();
            if(intent!=null && intent.hasExtra("model")){
                ExportStatement model = (ExportStatement) intent.getSerializableExtra("model");
                ImportStatementFromExportDetailViewModel mViewModel = (ImportStatementFromExportDetailViewModel) viewModel;
                mViewModel.init(model);
            }
        }

        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_import_statement_from_export_detail_item, viewModel, (view, baseModel) -> {}, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_import_statement_from_export_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportStatementFromExportDetailViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.statementDetails;
    }
}
