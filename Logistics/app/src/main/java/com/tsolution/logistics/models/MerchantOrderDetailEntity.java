package com.tsolution.logistics.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class MerchantOrderDetailEntity extends SuperEntity implements Serializable {


//	private MerchantOrderEntity merchantOrder;


	private Product product;
	private PackingType packingType;
	private Long quantity;
	private BigDecimal price;

	/**
	 * 0: from_store không đủ hàng, 1: from_store đủ hàng
	 */

	private Boolean status;




}