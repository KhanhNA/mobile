package com.tsolution.logistics.utils.DateTimePickerUtils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.tsolution.base.BaseModel;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public interface GetDate {
        void getDateTime(Date date, BaseModel baseModel);
    }

    GetDate mGetDate;
    BaseModel baseModel;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        baseModel = (BaseModel) getArguments().getSerializable("baseModel");
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        populateSetDate(year, month , dayOfMonth);
    }

    public void populateSetDate(int year, int month, int day) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(year, month, day);
        mGetDate.getDateTime(calendar.getTime(), baseModel);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void init(GetDate getDate) {
        this.mGetDate = getDate;
    }

}
