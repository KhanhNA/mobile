package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.base.BaseModel;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Base extends BaseModel {
    protected Long id;
    @JsonFormat(pattern = AppUtils.dateFormat)
    protected Date createDate;
    protected String createUser;
    @JsonFormat(pattern = AppUtils.dateFormat)
    protected Date updateDate;
    protected String updateUser;
}
