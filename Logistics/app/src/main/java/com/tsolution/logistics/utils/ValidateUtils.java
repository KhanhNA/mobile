package com.tsolution.logistics.utils;

import java.util.List;

public class ValidateUtils {
    public static boolean isNullOrEmpty(List list) {
        return list == null || list.size() == 0;
    }
    public static boolean isNullOrEmpty(String str) {
        return str == null || "".equals(str.trim());
    }

}
