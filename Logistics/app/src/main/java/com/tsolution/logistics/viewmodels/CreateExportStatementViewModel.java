package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.util.Log;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.utils.ValidateUtils;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.models.ExportStatementDetail;
import com.tsolution.logistics.models.PackingType;
import com.tsolution.logistics.models.Product;
import com.tsolution.logistics.models.ProductDescription;
import com.tsolution.logistics.models.Store;
import com.tsolution.logistics.models.StoreProductPacking;
import com.tsolution.logistics.models.entity.ProductDescriptionEntity;
import com.tsolution.logistics.models.entity.ProductEntity;
import com.tsolution.logistics.models.mapper.ExportStatementDetailMapper;
import com.tsolution.logistics.models.mapper.ProductDescriptionMapper;
import com.tsolution.logistics.models.mapper.ProductMapper;
import com.tsolution.logistics.repository.callback.DatabaseCallback;
import com.tsolution.logistics.repository.ProductDescriptionRepository;
import com.tsolution.logistics.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import lombok.Getter;
import lombok.Setter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class CreateExportStatementViewModel extends BaseViewModel implements DatabaseCallback {
    private Store fromStore = new Store();
    private Store toStore = new Store();
    private MutableLiveData<ExportStatement> statement = new MutableLiveData<>();
    private Integer selectedFromStorePosition;
    private Integer selectedToStorePosition;
    private ObservableList<Store> lstFromStore = new ObservableArrayList<>();
    private ObservableList<Store> lstToStore = new ObservableArrayList<>();
    private ObservableField<String> statementQuantity = new ObservableField<>();
    private ObservableField<ProductDescriptionEntity> currentProduct = new ObservableField<>();
    private ObservableField<Integer> index = new ObservableField<>();
    private ObservableField<Integer> selectedPackingTypePosition = new ObservableField<>();
    private ObservableList<PackingType> lstPackingType = new ObservableArrayList<>();
    private List<Long> exceptId;
    private ProductRepository productRepository;
    private ProductDescriptionRepository productDescriptionRepository;

    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    private LiveData<List<ProductDescriptionEntity>> allProductDescription;
//    ProductRepository productRepository = new ProductRepository(getApplication());

    public void init() {
        index.set(-1);
        statement.setValue(new ExportStatement());
        exceptId = new ArrayList<>();
        getPackingType();
        baseModels.set(new ArrayList<>());
        getFromStore();

        productRepository = new ProductRepository(getApplication());
        productDescriptionRepository = new ProductDescriptionRepository(getApplication());
        allProductDescription = productDescriptionRepository.getAllProductDescription();

    }

    private void getPackingType() {
        AppUtils.callService().getPackingType(true).enqueue(callBack(this::packingTypeSuccess, PackingType.class, List.class));
    }

    private void packingTypeSuccess(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {
        lstPackingType.clear();
        lstPackingType.addAll((List) body);
    }

    public void setSelectedFromStorePosition(Integer selectedFromStorePosition) {
        this.selectedFromStorePosition = selectedFromStorePosition;
        if (baseModels.get() != null){
            ((List) baseModels.get()).clear();
        }
        fromStore = lstFromStore.get(selectedFromStorePosition);
        getInfoFromStore();
    }

    private void getInfoFromStore() {
        AppUtils.callService().findStoreById(fromStore.getId()).enqueue(callBack(this::findInfoFromStoreSuccess, Store.class, null));
    }

    private void findInfoFromStoreSuccess(Call call, Response response, Object o, Throwable throwable) {
        fromStore = (Store) o;
        getToStore();
        initProduct();
    }


    private void getToStore() {
        if(fromStore!=null){
            ArrayList<Long> exceptIds = new ArrayList<>();
            exceptIds.add(-1L);
            exceptIds.add(fromStore.getId());
            AppUtils.callService().getToStore(true, true, true, exceptIds).enqueue(callBack(this::successToStore, Store.class, List.class));
        }
    }

    private void getFromStore() {
        ArrayList<Long> exceptIds = new ArrayList<>();
        exceptIds.add(-1L);
        AppUtils.callService().getFromStore(true, true, exceptIds).enqueue(callBack(this::successFromStore, Store.class, List.class));
    }

    private void successToStore(Call<ResponseBody> call, Response<ResponseBody> response, Object body, Throwable t) {
        lstToStore.clear();
        lstToStore.addAll((List) body);
    }

    private void successFromStore(Call call, Response response, Object body, Throwable throwable) {
        if (body != null) {
            lstFromStore.clear();
            lstFromStore.addAll((List) body);
        } else {
            appException.setValue(new AppException(R.string.error_process, "product_info_error"));
        }

    }

    private void initProduct() {
        currentProduct.set(new ProductDescriptionEntity());
        clearDataBase();
    }

    private void addProductToLocalDb() {
        List<StoreProductPacking> storeProductPackings = fromStore.getStoreProductPackings();
        for (StoreProductPacking storeProductPacking : storeProductPackings) {
            PackingType packingType = storeProductPacking.getProductPacking().getPackingType();
            Product product = storeProductPacking.getProduct();
            product.setQuantity(storeProductPacking.getTotalQuantity());
            product.setPackingQuantity(packingType.getQuantity());
            ProductEntity productEntity = ProductMapper.INSTANCE.productDtoToProduct(product);
            productRepository.insert(productEntity);
            for (ProductDescription description : product.getProductDescriptions()) {
                description.index = 1;
                description.checked = true;
                description.setPackingTypeId(packingType.getId());
                ProductDescriptionEntity descriptionEntity = ProductDescriptionMapper.INSTANCE.productDescriptionDtoToProductDescription(description);
                productDescriptionRepository.insert(descriptionEntity);
            }
        }
    }

    private void clearDataBase() {
        productDescriptionRepository.deleteAll(this);
    }

    public CreateExportStatementViewModel(@NonNull Application application) {
        super(application);
    }

    private void productToStatementDetail(ExportStatementDetail exportStatementDetail) {
        String quantity = statementQuantity.get();
        exportStatementDetail.setQuantity(Long.parseLong(ValidateUtils.isNullOrEmpty(quantity) ? "0" : quantity));
    }

    public void onAddOrUpdate() {
        ProductDescriptionEntity productDescription = currentProduct.get();
        if (productDescription != null) {
            ExportStatementDetail exportStatementDetail = ExportStatementDetailMapper.INSTANCE.productDescriptionToExportStatementDetail(productDescription);
            productToStatementDetail(exportStatementDetail);
            if (validate()) {
                Integer position = index.get();
                if (position != null) {
                    List listBaseModels = (List) baseModels.get();
                    if (position == -1) {
                        listBaseModels.add(exportStatementDetail);
                        baseModels.notifyChange();
                        exceptId.add(exportStatementDetail.getProductId());
                    } else {
                        listBaseModels.set(position, exportStatementDetail);
                        baseModels.notifyChange();
                    }
                    statementQuantity.set("");
                    index.set(-1);
                    currentProduct.set(new ProductDescriptionEntity());
                }
            } else {
                Log.e("NMQ", "Thong bao loi");
                appException.setValue(new AppException(R.string.product_info_error, "product_info_error"));
                // TODO: 7/18/2019 Show message error
            }
        }
    }

    private boolean validate() {
        ProductDescriptionEntity entity = currentProduct.get();
        String s = statementQuantity.get();
        Long quantity = ValidateUtils.isNullOrEmpty(s) ? 0 : Long.parseLong(s);
        return entity != null && entity.getId() != null &&
                entity.getPackingId() != null && entity.getCode() != null &&
                entity.getQuantity() >= quantity;
    }

    public void onCreate() {
        Log.e("NMQ", "NMQ");
        ExportStatement exportStatement = statement.getValue();
        List<ExportStatementDetail> listBaseModels = (List<ExportStatementDetail>) baseModels.get();
        if (exportStatement != null && listBaseModels != null) {
            setStoreForStatement();
            exportStatement.setStatus(ExportStatement.ExportStatementStatus.REQUESTED.getValue());
            exportStatement.setExportStatementDetails(listBaseModels);
            List<ExportStatement> exportStatements = new ArrayList<>();
            exportStatements.add(exportStatement);
            AppUtils.callService().exportStatement(exportStatements).enqueue(callBack(this::statementSuccess, ExportStatement.class, List.class));
        }
    }

    private void statementSuccess(Call call, Response response, Object body, Throwable throwable) {
        if (body != null) {
            Log.e("NMQ", "NMQ");
            appException.setValue(new AppException(R.string.success, "success"));
        }
    }

    private void setStoreForStatement() {
        fromStore = lstFromStore.get(selectedFromStorePosition);
        toStore = lstToStore.get(selectedToStorePosition);
        ExportStatement exportStatement = statement.getValue();
        exportStatement.setFromStoreId(fromStore.getId());
        exportStatement.setFromStoreCode(fromStore.getCode());
        exportStatement.setToStoreId(toStore.getId());
        exportStatement.setToStoreCode(toStore.getCode());
    }

    public void editItem(ExportStatementDetail exportStatementDetail) {

        index.set(exceptId.indexOf(exportStatementDetail.getProductId()));
        currentProduct.set(ExportStatementDetailMapper.INSTANCE.statementDetailToProduct(exportStatementDetail));
        statementQuantity.set(exportStatementDetail.getQuantityStr());
    }

    public void deleteItem(ExportStatementDetail exportStatementDetail) {
        int position = exceptId.indexOf(exportStatementDetail.getProductId());
        if (position != -1) {
            List list = (List) baseModels.get();
            if (list != null) {
                list.remove(position);
                baseModels.set(list);
                baseModels.notifyChange();
            }
            exceptId.remove(position);
        }

    }

    @Override
    public void onSuccess(int code) {
        if (code == 0) {
            productRepository.deleteAll(this);
        } else if (code == 1) {
            addProductToLocalDb();
        }
    }

    @Override
    public void onError(int code, Throwable e) {
        appException.setValue(new AppException(R.string.error_process, "error_process"));
    }
}
