package com.tsolution.logistics.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.models.ImportStatementDetail;
import com.tsolution.logistics.models.PackingType;
import com.tsolution.logistics.models.entity.ProductDescriptionEntity;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateStatementImportFCViewModel extends BaseViewModel {
    private ImportStatement statement;
    private ImportStatementDetail statementDetail;
    private ObservableList<PackingType> lstPackingType = new ObservableArrayList<>();
    private ObservableField<Integer> selectedPackingTypePosition = new ObservableField<>();
    private ObservableField<ProductDescriptionEntity> currentProduct = new ObservableField<>();
    private List<Long> exceptId;
    private int index = -1;
    public CreateStatementImportFCViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(){
        exceptId = new ArrayList<>();
        exceptId.add(-1L);
    }


    public void onAddOrUpdate(){

    }

    public void onCreate(){

    }
}
