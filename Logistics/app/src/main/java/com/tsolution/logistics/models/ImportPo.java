package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImportPo extends Base {
	public enum ImportPoStatus {
		NEW(0), IN_PALLET(1), OUT_OF_PALLET(2);

		private Integer value;

		ImportPoStatus(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return this.value;
		}
	}
	private Manufacturer manufacturer;

	private String code;

	private String qRCode;

	private String description = "NULL";

	private Po po;

	private List<ImportPoDetail> importPoDetails;

	private Integer status;
}