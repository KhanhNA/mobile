package com.tsolution.logistics.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tsolution.base.BaseDialogFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.viewmodels.EditCountryViewModel;

public class EditCountryFragment extends BaseDialogFragment {

    View view;
    EditCountryViewModel editCountryViewModel;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        editCountryViewModel = (EditCountryViewModel) viewModel;
        view = binding.getRoot();
        return view;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_edit_country_description;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return EditCountryViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
