package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.content.Intent;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportPo;
import com.tsolution.logistics.models.ImportPoDetail;
import com.tsolution.logistics.models.ImportPoDetailPallet;
import com.tsolution.logistics.models.Pallet;
import com.tsolution.logistics.models.StatementDetail;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.utils.ValidateUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class ImportPoSplitViewModel extends BaseViewModel {

    private ObservableField<String> quantityStr = new ObservableField<>("");
    private ObservableField<ImportPoDetailPallet> importPoDetailPallet = new ObservableField<>();
    private ImportPoDetail importPoDetail;
    private ImportPo importPo;
    private List<Pallet> pallets = new ArrayList<>();
    private ObservableField<Integer> index = new ObservableField<>(-1);
    private ObservableField<Pallet> pallet = new ObservableField<>();
    private ObservableField<Long> sum = new ObservableField<>(0L);

    public ImportPoSplitViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        setData(new ArrayList<>());
        AppUtils.callService().findAllPallet().enqueue(callBack(this::getSuccess, Pallet.class, List.class));
    }

    private void getSuccess(Call call, Response response, Object body, Throwable throwable) {
        pallets = (List<Pallet>) body;
    }

    public void setPallet(String code) {
        if (pallets != null) {
            for (Pallet pallet : pallets) {
                if (pallet.getCode().equals(code)) {
                    this.pallet.set(pallet);
                    return;
                }
            }
        }
    }

    public void editItem(ImportPoDetailPallet importPoDetailPallet) {
        List<ImportPoDetailPallet> importPoDetailPallets = getBaseModelsE();
        int stt = importPoDetailPallets.lastIndexOf(importPoDetailPallet);
        index.set(stt);
        quantityStr.set(importPoDetailPallet.getQuantity().toString());
        pallet.set(importPoDetailPallet.getPallet());
    }

    public void deleteItem(ImportPoDetailPallet importPoDetailPallet) {
        List<ImportPoDetailPallet> importPoDetailPallets = getBaseModelsE();
        int stt = importPoDetailPallets.lastIndexOf(importPoDetailPallet);
        Integer position = index.get();
        Long s = sum.get();
        if (s != null) {
            sum.set(s - importPoDetailPallet.getQuantity());
        }
        importPoDetailPallets.remove(importPoDetailPallet);
        baseModels.notifyChange();
        if(position!=null && position == stt) {
            pallet.set(null);
            quantityStr.set("");
            index.set(-1);
        }
    }

    public void onAddOrUpdate() {
        Integer stt = index.get();
        String quantity = quantityStr.get();
        Long number = sum.get();
        List<ImportPoDetailPallet> importPoDetailPallets = getBaseModelsE();
        if (!ValidateUtils.isNullOrEmpty(quantity) && pallet.get() != null) {
            if (stt != null && stt == -1 && number != null) {
                if(number + Long.parseLong(quantity) > importPoDetail.getQuantity()){
                    showMessage(R.string.product_info_error, "product_info_error");
                    return;
                }
                ImportPoDetailPallet importPoDetailPallet = new ImportPoDetailPallet();
                // quantity
                quantityStr.set("");
                importPoDetailPallet.setQuantity(Long.parseLong(quantity));

                sum.set(number + Long.parseLong(quantity));

                // import po detail
                // Tránh đệ quy
                ImportPoDetail importPoDetail = new ImportPoDetail();
                importPoDetail.setId(this.importPoDetail.getId());
                importPoDetailPallet.setImportPoDetail(importPoDetail);

                //pallet
                importPoDetailPallet.setPallet(pallet.get());

                //import po
                // Tranh de quy
                ImportPo importPo = new ImportPo();
                importPo.setId(this.importPo.getId());
                importPoDetailPallet.setImportPo(importPo);

                importPoDetailPallets.add(importPoDetailPallet);


            } else if (stt != null) {
                ImportPoDetailPallet importPoDetailPallet = importPoDetailPallets.get(stt);
                if(number!=null && number + Long.parseLong(quantity) - importPoDetailPallet.getQuantity() <= importPoDetail.getQuantity()){
                    sum.set(number + Long.parseLong(quantity) - importPoDetailPallet.getQuantity());
                    importPoDetailPallet.setQuantity(Long.parseLong(quantity));
                    importPoDetailPallet.setPallet(pallet.get());
                }
                else{
                    showMessage(R.string.product_info_error, "product_info_error");
                    return;
                }
                index.set(-1);
            }
            quantityStr.set("");
            this.pallet.set(null);
            baseModels.notifyChange();
        } else {
            showMessage(R.string.product_info_error, "product_info_error");
        }
    }

    public boolean enoughQuantity() {
        Long s = sum.get();
        return s != null && s.equals(importPoDetail.getQuantity());
    }
}
