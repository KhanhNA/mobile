package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PalletDetail extends Base {

	private Pallet pallet;

	private Product product;

	private ProductPacking productPacking;

	private ProductPackingPrice productPackingPrice;

	private Long quantity;

	private Long orgQuantity;

	@JsonFormat(pattern = AppUtils.dateFormat)
	private Date expireDate;

	private ImportPoDetailPallet importPoDetailPallet;

	private Long inventory;

}