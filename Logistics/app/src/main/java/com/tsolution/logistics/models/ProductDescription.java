package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDescription extends Base {
    private Product product;
    private Language language;
    private String code;
    private String name;
    private Long totalQuantity;
    private Integer packingQuantity;
    private Long packingTypeId;
    @Override
    public boolean equals(Object obj) {
        if(obj==null){return false;}
        if(obj instanceof StatementDetail){
            StatementDetail statementDetail = (StatementDetail)obj;
            return statementDetail.getProductId().equals(this.product.getId());
        }
        return super.equals(obj);
    }

    public Long getTotalQuantity() {
        return totalQuantity == null ? 0 : totalQuantity;
    }

    public Integer getPackingQuantity() {
        return packingQuantity == null ? 0 : packingQuantity;
    }
}
