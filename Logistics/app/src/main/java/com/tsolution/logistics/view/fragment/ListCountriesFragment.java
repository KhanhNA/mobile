package com.tsolution.logistics.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterActionsListener;
import com.tsolution.logistics.R;
import com.tsolution.logistics.viewmodels.ListCountriesActivityViewModel;

public class ListCountriesFragment extends BaseFragment implements AdapterActionsListener {

    ListCountriesActivityViewModel countriesActivityViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        countriesActivityViewModel = (ListCountriesActivityViewModel) viewModel;
        countriesActivityViewModel.init();
        BaseAdapter countriesAdapter = new BaseAdapter(R.layout.countries_info, viewModel,this::adapterAction, getBaseActivity());
        recyclerView.setAdapter(countriesAdapter);
        return binding.getRoot();
    }

    @Override
    public void adapterAction(View view, BaseModel baseModel) {
        EditCountryFragment dialog = new EditCountryFragment();
        Bundle args = new Bundle();
        args.putSerializable("countryId",baseModel);
        dialog.setArguments(args);
        if(getActivity()!=null){
            dialog.show(getActivity().getSupportFragmentManager(), "MyDialogFragment");
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_list_countries;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ListCountriesActivityViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.lst_countries;
    }
}
