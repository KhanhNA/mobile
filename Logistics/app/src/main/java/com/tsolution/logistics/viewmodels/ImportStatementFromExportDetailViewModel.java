package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.util.Log;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.utils.AppUtils;


import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class ImportStatementFromExportDetailViewModel extends BaseViewModel {
    private ObservableField<ExportStatement> exportStatement = new ObservableField<>();

    public ImportStatementFromExportDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(ExportStatement statement) {
        exportStatement.set(statement);
        findExportStatementById(statement.getId());
    }
    private void findExportStatementById(Long idStatement) {
        if(idStatement!=null) {
            AppUtils.callService().findExportStatementById(idStatement).enqueue(callBack(this::getExportStatementSuccess, ExportStatement.class, null));
        }
    }

    private void getExportStatementSuccess(Call call, Response response, Object o, Throwable throwable) {
        ExportStatement statement = (ExportStatement) o;
        exportStatement.set(statement);
        setData(statement.getExportStatementDetails());
    }
    public void onCreate() {
        // TODO: 24/08/2019 REQUEST -> PROCESSING
        Log.e("NMQ", "NMQ");
        ExportStatement statement = exportStatement.get();
        if (statement != null) {
            AppUtils.callService().createFromExportStatement(statement.getId()).enqueue(callBack(this::statementSuccess, ImportStatement.class, null));
        }
    }


    private void statementSuccess(Call call, Response response, Object o, Throwable throwable) {
        Log.e("NMQ", "NMQ");
        appException.setValue(new AppException(R.string.success, "success"));
    }
}
