package com.tsolution.logistics.repository.callback;

public interface DatabaseCallback {
    void onSuccess(int code);
    void onError(int code, Throwable e);
}
