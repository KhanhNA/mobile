package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportStatementDetail;
import com.tsolution.logistics.models.RepackingPlanning;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.view.dialog.PalletDialog;
import com.tsolution.logistics.viewmodels.ImportFCFromRepackingPlanningDetailViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

public class ImportFCFromRepackingPlanningDetailFragment extends BaseFragment {
    private ImportFCFromRepackingPlanningDetailViewModel repakingViewModel;
    private PalletDialog palletDialog;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        repakingViewModel = (ImportFCFromRepackingPlanningDetailViewModel) viewModel;
        palletDialog = new PalletDialog();
        palletDialog.setPalletMutableLiveData(repakingViewModel.getPallet());

        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_import_fc_from_repacking_planning_detail_item, viewModel, (view, baseModel) -> {
            ImportStatementDetail importStatementDetail = (ImportStatementDetail) baseModel;
            repakingViewModel.setImportStatementDetail(importStatementDetail);
            if(getFragmentManager()!=null){
                palletDialog.show(getFragmentManager(), "palletDialog");
            }
        }, getBaseActivity());

        repakingViewModel.getPallet().observe(this, pallet -> {
            if(pallet!=null){
                List<ImportStatementDetail> importStatementDetails = repakingViewModel.getImportStatementDetails().get();
                if(importStatementDetails!=null){
                    importStatementDetails.get(repakingViewModel.getPosition()).setPallet(pallet);
                }
                baseAdapter.notifyDataSetChanged();

            }
        });
        recyclerView.setAdapter(baseAdapter);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Intent intent = activity.getIntent();
            if (intent != null && intent.hasExtra("repackingPlanning")) {
                RepackingPlanning repackingPlanning = (RepackingPlanning) intent.getSerializableExtra("repackingPlanning");
                repakingViewModel.init(repackingPlanning);
            }
        }

        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
//        super.action(view, baseViewModel);
        String tag = (String) view.getTag();
        if ("scanCode".equals(tag)) {
            Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
            startActivityForResult(qrScan, 0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                repakingViewModel.checkRepackingRepacked(result);
            }
        }
        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_import_fc_from_repacking_planning_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportFCFromRepackingPlanningDetailViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.repacking_planning_details;
    }
}
