package com.tsolution.logistics.models.mapper;

import com.tsolution.logistics.models.Pallet;
import com.tsolution.logistics.models.entity.PalletEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PalletMapper {
    PalletMapper INSTANCE = Mappers.getMapper(PalletMapper.class);

    @Mapping(source = "store.id", target = "storeId")
    @Mapping(source = "parentPallet.id", target = "parentPalletId")
    @Mapping(target = "index", ignore = true)
    @Mapping(target = "checked", ignore = true)
    PalletEntity palletToPalletEntity(Pallet pallet);
}
