package com.tsolution.logistics.models.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.tsolution.base.BaseModel;

import androidx.room.Transaction;
import lombok.Getter;
import lombok.Setter;

@Entity(tableName = "product_description", foreignKeys = @ForeignKey(entity = ProductEntity.class,
        parentColumns = "id",
        childColumns = "product_entity_id"))
@Getter
@Setter
public class ProductDescriptionEntity extends BaseModel {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;

    @ColumnInfo(name="product_entity_id")
    private Long productEntityId;

    @ColumnInfo(name="code")
    private String code;

    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="lang_id")
    private Long langId;

    @ColumnInfo(name="packing_quantity")
    private Long packingQuantity;

    @ColumnInfo(name="quantity")
    private Long quantity;

    @ColumnInfo(name="packing_id")
    private Long packingId;

    public String getPackingQuantityStr(){
        return packingQuantity == null ? "" : packingQuantity.toString();
    }

    public String getQuantityStr(){
        return quantity == null ? "" : quantity.toString();
    }
}
