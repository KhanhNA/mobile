package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductPackingPrice extends Base{
    private Currency currency;
    private ProductPacking productPacking;
    private BigDecimal price;
    private String saleType;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date fromDate;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date toDate;
    private Boolean status;


}
