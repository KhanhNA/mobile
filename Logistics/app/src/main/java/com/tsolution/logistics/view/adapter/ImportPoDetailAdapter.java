package com.tsolution.logistics.view.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tsolution.base.CommonActivity;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportPoDetail;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.view.fragment.ImportPoSplitFragment;
import com.tsolution.logistics.viewmodels.ImportPoViewModel;

import java.util.ArrayList;
import java.util.List;

public class ImportPoDetailAdapter extends RecyclerView.Adapter<ImportPoDetailAdapter.ViewHolder>  {

    private int REQUEST_CODE_SPLIT = 1111;
    private int REQUEST_CODE_SCAN_CAMERA = 2222;

    private Activity activity;
    private List<ImportPoDetail> data;
    private ImportPoViewModel importPoViewModel;

    public ImportPoDetailAdapter(Activity activity, ImportPoViewModel importPoViewModel) {
        this.activity = activity;
        data = new ArrayList<>();
        this.importPoViewModel = importPoViewModel;
    }

    public void updateData(List<ImportPoDetail> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.fragment_import_po_detail_item, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ImportPoDetail importPoDetail = data.get(position);
        holder.txtImPortPoCode.setText(importPoDetail.getImportPo().getCode());
        holder.txtExpireDate.setText(importPoDetail.getExpireDateStr());
        holder.txtPacking.setText(importPoDetail.getProductPacking().getPackingType().getQuantity());
        holder.txtQuantity.setText(importPoDetail.getQuantity().toString());
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtImPortPoCode, txtPacking, txtExpireDate, txtQuantity;
        private RecyclerView rcSplitQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            txtImPortPoCode = itemView.findViewById(R.id.txtImPortPoCode);
            txtPacking = itemView.findViewById(R.id.txtPacking);
            txtExpireDate = itemView.findViewById(R.id.txtExpireDate);
            txtExpireDate = itemView.findViewById(R.id.txtExpireDate);
            txtQuantity = itemView.findViewById(R.id.txtQuantity);
            rcSplitQuantity = itemView.findViewById(R.id.rcSplitQuantity);
        }


        @Override
        public void onClick(View v) {
            ImportPoDetail importPoDetail = data.get(getAdapterPosition());
            importPoViewModel.setIndex(importPoDetail);
            switch (v.getId()) {
                case R.id.split:
                    Intent intent = new Intent(activity, CommonActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("importPoDetail", importPoDetail);
                    bundle.putSerializable("importPo", importPoViewModel.getImportPo());
                    intent.putExtras(bundle);
                    intent.putExtra("FRAGMENT", ImportPoSplitFragment.class);
                    activity.startActivityForResult(intent, REQUEST_CODE_SPLIT);
                    break;
                case R.id.scan_camera:
                    Intent qrScan = new Intent(activity, BarCodeReaderActivity.class);
                    activity.startActivityForResult(qrScan, REQUEST_CODE_SCAN_CAMERA);
                    break;
                case R.id.item_pallet:
                    importPoViewModel.getArrSplitPallets().setData(importPoViewModel.getSparseArray().get(importPoDetail.index -1));
                    break;

            }
        }
    }


}
