package com.tsolution.logistics.models.mapper;

import com.tsolution.logistics.models.ImportStatementDetail;
import com.tsolution.logistics.models.RepackingPlanningDetailRepacked;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ImportStatementDetailMapper {
    ImportStatementDetailMapper INSTANCE = Mappers.getMapper( ImportStatementDetailMapper.class );

    @Mapping(source = "productPacking.packingType.id", target = "productPackingId")
    @Mapping(source = "productPacking.packingType.code", target = "productPackingCode")
    @Mapping(target = "checked", ignore = true)
    ImportStatementDetail repackingPlanningToImportStatementDetail(RepackingPlanningDetailRepacked product);

}
