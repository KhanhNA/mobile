package com.tsolution.logistics.view.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseDialogFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportStatementDetail;
import com.tsolution.logistics.models.Pallet;
import com.tsolution.logistics.viewmodels.PalletDialogViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PalletDialog extends BaseDialogFragment {
    private ImportStatementDetail importStatementDetail;
    private ObservableField<List<ImportStatementDetail>> importStatementDetails;
    private MutableLiveData<Pallet> palletMutableLiveData;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        PalletDialogViewModel palletVM = (PalletDialogViewModel) viewModel;
        palletVM.init();
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.dialog_search_pallet_item, viewModel, (view, baseModel) -> {
//            importStatementDetail.setPallet((Pallet) baseModel);
//            importStatementDetails.notifyChange();
            palletMutableLiveData.setValue((Pallet) baseModel);
            dismiss();
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_pallet;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PalletDialogViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.pallets;
    }
}
