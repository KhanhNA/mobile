package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportStatement;
import com.tsolution.logistics.models.ImportStatementDetail;
import com.tsolution.logistics.models.PackingType;
import com.tsolution.logistics.models.Pallet;
import com.tsolution.logistics.models.ProductPacking;
import com.tsolution.logistics.models.RepackingPlanning;
import com.tsolution.logistics.models.RepackingPlanningDetail;
import com.tsolution.logistics.models.RepackingPlanningDetailRepacked;
import com.tsolution.logistics.models.Store;
import com.tsolution.logistics.utils.AppUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class ImportFCFromRepackingPlanningDetailViewModel extends BaseViewModel {
    private RepackingPlanning repackingPlanning;
    private HashMap<String, Integer> hashMapCode = new HashMap<>();
    private ObservableField<List<ImportStatementDetail>> importStatementDetails = new ObservableField<>();
    private MutableLiveData<Pallet> pallet = new MutableLiveData<>();
    private Store storeFC;
    private int position;

    public ImportFCFromRepackingPlanningDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(RepackingPlanning repackingPlanning) {
        AppUtils.callService().findRepackingPlanningById(repackingPlanning.getId()).enqueue(callBack(this::success, RepackingPlanning.class, null));
        AppUtils.callService().getStoreFC().enqueue(callBack(this::getFcSuccees, Store.class, null));
    }

    private void getFcSuccees(Call call, Response response, Object body, Throwable throwable) {
        storeFC = (Store) body;
    }

    private void success(Call call, Response response, Object o, Throwable throwable) {
        RepackingPlanning repackingPlanning = (RepackingPlanning) o;
        this.repackingPlanning = repackingPlanning;
        int stt = 0;
        List<ImportStatementDetail> importStatementDetailList = new ArrayList<>();
        for (RepackingPlanningDetail repackingPlanningDetail : repackingPlanning.getRepackingPlanningDetails()) {
            for (RepackingPlanningDetailRepacked repackingPlanningDetailRepacked : repackingPlanningDetail.getRepackingPlanningDetailRepackeds()) {
                String code = AppUtils.changeFormCode(repackingPlanningDetailRepacked.getCode());
                hashMapCode.put(code, stt);
//                ImportStatementDetail importStatementDetail
//                        = ImportStatementDetailMapper.INSTANCE.repackingPlanningToImportStatementDetail(repackingPlanningDetailRepacked);

                ImportStatementDetail importStatementDetail = new ImportStatementDetail();
                // packing type
                PackingType packingType = repackingPlanningDetailRepacked.getProductPacking().getPackingType();
                importStatementDetail.setPackingTypeId(packingType.getId());
                importStatementDetail.setPackingTypeCode(packingType.getCode());
                importStatementDetail.setPackingTypeQuantity(packingType.getQuantity());

                //product packing
                ProductPacking productPacking =  repackingPlanningDetailRepacked.getProductPacking();
                importStatementDetail.setProductPackingId(productPacking.getId());
                importStatementDetail.setProductPackingCode(productPacking.getCode());


                importStatementDetail.setQuantity(repackingPlanningDetailRepacked.getQuantity());
                importStatementDetail.setExpireDate(repackingPlanningDetail.getExpireDate());

                importStatementDetailList.add(importStatementDetail);
                stt++;
            }
        }
        importStatementDetails.set(importStatementDetailList);
        setData(importStatementDetailList);
    }

    public void onCreate() {
        List<ImportStatementDetail> list = (List) baseModels.get();
        if (list != null) {
            for (ImportStatementDetail statementDetail : list) {
//                if (statementDetail.checked == null || !statementDetail.checked) {
//
//                    return;
//                } else {
                    statementDetail.setPalletId(statementDetail.getPallet().getId());
//                }
            }
        }
//        List<ImportStatementDetail> importStatementDetails = new ArrayList<>();
//        for(RepackingPlanningDetail repackingPlanningDetail : repackingPlanning.getRepackingPlanningDetails()){
//            ImportStatementDetail importStatementDetail = new ImportStatementDetail();
//            importStatementDetail.setExpireDate(repackingPlanningDetail.getExpireDate());
//            importStatementDetail.setTotalQuantity(repackingPlanningDetail.getQuantity());
//            importStatementDetail.setPackingTypeCode(repackingPlanningDetail.getProductPacking().getPackingType().getCode());
//            importStatementDetail.setPackingTypeId(repackingPlanningDetail.getProductPacking().getPackingType().getId());
//            importStatementDetail.setProductCode(repackingPlanningDetail.getProductPacking().getProduct().getCode());
//            importStatementDetail.setProductId(repackingPlanningDetail.getProductPacking().getProduct().getId());
//            importStatementDetails.add(importStatementDetail);
//        }
//        ImportStatement importStatement = new ImportStatement();
//        importStatement.setImportStatementDetails(importStatementDetails);
//        importStatement.setDescription(repackingPlanning.getDescription());
        ImportStatement importStatement = new ImportStatement();
        importStatement.setDescription(repackingPlanning.getDescription());
        importStatement.setToStoreId(storeFC.getId());
        importStatement.setToStoreCode(storeFC.getCode());
        importStatement.setRepackingPlanningId(repackingPlanning.getId());
        importStatement.setImportStatementDetails(importStatementDetails.get());

        AppUtils.callService().createImportStatementFromRepackingPlanning(importStatement).enqueue(customCallBack((call1, response, body, throwable) -> createSuccess(response)));
    }

    public void setImportStatementDetail(ImportStatementDetail importStatementDetail) {
        List list = getBaseModelsE();
        if (list != null) {
            this.position = list.indexOf(importStatementDetail);
        }

    }

    private void createSuccess(Response response) {
        if(response.code()==AppUtils.CODE_SUCCESS){
            showMessage(R.string.success, "success");
        }
        else{
            showMessage(R.string.error_process, "error_process");
        }
    }

    public void checkRepackingRepacked(String result) {
        result = AppUtils.changeFormCode(result);
        Integer stt = hashMapCode.get(result);
        if (stt != null) {
            List lst = (List) baseModels.get();
            ImportStatementDetail sd = (ImportStatementDetail) lst.get(stt);
            sd.checked = true;
            lst.set(lst.indexOf(lst.get(stt)), sd);
            baseModels.notifyChange();
        }
    }
}
