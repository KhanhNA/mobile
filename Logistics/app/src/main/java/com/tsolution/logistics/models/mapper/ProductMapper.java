package com.tsolution.logistics.models.mapper;

import com.tsolution.logistics.models.Product;
import com.tsolution.logistics.models.entity.ProductEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );
    ProductEntity productDtoToProduct(Product product);
}
