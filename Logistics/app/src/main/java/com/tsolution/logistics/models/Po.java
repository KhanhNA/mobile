package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Po extends Base {

    private Manufacturer manufacturer;

    private String code;

    private String qRCode;

    private Store store;

    private String description = "NULL";

    private Integer status;

    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date printedDate;

    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date importDate;

    private List<PoDetail> poDetails;

    private ImportPo importPo;
}