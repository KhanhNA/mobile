package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.util.Log;

import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.Pallet;
import com.tsolution.logistics.models.Principal;
import com.tsolution.logistics.models.Store;
import com.tsolution.logistics.models.entity.PalletEntity;
import com.tsolution.logistics.models.entity.StoreEntity;
import com.tsolution.logistics.models.mapper.PalletMapper;
import com.tsolution.logistics.models.mapper.StoreMapper;
import com.tsolution.logistics.repository.PalletRepository;
import com.tsolution.logistics.repository.StoreRepository;
import com.tsolution.logistics.utils.AppUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserViewModel extends BaseViewModel {
    private int CODE_RESPONSE_FROM_STORE = 1000;
    private String fullName;
    private LiveData<List<PalletEntity>> pallets;
    private LiveData<List<StoreEntity>> stores;

    private PalletRepository palletRepository;
    private StoreRepository storeRepository;

    public UserViewModel(@NonNull Application application) {
        super(application);
        Principal principal = AppUtils.user.getUserAuthentication().getPrincipal();
        fullName = principal.getFirstName() + " " + principal.getLastName();
    }

    private void initConnect() {
        palletRepository = new PalletRepository(getApplication());
        storeRepository = new StoreRepository(getApplication());
    }


    public void createDataBaseLocal() {
        initConnect();

        storeRepository.getAllStoreFromLocal();
        stores = storeRepository.getAllStore();
//        palletRepository.getAllPalletFromLocal();
    }

    private void getAllStoreSuccess(Object body) {
        List<Store> stores = (List<Store>) body;
        AppUtils.callService().findAllPallet().enqueue(callBack((call, response, o, throwable) -> insertDataToLocal(stores, o), Pallet.class, List.class));
    }

    private void insertDataToLocal(List<Store> stores, Object body) {
        List<Pallet> pallets = (List) body;
        for (Store store : stores) {
            StoreEntity storeEntity = StoreMapper.INSTANCE.storeToStoreEntity(store);
            storeRepository.insert(storeEntity);
        }
        for(Pallet pallet: pallets){
            PalletEntity palletEntity = PalletMapper.INSTANCE.palletToPalletEntity(pallet);
            palletRepository.insert(palletEntity);
        }
        showMessage(R.string.async_task_success, "async_task_success");
    }

    public void synchronizeData() {
        Log.e("NMQ", "synchronizeData");
        //clearDataBase();

        // Store
        AppUtils.callService().getAllStore().enqueue(callBack((call1, response1, body, throwable1) -> getAllStoreSuccess(body), Store.class, List.class));
    }

    public void clearDataBase() {
        palletRepository.deleteAll();
        storeRepository.deleteAll();
    }
}
