package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Store extends Base{
    private Country country;
    private Language language;
    private Currency currency;
    private String code;
    private String name;
    private String domainName;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date inBusinessSince;
    private String email;
    private String logo;
    private String address;
    private String city;
    private String phone;
    private String postalCode;
    private Boolean dc;
    private Boolean status;
    private BigDecimal lat;
    private BigDecimal lng;
    private List<StoreProductPacking> storeProductPackings;
    @Override
    public String toString() {
        return "Store{" +
                "code='" + code + '\'' +
                '}';
    }
}
