package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RepackingPlanning extends Base {
    public enum RepackingPlanningStatus {
        NEW(0), REPACKED(1), IMPORTED(2), CANCEL(-1);
        private Integer value;

        RepackingPlanningStatus(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
    private String code;

    private String description;

    private Integer status;

    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date repackedDate;

    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date planningDate;

    private List<RepackingPlanningDetail> repackingPlanningDetails;

    public String getRepackedDateStr() {
        return repackedDate == null ? "" : DateUtils.convertDateToString(repackedDate, "dd/MM/yyyy");
    }

    public String getPlanningDateStr() {
        return planningDate == null ? "" : DateUtils.convertDateToString(planningDate, "dd/MM/yyyy");
    }
}
