package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product extends Base {
    private ProductType productType;
    private String code;
    private String name;
    private Boolean available;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date availableDate;
    private Integer quantityOrdered;
    private Integer sortOrder;
    private Boolean hot;
    private Long totalQuantity;
    private Boolean popular;
    private Integer packingQuantity;
    private Long packingId;
    private String packingCode;
    private Long quantity;
    private List<ProductDescription> productDescriptions;

    private List<ProductPacking> productPackings;

    public String getName() {
        String result = name;
        for (ProductDescription productDescription :
                productDescriptions) {
            if (AppUtils.language.equals(productDescription.getLanguage().getId())) {
                result = productDescription.getName();
            }
        }
        return result;
    }
}
