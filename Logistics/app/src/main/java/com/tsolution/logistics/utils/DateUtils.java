package com.tsolution.logistics.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class DateUtils {
    public static String convertDateToString(Date date, String type){
        String result = "";
        SimpleDateFormat formatter = new SimpleDateFormat(type, Locale.US);
        result = formatter.format(date);
        return result;
    }
    public static Date convertStringToDate(String date, String type){
        DateFormat format = new SimpleDateFormat(type, Locale.US);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date convertDMYToDate(int day, int month, int year){
        Calendar calendar = new GregorianCalendar();
        calendar.set(year, month, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

}
