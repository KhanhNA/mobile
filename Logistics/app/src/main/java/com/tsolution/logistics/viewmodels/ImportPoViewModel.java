package com.tsolution.logistics.viewmodels;

import android.app.Application;
import android.util.SparseArray;

import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportPo;
import com.tsolution.logistics.models.ImportPoDetail;
import com.tsolution.logistics.models.ImportPoDetailPallet;
import com.tsolution.logistics.models.Pallet;
import com.tsolution.logistics.models.entity.PalletEntity;
import com.tsolution.logistics.repository.PalletRepository;
import com.tsolution.logistics.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportPoViewModel extends BaseViewModel {
    private ImportPo importPo;
    private int position;

    private BaseViewModel arrSplitPallets = new BaseViewModel(getApplication());

    private SparseArray<List<ImportPoDetailPallet>> sparseArray = new SparseArray<>();

    public ImportPoViewModel(@NonNull Application application) {
        super(application);
    }

    private LiveData<List<PalletEntity>> pallets;
    private PalletRepository palletRepository;

    public void init() {
        palletRepository = new PalletRepository(getApplication());
        palletRepository.getAllPalletFromLocal();
        pallets = palletRepository.getAllPallet();
        if (importPo != null) {
            AppUtils.callService().findById(importPo.getId()).enqueue(callBack((call1, response, body, throwable) -> findSuccess(body), ImportPo.class, null));
        }
    }

    public void setIndex(ImportPoDetail importPoDetail) {
        List<ImportPoDetail> poDetails = this.importPo.getImportPoDetails();
        position = poDetails.lastIndexOf(importPoDetail);
    }


    public void onSplitResult(List<ImportPoDetailPallet> importPoDetailPallets){
        if(importPoDetailPallets != null) {
            sparseArray.put(position, importPoDetailPallets);
        }
    }

    private Long findPalletIdByCode(String qrCode) {
        List<PalletEntity> palletEntities = pallets.getValue();
        if(palletEntities!=null){
            for (PalletEntity pallet : palletEntities) {
                if (pallet.getCode().equals(qrCode)) {
                    return pallet.getId();
                }
            }
        }
        else{
            showMessage(R.string.async_task, "async_task");
            return null;
        }

        return 0L;
    }

    public void onSplitResult(String qrCode) {

        List<ImportPoDetailPallet> importPoDetailPallets = new ArrayList<>();
        ImportPoDetailPallet importPoDetailPallet = new ImportPoDetailPallet();

        ImportPo importPo = new ImportPo();
        importPo.setId(this.importPo.getId());
        importPoDetailPallet.setImportPo(importPo);

        Long idPallet =  findPalletIdByCode(qrCode);

        //pallet
        if (idPallet != null && idPallet!=0L) {
            Pallet pallet = new Pallet();
            pallet.setId(idPallet);
            importPoDetailPallet.setPallet(pallet);
        } else {
            showMessage(R.string.no_pallet_found, "no_pallet_found");
        }

        // importPoDetail
        // khong set thang dc vi sau khong goi dc API. Loi treo may ?
        ImportPoDetail importPoDetail = new ImportPoDetail();
        importPoDetail.setId(this.importPo.getImportPoDetails().get(position).getId());
        importPoDetailPallet.setImportPoDetail(importPoDetail);

        //quantity
        Long q = this.importPo.getImportPoDetails().get(position).getQuantity();
        importPoDetailPallet.setQuantity(q);


        importPoDetailPallets.add(importPoDetailPallet);
        sparseArray.put(position, importPoDetailPallets);
    }

    private void findSuccess(Object body) {
        this.importPo = (ImportPo) body;
        setData(this.importPo.getImportPoDetails());
    }

    public void onCreate() {
        List<ImportPoDetail> poDetails = this.importPo.getImportPoDetails();
        List<ImportPoDetailPallet> allDetailPallets = new ArrayList<>();
        for (int i = 0; i < poDetails.size(); i++) {
            List<ImportPoDetailPallet> importPoDetailPallets = sparseArray.get(i, null);
            if (importPoDetailPallets == null) {
                showMessage(R.string.error_process, "product_info_error");
                return;
            }

            allDetailPallets.addAll(importPoDetailPallets);
        }

        AppUtils.callService().updatePallet(importPo.getId(), allDetailPallets).enqueue(callBack((call1, response, body, throwable) -> createSuccess(body), ImportPoDetailPallet.class, List.class));
    }

    private void createSuccess(Object body) {
        if (body != null) {
            showMessage(R.string.success, "success");
        } else {
            showMessage(R.string.error_process, "error_process");
        }
    }
}
