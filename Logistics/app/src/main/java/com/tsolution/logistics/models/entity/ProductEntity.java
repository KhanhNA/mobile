package com.tsolution.logistics.models.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;

@Entity(tableName = "product")
@Getter
@Setter
public class ProductEntity {
    @PrimaryKey
    @ColumnInfo(name="id")
    private Long id;

    @ColumnInfo(name="code")
    private String code;

    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="quantity")
    private Long quantity;

    @ColumnInfo(name="packing_quantity")
    private Long packingQuantity;

    @ColumnInfo(name="packing_id")
    private Long packingId;

    @ColumnInfo(name="packing_code")
    private Long packingCode;

}
