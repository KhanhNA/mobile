package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Pageable;
import com.tsolution.logistics.models.RepackingPlanning;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.utils.DateUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class ImportFCFromRepackingPlanningViewModel extends BaseViewModel {
    private ObservableField<String> planningCode = new ObservableField<>("");
    private ObservableField<Date> fromDate = new ObservableField<>(Calendar.getInstance().getTime());
    private ObservableField<Date> toDate = new ObservableField<>(Calendar.getInstance().getTime());
    private ObservableList<RepackingPlanning.RepackingPlanningStatus> dateTypes = new ObservableArrayList<>();
    private Integer index;

    public void init() {
        dateTypes.addAll(Arrays.asList(RepackingPlanning.RepackingPlanningStatus.values()));
    }

    public ImportFCFromRepackingPlanningViewModel(@NonNull Application application) {
        super(application);
    }

    public void search() {
        Date fDate = fromDate.get();
        Date tDate = toDate.get();
        String page = (String) currentPage.get();
        if (fDate != null && tDate != null && page != null) {
            String from = DateUtils.convertDateToString(fDate, AppUtils.dateFormat);
            String to = DateUtils.convertDateToString(tDate, AppUtils.dateFormat);
            AppUtils.callService().findRepackingPlanning(planningCode.get(),
                    dateTypes.get(index).getValue(), from, to, Integer.parseInt(page), AppUtils.pageSize, 1)
                    .enqueue(callBack(this::findSuccess, Pageable.class, RepackingPlanning.class, null));
        }
    }

    private void findSuccess(Call call, Response response, Object body, Throwable throwable) {
        Pageable<RepackingPlanning> pageable = (Pageable) body;
        setData(pageable.getContent());
    }

    public void setDate(String tag, int year, int month, int day) {
        Date date = DateUtils.convertDMYToDate(day, month, year);
        switch (tag) {
            case "fromDate":
                fromDate.set(date);
                break;
            case "toDate":
                toDate.set(date);
                break;
        }
    }
}
