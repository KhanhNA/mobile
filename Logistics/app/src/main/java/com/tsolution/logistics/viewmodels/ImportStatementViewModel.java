package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Pageable;
import com.tsolution.logistics.models.ExportStatement;
import com.tsolution.logistics.utils.AppUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import retrofit2.Call;
import retrofit2.Response;

public class ImportStatementViewModel extends BaseViewModel {
    private ObservableField<String> exportStatementCode = new ObservableField<>("");

    public ImportStatementViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        search();
    }

    private void searchSuccess(Call call, Response response, Object o, Throwable throwable) {
        Pageable<ExportStatement> pageable = (Pageable<ExportStatement>) o;
        setPage(pageable);
        List<ExportStatement> exportStatements = pageable.getContent();
        setData(exportStatements);
    }

    public void search() {
        Integer page = Integer.parseInt((String) currentPage.get());
        AppUtils.callService()
                .findImportStatementByCode(exportStatementCode.get(), ExportStatement.ExportStatementStatus.RELEASED.getValue(), page, AppUtils.pageSize)
                .enqueue(callBack(this::searchSuccess, Pageable.class, ExportStatement.class, null));

    }

    public void searchByCode(String code) {
        exportStatementCode.set(code);
        search();
    }
}
