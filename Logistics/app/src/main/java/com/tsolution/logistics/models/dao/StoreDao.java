package com.tsolution.logistics.models.dao;

import com.tsolution.logistics.models.entity.StoreEntity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface StoreDao {
    @Query("SELECT s.* FROM store s LIMIT :limit OFFSET :offset")
    LiveData<List<StoreEntity>> getStoreByPage(int limit, int offset);

    @Query("SELECT s.* FROM store s")
    LiveData<List<StoreEntity>> getAllStore();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(StoreEntity storeEntity);

    @Query("DELETE FROM store")
    public void deleteAll();

}
