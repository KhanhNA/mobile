package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.utils.DateUtils;
import com.tsolution.logistics.utils.ValidateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatementDetail extends Base {
    private Statement statement;
    private ProductPacking productPacking;
    private ProductPackingPrice productPackingPrice;
    private Long quantity;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date importDate;
    private Long productId;
    private String productCode;
    private Long packingTypeId;
    private PackingType packingType;
    private String packingTypeCode;
    private Integer packingTypeQuantity;
    private BigDecimal price;
    private String productName;
    private StoreProductPackingDetail storeProductPackingDetail;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date expireDate;
    private String expireDateStr;
    private Long totalQuantity;



    public StatementDetail(Long productId, String productCode, String productName) {
        this.productId = productId;
        this.productCode = productCode;
        this.productName = productName;
    }

    public String getQuantityStr() {
        return String.valueOf(quantity == null ? "" : quantity);
    }

    public void setQuantityStr(String str) {
        if (ValidateUtils.isNullOrEmpty(str)) {
            quantity = 0L;
        } else {
            quantity = Long.parseLong(str);
        }
    }

    public void setExpireDate(Date expireDate) {
        if (expireDate == null) {
            expireDateStr = "";
        } else {
            expireDateStr = DateUtils.convertDateToString(expireDate, "dd/MM/yyyy");
        }
        this.expireDate = expireDate;
    }

    public Long getTotalQuantity() {
        return totalQuantity == null ? 0 : totalQuantity;
    }

    public Integer getPackingTypeQuantity() {
        return packingTypeQuantity == null ? 0 : packingTypeQuantity;
    }
}
