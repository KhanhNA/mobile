package com.tsolution.logistics.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.utils.DateTimePickerUtils.DatePickerFragment;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.StatementDetail;
import com.tsolution.logistics.viewmodels.CreateStatementViewModel;

import java.util.Calendar;
import java.util.Date;


public class CreateStatementFragment extends BaseFragment implements DatePickerFragment.GetDate {
    private CreateStatementViewModel statementViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
//        init(inflater, container, savedInstanceState, R.layout.create_statement_v2, CreateStatementViewModel.class, R.id.deltail);
        statementViewModel = (CreateStatementViewModel) viewModel;
        if (this.getArguments() != null) {
            String s = this.getArguments().getString("fragment");
            ((CreateStatementViewModel) viewModel).setType(s);
        }
        BaseAdapter createStatementAdapter = new BaseAdapter(R.layout.create_statement_detail_item, viewModel, this::onAdapterClicked, getBaseActivity());

        recyclerView.setAdapter(createStatementAdapter);

        viewModel.init();
        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {


    }

    @Override
    public void adapterAction(View view, BaseModel baseModel) {
        if (baseModel instanceof StatementDetail) {
            showDatePicker(baseModel);
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
        }
        closeProcess();
    }

    //
    private void showDatePicker(BaseModel baseModel) {
        DatePickerFragment dialogFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("baseModel", baseModel);
        dialogFragment.setArguments(bundle);
        dialogFragment.init(this::getDateTime);
        dialogFragment.show(getFragmentManager(), "DatePicker");
        dialogFragment.setTargetFragment(CreateStatementFragment.this, 1);
    }

    //
    @Override
    public void getDateTime(Date date, BaseModel baseModel) {
        if (baseModel instanceof StatementDetail) {
            Calendar cal = Calendar.getInstance(); // locale-specific
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            statementViewModel.setDate(cal.getTime());
        }
    }
    @Override
    public int getLayoutRes() {
        return R.layout.create_statement_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateStatementViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.deltail;
    }
}
