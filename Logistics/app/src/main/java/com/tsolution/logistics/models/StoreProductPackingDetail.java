package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StoreProductPackingDetail extends Base{
    private StoreProductPacking storeProductPacking;
    private ProductPackingPrice productPackingPrice;
    private Long quantity;
    private Long originalQuantity;
    private String code;
    private String qRCode;
}
