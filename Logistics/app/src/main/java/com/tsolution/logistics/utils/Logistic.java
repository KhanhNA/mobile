package com.tsolution.logistics.utils;

public class Logistic extends android.app.Application {
    private static Logistic mInstance;
    public static synchronized Logistic getInstance() {
        if(mInstance==null){
            mInstance = new Logistic();
        }
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}
