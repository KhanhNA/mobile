package com.tsolution.logistics.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.models.CountryDescription;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class ListCountriesActivityViewModel extends BaseViewModel {
    private MutableLiveData<List<CountryDescription>> lstCountries;

    public ListCountriesActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        AppUtils.callService().getAllCountriesDescription(true, AppUtils.language).enqueue(callBack(this::getCountriesSuccess, CountryDescription.class, List.class));
    }

    private void getCountriesSuccess(Call call, Response response, Object o, Throwable throwable) {
        List<CountryDescription> descriptions = (List<CountryDescription>) o;
//        baseModels.clear();
//        baseModels.addAll(descriptions);
        setData(descriptions);
    }
}
