package com.tsolution.logistics.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.ImportPo;
import com.tsolution.logistics.viewmodels.UpdatePalletViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class UpdatePalletFragment extends BaseFragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_update_pallet_item, viewModel, (view, baseModel) -> {
            ImportPo importPo = (ImportPo) baseModel;
            Intent intent = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", importPo);
            intent.putExtras(bundle);
            intent.putExtra("FRAGMENT", ImportPoFragment.class);
            startActivity(intent);
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_update_pallet;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return UpdatePalletViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.importPos;
    }
}
