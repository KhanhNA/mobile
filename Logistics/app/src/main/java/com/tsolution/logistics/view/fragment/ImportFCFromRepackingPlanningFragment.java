package com.tsolution.logistics.view.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.RepackingPlanning;
import com.tsolution.logistics.utils.QRCodeUtils.BarCodeReaderActivity;
import com.tsolution.logistics.viewmodels.ImportFCFromRepackingPlanningViewModel;

import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ImportFCFromRepackingPlanningFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    private ImportFCFromRepackingPlanningViewModel mViewModel;
    private String tag = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mViewModel = (ImportFCFromRepackingPlanningViewModel) viewModel;
        mViewModel.init();
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_import_fc_from_repacking_planning_item, viewModel, (view, baseModel) -> {
            RepackingPlanning repackingPlanning = (RepackingPlanning) baseModel;
            Intent intent = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("repackingPlanning", repackingPlanning);
            intent.putExtras(bundle);
            intent.putExtra("FRAGMENT", ImportFCFromRepackingPlanningDetailFragment.class);
            startActivity(intent);
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }



    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        tag = (String) view.getTag();
        switch (tag) {
            case "fromDate":
                Date toDate = mViewModel.getToDate().get();
                if(toDate!=null){
                    showDialogDatePicker(null, toDate.getTime());
                }
                break;
            case "toDate":
                Date fromDate = mViewModel.getFromDate().get();
                if(fromDate!=null){
                    showDialogDatePicker(fromDate.getTime(), System.currentTimeMillis()-1000);
                }
                break;
            case "scanCode":
                Intent qrScan = new Intent(getContext(), BarCodeReaderActivity.class);
                startActivityForResult(qrScan, 0);
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                mViewModel.getPlanningCode().set(result);
            }
        }
        closeProcess();
    }

    private void showDialogDatePicker(Long minDate, Long maxDate) {
        if (getActivity() != null) {
            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            if(maxDate!=null){
                datePickerDialog.getDatePicker().setMaxDate(maxDate);
            }
            if(minDate!=null){
                datePickerDialog.getDatePicker().setMinDate(minDate);
            }
            datePickerDialog.show();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_import_fc_from_repacking_planning;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportFCFromRepackingPlanningViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.repackingPlannings;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mViewModel.setDate(tag, year, month, dayOfMonth);
    }
}
