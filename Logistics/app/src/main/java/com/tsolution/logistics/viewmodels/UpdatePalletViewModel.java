package com.tsolution.logistics.viewmodels;

import android.app.Application;

import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Pageable;
import com.tsolution.logistics.models.ImportPo;
import com.tsolution.logistics.utils.AppUtils;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import retrofit2.Call;
import retrofit2.Response;

public class UpdatePalletViewModel extends BaseViewModel {
    public ObservableField<String> code = new ObservableField<>("");
    public UpdatePalletViewModel(@NonNull Application application) {
        super(application);
    }

    public void search(){
        Integer page = Integer.parseInt((String) currentPage.get());
        AppUtils.callService()
                .findImportPo(page, AppUtils.pageSize, ImportPo.ImportPoStatus.NEW.getValue(), code.get())
                .enqueue(callBack(this::searchSuccess, Pageable.class, ImportPo.class, null));

    }

    private void searchSuccess(Call call, Response response, Object body, Throwable throwable) {
        Pageable pageable = (Pageable) body;
        setPage(pageable);
        setData(pageable.getContent());
    }
}
