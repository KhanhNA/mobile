package com.tsolution.logistics.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.logistics.R;
import com.tsolution.logistics.view.dialog.ProductDialog;
import com.tsolution.logistics.viewmodels.CreateStatementImportFCViewModel;

public class CreateStatementImportFC extends BaseFragment {
    private CreateStatementImportFCViewModel createStatementImportFCViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);
        createStatementImportFCViewModel = (CreateStatementImportFCViewModel) viewModel;
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.create_statement_detail_item, viewModel, (view, baseModel) -> {

        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        ProductDialog productDialog = new ProductDialog();
        productDialog.setExceptId(createStatementImportFCViewModel.getExceptId());
        productDialog.setSearchByService(true);
        productDialog.setResult(createStatementImportFCViewModel.getCurrentProduct());
        if(getFragmentManager()!=null){
            productDialog.show(getFragmentManager(), "productDialog");
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_statement_import_fc;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateStatementImportFCViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.importDetail;
    }
}
