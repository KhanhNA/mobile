package com.tsolution.logistics.models.entity;

import com.tsolution.base.BaseModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Entity(tableName = "store")
@Getter
@Setter
public class StoreEntity extends BaseModel {

    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;

    @ColumnInfo(name = "code")
    private String code;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "domainName")
    private String domainName;

    @ColumnInfo(name = "logo")
    private String logo;

    @ColumnInfo(name = "address_name")
    private String address;

    @ColumnInfo(name = "city")
    private String city;

    @ColumnInfo(name = "phone")
    private String phone;

}
