package com.tsolution.logistics.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class MerchantOrderEntity extends SuperEntity implements Serializable {


	private static final long serialVersionUID = 6858507565094911086L;

	private String code;
	private String qRCode;
	private Long clientOrderId;
	private Store fromStore;
	private MerchantEntity merchant;

	/**
	 * 0: Đơn hàng Store thiếu hàng, 1: Đơn hàng Store đủ hàng, 2: Đơn hàng trả
	 * hàng thành công, 3: Bị hủy, 4: Bị trả
	 */

	private Integer status;
	private List<MerchantOrderDetailEntity> merchantOrderDetails;

	public MerchantOrderEntity(String name, String orderNumber) {
		this.code = orderNumber;
		merchant = new MerchantEntity();
		merchant.setName(name);

	}

	public MerchantOrderEntity() {
	}
}