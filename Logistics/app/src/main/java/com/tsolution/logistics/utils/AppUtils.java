package com.tsolution.logistics.utils;

import com.tsolution.base.RetrofitClient;
import com.tsolution.logistics.R;
import com.tsolution.logistics.models.User;
import com.tsolution.logistics.repository.ApiService;
import com.tsolution.logistics.utils.AppModel.MenuTitle;
import com.tsolution.logistics.view.fragment.CreateExportStatementFragment;
import com.tsolution.logistics.view.fragment.CreateExportStatementReleaseFragment;
import com.tsolution.logistics.view.fragment.CreateImportStatementFromExportFragment;
import com.tsolution.logistics.view.fragment.CreateStatementFragment;
import com.tsolution.logistics.view.fragment.ImportFCFromRepackingPlanningFragment;
import com.tsolution.logistics.view.fragment.ImportStatementFromExportReleaseFragment;
import com.tsolution.logistics.view.fragment.ListCountriesFragment;
import com.tsolution.logistics.view.fragment.ListOrderFragment;
import com.tsolution.logistics.view.fragment.UpdatePalletFragment;

import java.util.HashMap;
import java.util.Map;

public class AppUtils {
    public static Long language = 1L;
    public static final String dateFormat = "yyyy-MM-dd HH:mm:ss.SSS'Z'";
    public static final String dateShowFormat = "dd-MM-yyyy";
    private static final String BASE_URL = "http://192.168.1.31:8888";
    private static final String BASE_URL_OAUTH = "http://192.168.1.155:9999";
    public static final int pageSize = 2;
    public static final int CODE_SUCCESS = 200;

    public static User user;
    static{
        com.tsolution.base.RetrofitClient.BASE_URL = BASE_URL;
        com.tsolution.base.RetrofitClient.BASE_URL_OAUTH = BASE_URL_OAUTH;
        com.tsolution.base.RetrofitClient.clientId = "Logistics";
        com.tsolution.base.RetrofitClient.clientSecret = "XY7kmzoNzl100";
        com.tsolution.base.RetrofitClient.DATE_FORMAT = dateFormat;
    }

    private static Map<String, MenuTitle> menuTitleMap = new HashMap<String, MenuTitle>() {{
        put("CATEGORY", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.category), new ListCountriesFragment(), null));
//        put("CATEGORY_COUNTRIEs", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.list_of_supported_countries), new ListCountriesFragment(), null));
//        put("CATEGORY_CURRENCIEs", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.list_of_supported_currencies), new TwoDScrollingFragment(), null));
//        put("CATEGORY_STOREs", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.store_management), null, null));
//        put("CATEGORY_PRODUCTs", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.product_management), new CreateStatementImportFC(), null));
        put("MERCHANT_ORDERs", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.order_processing), new ListCountriesFragment(), null));
        put("MERCHANT_ORDERs_LIST_MERCHANT_ORDER", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.check_orders_of_merchant), new ListOrderFragment(), null));
//        put("MERCHANT_ORDERs_CREATE_ADJUST_STATEMENT", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.create_adjust_statement_for_stores), new ListCountriesFragment(), null));
        put("IMPORT_STATEMENTs", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.import_statement), new ListCountriesFragment(), null));
        put("IMPORT_STATEMENT_RELEASES", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.import_statement_release), new CreateExportStatementReleaseFragment(), null));
        put("IMPORT_STATEMENTs_IMPORT_FC", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.create_a_statement_to_FC), new CreateStatementFragment(), "ImportFC"));
        put("IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.create_import_statement_from_export_statement), new CreateImportStatementFromExportFragment(), null));
        put("IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT_RELEASES", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.create_import_statement_from_export_statement_release), new ImportStatementFromExportReleaseFragment(), null));
        put("IMPORT_STATEMENTs_FROM_REPACKING_PLANNING", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.create_import_statement_from_repacking_planning), new ImportFCFromRepackingPlanningFragment(), null));
        put("EXPORT_STATEMENTs", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.export_statement), new ListCountriesFragment(), null));
        put("EXPORT_STATEMENTs_EXPORT_STATEMENT", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.create_an_export_statement), new CreateExportStatementFragment(), null));
        put("UPDATE_PALLET", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.update_pallet), new UpdatePalletFragment(), null));

        //        put("EXPORT_STATEMENTs_EXPORT_FROM_MERCHANT_ORDER", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.create_an_export_statement_from_order), new ImportStoreFromExportFragment(), "createExportFromMerchantOrder"));
        put("EXPORT_STATEMENT_RELEASES", new MenuTitle(Logistic.getInstance().getResources().getString(R.string.export_statement_release), new CreateExportStatementReleaseFragment(), null));

    }};

    public static MenuTitle findMenuByCode(String title) {
        if (ValidateUtils.isNullOrEmpty(title) || !menuTitleMap.containsKey(title)) {
            return null;
        }
        return menuTitleMap.get(title);
    }

//    public static User user;

    public static ApiService callService() {
        return RetrofitClient.getClient().create(ApiService.class);
    }

    public static String changeFormCode(String code){
        StringBuilder result = new StringBuilder();
        String[] s = code.replaceAll("\\r", "").replaceAll("\\n", "").split(" ");
        for (String str : s){
            result.append(str).append(" ");
        }
        return result.toString().trim();
    }
//
    public static ApiService getApiServiceAuth() {
        return RetrofitClient.getAuthClient().create(ApiService.class);
    }

    public static String get() {
        return "";
    }

//
//    public static void requestLogin(MutableLiveData<Throwable> appException, AppUser appUser, ResponseResult result) {
//
//        //loading("Login...");
//
//        OAuth2Client.Builder builder = new OAuth2Client.Builder("Logistics", "XY7kmzoNzl100", AppUtils.BASE_URL_OAUTH + "/oauth/token")
//                .grantType("password")
//                .username(appUser.getUsername())
//                .password(appUser.getPassword());
//
//        builder.build().requestAccessToken(new OAuthResponseCallback() {
//            @Override
//            public void onResponse(OAuthResponse response) {
//                if (response.isSuccessful()) {
//                    String token = response.getAccessToken();
//                    AppUtils.token = token;
//                    RetrofitClient.init(AppUtils.BASE_URL, token);
//                    appUser.setLoginStatus(0);
////                    getRole();
//                    result.onResponse(null, null, null, null);
//                    //ownerView.onClicked(null, model);
//
//                } else {
//                    if (appException != null) {
//                        appException.postValue(new AppException(response.getCode(), ""));
//                    } else {
//                        appUser.setLoginStatus(response.getCode());
//                        result.onResponse(null, null, null, null);
//                    }
////                    ownerView.onClicked(null, model);
//
//                }
//
//            }
//        });
//
//    }
}
