package com.tsolution.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsolution.logistics.utils.AppUtils;
import com.tsolution.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StoreProductPacking extends Base {
    private Store store;
    private ProductPacking productPacking;

    private Product product;

    private Long totalQuantity;
    @JsonFormat(pattern = AppUtils.dateFormat)
    private Date expireDate;
    private List<StoreProductPackingDetail> storeProductPackingDetails;

    public String getExpireDateStr() {
        return expireDate == null ? "" : DateUtils.convertDateToString(expireDate, "dd/MM/yyyy");
    }
}
