package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.Product;
import com.nextsolutions.dcommerce.model.ProductDescription;
import com.nextsolutions.dcommerce.shared.constant.LanguageConstant;
import com.nextsolutions.dcommerce.shared.dto.ProductDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.ProductGeneralDto;
import com.nextsolutions.dcommerce.ui.model.product.PostProductDescriptionRequestModel;
import com.nextsolutions.dcommerce.ui.model.product.PostProductGeneralRequestModel;
import com.nextsolutions.dcommerce.ui.model.product.PutProductDescriptionRequestModel;
import com.nextsolutions.dcommerce.ui.model.product.PutProductGeneralRequestModel;
import com.nextsolutions.dcommerce.ui.model.product.PostProductDescriptionResponseModel;
import com.nextsolutions.dcommerce.ui.model.product.PostProductGeneralResponseModel;
import com.nextsolutions.dcommerce.ui.model.product.PutProductDescriptionResponseModel;
import com.nextsolutions.dcommerce.ui.model.product.PutProductGeneralResponseModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static com.nextsolutions.dcommerce.shared.constant.ProductConstant.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ProductManagementMapperTest.TestConfig.class)
public class ProductManagementMapperTest {

    @Configuration
    static class TestConfig {
        @Bean
        ProductManagementMapper productManagementMapper() {
            return new ProductManagementMapperImpl();
        }

        @Bean
        ProductDescriptionMapper productDescriptionMapper() {
            return new ProductDescriptionMapperImpl();
        }
    }

    @Autowired
    ProductManagementMapper productManagementMapper;

    @Autowired
    ProductDescriptionMapper productDescriptionMapper;

    private ProductGeneralDto createProductGeneralDto() {
        return ProductGeneralDto
                .builder()
                .id(1L)
                .code(VALID_PRODUCT_CODE)
                .internationalName(INTERNATIONAL_NAME)
                .descriptions(Arrays.asList(
                        ProductDescriptionDto.builder()
                                .languageId(LanguageConstant.VIETNAMESE.value())
                                .productId(1L)
                                .title(VIETNAMESE_LOCALIZED_TITLE)
                                .localizedName(VIETNAMESE_LOCALIZED_NAME)
                                .description(VIETNAMESE_DESCRIPTION)
                                .metaTagDescription(VIETNAMESE_META_TAG_DESCRIPTION)
                                .metaTagKeywords(VIETNAMESE_META_TAG_KEYWORDS)
                                .metaTagTitle(VIETNAMESE_META_TAG_TITLE)
                                .productTags(Arrays.asList(VIETNAMESE_PRODUCT_TAGS))
                                .build(),
                        ProductDescriptionDto.builder()
                                .languageId(LanguageConstant.ENGLISH.value())
                                .productId(2L)
                                .title(ENGLISH_META_TITLE)
                                .localizedName(ENGLISH_LOCALIZED_NAME)
                                .description(ENGLISH_DESCRIPTION)
                                .metaTagDescription(ENGLISH_META_TAG_DESCRIPTION)
                                .metaTagKeywords(ENGLISH_META_TAG_KEYWORDS)
                                .metaTagTitle(ENGLISH_META_TAG_TITLE)
                                .productTags(Arrays.asList(ENGLISH_PRODUCT_TAGS))
                                .build(),
                        ProductDescriptionDto.builder()
                                .languageId(LanguageConstant.BURMESE.value())
                                .productId(3L)
                                .title(BURMESE_LOCALIZED_TITLE)
                                .localizedName(BURMESE_LOCALIZED_NAME)
                                .description(BURMESE_DESCRIPTION)
                                .metaTagDescription(BURMESE_META_TAG_DESCRIPTION)
                                .metaTagKeywords(BURMESE_META_TAG_KEYWORDS)
                                .metaTagTitle(BURMESE_META_TAG_TITLE)
                                .productTags(Arrays.asList(BURMESE_PRODUCT_TAGS))
                                .build()
                ))
                .build();
    }

    @Test
    public void testPostProductGeneralRequestModel_To_ProductGeneralDto() {
        PostProductGeneralRequestModel requestModel = PostProductGeneralRequestModel
                .builder()
                .code(VALID_PRODUCT_CODE)
                .internationalName(INTERNATIONAL_NAME)
                .descriptions(Arrays.asList(
                        PostProductDescriptionRequestModel.builder()
                                .languageId(LanguageConstant.VIETNAMESE.value())
                                .localizedName(VIETNAMESE_LOCALIZED_NAME)
                                .description(VIETNAMESE_DESCRIPTION)
                                .metaTagDescription(VIETNAMESE_META_TAG_DESCRIPTION)
                                .metaTagKeywords(VIETNAMESE_META_TAG_KEYWORDS)
                                .metaTagTitle(VIETNAMESE_META_TAG_TITLE)
                                .productTags(Arrays.asList(VIETNAMESE_PRODUCT_TAGS))
                                .build(),
                        PostProductDescriptionRequestModel.builder()
                                .languageId(LanguageConstant.ENGLISH.value())
                                .localizedName(ENGLISH_LOCALIZED_NAME)
                                .build(),
                        PostProductDescriptionRequestModel.builder()
                                .languageId(LanguageConstant.BURMESE.value())
                                .localizedName(BURMESE_LOCALIZED_NAME)
                                .build()
                ))
                .build();

        ProductGeneralDto productGeneralDto = productManagementMapper.toProductGeneralDto(requestModel);

        assertThat(productGeneralDto.getCode()).isEqualTo(VALID_PRODUCT_CODE);
        assertThat(productGeneralDto.getInternationalName()).isEqualTo(INTERNATIONAL_NAME);

        List<ProductDescriptionDto> descriptions = productGeneralDto.getDescriptions();
        assertThat(descriptions).hasSize(3);

        ProductDescriptionDto vietnameseProductDescriptionDto = descriptions.get(0);
        assertThat(vietnameseProductDescriptionDto.getLocalizedName()).isEqualTo(VIETNAMESE_LOCALIZED_NAME);
        assertThat(vietnameseProductDescriptionDto.getDescription()).isEqualTo(VIETNAMESE_DESCRIPTION);
        assertThat(vietnameseProductDescriptionDto.getMetaTagTitle()).isEqualTo(VIETNAMESE_META_TITLE);
        assertThat(vietnameseProductDescriptionDto.getMetaTagDescription()).isEqualTo(VIETNAMESE_META_DESCRIPTION);
        assertThat(vietnameseProductDescriptionDto.getMetaTagKeywords()).isEqualTo(VIETNAMESE_META_KEYWORDS);
        assertThat(vietnameseProductDescriptionDto.getLanguageId()).isEqualTo(1);
        assertThat(vietnameseProductDescriptionDto.getProductTags()).hasSize(3);

        assertThat(descriptions.get(1).getLocalizedName()).isEqualTo(ENGLISH_LOCALIZED_NAME);
        assertThat(descriptions.get(1).getLanguageId()).isEqualTo(2);

        assertThat(descriptions.get(2).getLanguageId()).isEqualTo(3);
        assertThat(descriptions.get(2).getLocalizedName()).isEqualTo(BURMESE_LOCALIZED_NAME);
    }

    @Test
    public void testProductGeneralDto_To_PostProductGeneralResponseModel() {
        ProductGeneralDto requestModel = createProductGeneralDto();

        PostProductGeneralResponseModel responseModel = productManagementMapper.toPostProductGeneralResponseModel(requestModel);

        assertThat(responseModel.getCode()).isEqualTo(VALID_PRODUCT_CODE);
        assertThat(responseModel.getInternationalName()).isEqualTo(INTERNATIONAL_NAME);

        List<PostProductDescriptionResponseModel> descriptions = responseModel.getDescriptions();
        assertThat(descriptions).hasSize(3);

        PostProductDescriptionResponseModel vietnameseProductDescriptionDto = descriptions.get(0);
        assertThat(vietnameseProductDescriptionDto.getTitle()).isEqualTo(VIETNAMESE_LOCALIZED_TITLE);
        assertThat(vietnameseProductDescriptionDto.getLocalizedName()).isEqualTo(VIETNAMESE_LOCALIZED_NAME);
        assertThat(vietnameseProductDescriptionDto.getDescription()).isEqualTo(VIETNAMESE_DESCRIPTION);
        assertThat(vietnameseProductDescriptionDto.getMetaTagTitle()).isEqualTo(VIETNAMESE_META_TAG_TITLE);
        assertThat(vietnameseProductDescriptionDto.getMetaTagDescription()).isEqualTo(VIETNAMESE_META_TAG_DESCRIPTION);
        assertThat(vietnameseProductDescriptionDto.getMetaTagKeywords()).isEqualTo(VIETNAMESE_META_TAG_KEYWORDS);
        assertThat(vietnameseProductDescriptionDto.getLanguageId()).isEqualTo(1);
        assertThat(vietnameseProductDescriptionDto.getProductTags()).hasSize(3);

        assertThat(descriptions.get(1).getLocalizedName()).isEqualTo(ENGLISH_LOCALIZED_NAME);
        assertThat(descriptions.get(1).getLanguageId()).isEqualTo(2);

        assertThat(descriptions.get(2).getLanguageId()).isEqualTo(3);
        assertThat(descriptions.get(2).getLocalizedName()).isEqualTo(BURMESE_LOCALIZED_NAME);
    }

    @Test
    public void testProductGeneralDto_To_Product() {
        ProductGeneralDto productGeneralDto = createProductGeneralDto();

        Product product = productManagementMapper.toProduct(productGeneralDto);

        assertThat(product).isNotNull();

        List<ProductDescription> productDescriptions = product.getProductDescriptions();

        assertThat(productDescriptions).isNotNull();
        assertThat(productDescriptions).hasSize(3);

        ProductDescription vietnameseProductDescription = productDescriptions.get(0);
        assertThat(vietnameseProductDescription.getLanguageId()).isEqualTo(1);
        assertThat(vietnameseProductDescription.getProductId()).isEqualTo(1);
        assertThat(vietnameseProductDescription.getProductName()).isEqualTo(VIETNAMESE_LOCALIZED_NAME);
        assertThat(vietnameseProductDescription.getDescription()).isEqualTo(VIETNAMESE_DESCRIPTION);
    }

    @Test
    public void testProduct_To_ProductGeneralDto() {
        Product product = Product.builder()
                .id(1L)
                .code(VALID_PRODUCT_CODE)
                .name(INTERNATIONAL_NAME)
                .productDescriptions(Arrays.asList(
                        ProductDescription.builder()
                                .id(1L)
                                .languageId(1L)
                                .productId(1L)
                                .title(VIETNAMESE_LOCALIZED_TITLE)
                                .productName(VIETNAMESE_LOCALIZED_NAME)
                                .description(VIETNAMESE_DESCRIPTION)
                                .metaDescription(VIETNAMESE_META_TAG_DESCRIPTION)
                                .metaKeywords(VIETNAMESE_META_TAG_KEYWORDS)
                                .metaTitle(VIETNAMESE_META_TAG_TITLE)
                                .build(),
                        ProductDescription.builder()
                                .id(2L)
                                .languageId(2L)
                                .productId(1L)
                                .productName(ENGLISH_LOCALIZED_NAME)
                                .build(),
                        ProductDescription.builder()
                                .id(3L)
                                .languageId(3L)
                                .productId(1L)
                                .productName(BURMESE_LOCALIZED_NAME)
                                .build()
                ))
                .build();

        ProductGeneralDto productGeneralDto = productManagementMapper.toProductGeneralDto(product);

        assertThat(productGeneralDto).isNotNull();
        assertThat(productGeneralDto.getId()).isNotNull();

        List<ProductDescriptionDto> descriptions = productGeneralDto.getDescriptions();

        assertThat(descriptions).isNotNull();
        assertThat(descriptions).hasSize(3);

        ProductDescriptionDto vietnameseProductDescriptionDto = descriptions.get(0);
        assertThat(vietnameseProductDescriptionDto.getId()).isEqualTo(1L);
        assertThat(vietnameseProductDescriptionDto.getProductId()).isEqualTo(1L);
        assertThat(vietnameseProductDescriptionDto.getLocalizedName()).isEqualTo(VIETNAMESE_LOCALIZED_NAME);

        ProductDescriptionDto englishProductDescriptionDto = descriptions.get(1);
        assertThat(englishProductDescriptionDto.getId()).isEqualTo(2L);
        assertThat(englishProductDescriptionDto.getProductId()).isEqualTo(1L);
        assertThat(englishProductDescriptionDto.getLocalizedName()).isEqualTo(ENGLISH_LOCALIZED_NAME);

        ProductDescriptionDto burmeseProductDescriptionDto = descriptions.get(2);
        assertThat(burmeseProductDescriptionDto.getId()).isEqualTo(3L);
        assertThat(burmeseProductDescriptionDto.getProductId()).isEqualTo(1L);
        assertThat(burmeseProductDescriptionDto.getLocalizedName()).isEqualTo(BURMESE_LOCALIZED_NAME);
    }

    @Test
    public void testPutProductGeneralRequestModel_To_ProductGeneralDto() {
        PutProductGeneralRequestModel requestModel = PutProductGeneralRequestModel
                .builder()
                .code(VALID_PRODUCT_CODE)
                .internationalName(INTERNATIONAL_NAME)
                .descriptions(Arrays.asList(
                        PutProductDescriptionRequestModel.builder()
                                .languageId(LanguageConstant.VIETNAMESE.value())
                                .id(1L)
                                .localizedName(VIETNAMESE_LOCALIZED_NAME)
                                .description(VIETNAMESE_DESCRIPTION)
                                .metaTagDescription(VIETNAMESE_META_TAG_DESCRIPTION)
                                .metaTagKeywords(VIETNAMESE_META_TAG_KEYWORDS)
                                .metaTagTitle(VIETNAMESE_META_TAG_TITLE)
                                .productTags(Arrays.asList(VIETNAMESE_PRODUCT_TAGS))
                                .build(),
                        PutProductDescriptionRequestModel.builder()
                                .id(2L)
                                .languageId(LanguageConstant.ENGLISH.value())
                                .localizedName(ENGLISH_LOCALIZED_NAME)
                                .build(),
                        PutProductDescriptionRequestModel.builder()
                                .id(3L)
                                .languageId(LanguageConstant.BURMESE.value())
                                .localizedName(BURMESE_LOCALIZED_NAME)
                                .build()
                ))
                .build();
        ProductGeneralDto productGeneralDto = productManagementMapper.toProductGeneralDto(requestModel);

        assertThat(productGeneralDto).isNotNull();

        List<ProductDescriptionDto> descriptions = productGeneralDto.getDescriptions();

        assertThat(descriptions).isNotNull();
        assertThat(descriptions).hasSize(3);

        ProductDescriptionDto vietnameseProductDescriptionDto = descriptions.get(0);
        assertThat(vietnameseProductDescriptionDto.getId()).isEqualTo(1L);
        assertThat(vietnameseProductDescriptionDto.getLocalizedName()).isEqualTo(VIETNAMESE_LOCALIZED_NAME);

        ProductDescriptionDto englishProductDescriptionDto = descriptions.get(1);
        assertThat(englishProductDescriptionDto.getId()).isEqualTo(2L);
        assertThat(englishProductDescriptionDto.getLocalizedName()).isEqualTo(ENGLISH_LOCALIZED_NAME);

        ProductDescriptionDto burmeseProductDescriptionDto = descriptions.get(2);
        assertThat(burmeseProductDescriptionDto.getId()).isEqualTo(3L);
        assertThat(burmeseProductDescriptionDto.getLocalizedName()).isEqualTo(BURMESE_LOCALIZED_NAME);
    }

    @Test
    public void testProductGeneral_To_PutProductGeneralResponseModel() {
        ProductGeneralDto productGeneralDto = createProductGeneralDto();

        PutProductGeneralResponseModel responseModel = productManagementMapper.toPutProductGeneralResponseModel(productGeneralDto);

        assertThat(responseModel.getCode()).isEqualTo(VALID_PRODUCT_CODE);
        assertThat(responseModel.getInternationalName()).isEqualTo(INTERNATIONAL_NAME);

        List<PutProductDescriptionResponseModel> descriptions = responseModel.getDescriptions();
        assertThat(descriptions).hasSize(3);

        PutProductDescriptionResponseModel vietnameseProductDescriptionDto = descriptions.get(0);
        assertThat(vietnameseProductDescriptionDto.getTitle()).isEqualTo(VIETNAMESE_LOCALIZED_TITLE);
        assertThat(vietnameseProductDescriptionDto.getLocalizedName()).isEqualTo(VIETNAMESE_LOCALIZED_NAME);
        assertThat(vietnameseProductDescriptionDto.getDescription()).isEqualTo(VIETNAMESE_DESCRIPTION);
        assertThat(vietnameseProductDescriptionDto.getMetaTagTitle()).isEqualTo(VIETNAMESE_META_TAG_TITLE);
        assertThat(vietnameseProductDescriptionDto.getMetaTagDescription()).isEqualTo(VIETNAMESE_META_TAG_DESCRIPTION);
        assertThat(vietnameseProductDescriptionDto.getMetaTagKeywords()).isEqualTo(VIETNAMESE_META_TAG_KEYWORDS);
        assertThat(vietnameseProductDescriptionDto.getLanguageId()).isEqualTo(1);
        assertThat(vietnameseProductDescriptionDto.getProductTags()).hasSize(3);

        assertThat(descriptions.get(1).getLocalizedName()).isEqualTo(ENGLISH_LOCALIZED_NAME);
        assertThat(descriptions.get(1).getLanguageId()).isEqualTo(2);

        assertThat(descriptions.get(2).getLanguageId()).isEqualTo(3);
        assertThat(descriptions.get(2).getLocalizedName()).isEqualTo(BURMESE_LOCALIZED_NAME);
    }
}
