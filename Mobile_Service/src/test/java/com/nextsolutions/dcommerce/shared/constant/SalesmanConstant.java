package com.nextsolutions.dcommerce.shared.constant;

public interface SalesmanConstant {
    String SALESMAN_USERNAME = "sonsausac01";
    String SALESMAN_FULL_NAME = "Sơn Sâu Sắc";
    String SALESMAN_PHONE_NUMBER = "0985583625";
    Integer SALESMAN_GENDER = 1;
    Integer SALESMAN_STATUS = 1;
    String SALESMAN_ADDRESS = "Số 2, đường Lê Văn Thiêm, phường Nhân Chính, quận Thanh Xuân, thành phố Hà Nội";
    String SALESMAN_EMAIL = "sonsausac@gmail.com";
}
