package com.nextsolutions.dcommerce.shared.constant;

public interface ProductConstant {

    String VALID_PRODUCT_CODE = "NOODLE001";

    String INTERNATIONAL_NAME = "Omachi instant noodle";

    String VIETNAMESE_LOCALIZED_TITLE = "Mì ăn liền Omachi";
    String VIETNAMESE_LOCALIZED_NAME = "Mì ăn liền Omachi";
    String VIETNAMESE_DESCRIPTION = "Mô tả sản phẩm bằng tiếng Việt";
    String VIETNAMESE_META_TITLE = "Omachi";
    String VIETNAMESE_META_DESCRIPTION = "Mì Omachi ngon";
    String VIETNAMESE_META_KEYWORDS = "Omachi, omachi, OMACHI, Mì Omachi";
    String[] VIETNAMESE_PRODUCT_TAGS = {"Omachi", "Omachi instant noodle", "Instant noodle"};
    String VIETNAMESE_META_TAG_DESCRIPTION = "Omachi, Omachi instant noodle, Instant noodle";
    String VIETNAMESE_META_TAG_KEYWORDS = "Omachi, Omachi instant noodle, Instant noodle";
    String VIETNAMESE_META_TAG_TITLE = "Omachi instant noodle";

    String ENGLISH_LOCALIZED_TITLE = "Omachi instant noodle";
    String ENGLISH_LOCALIZED_NAME = "Omachi instant noodle";
    String ENGLISH_DESCRIPTION = "Omachi instant noodle description";
    String ENGLISH_META_TITLE = "Omachi";
    String ENGLISH_META_DESCRIPTION = "Omachi instant";
    String ENGLISH_META_KEYWORDS = "Omachi, omachi, OMACHI, Mì Omachi";
    String[] ENGLISH_PRODUCT_TAGS = {"Omachi", "Omachi instant noodle", "Instant noodle"};
    String ENGLISH_META_TAG_DESCRIPTION = "Omachi, Omachi instant noodle, Instant noodle";
    String ENGLISH_META_TAG_KEYWORDS = "Omachi, Omachi instant noodle, Instant noodle";
    String ENGLISH_META_TAG_TITLE = "Omachi instant noodle";

    String BURMESE_LOCALIZED_TITLE = "Omachi instant noodle";
    String BURMESE_LOCALIZED_NAME = "Omachi ချက်ချင်းခေါက်ဆွဲ";
    String BURMESE_DESCRIPTION = "ချက်ချင်းခေါက်ဆွဲ";
    String BURMESE_META_TITLE = "ချက်ချင်းခေါက်ဆွဲ";
    String BURMESE_META_DESCRIPTION = "ချက်ချင်းခေါက်ဆွဲ";
    String BURMESE_META_KEYWORDS = "ချက်ချင်းခေါ, က်ချဆွဲါ, က်က်ဆွဲ, က်ဆးခေွဲ";
    String[] BURMESE_PRODUCT_TAGS = {"Omachi", "Omachi instant noodle", "Instant noodle"};
    String BURMESE_META_TAG_DESCRIPTION = "Omachi, Omachi instant noodle, Instant noodle";
    String BURMESE_META_TAG_KEYWORDS = "ချက်ချင်းခေါ, က်ချဆွဲါ, က်က်ဆွဲ, က်ဆးခေွဲ";
    String BURMESE_META_TAG_TITLE = "Omachi instant noodle";
}
