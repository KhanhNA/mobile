package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.ui.model.salesman.CreateSalesmanRequestModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SalesmanMapperTest.TestConfig.class)
public class SalesmanMapperTest {

    @Configuration
    static class TestConfig {
    }

    @Test
    public void name() {
        CreateSalesmanRequestModel requestModel = CreateSalesmanRequestModel
                .builder()
                .phoneNumber("0985583625")
                .address("Số 2, đường Lê Văn Thiêm, phường Nhân Chính, quận Thanh Xuân, thành phố Hà Nội")
                .email("sonsausac@gmail.com")
                .fullName("Sơn Sâu Sắc")
                .gender(1)
                .status(1)
                .username("sonsausac01")
                .birthDate(LocalDateTime.of(1991, 3, 20, 0, 0))
                .build();
    }
}
