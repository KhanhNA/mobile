package com.nextsolutions.dcommerce.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nextsolutions.dcommerce.shared.constant.LanguageConstant;
import com.nextsolutions.dcommerce.ui.model.product.PostProductDescriptionRequestModel;
import com.nextsolutions.dcommerce.ui.model.product.PostProductGeneralRequestModel;
import com.nextsolutions.dcommerce.ui.model.product.PostProductGeneralResponseModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static com.nextsolutions.dcommerce.shared.constant.ProductConstant.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ActiveProfiles({"test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductIntegrationTest {

    @Value("${security.oauth2.client.clientId}")
    String clientId;

    @Value("${security.oauth2.client.clientSecret}")
    String clientSecret;

    @Value("${security.oauth2.client.accessTokenUri}")
    String accessTokenUri;

    static final String GENERAL_ENDPOINT = "/api/v1/products";

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    ObjectMapper objectMapper;

    String accessToken;

//    @Before
//    public void setUp() throws Exception {
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//        headers.setBasicAuth(clientId, clientSecret);
//        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
//        map.add("username", "admin");
//        map.add("password", "abc@123");
//        map.add("grant_type", "password");
//
//        ResponseEntity<String> response = restTemplate.exchange(
//                accessTokenUri,
//                HttpMethod.POST,
//                new HttpEntity<>(map, headers),
//                String.class);
//        JsonNode node = objectMapper.readTree(response.getBody());
//        accessToken = node.get("access_token").asText();
//    }

    @Test
    public void createProductGeneral() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(accessToken);

        PostProductGeneralRequestModel postProductGeneralRequestModel = PostProductGeneralRequestModel
                .builder()
                .code(VALID_PRODUCT_CODE)
                .internationalName(INTERNATIONAL_NAME)
                .descriptions(Arrays.asList(
                        PostProductDescriptionRequestModel
                                .builder()
                                .title(VIETNAMESE_LOCALIZED_TITLE)
                                .localizedName(VIETNAMESE_LOCALIZED_NAME)
                                .description(VIETNAMESE_META_DESCRIPTION)
                                .languageId(LanguageConstant.VIETNAMESE.value())
                                .metaTagTitle(VIETNAMESE_META_TITLE)
                                .metaTagDescription(VIETNAMESE_META_DESCRIPTION)
                                .metaTagKeywords(VIETNAMESE_META_KEYWORDS)
                                .productTags(Arrays.asList("Mì", "Mỳ", "Mỳ ăn liền"))
                                .build(),
                        PostProductDescriptionRequestModel
                                .builder()
                                .languageId(LanguageConstant.ENGLISH.value())
                                .title(ENGLISH_LOCALIZED_TITLE)
                                .localizedName(ENGLISH_LOCALIZED_NAME)
                                .description(ENGLISH_DESCRIPTION)
                                .metaTagTitle(ENGLISH_META_TITLE)
                                .metaTagDescription(ENGLISH_META_DESCRIPTION)
                                .metaTagKeywords(ENGLISH_META_KEYWORDS)
                                .productTags(Arrays.asList("Mì", "Mỳ", "Mỳ ăn liền"))
                                .build(),
                        PostProductDescriptionRequestModel
                                .builder()
                                .languageId(LanguageConstant.BURMESE.value())
                                .title(BURMESE_META_TITLE)
                                .localizedName(BURMESE_LOCALIZED_NAME)
                                .description(BURMESE_META_DESCRIPTION)
                                .metaTagTitle(BURMESE_META_TITLE)
                                .metaTagDescription(BURMESE_META_DESCRIPTION)
                                .metaTagKeywords(BURMESE_META_KEYWORDS)
                                .productTags(Arrays.asList("Mì", "Mỳ", "Mỳ ăn liền"))
                                .build()
                        )
                )
                .build();

        HttpEntity<PostProductGeneralRequestModel> entity = new HttpEntity<>(postProductGeneralRequestModel, headers);
        ResponseEntity<PostProductGeneralResponseModel> response = restTemplate.postForEntity(
                GENERAL_ENDPOINT,
                entity,
                PostProductGeneralResponseModel.class
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

//        PostProductGeneralResponseModel body = response.getBody();
//        assertThat(body).isNotNull();
//        assertThat(body.getId()).isNotNull();
//        assertThat(body.getCode()).isEqualTo(PRODUCT_CODE);
//        assertThat(body.getInternationalName()).isEqualTo(INTERNATIONAL_NAME);
//
//        assertThat(body.getDescriptions()).hasSize(3);
//
//        PostProductDescriptionResponseModel vietnameseProductDescription = body.getDescriptions().get(0);
//        assertThat(vietnameseProductDescription.getId()).isNotNull();

    }

}
