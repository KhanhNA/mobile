package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.Product;
import com.nextsolutions.dcommerce.repository.ProductRepository;
import com.nextsolutions.dcommerce.service.ProductManagementService;
import com.nextsolutions.dcommerce.shared.constant.LanguageConstant;
import com.nextsolutions.dcommerce.shared.dto.ProductDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.ProductGeneralDto;
import com.nextsolutions.dcommerce.shared.mapper.ProductDescriptionMapper;
import com.nextsolutions.dcommerce.shared.mapper.ProductDescriptionMapperImpl;
import com.nextsolutions.dcommerce.shared.mapper.ProductManagementMapper;
import com.nextsolutions.dcommerce.shared.mapper.ProductManagementMapperImpl;
import com.nextsolutions.dcommerce.ui.controller.AbstractControllerTest;
import com.nextsolutions.dcommerce.ui.model.product.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static com.nextsolutions.dcommerce.shared.constant.ProductConstant.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ProductManagementController.class)
@ContextConfiguration(classes = ProductManagementControllerTest.MapperConfig.class)
public class ProductManagementControllerTest extends AbstractControllerTest {

    private static final String PRODUCT_ENDPOINT = "/api/v1/products";

    @Configuration
    public static class MapperConfig {
        @Bean
        ProductManagementMapper productManagementMapper() {
            return new ProductManagementMapperImpl();
        }

        @Bean
        ProductDescriptionMapper productDescriptionMapper() {
            return new ProductDescriptionMapperImpl();
        }
    }

    @Autowired
    ProductManagementMapper productManagementMapper;

    @MockBean
    ProductManagementService productManagementService;

    @MockBean
    ProductRepository productRepository;

    @Test
    public void createProductGeneral_whenCreateProductGeneralIsValid_receiveOK() throws Exception {
        PostProductGeneralRequestModel postProductGeneralRequestModel = createValidCreateProductGeneralRequestModelBody();

        mvc.perform(post(PRODUCT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(postProductGeneralRequestModel)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void createProductGeneral_whenCreateProductGeneralIsValid_receiveProductGeneral() throws Exception {
        PostProductGeneralRequestModel postProductGeneralRequestModel = createValidCreateProductGeneralRequestModelBody();

        ProductGeneralDto productGeneralDto = createProductResponseGeneral();

        when(productManagementService.createProduct(any())).thenReturn(productGeneralDto);

        mvc.perform(post(PRODUCT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(postProductGeneralRequestModel)))
                .andDo(print())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.code").value(VALID_PRODUCT_CODE))
                .andExpect(jsonPath("$.internationalName").value(INTERNATIONAL_NAME))
                .andExpect(jsonPath("$.descriptions").isArray())
                .andExpect(jsonPath("$.descriptions[0].id").value(1L))
                .andExpect(jsonPath("$.descriptions[0].languageId").value(LanguageConstant.VIETNAMESE.value()))
                .andExpect(jsonPath("$.descriptions[0].localizedName").value(VIETNAMESE_LOCALIZED_NAME))
                .andExpect(jsonPath("$.descriptions[1].id").value(2L))
                .andExpect(jsonPath("$.descriptions[1].languageId").value(LanguageConstant.ENGLISH.value()))
                .andExpect(jsonPath("$.descriptions[1].localizedName").value(ENGLISH_LOCALIZED_NAME))
                .andExpect(jsonPath("$.descriptions[2].id").value(3L))
                .andExpect(jsonPath("$.descriptions[2].languageId").value(LanguageConstant.BURMESE.value()))
                .andExpect(jsonPath("$.descriptions[2].localizedName").value(BURMESE_LOCALIZED_NAME));
    }

    @Test
    public void createProductGeneral_whenDoesNotProvideProductGeneral_receiveBadRequest() throws Exception {
        mvc.perform(post(PRODUCT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createProductGeneral_whenProductGeneralHasNullCode_receiveBadRequest() throws Exception {
        PostProductGeneralRequestModel postProductGeneralRequestModel = PostProductGeneralRequestModel
                .builder()
                .code("$$$")
                .internationalName("  ")
                .descriptions(Arrays.asList(
                        PostProductDescriptionRequestModel.builder().localizedName("").build(),
                        PostProductDescriptionRequestModel.builder().localizedName("").build(),
                        PostProductDescriptionRequestModel.builder().localizedName("").build()
                ))
                .build();

        mvc.perform(post(PRODUCT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(postProductGeneralRequestModel)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateProductGeneral_whenPutProductGeneralIsValid_receiveOk() throws Exception {
        ProductGeneralDto productGeneralDto = createProductResponseGeneral();

        when(productManagementService.updateProductGeneral(any(), any())).thenReturn(productGeneralDto);

        mvc.perform(put(String.format("%s/%s/general", PRODUCT_ENDPOINT, 1L))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productGeneralDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.code").value(VALID_PRODUCT_CODE))
                .andExpect(jsonPath("$.internationalName").value(INTERNATIONAL_NAME))
                .andExpect(jsonPath("$.descriptions").isArray())
                .andExpect(jsonPath("$.descriptions[0].id").value(1L))
                .andExpect(jsonPath("$.descriptions[0].languageId").value(LanguageConstant.VIETNAMESE.value()))
                .andExpect(jsonPath("$.descriptions[0].localizedName").value(VIETNAMESE_LOCALIZED_NAME))
                .andExpect(jsonPath("$.descriptions[1].id").value(2L))
                .andExpect(jsonPath("$.descriptions[1].languageId").value(LanguageConstant.ENGLISH.value()))
                .andExpect(jsonPath("$.descriptions[1].localizedName").value(ENGLISH_LOCALIZED_NAME))
                .andExpect(jsonPath("$.descriptions[2].id").value(3L))
                .andExpect(jsonPath("$.descriptions[2].languageId").value(LanguageConstant.BURMESE.value()))
                .andExpect(jsonPath("$.descriptions[2].localizedName").value(BURMESE_LOCALIZED_NAME));
    }

    @Test
    public void updateProductGeneral_whenPutProductGeneralNullInternationalName_receiveBadRequest() throws Exception {
        PutProductGeneralRequestModel putProductGeneralRequestModel = createValidPutProductGeneralRequestModelBody();
        putProductGeneralRequestModel.setInternationalName(null);
        mvc.perform(put(String.format("%s/%s/general", PRODUCT_ENDPOINT, 1L))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(putProductGeneralRequestModel)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getProductGeneral_whenIdIsValid_receiveOK() throws Exception {
        ProductGeneralDto productGeneralDto = createProductResponseGeneral();
        when(productManagementService.getProductGeneral(any())).thenReturn(productGeneralDto);
        mvc.perform(get(String.format("%s/%s/general", PRODUCT_ENDPOINT, 1L))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getProductGeneral_whenIdIsValid_receiveProductGeneral() throws Exception {
        ProductGeneralDto productGeneralDto = createProductResponseGeneral();
        when(productManagementService.getProductGeneral(any())).thenReturn(productGeneralDto);
        mvc.perform(get(String.format("%s/%s/general", PRODUCT_ENDPOINT, 1L))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.code").value(VALID_PRODUCT_CODE))
                .andExpect(jsonPath("$.internationalName").value(INTERNATIONAL_NAME))
                .andExpect(jsonPath("$.descriptions").isArray())
                .andExpect(jsonPath("$.descriptions[0].id").value(1L))
                .andExpect(jsonPath("$.descriptions[0].languageId").value(LanguageConstant.VIETNAMESE.value()))
                .andExpect(jsonPath("$.descriptions[0].localizedName").value(VIETNAMESE_LOCALIZED_NAME))
                .andExpect(jsonPath("$.descriptions[1].id").value(2L))
                .andExpect(jsonPath("$.descriptions[1].languageId").value(LanguageConstant.ENGLISH.value()))
                .andExpect(jsonPath("$.descriptions[1].localizedName").value(ENGLISH_LOCALIZED_NAME))
                .andExpect(jsonPath("$.descriptions[2].id").value(3L))
                .andExpect(jsonPath("$.descriptions[2].languageId").value(LanguageConstant.BURMESE.value()))
                .andExpect(jsonPath("$.descriptions[2].localizedName").value(BURMESE_LOCALIZED_NAME));
    }

    @Test
    public void updateProductData_whenPutProductDataIsValid_receiveOk() throws Exception {
        when(productRepository.findByCode(any())).thenReturn(Optional.of(new Product()));
        mvc.perform(put(String.format("%s/%s/data", PRODUCT_ENDPOINT, 1L))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(PutProductDataRequestModel.builder()
                        .brand("Brand Test")
                        .origin("Vietnam")
                        .sku("01921242")
                        .barcode("01921242")
                        .manufacturerPartNumber("12032")
                        .isbn("12323123")
                        .length(BigDecimal.ONE)
                        .width(BigDecimal.ONE)
                        .height(BigDecimal.ONE)
                        .lengthClass("mm")
                        .weight(BigDecimal.ONE)
                        .weightClass("kg")
                        .status(1)
                        .lifecycle(1)
                        .code("012")
                        .build())))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private PutProductDataRequestModel createValidProductData() {
        return PutProductDataRequestModel
                .builder()
                .code("CODE")
                .brand("Brand Test")
                .origin("Vietnam")
                .sku("01921242")
                .barcode("01921242")
                .manufacturerPartNumber("12032")
                .isbn("12323123")
                .length(BigDecimal.ONE)
                .width(BigDecimal.ONE)
                .height(BigDecimal.ONE)
                .lengthClass("mm")
                .weight(BigDecimal.ONE)
                .weightClass("kg")
                .status(1)
                .lifecycle(1)
                .dateAvailable(LocalDateTime.now())
                .build();
    }

    private PostProductGeneralRequestModel createValidCreateProductGeneralRequestModelBody() {
        return PostProductGeneralRequestModel
                .builder()
                .code(VALID_PRODUCT_CODE)
                .internationalName(INTERNATIONAL_NAME)
                .descriptions(Arrays.asList(
                        PostProductDescriptionRequestModel.builder()
                                .languageId(LanguageConstant.VIETNAMESE.value())
                                .localizedName(VIETNAMESE_LOCALIZED_NAME)
                                .build(),
                        PostProductDescriptionRequestModel.builder()
                                .languageId(LanguageConstant.ENGLISH.value())
                                .localizedName(ENGLISH_LOCALIZED_NAME)
                                .build(),
                        PostProductDescriptionRequestModel.builder()
                                .languageId(LanguageConstant.BURMESE.value())
                                .localizedName(BURMESE_LOCALIZED_NAME)
                                .build()
                ))
                .build();
    }

    private PutProductGeneralRequestModel createValidPutProductGeneralRequestModelBody() {
        return PutProductGeneralRequestModel
                .builder()
                .code(VALID_PRODUCT_CODE)
                .internationalName(INTERNATIONAL_NAME)
                .descriptions(Arrays.asList(
                        PutProductDescriptionRequestModel.builder()
                                .id(1L)
                                .languageId(LanguageConstant.VIETNAMESE.value())
                                .localizedName(VIETNAMESE_LOCALIZED_NAME)
                                .build(),
                        PutProductDescriptionRequestModel.builder()
                                .id(2L)
                                .languageId(LanguageConstant.ENGLISH.value())
                                .localizedName(ENGLISH_LOCALIZED_NAME)
                                .build(),
                        PutProductDescriptionRequestModel.builder()
                                .id(3L)
                                .languageId(LanguageConstant.BURMESE.value())
                                .localizedName(BURMESE_LOCALIZED_NAME)
                                .build()
                ))
                .build();
    }

    private ProductGeneralDto createProductResponseGeneral() {
        return ProductGeneralDto.builder()
                .id(1L)
                .code(VALID_PRODUCT_CODE)
                .internationalName(INTERNATIONAL_NAME)
                .descriptions(Arrays.asList(
                        ProductDescriptionDto.builder()
                                .id(1L)
                                .languageId(LanguageConstant.VIETNAMESE.value())
                                .localizedName(VIETNAMESE_LOCALIZED_NAME)
                                .build(),
                        ProductDescriptionDto.builder()
                                .id(2L)
                                .languageId(LanguageConstant.ENGLISH.value())
                                .localizedName(ENGLISH_LOCALIZED_NAME)
                                .build(),
                        ProductDescriptionDto.builder()
                                .id(3L)
                                .languageId(LanguageConstant.BURMESE.value())
                                .localizedName(BURMESE_LOCALIZED_NAME)
                                .build()
                ))
                .build();
    }
}
