package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.Distributor;
import com.nextsolutions.dcommerce.repository.DistributorRepository;
import com.nextsolutions.dcommerce.service.DistributorService;
import com.nextsolutions.dcommerce.shared.dto.distributor.DistributorDto;
import com.nextsolutions.dcommerce.shared.mapper.DistributorMapper;
import com.nextsolutions.dcommerce.shared.mapper.DistributorMapperImpl;
import com.nextsolutions.dcommerce.ui.controller.AbstractControllerTest;
import com.nextsolutions.dcommerce.ui.model.distributor.PostDistributorRequestModel;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = DistributorController.class)
@ContextConfiguration(classes = DistributorControllerTest.MapperConfig.class)
public class DistributorControllerTest extends AbstractControllerTest {

    private static final String DISTRIBUTOR_URI = "/api/v1/distributors";

    @Configuration
    public static class MapperConfig {
        @Bean
        DistributorMapper distributorMapper() {
            return new DistributorMapperImpl();
        }
    }

    @MockBean
    DistributorService distributorService;

    @MockBean
    DistributorRepository distributorRepository;

    @Autowired
    DistributorMapper distributorMapper;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void createDistributor_whenPostDistributorRequestModelIsValid_receiveCreated() throws Exception {
        PostDistributorRequestModel requestModel = createValidPostDistributorRequestModel();
        mvc.perform(post(DISTRIBUTOR_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void createDistributor_whenPostDistributorHasPhoneNumberExists_receiveBadRequest() throws Exception {
        PostDistributorRequestModel requestModel = createValidPostDistributorRequestModel();
        when(distributorService.findByPhoneNumber(any())).thenReturn(Optional.of(new Distributor()));
        mvc.perform(post("/api/v1/distributors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createDistributor_whenPostDistributorHasPhoneNumberExists_receiveMessageOfFieldPhoneNumber() throws Exception {
        PostDistributorRequestModel requestModel = createValidPostDistributorRequestModel();
//        PostDistributorRequestModel requestModel = new PostDistributorRequestModel();
        requestModel.setPhoneNumber("0239423493");
        when(distributorService.findByPhoneNumber(any())).thenReturn(Optional.of(new Distributor()));
        mvc.perform(post("/api/v1/distributors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(jsonPath("$.fieldErrors.phoneNumber").value("Phone number is exists"));
    }

    private PostDistributorRequestModel createValidPostDistributorRequestModel() {
        return PostDistributorRequestModel.builder()
                .name("mfunction")
                .aboutUs("mfunction")
                .address("mfunction address")
                .code("mfunction")
                .phoneNumber("0912312342")
                .username("username")
                .email("mfunction@mfunction.com")
                .taxCode("0123912313")
                .enabled(true)
                .build();
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveOk() throws Exception {
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithCode() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.code").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithName() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.name").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithUsername() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.username").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithWalletId() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.walletId").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithAddress() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.address").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithPhoneNumber() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.phoneNumber").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithEmail() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.email").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithEnabled() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.enabled").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithAboutUs() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.aboutUs").exists());
    }

    @Test
    public void getDistributor_whenIdIsValid_receiveGetDistributorResponseModelWithTaxcode() throws Exception {
        when(distributorService.getDistributor(any())).thenReturn(sampleDistributorDto());
        mvc.perform(get(DISTRIBUTOR_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.taxCode").exists());
    }

    private DistributorDto sampleDistributorDto() {
        return DistributorDto.builder()
                .id(1L)
                .code("123")
                .name("HuaVanSon")
                .distributorId("asfwefwd92g82ndfsdf")
                .username("abc")
                .walletId(1L)
                .address("sjdfsdf")
                .phoneNumber("01238123213")
                .email("asdvs@gmail.com")
                .enabled(true)
                .aboutUs("anc")
                .taxCode("1231203123")
                .createdTime(LocalDateTime.now())
                .latestModified(LocalDateTime.now())
                .build();
    }
}
