package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.service.impl.FileStorageService;
import com.nextsolutions.dcommerce.ui.controller.AbstractControllerTest;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = CommonRestController.class)
public class CommonRestControllerTest extends AbstractControllerTest {

    private static final String LIST_ALL_ENDPOINT = "/api/v1/common/list";

    @MockBean
    CommonService commonService;

    @MockBean
    FileStorageService fileStorageService;

    @Test
    public void testListDistributor() throws Exception {
        Page<Distributor> distributorPage = new PageImpl<>(
                Stream.of(Distributor.builder()
                        .id(1L)
                        .code("test")
                        .name("test")
                        .username("test")
                        .aboutUs("test")
                        .phoneNumber("0123912312")
                        .walletId(1L)
                        .address("test")
                        .createdTime(LocalDateTime.now())
                        .isSynchronization(false)
                        .email("test@test.com")
                        .latestModified(LocalDateTime.now())
                        .enabled(true)
                        .taxCode("0123123")
                        .build())
                        .collect(Collectors.toList()),
                Pageable.unpaged(),
                1);

        when(commonService.findAll(any(Distributor.class), any(), any())).thenReturn(distributorPage);

        mvc.perform(post(LIST_ALL_ENDPOINT)
                .param("entityName", "Distributor"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].id").value(1))
                .andExpect(jsonPath("$.content[0].code").hasJsonPath())
                .andExpect(jsonPath("$.content[0].name").hasJsonPath())
                .andExpect(jsonPath("$.content[0].aboutUs").hasJsonPath())
                .andExpect(jsonPath("$.content[0].phoneNumber").hasJsonPath())
                .andExpect(jsonPath("$.content[0].walletId").hasJsonPath())
                .andExpect(jsonPath("$.content[0].address").hasJsonPath())
                .andExpect(jsonPath("$.content[0].createdTime").hasJsonPath())
                .andExpect(jsonPath("$.content[0].isSynchronization").hasJsonPath())
                .andExpect(jsonPath("$.content[0].email").hasJsonPath())
                .andExpect(jsonPath("$.content[0].latestModified").hasJsonPath())
                .andExpect(jsonPath("$.content[0].taxCode").hasJsonPath())
                .andExpect(jsonPath("$.content[0].enabled").hasJsonPath());
    }

    @Test
    public void testListProduct() throws Exception {
        Page<Product> productPage = new PageImpl<>(
                Stream.of(Product.builder()
                        .id(1L)
                        .code("test")
                        .productDescriptions(new ArrayList<>())
                        .manufacturer(Manufacturer.builder().id(1L).code("test").build())
                        .productType(ProductType.builder().id(1L).code("test").build())
                        .distributor(Distributor.builder().id(1L).code("test").build())
                        .calculatedLength(BigDecimal.ONE)
                        .calculatedWidth(BigDecimal.ONE)
                        .calculatedHeight(BigDecimal.ONE)
                        .calculatedWeight(BigDecimal.ONE)
                        .warningThreshold(1)
                        .lifecycle(1)
                        .build())
                        .collect(Collectors.toList()),
                Pageable.unpaged(),
                1);

        when(commonService.findAll(any(Product.class), any(), any())).thenReturn(productPage);

        mvc.perform(post(LIST_ALL_ENDPOINT)
                .param("entityName", "Product"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].code").value("test"))
                .andExpect(jsonPath("$.content[0].productDescriptions").isArray())
                .andExpect(jsonPath("$.content[0].manufacturer.id").value(1))
                .andExpect(jsonPath("$.content[0].manufacturer.code").value("test"))
                .andExpect(jsonPath("$.content[0].productType.id").value(1))
                .andExpect(jsonPath("$.content[0].productType.code").value("test"))
                .andExpect(jsonPath("$.content[0].distributor.id").value(1))
                .andExpect(jsonPath("$.content[0].distributor.code").value("test"))
                .andExpect(jsonPath("$.content[0].calculatedLength").value(1))
                .andExpect(jsonPath("$.content[0].calculatedWidth").value(1))
                .andExpect(jsonPath("$.content[0].calculatedHeight").value(1))
                .andExpect(jsonPath("$.content[0].calculatedWeight").value(1))
                .andExpect(jsonPath("$.content[0].warningThreshold").value(1))
                .andExpect(jsonPath("$.content[0].lifecycle").value(1));
    }

    @Test
    public void testListManufacturer() throws Exception {
        Page<Manufacturer> manufacturerPage = new PageImpl<>(
                Stream.of(Manufacturer.builder()
                        .id(1L)
                        .code("test")
                        .modifiedDate(new Date())
                        .createdDate(new Date())
                        .updtId("123456")
                        .email("test@test.com")
                        .image("test")
                        .merchantId(123456789)
                        .sortOrder(25869775)
                        .tel("test")
                        .descriptions(new ArrayList<>())
                        .isSynchronization(false)
                        .build()).collect(Collectors.toList()),
                Pageable.unpaged(),

                1);
        when(commonService.findAll(any(Manufacturer.class), any(), any())).thenReturn(manufacturerPage);

        mvc.perform(post(LIST_ALL_ENDPOINT)
                .param("entityName", "Manufacturer"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].id").value(1))
                .andExpect(jsonPath("$.content[0].code").value("test"))
                .andExpect(jsonPath("$.content[0].modifiedDate").hasJsonPath())
                .andExpect(jsonPath("$.content[0].createdDate").hasJsonPath())
                .andExpect(jsonPath("$.content[0].updtId").hasJsonPath())
                .andExpect(jsonPath("$.content[0].email").hasJsonPath())
                .andExpect(jsonPath("$.content[0].image").hasJsonPath())
                .andExpect(jsonPath("$.content[0].merchantId").hasJsonPath())
                .andExpect(jsonPath("$.content[0].sortOrder").hasJsonPath())
                .andExpect(jsonPath("$.content[0].tel").hasJsonPath())
                .andExpect(jsonPath("$.content[0].descriptions").hasJsonPath())
                .andExpect(jsonPath("$.content[0].isSynchronization").hasJsonPath());
    }

    @Test
    public void testListProductType() throws Exception {
        Page<ProductType> productTypePage = new PageImpl<>(
                Stream.of(ProductType.builder()
                        .id(1L)
                        .code("test")
                        .prdTypeAddToCart(true)
                        .createdDate(new Date())
                        .isSynchronization(false)
                        .modifiedDate(new Date())
                        .name("test")
                        .updtId("1234556")
                        .build()).collect(Collectors.toList()),
                Pageable.unpaged(),
                1
        );
        when(commonService.findAll(any(ProductType.class), any(), any())).thenReturn(productTypePage);

        mvc.perform(post(LIST_ALL_ENDPOINT)
                .param("entityName", "ProductType"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].id").value(1))
                .andExpect(jsonPath("$.content[0].code").value("test"))
                .andExpect(jsonPath("$.content[0].prdTypeAddToCart").hasJsonPath())
                .andExpect(jsonPath("$.content[0].createdDate").hasJsonPath())
                .andExpect(jsonPath("$.content[0].isSynchronization").hasJsonPath())
                .andExpect(jsonPath("$.content[0].modifiedDate").hasJsonPath())
                .andExpect(jsonPath("$.content[0].name").hasJsonPath())
                .andExpect(jsonPath("$.content[0].updtId").hasJsonPath());

    }

    @Test
    public void testListPackingProduct() throws Exception {
        Page<PackingProduct> packingProductPage = new PageImpl<>(
                Stream.of(PackingProduct.builder()
                        .packingProductId(1L)
                        .code("test")
                        .product(ProductEntity.builder().id(1L).code("test").build())
                        .packingType(PackingTypeEntity.builder().packingTypeId(1L).code("test").build())
                        .distributor(DistributorLink.builder().id(1L).code("test").build())
                        .calculatedHeight(BigDecimal.ONE)
                        .calculatedWeight(BigDecimal.ONE)
                        .calculatedLength(BigDecimal.ONE)
                        .calculatedWidth(BigDecimal.ONE)
                        .waningThreshold(1)
                        .lifecycle(1)
                        .build()).collect(Collectors.toList()),
                Pageable.unpaged(),
                1);
        when(commonService.findAll(any(PackingProduct.class), any(), any())).thenReturn(packingProductPage);
        mvc.perform(post(LIST_ALL_ENDPOINT)
                .param("entityName", "PackingProduct"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].code").value("test"))
                .andExpect(jsonPath("$.content[0].product.id").value(1))
                .andExpect(jsonPath("$.content[0].product.code").value("test"))
                .andExpect(jsonPath("$.content[0].packingType.packingTypeId").value(1))
                .andExpect(jsonPath("$.content[0].packingType.code").value("test"))
                .andExpect(jsonPath("$.content[0].distributor.id").value(1))
                .andExpect(jsonPath("$.content[0].distributor.code").value("test"))
                .andExpect(jsonPath("$.content[0].calculatedLength").value(1))
                .andExpect(jsonPath("$.content[0].calculatedWidth").value(1))
                .andExpect(jsonPath("$.content[0].calculatedHeight").value(1))
                .andExpect(jsonPath("$.content[0].calculatedWeight").value(1))
                .andExpect(jsonPath("$.content[0].waningThreshold").value(1))
                .andExpect(jsonPath("$.content[0].lifecycle").value(1));
    }

    @Test
    public void testListPackingPrice() throws Exception {
        Page<PackingPrice> packingPricePage = new PageImpl<>(
                Stream.of(PackingPrice.builder()
                        .id(1L)
                        .packingProduct(ProductPackingEntity.builder().packingProductId(1L).code("test").build())
                        .priceType(1234)
                        .fromDate(LocalDateTime.now())
                        .toDate(LocalDateTime.now())
                        .currency(Currency.builder().id(1L).code("test").build())
                        .build()).collect(Collectors.toList()),
                Pageable.unpaged(),
                1
        );
        when(commonService.findAll(any(PackingPrice.class), any(), any())).thenReturn(packingPricePage);
        mvc.perform(post(LIST_ALL_ENDPOINT)
                .param("entityName", "PackingPrice"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].packingProduct.packingProductId").value(1))
                .andExpect(jsonPath("$.content[0].packingProduct.code").value("test"))
                .andExpect(jsonPath("$.content[0].fromDate").hasJsonPath())
                .andExpect(jsonPath("$.content[0].toDate").hasJsonPath())
                .andExpect(jsonPath("$.content[0].currency.id").value(1))
                .andExpect(jsonPath("$.content[0].currency.code").value("test"));
    }

}
