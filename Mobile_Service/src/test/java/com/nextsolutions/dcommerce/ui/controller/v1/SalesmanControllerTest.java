package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.service.SalesmanService;
import com.nextsolutions.dcommerce.shared.constant.SalesmanConstant;
import com.nextsolutions.dcommerce.shared.dto.SalesmanDto;
import com.nextsolutions.dcommerce.shared.mapper.*;
import com.nextsolutions.dcommerce.ui.controller.AbstractControllerTest;
import com.nextsolutions.dcommerce.ui.model.salesman.CreateSalesmanRequestModel;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.nextsolutions.dcommerce.shared.constant.SalesmanConstant.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = SalesmanController.class)
@ContextConfiguration(classes = {SalesmanControllerTest.MapperConfig.class})
public class SalesmanControllerTest extends AbstractControllerTest {

    private final String API_V1_SALESMEN = "/api/v1/salesmen";

    @Configuration
    public static class MapperConfig {
        @Bean
        SalesmanMapper salesmanMapper() {
            return new SalesmanMapperImpl();
        }
    }

    @Autowired
    SalesmanMapper salesmanMapper;

    @MockBean
    SalesmanService salesmanService;

    @MockBean
    MerchantJpaRepository merchantJpaRepository;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        when(salesmanService.createSalesman(any())).thenReturn(SalesmanDto
                .builder()
                .salesmanId(1L)
                .username(SALESMAN_USERNAME)
                .phoneNumber(SALESMAN_PHONE_NUMBER)
                .status(SALESMAN_STATUS)
                .email(SALESMAN_EMAIL)
                .gender(SALESMAN_GENDER)
                .address(SALESMAN_ADDRESS)
                .fullName(SALESMAN_FULL_NAME)
                .build()
        );
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelIsValid_receiveCreated() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelIsValid_receiveCreateSalesmanResponseModelHasId() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(jsonPath("$.id").isNumber());
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelIsValid_receiveCreateSalesmanResponseModelHasUsername() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(jsonPath("$.username").value(SALESMAN_USERNAME));
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelIsValid_receiveCreateSalesmanResponseModelHasPhoneNumber() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(jsonPath("$.phoneNumber").value(SALESMAN_PHONE_NUMBER));
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelIsValid_receiveCreateSalesmanResponseModelHasAddress() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(jsonPath("$.address").value(SALESMAN_ADDRESS));
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelIsValid_receiveCreateSalesmanResponseModelHasFullName() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(jsonPath("$.fullName").value(SALESMAN_FULL_NAME));
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelIsValid_receiveCreateSalesmanResponseModelHasGender() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(jsonPath("$.gender").value(SALESMAN_GENDER));
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelIsValid_receiveCreateSalesmanResponseModelHasStatus() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(jsonPath("$.status").value(SALESMAN_STATUS));
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelNullUsername_receiveBadRequest() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        requestModel.setUsername(null);
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelNullFullname_receiveBadRequest() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        requestModel.setFullName(null);
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelHasFullNameLengthLessThanRequired_receiveBadRequest() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        requestModel.setFullName("a");
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelHasFullNameLengthExceedsRequired_receiveBadRequest() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        String valueOf46Chars = IntStream.range(0, 46).mapToObj(a -> "a").collect(Collectors.joining());
        requestModel.setFullName(valueOf46Chars);
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelNullPhoneNumber_receiveBadRequest() throws Exception {
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        requestModel.setPhoneNumber(null);
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createSalesman_whenCreateSalesmanRequestModelExistsPhoneNumber_receiveBadRequest() throws Exception {
        when(merchantJpaRepository.findByMobilePhone(any())).thenReturn(Optional.of(new Merchant()));
        CreateSalesmanRequestModel requestModel = createValidSalesman();
        mvc.perform(post(API_V1_SALESMEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestModel)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }


    private CreateSalesmanRequestModel createValidSalesman() {
        return CreateSalesmanRequestModel
                .builder()
                .username(SALESMAN_USERNAME)
                .phoneNumber(SALESMAN_PHONE_NUMBER)
                .address(SALESMAN_ADDRESS)
                .email(SALESMAN_EMAIL)
                .fullName(SALESMAN_FULL_NAME)
                .gender(SALESMAN_GENDER)
                .status(SALESMAN_STATUS)
                .birthDate(LocalDateTime.of(1991, 3, 20, 0, 0))
                .build();
    }
}
