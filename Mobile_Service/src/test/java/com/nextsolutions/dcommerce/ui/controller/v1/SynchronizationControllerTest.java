package com.nextsolutions.dcommerce.ui.controller.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nextsolutions.dcommerce.service.SynchronizationService;
import com.nextsolutions.dcommerce.ui.controller.AbstractControllerTest;
import com.nextsolutions.dcommerce.ui.model.request.SynchronizationResource;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = SynchronizationController.class)
public class SynchronizationControllerTest extends AbstractControllerTest {

    private static final String ENDPOINT = "/api/v1/synchronize";

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    SynchronizationService synchronizationService;

    @Test
    public void synchronizeAllRelatedProducts() throws Exception {
        List<SynchronizationResource> synchronizationResources = new ArrayList<>(1);
        SynchronizationResource synchronizationResource = new SynchronizationResource();
        synchronizationResource.setEntityName("Product");
        synchronizationResource.setIds(Stream.of(1L).collect(Collectors.toList()));
        synchronizationResources.add(synchronizationResource);

        Map<String, List<Long>> entityUpdatingFailure = new HashMap<>();

        when(synchronizationService.synchronize(any())).thenReturn(entityUpdatingFailure);

        mvc.perform(post(ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(synchronizationResources)))
                .andDo(print())
                .andExpect(status().isNoContent());
    }
}
