package com.nextsolutions.dcommerce.ui.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nextsolutions.dcommerce.exception.FieldValidationError;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AbstractControllerTest.TestConfig.class)
public abstract class AbstractControllerTest {

    @Configuration
    @EnableSpringDataWebSupport
    public static class TestConfig extends WebMvcConfigurationSupport {

        @Override
        public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
            argumentResolvers.add(new PageableHandlerMethodArgumentResolver());
        }

        @Bean
        ObjectMapper objectMapper() {
            return new ObjectMapper();
        }

        @RestControllerAdvice
        static class GlobalHandler extends ResponseEntityExceptionHandler {
            @Autowired
            MessageSource messageSource;

            @Override
            protected ResponseEntity<Object> handleMethodArgumentNotValid(
                    MethodArgumentNotValidException exception,
                    HttpHeaders headers,
                    HttpStatus status,
                    WebRequest request
            ) {
                BindingResult bindingResult = exception.getBindingResult();
                List<FieldError> fieldErrors = bindingResult.getFieldErrors();
                Map<String, String> errors = new LinkedHashMap<>();
                for (FieldError error : fieldErrors) {
                    errors.put(error.getField(), error.getDefaultMessage());
                }
                return new ResponseEntity<>(new FieldValidationError(400, "Validation error", errors),
                        HttpStatus.BAD_REQUEST);
            }
        }
    }

    protected MockMvc mvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
}
