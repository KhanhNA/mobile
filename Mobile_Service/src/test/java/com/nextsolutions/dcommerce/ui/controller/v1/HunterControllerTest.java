package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.HunterService;
import com.nextsolutions.dcommerce.shared.dto.HunterDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.mapper.HunterMapper;
import com.nextsolutions.dcommerce.shared.mapper.HunterMapperImpl;
import com.nextsolutions.dcommerce.ui.controller.AbstractControllerTest;
import com.nextsolutions.dcommerce.ui.model.request.UpdateHunterRequestModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = HunterController.class)
@ContextConfiguration(classes = HunterControllerTest.MapperConfig.class)
public class HunterControllerTest extends AbstractControllerTest {

    @Configuration
    public static class MapperConfig {
        @Bean
        HunterMapper hunterMapper() {
            return new HunterMapperImpl();
        }
    }

    @MockBean
    HunterService hunterService;

    @Autowired
    HunterMapper hunterMapper;

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void getHunters() {
    }

    @Test
    public void createHunter() {
    }

    @Test
    public void getHunter() throws Exception {
        Long hunterId = 1L;

        HunterDto hunterDto = new HunterDto();
        hunterDto.setHunterId(1L);
        hunterDto.setFullName("test");

        when(hunterService.getHunterById(hunterId)).thenReturn(hunterDto);

        mvc.perform(get("/api/v1/hunters/{id}", hunterId)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.fullName").value("test"))
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    public void getMerchantsByHunterId() throws Exception {
//        Long hunterId = 1L;
//
//        List<MerchantDto> merchantsDto = Stream.of(MerchantDto.builder()
//                .merchantId(1L)
//                .fullName("test")
//                .build())
//                .collect(Collectors.toList());
//
//        Page<MerchantDto> page = new PageImpl<>(merchantsDto, PageRequest.of(0, 10), 1);
//
//        when(hunterService.findMerchants(any(), any(), any())).thenReturn(page);
//
//        mvc.perform(get("/api/v1/hunters/{hunterId}/merchants", hunterId))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.content").isArray());
    }

    @Test
    public void updateHunter() throws Exception {
        UpdateHunterRequestModel requestModel = UpdateHunterRequestModel.builder()
                .username("")
                .fullName("")
                .gender(1)
                .phoneNumber("0213123")
                .status(1)
                .build();

        mvc.perform(put("/api/v1/hunters/{hunterId}", 1)
                .content(objectMapper.writeValueAsString(requestModel))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
