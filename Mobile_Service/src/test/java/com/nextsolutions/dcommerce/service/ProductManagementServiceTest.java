package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.Product;
import com.nextsolutions.dcommerce.model.ProductDescription;
import com.nextsolutions.dcommerce.repository.ProductRepository;
import com.nextsolutions.dcommerce.service.impl.ProductManagementServiceImpl;
import com.nextsolutions.dcommerce.shared.constant.LanguageConstant;
import com.nextsolutions.dcommerce.shared.dto.ProductDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.ProductGeneralDto;
import com.nextsolutions.dcommerce.shared.mapper.ProductDescriptionMapper;
import com.nextsolutions.dcommerce.shared.mapper.ProductDescriptionMapperImpl;
import com.nextsolutions.dcommerce.shared.mapper.ProductManagementMapper;
import com.nextsolutions.dcommerce.shared.mapper.ProductManagementMapperImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;

import static com.nextsolutions.dcommerce.shared.constant.ProductConstant.*;
import static com.nextsolutions.dcommerce.shared.constant.ProductConstant.BURMESE_LOCALIZED_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ProductManagementServiceTest.TestConfig.class})
public class ProductManagementServiceTest {

    @Configuration
    static class TestConfig {
        @Bean
        ProductManagementMapper productManagementMapper () {
            return new ProductManagementMapperImpl();
        }

        @Bean
        ProductDescriptionMapper productDescriptionMapper () {
            return new ProductDescriptionMapperImpl();
        }
    }

    private ProductManagementService productManagementService;

    @Mock
    private ProductRepository productRepository;

    @Autowired
    private ProductManagementMapper productManagementMapper;

    @Before
    public void setUp() {
        productManagementService = new ProductManagementServiceImpl(productRepository, productManagementMapper);
    }

    @Test
    public void givenProductGeneralDto_whenCreateProduct_thenReturnsProductGeneralDtoHasId() {
        ProductGeneralDto productGeneralDto = ProductGeneralDto.builder()
                .build();

        when(productRepository.save(any())).thenReturn(Product.builder().id(1L).build());

        ProductGeneralDto result = productManagementService.createProduct(productGeneralDto);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();
    }

    @Test
    public void givenProductGeneralDto_whenUpdateProductGeneral_thenReturnsCorrectValue() {
        ProductGeneralDto productGeneralDto = ProductGeneralDto.builder()
                .id(1L)
                .code("UPDATED PRODUCT CODE")
                .internationalName("INTERNATIONAL PRODUCT NAME")
                .descriptions(Arrays.asList(
                        ProductDescriptionDto.builder()
                                .id(1L)
                                .languageId(LanguageConstant.VIETNAMESE.value())
                                .productId(1L)
                                .title(VIETNAMESE_LOCALIZED_TITLE)
                                .localizedName(VIETNAMESE_LOCALIZED_NAME)
                                .description(VIETNAMESE_DESCRIPTION)
                                .metaTagDescription(VIETNAMESE_META_DESCRIPTION)
                                .metaTagKeywords(VIETNAMESE_META_KEYWORDS)
                                .metaTagTitle(VIETNAMESE_META_TITLE)
                                .build(),
                        ProductDescriptionDto.builder()
                                .id(2L)
                                .languageId(LanguageConstant.ENGLISH.value())
                                .productId(1L)
                                .localizedName(ENGLISH_LOCALIZED_NAME)
                                .description(ENGLISH_DESCRIPTION)
                                .metaTagDescription(ENGLISH_META_DESCRIPTION)
                                .metaTagKeywords(ENGLISH_META_KEYWORDS)
                                .metaTagTitle(ENGLISH_META_TITLE)
                                .build(),
                        ProductDescriptionDto.builder()
                                .id(3L)
                                .languageId(LanguageConstant.BURMESE.value())
                                .productId(1L)
                                .localizedName(BURMESE_LOCALIZED_NAME)
                                .description(BURMESE_DESCRIPTION)
                                .metaTagDescription(BURMESE_META_DESCRIPTION)
                                .metaTagKeywords(BURMESE_META_KEYWORDS)
                                .metaTagTitle(BURMESE_META_TITLE)
                                .build()
                ))
                .build();

        when(productRepository.findById(any())).thenReturn(Optional.of(new Product()));

        when(productRepository.save(any())).thenReturn(Product.builder()
                .id(1L)
                .code("UPDATED PRODUCT CODE")
                .name("UPDATED INTERNATIONAL PRODUCT NAME")
                .productDescriptions(Arrays.asList(
                        ProductDescription.builder()
                                .id(1L)
                                .languageId(1L)
                                .productId(1L)
                                .title(VIETNAMESE_LOCALIZED_TITLE)
                                .productName(VIETNAMESE_LOCALIZED_NAME)
                                .description(VIETNAMESE_DESCRIPTION)
                                .metaDescription(VIETNAMESE_META_DESCRIPTION)
                                .metaKeywords(VIETNAMESE_META_KEYWORDS)
                                .metaTitle(VIETNAMESE_META_TITLE)
                                .build(),
                        ProductDescription.builder()
                                .id(2L)
                                .languageId(2L)
                                .productId(1L)
                                .productName(ENGLISH_LOCALIZED_NAME)
                                .description(ENGLISH_DESCRIPTION)
                                .metaDescription(ENGLISH_META_DESCRIPTION)
                                .metaKeywords(ENGLISH_META_KEYWORDS)
                                .metaTitle(ENGLISH_META_TITLE)
                                .build(),
                        ProductDescription.builder()
                                .id(3L)
                                .languageId(3L)
                                .productId(1L)
                                .productName(BURMESE_LOCALIZED_NAME)
                                .description(BURMESE_DESCRIPTION)
                                .metaDescription(BURMESE_META_DESCRIPTION)
                                .metaKeywords(BURMESE_META_KEYWORDS)
                                .metaTitle(BURMESE_META_TITLE)
                                .build()
                ))
                .build()
        );

        ProductGeneralDto result = productManagementService.updateProductGeneral(1L, productGeneralDto);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(1L);
        assertThat(result.getCode()).isEqualTo("UPDATED PRODUCT CODE");
        assertThat(result.getInternationalName()).isEqualTo("UPDATED INTERNATIONAL PRODUCT NAME");
    }
}
