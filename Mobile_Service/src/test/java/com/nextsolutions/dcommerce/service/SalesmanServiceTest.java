package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.shared.dto.SalesmanDto;
import com.nextsolutions.dcommerce.shared.mapper.SalesmanMapper;
import com.nextsolutions.dcommerce.shared.mapper.SalesmanMapperImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.nextsolutions.dcommerce.shared.constant.SalesmanConstant.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {SalesmanServiceTest.MapperConfig.class})
public class SalesmanServiceTest {

    @Configuration
    public static class MapperConfig {
        @Bean
        SalesmanMapper salesmanMapper() {
            return new SalesmanMapperImpl();
        }
    }

    private SalesmanService salesmanService;

    @Autowired
    private SalesmanMapper salesmanMapper;

    @MockBean
    private MerchantJpaRepository merchantJpaRepository;

    @Before
    public void setUp() throws Exception {
        salesmanService = new SalesmanServiceImpl(merchantJpaRepository, salesmanMapper);

        SalesmanDto salesmanDto = createSalesmanDto();

        when(merchantJpaRepository.save(any())).thenReturn(createSalesmanDto());
    }

    @Test
    public void createSalesman_whenSalesmanDtoIsValid_returnsSalesmanDtoHasId() {
        SalesmanDto salesmanDto = createSalesmanDto();
        salesmanDto.setSalesmanId(null);
        SalesmanDto createdSalesmanDto = salesmanService.createSalesman(salesmanDto);
        assertThat(createdSalesmanDto.getSalesmanId()).isNotNull();
    }

    private SalesmanDto createSalesmanDto() {
        return SalesmanDto
                .builder()
                .username(SALESMAN_USERNAME)
                .phoneNumber(SALESMAN_PHONE_NUMBER)
                .status(SALESMAN_STATUS)
                .email(SALESMAN_EMAIL)
                .gender(SALESMAN_GENDER)
                .address(SALESMAN_ADDRESS)
                .fullName(SALESMAN_FULL_NAME)
                .build();
    }
}
