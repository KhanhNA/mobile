package com.nextsolutions.dcommerce.utils;

import org.junit.Test;

import java.math.BigDecimal;

import static com.nextsolutions.dcommerce.utils.UnitUtils.*;
import static org.junit.Assert.assertEquals;

public class UnitUtilsTest {

    private void assertDouble(double expected, double actual) {
        assertEquals(BigDecimal.valueOf(expected).doubleValue(), actual, 0);
    }

    @Test
    public void testConvertMetersToMillimeters() {
        assertDouble(1000, convertMetersToMillimeters(BigDecimal.valueOf(1)).doubleValue());
        assertDouble(1200, convertMetersToMillimeters(BigDecimal.valueOf(1.2)).doubleValue());
        assertDouble(1632, convertMetersToMillimeters(BigDecimal.valueOf(1.632)).doubleValue());
    }

    @Test
    public void testConvertCentimetersToMillimeters() {
        assertDouble(10, convertCentimetersToMillimeters(BigDecimal.valueOf(1)).doubleValue());
        assertDouble(12, convertCentimetersToMillimeters(BigDecimal.valueOf(1.2)).doubleValue());
        assertDouble(16.32, convertCentimetersToMillimeters(BigDecimal.valueOf(1.632)).doubleValue());
    }

    @Test
    public void testConvertPoundsToGrams() {
        assertDouble(45.359237, convertPoundsToGrams(BigDecimal.valueOf(0.1)).doubleValue());
        assertDouble(453.59237, convertPoundsToGrams(BigDecimal.valueOf(1)).doubleValue());
    }

    @Test
    public void testConvertKilogramsToGrams() {
        assertDouble(1000, convertKilogramsToGrams(BigDecimal.valueOf(1)).doubleValue());
        assertDouble(1200, convertKilogramsToGrams(BigDecimal.valueOf(1.2)).doubleValue());
    }

    @Test
    public void testConvertMilligramsToGrams() {
        assertDouble(0.003, convertMilligramsToGrams(BigDecimal.valueOf(3)).doubleValue());
        assertDouble(0.0012, convertMilligramsToGrams(BigDecimal.valueOf(1.2)).doubleValue());
    }

    @Test
    public void testConvertOuncesToGrams() {
        assertDouble(28.34952, convertOuncesToGrams(BigDecimal.valueOf(1)).doubleValue());
    }
}
