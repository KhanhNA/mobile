package com.nextsolutions.dcommerce.repository.custom.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IncentiveDateRange {
    private Long id;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
}
