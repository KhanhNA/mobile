package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.shared.dto.distributor.DistributorDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DistributorRepositoryCustom {
    Page<DistributorDto> queryDistributors(DistributorDto distributorDto, Pageable pageable);
}
