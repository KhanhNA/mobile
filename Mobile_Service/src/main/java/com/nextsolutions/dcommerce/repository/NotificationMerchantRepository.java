package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.NotificationMerchant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationMerchantRepository extends JpaRepository<NotificationMerchant, Long> {
}
