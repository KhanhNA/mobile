package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import com.nextsolutions.dcommerce.shared.dto.ProductDto;

import com.nextsolutions.dcommerce.ui.model.request.CategoryGroupPriceRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductPackageResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PackingProductRepositoryCustom {
    Page<ProductDto> findAll(Pageable pageable, Long langId, String keyWord);

    Page<ProductDto> findAllIn(Pageable pageable, List<String> lstPredictionId, Long langId);

    Page<ProductDto> findAllNotIn(Pageable pageable, List<String> lstPredictionId, Long langId, int filterType);

    Page<ProductDto> findAllByCategory(Pageable pageable, Long langId, Long categoryId);

    List<PackingProductDTO> findAllByProductIdInV2(List<Long> ids);

    int applyGroupPrice(CategoryGroupPriceRequestModel request);

    ProductPackageResource findAllPackingByProductId(Long id);

    Page<PackingProductSearchResult> searchPackingProduct(String keyword, Long languageId, Long incentiveProgramId, Pageable pageable);

    Page<PackingProductSearchResult> searchPackingProductByKeyWord(String keyword, Long languageId, Pageable pageable);

    List<PackingProductSearchResult> findAllPackingProductByCategoryIdAndLanguageId(Long categoryId, Long languageId, Long incentiveProgramId);

    List<PackingProductSearchResult> findAllPackingProductByManufacturerIdAndLanguageId(Long manufacturerId, Long languageId, Long incentiveProgramId);

    List<PackingProductSearchResult> findAllPackingProductWith(List<Long> packingProductIds, Long languageId, Long incentiveProgramId);

    PackingProductSearchResult findPackingProductSearchBy(Long incentiveProgramId, Long packingProductId, Long languageId);

}

