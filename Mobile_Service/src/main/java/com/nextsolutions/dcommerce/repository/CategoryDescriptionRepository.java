package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.CategoryDescription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryDescriptionRepository extends JpaRepository<CategoryDescription, Long> {
    Optional<CategoryDescription> findByCategoryIdAndLanguageId(Long categoryId, Long languageId);
}
