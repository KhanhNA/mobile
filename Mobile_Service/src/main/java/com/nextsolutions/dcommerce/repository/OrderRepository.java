package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.repository.custom.OrderRepositoryCustom;
import com.nextsolutions.dcommerce.model.order.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long>, OrderRepositoryCustom {
}
