package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.incentive.IncentivePackingSale;
import org.springframework.data.jpa.repository.JpaRepository;


public interface GroupLevelDetailJpaRepository extends JpaRepository<IncentivePackingSale, Long> {
}
