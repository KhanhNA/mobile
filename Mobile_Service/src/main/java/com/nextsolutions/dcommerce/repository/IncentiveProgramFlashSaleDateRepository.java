package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.IncentiveProgramFlashSaleDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IncentiveProgramFlashSaleDateRepository extends JpaRepository<IncentiveProgramFlashSaleDate, Long>, JpaSpecificationExecutor<IncentiveProgramFlashSaleDate> {

    void deleteByIncentiveProgramDetailInformationId(Long id);
}
