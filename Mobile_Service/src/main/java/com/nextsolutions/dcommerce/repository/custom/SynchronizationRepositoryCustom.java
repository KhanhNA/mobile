package com.nextsolutions.dcommerce.repository.custom;

import java.util.List;

public interface SynchronizationRepositoryCustom {
    void synchronize(String tableName, List<Long> updateIds) throws ClassNotFoundException;
}
