package com.nextsolutions.dcommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.MerchantGroupDescription;

public interface MerchantGroupDescriptionRepository extends JpaRepository<MerchantGroupDescription, Long>, JpaSpecificationExecutor<MerchantGroupDescription> {

}