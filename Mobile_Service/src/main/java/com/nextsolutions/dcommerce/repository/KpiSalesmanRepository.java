package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.KpiSalesman;
import com.nextsolutions.dcommerce.repository.custom.KpiSalesmanRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface KpiSalesmanRepository extends JpaRepository<KpiSalesman, Long>, JpaSpecificationExecutor<KpiSalesman>, KpiSalesmanRepositoryCustom {
    Optional<KpiSalesman> findById(Long id);
}
