package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.repository.custom.OrderRepositoryCustom;
import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import com.nextsolutions.dcommerce.shared.dto.OrderPackingInfo;
import com.nextsolutions.dcommerce.ui.model.request.OrderRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderDetailsResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class OrderRepositoryCustomImpl implements OrderRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean changeStatusOrder(OrderDto orderDto) {
        String sql = "UPDATE Order SET ORDER_STATUS = :orderStatus, DELIVERY_STATUS = :deliveryStatus, PAYMENT_STATUS = :paymentStatus WHERE ORDER_ID = :orderId";
        Query getPackingQuery = em.createQuery(sql.toString());
        getPackingQuery.setParameter("orderStatus", orderDto.getOrderStatus());
        getPackingQuery.setParameter("deliveryStatus", orderDto.getDeliveryStatus());
        getPackingQuery.setParameter("paymentStatus", orderDto.getPaymentStatus());
        getPackingQuery.setParameter("orderId", orderDto.getOrderId());
        int i = getPackingQuery.executeUpdate();
        return i == 1;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<OrderResponseModel> findByParams(OrderRequestModel orderRequestModel, Pageable pageable) {
        String selectClause = " select o.order_id id, " +
                "m.merchant_id merchantId, " +
                "m.merchant_name merchantName, " +
                "o.order_status status, " +
                "o.amount amount, " +
                "o.order_date dateAdded, " +
                "o.date_modified dateModified, " +
                "o.logistic_code logisticsCode, " +
                "o.reason";
        String selectCount = "select count(*) ";
        String fromClause = " from orders o join merchant m on o.merchant_id = m.merchant_id ";
        String whereClause = " where m.merchant_type_id = 1 ";
        String orderByClause = " ORDER BY " + pageable.getSort().stream()
                .map(sort -> sort.getProperty() + " " + sort.getDirection().name())
                .collect(Collectors.joining(", "));

        if (Objects.nonNull(orderRequestModel.getId())) {
            whereClause += " AND o.order_id = :orderId";
        }

        if (StringUtils.hasText(orderRequestModel.getMerchantName())) {
            whereClause += " AND lower(m.merchant_name) like lower(trim(:merchantName)) ";
        }

        if (Objects.nonNull(orderRequestModel.getAmount())) {
            whereClause += " AND o.amount = :amount ";
        }

        if (Objects.nonNull(orderRequestModel.getStatus())) {
            whereClause += " AND o.order_status = :orderStatus ";
        }

        if (Objects.nonNull(orderRequestModel.getDateAdded())) {
            whereClause += " AND DATE(o.order_date) = DATE(:dateAdded) ";
        }

        if (Objects.nonNull(orderRequestModel.getDateModified())) {
            whereClause += " AND DATE(o.date_modified) = DATE(:dateModified) ";
        }

        Query nativeQuery = em.createNativeQuery(selectClause + fromClause + whereClause + orderByClause,
                "OrderResponseMapping");
        Query nativeQueryCount = em.createNativeQuery(selectCount + fromClause + whereClause);

        if (Objects.nonNull(orderRequestModel.getId())) {
            nativeQuery.setParameter("orderId", orderRequestModel.getId());
            nativeQueryCount.setParameter("orderId", orderRequestModel.getId());
        }

        if (StringUtils.hasText(orderRequestModel.getMerchantName())) {
            nativeQuery.setParameter("merchantName", "%" + orderRequestModel.getMerchantName() + "%");
            nativeQueryCount.setParameter("merchantName", "%" + orderRequestModel.getMerchantName() + "%");
        }

        if (Objects.nonNull(orderRequestModel.getAmount())) {
            nativeQuery.setParameter("amount", orderRequestModel.getAmount());
            nativeQueryCount.setParameter("amount", orderRequestModel.getAmount());
        }

        if (Objects.nonNull(orderRequestModel.getStatus())) {
            nativeQuery.setParameter("orderStatus", orderRequestModel.getStatus());
            nativeQueryCount.setParameter("orderStatus", orderRequestModel.getStatus());
        }

        if (Objects.nonNull(orderRequestModel.getDateAdded())) {
            nativeQuery.setParameter("dateAdded", orderRequestModel.getDateAdded());
            nativeQueryCount.setParameter("dateAdded", orderRequestModel.getDateAdded());
        }

        if (Objects.nonNull(orderRequestModel.getDateModified())) {
            nativeQuery.setParameter("dateModified", orderRequestModel.getDateModified());
            nativeQueryCount.setParameter("dateModified", orderRequestModel.getDateModified());
        }

        nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        nativeQuery.setMaxResults(pageable.getPageSize());

        List<OrderResponseModel> resultList = nativeQuery.getResultList();
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();
        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public OrderDetailsResponseModel getOrderDetailsById(Long id) {
        Query primaryQuery = em.createNativeQuery("select 'fake store' store,  " +
                "       o.order_date orderDate,  " +
                "       pm.payment_method_name paymentMethod,  " +
                "       'fake shipping method' shippingMethod,  " +
                "       m.merchant_name merchantName,  " +
                "       'fake merchant group' merchantGroup,  " +
                "       'mail@fake.com' email,  " +
                "       m.mobile_phone phoneNumber,  " +
                "       o.order_id orderId,  " +
                "       o.logistic_code logisticsCode " +
                "from orders o  " +
                "         join merchant m on o.merchant_id = m.merchant_id  " +
                "         left join payment_method pm on o.payment_method_id = pm.payment_method_id  " +
                "where order_id = :orderId", "OrderDetailsResponseMapping");
        primaryQuery.setParameter("orderId", id);

        try {
            OrderDetailsResponseModel orderDetailsResponseModel = (OrderDetailsResponseModel) primaryQuery.getSingleResult();

            Query secondQuery = em.createNativeQuery("select  " +
                    "  op.order_packing_id id,  " +
                    "  p.product_code productCode,  " +
                    "  pd.product_name productName,  " +
                    "  pp.code packingProductCode,  " +
                    "  pp.name packingProductName,  " +
                    "  op.order_quantity quantity,  " +
                    "  op.price price,  " +
                    "  op.vat vat,  " +
                    "  op.amount total  " +
                    "from  " +
                    "  order_packing_v2 op  " +
                    "  join product p on p.product_id = op.product_id  " +
                    "  join product_description pd on p.product_id = pd.product_id  " +
                    "  join packing_product pp on op.packing_product_id = pp.packing_product_id  " +
                    "where  " +
                    "  op.order_id = :orderId  " +
                    "  and pd.language_id = 1", "orderPackingInfoMapping");

            secondQuery.setParameter("orderId", id);
            List<OrderPackingInfo> details = secondQuery.getResultList();
            orderDetailsResponseModel.setDetails(details);
            return orderDetailsResponseModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean cancelOrderByIdWithReason(Long id, String reason) {
        Query query = em.createNativeQuery("UPDATE orders o " +
                "set o.order_status = -1, o.reason = :reason " +
                "where o.order_id = :id");
        query.setParameter("reason", reason);
        query.setParameter("id", id);
        int rowAffects = query.executeUpdate();
        return rowAffects > 0;
    }
}
