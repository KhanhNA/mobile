package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.repository.custom.PackingProductRepositoryCustom;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.shared.dto.PackingProductInformationDTO;
import com.nextsolutions.dcommerce.shared.dto.PackingSupperSaleDTO;
import com.nextsolutions.dcommerce.shared.projection.IncentivePackingProductProjection;
import com.nextsolutions.dcommerce.shared.projection.PackingProductProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PackingProductRepository extends JpaRepository<PackingProduct, Long>, PackingProductRepositoryCustom {
    List<PackingProduct> findAllByProductIdIn(List<Long> ids);

    List<PackingProduct> findAllByPackingProductIdIsIn(List<Long> ids);

    List<PackingProduct> findAllByProductId(Long id);

    @Query(value = " SELECT " +
            "   pp.packing_product_id as packingId, " +
            "   pp.code as packingCode, " +
            "   pd.product_name as productName, " +
            "   pp.org_price as packingOriginalPrice, " +
            "   pp.price as packingSalePrice " +
            "FROM packing_product pp " +
            "   JOIN product p on pp.product_id = p.product_id " +
            "   JOIN product_description pd on pd.product_id = p.product_id " +
            "WHERE pp.packing_product_id = ?1 AND pd.language_id = ?2", nativeQuery = true)
    IncentivePackingProductProjection getIncentivePackingProductById(Long id, Long languageId);

    @Query("SELECT pp.packingProductId as packingProductId, pp.price as price " +
            "FROM Product p JOIN p.packingProducts pp " +
            "WHERE p.categoryId = ?1 AND p.id in ?2 AND pp.packingTypeId = ?3")
    List<PackingProductProjection> getPackingProductPrices(Long categoryId, List<Long> productIds, Long packingTypeId);

    @Modifying
    @Query("UPDATE PackingProduct pp " +
            "SET " +
            "pp.distributorId = ?2, " +
            "pp.distributorCode = ?3, " +
            "pp.distributorWalletId = ?4, " +
            "pp.isSynchronization = false " +
            "WHERE pp.packingProductId = ?1")
    void updateDistributorById(Long packingProductId, Long distributorId, String distributorCode, Long distributorWalletId);

    @Transactional
    @Modifying
    @Query("UPDATE PackingProduct pp SET pp.isSynchronization = false , pp.status = 0 where pp.packingProductId = ?1")
    void inactive(Long id);

    @Query(value = "select " +
            "pp.PACKING_PRODUCT_ID packingProductId, " +
            "pp.code packingProductCode, " +
            "pp.PACKING_URL packingUrl, " +
            "pt.quantity, " +
            "price.price, " +
            "pp.MARKET_PRICE orgPrice, " +
            "pp.PRODUCT_ID productId " +
            "from " +
            "product p " +
            "join packing_product pp on " +
            "p.PRODUCT_ID = pp.PRODUCT_ID " +
            "join product_description pd on " +
            "pd.PRODUCT_ID = p.PRODUCT_ID " +
            "and pd.LANGUAGE_ID = :langId " +
            "join packing_type pt on " +
            "pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
            "join packing_price price on " +
            "price.packing_product_id = pp.PACKING_PRODUCT_ID " +
            "and date(price.from_date) <= date(current_date()) " +
            "and (date(price.to_date)>= date(current_date()) " +
            "or price.to_date is null) " +
            "and price.price_type = 2 " +
            "where " +
            "p.STATUS = 1 " +
            "and pp.product_id = :productId ", nativeQuery = true)
    List<PackingProductInformationDTO> getListPackingByProductId(@Param("productId") Long productId, @Param("langId") Long langId);


    @Query(value = "select " +
            "pp.PACKING_PRODUCT_ID packingProductId, " +
            "pp.code packingProductCode, " +
            "pp.PACKING_URL packingUrl, " +
            "pt.quantity, " +
            "pp.PRODUCT_ID productId, " +
            "ipdi.discount_type discountType, " +
            "ipdi.sold, " +
            "ipdi.limited_quantity limitedQuantity, " +
            "price.price, " +
            "pp.MARKET_PRICE orgPrice, " +
            "case " +
            "when ipdi.discount_type = 1 then ipdi.discount_percent " +
            "when ipdi.discount_type = 2 then (ipdi.discount_price / price.price)* 100 " +
            "else 0 " +
            "end discountPercent " +
            "from " +
            "product p " +
            "join packing_product pp on " +
            "p.PRODUCT_ID = pp.PRODUCT_ID " +
            "join product_description pd on " +
            "pd.PRODUCT_ID = p.PRODUCT_ID " +
            "and pd.LANGUAGE_ID = :langId " +
            "join packing_type pt on " +
            "pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
            "left join incentive_program_detail_information ipdi on " +
            "ipdi.packing_id = pp.PACKING_PRODUCT_ID " +
            "and ipdi.incentive_program_id in (:lstIncentiveId) " +
            "and ((ipdi.discount_type in (1, 2) " +
            "and ifnull(ipdi.sold, 0) < ipdi.limited_quantity) " +
            "or ( ipdi.discount_type = 3 " +
            "and ifnull(ipdi.sold, 0) < ipdi.limited_packing_quantity )) " +
            "join packing_price price on " +
            "price.packing_product_id = pp.PACKING_PRODUCT_ID " +
            "and date(price.from_date) <= date(current_date()) " +
            "and (date(price.to_date)>= date(current_date()) " +
            "or price.to_date is null) " +
            "and price.price_type = 2 " +
            "where " +
            "p.STATUS = 1 " +
            "and pp.product_id = :productId ", nativeQuery = true)
    List<PackingProductInformationDTO> getListPackingByProductIdAndIncentiveId(@Param("lstIncentiveId") List<Long> lstIncentiveId, @Param("productId") Long productId, @Param("langId") Long langId);
}
