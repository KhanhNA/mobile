package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import com.nextsolutions.dcommerce.ui.model.request.OrderRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderDetailsResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderResponseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderRepositoryCustom  {
    boolean changeStatusOrder(OrderDto orderDto);

    Page<OrderResponseModel> findByParams(OrderRequestModel orderRequestModel, Pageable pageable);

    OrderDetailsResponseModel getOrderDetailsById(Long id);

    boolean cancelOrderByIdWithReason(Long id, String reason);
}
