package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.repository.custom.BannerRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.Banner;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface BannerRepository extends JpaRepository<Banner, Long>, JpaSpecificationExecutor<Banner>, BannerRepositoryCustom {

    @Transactional
    @Modifying
    @Query("UPDATE Banner b SET b.status = 0 WHERE b.id = ?1")
    void inactive(Long id);
}
