package com.nextsolutions.dcommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.AttributeDescription;

public interface AttributeDescriptionRepository extends JpaRepository<AttributeDescription, Long>, JpaSpecificationExecutor<AttributeDescription> {

}