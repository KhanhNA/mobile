package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.repository.custom.CategoryRepositoryCustom;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.shared.dto.CategoryDto;
import com.nextsolutions.dcommerce.shared.dto.CategoryHierarchy;
import com.nextsolutions.dcommerce.shared.projection.CategoryProductProjection;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class CategoryRepositoryCustomImpl implements CategoryRepositoryCustom {
    @Autowired
    CommonService commonService;
    @PersistenceContext
    EntityManager em;

    @Override
    public List<CategoryDto> getCategoryTreeFormat(Long id, Integer langId) {
        StringBuilder sb = new StringBuilder();
        sb.append(
                " select c.category_id id, c.CODE, cd.TITLE, nvl(cd.NAME, c.NAME) name, cd.DESCRIPTION,  nvl(cd.URL_IMAGE, c.URL_IMAGE) image, c.parent_id, null sort_order, null tree_struct, ");
        sb.append("		   exists(select * from category c1 where c1.PARENT_ID = c.CATEGORY_ID) expandable ");
        sb.append(" from category c ");
        sb.append("      left join category_description cd on c.CATEGORY_ID = cd.CATEGORY_ID ");
        sb.append("												and cd.LANGUAGE_ID = :langId ");
        sb.append(" where ((:id is null and c.PARENT_ID is null) or c.PARENT_ID = :id) and c.VISIBLE = 1 ");
        sb.append(" order by c.CATEGORY_ID desc ");

        Query nativeQuery = this.em.createNativeQuery(sb.toString(), CategoryDto.class);
        nativeQuery.setParameter("id", id);
        nativeQuery.setParameter("langId", langId);
        return nativeQuery.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<CategoryHierarchy> getCategoryHierarchyById(Long id) {
        BigInteger parentId = (BigInteger) em.createNativeQuery("SELECT parent_id FROM category where CATEGORY_ID = :id")
                .setParameter("id", id)
                .getSingleResult();

        String condition = parentId == null ? " parent_id IS NULL " : " parent_id = :parentId ";
        String sqlQuery = "WITH RECURSIVE category_paths as " +
                "( SELECT category_id, 1 lvl FROM category WHERE " +
                condition + " AND category_id = :id " +
                "UNION ALL SELECT c.category_id, lvl + 1 " +
                "FROM category c JOIN category_paths cp ON c.parent_id = cp.category_id ) " +
                "SELECT category_id, lvl FROM category_paths";
        Query nativeQuery = em.createNativeQuery(sqlQuery);
        nativeQuery.setParameter("id", id);
        if(parentId != null) {
            nativeQuery.setParameter("parentId", parentId);
        }

        return (List<CategoryHierarchy>) nativeQuery.getResultList().stream()
                .map(result -> {
                    Object[] row = (Object[]) result;
                    return new CategoryHierarchy(((BigInteger) row[0]).longValue(), (Integer) row[1]);
                }).collect(Collectors.toList());
    }

    @Override
    public Page<CategoryDto> getListCategoryByLevel(Pageable pageable, Long level, Long code) throws Exception{
        HashMap<String,Object> params = new HashMap<>();
        StringBuilder selectQuery = new StringBuilder();
        selectQuery.append(" WITH RECURSIVE category_paths AS");
        selectQuery.append(" (SELECT c.code code ,c.url_image image,c.name,category_id, 1 level");
        selectQuery.append(" FROM category c WHERE parent_id IS NULL");
        selectQuery.append(" UNION ALL");
        selectQuery.append("  SELECT c.code code ,c.url_image urlImage,c.name name,c.category_id,(level + 1) level");
        selectQuery.append(" FROM category c JOIN category_paths cp ON c.parent_id = cp.category_id)");
        selectQuery.append(" SELECT level as level,code as code ,image as image ,name as name,category_id as id FROM category_paths where 1=1");

        StringBuilder whereClause = new StringBuilder();
        if(code != null) {
            whereClause.append(" and code = :code");
            params.put("code",code);
        }
        if(level != null) {
            whereClause.append(" and level = :lvl");
            params.put("lvl",level);
        }
        List<CategoryDto> listCategory =   commonService.findAll(pageable,CategoryDto.class,selectQuery.toString() + whereClause.toString(),params)  ;
        StringBuilder selectCount = new StringBuilder("Select count(*) from (");
        int count = commonService.getRowCountV2(selectCount.toString() + selectQuery.toString() + whereClause.toString() + ") count ",params);
        return new PageImpl<>(listCategory, pageable, count);
    }
}
