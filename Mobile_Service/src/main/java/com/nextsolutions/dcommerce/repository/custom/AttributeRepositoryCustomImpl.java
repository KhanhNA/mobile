package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.shared.dto.composition.AttributeComposition;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeResource;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeValueResource;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Service
public class AttributeRepositoryCustomImpl implements AttributeRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    public List<AttributeResource> mapToAttributeResource(Integer languageId, List<AttributeComposition> attributeCompositions) {
        List<AttributeResource> resources = new LinkedList<>();

        Map<Long, AttributeResource> attributeResourceMap = new HashMap<>(attributeCompositions.size());

        attributeCompositions.forEach(compose -> {
            AttributeValueResource value = new AttributeValueResource();
            value.setId(compose.getAttributeValueId());
            value.setCode(compose.getAttributeValueCode());
            value.setName(compose.getAttributeValueName());
            value.setLanguageId(languageId.longValue());

            if(attributeResourceMap.containsKey(compose.getAttributeId())) {
                AttributeResource resource = attributeResourceMap.get(compose.getAttributeId());
                resource.getValues().add(value);
            } else {
                AttributeResource resource = new AttributeResource();
                resource.setId(compose.getAttributeId());
                resource.setCode(compose.getAttributeCode());
                resource.setName(compose.getAttributeName());
                resource.setType(compose.getAttributeType());
                List<AttributeValueResource> values = new LinkedList<>();
                values.add(value);
                resource.setValues(values);
                attributeResourceMap.put(resource.getId(), resource);
                resources.add(resource);
            }
        });
        return resources;
    }

    private String generalSQL() {
        return "select  " +
                "  a.id attributeId,  " +
                "  a.code attributeCode,  " +
                "  ad.name attributeName,  " +
                "  a.type attributeType,  " +
                "  av.id attributeValueId,  " +
                "  av.code attributeValueCode,  " +
                "  avd.name attributeValueName  " +
                "from  " +
                "  attribute a  " +
                "  left join attribute_description ad on a.id = ad.attribute_id  " +
                "  join attribute_value av on a.id = av.attribute_id  " +
                "  left join attribute_value_description avd on av.id = avd.attribute_value_id  " +
                "where  " +
                "  a.status <> 0  " +
                "  and av.status <> 0  " +
                "  and ad.language_id = :languageId  " +
                "  and avd.language_id = :languageId" +
                " and a.status_process = 1 and av.status_process = 1";
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<AttributeResource> findAllByLanguageId(Integer languageId) {
        List<AttributeComposition> attributeCompositions = em.createNativeQuery(generalSQL(), "AttributeComposition")
                .setParameter("languageId", languageId)
                .getResultList();
        return mapToAttributeResource(languageId, attributeCompositions);
    }

    @Override
    public List<AttributeResource> findAllMerchantGroupAttributesAvailable(Integer languageId) {
        List<AttributeComposition> attributeCompositions = em.createNativeQuery(
                generalSQL() + " and (a.type = 1 OR a.type = 3)",
                "AttributeComposition")
                .setParameter("languageId", languageId)
                .getResultList();
        return mapToAttributeResource(languageId, attributeCompositions);
    }

    @Override
    public List<AttributeResource> findAllProductAttributesAvailable(Integer languageId) {
        List<AttributeComposition> attributeCompositions = em.createNativeQuery(
                generalSQL() + " and (a.type = 1 OR a.type = 2)",
                "AttributeComposition")
                .setParameter("languageId", languageId)
                .getResultList();
        return mapToAttributeResource(languageId, attributeCompositions);
    }
}
