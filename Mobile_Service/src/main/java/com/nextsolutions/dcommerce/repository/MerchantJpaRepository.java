package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.repository.custom.MerchantJpaRepositoryCustom;
import com.nextsolutions.dcommerce.shared.dto.SalemanExportDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

public interface MerchantJpaRepository extends JpaRepository<Merchant, Long>, MerchantJpaRepositoryCustom {
    Merchant findByActiveCode(String code);

    Optional<Merchant> findByMobilePhone(String sdt);

    Merchant findByUserName(String userName);



    Optional<Merchant> findByDefaultBankAccountNo(String bankAccoutNo);

    Optional<Merchant> findByIdentityNumber(String identityNumber);

    Optional<Merchant> findByWalletClientId(Long walletClientId);

    Optional<Merchant> findByContractNumber(String contractNumber);




    @Modifying
    @Query("UPDATE Merchant m SET m.status = 2, m.deactiveDate = current_timestamp, m.userModified = ?2 , m.action = 0 WHERE m.merchantId = ?1")
    void deactivate(Long id, String username);

    @Modifying
    @Query("UPDATE Merchant m SET m.status = 1, m.reactiveDate = current_timestamp, m.userModified = ?2 , m.action = 0 WHERE m.merchantId = ?1")
    void reactivate(Long id, String username);

    @Modifying
    @Query("UPDATE Merchant m SET m.status = 1, m.activeDate = current_timestamp, m.userModified = ?2, m.action = 0 WHERE m.merchantId = ?1")
    void activate(Long id, String username);

    @Modifying
    @Query("UPDATE Merchant m SET m.status = -1, m.userModified = ?2 WHERE m.merchantId = ?1")
    void rejectActivate(Long id, String username);

    @Modifying
    @Query("UPDATE Merchant m SET m.action = 0, m.userModified = ?2 WHERE m.merchantId = ?1")
    void rejectAction(Long id, String username);

    @Modifying
    @Query("UPDATE Merchant m SET m.walletClientId = ?2 WHERE m.merchantId = ?1")
    void updateWalletId(Long id, Long walletId);

    @Modifying
    @Transactional
    @Query("UPDATE Merchant m set m.contractNumber = ?2 where m.merchantId = ?1")
    Integer updateContractNumber(Long id, String ContractNumber);
}
