package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.MerchantAddress;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface MerchantAddressRepository extends JpaRepository<MerchantAddress, Long> {
    Page<MerchantAddress> findByMerchantId(Pageable pageable, Long merchantId);

    @Query(value = "select * from merchant_address where is_select = 1 and merchant_id = :merchantId and id <> :id", nativeQuery = true)
    MerchantAddress getMerchantAddressSelected(@Param(value = "merchantId") Long merchantId, @Param(value = "id") Long id);

    @Query(value = "select * from merchant_address where is_select = 1 and merchant_id = :merchantId", nativeQuery = true)
    MerchantAddress getMerchantAddressSelected(@Param(value = "merchantId") Long merchantId);

    MerchantAddress findByMerchantIdAndAddress(Long merchantId, String address);

    @Query(value = "select count(*) from merchant_address where merchant_id = :merchantId", nativeQuery = true)
    long countMerchantIdBy(@Param(value = "merchantId") Long merchantId);
}
