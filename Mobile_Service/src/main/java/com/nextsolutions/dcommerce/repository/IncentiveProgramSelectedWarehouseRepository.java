package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.IncentiveProgramSelectedWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface IncentiveProgramSelectedWarehouseRepository extends JpaRepository<IncentiveProgramSelectedWarehouse, Long>, JpaSpecificationExecutor<IncentiveProgramSelectedWarehouse> {
    void deleteByIncentiveProgramId(Long id);

    List<IncentiveProgramSelectedWarehouse> findAllByIncentiveProgramId(Long incentiveProgramId);
}
