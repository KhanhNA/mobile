package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.AndroidCrashLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AndroidCrashLogRepository extends JpaRepository<AndroidCrashLog, Long> {
}
