package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.model.IncentiveProgram;
import com.nextsolutions.dcommerce.repository.custom.IncentiveProgramRepositoryCustom;
import com.nextsolutions.dcommerce.repository.custom.dto.IncentiveDateRange;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.ui.model.incentive.IncentiveProgramQuery;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

@Repository
public class IncentiveProgramRepositoryCustomImpl implements IncentiveProgramRepositoryCustom {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private CommonService commonService;

    @Override
    public Page<IncentiveProgram> findByQuery(IncentiveProgramQuery query, Pageable pageable) {
        IncentiveProgramQueryHelper incentiveProgramQueryHelper = new IncentiveProgramQueryHelper(query, pageable);
        return incentiveProgramQueryHelper.getResult();
    }

    private class IncentiveProgramQueryHelper {
        private final IncentiveProgramQuery incentiveProgramQuery;
        private final Long languageId;
        private final Pageable pageable;
        private final String fromClause;
        private final String whereClause;

        public IncentiveProgramQueryHelper(IncentiveProgramQuery incentiveProgramQuery, Pageable pageable) {
            this.incentiveProgramQuery = incentiveProgramQuery;
            this.pageable = pageable;
            this.languageId = LanguageUtils.getCurrentLanguageId();
            this.fromClause = fromClause();
            this.whereClause = whereClause();
        }

        private String fromClause() {
            return " FROM incentive_program_v2 ip " +
                    "   JOIN incentive_program_description_v2 ipd ON ip.id = ipd.incentive_program_id ";
        }

        private String whereClause() {
            String whereClause = " WHERE ipd.language_id = :languageId ";
            if (Objects.nonNull(incentiveProgramQuery))
                whereClause += appendCode()
                        + appendName()
                        + appendDescription()
                        + appendType()
                        + appendStatus()
                        + appendFromDate()
                        + appendToDate();

            return whereClause;
        }

        private String appendToDate() {
            return Objects.nonNull(incentiveProgramQuery.getToDate()) ? " AND date(ip.to_date) <= date(:toDate)" : "";
        }

        private String appendFromDate() {
            return Objects.nonNull(incentiveProgramQuery.getFromDate()) ? " AND date(ip.from_date) >= date(:fromDate)" : "";
        }

        private String appendStatus() {
            Integer status = incentiveProgramQuery.getStatus();
            return Objects.nonNull(status) ? " AND ip.status  " + (status == -1 ? " IS NULL " : " = :status") : "";
        }

        private String appendType() {
            return Objects.nonNull(incentiveProgramQuery.getType()) ? " AND ip.type = :type" : "";
        }

        private String appendDescription() {
            return StringUtils.notEmpty(incentiveProgramQuery.getDescription()) ? " AND lower(ipd.description) like concat('%', :description ,'%')" : "";
        }

        private String appendName() {
            return StringUtils.notEmpty(incentiveProgramQuery.getName()) ? " AND lower(ipd.name) like concat('%', :name ,'%')" : "";
        }

        private String appendCode() {
            return StringUtils.notEmpty(incentiveProgramQuery.getCode()) ? " AND lower(ip.code) like concat('%', :code ,'%')" : "";
        }

        @SuppressWarnings("unchecked")
        public Page<IncentiveProgram> getResult() {
            Query nativeQuery = em.createNativeQuery(generateNativeQuery(), "GetIncentiveProgram");
            bindParameters(nativeQuery);
            Query nativeQueryCount = em.createNativeQuery(generateNativeQueryCount());
            bindParameters(nativeQueryCount);
            if (pageable.isPaged()) {
                nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                nativeQuery.setMaxResults(pageable.getPageSize());
            }
            List<IncentiveProgram> resultList = nativeQuery.getResultList();
            BigInteger totalCount = (BigInteger) nativeQueryCount.getSingleResult();
            return new PageImpl<>(resultList, pageable, totalCount.longValue());
        }

        private String generateNativeQuery() {
            return "SELECT " +
                    " ip.id id, " +
                    " ip.code code, " +
                    " ipd.name name, " +
                    " ipd.description description, " +
                    " ip.type type, " +
                    " ip.status status, " +
                    " ip.from_date fromDate, " +
                    " ip.to_date toDate, " +
                    " ip.created_user createdUser, " +
                    " ip.created_date createdDate, " +
                    " ip.latest_modified_user latestModifiedUser, " +
                    " ip.latest_modified_date latestModifiedDate, " +
                    " ( SELECT " +
                    "       IF(ip.type = 3," +
                    "          (SELECT EXISTS (SELECT 1 FROM incentive_program_merchant_group ipmg WHERE ipmg.incentive_program_id = ip.id)), " +
                    "          (SELECT EXISTS (SELECT 1 FROM incentive_program_detail_information ipdi WHERE ipdi.incentive_program_id = ip.id)) " +
                    "           AND (SELECT EXISTS (SELECT 1 FROM incentive_program_merchant_group ipmg WHERE ipmg.incentive_program_id = ip.id)) " +
                    "       )" +
                    "  ) canActivate " // TODO: If the performance slow, try to add LIMIT 1 IN SELECT 1 statement (haint)
                    + fromClause + whereClause + SpringUtils.getOrderBy(pageable);
        }

        private String generateNativeQueryCount() {
            return "SELECT count(1) " + fromClause + whereClause;
        }

        private void bindParameters(Query nativeQuery) {
            if (StringUtils.notEmpty(incentiveProgramQuery.getCode()))
                nativeQuery.setParameter("code", incentiveProgramQuery.getCode());

            if (StringUtils.notEmpty(incentiveProgramQuery.getName()))
                nativeQuery.setParameter("name", incentiveProgramQuery.getName());

            if (StringUtils.notEmpty(incentiveProgramQuery.getDescription()))
                nativeQuery.setParameter("description", incentiveProgramQuery.getDescription());

            if (Objects.nonNull(incentiveProgramQuery.getType()))
                nativeQuery.setParameter("type", incentiveProgramQuery.getType());

            if (Objects.nonNull(incentiveProgramQuery.getStatus()) && incentiveProgramQuery.getStatus() != -1)
                nativeQuery.setParameter("status", incentiveProgramQuery.getStatus());

            if (Objects.nonNull(incentiveProgramQuery.getFromDate()))
                nativeQuery.setParameter("fromDate", incentiveProgramQuery.getFromDate());

            if (Objects.nonNull(incentiveProgramQuery.getToDate()))
                nativeQuery.setParameter("toDate", incentiveProgramQuery.getToDate());

            if (Objects.nonNull(languageId))
                nativeQuery.setParameter("languageId", languageId);
        }
    }

    @Override
    public boolean isExistsPackingIdInOtherIncentivePrograms(Long id, Long packingId) {
        ExistingIncentiveProgramRepositoryHelper existingIncentiveProgramRepositoryHelper = new ExistingIncentiveProgramRepositoryHelper(id);
        return existingIncentiveProgramRepositoryHelper.existsPackingProduct(packingId);
    }

    @Override
    public boolean isExistsPackingProductsIncentivePrograms(Long id) throws Exception {
        //Get packing incentive
        List<Long> lstPackingId = commonService.findAll(null, Long.class, getSqlPackingByIncentiveId(), id);
        ExistingIncentiveProgramRepositoryHelper
                existingIncentiveProgramRepositoryHelper = new ExistingIncentiveProgramRepositoryHelper(id);
        return existingIncentiveProgramRepositoryHelper.existsPackingProducts(lstPackingId);
    }

    private class ExistingIncentiveProgramRepositoryHelper {
        private final IncentiveDateRange incentiveDateRange;
        private Long id;

        private ExistingIncentiveProgramRepositoryHelper(Long id) {
            this.id = id;
            this.incentiveDateRange = (IncentiveDateRange) em.createQuery("SELECT " +
                    "new com.nextsolutions.dcommerce.repository.custom.dto.IncentiveDateRange(ip.id, ip.fromDate, ip.toDate) " +
                    "FROM IncentiveProgram ip WHERE ip.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
        }

        private boolean existsPackingProduct(Long packingId) {
            return (Integer) em.createNativeQuery(
                    "SELECT EXISTS (SELECT 1 " +
                            " FROM " +
                            "    incentive_program_detail_information ipd " +
                            "    JOIN incentive_program_v2 ip ON ipd.incentive_program_id = ip.id " +
                            " WHERE " +
                            "    ipd.packing_id = :packingId " +
                            "    AND IF(:toDate IS NULL, " +
                            "        (IF(ip.to_date IS NULL, TRUE, date(ip.to_date) >= date(:fromDate))), " +
                            "        (date(ip.from_date) <= date(:toDate) " +
                            "         AND (date(ip.to_date) >= date(:fromDate) OR ip.TO_DATE IS NULL" +
                            "        ))) " +
                            "      AND ip.id <> :incentiveProgramId " +
                            "    AND ip.status = 1 )")
                    .setParameter("incentiveProgramId", id)
                    .setParameter("packingId", packingId)
                    .setParameter("fromDate", incentiveDateRange.getFromDate())
                    .setParameter("toDate", incentiveDateRange.getToDate())
                    .getSingleResult() > 0;
        }

        private boolean existsPackingProducts(List<Long> packingIds) {
            return (Integer) em.createNativeQuery(
                    "SELECT EXISTS (SELECT 1 " +
                            " FROM " +
                            "    incentive_program_detail_information ipd " +
                            "    JOIN incentive_program_v2 ip ON ipd.incentive_program_id = ip.id " +
                            " WHERE " +
                            "    ipd.packing_id IN ( :packingIds ) " +
                            "    AND IF(:toDate IS NULL, " +
                            "        (IF(ip.to_date IS NULL, TRUE, date(ip.to_date) >= date(:fromDate))), " +
                            "        (date(ip.from_date) <= date(:toDate) " +
                            "         AND (date(ip.to_date) >= date(:fromDate) OR ip.TO_DATE IS NULL" +
                            "        ))) " +
                            "      AND ip.id <> :incentiveProgramId " +
                            "    AND ip.status = 1)")
                    .setParameter("packingIds", packingIds)
                    .setParameter("incentiveProgramId", id)
                    .setParameter("fromDate", incentiveDateRange.getFromDate())
                    .setParameter("toDate", incentiveDateRange.getToDate())
                    .getSingleResult() > 0;
        }

    }

    private String getSqlPackingByIncentiveId() {
        return "select packing_id from incentive_program_detail_information  where incentive_program_id = :incentiveProgramId";
    }
}
