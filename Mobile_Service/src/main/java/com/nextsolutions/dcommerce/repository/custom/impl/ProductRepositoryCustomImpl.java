package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.model.Product;
import com.nextsolutions.dcommerce.model.ProductLinks;
import com.nextsolutions.dcommerce.repository.CategoryRepository;
import com.nextsolutions.dcommerce.repository.custom.ProductRepositoryCustom;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.shared.constant.PriceType;
import com.nextsolutions.dcommerce.shared.dto.CategoryHierarchy;
import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import com.nextsolutions.dcommerce.shared.dto.ProductDetailDto;
import com.nextsolutions.dcommerce.shared.dto.product.PackingPriceDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductQueryDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductTableDto;
import com.nextsolutions.dcommerce.ui.model.response.ProductFormResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductPackingResponseModel;
import com.nextsolutions.dcommerce.shared.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.isEmpty;

@Repository
@RequiredArgsConstructor
public class ProductRepositoryCustomImpl implements ProductRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    private final ProductMapper productMapper;

    private final CategoryRepository categoryRepository;

    private final CommonService commonService;

    @Override
    @Transactional
    public ProductDetailDto getProductDetail(long id, Long langId) throws Exception {
        String selectClause = " SELECT  " +
                "  pd.product_id id, " +
                "  pd.product_name product_Name," +
                "  p.product_image url_Image, " +
                "  pd.DESCRIPTION des_Product, " +
                "  p.review_avg review_Avg, " +
                "  p.review_count review_Count, " +
                "  p.product_ship product_Ship, " +
                "  ifnull(temp.promotion_sale_price,pp.price) product_Price," +
                "  pt.quantity product_Quantity ," +
                "  mfd.name manufacturer_Name, " +
                "  mfd.MANUFACTURER_ID manufacturer_Id, " +
                "  seller.name sellerName, " +
                "  pp.code packingProductCode";

        String sqlString = selectClause +
                " FROM   " +
                "  product p " +
                "  LEFT JOIN distributor seller on p.DISTRIBUTOR_ID = seller.ID " +
                "  JOIN product_description pd ON pd.product_id = p.product_id " +
                "  JOIN packing_product pp ON pp.product_id = p.product_id   " +
                "  LEFT JOIN packing_product temp on temp.PACKING_PRODUCT_ID =pp.PACKING_PRODUCT_ID    " +
                "  AND DATE(current_date()) >= DATE(temp.from_date) and (DATE(temp.to_date)>= DATE(current_date()) or temp.TO_DATE is null)   " +
                "  LEFT JOIN manufacturer_description mfd on mfd.MANUFACTURER_ID = p.MANUFACTURER_ID " +
                "  LEFT JOIN packing_type pt ON pt.packing_type_id = pp.packing_type_id " +
                "  where p.product_id =:id " +
                "  and pd.LANGUAGE_ID =:langId ";
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("langId", langId);
        List<ProductDetailDto> dtos = commonService.findAll(null, ProductDetailDto.class, sqlString, params);
        if (dtos != null && dtos.size() > 0) {
            ProductDetailDto productDetailDto = dtos.get(0);
            String sqlImage = new StringBuilder()
                    .append("SELECT pi.PRODUCT_IMAGE FROM product p JOIN product_image pi ON pi.PRODUCT_ID = p.PRODUCT_ID ")
                    .append(" JOIN product_image_description pid ON pid.PRODUCT_IMAGE_ID = pi.PRODUCT_IMAGE_ID ")
                    .append("  where pi.PRODUCT_ID = :id ")
                    .append(" and pid.LANGUAGE_ID = :langId").toString();
            Query getImageQuery = em.createNativeQuery(sqlImage);
            getImageQuery.setParameter("id", id);
            getImageQuery.setParameter("langId", langId);
            productDetailDto.setImageList((ArrayList<String>) getImageQuery.getResultList());

            String packingLst = new StringBuilder()
                    .append("SELECT pp.PRODUCT_ID productId,  " +
                            "       pp.PACKING_URL packingUrl,  " +
                            "       pp.PACKING_PRODUCT_ID packingProductId,  " +
                            "       ifnull(temp.promotion_sale_price,pp.PRICE) price,  " +
                            "       pp.DISCOUNT_PERCENT discountPercent,  " +
                            "       pp.code packingProductCode,  " +
                            "       pp.ORG_PRICE orgPrice,  " +
                            "        pp.MARKET_PRICE marketPrice,  " +
                            "       pt.QUANTITY quantity  " +
                            "     FROM packing_product pp  " +
                            "    JOIN packing_type pt on pp.PACKING_TYPE_ID = pt.PACKING_TYPE_ID  " +
                            " LEFT JOIN packing_product temp on temp.PACKING_PRODUCT_ID = pp.PACKING_PRODUCT_ID " +
                            " AND DATE(current_date()) >= DATE(temp.from_date) and (DATE(temp.to_date)>= DATE(current_date()) or temp.TO_DATE is null) " +
                            "   WHERE pp.PRODUCT_ID = :productId")
                    .toString();
            Query getPackingQuery = em.createNativeQuery(packingLst, "PackingProductDTOMappingAll");
            getPackingQuery.setParameter("productId", productDetailDto.getId());
            List<PackingProductDTO> result = getPackingQuery.getResultList();

            productDetailDto.setPackingProductList(result);
            return productDetailDto;
        } else {
            return null;
        }

    }

    @Override
    public Page<ProductTableDto> findByQuery(ProductQueryDto query, Pageable pageable) {
        ProductTableQueryHelper productTableQueryHelper = new ProductTableQueryHelper(query, pageable).executeQuery();
        return new PageImpl<>(productTableQueryHelper.getContent(), pageable, productTableQueryHelper.getTotalElements());
    }

    @Override
    public ProductFormResponseModel findByIdAndCategoryIdAndLanguageId(Long id, Long categoryId, Long languageId) {
        ProductFormResponseModel productFormResponseModel;
        try {
            TypedQuery<Product> query = em.createQuery(createQuery(), Product.class)
                    .setParameter("id", id)
                    .setParameter("categoryId", categoryId)
                    .setParameter("languageId", languageId);
            Product product = query.getSingleResult();
            productFormResponseModel = this.productMapper.toProductFormResponse(product);
        } catch (NoResultException e) {
            TypedQuery<Product> query = em.createQuery(
                    "SELECT p FROM Product p WHERE p.id = :id AND p.category.id = :categoryId", Product.class)
                    .setParameter("id", id)
                    .setParameter("categoryId", categoryId);
            productFormResponseModel = this.productMapper.toProductFormResponse(query.getSingleResult());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        productFormResponseModel.getPackingProducts()
                .forEach(packingProductDto -> {
                    // Get price
                    try {
                        packingProductDto.setPackingSaleOutPrice(getPackingPrice(packingProductDto.getId(), PriceType.SALE_OUT.value()));
                        packingProductDto.setPackingSaleInPrice(getPackingPrice(packingProductDto.getId(), PriceType.SALE_INT.value()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
        return productFormResponseModel;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<ProductPackingResponseModel> findByCategoryIdAndPackingTypeId(Long categoryId, Long packingTypeId, Pageable pageable) {
        String selectNativeQueryClause = "select p.product_id id, " +
                "       pp.packing_url imageUrl, " +
                "       p.product_code code, " +
                "       p.product_name name, " +
                "       p.sku sku, " +
                "       pp.code packingCode, " +
                "       pp.name packingName, " +
                "       pp.org_price originalPrice, " +
                "       pp.price salePrice, " +
                "       pp.promotion_sale_price discountSalePrice, " +
                "       pp.discount_percent discountPercent, " +
                "       pp.from_date fromDate, " +
                "       pp.to_date toDate ";
        String fromClause = " from product p inner join packing_product pp on p.product_id = pp.product_id ";
        String whereClause = " where p.category_id = :categoryId and pp.packing_type_id = :packingTypeId ";

        Query nativeQuery = em.createNativeQuery(selectNativeQueryClause + fromClause + whereClause, "ProductPackingResponseModelMapping");
        Query nativeQueryCount = em.createNativeQuery("select count(*) " + fromClause + whereClause);
        nativeQuery.setParameter("categoryId", categoryId);
        nativeQuery.setParameter("packingTypeId", packingTypeId);
        nativeQueryCount.setParameter("categoryId", categoryId);
        nativeQueryCount.setParameter("packingTypeId", packingTypeId);

        nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        nativeQuery.setMaxResults(pageable.getPageSize());

        List<ProductPackingResponseModel> resultList = nativeQuery.getResultList();
        BigInteger singleResult = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, singleResult.longValue());
    }

    @Override
    public PackingPriceDto getPackingPrice(Long packingProductId, Integer type) throws Exception {
        String sql = " SELECT * " +
                " FROM " +
                "    packing_price" +
                " WHERE " +
                " packing_product_id = :packingProductId" +
                " AND DATE(from_date) <= DATE(CURRENT_DATE())" +
                " AND price_type = :type " +
                " AND (DATE(to_date) >= DATE(CURRENT_DATE())" +
                " OR to_date IS NULL)";
        return commonService.findOne(PackingPriceDto.class, sql, packingProductId, type);
    }


    private String createQuery() {
        return "SELECT " +
                "   p " +
                "FROM " +
                "   Product p " +
                "   JOIN p.manufacturer m " +
                "   JOIN m.descriptions md " +
                "   JOIN p.category c " +
                "   JOIN c.descriptions cd " +
                "WHERE " +
                "   p.id = :id AND " +
                "   p.category.id = :categoryId AND " +
                "   md.languageId = :languageId AND " +
                "   cd.languageId = :languageId";
    }

    private class ProductTableQueryHelper {
        private ProductQueryDto query;
        private Pageable pageable;
        private List<ProductTableDto> productTableDtos;
        private Long totalProducts;
        private TypedQuery<Product> productTypedQuery;
        private Query productQueryCount;
        private List<Long> categoryIds;

        ProductTableQueryHelper(ProductQueryDto query, Pageable pageable) {
            this.query = query;
            this.pageable = pageable;
            this.categoryIds = new ArrayList<>();
        }

        List<ProductTableDto> getContent() {
            return productTableDtos;
        }

        Long getTotalElements() {
            return totalProducts;
        }

        ProductTableQueryHelper executeQuery() {
            initializeCategoryIds();
            initializeQueries();
            getResults();
            return this;
        }

        private void initializeCategoryIds() {
            categoryIds = categoryRepository.getCategoryHierarchyById(query.getCategoryId())
                    .stream().map(CategoryHierarchy::getId).collect(Collectors.toList());
            if (categoryIds.isEmpty())
                categoryIds.add(query.getCategoryId());
        }

        void initializeQueries() {
            productTypedQuery = em.createQuery(generateJPQL("new Product(p.id, p.code, pd.productName, p.imageDefaultUrl, p.sku, p.width, p.height, p.weight, p.length, p.category, p.importPrice, p.vat) ") + generateOrderByClause(), Product.class);
            setParameters(productTypedQuery);
            productTypedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
            productTypedQuery.setMaxResults(pageable.getPageSize());
            productQueryCount = em.createQuery(generateJPQL("count(p)"));
            setParameters(productQueryCount);
        }

        private void getResults() {
            productTableDtos = productMapper.toProductTableDto(productTypedQuery.getResultList());
            totalProducts = (Long) productQueryCount.getSingleResult();
        }

        private String generateOrderByClause() {
            return " ORDER BY " + pageable.getSort().stream()
                    .map(sort -> "p." + sort.getProperty() + " " + sort.getDirection().name())
                    .collect(Collectors.joining(", "));
        }

        private String generateJPQL(String selectClause) {
            StringBuilder sb = new StringBuilder();
            sb.append(" SELECT ");
            sb.append(selectClause);
            sb.append(" FROM ");
            sb.append(" Product p JOIN p.productDescriptions pd ");
            sb.append(" WHERE ");
            sb.append(" p.category.id in (:categoryIds) AND p.status = 1 ");
            if (!isEmpty(query.getCode())) {
                sb.append(" AND (p.code like :code) ");
            }
            if (!isEmpty(query.getName())) {
                sb.append(" AND (pd.productName like :name) ");
            }
            if (!isEmpty(query.getSku())) {
                sb.append(" AND (p.sku like :sku) ");
            }
            if (!isEmpty(query.getLanguageId())) {
                sb.append(" AND (pd.languageId = :languageId) ");
            } else {
                sb.append(" AND (pd.languageId = 1) ");
            }
            return sb.toString();
        }

        private void setParameters(Query query) {
            query.setParameter("categoryIds", this.categoryIds);
            if (!isEmpty(this.query.getCode())) {
                query.setParameter("code", "%" + this.query.getCode() + "%");
            }
            if (!isEmpty(this.query.getName())) {
                query.setParameter("name", "%" + this.query.getName() + "%");
            }
            if (!isEmpty(this.query.getSku())) {
                query.setParameter("sku", "%" + this.query.getSku() + "%");
            }
            if (!isEmpty(this.query.getLanguageId())) {
                query.setParameter("languageId", this.query.getLanguageId());
            }
        }
    }
}
