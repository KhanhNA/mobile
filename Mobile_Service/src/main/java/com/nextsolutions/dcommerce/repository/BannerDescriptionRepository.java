package com.nextsolutions.dcommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.BannerDescription;

public interface BannerDescriptionRepository extends JpaRepository<BannerDescription, Long>, JpaSpecificationExecutor<BannerDescription> {
}
