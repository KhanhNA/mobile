package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.IncentiveProgramGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface IncentiveProgramGroupRepository extends JpaRepository<IncentiveProgramGroup, Long>, JpaSpecificationExecutor<IncentiveProgramGroup> {
}
