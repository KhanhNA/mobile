package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.shared.dto.ManufacturerSearchProjection;
import com.nextsolutions.dcommerce.repository.custom.ManufacturerRepositoryCustom;
import com.nextsolutions.dcommerce.shared.projection.ManufacturerProjection;
import com.nextsolutions.dcommerce.ui.model.response.ManufacturerLookupResponseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.Manufacturer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long>, JpaSpecificationExecutor<Manufacturer>, ManufacturerRepositoryCustom {

    @Query("SELECT " +
            "new com.nextsolutions.dcommerce.ui.model.response.ManufacturerLookupResponseModel(m.id, m.code, md.name, md.languageId) " +
            "FROM Manufacturer m JOIN m.descriptions md " +
            "WHERE md.languageId = :languageId")
    List<ManufacturerLookupResponseModel> findAllByLanguageId(@Param("languageId") Long languageId);

    @Query("SELECT m.id as id, m.code as code, md.name as name FROM Manufacturer m JOIN m.descriptions md WHERE (LOWER(m.code) LIKE LOWER(CONCAT('%', ?1 ,'%')) OR LOWER(md.name) LIKE LOWER(CONCAT('%', ?1 ,'%'))) AND md.languageId = ?2")
    Page<ManufacturerSearchProjection> searchManufacturer(String keyword, Long languageId, Pageable pageable);

    Optional<Manufacturer> findByCode(String code);

    Optional<Manufacturer> findByTel(String tel);

    Optional<Manufacturer> findByTaxCode(String taxCode);

    Optional<Manufacturer> findByEmail(String email);

    @Query("SELECT new Manufacturer(m.id, m.code, md.name) FROM Manufacturer m JOIN m.descriptions md WHERE m.id = ?1 AND md.languageId = ?2")
    ManufacturerProjection getByIdAndLanguageId(Long id, Long languageId);

    @Transactional
    @Modifying
    @Query("UPDATE Manufacturer a SET a.isActive = 0, a.isSynchronization = false WHERE a.id = ?1")
    void inactive(Long id);

}
