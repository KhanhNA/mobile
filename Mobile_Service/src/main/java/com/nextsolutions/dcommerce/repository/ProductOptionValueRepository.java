package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.ProductOptionValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;

public interface ProductOptionValueRepository extends JpaRepository<ProductOptionValue, Long> {
    Optional<ProductOptionValue> findByIdAndOptionId(Long id, Long optionId);

    @Transactional
    @Modifying()
    @Query("DELETE FROM ProductOptionValue pov WHERE pov.id = ?1 AND pov.productOptionId = ?2")
    void deleteByIdAndProductOptionId(Long id, Long productOptionId);
}
