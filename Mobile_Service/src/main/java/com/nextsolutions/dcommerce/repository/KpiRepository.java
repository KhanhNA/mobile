package com.nextsolutions.dcommerce.repository;


import com.nextsolutions.dcommerce.model.Kpi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KpiRepository extends JpaRepository<Kpi, Long> {
    List<Kpi> findByType(String type);
}
