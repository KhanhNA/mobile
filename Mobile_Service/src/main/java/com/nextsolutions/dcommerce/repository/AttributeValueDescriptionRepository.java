package com.nextsolutions.dcommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.AttributeValueDescription;

public interface AttributeValueDescriptionRepository extends JpaRepository<AttributeValueDescription, Long>, JpaSpecificationExecutor<AttributeValueDescription> {

}