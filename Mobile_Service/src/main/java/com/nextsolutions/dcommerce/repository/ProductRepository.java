package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.Product;
import com.nextsolutions.dcommerce.repository.custom.ProductManagementRepositoryCustom;
import com.nextsolutions.dcommerce.repository.custom.ProductRepositoryCustom;
import com.nextsolutions.dcommerce.shared.dto.PackingSupperSaleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long>,
        ProductRepositoryCustom, ProductManagementRepositoryCustom {
    boolean existsByCode(String code);

    Optional<Product> findByCode(String code);

    @Modifying
    @Query("UPDATE Product p SET p.distributorId = ?2, p.distributorCode = ?3, p.isSynchronization = false WHERE p.id = ?1")
    void updateDistributorIdById(Long id, Long distributorId, String distributorCode);

    @Query("SELECT pp FROM PackingProduct pp WHERE pp.productId = :id")
    Page<PackingProduct> findAllPackingProducts(@Param("id") Long id, Pageable pageable);

    @Transactional
    @Modifying
    @Query("UPDATE Product p SET p.isSynchronization = false, p.status = 0 where p.id = ?1 ")
    void inactive(Long id);

    @Query(value = "SELECT " +
            "    pp.PRODUCT_ID productId,pp.PACKING_PRODUCT_ID packingProductId, " +
            "    pd.PRODUCT_NAME productName,pp.PACKING_URL packingUrl,pt.QUANTITY, " +
            "    ipdi.discount_price discountPrice," +
            "    ipdi.discount_type discountType,ipdi.sold,ipdi.limited_quantity limitedQuantity,price.price ," +
            "    CASE " +
            "    when ipdi.discount_type = 3 then pp.MARKET_PRICE " +
            "    else price.PRICE " +
            "    END orgPrice, " +
            "    CASE" +
            "    when ipdi.discount_type =1 then ipdi.discount_percent " +
            "    when ipdi.discount_type =2 then (ipdi.discount_price/ price.price)*100  " +
            "    ELSE 0 " +
            "    END discountPercent  " +
            " FROM " +
            "  incentive_program_detail_information ipdi  " +
            "  JOIN packing_product pp ON ipdi.packing_id = pp.PACKING_PRODUCT_ID " +
            "  JOIN product p on p.PRODUCT_ID = pp.PRODUCT_ID and p.status =1 " +
            "  JOIN product_description pd ON pp.PRODUCT_ID = pd.PRODUCT_ID " +
            "  JOIN packing_type pt ON pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
            "  JOIN packing_price price on price.packing_product_id = pp.packing_product_id " +
            "  WHERE " +
            "  ipdi.incentive_program_id in (:lstIncentiveId) " +
            "  AND pd.LANGUAGE_ID = :langId" +
            " and date(current_date()) >= date(price.from_date)  " +
            " and (date(price.to_date)>= date(current_date())  " +
            " or price.to_date is null)" +
            " and price.price_type = 2 " +
            " and ((ipdi.discount_type in (1,2) and ifnull(ipdi.sold,0) < ipdi.limited_quantity) " +
            " or ( ipdi.discount_type = 3 and ifnull(ipdi.sold,0) < ipdi.limited_packing_quantity )) ", countProjection = "count(pp.PRODUCT_ID)", nativeQuery = true)
    Page<PackingSupperSaleDTO> getProductSupperSale(Pageable pageable, @Param("lstIncentiveId") List<Long> lstIncentiveId, @Param("langId") Long langId);


}
