package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductManagementRepositoryCustom {
    ProductLinks getProductLinks(Long id, Long languageId);

    void updateProductLinks(Long id, ProductLinks productLinks);

    Page<ManufacturerLink> getProductLinksManufacturers(String keyword, Long languageId, Pageable pageable);

    Page<DistributorLink> getProductLinksDistributors(String keyword, Long languageId, Pageable pageable);

    List<ProductOptions> getProductOptions(Long id);

    void updateProductOptions(Long id, List<ProductOptions> options);

    Page<PackingProduct> getPackingProducts(Long id, Pageable pageable);
}
