package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.shared.dto.OverviewStatistics;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.BigInteger;

@Repository
public class OverviewRepository {

    @PersistenceContext
    private EntityManager em;

    public OverviewStatistics getOverviewStatistics() {
        try {
            OverviewStatistics overviewStatistics = new OverviewStatistics();

            Object totalOrdersAndSales = getTotalOrdersAndSales();
            Object[] ordersAndSales = (Object[]) totalOrdersAndSales;
            long totalOrders = ordersAndSales[0] != null ? ((BigInteger) ordersAndSales[0]).longValue() : 0;
            BigDecimal totalSales = ordersAndSales[1] != null ? ((BigDecimal) ordersAndSales[1]) : BigDecimal.ZERO;
            overviewStatistics.setTotalOrders(totalOrders);
            overviewStatistics.setTotalSales(totalSales);

            Object totalOrdersTodayAndAmountToday = getTotalOrdersTodayAndAmountToday();
            Object totalOrdersAndAmountYesterday = getTotalOrdersAndAmountYesterday();

            Object[] ordersTodayAndAmountToday = (Object[]) totalOrdersTodayAndAmountToday;
            Object[] ordersAndAmountYesterday = (Object[]) totalOrdersAndAmountYesterday;

            long totalOrdersToday = ordersTodayAndAmountToday[0] != null ? ((BigInteger) ordersTodayAndAmountToday[0]).longValue() : 0;
            long totalOrdersYesterday = ordersAndAmountYesterday[0] != null ? ((BigInteger) ordersAndAmountYesterday[0]).longValue() : 0;
            BigDecimal totalAmountToday = ordersTodayAndAmountToday[1] != null ? (BigDecimal) ordersTodayAndAmountToday[1] : BigDecimal.ZERO;
            BigDecimal totalAmountYesterday = ordersAndAmountYesterday[1] != null ? (BigDecimal) ordersAndAmountYesterday[1] : BigDecimal.ZERO;

            double totalOrderRate = 0;
            double totalSalesRate = 0;
            if (totalOrdersYesterday != 0) {
                totalOrderRate = -((totalOrdersYesterday * 1.0 - totalOrdersToday) / totalOrdersYesterday * 100);
            } else {
                totalOrderRate = -((totalOrdersYesterday * 1.0 - totalOrdersToday) / 1 * 100);
            }
            if (totalAmountYesterday.doubleValue() != 0) {
                totalSalesRate = -((totalAmountYesterday.doubleValue() - totalAmountToday.doubleValue()) / totalAmountYesterday.doubleValue() * 100);
            } else {
                totalSalesRate = -((totalAmountYesterday.doubleValue() - totalAmountToday.doubleValue()) / 1 * 100);
            }
            overviewStatistics.setTotalOrderRate(totalOrderRate);
            overviewStatistics.setTotalSalesRate(totalSalesRate);

            long totalActiveMerchants = ((BigInteger) getTotalMerchantsActive()).longValue();
            long totalActiveMerchantsYesterday = ((BigInteger) getTotalMerchantsActiveYesterday()).longValue();
            long totalActiveMerchantsToday = ((BigInteger) getTotalMerchantsActiveToday()).longValue();

            double totalActiveMerchantsRate = 0;
            if (totalActiveMerchantsYesterday != 0) {
                totalActiveMerchantsRate = -((totalActiveMerchantsYesterday * 1.0 - totalActiveMerchantsToday)
                        / totalActiveMerchantsYesterday * 100);
            } else {
                totalActiveMerchantsRate = -((totalActiveMerchantsYesterday * 1.0 - totalActiveMerchantsToday)
                        / 1 * 100);
            }

            overviewStatistics.setTotalActiveMerchants(totalActiveMerchants);
            overviewStatistics.setTotalActiveMerchantsRate(totalActiveMerchantsRate);

            int totalMerchantsActiveInMonth = getTotalMerchantsActiveInMonth();
            int totalMerchantsActiveInLastMonth = getTotalMerchantsActiveInLastMonth();

            double totalActiveMerchantsInMonthRate = 0;
            if(totalMerchantsActiveInLastMonth != 0) {
                totalActiveMerchantsInMonthRate = -((totalMerchantsActiveInLastMonth * 1.0 - totalMerchantsActiveInMonth)
                        / totalMerchantsActiveInLastMonth * 100);
            } else {
                totalActiveMerchantsInMonthRate = -((totalMerchantsActiveInLastMonth * 1.0 - totalMerchantsActiveInMonth)
                        / 1 * 100);
            }
            overviewStatistics.setTotalActiveMerchantsInMonth(totalMerchantsActiveInMonth);
            overviewStatistics.setTotalActiveMerchantsInMonthRate(totalActiveMerchantsInMonthRate);
            return overviewStatistics;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private int getTotalMerchantsActiveInMonth() {
        BigInteger result = (BigInteger) em.createNativeQuery("select count(distinct m.merchant_id) " +
                "from merchant m " +
                "         join orders o on m.merchant_id = o.merchant_id " +
                "where m.merchant_type_id = 1 " +
                "  and date(order_date) between date_format(now() ,'%Y-%m-01') and now()")
                .getSingleResult();
        return result.intValue();
    }

    private int getTotalMerchantsActiveInLastMonth() {
        BigInteger result = (BigInteger) em.createNativeQuery("select count(distinct m.merchant_id) " +
                "from merchant m " +
                "  join orders o on m.merchant_id = o.merchant_id " +
                "where m.merchant_type_id = 1 " +
                "  and date(order_date) between (date_format(now(), '%Y-%m-01') - interval 1 month) and (date_format(now(), '%Y-%m-01') - interval 1 day)")
                .getSingleResult();
        return result.intValue();
    }

    private Object getTotalMerchantsActive() {
        return em.createNativeQuery("select count(1) totalMerchants " +
                "from merchant " +
                "where merchant_type_id = 1 and status = 1")
                .getSingleResult();
    }

    private Object getTotalMerchantsActiveYesterday() {
        return em.createNativeQuery("select count(1) totalMerchants " +
                "from merchant " +
                "where merchant_type_id = 1 and status = 1 and date(active_date) = curdate() - interval 1 day")
                .getSingleResult();
    }

    private Object getTotalMerchantsActiveToday() {
        return em.createNativeQuery("select count(1) today " +
                "from merchant " +
                "where merchant_type_id = 1 " +
                "  and status = 1 " +
                "  and date(active_date) = curdate()")
                .getSingleResult();
    }

    private Object getTotalOrdersAndAmountYesterday() {
        return em.createNativeQuery("select " +
                "  count(1) totalOrdersYesterday, sum(amount) totalAmountYesterday " +
                "from orders o " +
                "  join merchant m on o.merchant_id = m.merchant_id " +
                "where m.merchant_type_id = 1 " +
                "  and o.order_status <> -1 " +
                "  and date(o.order_date) = curdate() - interval 1 day")
                .getSingleResult();
    }

    private Object getTotalOrdersAndSales() {
        return em.createNativeQuery("select count(1) totalOrders, sum(amount) totalSales " +
                "from orders o " +
                "  join merchant m on o.merchant_id = m.merchant_id " +
                "where m.merchant_type_id = 1 " +
                "  and o.order_status <> -1")
                .getSingleResult();
    }

    private Object getTotalOrdersTodayAndAmountToday() {
        return em.createNativeQuery("select " +
                "  count(1) totalOrdersToday, sum(amount) totalAmountToday " +
                "from orders o " +
                "  join merchant m on o.merchant_id = m.merchant_id " +
                "where m.merchant_type_id = 1 " +
                "  and o.order_status <> -1 " +
                "  and date(o.order_date) = curdate()")
                .getSingleResult();
    }
}
