package com.nextsolutions.dcommerce.repository.custom.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nextsolutions.dcommerce.model.CategoryLink;
import com.nextsolutions.dcommerce.model.DistributorLink;
import com.nextsolutions.dcommerce.model.ManufacturerLink;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.ProductLinks;
import com.nextsolutions.dcommerce.model.ProductOptions;
import com.nextsolutions.dcommerce.model.RelatedProduct;
import com.nextsolutions.dcommerce.repository.custom.ProductManagementRepositoryCustom;

@Repository
public class ProductManagementRepositoryCustomImpl implements ProductManagementRepositoryCustom {
	private static final Logger log = LogManager.getLogger(ProductManagementRepositoryCustomImpl.class);
	@PersistenceContext
	private EntityManager em;

	@Override
	public ProductLinks getProductLinks(Long id, Long languageId) {
		try {
			ProductLinks productLinks = getProductLinksById(id);
			productLinks.setCategory(getCategoryLink(productLinks.getCategoryId(), languageId));
			productLinks.setDistributor(getDistributor(productLinks.getDistributorId()));
			productLinks.setManufacturer(getManufacturerLink(productLinks.getManufacturerId(), languageId));
			productLinks.setRelatedProductIds(getRelatedProductIds(id));
			productLinks.setRelatedProducts(getRelatedProducts(id, languageId));
			return productLinks;
		} catch (Exception e) {
			return null;
		}
	}

	private ProductLinks getProductLinksById(Long id) {
		return em.createQuery("SELECT p FROM ProductLinks p WHERE p.id = :id", ProductLinks.class)
				.setParameter("id", id).getSingleResult();
	}

	private List<Long> getRelatedProductIds(Long id) {
		return em.createQuery("SELECT rp.relatedProductId FROM RelatedProduct rp WHERE rp.productId = : id", Long.class)
				.setParameter("id", id).getResultList();
	}

	private List<RelatedProduct> getRelatedProducts(Long productId, Long languageId) {
		return em
				.createQuery(
						"SELECT new RelatedProduct(p.id, p.code, pd.productName) "
								+ "FROM RelatedProduct rp JOIN rp.product p JOIN p.productDescriptions pd "
								+ "WHERE pd.languageId = :languageId AND rp.productId = :productId",
						RelatedProduct.class)
				.setParameter("languageId", languageId).setParameter("productId", productId).getResultList();
	}

	private ManufacturerLink getManufacturerLink(Long manufacturerId, Long languageId) {
		if (manufacturerId == null)
			return null;
		try {
			return em
					.createQuery(
							"SELECT new ManufacturerLink(m.id, m.code, md.name) "
									+ "FROM ManufacturerDescription md JOIN md.manufacturer m "
									+ "WHERE m.id = :manufacturerId AND md.languageId = :languageId",
							ManufacturerLink.class)
					.setParameter("languageId", languageId).setParameter("manufacturerId", manufacturerId)
					.getSingleResult();
		} catch (Exception e) {
			log.error(e.getMessage());
			return em
					.createQuery("SELECT new ManufacturerLink(c.id, c.code, '') "
							+ "FROM ManufacturerLink c WHERE c.id = :id", ManufacturerLink.class)
					.setParameter("id", manufacturerId).getSingleResult();
		}
	}

	private CategoryLink getCategoryLink(Long categoryId, Long languageId) {
		if (categoryId == null)
			return null;
		try {
			return em
					.createQuery("SELECT new CategoryLink(c.id, c.code, cd.name) "
							+ "FROM CategoryDescription cd JOIN cd.category c "
							+ "WHERE c.id = :categoryId AND cd.languageId = :languageId", CategoryLink.class)
					.setParameter("languageId", languageId).setParameter("categoryId", categoryId).getSingleResult();
		} catch (Exception e) {
			log.error(e.getMessage());
			return em.createQuery("SELECT new CategoryLink(c.id, c.code, '') " + "FROM CategoryLink c WHERE c.id = :id",
					CategoryLink.class).setParameter("id", categoryId).getSingleResult();
		}
	}

	private DistributorLink getDistributor(Long distributorId) {
		if (distributorId == null)
			return null;
		return em.createQuery("SELECT d FROM DistributorLink d WHERE d.id = :id", DistributorLink.class)
				.setParameter("id", distributorId).getSingleResult();
	}

	@Override
	@Transactional
	public void updateProductLinks(Long productId, ProductLinks productLinks) {
		em.createQuery("DELETE FROM RelatedProduct rp WHERE rp.productId = :productId")
				.setParameter("productId", productId).executeUpdate();

		em.createQuery("UPDATE ProductLinks p " + "SET p.categoryId = :categoryId, "
				+ "p.distributorId = :distributorId, " + "p.manufacturerId = :manufacturerId " + "WHERE p.id = :id")
				.setParameter("id", productId).setParameter("categoryId", productLinks.getCategoryId())
				.setParameter("distributorId", productLinks.getDistributorId())
				.setParameter("manufacturerId", productLinks.getManufacturerId()).executeUpdate();

		List<Long> relatedProductIds = productLinks.getRelatedProductIds();
		if (relatedProductIds != null && !relatedProductIds.isEmpty()) {
			String values = relatedProductIds.stream()
					.map(relatedProductId -> String.format("(%d, %d)", productId, relatedProductId))
					.collect(Collectors.joining(", "));
			em.createNativeQuery("INSERT INTO related_product(PRODUCT_ID, RELATED_PRODUCT_ID) VALUES " + values)
					.executeUpdate();
		}
	}

	@Override
	public Page<ManufacturerLink> getProductLinksManufacturers(String keyword, Long languageId, Pageable pageable) {
		String sqlString = " FROM ManufacturerDescription md JOIN md.manufacturer m "
				+ " WHERE (lower(m.code) like concat('%', lower(:keyword) ,'%') OR lower(md.name) like concat('%', lower(:keyword) ,'%')) "
				+ "AND md.languageId = :languageId";

		List<ManufacturerLink> resultList = em
				.createQuery("SELECT new ManufacturerLink(m.id, m.code, md.name) " + sqlString, ManufacturerLink.class)
				.setParameter("languageId", languageId).setParameter("keyword", keyword)
				.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()).setMaxResults(pageable.getPageSize())
				.getResultList();

		Long total = (Long) em.createQuery("SELECT count(md) " + sqlString).setParameter("languageId", languageId)
				.setParameter("keyword", keyword).setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
				.setMaxResults(pageable.getPageSize()).getSingleResult();

		return new PageImpl<>(resultList, pageable, total);
	}

	@Override
	public Page<DistributorLink> getProductLinksDistributors(String keyword, Long languageId, Pageable pageable) {
		String sqlString = " FROM DistributorLink d  WHERE (lower(d.code) like concat('%', lower(:keyword) ,'%') OR lower(d.name) like concat('%', lower(:keyword) ,'%')) ";

		List<DistributorLink> resultList = em
				.createQuery("SELECT new DistributorLink(d.id, d.code, d.name, d.walletId) " + sqlString,
						DistributorLink.class)
				.setParameter("keyword", keyword).setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
				.setMaxResults(pageable.getPageSize()).getResultList();

		Long total = (Long) em.createQuery("SELECT count(d) " + sqlString).setParameter("keyword", keyword)
				.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()).setMaxResults(pageable.getPageSize())
				.getSingleResult();

		return new PageImpl<>(resultList, pageable, total);
	}

	@Override
	public List<ProductOptions> getProductOptions(Long productId) {
		return em.createQuery("SELECT po FROM ProductOptions po WHERE po.productId = :productId", ProductOptions.class)
				.setParameter("productId", productId).getResultList();
	}

	@Override
	@Transactional
	public void updateProductOptions(Long productId, List<ProductOptions> options) {
		em.createQuery("DELETE FROM ProductOptions po WHERE po.productId = :productId")
				.setParameter("productId", productId).executeUpdate();

		String values = options.stream().map(productOptions -> String.format("(%s,%s,%s)", productId,
				productOptions.getAttributeId(), productOptions.getAttributeValueId()))
				.collect(Collectors.joining(", "));

		em.createNativeQuery(String
				.format("INSERT INTO product_options(product_id, attribute_id, attribute_value_id) VALUES %s", values))
				.executeUpdate();
	}

	@Override
	public Page<PackingProduct> getPackingProducts(Long id, Pageable pageable) {
		List<PackingProduct> resultList = em
				.createQuery("SELECT pp FROM PackingProduct pp WHERE pp.productId = :productId", PackingProduct.class)
				.setParameter("productId", id).getResultList();
		Long total = (Long) em.createQuery("SELECT count(pp) FROM PackingProduct pp WHERE pp.productId = :productId")
				.setParameter("productId", id).getSingleResult();

		return new PageImpl<>(resultList, pageable, total);
	}

}
