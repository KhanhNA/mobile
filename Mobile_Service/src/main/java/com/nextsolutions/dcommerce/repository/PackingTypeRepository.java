package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.PackingType;
import com.nextsolutions.dcommerce.ui.model.response.PackingTypeLookupResponseModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PackingTypeRepository extends JpaRepository<PackingType, Long> {

    @Query("SELECT new com.nextsolutions.dcommerce.ui.model.response.PackingTypeLookupResponseModel" +
            "(p.packingTypeId, p.code, p.quantity) FROM PackingType p")
    List<PackingTypeLookupResponseModel> findAllPackingTypeLookup();

    PackingType findByCode(String code);
}
