package com.nextsolutions.dcommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.ProductImageDescription;

public interface ProductImageDescriptionRepository extends JpaRepository<ProductImageDescription, Long>, JpaSpecificationExecutor<ProductImageDescription> {

}
