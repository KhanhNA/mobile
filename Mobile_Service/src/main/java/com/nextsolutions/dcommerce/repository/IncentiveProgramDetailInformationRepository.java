package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.IncentiveProgramDetailInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IncentiveProgramDetailInformationRepository extends JpaRepository<IncentiveProgramDetailInformation, Long> {
    IncentiveProgramDetailInformation findByIncentiveProgramId(Long id);
    List<IncentiveProgramDetailInformation> findAllByIncentiveProgramId(Long id);

    @Modifying
    @Query("DELETE FROM IncentiveProgramDetailInformation ipdi WHERE ipdi.incentiveProgramId = ?1")
    void deleteByIncentiveProgramId(Long id);
}
