package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.OrderTemplate;
import com.nextsolutions.dcommerce.shared.dto.OrderTemplateDetailDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderTemplateRepository extends JpaRepository<OrderTemplate, Long> {
    @Query(value = "select " +
            "o.ORDER_ID orderId, " +
            "o.ORDER_DATE orderDate, " +
            "o.AMOUNT, " +
            "op.order_packing_id orderPackingId, " +
            "op.PACKING_PRODUCT_ID packingProductId, " +
            "op.ORDER_QUANTITY orderQuantity , " +
            "pp.PACKING_URL packingUrl, " +
            "op.price_sale price, " +
            "pp.MARKET_PRICE orgPrice, " +
            "pd.product_name productName, " +
            "ot.name orderName, " +
            "ot.id orderTemplateId " +
            "from " +
            "order_template ot " +
            "join orders o on o.ORDER_ID = ot.order_id and ot.MERCHANT_ID = :merchantId " +
            "join order_packing_v2 op on " +
            "o.ORDER_ID = op.ORDER_ID and op.is_default = 1 " +
            "join packing_product pp on " +
            "pp.PACKING_PRODUCT_ID = op.PACKING_PRODUCT_ID " +
            "join product_description pd on " +
            "pd.PRODUCT_ID = op.PRODUCT_ID and pd.language_id = :langId " +
            "order by ot.create_date  desc", countProjection = "o.order_id", nativeQuery = true)
    Page<OrderTemplateDetailDTO> getOrderTemplateDetailByMerchantId(@Param("merchantId") Long merchantId, @Param("langId") Long langId, Pageable pageable);

}
