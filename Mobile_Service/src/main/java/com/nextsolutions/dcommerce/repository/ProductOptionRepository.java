package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.ProductOption;
import com.nextsolutions.dcommerce.repository.custom.ProductOptionRepositoryCustom;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionValueTableResponseModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductOptionRepository extends JpaRepository<ProductOption, Long>, ProductOptionRepositoryCustom {

    @Query("SELECT new com.nextsolutions.dcommerce.ui.model.response.ProductOptionValueTableResponseModel" +
            "(pov.id, po.id, po.code, pov.code, povd.name) " +
            "FROM ProductOption po JOIN  po.values pov JOIN pov.descriptions povd " +
            "WHERE po.id = ?1 and povd.languageId = ?2")
    List<ProductOptionValueTableResponseModel> findAllProductOptionValues(Long productOptionId, Long languageId);
}
