package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.ui.model.request.MerchantGroupQueryRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupMerchantResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MerchantGroupRepositoryCustom {
    Page<MerchantGroupResource> findByQuery(MerchantGroupQueryRequestModel query, Pageable pageable);

    List<MerchantGroupMerchantResource> findAllMerchantGroupMerchantById(Long id);

    Page<MerchantGroupResource> findAllByNativeSql(String keyword, Pageable pageable, Integer languageId);

    @SuppressWarnings("unchecked")
    List<MerchantGroupResource> findAllByNativeSql(String keyword, Integer languageId, Long merchantId);
}
