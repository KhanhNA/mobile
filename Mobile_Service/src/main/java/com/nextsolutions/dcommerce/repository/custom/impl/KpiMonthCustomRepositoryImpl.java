package com.nextsolutions.dcommerce.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.nextsolutions.dcommerce.shared.dto.KpiMonthDto;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nextsolutions.dcommerce.model.charts.KpiMonthChart;
import com.nextsolutions.dcommerce.repository.custom.KpiMonthCustomRepository;

@Repository
@Transactional(readOnly = true)
public class KpiMonthCustomRepositoryImpl implements KpiMonthCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<KpiMonthDto> findAll(KpiMonthDto dto) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT km.id, km.kpi_id, k.name kpi_name," + "            km.month, km.merchant_id,"
				+ "            km.plan, km.done, km.bonus " + " FROM kpi_month km "
				+ "      JOIN kpi k ON km.kpi_id = k.id " + " where first_day(sysdate()) < km.month "
				+ "       and km.month < last_day(sysdate()) " + " and km.merchant_id = :merchantId ");
		Query query = this.entityManager.createNativeQuery(sql.toString(), KpiMonthDto.class);
		query.setParameter("merchantId", dto.getMerchantId());
		return query.getResultList();
	}

	@Override
	public List<KpiMonthChart> findAllInMonthByKpiType(String kpiType) {
		StringBuilder sb = new StringBuilder();
		sb.append(" select (km.merchant_id + km.kpi_id) id, date(km.month) month, ");
		sb.append("        km.merchant_id, nvl(m.merchant_name, m.merchant_code) merchant_name, ");
		sb.append("        km.kpi_id, kpi.code kpi_code, ");
		sb.append("	       max(km.plan) plan, sum(km.done) done, sum(km.bonus) bonus ");
		sb.append("	from kpi_month km ");
		sb.append("      join kpi on kpi.id = km.kpi_id ");
		sb.append("      join merchant m on m.merchant_id = km.merchant_id ");
		sb.append("	where first_day(sysdate()) <= km.month ");
		sb.append("		  and km.month <= last_day(sysdate()) ");
		sb.append("       and kpi.from_date <= last_day(sysdate()) ");
		sb.append("       and (kpi.to_date is null or kpi.to_date >= first_day(sysdate())) ");
		sb.append("       and kpi.type = :kpiType ");
		sb.append(" group by date(km.month), km.merchant_id, km.kpi_id ");
		sb.append(" order by bonus desc, done desc, m.active_date ");
		sb.append(" limit 3 ");

		Query nativeQuery = this.entityManager.createNativeQuery(sb.toString(), KpiMonthChart.class);
		nativeQuery.setParameter("kpiType", kpiType);

		return nativeQuery.getResultList();
	}

	@Override
	public List<KpiMonthChart> findAllInYearByKpiType(String kpiType) {
		StringBuilder sb = new StringBuilder();
		sb.append(" select km.merchant_id id, date(km.month) month, ");
		sb.append("        km.merchant_id, nvl(m.merchant_name, m.merchant_code) merchant_name, ");
		sb.append("        km.kpi_id, kpi.code kpi_code, ");
		sb.append("	       max(km.plan) plan, sum(km.done) done, sum(km.bonus) bonus ");
		sb.append("	from kpi_month km ");
		sb.append("      join kpi on kpi.id = km.kpi_id ");
		sb.append("      join merchant m on m.merchant_id = km.merchant_id ");
		sb.append("	where MAKEDATE(EXTRACT(YEAR FROM CURDATE()),1) <= km.month ");
		sb.append("		  and km.month <= last_day(sysdate()) ");
		sb.append("       and kpi.from_date <= last_day(sysdate()) ");
		sb.append("       and (kpi.to_date is null or kpi.to_date >= MAKEDATE(EXTRACT(YEAR FROM CURDATE()),1)) ");
		sb.append("       and kpi.type = :kpiType ");
		sb.append(" group by km.merchant_id ");
		sb.append(" order by bonus desc, done desc, m.active_date ");
		sb.append(" limit 3 ");

		Query nativeQuery = this.entityManager.createNativeQuery(sb.toString(), KpiMonthChart.class);
		nativeQuery.setParameter("kpiType", kpiType);

		return nativeQuery.getResultList();
	}

}
