package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.KpiMonth;
import org.springframework.data.jpa.repository.JpaRepository;

import com.nextsolutions.dcommerce.repository.custom.KpiMonthCustomRepository;

import java.util.List;

public interface KpiMonthRepository extends JpaRepository<KpiMonth, Long>, KpiMonthCustomRepository {
    List<KpiMonth> findAllByKpiId(List<Long> singletonList);
}
