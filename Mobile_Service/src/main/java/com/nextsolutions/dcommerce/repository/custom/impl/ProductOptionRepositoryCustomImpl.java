package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.ui.model.request.ProductOptionQueryRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionTableResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionValueResponseModel;
import com.nextsolutions.dcommerce.repository.custom.ProductOptionRepositoryCustom;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionResponseModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.*;

public class ProductOptionRepositoryCustomImpl implements ProductOptionRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    @SuppressWarnings("unchecked")
    public Collection<ProductOptionResponseModel> findAllByLanguageId(Long languageId) {
        List<Object[]> resultList = em.createNativeQuery(generateNativeQuery())
                .setParameter("languageId", languageId)
                .getResultList();

        Map<Long, ProductOptionResponseModel> map = new HashMap<>();

        for (Object[] a : resultList) {
            Long productOptionId = a[0] != null ? ((BigInteger) a[0]).longValue() : null;
            String productOptionCode = (String) a[1];
            String productOptionName = (String) a[2];
            Integer productOptionLanguageId = a[3] != null ? (Integer) a[3] : null;
            Long productOptionValueId = a[4] != null ? ((BigInteger) a[4]).longValue() : null;
            String productOptionValueCode = a[5] != null ? (String) a[5] : null;
            String productOptionValueName = a[6] != null ? (String) a[6] : null;

            ProductOptionValueResponseModel productOptionValue = new ProductOptionValueResponseModel();
            productOptionValue.setId(productOptionValueId);
            productOptionValue.setCode(productOptionValueCode);
            productOptionValue.setName(productOptionValueName);
            productOptionValue.setLanguageId(productOptionLanguageId);
            productOptionValue.setProductOptionId(productOptionId);

            if (map.containsKey(productOptionId)) {
                ProductOptionResponseModel productOptionResponseModel = map.get(productOptionId);
                List<ProductOptionValueResponseModel> values = productOptionResponseModel.getValues();
                if (values == null) {
                    values = new ArrayList<>();
                }
                values.add(productOptionValue);
                productOptionResponseModel.setValues(values);
                map.put(productOptionId, productOptionResponseModel);
            } else {
                ProductOptionResponseModel productOptionResponseModel = new ProductOptionResponseModel();
                productOptionResponseModel.setId(productOptionId);
                productOptionResponseModel.setCode(productOptionCode);
                productOptionResponseModel.setName(productOptionName);
                productOptionResponseModel.setLanguageId(productOptionLanguageId);
                List<ProductOptionValueResponseModel> values = productOptionResponseModel.getValues();
                if (values == null) {
                    values = new ArrayList<>();
                }
                values.add(productOptionValue);
                productOptionResponseModel.setValues(values);
                map.put(productOptionId, productOptionResponseModel);
            }
        }
        return map.values();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ProductOptionTableResponseModel> findAllByQueryAndLanguage(ProductOptionQueryRequestModel queryRequest, Long languageId) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT ");
        queryBuilder.append("new com.nextsolutions.dcommerce.ui.model.response.ProductOptionTableResponseModel(po.id, po.code, pod.name)");
        queryBuilder.append("FROM ProductOption po JOIN po.descriptions pod ");
        queryBuilder.append("WHERE ");
        queryBuilder.append("pod.languageId = :languageId ");
        return em.createQuery(queryBuilder.toString())
                .setParameter("languageId", languageId)
                .getResultList();
    }

    private String generateNativeQuery() {
        return "SELECT " +
                "  po.PRODUCT_OPTION_ID productOptionId, " +
                "  po.PRODUCT_OPTION_CODE productOptionCode, " +
                "  pod.NAME productOptionName, " +
                "  pod.LANGUAGE_ID productOptionLanguageId, " +
                "  pov.PRODUCT_OPTION_VALUE_ID productOptionValueId, " +
                "  pov.PRODUCT_OPTION_VALUE_CODE productOptionValueCode, " +
                "  povd.NAME productOptionValueName " +
                "FROM " +
                "  product_option po " +
                "  JOIN product_option_description pod ON po.PRODUCT_OPTION_ID = pod.PRODUCT_OPTION_ID " +
                "  JOIN product_option_value pov ON pov.PRODUCT_OPTION_ID = po.PRODUCT_OPTION_ID " +
                "  JOIN product_option_value_description povd ON povd.PRODUCT_OPTION_VALUE_ID = pov.PRODUCT_OPTION_VALUE_ID " +
                "WHERE " +
                "  pod.LANGUAGE_ID = :languageId " +
                "  AND povd.LANGUAGE_ID = :languageId";
    }


}
