package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.repository.custom.AttributeRepositoryCustom;
import com.nextsolutions.dcommerce.shared.dto.AttributeDto;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.Attribute;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface AttributeRepository extends JpaRepository<Attribute, Long>, JpaSpecificationExecutor<Attribute>, AttributeRepositoryCustom {

    @Query("SELECT a FROM Attribute a WHERE a.id = ?1 AND a.status <> 0")
    Optional<Attribute> findByIdAndActivated(Long id);

    @Query("SELECT new com.nextsolutions.dcommerce.shared.dto.AttributeDto(a.id, a.code, ad.name, a.type, a.request, a.statusProcess, a.value, 1) FROM Attribute a JOIN a.descriptions ad WHERE a.status <> 0 AND ad.languageId = ?1 order by a.code")
    List<AttributeDto> getAttributeTable(Integer languageId);

    @Transactional
    @Modifying
    @Query("UPDATE Attribute a SET a.status = 0 WHERE a.id = ?1")
    void inactive(Long id);
}
