package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.dto.SalemanExportDTO;
import com.nextsolutions.dcommerce.ui.model.request.HunterRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.MerchantCrudRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.MerchantRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.HunterResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;

public interface MerchantJpaRepositoryCustom {
    Page<MerchantResource> findByQuery(MerchantRequestModel query, Pageable pageable);
    Page<MerchantCrudResource> findByQuery(MerchantCrudRequestModel query, Pageable pageable);
    Page<HunterResource> findByQuery(HunterRequestModel hunterQuery, Pageable pageable);
    Collection<SalemanExportDTO> findSalemanExportData(HunterRequestModel hunterQuery, Pageable pageable);
    Page<MerchantCrudResource> findMerchants(Long hunterId, MerchantDto merchantDto, Pageable pageable);
}
