package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.model.Banner;
import com.nextsolutions.dcommerce.model.BannerDetails;
import com.nextsolutions.dcommerce.repository.custom.BannerRepositoryCustom;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerResource;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.nextsolutions.dcommerce.utils.StringUtils.notEmpty;

@Repository
@RequiredArgsConstructor
public class BannerRepositoryCustomImpl implements BannerRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public Page<BannerResource> findByQuery(BannerResource resource, Pageable pageable) {
        String selectClause = "SELECT b.id id, b.code code, bd.name name, b.from_date fromDate, b.to_date toDate, bd.language_id languageId ";
        String selectCountClause = "SELECT count(1) ";
        String fromClause = " FROM banner b JOIN banner_description bd ON b.id = bd.banner_id ";

        StringBuilder whereClause = new StringBuilder();
        whereClause.append(" WHERE (b.status <> 0)  ");
        whereClause.append(" AND bd.language_id = :languageId ");

        String code = resource.getCode();
        String name = resource.getName();
        LocalDateTime fromDate = resource.getFromDate();
        LocalDateTime toDate = resource.getToDate();
        Integer languageId = resource.getLanguageId();

        if (notEmpty(code)) {
            whereClause.append(" AND lower(b.code) like lower(concat('%',:code,'%')) ");
        }

        if (notEmpty(name)) {
            whereClause.append(" AND lower(bd.name) like lower(concat('%',:name,'%')) ");
        }

        if (fromDate != null && toDate != null) {
            whereClause.append(" AND b.from_date >= date(:fromDate) AND b.to_date <= date(:toDate) ");
        } else {
            if (fromDate != null) {
                whereClause.append(" AND date(b.from_date) >= date(:fromDate) ");
            }
            if (toDate != null) {
                whereClause.append(" AND date(b.to_date) <= date(:toDate) ");
            }
        }

        String nativeQueryString = selectClause + fromClause + whereClause.toString() + SpringUtils.getOrderBy(pageable);
        Query nativeQuery = em.createNativeQuery(nativeQueryString, "BannerResourceMapping");

        String nativeQueryCountString = selectCountClause + fromClause + whereClause.toString();
        Query nativeQueryCount = em.createNativeQuery(nativeQueryCountString);

        if (notEmpty(code)) {
            nativeQuery.setParameter("code", code);
            nativeQueryCount.setParameter("code", code);
        }

        if (notEmpty(name)) {
            nativeQuery.setParameter("name", name);
            nativeQueryCount.setParameter("name", name);
        }

        if (fromDate != null && toDate != null) {
            nativeQuery.setParameter("fromDate", fromDate);
            nativeQuery.setParameter("toDate", toDate);
            nativeQueryCount.setParameter("fromDate", fromDate);
            nativeQueryCount.setParameter("toDate", toDate);
        } else {
            if (fromDate != null) {
                nativeQuery.setParameter("fromDate", fromDate);
                nativeQueryCount.setParameter("fromDate", fromDate);
            }
            if (toDate != null) {
                nativeQuery.setParameter("toDate", toDate);
                nativeQueryCount.setParameter("toDate", toDate);
            }
        }

        if (languageId == null) {
            nativeQuery.setParameter("languageId", 1);
            nativeQueryCount.setParameter("languageId", 1);
        } else {
            nativeQuery.setParameter("languageId", languageId);
            nativeQueryCount.setParameter("languageId", languageId);
        }

        nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        nativeQuery.setMaxResults(pageable.getPageSize());

        List<BannerResource> bannerResources = nativeQuery.getResultList();
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(bannerResources, pageable, total.longValue());
    }

    @Override
    public Optional<Banner> findByIdAndMustActive(Long id) {
        Banner banner;
        try {
            banner = em.createQuery("SELECT b FROM Banner b WHERE b.id = :id AND b.status <> 0", Banner.class)
                    .setParameter("id", id)
                    .getSingleResult();
            List<BannerDetails> bannerDetails = em.createQuery("SELECT bd FROM BannerDetails bd WHERE bd.status = true AND bd.bannerId = :bannerId", BannerDetails.class)
                    .setParameter("bannerId", id)
                    .getResultList();
            banner.setDetails(bannerDetails);
        } catch (Exception e) {
            banner = null;
        }
        return Optional.ofNullable(banner);
    }
}
