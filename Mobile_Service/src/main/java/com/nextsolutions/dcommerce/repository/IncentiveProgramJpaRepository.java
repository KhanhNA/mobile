package com.nextsolutions.dcommerce.repository;


import com.nextsolutions.dcommerce.model.incentive.Incentive;
import org.springframework.data.jpa.repository.JpaRepository;


public interface IncentiveProgramJpaRepository extends JpaRepository<Incentive, Long> {
}
