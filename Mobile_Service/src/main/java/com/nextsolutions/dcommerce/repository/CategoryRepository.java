package com.nextsolutions.dcommerce.repository;

import java.util.List;

import com.nextsolutions.dcommerce.repository.custom.CategoryRepositoryCustom;
import com.nextsolutions.dcommerce.shared.projection.CategoryProductProjection;
import com.nextsolutions.dcommerce.shared.projection.CategoryProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nextsolutions.dcommerce.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>, CategoryRepositoryCustom {
	@Query(value = " select c.CATEGORY_ID, c.CODE, nvl(cd.NAME, c.NAME) name, c.sort_order, nvl(cd.URL_IMAGE, c.URL_IMAGE) URL_IMAGE, c.parent_id, c.VISIBLE,c.is_synchronization  "
			+ " from category c left join category_description cd on c.CATEGORY_ID = cd.CATEGORY_ID and cd.LANGUAGE_ID = :langId "
			+ " where c.PARENT_ID is null and c.VISIBLE = 1 order by c.CATEGORY_ID desc", nativeQuery = true)
	List<Category> getAllRoot(@Param(value = "langId") int langId);

	@Query("SELECT new Category(c.id, c.code, c.name) FROM Category c JOIN c.descriptions cd WHERE c.id = ?1 AND cd.languageId = ?2")
    CategoryProjection getByIdAndLanguageId(Long id, Long languageId);

	@Query(value = "SELECT " +
			"c.category_id as id, " +
			"c.code as code, " +
			"cd.name as name, " +
			"cd.language_id as languageId, " +
			"(WITH RECURSIVE category_paths AS " +
			"  (SELECT category_id, 1 lvl " +
			"   FROM category " +
			"   WHERE parent_id IS NULL " +
			"   UNION ALL SELECT c.category_id, " +
			"                    (lvl + 1) lvl " +
			"   FROM category c " +
			"   JOIN category_paths cp ON c.parent_id = cp.category_id) " +
			"SELECT lvl " +
			"FROM category_paths where category_id = ?1) level " +
			"FROM category c JOIN category_description cd ON c.category_id = cd.category_id " +
			"WHERE c.category_id = ?1 AND cd.language_id = ?2", nativeQuery = true)
	CategoryProductProjection getByCategoryIdAndLanguageId(Long id, Long languageId);


}
