package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.loyalty.Loyalty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoyaltyRepository extends JpaRepository<Loyalty, Long> {
}
