package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.IncentiveProgramDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IncentiveProgramDescriptionRepository extends JpaRepository<IncentiveProgramDescription, Long>, JpaSpecificationExecutor<IncentiveProgramDescription> {

}
