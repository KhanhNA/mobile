package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.ManufacturerDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ManufacturerDescriptionRepository extends JpaRepository<ManufacturerDescription, Long>, JpaSpecificationExecutor<ManufacturerDescription> {

}
