package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.repository.custom.MerchantJpaRepositoryCustom;
import com.nextsolutions.dcommerce.service.impl.CommonServiceImpl;
import com.nextsolutions.dcommerce.service.impl.HunterServiceImpl;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.dto.SalemanExportDTO;
import com.nextsolutions.dcommerce.ui.model.request.HunterRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.MerchantCrudRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.MerchantRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.HunterResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantResource;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.StringUtils;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;

@Repository
@RequiredArgsConstructor
public class MerchantJpaRepositoryCustomImpl implements MerchantJpaRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private CommonServiceImpl commonService;
    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public Page<MerchantResource> findByQuery(MerchantRequestModel merchantRequestModel, Pageable pageable) {
        StringBuilder sb = new StringBuilder();
        String nativeQuerySelectClause = " SELECT " +
                "m.merchant_id id, m.merchant_code code, m.user_name username, m.full_name fullName, " +
                "m.mobile_phone phoneNumber, m.address address, m.create_date registerDate ";

        if (merchantRequestModel.isExists() && Objects.nonNull(merchantRequestModel.getMerchantGroupId())) {
            sb.append(" FROM merchant m join merchant_group_merchant mgm ON m.merchant_id = mgm.merchant_id ");
            sb.append(" WHERE m.merchant_type_id in (1, 2) AND m.status = 1 ");
            sb.append(" AND mgm.merchant_group_id = ").append(merchantRequestModel.getMerchantGroupId());
        } else {
            sb.append(" FROM merchant m WHERE m.merchant_type_id in (1, 2) AND m.status = 1 ");
        }

        MerchantQueryHelper helper = MerchantQueryHelper
                .builder()
                .code(merchantRequestModel.getCode())
                .username(merchantRequestModel.getUsername())
                .fullName(merchantRequestModel.getFullName())
                .phoneNumber(merchantRequestModel.getPhoneNumber())
                .address(merchantRequestModel.getAddress())
                .registerDate(merchantRequestModel.getRegisterDate())
                .build();

        appendCondition(sb, helper);

        String fromClause = sb.toString();

        String nativeQueryString = nativeQuerySelectClause + fromClause + SpringUtils.getOrderBy(pageable);
        Query nativeQuery = this.createMerchantNativeQuery(helper, nativeQueryString, "MerchantResource");
        Query nativeQueryCount = this.createMerchantNativeQueryCount(helper, "SELECT count(1) " + fromClause);

        nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        nativeQuery.setMaxResults(pageable.getPageSize());

        List<MerchantResource> resultList = nativeQuery.getResultList();
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<MerchantCrudResource> findByQuery(MerchantCrudRequestModel merchantCrudQuery, Pageable pageable) {
        StringBuilder sb = new StringBuilder();
        String nativeQuerySelectClause = " SELECT " +
                "m.merchant_id id, " +
                "m.merchant_code code, " +
                "m.user_name username, " +
                "m.full_name fullName, " +
                "m.mobile_phone phoneNumber, " +
                "m.address address, " +
                "m.create_date registerDate, " +
                "m.wallet_client_id walletId, " +
                "m.status status, " +
                "m.action action ";

        sb.append(" FROM merchant m WHERE m.merchant_type_id = 1 ");

        MerchantQueryHelper helper = MerchantQueryHelper
                .builder()
                .code(merchantCrudQuery.getCode())
                .fullName(merchantCrudQuery.getFullName())
                .phoneNumber(merchantCrudQuery.getPhoneNumber())
                .address(merchantCrudQuery.getAddress())
                .registerDate(merchantCrudQuery.getRegisterDate())
                .build();
        appendCondition(sb, helper);

        if (Objects.nonNull(merchantCrudQuery.getStatus())) {
            sb.append(" AND m.status = :status ");
        }

        if (Objects.nonNull(merchantCrudQuery.getAction())) {
            sb.append(" AND m.action = :action ");
        }

        if (!CollectionUtils.isEmpty(merchantCrudQuery.getIncludedIds())) {
            sb.append(" AND m.merchant_id IN (:includedIds) ");
        }

        if (!CollectionUtils.isEmpty(merchantCrudQuery.getExcludedIds())) {
            sb.append(" AND m.merchant_id NOT IN (:excludedIds) ");
        }

        String fromClause = sb.toString();

        String nativeQueryString = nativeQuerySelectClause + fromClause + SpringUtils.getOrderBy(pageable);
        Query nativeQuery = createMerchantNativeQuery(helper, nativeQueryString, "MerchantCrudResource");
        Query nativeQueryCount = createMerchantNativeQueryCount(helper, "SELECT count(1) " + fromClause);

        if (Objects.nonNull(merchantCrudQuery.getStatus())) {
            nativeQuery.setParameter("status", merchantCrudQuery.getStatus());
            nativeQueryCount.setParameter("status", merchantCrudQuery.getStatus());
        }

        if (Objects.nonNull(merchantCrudQuery.getAction())) {
            nativeQuery.setParameter("action", merchantCrudQuery.getAction());
            nativeQueryCount.setParameter("action", merchantCrudQuery.getAction());
        }

        if (!CollectionUtils.isEmpty(merchantCrudQuery.getIncludedIds())) {
            nativeQuery.setParameter("includedIds", merchantCrudQuery.getIncludedIds());
            nativeQueryCount.setParameter("includedIds", merchantCrudQuery.getIncludedIds());
        }

        if (!CollectionUtils.isEmpty(merchantCrudQuery.getExcludedIds())) {
            nativeQuery.setParameter("excludedIds", merchantCrudQuery.getExcludedIds());
            nativeQueryCount.setParameter("excludedIds", merchantCrudQuery.getExcludedIds());
        }

        if (pageable.isPaged()) {
            nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
            nativeQuery.setMaxResults(pageable.getPageSize());
        }

        List<MerchantCrudResource> resultList = nativeQuery.getResultList();
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<HunterResource> findByQuery(HunterRequestModel hunterQuery, Pageable pageable) {
        StringBuilder sb = new StringBuilder();
        String nativeQuerySelectClause = " SELECT " +
                "m.merchant_id id, m.merchant_code code, m.user_name username, m.full_name fullName, " +
                "m.mobile_phone phoneNumber, m.address address, m.create_date registerDate, m.action action, m.status status ";
        sb.append(" FROM merchant m WHERE m.merchant_type_id = 0 and m.status != 2 ");

        MerchantQueryHelper helper = MerchantQueryHelper
                .builder()
                .code(hunterQuery.getCode())
                .username(hunterQuery.getUsername())
                .fullName(hunterQuery.getFullName())
                .phoneNumber(hunterQuery.getPhoneNumber())
                .address(hunterQuery.getAddress())
                .registerDate(hunterQuery.getRegisterDate())
                .action(hunterQuery.getAction())
                .status(hunterQuery.getStatus())
                .fromRegisterDate(hunterQuery.getFromRegisterDate())
                .toRegisterDate(hunterQuery.getToRegisterDate())
                .hunterId(hunterQuery.getHunterId())
                .searchCode(hunterQuery.getSearchCode())
                .build();
        appendCondition(sb, helper);

        String fromClause = sb.toString();

        String nativeQueryString = nativeQuerySelectClause + fromClause + SpringUtils.getOrderBy(pageable);
        Query nativeQuery = this.createMerchantNativeQuery(helper, nativeQueryString, "HunterResource");
        Query nativeQueryCount = this.createMerchantNativeQueryCount(helper, "SELECT count(1) " + fromClause);

        nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        nativeQuery.setMaxResults(pageable.getPageSize());

        List<HunterResource> resultList = nativeQuery.getResultList();
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    @Override
    public Collection<SalemanExportDTO> findSalemanExportData(HunterRequestModel hunterQuery, Pageable pageable) {
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> params;

        String nameQuery;
        if(LocaleContextHolder.getLocale().getLanguage().equals("vi")){
            nameQuery = " CONCAT(m.first_name, ' ', m.last_name) ";
        }else {
            nameQuery = " CONCAT(m.last_name, ' ', m.first_name) ";
        }
        String nativeQuerySelectClause = " SELECT m.merchant_code code, m.USER_NAME username, "+nameQuery+" fullName, " +
                "m.MOBILE_PHONE phoneNumber, m.address address, date_format(m.create_date, '%d/%m/%Y') registerDate, m.status status, CONCAT(parent.first_name, ' ', parent.last_name) parent, " +
                "m.identity_number idNumber, m.contract_number contractNumber, m.default_bank_account_no " +
                "bankAccountNo, m.wallet_client_id clientWalletId";

        sb.append(" FROM merchant m left join merchant parent on parent.MERCHANT_ID = m.PARENT_MARCHANT_ID WHERE m.merchant_type_id = 0 ");

        if (Objects.nonNull(hunterQuery.getStatus())) {
            sb.append(" AND m.status = :status ");
        } else {
            sb.append(" and m.status in (0,1) ");
        }

        MerchantQueryHelper helper = MerchantQueryHelper
                .builder()
                .code(hunterQuery.getCode())
                .username(hunterQuery.getUsername())
                .fullName(hunterQuery.getFullName())
                .phoneNumber(hunterQuery.getPhoneNumber())
                .address(hunterQuery.getAddress())
                .registerDate(hunterQuery.getRegisterDate())
                .action(hunterQuery.getAction())
                .status(hunterQuery.getStatus())
                .fromRegisterDate(hunterQuery.getFromRegisterDate())
                .toRegisterDate(hunterQuery.getToRegisterDate())
                .hunterId(hunterQuery.getHunterId())
                .searchCode(hunterQuery.getSearchCode())
                .build();
        appendCondition(sb, helper);
        params = appendParams(hunterQuery);

        String fromClause = sb.toString();
        String nativeQueryString = nativeQuerySelectClause + fromClause + SpringUtils.getOrderBy(pageable);

        Collection<SalemanExportDTO> listResult = null;
        try {
            listResult = commonService.findAll(null, SalemanExportDTO.class, nativeQueryString, params);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return listResult;
    }

    private HashMap<String, Object> appendParams(HunterRequestModel model) {
        HashMap<String, Object> params = new HashMap<>();
        if (StringUtils.notEmpty(model.getCode())) {
            params.put("code", model.getCode());
        }

        if (StringUtils.notEmpty(model.getUsername())) {
            params.put("username", model.getUsername());
        }

        if (StringUtils.notEmpty(model.getFullName())) {
            params.put("fullName", model.getFullName());
        }

        if (StringUtils.notEmpty(model.getPhoneNumber())) {
            params.put("phoneNumber", model.getPhoneNumber());
        }

        if (StringUtils.notEmpty(model.getAddress())) {
            params.put("address", model.getAddress());
        }

        if (Objects.nonNull(model.getRegisterDate())) {
            params.put("registerDate", model.getRegisterDate());
        }

        if (Objects.nonNull(model.getAction())) {
            params.put("action", model.getAction());
        }

        if (Objects.nonNull(model.getStatus())) {
            params.put("status", model.getStatus());
        }
        if (Objects.nonNull(model.getFromRegisterDate())) {
            params.put("fromRegisterDate", model.getFromRegisterDate());
        }
        if (Objects.nonNull(model.getToRegisterDate())) {
            params.put("toRegisterDate", model.getToRegisterDate());
        }
        if (Objects.nonNull(model.getSearchCode())) {
            params.put("searchCode", model.getSearchCode());
        }
        if (Objects.nonNull(model.getHunterId())) {
            params.put("hunterId", model.getHunterId());
        }
        return params;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<MerchantCrudResource> findMerchants(Long hunterId, MerchantDto merchantDto, Pageable pageable) {
        StringBuilder sb = new StringBuilder();
        String nativeQuerySelectClause = " SELECT " +
                "m.merchant_id id, " +
                "m.merchant_code code, " +
                "m.user_name username, " +
                "m.full_name fullName, " +
                "m.mobile_phone phoneNumber, " +
                "m.address address, " +
                "m.create_date registerDate, " +
                "m.wallet_client_id walletId, " +
                "m.status status, " +
                "m.action action ";

        sb.append(" FROM merchant m WHERE m.PARENT_MARCHANT_ID = :hunterId AND m.merchant_type_id = 1 ");

        MerchantQueryHelper helper = MerchantQueryHelper
                .builder()
                .code(merchantDto.getMerchantCode())
                .username(merchantDto.getUserName())
                .fullName(merchantDto.getFullName())
                .phoneNumber(merchantDto.getMobilePhone())
                .address(merchantDto.getAddress())
                .registerDate(merchantDto.getRegisterDate())
                .action(merchantDto.getAction())
                .status(merchantDto.getStatus())
                .build();
        appendCondition(sb, helper);

        String fromClause = sb.toString();

        String nativeQueryString = nativeQuerySelectClause + fromClause + SpringUtils.getOrderBy(pageable);
        Query nativeQuery = this.createMerchantNativeQuery(helper, nativeQueryString, "MerchantCrudResource");
        Query nativeQueryCount = this.createMerchantNativeQueryCount(helper, "SELECT count(1) " + fromClause);

        nativeQuery.setParameter("hunterId", hunterId);
        nativeQueryCount.setParameter("hunterId", hunterId);

        nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        nativeQuery.setMaxResults(pageable.getPageSize());

        List<MerchantCrudResource> resultList = nativeQuery.getResultList();
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    private Query createMerchantNativeQuery(MerchantQueryHelper merchantCrudQuery, String query, String mappingName) {
        Query nativeQuery = em.createNativeQuery(query, mappingName);
        setMerchantQueryParams(merchantCrudQuery, nativeQuery);
        return nativeQuery;
    }

    private Query createMerchantNativeQueryCount(MerchantQueryHelper merchantCrudQuery, String query) {
        Query nativeQuery = em.createNativeQuery(query);
        setMerchantQueryParams(merchantCrudQuery, nativeQuery);
        return nativeQuery;
    }

    private void setMerchantQueryParams(MerchantQueryHelper helper, Query nativeQuery) {
        if (StringUtils.notEmpty(helper.code)) {
            nativeQuery.setParameter("code", decode(helper.code));
        }
        if (StringUtils.notEmpty(helper.username)) {
            nativeQuery.setParameter("username", decode(helper.username));
        }
        if (StringUtils.notEmpty(helper.fullName)) {
            nativeQuery.setParameter("fullName", decode(helper.fullName));
        }
        if (StringUtils.notEmpty(helper.phoneNumber)) {
            nativeQuery.setParameter("phoneNumber", decode(helper.phoneNumber));
        }
        if (StringUtils.notEmpty(helper.address)) {
            nativeQuery.setParameter("address", decode(helper.address));
        }
        if (Objects.nonNull(helper.registerDate)) {
            nativeQuery.setParameter("registerDate", helper.registerDate);
        }
        if (Objects.nonNull(helper.action)) {
            nativeQuery.setParameter("action", helper.action);
        }
        if (Objects.nonNull(helper.status)) {
            nativeQuery.setParameter("status", helper.status);
        }
        if (Objects.nonNull(helper.hunterId)) {
            nativeQuery.setParameter("hunterId", helper.hunterId);
        }
        if (Objects.nonNull(helper.fromRegisterDate)) {
            nativeQuery.setParameter("fromRegisterDate", helper.fromRegisterDate);
        }
        if (Objects.nonNull(helper.toRegisterDate)) {
            nativeQuery.setParameter("toRegisterDate", helper.toRegisterDate);
        }
        if (Objects.nonNull(helper.searchCode)) {
            nativeQuery.setParameter("searchCode", helper.searchCode);
        }
    }

    private String decode(String value) {
        try {
            return URLDecoder.decode(value, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            return value;
        }
    }


    private void appendCondition(StringBuilder sb, MerchantQueryHelper helper) {
        if (StringUtils.notEmpty(helper.code)) {
            sb.append(" AND m.merchant_code like CONCAT('%', :code ,'%') ");
        }

        if (StringUtils.notEmpty(helper.username)) {
            sb.append(" AND m.user_name like CONCAT('%', :username ,'%') ");
        }

        if (StringUtils.notEmpty(helper.fullName)) {
            sb.append(" AND m.full_name like CONCAT('%', :fullName ,'%') ");
        }

        if (StringUtils.notEmpty(helper.phoneNumber)) {
            sb.append(" AND m.mobile_phone like CONCAT('%', :phoneNumber ,'%') ");
        }

        if (StringUtils.notEmpty(helper.address)) {
            sb.append(" AND m.address like CONCAT('%', CONVERT( :address , BINARY) ,'%') ");
        }

        if (Objects.nonNull(helper.registerDate)) {
            sb.append(" AND date(m.create_date) = date( :registerDate ) ");
        }

        if (Objects.nonNull(helper.action)) {
            sb.append(" AND m.action = :action");
        }

        if (Objects.nonNull(helper.status)) {
            sb.append(" AND m.status = :status");
        }
        if (Objects.nonNull(helper.hunterId)) {
            sb.append(" and m.PARENT_MARCHANT_ID = :hunterId");
        }
        if (Objects.nonNull(helper.fromRegisterDate)) {
            sb.append(" and date(m.create_date) >= date( :fromRegisterDate ) ");
        }
        if (Objects.nonNull(helper.toRegisterDate)) {
            sb.append("and date(m.create_date) <= date( :toRegisterDate ) ");
        }
        if (Objects.nonNull(helper.searchCode)) {
            sb.append(" and (m.mobile_phone like CONCAT('%', :searchCode ,'%') or m.full_name like ");
            sb.append(" CONCAT('%', :searchCode ,'%') or m.identity_number like CONCAT('%', :searchCode ,'%' ) )");
        }
    }

    @Builder
    private static class MerchantQueryHelper {
        String code;
        String username;
        String fullName;
        String phoneNumber;
        String address;
        LocalDateTime registerDate;
        Integer action;
        Integer status;
        String searchCode;
        LocalDateTime fromRegisterDate;
        LocalDateTime toRegisterDate;
        Long hunterId;
    }
}
