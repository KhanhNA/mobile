package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.model.PackingPrice;
import com.nextsolutions.dcommerce.model.ProductLinks;
import com.nextsolutions.dcommerce.shared.dto.ProductDetailDto;
import com.nextsolutions.dcommerce.shared.dto.product.PackingPriceDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductQueryDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductTableDto;
import com.nextsolutions.dcommerce.ui.model.response.ProductFormResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductPackingResponseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductRepositoryCustom {
    ProductDetailDto getProductDetail(long id, Long langId) throws Exception;

    Page<ProductTableDto> findByQuery(ProductQueryDto query, Pageable pageable);

    ProductFormResponseModel findByIdAndCategoryIdAndLanguageId(Long id, Long categoryId, Long languageId);

    Page<ProductPackingResponseModel> findByCategoryIdAndPackingTypeId(Long categoryId, Long packingTypeId, Pageable pageable);

    PackingPriceDto getPackingPrice(Long packingProductId, Integer type) throws Exception;
}
