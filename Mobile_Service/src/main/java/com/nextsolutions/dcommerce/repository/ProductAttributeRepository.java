package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.ProductAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ProductAttributeRepository extends JpaRepository<ProductAttribute, Long> {

    @Modifying
    @Query("DELETE FROM ProductAttribute pa WHERE pa.productOptionId = ?1")
    void deleteAllByProductOptionId(Long productOptionId);
}
