package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.RecommendSearch;
import com.nextsolutions.dcommerce.repository.custom.RecommendSearchRepositoryCustom;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.shared.dto.resource.RecommendSearchResource;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.nextsolutions.dcommerce.utils.StringUtils.notEmpty;


@Repository
@RequiredArgsConstructor
public class RecommendSearchRepositoryCustomImpl implements RecommendSearchRepositoryCustom {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    CommonService commonService;

    @SuppressWarnings("unchecked")
    @Override
    public Page<RecommendSearchResource> findByQuery(RecommendSearchResource recommendSearchResource, Pageable pageable) throws Exception {
        StringBuilder selectAllValue = new StringBuilder(" SELECT id,recommendId,fromDate,toDate,type,status,languageId,name,imageUrl from ");
        StringBuilder selectClause = new StringBuilder(" (( Select r.id id, r.recommend_id recommendId, r.from_date fromDate, ");
        selectClause.append(" r.to_date toDate, r.type type, r.status status,  p.language_id languageId,");
        selectClause.append(" p.product_name name, pp.PACKING_URL imageUrl");
        java.lang.String selectCountClause = "select count(1) from (";
        selectClause.append(" from recommend_search r join packing_product pp on r.recommend_id = pp.packing_product_id");
        selectClause.append(" join product_description p on p.product_id = pp.PRODUCT_ID where r.type = 1)");

        HashMap<String, Object> params = new HashMap<>();
        Long recommendId = recommendSearchResource.getRecommendId();
        String name = recommendSearchResource.getName();
        LocalDateTime fromDate = recommendSearchResource.getFromDate();
        LocalDateTime toDate = recommendSearchResource.getToDate();
        Integer languageId = recommendSearchResource.getLanguageId();
        Integer type = recommendSearchResource.getType();
        Long status = recommendSearchResource.getStatus();
        StringBuilder whereClause = new StringBuilder();

        selectClause.append(" union ( Select r.id id, r.recommend_id recommendId, r.from_date fromDate, ");
        selectClause.append(" r.to_date toDate, r.type type, r.status status,  cd.language_id languageId,");
        selectClause.append(" cd.name name, c.url_image imageUrl");
        selectClause.append(" from recommend_search r join category_description cd ON r.recommend_id = cd.CATEGORY_ID");
        selectClause.append(" join category c ON c.category_id = cd.category_id where r.type = 2))");
        whereClause.append(" as value");
        whereClause.append(" where languageId = :languageId ");
        if (recommendId != null) {
            whereClause.append(" AND recommendId = :recommendId ");
        }

        if (notEmpty(name)) {
            whereClause.append(" AND lower(name) like lower(concat('%',:name,'%')) ");
        }

        if (fromDate != null && toDate != null) {
            whereClause.append(" AND fromDate >= date('" + parseDatetoString(fromDate) + "') AND toDate <= date('" + parseDatetoString(toDate) + "') ");
        } else {
            if (fromDate != null) {
                whereClause.append(" AND date(fromDate) >= date('" + parseDatetoString(fromDate) + "') ");
            }
            if (toDate != null) {
                whereClause.append(" AND date(toDate) <= date('" + parseDatetoString(toDate) + "') ");
            }
        }

        if (type != null) {
            whereClause.append(" AND type = :type");
        }

        if (status != null) {
            whereClause.append(" and status = :status");
        }

        String nativeQueryString = selectAllValue.toString() + selectClause.toString() + whereClause.toString();

        if (recommendId != null) {
            params.put("recommendId", recommendId);
        }

        if (notEmpty(name)) {
            params.put("name", name);
        }

/*
        if (fromDate != null && toDate != null) {
            params.put("fromDate", fromDate);
            params.put("toDate", toDate);
        } else {
            if (fromDate != null) {
                params.put("fromDate", fromDate);
            }
            if (toDate != null) {
                params.put("toDate", toDate);
            }
        }
*/

        if (type != null) {
            params.put("type", type);
        }

        if (status != null) {
            params.put("status", status);
        }

        if (languageId == null) {
            params.put("languageId", 1);
        } else {
            params.put("languageId", languageId);
        }

        List<RecommendSearchResource> recommendSearchResourceList =
                commonService.findAll(pageable, RecommendSearchResource.class, nativeQueryString, params);

        String sqlCount = String.format("select count(*) from (%s) a", nativeQueryString);
        int count = commonService.getRowCountV2(sqlCount, params);
        return new PageImpl<>(recommendSearchResourceList, pageable, count);
    }

    @Override
    public Optional<RecommendSearch> findbyIdAndMustActive(Long id) {
        return Optional.empty();
    }

    @Override
    public RecommendSearchResource getRecommendSearch(Long id) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        StringBuilder selectAllValue = new StringBuilder(" SELECT id,recommendId,fromDate,toDate,type,status,languageId,name,imageUrl from ");
        StringBuilder selectClause = new StringBuilder(" (( Select r.id id, r.recommend_id recommendId, r.from_date fromDate, ");
        selectClause.append(" r.to_date toDate, r.type type, r.status status,  p.language_id languageId,");
        selectClause.append(" p.product_name name, pp.PACKING_URL imageUrl");
        selectClause.append(" from recommend_search r join packing_product pp on r.recommend_id = pp.packing_product_id");
        selectClause.append(" join product_description p on p.product_id = pp.PRODUCT_ID where r.type = 1)");
        selectClause.append(" union ( Select r.id id, r.recommend_id recommendId, r.from_date fromDate, ");
        selectClause.append(" r.to_date toDate, r.type type, r.status status,  cd.language_id languageId,");
        selectClause.append(" cd.name name, c.url_image imageUrl");
        selectClause.append(" from recommend_search r join category_description cd ON r.recommend_id = cd.CATEGORY_ID");
        selectClause.append(" join category c ON c.category_id = cd.category_id where r.type = 2)) as value");
        selectClause.append(" where languageId = :languageId and id = :id ");
        params.put("languageId", LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage()));
        params.put("id", id);
        List<RecommendSearchResource> resources = commonService.findAll(null, RecommendSearchResource.class, selectAllValue.toString() + selectClause.toString(), params);
        StringBuilder selectRecommendItem = new StringBuilder();
        if (!resources.isEmpty()) {
            Integer type = resources.get(0).getType();
            Long recommendId = resources.get(0).getRecommendId();
            if (type == 2) {
                selectRecommendItem.append("Select c.category_id packingProductId, c.code code, c.url_image packingUrl, c.name name from category c join category_description cd on cd.category_id = c.category_id  where cd.language_id = :languageId and c.category_id = :id");
            } else {
                selectRecommendItem.append("Select pd.packing_product_id packingProductId, pd.code code, pd.PACKING_URL packingUrl, pd.name name from packing_product pd join product_description p on p.product_id = pd.PRODUCT_ID  where p.language_id = :languageId and pd.packing_product_id = :id");
            }
            HashMap<String, Object> p = new HashMap<>();
            p.put("id", recommendId);
            p.put("languageId", LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage()));
            List<PackingProduct> list = commonService.findAll(null, PackingProduct.class, selectRecommendItem.toString(), p);
            resources.get(0).setPackingProduct(list.get(0));
            return resources.get(0);
        } else
            return null;
    }

    private String parseDatetoString(LocalDateTime dateTime) {
        String str = dateTime.toString();
        return str.substring(0, 10);
    }
}
