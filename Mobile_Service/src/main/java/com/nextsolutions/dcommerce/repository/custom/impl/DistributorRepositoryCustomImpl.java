package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.repository.custom.DistributorRepositoryCustom;
import com.nextsolutions.dcommerce.shared.dto.distributor.DistributorDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class DistributorRepositoryCustomImpl implements DistributorRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<DistributorDto> queryDistributors(DistributorDto distributorDto, Pageable pageable) {

        return null;
    }
}
