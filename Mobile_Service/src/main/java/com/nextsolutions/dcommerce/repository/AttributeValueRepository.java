package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.AttributeValue;
import com.nextsolutions.dcommerce.shared.dto.AttributeValueDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface AttributeValueRepository extends JpaRepository<AttributeValue, Long>, JpaSpecificationExecutor<AttributeValue> {

    @Query("SELECT av FROM AttributeValue av WHERE av.attributeId = ?1 AND av.id = ?2 AND av.status <> 0")
    Optional<AttributeValue> findByIdAndActivated(Long id, Long valueId);

    @Query("SELECT new com.nextsolutions.dcommerce.shared.dto.AttributeValueDto(av.id, a.code, av.code, avd.name, av.request, av.statusProcess, av.value) " +
            "FROM AttributeValue av JOIN av.attribute a JOIN av.descriptions avd " +
            "WHERE a.id = ?1 AND avd.languageId = ?2 AND av.status <> 0")
    List<AttributeValueDto> getAttributeValueTable(Long id, Integer languageId);

    @Transactional
    @Modifying
    @Query("UPDATE AttributeValue av SET av.status = 0 WHERE av.attributeId = ?1 AND av.id = ?2")
    void inactive(Long id, Long valueId);

    @Transactional
    @Modifying
    @Query("UPDATE AttributeValue av SET av.status = 0 WHERE av.attributeId = ?1")
    void inactive(Long id);

    @Query("FROM AttributeValue av WHERE av.id = ?1 AND av.attributeId = ?2")
    Optional<AttributeValue> findByIdAndAttributeId(Long valueId, Long id);
}
