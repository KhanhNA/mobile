package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.IncentiveProgramMerchantGroup;
import com.nextsolutions.dcommerce.model.IncentiveProgramSelectedWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface IncentiveProgramMerchantGroupRepository extends JpaRepository<IncentiveProgramMerchantGroup, Long>, JpaSpecificationExecutor<IncentiveProgramMerchantGroup> {

    void deleteByIncentiveProgramId(Long id);

    List<IncentiveProgramMerchantGroup> findAllByIncentiveProgramId(Long incentiveProgramId);
}
