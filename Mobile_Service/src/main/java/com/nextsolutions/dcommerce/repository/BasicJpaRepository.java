package com.nextsolutions.dcommerce.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;

public class BasicJpaRepository {
	@PersistenceContext
	private EntityManager entityManager;

	public <T> List<T> findAll(Class<T> clazz, String sql, HashMap<String, Object> params, Integer maxResult)
			throws Exception {
		Query q = entityManager.createQuery(sql);
		if (params != null) {
			for (HashMap.Entry me : params.entrySet()) {
				System.out.println("Key: " + me.getKey() + " & Value: " + me.getValue());
				q.setParameter((String) me.getKey(), me.getValue());
			}
		}
		
		return q.getResultList();

	}

}
