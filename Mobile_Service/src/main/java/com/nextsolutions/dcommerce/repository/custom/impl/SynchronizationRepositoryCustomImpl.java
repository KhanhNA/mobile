package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.repository.custom.SynchronizationRepositoryCustom;
import com.nextsolutions.dcommerce.service.exception.UpdatingIdsFailureException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SynchronizationRepositoryCustomImpl implements SynchronizationRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public void synchronize(String entityName, List<Long> updateIds) throws ClassNotFoundException {
        List<Long> failureIds = new ArrayList<>(updateIds.size());
        String tableName = entityName;
        Class<?> clazz = Class.forName("com.nextsolutions.dcommerce.model." + entityName);

        if(clazz.isAnnotationPresent(Entity.class)) {
            if (clazz.isAnnotationPresent(Table.class)) {
                Table annotation = clazz.getAnnotation(Table.class);
                tableName = annotation.name();
            } else {
                tableName = clazz.getSimpleName();
            }
        }

        String idName = "";
        for (Field field : clazz.getDeclaredFields()) {
            if(field.isAnnotationPresent(Id.class)) {
                if(field.isAnnotationPresent(Column.class)) {
                    Column column = field.getAnnotation(Column.class);
                    idName = column.name();
                } else {
                    idName = field.getName();
                }
                break;
            }
        }

        for (Long updateId : updateIds) {
            try {
                Query query = em.createNativeQuery("update " + tableName + " set is_synchronization = true where " + idName + " = :id");
                query.setParameter("id", updateId);
                int rowAffected = query.executeUpdate();
                if (rowAffected <= 0) {
                    throw new RuntimeException();
                }
            } catch (Exception e) {
                failureIds.add(updateId);
            }
        }
        if (failureIds.size() > 0) {
            throw new UpdatingIdsFailureException(entityName, failureIds);
        }
    }
}
