package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.MerchantActiveCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MerchantActiveCodeRepository extends JpaRepository<MerchantActiveCode, Long> {
    MerchantActiveCode findByActiveCode(String code);
}
