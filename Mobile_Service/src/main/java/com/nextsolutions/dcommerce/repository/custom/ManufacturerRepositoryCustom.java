package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.shared.dto.resource.ManufacturerResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManufacturerRepositoryCustom {
    Page<ManufacturerResource> findAll(ManufacturerResource resource, Pageable pageable);


}
