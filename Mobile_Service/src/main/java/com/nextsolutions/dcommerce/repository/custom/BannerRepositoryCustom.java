package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.model.Banner;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface BannerRepositoryCustom {
    Page<BannerResource> findByQuery(BannerResource resource, Pageable pageable);

    Optional<Banner> findByIdAndMustActive(Long id);
}
