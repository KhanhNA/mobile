package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.model.RecommendSearch;
import com.nextsolutions.dcommerce.shared.dto.resource.RecommendSearchResource;
import com.nextsolutions.dcommerce.ui.model.response.RecommendSearchResponseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface RecommendSearchRepositoryCustom {
    Page<RecommendSearchResource> findByQuery(RecommendSearchResource recommendSearchResource, Pageable pageable) throws Exception;

    Optional<RecommendSearch> findbyIdAndMustActive(Long id);

    RecommendSearchResource getRecommendSearch(Long id) throws Exception;


}
