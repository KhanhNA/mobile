package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.shared.dto.CategoryDto;
import com.nextsolutions.dcommerce.shared.dto.CategoryHierarchy;
import com.nextsolutions.dcommerce.shared.projection.CategoryProductProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CategoryRepositoryCustom {
	List<CategoryDto> getCategoryTreeFormat(Long id, Integer langId);

	List<CategoryHierarchy> getCategoryHierarchyById(Long id);

	Page<CategoryDto> getListCategoryByLevel(Pageable pageable, Long level, Long code) throws Exception;
}
