package com.nextsolutions.dcommerce.repository.custom;

import java.util.List;

import com.nextsolutions.dcommerce.shared.dto.KpiMonthDto;
import com.nextsolutions.dcommerce.model.charts.KpiMonthChart;

public interface KpiMonthCustomRepository {

	List<KpiMonthDto> findAll(KpiMonthDto dto) throws Exception;

	List<KpiMonthChart> findAllInMonthByKpiType(String kpiType);

	List<KpiMonthChart> findAllInYearByKpiType(String kpiType);
}
