package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.repository.custom.MerchantGroupRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;

public interface MerchantGroupRepository extends JpaRepository<MerchantGroup, Long>, MerchantGroupRepositoryCustom {

    @Query("SELECT mg FROM MerchantGroup mg WHERE mg.status <> 0 AND mg.id = ?1")
    Optional<MerchantGroup> findByIdAndActivated(Long id);

    @Modifying
    @Transactional
    @Query("UPDATE MerchantGroup mg SET mg.status = 0 WHERE mg.id = ?1")
    void inactiveMerchantGroup(Long id);
}
