package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.MerchantImages;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MerchantImageRepository extends JpaRepository<MerchantImages, Long> {

}
