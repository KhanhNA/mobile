package com.nextsolutions.dcommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.MerchantGroupAttribute;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.DeleteMapping;

import javax.transaction.Transactional;

public interface MerchantGroupAttributeRepository extends JpaRepository<MerchantGroupAttribute, Long>, JpaSpecificationExecutor<MerchantGroupAttribute> {

    @Modifying
    @Query("DELETE FROM MerchantGroupAttribute mga WHERE mga.merchantGroupId = ?1")
    void deleteByMerchantGroupId(Long id);
}
