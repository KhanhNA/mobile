package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.Distributor;
import com.nextsolutions.dcommerce.repository.custom.DistributorRepositoryCustom;
import com.nextsolutions.dcommerce.service.annotation.ColumnMapper;
import com.nextsolutions.dcommerce.shared.dto.distributor.DistributorDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface DistributorRepository extends JpaRepository<Distributor, Long>,
        DistributorRepositoryCustom,
        JpaSpecificationExecutor<Distributor> {

    Optional<Distributor> findByCode(String code);

    @Modifying
    @Query("UPDATE Distributor d SET d.enabled = false, d.isSynchronization = false WHERE d.id = ?1")
    void inactivateById(Long id);

    Optional<Distributor> findByPhoneNumber(String phoneNumber);

    @Query("SELECT d.username FROM Distributor d WHERE d.id = ?1")
    String findUsernameById(Long id);

    Optional<Distributor> findByEmail(String email);

    Optional<Distributor> findByTaxCode(String email);
}
