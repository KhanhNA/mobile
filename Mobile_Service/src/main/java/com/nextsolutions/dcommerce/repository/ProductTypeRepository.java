package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.ui.model.response.ProductTypeLookupResponseModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.ProductType;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductTypeRepository extends JpaRepository<ProductType, Long>, JpaSpecificationExecutor<ProductType> {

    @Query("SELECT new com.nextsolutions.dcommerce.ui.model.response.ProductTypeLookupResponseModel(p.id, p.code, p.name) " +
            "FROM ProductType p")
    List<ProductTypeLookupResponseModel> findAllProductTypesLookup();
}
