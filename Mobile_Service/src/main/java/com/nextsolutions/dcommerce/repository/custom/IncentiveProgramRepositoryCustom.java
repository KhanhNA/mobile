package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.model.IncentiveProgram;
import com.nextsolutions.dcommerce.ui.model.incentive.IncentiveProgramQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IncentiveProgramRepositoryCustom {
    Page<IncentiveProgram> findByQuery(IncentiveProgramQuery query, Pageable pageable);

    boolean isExistsPackingIdInOtherIncentivePrograms(Long id, Long packingId);

    boolean isExistsPackingProductsIncentivePrograms(Long id) throws Exception;
}
