package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.Banner;
import com.nextsolutions.dcommerce.model.RecommendSearch;
import com.nextsolutions.dcommerce.repository.custom.BannerRepositoryCustom;
import com.nextsolutions.dcommerce.repository.custom.RecommendSearchRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface RecommendSearchRepository extends JpaRepository<RecommendSearch, Long>, JpaSpecificationExecutor<RecommendSearch>, RecommendSearchRepositoryCustom {
    @Transactional
    @Modifying
    @Query("Update RecommendSearch r set r.status = 0 where r.id = ?1")
    void inactive(Long id);
}
