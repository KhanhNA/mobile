package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.ui.model.request.ProductOptionQueryRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionTableResponseModel;

import java.util.Collection;
import java.util.List;

public interface ProductOptionRepositoryCustom {

    Collection<ProductOptionResponseModel> findAllByLanguageId(Long languageId);

    List<ProductOptionTableResponseModel> findAllByQueryAndLanguage(ProductOptionQueryRequestModel query, Long languageId);
}
