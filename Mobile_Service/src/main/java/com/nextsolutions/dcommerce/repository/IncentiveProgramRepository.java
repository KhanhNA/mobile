package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.IncentiveProgram;
import com.nextsolutions.dcommerce.repository.custom.IncentiveProgramRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface IncentiveProgramRepository extends JpaRepository<IncentiveProgram, Long>,
        JpaSpecificationExecutor<IncentiveProgram>, IncentiveProgramRepositoryCustom {
    Optional<IncentiveProgram> findByCode(String code);


}
