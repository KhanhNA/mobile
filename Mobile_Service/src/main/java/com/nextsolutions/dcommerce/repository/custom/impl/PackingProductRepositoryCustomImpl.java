package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.repository.CategoryRepository;
import com.nextsolutions.dcommerce.repository.custom.PackingProductRepositoryCustom;
import com.nextsolutions.dcommerce.repository.custom.dto.IncentiveDateRange;
import com.nextsolutions.dcommerce.shared.dto.CategoryHierarchy;
import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import com.nextsolutions.dcommerce.shared.dto.ProductDto;
import com.nextsolutions.dcommerce.ui.model.request.CategoryGroupPriceRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.PackingProductResource;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductPackageResource;
import com.nextsolutions.dcommerce.shared.dto.resource.PromotionPriceResource;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.StringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class PackingProductRepositoryCustomImpl implements PackingProductRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    private final CategoryRepository categoryRepository;

    @Override
    @SuppressWarnings("unchecked")
    public Page<ProductDto> findAll(Pageable pageable, Long langId, String keyWord) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT ");
        sql.append(findAllColumns());
        sql.append(" FROM ");
        sql.append(getSqlSelectFirstPacking());
        sql.append(" JOIN product_description pd ON pd.product_id = a.product_id " +
                "    LEFT JOIN packing_type pt ON pt.packing_type_id = a.PACKING_TYPE_ID " +
                "    WHERE " +
                "    pd.LANGUAGE_ID = :langId");
        if (StringUtils.notEmpty(keyWord)) {
            sql.append(" and lower(pd.PRODUCT_NAME) like lower(:keyWord)");
        }
        Query nativeQuery = em.createNativeQuery(sql.toString(), "ProductDtoMappingAll");
        nativeQuery.setParameter("langId", langId);
        if (StringUtils.notEmpty((keyWord))) {
            nativeQuery.setParameter("keyWord", keyWord + "%");
        }
        nativeQuery.setFirstResult(pageable.getPageSize() * pageable.getPageNumber());
        nativeQuery.setMaxResults(pageable.getPageSize() * pageable.getPageNumber() + pageable.getPageSize());
        List<ProductDto> resultList = nativeQuery.getResultList();

        Query nativeQueryCount = em.createNativeQuery(sql.toString().replace(findAllColumns(), "count(*)"));
        nativeQueryCount.setParameter("langId", langId);
        if (StringUtils.notEmpty(keyWord)) {
            nativeQueryCount.setParameter("keyWord", keyWord + "%");
        }
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<ProductDto> findAllIn(Pageable pageable, List<String> lstPredictionId, Long langId) {
        StringBuilder sqlOrigin = new StringBuilder();
        StringBuilder sql = new StringBuilder();
        sqlOrigin.append(" SELECT ");
        sqlOrigin.append(findAllColumns());
        sqlOrigin.append(" FROM ");
        sqlOrigin.append(getSqlSelectFirstPacking());
        sqlOrigin.append(" JOIN product_description pd ON pd.product_id = a.product_id " +
                "    LEFT JOIN packing_type pt ON pt.packing_type_id = a.PACKING_TYPE_ID " +
                "    WHERE " +
                "    pd.LANGUAGE_ID = :langId");
        sql.append(sqlOrigin.toString());

        if (Utils.isNotEmpty(lstPredictionId)) {
            sql.append(" and a.PACKING_PRODUCT_ID in ");
            sql.append(" ( ");
            sql.append(Utils.join(",", lstPredictionId));
            sql.append(" ) ");
        }

        Query nativeQuery = em.createNativeQuery(sql.toString(), "ProductDtoMappingAll");
        nativeQuery.setParameter("langId", langId);
        nativeQuery.setFirstResult(pageable.getPageSize() * pageable.getPageNumber());
        nativeQuery.setMaxResults(pageable.getPageSize() * pageable.getPageNumber() + pageable.getPageSize());
        List<ProductDto> resultList = nativeQuery.getResultList();

        Query nativeQueryCount = em.createNativeQuery(sqlOrigin.toString().replace(findAllColumns(), "count(*)"));
        nativeQueryCount.setParameter("langId", langId);
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    @Override
    public Page<ProductDto> findAllNotIn(Pageable pageable, List<String> lstPredictionId, Long langId, int filterType) {
        StringBuilder sqlOrigin = new StringBuilder();
        String sqlOrderBy = "";
        StringBuilder sql = new StringBuilder();
        sqlOrigin.append(" SELECT ");
        sqlOrigin.append(findAllColumns());
        sqlOrigin.append(" FROM ");
        sqlOrigin.append(getSqlSelectFirstPacking());
        sqlOrigin.append(" JOIN product_description pd ON pd.product_id = a.product_id " +
                "    LEFT JOIN packing_type pt ON pt.packing_type_id = a.PACKING_TYPE_ID " +
                "    WHERE " +
                "    pd.LANGUAGE_ID = :langId");
        if (filterType == 1) {
            // Name
            sqlOrderBy = " order by pd.PRODUCT_NAME  ";
        } else if (filterType == 2) {
            // Rating
            sqlOrderBy = " order by a.REVIEW_AVG desc  ";
        } else if (filterType == 3) {
            // Price asc
            sqlOrderBy = " order by price asc ";
        } else if (filterType == 4) {
            // Price desc
            sqlOrderBy = " order by price desc  ";
        }
        sql.append(sqlOrigin.toString());
        if (Utils.isNotEmpty(lstPredictionId)) {
            sql.append(" and a.PACKING_PRODUCT_ID not in ");
            sql.append(" ( ");
            sql.append(Utils.join(",", lstPredictionId));
            sql.append(" ) ");
        }
        sql.append(sqlOrderBy.toString());
        Query nativeQuery = em.createNativeQuery(sql.toString(), "ProductDtoMappingAll");
        nativeQuery.setParameter("langId", langId);
        nativeQuery.setFirstResult(pageable.getPageSize() * pageable.getPageNumber());
        nativeQuery.setMaxResults(pageable.getPageSize() * pageable.getPageNumber() + pageable.getPageSize());
        List<ProductDto> resultList = nativeQuery.getResultList();

        Query nativeQueryCount = em.createNativeQuery(sqlOrigin.toString().replace(findAllColumns(), "count(*)"));
        nativeQueryCount.setParameter("langId", langId);
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    private String findAllColumns() {
        return "     a.PACKING_PRODUCT_ID packingProductId, " +
                "    a.CODE packingProductCode, " +
                "    a.MARKET_PRICE marketPrice, " +
                "    a.ORG_PRICE orgPrice, " +
                "    pt.QUANTITY quantity, " +
                "    ifnull(a.priceSale, a.PRICE)  price, " +
                "    CASE WHEN a.priceSale is null THEN  0 " +
                "         ELSE a.DISCOUNT_PERCENT " +
                "    END discountPercent, " +
                "    a.REVIEW_COUNT reviewCount, " +
                "    a.REVIEW_AVG reviewAvg, " +
                "    a.PACKING_URL urlImage, " +
                "    pd.PRODUCT_NAME productName, " +
                "    pd.PRODUCT_ID id, " +
                "    a.MANUFACTURER_ID manufacturerId ";
    }

    private String getSqlSelectFirstPacking() {
        return "     (   SELECT " +
                " p.*, " +
                " pp.PACKING_PRODUCT_ID, " +
                " pp.CODE, " +
                " pp.PRICE, " +
                " pp.PACKING_TYPE_ID, " +
                " pp.MARKET_PRICE, " +
                " pp.DISCOUNT_PERCENT, " +
                " pp.PACKING_URL, " +
                " pp.ORG_PRICE ," +
                "  (select PROMOTION_SALE_PRICE from packing_product where DATE(CURRENT_DATE())>= DATE(from_date) and DATE(TO_DATE+1) > current_date() and PACKING_PRODUCT_ID = pp.PACKING_PRODUCT_ID  limit 1) priceSale  " +
                "        FROM " +
                " product p " +
                "     JOIN packing_product pp ON pp.PACKING_PRODUCT_ID = ( " +
                "     SELECT " +
                "         packing_product.PACKING_PRODUCT_ID " +
                "     FROM " +
                "         packing_product " +
                "     WHERE " +
                "  packing_product.PRODUCT_ID = p.PRODUCT_ID " +
                "     LIMIT " +
                "         1 " +
                " ) " +
                "    ) AS a ";
    }


    @Override
    public Page<ProductDto> findAllByCategory(Pageable pageable, Long langId, Long categoryId) {
        return new PackingProductCategoryHelper(pageable, langId, categoryId).invoke();
    }

    @Override
    public List<PackingProductDTO> findAllByProductIdInV2(List<Long> ids) {
        if (ids != null && ids.size() > 0) {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT pp.product_id productId," +
                    "    pp.PACKING_URL packingUrl," +
                    "    pp.PACKING_PRODUCT_ID packingProductId," +
                    "    pp.CODE packingProductCode," +
                    "    ifnull( (select PROMOTION_SALE_PRICE from packing_product where PACKING_PRODUCT_ID = pp.PACKING_PRODUCT_ID and DATE(CURRENT_DATE())>= DATE(from_date) and DATE(to_date +1)> current_date() limit 1),pp.PRICE)  price," +
                    "    ifnull(pp.DISCOUNT_PERCENT,0)  discountPercent," +
                    "    ifnull(pp.ORG_PRICE ,0)  orgPrice," +
                    "    ifnull(pp.MARKET_PRICE,0)  marketPrice," +
                    "    pt.quantity quantity" +
                    "    FROM");
            sql.append("  packing_product pp JOIN" +
                    " packing_type pt ON pt.packing_type_id = pp.packing_type_id" +
                    " WHERE" +
                    " pp.product_id IN (:ids) ");
            Query query = em.createNativeQuery(sql.toString(), "PackingProductDTOMappingAll");
            query.setParameter("ids", ids);
            return (List<PackingProductDTO>) query.getResultList();
        }
        return null;
    }

    @Override
    @Transactional
    public int applyGroupPrice(CategoryGroupPriceRequestModel request) {
        String queryBuilder = updateClause() + getSetClause(request.getType()) + whereClause();
        Query query = em.createNativeQuery(queryBuilder);
        query.setParameter("amountApply", request.getAmountApply());
        query.setParameter("startDate", request.getStartDate());
        query.setParameter("endDate", request.getEndDate());
        query.setParameter("categoryId", request.getCategoryId());
        query.setParameter("productIds", request.getProductIds());
        query.setParameter("packingTypeId", request.getPackingTypeId());
        return query.executeUpdate();
    }

    @Override
    @SuppressWarnings("unchecked")
    public ProductPackageResource findAllPackingByProductId(Long id) {
        Query query = em.createNativeQuery(getPackingProductResourceSqlQuery(), "PackingProductResource");
        query.setParameter("productId", id);
        List<PackingProductResource> result = query.getResultList();

        if (!result.isEmpty()) {
            result.forEach(packingProductResource -> {
                List<PromotionPriceResource> promotionPriceResources =
                        em.createNativeQuery(getPromotionPriceResourceSqlQuery(), "PromotionPriceResource")
                                .setParameter("packingProductId", packingProductResource.getId())
                                .getResultList();
                packingProductResource.setPromotionPrices(promotionPriceResources);
            });
        }

        ProductPackageResource resource = new ProductPackageResource();
        resource.setId(id);
        resource.setPackingProducts(result);
        return resource;
    }

    @Override
    public Page<PackingProductSearchResult> searchPackingProduct(String keyword, Long languageId, Long incentiveProgramId, Pageable pageable) {
        SearchPackingProductHelper searchPackingProductHelper = new SearchPackingProductHelper(incentiveProgramId);
        searchPackingProductHelper.prepareSearchPackingProduct(keyword, languageId, pageable);
        return searchPackingProductHelper.getSearchResult();
    }

    @Override
    public Page<PackingProductSearchResult> searchPackingProductByKeyWord(String keyword, Long languageId, Pageable pageable) {
        SearchPackingProductHelper searchPackingProductHelper = new SearchPackingProductHelper();
        searchPackingProductHelper.prepareSearchPackingProductByKeyword(keyword, languageId, pageable);
        return searchPackingProductHelper.getSearchPackingResult();
    }

    @Override
    public List<PackingProductSearchResult> findAllPackingProductByCategoryIdAndLanguageId(Long categoryId, Long languageId, Long incentiveProgramId) {
        SearchPackingProductHelper searchPackingProductHelper = new SearchPackingProductHelper(incentiveProgramId);
        searchPackingProductHelper.prepareSearchCategoryPackingProduct(categoryId, languageId);
        return searchPackingProductHelper.getCategoryResultList();
    }

    @Override
    public List<PackingProductSearchResult> findAllPackingProductByManufacturerIdAndLanguageId(Long manufacturerId, Long languageId, Long incentiveProgramId) {
        SearchPackingProductHelper searchPackingProductHelper = new SearchPackingProductHelper(incentiveProgramId);
        searchPackingProductHelper.prepareSearchManufacturerPackingProduct(manufacturerId, languageId);
        return searchPackingProductHelper.getManufacturerResultList();
    }

    @Override
    public List<PackingProductSearchResult> findAllPackingProductWith(List<Long> packingProductIds, Long languageId, Long incentiveProgramId) {
        SearchPackingProductHelper searchPackingProductHelper = new SearchPackingProductHelper(incentiveProgramId);
        searchPackingProductHelper.prepareSearchPackingProductWith(packingProductIds, languageId);
        return searchPackingProductHelper.getPackingProductResultList();
    }

    @Override
    public PackingProductSearchResult findPackingProductSearchBy(Long incentiveProgramId, Long packingProductId, Long languageId) {
        SearchPackingProductHelper searchPackingProductHelper = new SearchPackingProductHelper(incentiveProgramId);
        searchPackingProductHelper.prepareFindPackingProduct(packingProductId, languageId);
        return searchPackingProductHelper.getSingleResult();
    }

    private class SearchPackingProductHelper {
        private final Long incentiveProgramId;
        private List<Long> packingProductIds;
        private String keyword;
        private Long languageId;
        private Pageable pageable;
        private Long categoryId;
        private Long manufacturerId;
        private Long packingProductId;
        private String selectClause;
        private String fromClause;
        private String whereClause;
        private IncentiveDateRange incentiveDateRange;

        private SearchPackingProductHelper(Long incentiveProgramId) {
            this.incentiveProgramId = incentiveProgramId;
            this.incentiveDateRange = (IncentiveDateRange) em.createQuery("SELECT new com.nextsolutions.dcommerce.repository.custom.dto.IncentiveDateRange(ip.id, ip.fromDate, ip.toDate) FROM IncentiveProgram ip WHERE ip.id = :id")
                    .setParameter("id", incentiveProgramId)
                    .getSingleResult();
        }

        private SearchPackingProductHelper() {
            incentiveProgramId = null;
        }

        public void prepareFindPackingProduct(Long packingProductId, Long languageId) {
            this.packingProductId = packingProductId;
            this.languageId = languageId;
            this.selectClause = selectClause();
            this.fromClause = fromClause();
            this.whereClause = wherePackingProductIdClause();
        }

        public void prepareSearchPackingProduct(String keyword, Long languageId, Pageable pageable) {
            this.keyword = keyword;
            this.languageId = languageId;
            this.pageable = pageable;
            this.selectClause = selectClause();
            this.fromClause = fromClause();
            this.whereClause = whereSearchClause();
        }

        public void prepareSearchPackingProductByKeyword(String keyword, Long languageId, Pageable pageable) {
            this.keyword = keyword;
            this.languageId = languageId;
            this.pageable = pageable;
            this.selectClause = selectPackingClause();
            this.fromClause = fromClausePackingPrice();
            this.whereClause = whereSearchClause();
        }

        public void prepareSearchCategoryPackingProduct(Long categoryId, Long languageId) {
            this.categoryId = categoryId;
            this.languageId = languageId;
            this.selectClause = selectClause();
            this.fromClause = fromClause();
            this.whereClause = whereCategoryClause();
        }

        public void prepareSearchManufacturerPackingProduct(Long manufacturerId, Long languageId) {
            this.manufacturerId = manufacturerId;
            this.languageId = languageId;
            this.selectClause = selectClause();
            this.fromClause = fromClause();
            this.whereClause = whereManufacturerClause();
        }

        public void prepareSearchPackingProductWith(List<Long> packingProductIds, Long languageId) {
            this.packingProductIds = packingProductIds;
            this.languageId = languageId;
            this.selectClause = selectClause();
            this.fromClause = fromClause();
            this.whereClause = wherePackingProductIdsClause();
        }

        private String fromClause() {
            return " FROM packing_product pp " +
                    "  JOIN product p on pp.product_id = p.product_id " +
                    "  JOIN product_description pd on pd.product_id = p.product_id " +
                    "  JOIN packing_type pt on pt.packing_type_id = pp.packing_type_id " +
                    " join packing_price priceSale on priceSale.packing_product_id = pp.`PACKING_PRODUCT_ID` " +
                    "and priceSale.price_type = 2 " +
                    "and date(current_date()) >= date(priceSale.from_date) and (date(priceSale.to_date)>= date(current_date()) or priceSale.to_date is null) " +
                    "left join packing_price priceIn on priceIn.packing_product_id = pp.PACKING_PRODUCT_ID " +
                    "and priceIn.price_type = 1 " +
                    "and date(current_date()) >= date(priceIn.from_date) and (date(priceIn.to_date)>= date(current_date()) or priceIn.to_date is null) ";
        }
        private String fromClausePackingPrice() {
            return " FROM packing_product pp " +
                    "  JOIN product p on pp.product_id = p.product_id " +
                    "  JOIN product_description pd on pd.product_id = p.product_id " +
                    "  JOIN packing_type pt on pt.packing_type_id = pp.packing_type_id ";
        }

        private String whereSearchClause() {
            return " WHERE " +
                    "  pd.language_id = :languageId " +
                    "  and (" +
                    "    pp.code like CONCAT('%', :keyword ,'%') OR " +
                    "    p.product_name like CONCAT('%', :keyword ,'%') " +
                    "  ) and p.status =1";
        }

        private String whereCategoryClause() {
            return " WHERE " +
                    "  pd.language_id = :languageId " +
                    "  AND p.category_id = :categoryId   AND p.status =1 ";
        }

        private String whereManufacturerClause() {
            return " WHERE " +
                    "  pd.language_id = :languageId " +
                    "  AND p.manufacturer_id = :manufacturerId ";
        }

        private String wherePackingProductIdsClause() {
            return " WHERE " +
                    "  pd.language_id = :languageId " +
                    "  AND pp.packing_product_id IN :packingProductIds ";
        }

        private String wherePackingProductIdClause() {
            return "WHERE pd.language_id = :languageId AND pp.packing_product_id = :packingProductId";
        }

        @SuppressWarnings("unchecked")
        public Page<PackingProductSearchResult> getSearchResult() {
            Query nativeQuery = em.createNativeQuery(generateNativeQueryWithSort(), "PackingProductSearchResult");
            Query nativeQueryCount = em.createNativeQuery(generateNativeQueryCount());
            if (pageable.isPaged()) {
                nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                nativeQuery.setMaxResults(pageable.getPageSize());
            }
            nativeQuery.setParameter("incentiveProgramId", incentiveProgramId);
            bindSearchParameters(nativeQuery);
            bindIncentiveDateRangeParameters(nativeQuery);
            bindSearchParameters(nativeQueryCount);
            List<PackingProductSearchResult> resultList = nativeQuery.getResultList();
            BigInteger totalRecord = (BigInteger) nativeQueryCount.getSingleResult();
            return new PageImpl<>(resultList, pageable, totalRecord.longValue());
        }

        @SuppressWarnings("unchecked")
        public Page<PackingProductSearchResult> getSearchPackingResult() {
            Query nativeQuery = em.createNativeQuery(generateNativeQueryWithSort(), "PackingProductSearchResult");
            Query nativeQueryCount = em.createNativeQuery(generateNativeQueryCount());
            if (pageable.isPaged()) {
                nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                nativeQuery.setMaxResults(pageable.getPageSize());
            }
            bindSearchParameters(nativeQuery);
            bindSearchParameters(nativeQueryCount);
            List<PackingProductSearchResult> resultList = nativeQuery.getResultList();
            BigInteger totalRecord = (BigInteger) nativeQueryCount.getSingleResult();
            return new PageImpl<>(resultList, pageable, totalRecord.longValue());
        }

        @SuppressWarnings("unchecked")
        public List<PackingProductSearchResult> getCategoryResultList() {
            Query nativeQuery = em.createNativeQuery(generateNativeQuery(), "PackingProductSearchResult");
            bindCategoryParameters(nativeQuery);
            return nativeQuery.getResultList();
        }

        @SuppressWarnings("unchecked")
        public List<PackingProductSearchResult> getManufacturerResultList() {
            Query nativeQuery = em.createNativeQuery(generateNativeQuery(), "PackingProductSearchResult");
            bindManufacturerParameters(nativeQuery);
            return nativeQuery.getResultList();
        }

        @SuppressWarnings("unchecked")
        public List<PackingProductSearchResult> getPackingProductResultList() {
            Query nativeQuery = em.createNativeQuery(generateNativeQuery(), "PackingProductSearchResult");
            bindPackingProductIdsParameters(nativeQuery);
            return nativeQuery.getResultList();
        }

        public PackingProductSearchResult getSingleResult() {
            Query nativeQuery = em.createNativeQuery(generateNativeQuery(), "PackingProductSearchResult");
            bindPackingProductIdParameters(nativeQuery);
            try {
                return (PackingProductSearchResult) nativeQuery.getSingleResult();
            } catch (Exception e) {
                return null;
            }
        }

        private String generateNativeQuery() {
            return selectClause + fromClause + whereClause;
        }

        private String generateNativeQueryWithSort() {
            return selectClause + fromClause + whereClause + SpringUtils.getOrderBy(pageable);
        }

        private String selectClause() {
            return "SELECT " +
                    "   pp.packing_product_id packingId, " +
                    "   pp.code packingCode, " +
                    "   pp.packing_url productImageUrl, " +
                    "   pd.product_name productName, " +
                    "   pt.quantity packingQuantity, " +
                    "   priceIn.price  packingOriginalPrice, " +
                    "   priceSale.price packingSalePrice, " +
                    "   (SELECT EXISTS (SELECT 1 " +
                    "    FROM incentive_program_detail_information ipd " +
                    "      JOIN incentive_program_v2 ip ON ipd.incentive_program_id = ip.id " +
                    "    WHERE ipd.packing_id = pp.packing_product_id " +
                    "       AND IF(:toDate IS NULL, " +
                    "              (IF(ip.to_date IS NULL, TRUE ,date(ip.to_date) >= date(:fromDate))), " +
                    "              (date(ip.from_date) <= date(:toDate) " +
                    "               AND (date(ip.to_date) >= date(:fromDate) OR ip.TO_DATE IS NULL" +
                    "              ))) " +
                    "      AND ip.id <> :incentiveProgramId " +
                    "      AND ip.status = 1) " + // TODO: If the performance slow, try to add LIMIT 1 IN SELECT 1 statement (haint)
                    "   ) isExistsInOtherIncentivePrograms ";
        }

        private String selectPackingClause() {
            return "SELECT " +
                    "   pp.packing_product_id packingId, " +
                    "   pp.code packingCode, " +
                    "   pp.packing_url productImageUrl, " +
                    "   pd.product_name productName, " +
                    "   pt.quantity packingQuantity, " +
                    "   p.import_price packingOriginalPrice, " +
                    "   pp.price packingSalePrice, " +
                    "   true isExistsInOtherIncentivePrograms ";
        }

        private String generateNativeQueryCount() {
            return "SELECT count(1) " + fromClause + whereClause;
        }

        private void bindSearchParameters(Query query) {
            query.setParameter("keyword", keyword);
            query.setParameter("languageId", languageId);
        }

        private void bindIncentiveDateRangeParameters(Query query) {
            query.setParameter("fromDate", incentiveDateRange.getFromDate());
            query.setParameter("toDate", incentiveDateRange.getToDate());
        }

        private void bindCategoryParameters(Query query) {
            query.setParameter("categoryId", categoryId);
            query.setParameter("languageId", languageId);
            query.setParameter("incentiveProgramId", incentiveProgramId);
            bindIncentiveDateRangeParameters(query);
        }

        private void bindManufacturerParameters(Query query) {
            query.setParameter("manufacturerId", manufacturerId);
            query.setParameter("languageId", languageId);
            query.setParameter("incentiveProgramId", incentiveProgramId);
            bindIncentiveDateRangeParameters(query);
        }

        private void bindPackingProductIdParameters(Query query) {
            query.setParameter("languageId", languageId);
            query.setParameter("packingProductId", packingProductId);
            query.setParameter("incentiveProgramId", incentiveProgramId);
            bindIncentiveDateRangeParameters(query);
        }

        private void bindPackingProductIdsParameters(Query query) {
            query.setParameter("languageId", languageId);
            query.setParameter("packingProductIds", packingProductIds);
            query.setParameter("incentiveProgramId", incentiveProgramId);
            bindIncentiveDateRangeParameters(query);
        }
    }

    private String getPromotionPriceResourceSqlQuery() {
        return "select " +
                "   p.packing_price_id id, " +
                "   p.packing_product_id packingProductId, " +
                "   price price, " +
                "   from_date fromDate, " +
                "   to_date toDate, " +
                "   price_type priceType, " +
                "   currency_code currencyCode " +
                "from " +
                "   packing_price p " +
                "where " +
                "   p.packing_product_id = :packingProductId " +
                "   and p.price_type = 3 " +
                "   and ( (curdate() between from_date and to_date) or curdate() < from_date ) " +
                "order by p.from_date asc";
    }

    private String getPackingProductResourceSqlQuery() {
        return "SELECT " +
                "  p.packing_product_id id, " +
                "  p.product_id productId, " +
                "  p.packing_type_id packingTypeId, " +
                "  p.code code, " +
                "  p.name name, " +
                "  p.packing_url imageUrl, " +
                "  (" +
                "    SELECT " +
                "      p1.price " +
                "    FROM " +
                "      packing_price p1 " +
                "    WHERE " +
                "      p.packing_product_id = p1.packing_product_id " +
                "      AND p1.price_type = 1 " +
                "      AND p1.created_time = (" +
                "        SELECT " +
                "          max(created_time) " +
                "        FROM " +
                "          packing_price " +
                "        WHERE " +
                "          p.packing_product_id = packing_product_id " +
                "          AND price_type = 1" +
                "      )" +
                "  ) originalPrice, " +
                "  (" +
                "    SELECT " +
                "      p1.price " +
                "    FROM " +
                "      packing_price p1 " +
                "    WHERE " +
                "      p.packing_product_id = p1.packing_product_id " +
                "      AND p1.price_type = 2 " +
                "      AND p1.created_time = (" +
                "        SELECT " +
                "          max(created_time) " +
                "        FROM " +
                "          packing_price " +
                "        WHERE " +
                "          p.packing_product_id = packing_product_id " +
                "          AND price_type = 2" +
                "      )" +
                "  ) salePrice, " +
                "  p.market_price marketPrice, " +
                "  p.uom uom " +
                "FROM " +
                "  packing_product p " +
                "WHERE " +
                "  p.product_id = :productId";
    }

    private String getSetClause(Integer type) {
        if (type.equals(1))
            return setAmountApply();
        else if (type.equals(2))
            return setReduceClause();
        else
            return setPercentClause();
    }

    private String setAmountApply() {
        return " set pp.promotion_sale_price = :amountApply, " +
                " pp.from_date = :startDate, " +
                " pp.to_date = :endDate, " +
                " pp.discount_percent = (100 - (:amountApply /  pp.price * 100)) ";
    }

    private String setReduceClause() {
        return " set pp.promotion_sale_price = (pp.price - :amountApply), " +
                " pp.from_date = :startDate, " +
                " pp.to_date = :endDate, " +
                " pp.discount_percent = (100 - ((pp.price - :amountApply) /  pp.price * 100)) ";
    }

    private String setPercentClause() {
        return " set pp.promotion_sale_price = pp.price - (pp.price * :amountApply / 100), " +
                " pp.from_date = :startDate, " +
                " pp.to_date = :endDate, " +
                " pp.discount_percent = :amountApply ";
    }

    private String updateClause() {
        return " update packing_product pp ";
    }

    private String whereClause() {
        return " where " +
                " pp.packing_product_id in " +
                " ( " +
                " select " +
                "   pp.packing_product_id " +
                " from " +
                "   product p inner join packing_product pp on p.product_id = pp.product_id " +
                " where " +
                "   p.category_id = :categoryId and " +
                "   p.product_id in :productIds and " +
                "   pp.packing_type_id = :packingTypeId " +
                " ) ";
    }

    private class PackingProductCategoryHelper {
        private Pageable pageable;
        private Long langId;
        private Long categoryId;

        PackingProductCategoryHelper(Pageable pageable, Long langId, Long categoryId) {
            this.pageable = pageable;
            this.langId = langId;
            this.categoryId = categoryId;
        }

        @SuppressWarnings("unchecked")
        Page<ProductDto> invoke() {
            List<Long> categoryIds = findCategoryHierarchyById(categoryId);
            StringBuilder sql = new StringBuilder();
            sql.append("select ");
            sql.append(findAllColumns());
            sql.append("FROM ");
            sql.append(getSqlSelectFirstPacking());
            sql.append(" JOIN product_description pd ON pd.product_id = a.product_id " +
                    "    LEFT JOIN packing_type pt ON pt.packing_type_id = a.PACKING_TYPE_ID " +
                    " WHERE " +
                    "    pd.LANGUAGE_ID = :langId AND a.CATEGORY_ID in :categoryIds");
            Query nativeQuery = em.createNativeQuery(sql.toString(), "ProductDtoMappingAll");
            nativeQuery.setParameter("langId", langId);
            nativeQuery.setParameter("categoryIds", categoryIds);
            nativeQuery.setFirstResult(pageable.getPageSize() * pageable.getPageNumber());
            nativeQuery.setMaxResults(pageable.getPageSize() * pageable.getPageNumber() + pageable.getPageSize());

            Query nativeQueryCount = em.createNativeQuery(sql.toString().replace(findAllColumns(), " count(*) "));
            nativeQueryCount.setParameter("langId", langId);
            nativeQueryCount.setParameter("categoryIds", categoryIds);
            BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();

            return new PageImpl<>(nativeQuery.getResultList(), pageable, total.longValue());
        }

        private List<Long> findCategoryHierarchyById(Long categoryId) {
            List<CategoryHierarchy> categories = categoryRepository.getCategoryHierarchyById(categoryId);
            List<Long> categoryIds = categories.stream().map(CategoryHierarchy::getId).collect(Collectors.toList());
            if (categoryIds.isEmpty())
                categoryIds.add(categoryId);
            return categoryIds;
        }
    }
}
