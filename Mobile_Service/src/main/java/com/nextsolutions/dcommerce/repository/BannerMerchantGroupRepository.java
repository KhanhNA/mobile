package com.nextsolutions.dcommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.BannerMerchantGroup;

public interface BannerMerchantGroupRepository extends JpaRepository<BannerMerchantGroup, Long>, JpaSpecificationExecutor<BannerMerchantGroup> {

    void deleteAllByBannerId(Long id);
}
