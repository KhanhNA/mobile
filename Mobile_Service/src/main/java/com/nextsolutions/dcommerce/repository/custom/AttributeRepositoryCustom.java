package com.nextsolutions.dcommerce.repository.custom;

import com.nextsolutions.dcommerce.shared.dto.resource.AttributeResource;

import java.util.List;

public interface AttributeRepositoryCustom {
    List<AttributeResource> findAllByLanguageId(Integer languageId);

    List<AttributeResource> findAllMerchantGroupAttributesAvailable(Integer languageId);

    List<AttributeResource> findAllProductAttributesAvailable(Integer languageId);
}
