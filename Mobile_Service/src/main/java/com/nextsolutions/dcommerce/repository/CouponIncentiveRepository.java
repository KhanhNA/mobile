package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.CouponIncentive;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CouponIncentiveRepository extends JpaRepository<CouponIncentive, Long> {
    Optional<CouponIncentive> findByCode(String code);
}
