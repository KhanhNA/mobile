package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.repository.custom.ManufacturerRepositoryCustom;
import com.nextsolutions.dcommerce.shared.dto.resource.ManufacturerResource;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static com.nextsolutions.dcommerce.utils.StringUtils.notEmpty;

@Repository
@RequiredArgsConstructor
public class ManufacturerRepositoryCustomImpl implements ManufacturerRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public Page<ManufacturerResource> findAll(ManufacturerResource resource, Pageable pageable) {

        String selectClause = "SELECT m.MANUFACTURER_ID id, m.code code, md.name name, md.LANGUAGE_ID languageId, m.DATE_CREATED createdDate, " +
                " m.TEL tel, m.EMAIL email, md.ADDRESS address, m.status status, m.action action, m.tax_code taxCode ";
        String selectCountClause = "SELECT count(1) ";
        String fromClause = " FROM manufacturer m JOIN manufacturer_description md ON m.MANUFACTURER_ID = md.MANUFACTURER_ID ";

        StringBuilder whereClause = new StringBuilder();

        whereClause.append(" WHERE md.LANGUAGE_ID = :languageId and m.is_active != 0");
        String code = resource.getCode();
        String name = resource.getName();
        LocalDateTime createdDate = resource.getCreatedDate();
        String tel = resource.getTel();
        String email = resource.getEmail();
        String address = resource.getAddress();
        Long languageId = resource.getLanguageId();
        Long status = resource.getStatus();
        Long action = resource.getAction();
        String taxCode = resource.getTaxCode();

        if (notEmpty(code)) {
            whereClause.append(" AND lower(m.code) like lower(concat('%',:code,'%')) ");
        }

        if (notEmpty(name)) {
            whereClause.append(" AND lower(md.name) like lower(concat('%',:name,'%')) ");
        }

        if (createdDate != null) {
            whereClause.append(" AND m.DATE_CREATED = date(:createdDate) ");
        } else {
            if (createdDate != null) {
                whereClause.append(" AND date(m.DATE_CREATED) = date(:createdDate) ");

            }
        }
        if (notEmpty(tel)) {
            whereClause.append(" AND lower(m.TEL) like lower(concat('%',:tel,'%')) ");
        }
        if (notEmpty(email)) {
            whereClause.append(" AND lower(m.EMAIL) like lower(concat('%',:email,'%')) ");
        }
        if (notEmpty(address)) {
            whereClause.append(" AND lower(md.ADDRESS   ) like lower(concat('%',:address,'%')) ");
        }
        if (status != null) {
            whereClause.append(" AND m.STATUS = :status");
        }
        if (action != null) {
            whereClause.append(" AND m.ACTION = :action");
        }
        if(notEmpty(taxCode)) {
            whereClause.append(" AND m.TAX_CODE = :taxCode");
        }

        String nativeQueryString = selectClause + fromClause + whereClause.toString() + SpringUtils.getOrderBy(pageable);
        Query nativeQuery = em.createNativeQuery(nativeQueryString, "ManufacturerDtoMapping");


        String nativeQueryCountString = selectCountClause + fromClause + whereClause.toString();
        Query nativeQueryCount = em.createNativeQuery(nativeQueryCountString);

        if (notEmpty(code)) {
            nativeQuery.setParameter("code", code);
            nativeQueryCount.setParameter("code", code);
        }
        if (notEmpty(name)) {
            nativeQuery.setParameter("name", name);
            nativeQueryCount.setParameter("name", name);
        }


        if (createdDate != null) {
            nativeQuery.setParameter("createdDate", parseDatetoString(createdDate));

            nativeQueryCount.setParameter("createdDate", parseDatetoString(createdDate));

        } else {
            if (createdDate != null) {
                nativeQuery.setParameter("createdDate", parseDatetoString(createdDate));
                nativeQueryCount.setParameter("createdDate", parseDatetoString(createdDate));
            }

        }
        if (notEmpty(tel)) {
            nativeQuery.setParameter("tel", tel);
            nativeQueryCount.setParameter("tel", tel);
        }
        if (notEmpty(email)) {
            nativeQuery.setParameter("email", email);
            nativeQueryCount.setParameter("email", email);
        }
        if (notEmpty(address)) {
            nativeQuery.setParameter("address", address);
            nativeQueryCount.setParameter("address", address);
        }
        if (status != null) {
            nativeQuery.setParameter("status", status);
            nativeQueryCount.setParameter("status", status);
        }
        if (action != null) {
            nativeQuery.setParameter("action", action);
            nativeQueryCount.setParameter("action", action);
        }
        if(notEmpty(taxCode)) {
            nativeQuery.setParameter("taxCode", taxCode);
            nativeQueryCount.setParameter("taxCode", taxCode);
        }
        if (languageId == null) {
            nativeQuery.setParameter("languageId", 1);
            nativeQueryCount.setParameter("languageId", 1);
        } else {
            nativeQuery.setParameter("languageId", languageId);
            nativeQueryCount.setParameter("languageId", languageId);
        }

        nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        nativeQuery.setMaxResults(pageable.getPageSize());

        List<ManufacturerResource> resultList = nativeQuery.getResultList();
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();
        return new PageImpl<>(resultList, pageable, total.longValue());
    }
    private String parseDatetoString( LocalDateTime dateTime) {
        String str = dateTime.toString();
        return str.substring(0,10);
    }

}
