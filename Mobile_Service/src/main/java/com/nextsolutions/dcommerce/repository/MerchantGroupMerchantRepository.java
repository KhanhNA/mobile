package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.MerchantGroupMerchant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MerchantGroupMerchantRepository extends JpaRepository<MerchantGroupMerchant, Long> {
    List<MerchantGroupMerchant> findByMerchantGroupId(Long id);
}
