package com.nextsolutions.dcommerce.repository;

import com.nextsolutions.dcommerce.model.AutoIncrement;
import com.nextsolutions.dcommerce.repository.custom.AutoIncrementJpaRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutoIncrementJpaRepository extends JpaRepository<AutoIncrement, Long>, AutoIncrementJpaRepositoryCustom {
    AutoIncrement findByCode(String code);
}
