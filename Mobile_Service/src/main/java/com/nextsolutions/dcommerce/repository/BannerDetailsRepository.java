package com.nextsolutions.dcommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.nextsolutions.dcommerce.model.BannerDetails;

public interface BannerDetailsRepository extends JpaRepository<BannerDetails, Long>, JpaSpecificationExecutor<BannerDetails> {

    void deleteAllByBannerId(Long id);
}
