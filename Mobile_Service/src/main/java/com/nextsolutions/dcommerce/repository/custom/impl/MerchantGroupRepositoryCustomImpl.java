package com.nextsolutions.dcommerce.repository.custom.impl;

import com.nextsolutions.dcommerce.repository.custom.MerchantGroupRepositoryCustom;
import com.nextsolutions.dcommerce.ui.model.request.MerchantGroupQueryRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupMerchantResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

@Repository
public class MerchantGroupRepositoryCustomImpl implements MerchantGroupRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    @SuppressWarnings("unchecked")
    public Page<MerchantGroupResource> findByQuery(MerchantGroupQueryRequestModel merchantGroupQueryRequestModel, Pageable pageable) {
        String name = merchantGroupQueryRequestModel.getName();
        String code = merchantGroupQueryRequestModel.getCode();
        Integer languageId = merchantGroupQueryRequestModel.getLanguageId();

        StringBuilder sb = new StringBuilder();
        sb.append(" FROM merchant_group mg JOIN merchant_group_description mgd ON mg.id = mgd.merchant_group_id ");
        sb.append(" WHERE mg.status <> 0 AND mgd.language_id = :languageId ");

        if (StringUtils.notEmpty(code)) {
            sb.append(" AND LOWER(mg.code) LIKE CONCAT('%', LOWER(:code), '%') ");
        }
        if (StringUtils.notEmpty(name)) {
            sb.append(" AND LOWER(mgd.name) LIKE CONCAT('%', LOWER(:name), '%') ");
        }

        String sql = "SELECT mg.id id, mg.code code, mgd.name name,0 isSelect " + sb.toString() + SpringUtils.getOrderBy(pageable);
        Query query = em.createNativeQuery(sql, "MerchantGroupResource");
        Query queryCount = em.createNativeQuery("SELECT count(1) " + sb.toString());

        query.setParameter("languageId", languageId);
        queryCount.setParameter("languageId", languageId);

        if (StringUtils.notEmpty(code)) {
            query.setParameter("code", code);
            queryCount.setParameter("code", code);
        }

        if (StringUtils.notEmpty(name)) {
            query.setParameter("name", name);
            queryCount.setParameter("name", name);
        }

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        List<MerchantGroupResource> resultList = query.getResultList();
        BigInteger total = (BigInteger) queryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<MerchantGroupMerchantResource> findAllMerchantGroupMerchantById(Long id) {
        return (List<MerchantGroupMerchantResource>) em.createNativeQuery("SELECT " +
                        "mgm.id id, mgm.merchant_id merchantId, mgm.merchant_group_id merchantGroupId " +
                        "FROM merchant_group_merchant mgm " +
                        "   JOIN merchant m ON m.merchant_id = mgm.merchant_id " +
                        "   JOIN merchant_group mg ON mg.id = mgm.merchant_group_id " +
                        "WHERE mgm.merchant_group_id = :id AND m.status = 1 AND mg.status <> 0",
                "MerchantGroupMerchantResource")
                .setParameter("id", id)
                .getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<MerchantGroupResource> findAllByNativeSql(String keyword, Pageable pageable, Integer languageId) {
        StringBuilder sb = new StringBuilder();
        sb.append(" FROM merchant_group mg JOIN merchant_group_description mgd ON mg.id = mgd.merchant_group_id ");
        sb.append(" WHERE mg.status <> 0 AND mgd.language_id = :languageId ");

        if (StringUtils.notEmpty(keyword)) {
            sb.append(" AND (LOWER(mg.code) LIKE CONCAT('%', LOWER(:keyword), '%') OR LOWER(mgd.name) LIKE CONCAT('%', LOWER(:keyword), '%')) ");
        }

        String sql = "SELECT mg.id id, mg.code code, mgd.name name, 0 isSelect " + sb.toString() + SpringUtils.getOrderBy(pageable);
        Query query = em.createNativeQuery(sql, "MerchantGroupResource");
        Query queryCount = em.createNativeQuery("SELECT count(1) " + sb.toString());

        query.setParameter("languageId", languageId);
        queryCount.setParameter("languageId", languageId);

        if (StringUtils.notEmpty(keyword)) {
            query.setParameter("keyword", keyword);
            queryCount.setParameter("keyword", keyword);
        }

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        List<MerchantGroupResource> resultList = query.getResultList();
        BigInteger total = (BigInteger) queryCount.getSingleResult();

        return new PageImpl<>(resultList, pageable, total.longValue());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<MerchantGroupResource> findAllByNativeSql(String keyword, Integer languageId, Long merchantId) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT mg.id id, mg.code code, mgd.name name, ");
        sb.append(" CASE" +
                " WHEN mgm.merchant_group_id is null THEN 0 " +
                " ELSE 1  END isSelect ");
        sb.append(" FROM merchant_group mg JOIN merchant_group_description mgd ON mg.id = mgd.merchant_group_id ");
        sb.append(" left JOIN merchant_group_merchant mgm on mgm.MERCHANT_GROUP_ID = mg.ID and mgm.MERCHANT_ID= :merchantId ");
        sb.append(" WHERE mg.status <> 0 AND mgd.language_id = :languageId ");

        if (StringUtils.notEmpty(keyword)) {
            sb.append(" AND (LOWER(mg.code) LIKE CONCAT('%', LOWER(:keyword), '%') OR LOWER(mgd.name) LIKE CONCAT('%', LOWER(:keyword), '%')) ");
        }

        Query query = em.createNativeQuery(sb.toString(), "MerchantGroupResource");

        query.setParameter("languageId", languageId);
        query.setParameter("merchantId", merchantId);

        if (StringUtils.notEmpty(keyword)) {
            query.setParameter("keyword", keyword);
        }
        return query.getResultList();
    }
}
