package com.nextsolutions.dcommerce.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ApiError {
    private long timestamp = new Date().getTime();
    private String code; // only for bienpv
    private int status;
    private String path;
    private String message;
    private Object developerMessage;
    private Object trace;

    public ApiError(int status, String url, String message) {
        this.status = status;
        this.code = String.valueOf(status);
        this.path = url;
        this.message = message;
    }

    public ApiError(int status, String url, String message, Object trace) {
        this.status = status;
        this.code = String.valueOf(status);
        this.path = url;
        this.message = message;
        this.trace = trace;
    }
}
