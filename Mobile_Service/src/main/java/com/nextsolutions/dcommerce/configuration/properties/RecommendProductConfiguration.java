package com.nextsolutions.dcommerce.configuration.properties;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "m-store.api.recommend-product")
public class RecommendProductConfiguration {
    private String accessKey;
    private int processSize = 50;

    @NestedConfigurationProperty
    private QueueName queueName;
    private String api;
    private String apiPrediction;

    public static class QueueName {
        public String order;

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }
    }
}
