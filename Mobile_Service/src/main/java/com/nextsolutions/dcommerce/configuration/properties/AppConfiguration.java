package com.nextsolutions.dcommerce.configuration.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "m-store")
public class AppConfiguration {
    private String uploadPath = "upload";
    private String attachmentPdfPath = "pdf";

    public String getFullAttachmentPdfPath() {
        return uploadPath + attachmentPdfPath;
    }
}
