package com.nextsolutions.dcommerce.configuration.activemq;

import com.nextsolutions.dcommerce.configuration.properties.RecommendProductConfiguration;
import com.nextsolutions.dcommerce.service.activemq.RecommendService;
import com.nextsolutions.dcommerce.shared.dto.OrderPredictionDTO;
import com.nextsolutions.dcommerce.shared.dto.recommend.RecommendProduct;
import lombok.RequiredArgsConstructor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.core.JmsTemplate;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;

@SuppressWarnings("unchecked")
//@Component
@RequiredArgsConstructor
public class ScheduledTasks {

	private static final Logger log = LogManager.getLogger(ScheduledTasks.class);
    private final RecommendService recommendService;
    private final JmsTemplate jmsTemplate;
    private final RecommendProductConfiguration recommendProductConfiguration;

    //@Scheduled(cron = "0 */5 * * * *")
    public void scheduleTaskGetOrderFromQueue() {
        log.info("START SCHEDULE");
        log.info("TIME: {}", new Date());
        try {
            OrderPredictionDTO order;
            int queueSize = countPendingMessages();
            int listSize = recommendProductConfiguration.getProcessSize();
            if (queueSize < recommendProductConfiguration.getProcessSize()) {
                listSize = queueSize;
            }
            int count = 0;
            while (count < listSize) {
                order = (OrderPredictionDTO) jmsTemplate.receiveAndConvert(recommendProductConfiguration.getQueueName().getOrder());
                if (order == null) {
                    break;
                }
                sendOrderToPrediction(order);
                count++;
            }

        } catch (Exception e) {
            log.error("*******************************************");
            log.error(e.getMessage());
        }

        log.info("END SCHEDULE");
        log.info("TIME: {}", new Date());
    }

    private void sendOrderToPrediction(OrderPredictionDTO order) {

        String merchantId = order.getMerchantId().toString();
        Date date = Date.from(order.getOrderDate().atZone(ZoneId.systemDefault()).toInstant());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        String eventTime = simpleDateFormat.format(date);
        order.getOrderPackings().forEach(orderPackingDTO -> {
            RecommendProduct recommendProduct = RecommendProduct.builder()
                    .event("buy")
                    .entityType("user")
                    .entityId(merchantId)
                    .targetEntityType("item")
                    .targetEntityId(orderPackingDTO.getPackingProductId().toString())
                    .eventTime(eventTime)
                    .build();
            if (recommendService.recommend(recommendProduct)) {
                log.info("Push to AI success");
            } else {
                log.info("Push to AI false. Something went wrong");
            }
        });
    }

    private int countPendingMessages() {
        Integer totalPendingMessages = null;
        if (this.jmsTemplate.getDefaultDestinationName() != null) {
            totalPendingMessages = this.jmsTemplate.browse(this.jmsTemplate.getDefaultDestinationName(), (session, browser) -> Collections.list(browser.getEnumeration()).size());
        }
        return totalPendingMessages == null ? 0 : totalPendingMessages;
    }

}
