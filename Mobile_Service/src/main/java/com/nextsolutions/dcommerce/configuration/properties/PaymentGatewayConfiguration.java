package com.nextsolutions.dcommerce.configuration.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "m-store.api.payment-gateway")
public class PaymentGatewayConfiguration {
    private String baseUrl;
    private String clientsUrl;
    private String transferUrl;
}
