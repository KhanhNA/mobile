package com.nextsolutions.dcommerce.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "m-store.api.oauth")
@Data
public class OAuthConfiguration {
    private String baseUrl;
    private String userUrl;
    private String userActivationUrl;
    private String userPasswordChangingUrl;
    private String userDeactivationUrl;
    private String userStatusUrl;
    private List<Long> merchantRoles;
    private List<Long> merchantL2Roles;
    private List<Long> salesManRoles;
    private Long distributorRole;
    private String distributorCode;
    private Long distributorId;
    private Long distributorWalletId;
    private String resetPasswordUrl;
}
