//package com.nextsolutions.dcommerce.configuration.aop;
//
//import java.util.Arrays;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.AfterThrowing;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//@Aspect
//public class LoggingAspect {
//
//	private static final Logger log = LogManager.getLogger(LoggingAspect.class);
//
//	@Pointcut("within(com.nextsolutions.dcommerce.ui.controller..*) "
//			+ "|| within(com.nextsolutions.dcommerce.repository..*) "
//			+ "|| within(com.nextsolutions.dcommerce.service..*)")
//	public void loggingPointcut() {
//	}
//
//	@AfterThrowing(pointcut = "loggingPointcut()", throwing = "e")
//	public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
//		log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
//				joinPoint.getSignature().getName(), e.getCause() != null ? e.getCause() : "NULL");
//	}
//
//	@Around("loggingPointcut()")
//	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
//		if (log.isDebugEnabled()) {
//			log.debug("Enter: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
//					joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
//		}
//		try {
//			Object result = joinPoint.proceed();
//			if (log.isDebugEnabled()) {
//				log.debug("Exit: {}.{}() with result = {}", joinPoint.getSignature().getDeclaringTypeName(),
//						joinPoint.getSignature().getName(), result);
//			}
//			return result;
//		} catch (IllegalArgumentException e) {
//			log.error("Illegal argument: {} in {}.{}()", Arrays.toString(joinPoint.getArgs()),
//					joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
//
//			throw e;
//		}
//	}
//
//}
