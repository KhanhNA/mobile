package com.nextsolutions.dcommerce.configuration;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

@Configuration
@EnableOAuth2Sso
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(ConfigConstant.PUBLIC_MATCHERS);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http//.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .anonymous().disable().requestMatcher(new BasicRequestMatcher())
                .authorizeRequests()
                .antMatchers(ConfigConstant.PUBLIC_MATCHERS).permitAll()
                .antMatchers("/**").authenticated()
//                .and().exceptionHandling().authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
                .and().httpBasic().disable()
                .cors().and().csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .formLogin().permitAll();
    }

    protected static class BasicRequestMatcher implements RequestMatcher {
        @Override
        public boolean matches(HttpServletRequest request) {
            String auth = request.getHeader("Authorization");
            return ((auth != null) && auth.toUpperCase().startsWith("BEARER"));
        }
    }
}
