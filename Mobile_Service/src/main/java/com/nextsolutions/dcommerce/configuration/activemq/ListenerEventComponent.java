package com.nextsolutions.dcommerce.configuration.activemq;

import com.nextsolutions.dcommerce.configuration.activemq.event.MessageEvent;
import com.nextsolutions.dcommerce.configuration.properties.RecommendProductConfiguration;
import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import com.nextsolutions.dcommerce.shared.dto.OrderPredictionDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;

//@Component
@RequiredArgsConstructor
public class ListenerEventComponent {

    private final RecommendProductConfiguration recommendProductConfiguration;
    private final JmsTemplate jmsTemplate;

    @Async
    @EventListener
    public void doorBellEventListener(MessageEvent messageEvent) throws InterruptedException {
        try {
            OrderDto order = messageEvent.getOrderDto();
            System.out.println("Start push");
            OrderPredictionDTO orderPredictionDTO = OrderPredictionDTO
                    .builder()
                    .merchantId(order.getMerchantId())
                    .orderDate(order.getOrderDate())
                    .orderPackings(order.getOrderPackings())
                    .build();
            jmsTemplate.convertAndSend(recommendProductConfiguration.getQueueName().getOrder(), orderPredictionDTO);
            System.out.println("end push");
        } catch (Exception e) {
            System.out.println("push error");
            System.out.println(e.getMessage());
        }
    }
}
