package com.nextsolutions.dcommerce.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
@Order(3)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private static final Logger log = LogManager.getLogger(ResourceServerConfig.class);

    @Override
    public void configure(HttpSecurity http) {
        try {
            http.anonymous().disable().requestMatcher(new WebSecurityConfigurer.BasicRequestMatcher()).authorizeRequests()
                    .antMatchers(ConfigConstant.PUBLIC_MATCHERS).permitAll()
                    .antMatchers("/**").authenticated().and()
                    .httpBasic().disable();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
