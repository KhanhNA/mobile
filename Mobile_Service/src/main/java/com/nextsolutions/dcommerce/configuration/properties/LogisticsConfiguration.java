package com.nextsolutions.dcommerce.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "m-store.api.logistics")
@Data
public class LogisticsConfiguration {
    private String baseUrl;
    private String merchantOrdersUrl;
}
