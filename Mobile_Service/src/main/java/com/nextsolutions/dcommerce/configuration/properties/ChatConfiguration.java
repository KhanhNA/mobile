package com.nextsolutions.dcommerce.configuration.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "m-store.api.chat")
public class ChatConfiguration {
    private String baseUrl;
    private String username;
    private String password;
    private String vhost;
    private String prid;
}
