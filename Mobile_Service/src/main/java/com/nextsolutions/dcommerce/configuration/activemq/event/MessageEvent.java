package com.nextsolutions.dcommerce.configuration.activemq.event;

import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class MessageEvent extends ApplicationEvent {
    private OrderDto orderDto;

    public MessageEvent(Object source, OrderDto orderDto) {
        super(source);
        this.orderDto = orderDto;

    }
}
