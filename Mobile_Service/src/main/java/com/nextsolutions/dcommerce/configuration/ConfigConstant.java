package com.nextsolutions.dcommerce.configuration;

public interface ConfigConstant {
    String[] PUBLIC_MATCHERS = {
            "/oauth/**",
            "/swagger**/**",
            "/webjars/**",
            "/v2/api-docs",
            "/files/**",
            "/filesPdf/**",
            "/download/files/**",
            "/api/v1/merchants/inputCode**",
            "/api/v1/merchants/createMerchant**",
            "/api/v1/notification/getDiscussion**",
            "/api/v1/files/**",
            "/api/v1/filesPdf/**",
            "/api/v1/download/files/**",
            "/api/v1/static/**",
            "/api/v1/fake/**",
            "/api/v1/notification/getList/**"
    };
}
