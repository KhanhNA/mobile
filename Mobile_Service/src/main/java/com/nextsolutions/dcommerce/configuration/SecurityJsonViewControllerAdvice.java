package com.nextsolutions.dcommerce.configuration;

import com.nextsolutions.dcommerce.model.JsonEntityViewer;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMappingJacksonResponseBodyAdvice;

import java.util.Collection;

@RestControllerAdvice
public class SecurityJsonViewControllerAdvice extends AbstractMappingJacksonResponseBodyAdvice {

	@Override
	protected void beforeBodyWriteInternal(MappingJacksonValue bodyContainer, MediaType contentType,
			MethodParameter returnType, ServerHttpRequest request, ServerHttpResponse response) {
		//TODO: Nho mo lai phan quyen
//		if ((SecurityContextHolder.getContext().getAuthentication() != null)
//				&& (SecurityContextHolder.getContext().getAuthentication().getAuthorities() != null)) {
//			Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication()
//					.getAuthorities();
//				Object value = bodyContainer.getValue();
					Class<?> itf = JsonEntityViewer.MapRequest.get(request.getURI().getPath());
					bodyContainer.setSerializationView(itf == null ? JsonEntityViewer.Human.Summary.class : itf);
//		}
	}
}