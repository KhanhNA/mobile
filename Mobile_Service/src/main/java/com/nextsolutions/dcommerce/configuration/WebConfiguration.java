package com.nextsolutions.dcommerce.configuration;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
public class WebConfiguration {

    private final AppConfiguration appConfiguration;

    public WebConfiguration(AppConfiguration appConfiguration) {
        this.appConfiguration = appConfiguration;
    }

    @Bean
    CommandLineRunner createUploadFolder() {
        return args -> {
            createNonExistingFolder(appConfiguration.getUploadPath());
            createNonExistingFolder(appConfiguration.getFullAttachmentPdfPath());
        };
    }

    private void createNonExistingFolder(String path) throws IOException {
        File folder = new File(path);
        boolean folderExists = folder.exists() && folder.isDirectory();
        if (!folderExists) {
            FileUtils.forceMkdir(folder);
        }
    }

}
