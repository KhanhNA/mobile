package com.nextsolutions.dcommerce.utils;

import org.springframework.data.domain.Pageable;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.stream.Collectors;

public class SpringUtils {

    public static String getOrderBy(Pageable pageable) {
        String sortTerm = pageable.getSort()
                .stream()
                .map(o -> String.format("%s %s", o.getProperty(), o.getDirection()))
                .collect(Collectors.joining(" "));
        if (!sortTerm.isEmpty()) {
            return String.format(" ORDER BY %s ", sortTerm);
        }
        return "";
    }

    public static String whereDate(String fromDate, String toDate, String actionDate) {
        return " DATE(" + actionDate + ") >= DATE('" + fromDate + "') AND DATE(" + actionDate + ") <= DATE('" + toDate + "')  ";
    }

    public static String getCurrentUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public static Calendar getCurrentTime() {
        return Calendar.getInstance();
    }

    public static String getCurrentLocalDateTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }


}
