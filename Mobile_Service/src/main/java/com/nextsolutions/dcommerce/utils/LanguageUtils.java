package com.nextsolutions.dcommerce.utils;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.LinkedHashMap;
import java.util.Map;

public class LanguageUtils {

    private static final Map<String, Long> languageIdMap = new LinkedHashMap<>();
    private static final Map<Long, String> languageCodeMap = new LinkedHashMap<>();

    static {
        languageIdMap.put("vi", 1L);
        languageIdMap.put("en", 2L);
        languageIdMap.put("my", 3L);

        languageCodeMap.put(1L, "vi");
        languageCodeMap.put(2L, "en");
        languageCodeMap.put(3L, "my");
    }

    public static Long getLanguageId(String locale) {
        return languageIdMap.getOrDefault(locale, 1L);
    }

    public static String getLanguageCode(Long id) {
        return languageCodeMap.get(id);
    }

    public static Long getCurrentLanguageId() {
        return getLanguageId(LocaleContextHolder.getLocale().getLanguage());
    }
}
