package com.nextsolutions.dcommerce.utils;

import java.util.UUID;

public class StringUtils {

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty() || str.trim().isEmpty();
    }

    public static boolean notEmpty(String str) {
        return !isEmpty(str);
    }

    public static String randomUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
