package com.nextsolutions.dcommerce.utils;

import com.nextsolutions.dcommerce.service.annotation.ColumnMapper;
import com.nextsolutions.dcommerce.shared.dto.BaseDto;
import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Utils {
    public static boolean isEmpty(List list) {
        return list == null || list.isEmpty();
    }

    public static boolean isNotEmpty(List list) {
        return list != null && !list.isEmpty();
    }

    public static List safe(List other) {

        return other == null ? Collections.EMPTY_LIST : other;
    }

    public static Set safe(Set other) {

        return other == null ? Collections.EMPTY_SET : other;
    }

    public static Map safe(Map other) {

        return other == null ? Collections.EMPTY_MAP : other;
    }

    public static LocalDateTime getLocalDatetime() {
        return LocalDateTime.now();
    }

    public static String getOrderNo(Merchant merchant, OrderDto orderDto) {
        return String.format("%s%d", merchant.getMerchantCode(), orderDto.getOrderId());
    }

    public static String getOrderNo(Merchant merchant, OrderV2Dto orderDto) {
        return String.format("%s%d", merchant.getMerchantCode(), orderDto.getOrderId());
    }

    public static String getMessage(HttpStatusCodeException httpStatusCodeException) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(httpStatusCodeException.getResponseBodyAsString());
        } catch (JSONException e) {
            e.getStackTrace();
            return "";
        }
        String message = "";
        try {
            message = jsonObject.getString("message");
        } catch (JSONException e) {
            e.getStackTrace();
        }
        return message;
    }

//	public static void getData(Object src, Object dest) throws ExceptionHandler {
//		Field[] fields = src.getClass().getDeclaredFields();
//
//		String methodName;
//		java.lang.reflect.Method method;
//
//		for(Field field: fields) {
//
//			field.setAccessible(true);
//			Object value = field.get(src);
//
//
//
//			methodName = StringUtils.capitalize(field.getName());
//
//			if(methodName == null) {
//				continue;
//
//			}
//			methodName = "set" + methodName;
//			try {
//				method = dest.getClass().getMethod(methodName, field.getType());
//
//				method.invoke(dest, value);
//			}catch (IllegalArgumentException e) { e.printStackTrace(); }
//			  catch (IllegalAccessException e) { e.printStackTrace(); }
//			  catch (InvocationTargetException e) { e.printStackTrace(); }
//			catch (ExceptionHandler e) { e.printStackTrace(); }
//		}
//	}

    public static void copyNonNullProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    private static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    public static <T> T getData(Object src, Class<T> clazz) throws Exception {
        return getData(src, clazz, null);
    }

    public static <T> T getData(Object src, T dest) throws Exception {
        return getData(src, dest, null);
    }

    public static <T> T getData(Object src, T dest, HashMap<String, Field> destFields) throws Exception {
//		T dest = clazz.newInstance();
        Field[] fields = src.getClass().getDeclaredFields();
        Field fieldDest;
        if (destFields == null) { //== null nghia la chi dung cho noi tai trong ham
            destFields = new HashMap<>();
        }
        for (Field field : dest.getClass().getDeclaredFields()) {
            destFields.put(field.getName(), field);
        }

        for (Field field : fields) {
            int modifier = field.getModifiers();
            if (Modifier.isStatic(modifier) || Modifier.isFinal(modifier)) {
                continue;
            }

            field.setAccessible(true);
            Object value = field.get(src);


            fieldDest = destFields.get(field.getName());
            if (fieldDest == null || fieldDest.getType() != field.getType()) {
                continue;
            }
            fieldDest.setAccessible(true);
            fieldDest.set(dest, value);


        }
        return dest;
    }

    public static <T> T getData(Object src, Class<T> clazz, HashMap<String, Field> destFields) throws Exception {
//		Field[] fields = src.getClass().getDeclaredFields();


        T dest = clazz.newInstance();
        return getData(src, dest, destFields);
//		Field fieldDest;
//		if(destFields == null) { //== null nghia la chi dung cho noi tai trong ham
//			destFields = new HashMap<>();
//		}
//		for(Field field: dest.getClass().getDeclaredFields()){
//			destFields.put(field.getName(), field);
//		}
//
//		for(Field field: fields) {
//			int modifier = field.getModifiers();
//			if(Modifier.isStatic(modifier) || Modifier.isFinal(modifier)){
//				continue;
//			}
//
//			field.setAccessible(true);
//			Object value = field.get(src);
//
//
//			fieldDest = destFields.get(field.getName());
//			if (fieldDest == null || fieldDest.getType() != field.getType()) {
//				continue;
//			}
//			fieldDest.setAccessible(true);
//			fieldDest.set(dest, value);
//
//
//		}
//		return dest;
    }

    public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }

        return fields;
    }

    public static StringBuilder buildWhere(Object obj, String alias, HashMap params) throws Exception {
        StringBuilder builder = new StringBuilder();
//		Field[] fields = obj.getClass().getDeclaredFields();

        List<Field> fields = new ArrayList<>();
        getAllFields(fields, obj.getClass());
        String methodName;
        java.lang.reflect.Method method;
        int modifier;
        if (!isEmpty(alias) && !alias.endsWith(".")) {
            alias = alias.trim() + ".";
        }
        for (Field field : fields) {

            modifier = field.getModifiers();
            if (Modifier.isFinal(modifier) || Modifier.isStatic(modifier) || field.getName().equalsIgnoreCase("_displayCols")) {
                continue;
            }
            field.setAccessible(true);
            Object value = field.get(obj);
            if (value == null || isEmpty(value)) {
                continue;
            }
            if (field.getName().equalsIgnoreCase("ORDER_BY")) {
                params.put("ORDER_BY", value);
                continue;
            }

            if (field.getName().equalsIgnoreCase("_TEXT")) {
                Map m = ((BaseDto) obj).getTextCondition(alias);
                builder.append(" and (").append((String) m.get("_CONDITION_")).append(") ");
                m.remove("_CONDITION_");
                if (m != null) {
                    params.putAll(m);
                }
                continue;
            }


            if (value != null) {
                if (value instanceof String) {
                    builder.append(" and (").append(alias).append(field.getName()).append(" LIKE CONCAT('%',:").append(field.getName()).append(",'%'))");
                } else {
                    builder.append(" and (").append(alias).append(field.getName()).append(" =:").append(field.getName()).append(")");
                }
                params.put(field.getName(), value);
            }

        }
        return builder;
    }

    // NMQ
    // dung tao sql vs dto
    public static StringBuilder buildWhereV2(Object obj, String tableName, HashMap params) throws Exception {
        StringBuilder builder = new StringBuilder();
        Field[] fields = obj.getClass().getDeclaredFields();
        int modifier;
        if (!isEmpty(tableName) && !tableName.endsWith(".")) {
            tableName = tableName + ".";
        }

        for (Field field : fields) {
            modifier = field.getModifiers();
            if (Modifier.isFinal(modifier) || Modifier.isStatic(modifier)) {
                continue;
            }
            field.setAccessible(true);
            Object value = field.get(obj);
            if ("ORDER_BY".equals(field.getName())) {
                params.put("ORDER_BY", value);
            }


            if (value != null) {
                if (field.isAnnotationPresent(ColumnMapper.class)) {
                    ColumnMapper columnMapper = field.getAnnotation(ColumnMapper.class);
                    String fieldName = columnMapper.map();
                    if (value instanceof String) {
                        builder.append(" and (").append(tableName).append(fieldName).append(" LIKE CONCAT('%',:").append(field.getName()).append(",'%'))");
                    } else {
                        builder.append(" and ").append(tableName).append(fieldName).append(" =:").append(field.getName()).append(" ");
                    }
                    params.put(field.getName(), value);
                }

            }

        }
        return builder;
    }

    public static boolean isNotNull(List ls) {
        return ls != null && ls.size() > 0;
    }

    public static HashMap getOtherCond(Object obj) throws Exception {

        HashMap<String, Object> cond = new HashMap<>();
        Field[] fields = obj.getClass().getSuperclass().getDeclaredFields();

        int modifier;

        for (Field field : fields) {

            modifier = field.getModifiers();
            if (Modifier.isFinal(modifier) || Modifier.isStatic(modifier)) {
                continue;
            }
            field.setAccessible(true);
            Object value = field.get(obj);
            cond.put(field.getName(), value);

        }
        return cond;
    }

    public static boolean isEmpty(String str) {
        return (str == null || "".equals(str.trim()));
    }

    public static boolean isEmpty(Object str) {
        return (str == null || "".equals(str));
    }

    public static String getStackTrace(Throwable e) {
        Writer sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stacktrace = sw.toString();
        pw.close();
        return stacktrace;
    }

    public static LocalDateTime tryToDate(String strDate) {
        LocalDateTime retDate;
        String[] dateFormat = {"dd/MM/yyyy", "dd-MM-yyyy", "dd/MM/yyyy HH:mm:ss", "dd-MM-yyyy HH:mm:ss"};
        for (String s : dateFormat) {
            retDate = toDate(strDate, s);
            if (retDate != null) {
                return retDate;
            }
        }
        return null;
    }

    public static Long tryToLong(String strLong) {
        try {
            return Long.parseLong(strLong);
        } catch (NumberFormatException ex) {
            return null;
        }

    }

    public static Double tryToDouble(String strDouble) {
        try {
            return Double.parseDouble(strDouble);
        } catch (NumberFormatException ex) {
            return null;
        }

    }

    public static LocalDateTime toDate(String strDate, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        try {

            LocalDateTime dateTime = LocalDateTime.parse(strDate, formatter);
            return dateTime;
        } catch (Exception ex) {
            try {
                LocalDate date = LocalDate.parse(strDate, formatter);
                return date.atTime(0, 0, 0);
            } catch (Exception ex1) {
                return null;
            }
        }
    }

    public static String genRandomCode() {
        RandomStringGenerator randomStringGenerator =
                new RandomStringGenerator.Builder()
                        .withinRange('0', 'z')
                        .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                        .build();
        return randomStringGenerator.generate(6).toUpperCase();
    }

    public static int generateOTP(String key) {
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);

        return otp;
    }

    public static String toString(LocalDateTime date, String fmt) {

        if (Utils.isEmpty(fmt)) {
            fmt = "dd/MM/yyyy";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(fmt);

        String formatDateTime = date.format(formatter);
        return formatDateTime;
    }


    public static String generateAccountNumber(Long id, String prefix) {
        String accountNumber = StringUtils.leftPad(id.toString(), 9, '0');
        // FINERACT-590
        // Because account_no is limited to 20 chars, we can only use the first 10 chars of prefix - trim if necessary
        if (prefix != null) {
            prefix = prefix.substring(0, Math.min(prefix.length(), 10));
        }

        accountNumber = StringUtils.overlay(accountNumber, prefix, 0, 0);

        return accountNumber;
    }

    /**
     * Returns a string containing the tokens joined by delimiters.
     *
     * @param tokens an array objects to be joined. Strings will be formed from
     *               the objects by calling object.toString().
     */
    public static String join(CharSequence delimiter, Iterable tokens) {
        StringBuilder sb = new StringBuilder();
        Iterator<?> it = tokens.iterator();
        if (it.hasNext()) {
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(delimiter);
                sb.append(it.next());
            }
        }
        return sb.toString();
    }

    public static String joinString(CharSequence delimiter, Iterable tokens) {
        StringBuilder sb = new StringBuilder();
        Iterator<?> it = tokens.iterator();
        if (it.hasNext()) {
            sb.append("'").append(it.next()).append("'");
            while (it.hasNext()) {
                sb.append(delimiter);
                sb.append("'").append(it.next()).append("'");
            }
        }
        return sb.toString();
    }

    public static <T> List<T> removeDuplicates(List<T> list) {
        ArrayList<T> newList = new ArrayList<T>();
        for (T element : list) {
            // then add it
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }
        return newList;
    }

    public static boolean isNumeric(String str) {
        try {
            Long.parseLong(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
