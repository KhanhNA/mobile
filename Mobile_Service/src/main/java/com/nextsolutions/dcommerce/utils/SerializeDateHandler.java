package com.nextsolutions.dcommerce.utils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class SerializeDateHandler extends StdSerializer<LocalDateTime> {
	public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS'Z'";
	private static final long serialVersionUID = -601499764403006185L;

	public SerializeDateHandler() {
		this(null);
	}

	public SerializeDateHandler(Class<LocalDateTime> t) {
		super(t);
	}

	@Override
	public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider arg2) throws IOException {
		gen.writeString(value.format(DateTimeFormatter.ofPattern(DATE_PATTERN)));
	}
}
