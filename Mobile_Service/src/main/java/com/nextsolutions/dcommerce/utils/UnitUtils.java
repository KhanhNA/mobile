package com.nextsolutions.dcommerce.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class UnitUtils {

    private static final MathContext MATH_CONTEXT = new MathContext(2, RoundingMode.UNNECESSARY);

    private static final BigDecimal ONE_METER_EQUAL_MILLIMETERS = BigDecimal.valueOf(1000);
    private static final BigDecimal ONE_CENTIMETER_EQUAL_MILLIMETERS = BigDecimal.valueOf(10);

    private static final BigDecimal ONE_OUNCE_EQUAL_GRAMS = BigDecimal.valueOf(28.34952);
    private static final BigDecimal ONE_POUND_EQUAL_GRAMS = BigDecimal.valueOf(453.59237);
    private static final BigDecimal ONE_MILLIGRAM_EQUAL_GRAMS = BigDecimal.valueOf(1000);
    private static final BigDecimal ONE_KILOGRAM_EQUAL_GRAMS = BigDecimal.valueOf(1000);

    public static BigDecimal convertMetersToMillimeters(BigDecimal value) {
        return value.multiply(ONE_METER_EQUAL_MILLIMETERS);
    }

    public static BigDecimal convertCentimetersToMillimeters(BigDecimal value) {
        return value.multiply(ONE_CENTIMETER_EQUAL_MILLIMETERS);
    }

    public static BigDecimal convertPoundsToGrams(BigDecimal value) {
        return value.multiply(ONE_POUND_EQUAL_GRAMS);
    }

    public static BigDecimal convertKilogramsToGrams(BigDecimal value) {
        return value.multiply(ONE_KILOGRAM_EQUAL_GRAMS);
    }

    public static BigDecimal convertMilligramsToGrams(BigDecimal value) {
        return value.divide(ONE_MILLIGRAM_EQUAL_GRAMS, MATH_CONTEXT);
    }

    public static BigDecimal convertOuncesToGrams(BigDecimal value) {
        return value.multiply(ONE_OUNCE_EQUAL_GRAMS);
    }
}
