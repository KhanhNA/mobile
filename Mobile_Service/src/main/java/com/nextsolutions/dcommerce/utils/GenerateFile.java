package com.nextsolutions.dcommerce.utils;

import com.nextsolutions.dcommerce.model.Category;
import com.nextsolutions.dcommerce.service.impl.HunterServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class GenerateFile {
    static final String PATH = "C:/Users/thonv/Documents/dc_web/CMS/src/app/generate/";
    static final String MODEL = "C:/Users/thonv/Documents/dc_web/CMS/src/app/_models/";
    static  String entity = "Category";
    static final String ADD_EDIT = "add-edit";
    static final String LIST = "list";
    static final String LOCALE_PATH = "C:/Users/thonv/Documents/dc_web/CMS/src/assets/i18n/";
    static final String MENU_FILE = "C:/Users/thonv/Documents/dc_web/CMS/src/assets/Menus.json";
    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);

    public static void main(String[] argv) {
        try {
            Class<?> clazz = Category.class;
            entity = clazz.getSimpleName().toLowerCase();
            Path path = Paths.get(PATH + entity);

            Files.createDirectories(path);
//            genMenu("menu.category", "category");
//            makeDir();
//
            genAEHtml(PATH + entity + "/a-e-" + entity + ".html", clazz);
            genAETs(PATH + entity + "/a-e-" + entity + ".component.ts", clazz);
            genLDHtml(PATH + entity + "/l-d-" + entity + ".html", clazz);
            genLDTs(PATH + entity + "/l-d-" + entity + ".component.ts", clazz);
            genModel(MODEL + entity + ".model.ts", clazz);
            genLocale(LOCALE_PATH + "en.json", clazz);
            genLocale(LOCALE_PATH + "vi.json", clazz);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
//        File dir = new File(path + entity);
//		dir.mkdir();
//
//        try (OutputStreamWriter writer =
//                     new OutputStreamWriter(new FileOutputStream(PROPERTIES_FILE), StandardCharsets.UTF_8))
//        // do stuff
//        }
        System.out.println("hello");
    }

    private static void makeDir() throws IOException {
        Path path = Paths.get(PATH + entity);

        Files.createDirectories(path);
//        path = Paths.get(PATH + entity + "/" + ADD_EDIT);
//        Files.createDirectories(path);
//        path = Paths.get(PATH + entity + "/" + LIST);
//        Files.createDirectories(path);
    }


    private static void genAEHtml(String file, Class<?> clazz) throws Exception {
        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            writer.write("<div class=\"form\">\n" +
                    "  <div class=\"add-title\">\n" +
                    "    <h2 class=\"primary\">{{this.title | translate}}</h2>\n" +
                    "\n" +
                    "  </div>\n" +
                    "  <app-ae [fields]=\"regConfig\" (submit)=\"submit($event)\">\n" +
                    "  </app-ae>\n" +
                    "  <div class=\"margin-top\">\n" +
                    "    {{ form.value | json }}\n" +
                    "  </div>\n" +
                    "</div>\n");
        }
    }

    private static void genLDHtml(String file, Class<?> clazz) throws Exception {
        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            writer.write("<h2 style=\"margin: auto\"> {{ '"+ entity + ".managerment' | translate }} </h2>\n" +
                    "  <app-ld [api]='api' [ldCols]=\"ldCols\" (editEvent)=\"onEditEvent($event)\"\n" +
                    "          (addEvent)=\"onAddEvent($event)\"></app-ld>");
        }
    }
    private static void genAETs(String file, Class<?> clazz) throws Exception {
        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            writer.write("import { Component, ViewChild } from '@angular/core';\n" +
                    "import { Validators } from '@angular/forms';\n" +
                    "import {FieldConfig} from '../base/field/field.interface';\n" +
                    "import {AEComponent} from '../base/a-e.component';\n" +
                    "\n" +
                    "\n" +
                    "@Component({\n" +
                    "  templateUrl: './a-e-" + entity + ".html',\n" +
                    "})\n" +
                    "export class AE" + clazz.getSimpleName() + "Component {\n" +
                    "  @ViewChild(AEComponent, {static: true}) form: AEComponent;\n" +
                    "  regConfig: FieldConfig[] = [\n"
                    );
            for(Field field: clazz.getDeclaredFields()){
                writer.write("    {\n" +
                        "      type: 'input',\n" +
                        "      label: 'locale." + entity + "." + field.getName() + "',\n" +
                        "      inputType: 'text',\n" +
                        "      name: '" + field.getName() + "',\n" +
                        "}, ") ;
            }


            writer.write(       "  ];\n" +
                    "\n" +
                    "  submit(value: any) {}\n" +
                    "}\n" );


        }
    }
    private static void genLDTs(String file, Class<?> clazz) throws Exception {
        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            writer.write("import {Component, OnInit} from '@angular/core';\n" +
                    "import {Router} from '@angular/router';\n" +
                    "import {ApiService} from '../_services/api.service';\n" +
                    "import {FormBuilder} from '@angular/forms';\n" +
                    "import {MatDialog, MatPaginatorIntl} from '@angular/material';\n" +
                    "import {MultilanguagePanigator} from '../_helpers/multilanguage.paginator';\n" +
                    "import {BaseFormLDComponent} from '../base/base-form-l-d.component';\n" +
                    "import {CookieService} from 'ngx-cookie-service';\n" +
                    "import {AE" + clazz.getSimpleName() + "Component} from './a-e-"+ entity +".component';\n" +
                    "import {" + clazz.getSimpleName() + "} from '../_models/" + entity + ".model';" +
                    "\n" +




                    "@Component({\n" +
                    "   selector: 'app-list-" + entity + "', /* dinh nghia id*/" +
                    "   templateUrl: './l-d-" + entity + ".html',\n" +
                    "providers: [\n" +
                    "    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator}\n" +
                    "  ]\n" +

                    "})\n" +
                    "export class LD" + clazz.getSimpleName() + "Component extends BaseFormLDComponent {\n" +

                    "\n" +
                    "  constructor(formBuilder: FormBuilder, private router: Router, userService: ApiService,\n" +
                    "              cookieService: CookieService,\n" +
                    "              public dialog: MatDialog) {\n" +
                    "    super(formBuilder, new " + clazz.getSimpleName() +"(), cookieService, userService);\n" +

                    "    this.api = '/" + entity + "s';\n" +
                    "    this.ldCols = [" );

            for(Field field: clazz.getDeclaredFields()){
                writer.write("'" + field.getName() + "', ") ;
            }
            writer.write("];\n");
            writer.write("}\n");
            writer.write(
                    "onAddEvent(event) {\n" +
                            "\n" +
                            "    this.dialog.open(AE" + clazz.getSimpleName() + "Component, {disableClose: false, width: '500px', data: event});\n" +
                            "  }\n" +
                            "\n" +
                            "  onEditEvent(event) {\n" +
                            "    console.log('event:', event);\n" +
                            "\n" +
                            "    this.dialog.open(AE" + clazz.getSimpleName() + "Component, {disableClose: false, width: '500px', data: event});\n" +
                            "\n" +
                            "  }\n" +
                            "\n" +
                            "}");
        }
    }
    private static void genModel(String file, Class<?> clazz) throws Exception {
        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            writer.write(String.format("import {BaseModel} from './Base.model';\n" +
                    "\n" +
                    "export class %s extends BaseModel {\n", clazz.getSimpleName()));
            for(Field field: clazz.getDeclaredFields()){
                writer.write("  " + field.getName() + ": string;\n");
            }

            writer.write("  constructor() {\n" +
                    "    super();\n" );
            for(Field field: clazz.getDeclaredFields()) {
                writer.write("    this." + field.getName() + " = '';\n");
            }
            writer.write("  }\n" +
                    "}");
        }
    }
    private static void gents(String file, Class<?> clazz) throws Exception {
        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            writer.write("import {Component, OnInit} from '@angular/core';\n" +
                    "import {FormBuilder, FormGroup, Validators} from '@angular/forms';\n" +
                    "import {Router} from '@angular/router';\n" +
                    "import {ApiService} from '../../_services/api.service';\n" +
                    "import {MatDialog} from '@angular/material';\n" +
                    "import {BaseFormComponent} from '../../base/BaseComponent.model';\n" +
                    "import {CookieService} from 'ngx-cookie-service';\n" +
                    "import {Product} from '../../_models/"+entity+".model';\n" +
                    "\n" +
                    "@Component({\n" +
                    "  templateUrl: './component.html',\n" +
                    //"  styleUrls: ['./component.scss']\n" +
                    "})\n" +
                    "export class AddEdit" + entity + "Component extends BaseFormComponent implements OnInit {\n" +
                    "\n" +
                    "  constructor(public dialog: MatDialog, private formBuilder: FormBuilder, private router: Router, userService: ApiService,\n" +
                    "              cookieService: CookieService) {\n" +
                    "    super(formBuilder, new " + entity + "(), cookieService, userService);\n" +
                    "\n" +
                    "  }\n" +
                    "  breakpoint: number;\n" +
                    "  title: string;\n" +
                    "  ngOnInit() {\n" +
                    "\n" +
                    "  }\n" +
                    "\n" +
                    "  onResize(event: any): void {\n" +
                    "    this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;\n" +
                    "  }\n" +
                    "\n" +
                    "  isEdit() {\n" +
                    "    return false;\n" +
                    "  }\n" +
                    "\n" +
                    "  doSave() {\n" +
                    "    console.log('dosave', this.fv);\n" +
                    "    this.userService.postV2('/products/save', this.fv, '', this.successFunc);\n" +
                    "  }\n" +
                    "  successFunc() {\n" +
                    "  }\n" +
                    "\n" +
                    "}\n");
        }
    }
    private static void genLocale(String file, Class<?> clazz) throws Exception {
//        JSONParser jsonParser = new JSONParser();

//        Map<String, String> obj = getFile(file);
        Map<String, String> obj = null;
        File fi = new File(file);
//
        if(fi.exists()) {
            try (FileReader reader = new FileReader(fi)) {
                //Read JSON file
//            Map<String, List<String>> map = new Gson().fromJson(json, new TypeToken<LinkedHashMap<String, List<String>>>(){}.getType());

//                obj = new Gson().fromJson(reader, new TypeToken<LinkedHashMap<String, String>>() {
//                }.getType());

            }
        }
        fi.createNewFile();
        String fileName = (fi.getName().split("\\."))[0];
//            obj = (JSONObject)jsonParser.parse(reader);
        String key, value;
        if(obj == null){
            obj = new LinkedHashMap<>();
        }
        for(Field field: clazz.getDeclaredFields()){
            key = "locale." + entity  + "." + field.getName();
            value = obj.get(key);
            value = value == null?key + "_" + fileName:value;
            obj.put(key, value);
        }




        if(obj != null){
//            Gson gson = new GsonBuilder().setPrettyPrinting().create();
//            try (FileWriter writer = new FileWriter(file)) {
//                gson.toJson(obj, writer);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

//            //Write JSON file
//            FileWriter f = new FileWriter(file);
//            f.write("{\n");
//            int len = obj.size();
//            final int[] i = {0};
//            final String[] commonChar = {","};
//            obj.forEach((k,v) ->{
//                i[0]++;
//                if(i[0] == len){
//                    commonChar[0] = "";
//                }
//                try {
//                    f.write(String.format(" \"%s\": \"%s\"%s\n",k,v, commonChar[0]));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            });
//            f.write("}");
//            f.flush();
        }
    }
    private static void genMenu(String parent, String chile) throws Exception{
        ArrayList<Map<String, Object>> obj = getFile(MENU_FILE);
//        String fileName = (fi.getName().split("/."))[0];
    }

    private static ArrayList<Map<String, Object>> getFile(String file) throws IOException {
//        JSONParser jsonParser = new JSONParser();
        ArrayList<Map<String, Object>> obj = null;
        File fi = new File(file);

        if(fi.exists()) {
            try (FileReader reader = new FileReader(fi)) {

//                obj = new Gson().fromJson(reader, new TypeToken<ArrayList<LinkedHashMap<String, Object>>>() {
//                }.getType());
//
            }
        }else {
            fi.createNewFile();
        }
        return obj;
    }
}
