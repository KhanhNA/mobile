package com.nextsolutions.dcommerce.utils;

import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDto;
import com.nextsolutions.dcommerce.shared.dto.LoyaltyDto;
import com.nextsolutions.dcommerce.shared.dto.ProductDto;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class CommonRuntimeData {
    public static HashMap<Integer, DataByDay<ProductDto>> productDto;

    public static HashMap<Integer, DataByDay<IncentiveProgramDto>> incentiveProgramDtos;

    public static HashMap<Integer, DataByDay<LoyaltyDto>> loyaltyDtos;

    public static HashMap<String, Object> cacheLanguage = new HashMap<>();

    public static List<IncentiveProgramDto> getIncentiveDto(Integer langId) {


        if(incentiveProgramDtos == null){
            return null;
        }

        DataByDay<IncentiveProgramDto> dto = incentiveProgramDtos.get(langId);
        if(dto == null){
            return null;
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        if(!timeStamp.equals(dto.getTimeStamp())){
            dto.clear();
            return null;

        }
        return dto.getData();
    }
    public static List<IncentiveProgramDto> storeIncentiveDto(Integer langId, List<IncentiveProgramDto> dtos){
        if(incentiveProgramDtos == null){
            incentiveProgramDtos = new HashMap<>();
        }

        DataByDay<IncentiveProgramDto> dto = incentiveProgramDtos.get(langId);
        if(dto == null){
            dto = new DataByDay<>();
            incentiveProgramDtos.put(langId, dto);
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        if(!timeStamp.equals(dto.getTimeStamp())){
            dto.clear();
        }
        dto.setTimeStamp(timeStamp);
        dto.setData(dtos);
        return dtos;
    }

    public static List<LoyaltyDto> getLoyaltyDto(Integer langId) {


        if(loyaltyDtos == null){
            return null;
        }

        DataByDay<LoyaltyDto> dto = loyaltyDtos.get(langId);
        if(dto == null){
            return null;
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        if(!timeStamp.equals(dto.getTimeStamp())){
            dto.clear();
            return null;

        }
        return dto.getData();
    }
    public static List<LoyaltyDto> storeLoyaltyDto(Integer langId, List<LoyaltyDto> dtos){
        if(loyaltyDtos == null){
            loyaltyDtos = new HashMap<>();
        }

        DataByDay<LoyaltyDto> dto = loyaltyDtos.get(langId);
        if(dto == null){
            dto = new DataByDay<>();
            loyaltyDtos.put(langId, dto);
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        if(!timeStamp.equals(dto.getTimeStamp())){
            dto.clear();
        }
        dto.setTimeStamp(timeStamp);
        dto.setData(dtos);
        return dtos;
    }



    public static List<ProductDto> getProductDto(Integer langId) {


        if(productDto == null){
            return null;
        }

        DataByDay<ProductDto> dto = productDto.get(langId);
        if(dto == null){
            return null;
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        if(!timeStamp.equals(dto.getTimeStamp())){
            dto.clear();
            return null;

        }
        return dto.getData();
    }
    public static List<ProductDto> storeProductDto(Integer langId, List<ProductDto> dtos){
        if(productDto == null){
            productDto = new HashMap<>();
        }

        DataByDay<ProductDto> dto = productDto.get(langId);
        if(dto == null){
            dto = new DataByDay<>();
            productDto.put(langId, dto);
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        if(!timeStamp.equals(dto.getTimeStamp())){
            dto.clear();
        }
        dto.setTimeStamp(timeStamp);
        dto.setData(dtos);
        return dtos;
    }
}
