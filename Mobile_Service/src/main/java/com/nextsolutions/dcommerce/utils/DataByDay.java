package com.nextsolutions.dcommerce.utils;

import java.util.ArrayList;
import java.util.List;

public class DataByDay<T> {
    private String timeStamp;
    private List<T> data;

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> aData) {
        if(this.data == null){
            this.data = new ArrayList<>();
        }
        this.data.clear();
        this.data.addAll(aData);
    }
    public void clear(){
        timeStamp = null;
        if(data != null) {
            data.clear();
        }
    }
}
