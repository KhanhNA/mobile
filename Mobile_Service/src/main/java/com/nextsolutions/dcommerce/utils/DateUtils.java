package com.nextsolutions.dcommerce.utils;

import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

public class DateUtils {
    public static String DATE_FORMAT = "dd/MM/yyyy";

    public static boolean isWithinRange(LocalDateTime checkTime, LocalDateTime startDate, LocalDateTime endDate) {
        return !(checkTime.isBefore(startDate) || checkTime.isAfter(endDate));
    }

}
