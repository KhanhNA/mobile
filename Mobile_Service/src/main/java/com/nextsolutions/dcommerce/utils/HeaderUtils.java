package com.nextsolutions.dcommerce.utils;

import org.springframework.http.HttpHeaders;

import java.util.List;

public class HeaderUtils {
    public static HttpHeaders getBearerToken(HttpHeaders headers) {
        List<String> authorization = headers.get("Authorization");
        if (authorization == null || authorization.isEmpty())
            throw new RuntimeException();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", authorization.get(0));
        return httpHeaders;
    }
}
