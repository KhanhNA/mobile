package com.nextsolutions.dcommerce.utils;


import com.nextsolutions.dcommerce.exception.CustomErrorException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateValidatorUsingDateFormat implements DateValidator {
    private String dateFormat;

    public DateValidatorUsingDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public boolean isValid(String dateStr) {
        DateFormat sdf = new SimpleDateFormat(this.dateFormat);
        sdf.setLenient(false);
        try {
            sdf.parse(dateStr);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static void validDate(@RequestParam String fromDate, @RequestParam String toDate) {
        DateValidatorUsingDateFormat dateFormat = new DateValidatorUsingDateFormat("yyyy-MM-dd");
        if (!org.springframework.util.StringUtils.isEmpty(fromDate) && !dateFormat.isValid(fromDate)) {
            throw new CustomErrorException("fromDate must format yyyy-MM-dd");
        }
        if (!StringUtils.isEmpty(toDate) && !dateFormat.isValid(toDate)) {
            throw new CustomErrorException("toDate must format yyyy-MM-dd");
        }
    }
}
