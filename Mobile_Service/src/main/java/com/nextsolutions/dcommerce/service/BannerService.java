package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.ui.model.request.BannerFormRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerDetailsResource;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerResource;
import com.nextsolutions.dcommerce.ui.model.response.BannerFormResponseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface BannerService {
    Page<BannerResource> findByQuery(BannerResource resource, Pageable pageable);

    void inactiveBanner(Long id);

    void createBanner(BannerFormRequestModel banner);

    BannerFormResponseModel updateBanner(Long id, BannerFormRequestModel request);

    BannerFormResponseModel getBannerFormResponse(Long id);

    Map<String, String> saveImageFileToLocalServer(MultipartFile image) throws IOException;

    List<BannerDetailsResource> getMarketing(Long merchantId,Long langId) throws Exception;

}
