package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.RecommendSearch;
import com.nextsolutions.dcommerce.shared.dto.resource.RecommendSearchResource;
import com.nextsolutions.dcommerce.ui.model.request.RecommendSearchRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.RecommendSearchResponseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RecommendSearchService {
    Page<RecommendSearchResource> findByQuery(RecommendSearchResource resource, Pageable pageable) throws Exception;

    void createRecommendSearch(RecommendSearchRequestModel request);

    void inactive(Long id);

    RecommendSearchResource getRecommendSearch(Long id) throws Exception;

    List<RecommendSearchResource> getRecommendSearch(Long merchantId, Long langId) throws Exception;

    RecommendSearch updateRecommendSearch(Long id, RecommendSearchRequestModel request);
}
