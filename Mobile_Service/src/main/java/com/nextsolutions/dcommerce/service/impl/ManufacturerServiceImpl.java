package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.repository.AutoIncrementJpaRepository;
import com.nextsolutions.dcommerce.repository.ManufacturerRepository;
import com.nextsolutions.dcommerce.service.ManufacturerService;
import com.nextsolutions.dcommerce.shared.constant.AutoIncrementType;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDto;
import com.nextsolutions.dcommerce.shared.dto.TemporaryManufacturerDTO;
import com.nextsolutions.dcommerce.shared.dto.TemporaryManufacturerDescriptionDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.ManufacturerResource;
import com.nextsolutions.dcommerce.shared.mapper.ManufacturerMapper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.String;

import com.nextsolutions.dcommerce.ui.model.request.TemporaryManufacturerRequestModel;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MimeTypeUtils;

import javax.persistence.EntityNotFoundException;
import java.net.URLConnection;
import java.util.*;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class ManufacturerServiceImpl implements ManufacturerService {

    @Autowired
    private CommonServiceImpl commonService;
    private final ManufacturerRepository manufacturerRepository;
    private final ManufacturerMapper manufacturerMapper;
    private final static String PARENT_PATH = "/images/manufacture";
    private final AppConfiguration appConfiguration;
    private final AutoIncrementJpaRepository autoIncrementJpaRepository;
    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);

    @Bean
    CommandLineRunner runner() {
        return args -> FileUtils.forceMkdir(new File(appConfiguration.getUploadPath() + PARENT_PATH));
    }

    @Override
    public ManufacturerDto findById(Long id) {
        return manufacturerRepository.findById(id)
                .map(manufacturerMapper::toManufacturerDto).orElse(null);
    }

    @Override
    @Transactional
    public ManufacturerDto requestCreateManufacturer(ManufacturerDto form) {
        Manufacturer manufacturer = new Manufacturer();
        AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode( AutoIncrementType.NPP.value() );
        Long code = Long.parseLong( autoIncrement.getValue() ) + 1;
        manufacturer.setCode(code.toString());
        manufacturer.setCreatedDate(form.getCreatedDate());
        manufacturer.setTel(form.getTel());
        manufacturer.setEmail(form.getEmail());
        manufacturer.setMerchantId(1);
        manufacturer.setImage(getImageFile(form.getImageUrl()));
        manufacturer.setTaxCode(form.getTaxCode());
        manufacturer.setStatus(0L);
        manufacturer.setAction(1L);

        manufacturerRepository.save(manufacturer);

        List<ManufacturerDescription> manufacturerDescriptions = Optional
                .ofNullable(form.getDescriptions())
                .map(resources -> resources.stream().map(resource -> {
                    ManufacturerDescription description = new ManufacturerDescription();
                    description.setManufacturerId(manufacturer.getId());
                    description.setLanguageId(resource.getLanguageId());
                    description.setName(resource.getName());
                    description.setAddress(resource.getAddress());
                    return description;
                }).collect(Collectors.toList()))
                .orElse(null);
        manufacturer.setDescriptions(manufacturerDescriptions);
        manufacturerRepository.save(manufacturer);
        autoIncrement.setValue(code.toString());
        autoIncrementJpaRepository.save(autoIncrement);
        return manufacturerMapper.toManufacturerDto(manufacturer);
    }

    private String getImageFile(String image) {
        if (image.startsWith("data:")) {
            try {
                image = image.replaceFirst("(data:.*;base64,)", "");
                byte[] decode = Base64.getDecoder().decode(image);
                String type = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(decode));
                String subtype = MimeTypeUtils.parseMimeType(type).getSubtype();
                if (subtype.equals("x-wav")) {
                    subtype = "webp";
                }
                String filename = "/" + UUID.randomUUID().toString().replaceAll("-", "") + "." + subtype;
                try (OutputStream out = new FileOutputStream(appConfiguration.getUploadPath() + PARENT_PATH + filename)) {
                    out.write(decode);
                    return PARENT_PATH + filename;
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return image;
        }
        return null;
    }

    @Override
    public Page<ManufacturerResource> getManufacturers(ManufacturerResource resource, Pageable pageable) {
        return manufacturerRepository.findAll(resource, pageable);
    }

    @Override
    @Transactional
    public ManufacturerDto requestUpdateManufacturer(Long id, ManufacturerDto form) {
        Manufacturer manufacturer = manufacturerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        Date timeRequest = new Date();
        TemporaryManufacturer temporaryManufacturer = new TemporaryManufacturer();
        temporaryManufacturer.setCreatedDate(form.getCreatedDate());
        temporaryManufacturer.setTel(form.getTel());
        temporaryManufacturer.setEmail(form.getEmail());
        temporaryManufacturer.setManufacturerId(id);
        temporaryManufacturer.setImage(getImageFile(form.getImageUrl()));
        temporaryManufacturer.setTimeRequest(timeRequest);
        temporaryManufacturer.setTaxCode(form.getTaxCode());

        form.getDescriptions().forEach(resource -> {
            TemporaryManufacturerDescription description = new TemporaryManufacturerDescription();
            description.setManufacturerId(id);
            description.setName(resource.getName());
            description.setLanguageId(resource.getLanguageId());
            description.setAddress(resource.getAddress());
            description.setTimeRequest(timeRequest);
            commonService.save(description);
        });
        manufacturer.setAction(2L);
        manufacturer.setStatus(0L);
        manufacturerRepository.save(manufacturer);
        commonService.save(temporaryManufacturer);
        return manufacturerMapper.toManufacturerDto(manufacturer);
    }

    @Override
    public ManufacturerDto updateManufacturerByManager(Long id, TemporaryManufacturerRequestModel form) {
        Manufacturer manufacturer = manufacturerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        String imageUrl = "";
        if(form.getImageUrl().startsWith("data:")) {
            imageUrl = getImageFile(form.getImageUrl());
        } else {
            imageUrl = form.getImageUrl();
        }
        manufacturer.setCreatedDate(form.getCreatedDate());
        manufacturer.setTel(form.getTel());
        manufacturer.setEmail(form.getEmail());
        manufacturer.setMerchantId(1);
        manufacturer.setTaxCode(form.getTaxCode());
        manufacturer.setImage(imageUrl);
        manufacturer.setStatus(1L);
        manufacturer.setAction(0L);
        manufacturer.setIsSynchronization(false);
        List<ManufacturerDescription> manufacturerDescriptions = new LinkedList<>();
        Map<Long, ManufacturerDescriptionDto> resourceMap = new HashMap<>();

        form.getDescriptions().forEach(resource -> {
            if (resource.getLanguageId() == null) {
                ManufacturerDescription description = new ManufacturerDescription();
                description.setManufacturerId(id);
                description.setName(resource.getName());
                description.setLanguageId(resource.getLanguageId());
                description.setAddress(resource.getAddress());
                manufacturerDescriptions.add(description);
            } else {
                resourceMap.put(resource.getLanguageId(), resource);
            }
        });


        List<ManufacturerDescription> descriptions = manufacturer.getDescriptions();
        descriptions.forEach(attributeDescription -> {
            if (resourceMap.containsKey(attributeDescription.getLanguageId())) {
                ManufacturerDescriptionDto resource = resourceMap.get(attributeDescription.getLanguageId());
                attributeDescription.setManufacturerId(manufacturer.getId());
                attributeDescription.setLanguageId(resource.getLanguageId());
                attributeDescription.setName(resource.getName());
                attributeDescription.setAddress(resource.getAddress());
            }
        });

        if (!manufacturerDescriptions.isEmpty()) {
            descriptions.addAll(manufacturerDescriptions);
        }

        manufacturerRepository.save(manufacturer);
        return manufacturerMapper.toManufacturerDto(manufacturer);
    }


    @Override
    public void requestDelete(Long id) {
        Manufacturer manufacturer = manufacturerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        manufacturer.setAction(3L);
        manufacturer.setStatus(0L);
        manufacturerRepository.save(manufacturer);
    }

    @Override
    public void inactiveManufacturer(Long id) {
        manufacturerRepository.inactive(id);
    }

    @Override
    public boolean existsByCode(String code) {
        return manufacturerRepository.findByCode(code).isPresent();
    }

    @Override
    public boolean existsByCode(Long id, String code) {
        return manufacturerRepository.findByCode(code)
                .map(distributor -> !distributor.getId().equals(id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsByTel(String tel) {
        return manufacturerRepository.findByTel(tel).isPresent();

    }

    @Override
    public boolean existsByTel(Long id, String tel) {
        return manufacturerRepository.findByTel(tel)
                .map(manufacturer -> !manufacturer.getId().equals(id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsByTaxCode(String taxCode) {
        return manufacturerRepository.findByTaxCode(taxCode).isPresent();
    }

    @Override
    public boolean existsByEmail(String email) {
        return manufacturerRepository.findByEmail(email).isPresent();
    }

    @Override
    public boolean existsByTaxCode(Long id, String taxCode) {
        return manufacturerRepository.findByTaxCode(taxCode)
                .map(manufacturer -> !manufacturer.getId().equals(id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsByEmail(Long id, String email) {
        return manufacturerRepository.findByEmail(email)
                .map(manufacturer -> !manufacturer.getId().equals(id))
                .orElseThrow(EntityNotFoundException::new);
    }


    @Override
    public TemporaryManufacturerDTO getManufacturersToCompare(Long id) {
        StringBuilder selectTemporary = new StringBuilder();
        selectTemporary.append(" Select code , tel , date_created createdDate, email email, tax_code taxCode,MANUFACTURER_IMAGE imageUrl from temporary_manufacturer where MANUFACTURER_ID = :id order by time_request desc limit 1");
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", id);

        List<TemporaryManufacturerDTO> temporaryManufacturer = new LinkedList<>();
        try {
            temporaryManufacturer = commonService.findAll(null, TemporaryManufacturerDTO.class, selectTemporary.toString(), params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        StringBuilder selectDescription = new StringBuilder();
        selectDescription.append(" select name, LANGUAGE_ID, ADDRESS from temporary_manufacturer_description where MANUFACTURER_ID = :id order by time_request desc limit 3");
        params = new HashMap<>();
        params.put("id", id);

        List<TemporaryManufacturerDescriptionDTO> listDescription = new LinkedList<>();
        try {
            listDescription = commonService.findAll(null, TemporaryManufacturerDescriptionDTO.class, selectDescription.toString(), params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        temporaryManufacturer.get(0).setDescriptions(listDescription);

        return temporaryManufacturer.get(0);
    }

    @Override
    public ManufacturerDto managerReject(Long id) {
        Manufacturer manufacturer = manufacturerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        manufacturer.setIsSynchronization(false);
        manufacturer.setStatus(-1L);
        manufacturer.setAction(0L);
        manufacturerRepository.save(manufacturer);
        return manufacturerMapper.toManufacturerDto(manufacturer);
    }

    @Override
    public ManufacturerDto managerApproved(Long id) {
        Manufacturer manufacturer = manufacturerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        manufacturer.setStatus(1L);
        manufacturer.setAction(0L);
        manufacturer.setIsSynchronization(false);
        manufacturerRepository.save(manufacturer);
        return manufacturerMapper.toManufacturerDto(manufacturer);
    }
}
