package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.shared.dto.CategoryDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.CategoryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import com.nextsolutions.dcommerce.model.Category;
import com.nextsolutions.dcommerce.model.CategoryDescription;

public interface CategoryService extends BaseService<CategoryDescription, Long> {
	ResponseEntity<Object> getCategoryTreeFormat(Long id, Integer langId);

	ResponseEntity<Object> getAllRoot(Integer langId);

	ResponseEntity<Object> findById(Long id) throws CustomErrorException;

	ResponseEntity<Object> create(Category category) throws Exception;

	ResponseEntity<Object> update(Category category) throws CustomErrorException;

	ResponseEntity<Void> activeDeactive(Long id, Boolean visible) throws CustomErrorException;

	CategoryDescriptionDto findByCategoryIdAndLanguageId(Long categoryId, Long languageId);

	Page<CategoryDto> getListCategoryByLevel(Pageable pageable, Long level, Long code) throws Exception;


}
