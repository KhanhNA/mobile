package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.form.MerchantGroupForm;
import com.nextsolutions.dcommerce.ui.model.request.MerchantGroupQueryRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupMerchantResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface MerchantGroupCrudService {
    Page<MerchantGroupResource> findAll(String keyword, Pageable pageable, Integer languageId);

    List<MerchantGroupResource> findAll(String keyword, Integer languageId, Long merchantId);

    Page<MerchantGroupResource> findByQuery(MerchantGroupQueryRequestModel query, Pageable pageable);

    MerchantGroupForm getMerchantGroupFormById(Long id);

    MerchantGroupForm createMerchantGroup(MerchantGroupForm form);

    MerchantGroupForm updateMerchantGroup(Long id, MerchantGroupForm form);

    void inactiveMerchantGroup(Long id);

    List<MerchantGroupMerchantResource> findAllMerchantGroupMerchantById(Long id);

    void saveMerchantGroupMerchants(Long id, Map<Long, MerchantGroupMerchantResource> resources);
}
