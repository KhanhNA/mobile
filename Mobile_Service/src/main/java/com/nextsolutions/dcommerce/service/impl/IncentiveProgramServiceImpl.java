package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.exception.ActivateIncentiveProgramException;
import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.repository.*;
import com.nextsolutions.dcommerce.service.IncentiveProgramService;
import com.nextsolutions.dcommerce.service.MessageResolverService;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.constant.DiscountType;
import com.nextsolutions.dcommerce.shared.constant.IncentiveType;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDetailInformationDto;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramGroupDto;
import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import com.nextsolutions.dcommerce.shared.dto.coupon.CouponIncentiveDTO;
import com.nextsolutions.dcommerce.shared.dto.incentive.IncentiveProgramDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.incentive.IncentiveProgramDto;
import com.nextsolutions.dcommerce.shared.mapper.IncentiveProgramDetailInformationMapper;
import com.nextsolutions.dcommerce.shared.mapper.IncentiveProgramMapper;
import com.nextsolutions.dcommerce.shared.projection.CategoryProjection;
import com.nextsolutions.dcommerce.shared.projection.IncentivePackingProductProjection;
import com.nextsolutions.dcommerce.shared.projection.ManufacturerProjection;
import com.nextsolutions.dcommerce.ui.model.incentive.IncentiveProgramQuery;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MimeTypeUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.nextsolutions.dcommerce.utils.LanguageUtils.getCurrentLanguageId;
import static com.nextsolutions.dcommerce.utils.LanguageUtils.getLanguageCode;

@Service
@RequiredArgsConstructor
public class IncentiveProgramServiceImpl extends CommonServiceImpl implements IncentiveProgramService {

    private final IncentiveProgramRepository incentiveProgramRepository;
    private final IncentiveProgramDetailInformationRepository incentiveProgramDetailInformationRepository;
    private final IncentiveProgramFlashSaleDateRepository incentiveProgramFlashSaleDateRepository;
    private final PackingProductRepository packingProductRepository;
    private final CouponIncentiveRepository couponIncentiveRepository;
    private final IncentiveProgramMapper incentiveProgramMapper;
    private final IncentiveProgramDetailInformationMapper incentiveProgramDetailInformationMapper;
    private final CategoryRepository categoryRepository;
    private final ManufacturerRepository manufacturerRepository;
    private final IncentiveProgramMerchantGroupRepository incentiveProgramMerchantGroupRepository;
    private final IncentiveProgramSelectedWarehouseRepository incentiveProgramSelectedWarehouseRepository;
    private final AppConfiguration appConfiguration;
    private final MessageResolverService messageResolverService;

    private final static String PARENT_PATH = "/images/incentive";

    @Bean
    CommandLineRunner runner() {
        return args -> FileUtils.forceMkdir(new File(appConfiguration.getUploadPath() + PARENT_PATH));
    }

    @Override
    @Transactional
    public IncentiveProgramDto createIncentiveProgram(IncentiveProgramDto incentiveProgramDto) {
        IncentiveProgram incentiveProgram = incentiveProgramMapper.toIncentiveProgram(incentiveProgramDto);
        for (IncentiveProgramDescription i : incentiveProgram.getDescriptions()) {
            i.setImageUrl(getImageFile(i.getImageUrl()));
        }
        incentiveProgramRepository.save(incentiveProgram);

        List<IncentiveProgramDescription> descriptions = incentiveProgram.getDescriptions();
        descriptions.forEach(description -> {
            description.setIncentiveProgramId(incentiveProgram.getId());
            description.setLanguageCode(getLanguageCode(description.getLanguageId()));
        });

        incentiveProgram.setCreatedUser(SpringUtils.getCurrentUsername());
        incentiveProgram.setCreatedDate(LocalDateTime.now());
        incentiveProgram.setLatestModifiedUser(SpringUtils.getCurrentUsername());
        incentiveProgram.setLatestModifiedDate(LocalDateTime.now());

        incentiveProgramRepository.save(incentiveProgram);

        return incentiveProgramMapper.toIncentiveProgramDto(incentiveProgram);
    }

    @Override
    public boolean existsIncentiveProgramCodeOnCreate(String code) {
        return incentiveProgramRepository.findByCode(code).isPresent();
    }

    @Override
    public boolean existsIncentiveProgramCodeOnUpdate(Long id, String code) {
        return incentiveProgramRepository.findByCode(code)
                .map(incentiveProgram -> !incentiveProgram.getId().equals(id))
                .orElse(false);
    }

    @Override
    public boolean existsCouponCodeOnUpdate(Long id, String code) {
        return couponIncentiveRepository.findByCode(code)
                .map(couponIncentive -> !couponIncentive.getIncentiveProgramId().equals(id))
                .orElse(false);
    }

    private String getImageFile(String image) {
        if (image.startsWith("data:")) {
            try {
                image = image.replaceFirst("(data:.*;base64,)", "");
                byte[] decode = Base64.getDecoder().decode(image);
                String type = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(decode));
                String subtype = MimeTypeUtils.parseMimeType(type).getSubtype();
                if (subtype.equals("x-wav")) {
                    subtype = "webp";
                }
                String filename = "/" + UUID.randomUUID().toString().replaceAll("-", "") + "." + subtype;
                try (OutputStream out = new FileOutputStream(appConfiguration.getUploadPath() + PARENT_PATH + filename)) {
                    out.write(decode);
                    return PARENT_PATH + filename;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return image;
        }
        return null;
    }

    @Override
    public IncentiveProgramDto updateIncentiveProgram(Long id, IncentiveProgramDto incentiveProgramDto) {
        IncentiveProgram incentiveProgram = getIncentiveProgramById(id);

        Map<Long, IncentiveProgramDescriptionDto> incentiveProgramDescriptionDtoLanguageMap = incentiveProgramDto
                .getDescriptions()
                .stream()
                .collect(Collectors.toMap(
                        IncentiveProgramDescriptionDto::getLanguageId,
                        incentiveProgramDescriptionDto -> incentiveProgramDescriptionDto)
                );

        List<IncentiveProgramDescription> descriptions = incentiveProgram.getDescriptions();
        descriptions.forEach(description -> {
            IncentiveProgramDescriptionDto incentiveProgramDescriptionDto =
                    incentiveProgramDescriptionDtoLanguageMap.get(description.getLanguageId());
            description.setIncentiveProgramId(id);
            description.setName(incentiveProgramDescriptionDto.getName());
            description.setDescription(incentiveProgramDescriptionDto.getDescription());
            description.setShortDescription(incentiveProgramDescriptionDto.getShortDescription());
            description.setImageUrl(getImageFile(incentiveProgramDescriptionDto.getImageUrl()));
        });

        incentiveProgram.setCode(incentiveProgramDto.getCode());
        incentiveProgram.setFromDate(incentiveProgramDto.getFromDate());
        incentiveProgram.setToDate(incentiveProgramDto.getToDate());
        incentiveProgram.setLatestModifiedUser(SpringUtils.getCurrentUsername());
        incentiveProgram.setLatestModifiedDate(LocalDateTime.now());
        incentiveProgram.setDisplayArea(incentiveProgramDto.getDisplayArea());

        incentiveProgramRepository.save(incentiveProgram);

        return incentiveProgramMapper.toIncentiveProgramDto(incentiveProgram);
    }

    private IncentiveProgram getIncentiveProgramById(Long id) {
        return incentiveProgramRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Incentive program with id " + id + " doesn't exists"));
    }

    @Override
    public IncentiveProgramDto getIncentiveProgram(Long id) {
        IncentiveProgram incentiveProgram = getIncentiveProgramById(id);
        IncentiveProgramGenerator generator = new IncentiveProgramGenerator(incentiveProgram);
        return generator.generateIncentiveProgramDto();
    }

    private class IncentiveProgramGenerator {
        private final Long id;
        private final IncentiveProgram incentiveProgram;

        public IncentiveProgramGenerator(IncentiveProgram incentiveProgram) {
            this.incentiveProgram = incentiveProgram;
            this.id = incentiveProgram.getId();
        }

        public IncentiveProgramDto generateIncentiveProgramDto() {
            if (isDiscountByProduct())
                return generateIncentiveProgramDtoByProduct();

            else if (isDiscountByProductGroup())
                return generateIncentiveProgramDtoByProductGroup();

            else if (isDiscountByFlashSale())
                return generateIncentiveProgramDtoByFlashSale();

            else {
                IncentiveProgramDto incentiveProgramDto = incentiveProgramMapper.toIncentiveProgramDto(incentiveProgram);
                bindUserTypeToIncentiveProgramDto(incentiveProgramDto);
                return incentiveProgramDto;
            }
        }

        private boolean isDiscountByProduct() {
            return incentiveProgram.getType() == IncentiveType.PRODUCT.value();
        }

        private boolean isDiscountByProductGroup() {
            return incentiveProgram.getType() == IncentiveType.GROUP.value();
        }

        private boolean isDiscountByFlashSale() {
            return incentiveProgram.getType() == IncentiveType.FLASH_SALE.value();
        }

        private IncentiveProgramDto generateIncentiveProgramDtoByProduct() {
            IncentiveProgramDetailInformation incentiveProgramDetailInformationInDB
                    = incentiveProgramDetailInformationRepository.findByIncentiveProgramId(id);

            incentiveProgram.setIncentiveProgramDetailInformation(incentiveProgramDetailInformationInDB);

            IncentiveProgramDto incentiveProgramDto = incentiveProgramMapper.toIncentiveProgramDto(incentiveProgram);
            IncentiveProgramDetailInformationDto incentiveProgramDetailInformation = incentiveProgramDto.getIncentiveProgramDetailInformation();

            if (Objects.nonNull(incentiveProgramDetailInformation)) {
                Long packingProductId = incentiveProgramDetailInformation.getPackingId();
                PackingProductSearchResult packingProduct
                        = packingProductRepository.findPackingProductSearchBy(id, packingProductId, getCurrentLanguageId());

                incentiveProgramDetailInformation.setPackingProduct(packingProduct);

                if (incentiveProgramDetailInformation.getDiscountType() == DiscountType.BONUS_PACKING_PRODUCT.value()) {
                    Long bonusPackingProductId = incentiveProgramDetailInformation.getBonusPackingId();
                    PackingProductSearchResult bonusPackingProduct
                            = packingProductRepository.findPackingProductSearchBy(id, bonusPackingProductId, getCurrentLanguageId());
                    incentiveProgramDetailInformation.setBonusPackingProduct(bonusPackingProduct);
                }
            }
            bindUserTypeToIncentiveProgramDto(incentiveProgramDto);
            return incentiveProgramDto;
        }

        private IncentiveProgramDto generateIncentiveProgramDtoByProductGroup() {
            IncentiveProgramDto incentiveProgramDto = incentiveProgramMapper.toIncentiveProgramDto(incentiveProgram);
            IncentiveProgramGroupDto incentiveProgramGroup = incentiveProgramDto.getIncentiveProgramGroup();
            if (Objects.nonNull(incentiveProgramGroup)) {
                if (Objects.nonNull(incentiveProgramGroup.getCategoryId())) {
                    Long categoryId = incentiveProgramGroup.getCategoryId();
                    CategoryProjection categoryProjection
                            = categoryRepository.getByIdAndLanguageId(categoryId, getCurrentLanguageId());

                    incentiveProgramGroup.setCategoryCode(categoryProjection.getCode());
                    incentiveProgramGroup.setCategoryName(categoryProjection.getName());
                } else {
                    Long manufacturerId = incentiveProgramGroup.getManufacturerId();
                    ManufacturerProjection manufacturerProjection
                            = manufacturerRepository.getByIdAndLanguageId(manufacturerId, getCurrentLanguageId());

                    incentiveProgramGroup.setManufacturerCode(manufacturerProjection.getCode());
                    incentiveProgramGroup.setManufacturerName(manufacturerProjection.getName());
                }
                List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroup
                        = incentiveProgramGroup.getIncentiveProgramDetailInformationGroup();

                if (!CollectionUtils.isEmpty(incentiveProgramDetailInformationGroup)) {
                    List<Long> packingIds = incentiveProgramDetailInformationGroup.stream()
                            .map(IncentiveProgramDetailInformationDto::getPackingId)
                            .collect(Collectors.toList());

                    List<PackingProductSearchResult> packingProductDetails = packingProductRepository
                            .findAllPackingProductWith(packingIds, getCurrentLanguageId(), id);

                    incentiveProgramGroup.setPackingProductDetails(packingProductDetails);
                }
            }
            bindUserTypeToIncentiveProgramDto(incentiveProgramDto);
            return incentiveProgramDto;
        }

        private IncentiveProgramDto generateIncentiveProgramDtoByFlashSale() {
            List<IncentiveProgramDetailInformation> incentiveProgramDetailInformationGroupInDB
                    = incentiveProgramDetailInformationRepository.findAllByIncentiveProgramId(id);

            incentiveProgram.setIncentiveProgramDetailInformationGroup(incentiveProgramDetailInformationGroupInDB);

            IncentiveProgramDto incentiveProgramDto = incentiveProgramMapper.toIncentiveProgramDto(incentiveProgram);
            List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroup = incentiveProgramDto.getIncentiveProgramDetailInformationGroup();

            List<IncentiveProgramDetailInformationDto> result = incentiveProgramDetailInformationGroup.stream().peek(incentiveProgramDetailInformationDto -> {
                IncentivePackingProductProjection incentivePackingProduct = packingProductRepository.getIncentivePackingProductById(incentiveProgramDetailInformationDto.getPackingId(), LanguageUtils.getCurrentLanguageId());
                incentiveProgramDetailInformationDto.setPackingCode(incentivePackingProduct.getPackingCode());
                incentiveProgramDetailInformationDto.setProductName(incentivePackingProduct.getProductName());
                incentiveProgramDetailInformationDto.setPackingOriginalPrice(incentivePackingProduct.getPackingOriginalPrice());
                incentiveProgramDetailInformationDto.setPackingSalePrice(incentivePackingProduct.getPackingSalePrice());
            }).collect(Collectors.toList());

            incentiveProgramDto.setIncentiveProgramDetailInformationGroup(result);
            bindUserTypeToIncentiveProgramDto(incentiveProgramDto);

            return incentiveProgramDto;
        }

        private void bindUserTypeToIncentiveProgramDto(IncentiveProgramDto incentiveProgramDto) {
            List<IncentiveProgramSelectedWarehouse> incentiveProgramSelectedWarehouses
                    = incentiveProgramSelectedWarehouseRepository.findAllByIncentiveProgramId(id);
            List<IncentiveProgramMerchantGroup> incentiveProgramMerchantGroups
                    = incentiveProgramMerchantGroupRepository.findAllByIncentiveProgramId(id);

            List<String> warehouseCodes = incentiveProgramSelectedWarehouses.stream()
                    .map(IncentiveProgramSelectedWarehouse::getWarehouseCode).collect(Collectors.toList());
            incentiveProgramDto.setWarehouseCodes(warehouseCodes);

            List<Long> merchantGroupIds = incentiveProgramMerchantGroups.stream()
                    .map(IncentiveProgramMerchantGroup::getMerchantGroupId).collect(Collectors.toList());

            incentiveProgramDto.setMerchantGroupIds(merchantGroupIds);
        }
    }

    @Override
    public Page<IncentiveProgramDto> getIncentivePrograms(IncentiveProgramQuery query, Pageable pageable) {
        Page<IncentiveProgram> incentiveProgramPage = incentiveProgramRepository.findByQuery(query, pageable);
        return incentiveProgramPage.map(incentiveProgramMapper::toIncentiveProgramDto);
    }

    @Override
    public IncentiveProgramDto toggleStatus(Long id) throws Exception {
        IncentiveProgram incentiveProgram = getIncentiveProgramById(id);
        Integer status = incentiveProgram.getStatus();
        status = status == null ? 1 : status == 0 ? 1 : 0;
        if (incentiveProgram.getType() != IncentiveType.COUPON.value()) {
            if (status == 1 && incentiveProgramRepository.isExistsPackingProductsIncentivePrograms(id))
                throw new ActivateIncentiveProgramException(messageResolverService.getMessage("incentiveProgram.existsPackingProduct"));
        }

        incentiveProgram.setStatus(status);
        incentiveProgramRepository.save(incentiveProgram);
        return incentiveProgramMapper.toIncentiveProgramDto(incentiveProgram);
    }

    @Override
    @Transactional
    public IncentiveProgramDto updateIncentiveProgramDetailInformation(Long id, IncentiveProgramDetailInformationDto detailInformationDto) {
        IncentiveProgram incentiveProgram = getIncentiveProgramById(id);
        IncentiveProgramDetailInformation detailInformation = incentiveProgramDetailInformationRepository.findByIncentiveProgramId(id);

        if (detailInformation == null) {
            IncentiveProgramDetailInformation incentiveProgramDetailInformation = incentiveProgramDetailInformationMapper.toIncentiveProgramDetailInformation(detailInformationDto);
            incentiveProgramDetailInformation.setIncentiveProgramId(id);
            incentiveProgramDetailInformation.setCreatedUser(SpringUtils.getCurrentUsername());
            incentiveProgramDetailInformation.setCreatedDate(LocalDateTime.now());
            incentiveProgram.setIncentiveProgramDetailInformation(incentiveProgramDetailInformation);
            incentiveProgramDetailInformationRepository.save(incentiveProgramDetailInformation);
        } else {
            map(detailInformationDto, detailInformation);
            detailInformation.setIncentiveProgramId(id);
            detailInformation.setLatestModifiedUser(SpringUtils.getCurrentUsername());
            detailInformation.setLatestModifiedDate(LocalDateTime.now());
            incentiveProgramDetailInformationRepository.save(detailInformation);
        }

        incentiveProgram.setLatestModifiedUser(SpringUtils.getCurrentUsername());
        incentiveProgram.setLatestModifiedDate(LocalDateTime.now());
        incentiveProgramRepository.save(incentiveProgram);
        return incentiveProgramMapper.toIncentiveProgramDto(incentiveProgram);
    }

    private void map(IncentiveProgramDetailInformationDto detailInformationDto,
                     IncentiveProgramDetailInformation detailInformation) {
        detailInformation.setCategoryId(detailInformation.getCategoryId());
        detailInformation.setManufacturerId(detailInformation.getManufacturerId());
        detailInformation.setPackingId(detailInformationDto.getPackingId());
        detailInformation.setDiscountType(detailInformationDto.getDiscountType());
        detailInformation.setDiscountPrice(detailInformationDto.getDiscountPrice());
        detailInformation.setDiscountPercent(detailInformationDto.getDiscountPercent());
        detailInformation.setLimitedQuantity(detailInformationDto.getLimitedQuantity());
        detailInformation.setIsFlashSale(detailInformationDto.getIsFlashSale());
        detailInformation.setBonusPackingId(detailInformationDto.getBonusPackingId());
        detailInformation.setRequiredMinimumPurchaseQuantity(detailInformationDto.getRequiredMinimumPurchaseQuantity());
        detailInformation.setBonusPackingQuantity(detailInformationDto.getBonusPackingQuantity());
        detailInformation.setLimitedPackingQuantity(detailInformationDto.getLimitedPackingQuantity());
    }

    @Override
    @Transactional
    public IncentiveProgramDto updateIncentiveProgramGroup(Long id, IncentiveProgramGroupDto incentiveProgramGroupDto) {
        IncentiveProgram incentiveProgram = getIncentiveProgramById(id);
        IncentiveProgramGroup incentiveProgramGroup = incentiveProgram.getIncentiveProgramGroup();
        String currentUsername = SpringUtils.getCurrentUsername();
        LocalDateTime now = LocalDateTime.now();

        if (incentiveProgramGroup == null) {
            IncentiveProgramGroup newIncentiveProgramGroup = new IncentiveProgramGroup();
            newIncentiveProgramGroup.setIncentiveProgramId(id);
            newIncentiveProgramGroup.setCategoryId(incentiveProgramGroupDto.getCategoryId());
            newIncentiveProgramGroup.setManufacturerId(incentiveProgramGroupDto.getManufacturerId());
            newIncentiveProgramGroup.setDiscountType(incentiveProgramGroupDto.getDiscountType());
            newIncentiveProgramGroup.setDiscountPercent(incentiveProgramGroupDto.getDiscountPercent());
            newIncentiveProgramGroup.setDiscountPrice(incentiveProgramGroupDto.getDiscountPrice());
            newIncentiveProgramGroup.setDefaultLimitedQuantity(incentiveProgramGroupDto.getDefaultLimitedQuantity());
            incentiveProgram.setIncentiveProgramGroup(newIncentiveProgramGroup);
        } else {
            incentiveProgramGroup.setCategoryId(incentiveProgramGroupDto.getCategoryId());
            incentiveProgramGroup.setManufacturerId(incentiveProgramGroupDto.getManufacturerId());
            incentiveProgramGroup.setDiscountType(incentiveProgramGroupDto.getDiscountType());
            incentiveProgramGroup.setDiscountPercent(incentiveProgramGroupDto.getDiscountPercent());
            incentiveProgramGroup.setDiscountPrice(incentiveProgramGroupDto.getDiscountPrice());
            incentiveProgramGroup.setDefaultLimitedQuantity(incentiveProgramGroupDto.getDefaultLimitedQuantity());
        }
        incentiveProgram.setLatestModifiedUser(currentUsername);
        incentiveProgram.setLatestModifiedDate(now);

        IncentiveProgram updatedIncentiveProgram = incentiveProgramRepository.save(incentiveProgram);
        IncentiveProgramGroup incentiveProgramGroupInDB = updatedIncentiveProgram.getIncentiveProgramGroup();
        incentiveProgramDetailInformationRepository.deleteByIncentiveProgramId(id);

        List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroupDto = incentiveProgramGroupDto.getIncentiveProgramDetailInformationGroup();
        List<IncentiveProgramDetailInformation> newIncentiveProgramDetailInformationGroup = incentiveProgramDetailInformationGroupDto.stream()
                .map(incentiveProgramDetailInformationDto -> {
                    IncentiveProgramDetailInformation incentiveProgramDetailInformation = new IncentiveProgramDetailInformation();
                    incentiveProgramDetailInformation.setIncentiveProgramId(id);
                    incentiveProgramDetailInformation.setIncentiveProgramGroupId(incentiveProgramGroupInDB.getId());
                    incentiveProgramDetailInformation.setCategoryId(incentiveProgramGroupDto.getCategoryId());
                    incentiveProgramDetailInformation.setManufacturerId(incentiveProgramGroupDto.getManufacturerId());
                    incentiveProgramDetailInformation.setDiscountType(incentiveProgramGroupDto.getDiscountType());
                    incentiveProgramDetailInformation.setDiscountPrice(incentiveProgramGroupDto.getDiscountPrice());
                    incentiveProgramDetailInformation.setDiscountPercent(incentiveProgramGroupDto.getDiscountPercent());
                    incentiveProgramDetailInformation.setPackingId(incentiveProgramDetailInformationDto.getPackingId());
                    incentiveProgramDetailInformation.setLimitedQuantity(incentiveProgramDetailInformationDto.getLimitedQuantity());
                    incentiveProgramDetailInformation.setCreatedUser(currentUsername);
                    incentiveProgramDetailInformation.setCreatedDate(now);
                    return incentiveProgramDetailInformation;
                }).collect(Collectors.toList());
        incentiveProgramDetailInformationRepository.saveAll(newIncentiveProgramDetailInformationGroup);

        IncentiveProgram incentiveProgramInDB = incentiveProgramRepository.getOne(id);
        return incentiveProgramMapper.toIncentiveProgramDto(incentiveProgramInDB);
    }

    @Override
    public boolean isExistsPackingIdInOtherIncentivePrograms(Long id, Long packingId) {
        return incentiveProgramRepository.isExistsPackingIdInOtherIncentivePrograms(id, packingId);
    }

    @Override
    @Transactional
    public CouponIncentiveDTO updateCouponCart(Long id, CouponIncentiveDTO couponIncentiveDTO) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", id);
        List<CouponIncentive> list = findAll(null, CouponIncentive.class, "Select * from coupon_incentive where incentive_program_id = :id ", params);
        CouponIncentive couponIncentive;
        if (Utils.isNotNull(list)) {
            // Chỉnh sửa
            couponIncentive = list.get(0);
        } else {
            // thêm mới
            couponIncentive = new CouponIncentive();
        }
        Integer type = couponIncentiveDTO.getType();
        couponIncentive.setRequiredMinimumAmount(couponIncentiveDTO.getRequiredMinimumAmount());
        couponIncentive.setIncentiveProgramId(id);
        couponIncentive.setCode(couponIncentiveDTO.getCode());
        if (type == DiscountType.DISCOUNT_PERCENT.value()) {
            couponIncentive.setDiscountPercent(couponIncentiveDTO.getDiscountPercent());
            couponIncentive.setLimitedPrice(couponIncentiveDTO.getLimitedPrice());
        } else if (type == DiscountType.DISCOUNT_PRICE.value()) {
            couponIncentive.setDiscountPrice(couponIncentiveDTO.getDiscountPrice());
        }
        couponIncentive.setLimitedQuantity(couponIncentiveDTO.getLimitedQuantity());
        couponIncentive.setCreateDate(LocalDateTime.now());
        couponIncentive.setCreateUser(SpringUtils.getCurrentUsername());
        couponIncentive.setType(type);
        save(couponIncentive);

        return incentiveProgramMapper.toCouponIncentiveDTO(couponIncentive);
    }

    @Override
    public CouponIncentiveDTO getCouponCart(Long id) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", id);
        String selectCoupon = "Select c.incentive_program_id incentiveProgramId, c.code code, c.type type," +
                " c.required_minimum_amount requiredMinimumAmount, c.limited_quantity limitedQuantity," +
                " c.discount_percent discountPercent, c.discount_price discountPrice," +
                " c.limited_price limitedPrice from coupon_incentive c" +
                " where c.incentive_program_id = :id";
        List<CouponIncentiveDTO> couponIncentiveDTOList = findAll(null, CouponIncentiveDTO.class, selectCoupon, params);
        return (Utils.isNotNull(couponIncentiveDTOList)) ? couponIncentiveDTOList.get(0) : null;
    }

    @Override
    @Transactional
    public IncentiveProgramDto updateIncentiveProgramMerchantGroup(Long id, IncentiveProgramDto incentiveProgramDto) {
        incentiveProgramMerchantGroupRepository.deleteByIncentiveProgramId(id);
        List<Long> merchantGroupIds = incentiveProgramDto.getMerchantGroupIds();
        if (Utils.isNotEmpty(merchantGroupIds)) {{
            List<IncentiveProgramMerchantGroup> incentiveProgramMerchantGroups = merchantGroupIds.stream()
                    .map(merchantGroupId -> new IncentiveProgramMerchantGroup(id, merchantGroupId))
                    .collect(Collectors.toList());
            incentiveProgramMerchantGroupRepository.saveAll(incentiveProgramMerchantGroups);

            incentiveProgramSelectedWarehouseRepository.deleteByIncentiveProgramId(id);
        }}
        List<String> warehouseCodes = incentiveProgramDto.getWarehouseCodes();
        if (Utils.isNotEmpty(warehouseCodes)) {
            List<IncentiveProgramSelectedWarehouse> incentiveProgramSelectedWarehouses = warehouseCodes.stream()
                    .map(warehouseCode -> new IncentiveProgramSelectedWarehouse(id, warehouseCode))
                    .collect(Collectors.toList());
            incentiveProgramSelectedWarehouseRepository.saveAll(incentiveProgramSelectedWarehouses);
        }

        IncentiveProgram incentiveProgram = incentiveProgramRepository.getOne(id);
        incentiveProgram.setApplyL2(incentiveProgramDto.getApplyL2());
        incentiveProgramRepository.save(incentiveProgram);

        return getIncentiveProgram(id);
    }

    @Override
    @Transactional
    public IncentiveProgramDto updateIncentiveProgramFlashSale(Long id, List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroupDto) {
        incentiveProgramDetailInformationRepository.deleteByIncentiveProgramId(id);
        incentiveProgramFlashSaleDateRepository.deleteByIncentiveProgramDetailInformationId(id);

        incentiveProgramDetailInformationGroupDto
                .stream()
                .map(incentiveProgramDetailInformationMapper::toIncentiveProgramDetailInformation)
                .forEach(incentiveProgramDetailInformation -> {
                    incentiveProgramDetailInformation.setIncentiveProgramId(id);
                    incentiveProgramDetailInformation.setIsFlashSale(true);
                    IncentiveProgramDetailInformation createdProgramDetailInformation = incentiveProgramDetailInformationRepository.save(incentiveProgramDetailInformation);
                    incentiveProgramDetailInformation.getFlashSaleDates().forEach(incentiveProgramFlashSaleDate -> {
                        incentiveProgramFlashSaleDate.setIncentiveProgramDetailInformationId(createdProgramDetailInformation.getId());
                        incentiveProgramFlashSaleDateRepository.save(incentiveProgramFlashSaleDate);
                    });
                });

        return getIncentiveProgram(id);
    }
}
