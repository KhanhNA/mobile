package com.nextsolutions.dcommerce.service.impl;

import java.nio.charset.Charset;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.nextsolutions.dcommerce.configuration.properties.PaymentGatewayConfiguration;
import com.nextsolutions.dcommerce.service.PaymentGatewayService;
import com.nextsolutions.dcommerce.shared.dto.wallet.TransferRequestBuilder;
import com.nextsolutions.dcommerce.shared.dto.wallet.WalletAccountRequestBuilder;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PaymentGatewayServiceImpl implements PaymentGatewayService {

    private final RestTemplate walletRestTemplate;
    private final PaymentGatewayConfiguration paymentGatewayConfiguration;

    @Override
    public Long createWalletAccount(WalletAccountRequestBuilder accountRequest) {
        try {
            ResponseEntity<Map<String, Object>> response = walletRestTemplate.exchange(
                    paymentGatewayConfiguration.getClientsUrl(),
                    HttpMethod.POST,
                    new HttpEntity<>(accountRequest, getHeaders()),
                    new ParameterizedTypeReference<Map<String, Object>>() {
                    });
            Map<String, Object> responseBody = response.getBody();
            assert responseBody != null;
            return Long.parseLong("" + responseBody.get("clientId"));
        } catch (HttpServerErrorException e) {
            throw new HttpServerErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        }
    }

    @Override
    public Map<String, Object> updateWalletAccount(Long id, WalletAccountRequestBuilder accountRequest) {
        try {
            ResponseEntity<Map<String, Object>> response = walletRestTemplate.exchange(
                    paymentGatewayConfiguration.getClientsUrl() + "/{clientId}",
                    HttpMethod.POST,
                    new HttpEntity<>(accountRequest, getHeaders()),
                    new ParameterizedTypeReference<Map<String, Object>>() {
                    }, id);
            return response.getBody();
        } catch (HttpServerErrorException e) {
            throw new HttpServerErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        }
    }

    @Override
    public boolean transfer(TransferRequestBuilder transferRequest, HttpHeaders header) {
        try {
            walletRestTemplate.exchange(
                    paymentGatewayConfiguration.getTransferUrl(),
                    HttpMethod.POST,
                    new HttpEntity<>(transferRequest, header),
                    String.class);
            return true;
        } catch (HttpServerErrorException e) {
            throw new HttpServerErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        }
    }

    @Override
    public boolean existsWalletAccount(Long walletId) {
        try {
            ResponseEntity<Object> response = walletRestTemplate.exchange(
                    paymentGatewayConfiguration.getClientsUrl() + "/{clientId}",
                    HttpMethod.GET,
                    new HttpEntity<>(getHeaders()),
                    Object.class,
                    walletId);
            return response.getStatusCode().is2xxSuccessful();
        } catch (HttpServerErrorException e) {
            throw new HttpServerErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        }
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("mifos", "password");
        return headers;
    }
}
