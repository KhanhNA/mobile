package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.incentive.IncentiveLevel;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingIncentive;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingSale;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IncentivePackingSaleService extends CommonService {
	Page<IncentivePackingSale> getPackingSale(Boolean isPackingGroup, Long ictPgId, Pageable pageable, Long status);
	void saveIctPackingIct(List<IncentivePackingIncentive> dtos, Long lvlId) throws Exception;
	void savePackingSale(List<IncentivePackingSale> ictPackingSales, Long ictPrgId, Integer type) throws Exception;
	void saveIctLvl(List<IncentiveLevel> dtos, Long sId) throws Exception;
	void delIctLvl(Long id) throws Exception;
	void delIctPackingIct(Long id) throws Exception;
	void delIctPackingSale(Long id) throws Exception;

	Page<PackingProduct> list(Pageable pageable, List<Long> exceptIds, String text) throws Exception;

	void saveIncentiveLvlForOrderValue(List<IncentiveLevel> dtos, Long loyaltyId) throws Exception;

	Page<IncentiveLevel> getIncentiveLvlList(Long loyaltyId, Long status, Pageable pageable);
}
