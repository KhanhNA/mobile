package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Language;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.incentive.Incentive;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingGroup;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingSale;
import com.nextsolutions.dcommerce.model.incentive.IncentiveProgramDescription;
import com.nextsolutions.dcommerce.service.IncentiveService;
import com.nextsolutions.dcommerce.shared.dto.*;
import com.nextsolutions.dcommerce.shared.dto.coupon.CouponIncentiveDTO;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.StringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class IncentiveServiceImpl extends CommonServiceImpl implements IncentiveService {

    private final AppConfiguration appConfiguration;
    static final String INCENTIVE_DESCRIPTION_IMAGES_FOLDER = "images/incentive/descriptions";
    private HashMap<String, Object> params;

    @Override
    public List<Incentive> getIctPrgFromPackingId(Long ictPrgId) throws Exception {
        String sql = "select * " +
                "from incentive_program ip " +
                "where ip.id in( " +
                "select incentive_program_id from incentive_packing_sale " +
                " where OBJ_TYPE = 1 and PACKING_PRODUCT_ID = :packingId " +
                "		and from_date <= curdate() and (to_date is null or to_date >= curDate()) " +
                "union all " +
                "select s.incentive_program_id " +
                "from incentive_packing_sale s join incentive_packing_group_packing p on s.incentive_packing_group_id = p.id " +
                "where OBJ_TYPE = 2 and p.PACKING_PRODUCT_ID = :packingId1 and s.from_date >= curdate()" +
                " and (s.to_date is null or s.to_date >= curDate())" +
                ")";
        HashMap<String, Object> params = new HashMap<>();
        params.put("packingId", ictPrgId);
        params.put("packingId1", ictPrgId);
        return findAll(null, Incentive.class, sql, params);
    }

    public Incentive insert(IncentiveProgramDto dto) throws Exception {
        Incentive entity = null;

        if (dto.id == null) {
            entity = new Incentive();
        } else {
            entity = find(Incentive.class, dto.id);
            LocalDateTime fromDate = entity.getFromDate();
            if (fromDate != null) {
                if (!fromDate.equals(dto.getFromDate())) {
                    throw new CustomErrorException("Can'tChangeFromDate");
                }
            }

            LocalDateTime toDate = entity.getToDate();
            if (toDate != null) {
                if (toDate.isBefore(LocalDateTime.now())) {
                    throw new CustomErrorException("ExpiredLoyalty");
                }
            }
        }
        entity = Utils.getData(dto, Incentive.class);
//        entity.setId(null);
        entity.setStatus(Incentive.STATUS_DRAFT);

        //default ten hien thi la tieng viet
        entity.setName(dto.getDescriptions().get("vi").getName());
        entity = save(entity);

        Iterator iterator = Utils.safe(dto.descriptions).entrySet().iterator();
        List<IncentiveProgramDescription> descriptions = new ArrayList<>();

        IncentiveProgramDescription description;
        HashMap<String, Language> langs = loadLanguages("");
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            description = (IncentiveProgramDescription) entry.getValue();
            description.setIncentiveProgramId(entity.getId());
            description.setLangCode((String) entry.getKey());
            description.setLangId(langs.get((String) entry.getKey()).getId());
            save(description);
        }

        //neu la khuyen mai don hang thi insert vao incentivePackingGroup 1 bang ghi mac dinh khuyen mai don hang
        // insert vao incentivePackingSale 1 ban ghi mac dinh khuyen mai theo don hang
        if (dto.getType() != 2) { //km don hang
            return entity;
        }

        IncentivePackingGroup ictPackingGroup = findPackingGroup(entity);
        if (ictPackingGroup == null) {
            ictPackingGroup = new IncentivePackingGroup();
        }
        ictPackingGroup.setIsAssign(1);
        ictPackingGroup.setStatus(1);
        ictPackingGroup.setCode("ORDER");
        ictPackingGroup.setName("Promotion on order");
        ictPackingGroup.setCreateDate(LocalDateTime.now());
        ictPackingGroup.setFromDate(entity.getFromDate());
        ictPackingGroup.setToDate(entity.getToDate());
        ictPackingGroup.setCreateUser("thonv");
        ictPackingGroup.setIctProgramId(entity.getId());
        ictPackingGroup = save(ictPackingGroup);
        //incentivePackingSale
        IncentivePackingSale ictPackingSale = findPackingSale(entity);
        if (ictPackingSale == null) {
            ictPackingSale = new IncentivePackingSale();
        }
        ictPackingSale.setFromDate(entity.getFromDate());
        ictPackingSale.setToDate(entity.getToDate());
        ictPackingSale.setStatus(1L);
        ictPackingSale.setIctPackingGroup(ictPackingGroup);
        ictPackingSale.setIncentiveProgramId(entity.getId());
        ictPackingSale.setObjType(3);//loai khuyen mai theo don hang
        ictPackingSale.setCreateDate(LocalDateTime.now());
        ictPackingSale.setCreateUser("thonv");
        save(ictPackingSale);
        return entity;
    }

    @Override
    public Map<String, String> uploadFile(MultipartFile imageFile) throws IOException {
        Map<String, String> map = new HashMap<>();
        String originalFilename = imageFile.getOriginalFilename();
        String name = UUID.randomUUID().toString().replaceAll("-", "");
        String extension = Objects.requireNonNull(originalFilename).replaceAll("(.*)(\\..*)", "$2");
        BufferedImage defaultImage = ImageIO.read(imageFile.getInputStream());
        writeImage(defaultImage, extension.substring(1), name + "-default" + extension);
        map.put("default", this.generateImageUrl(INCENTIVE_DESCRIPTION_IMAGES_FOLDER, name + "-default" + extension));
        return map;
    }

    @Override
    public List<IncentiveDescriptionDTO> getIncentiveFromId(List<Long> ids, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        String sql = "SELECT ip.id incentiveProgramId, ip.code, ipd.name , ipd.description, ipd.short_description ,ip.TYPE incentiveType " +
                "FROM incentive_program_v2 ip JOIN incentive_program_description_v2 ipd ON ip.ID = ipd.incentive_program_id AND ipd.language_id = :langId " +
                "where ip.id in(" + Utils.join(",", ids) + ")";
        return findAll(null, IncentiveDescriptionDTO.class, sql, langId);
    }

    @Override
    public List<CouponIncentiveDTO> getCouponCode(Long merchantId, Long langId) throws Exception {
        String sql = "SELECT ipd.name,ipd.description incentiveDescription, ipd.short_description ,ip.from_date,ip.to_date,ci.* " +
                " from incentive_program_v2 ip " +
                " JOIN incentive_program_merchant_group ipmg ON ipmg.incentive_program_id = ip.id" +
                " JOIN merchant_group_merchant mgm on mgm.merchant_group_id = ipmg.merchant_group_id and mgm.merchant_id = :merchantId" +
                " JOIN coupon_incentive ci on ci.incentive_program_id = ip.id" +
                " LEFT JOIN incentive_program_description_v2 ipd on ipd.incentive_program_id  = ip.id and ipd.language_id = :langId" +
                " where " +
                " ip.type = 3 and ip.status =1 " +
                " AND DATE(ip.FROM_DATE) <= DATE(CURRENT_DATE()) " +
                " AND DATE(ip.TO_DATE) >= DATE(CURRENT_DATE()) " +
                " AND (select count(*) from coupon_merchant_his where coupon_code = ci.code and merchant_id = :merchant_id ) < ci.limited_quantity " +
                " GROUP by ip.id ";
        return findAll(null, CouponIncentiveDTO.class, sql, merchantId, langId, merchantId);
    }

    @Override
    public List<IncentiveDescriptionDTO> getIncentive(Long merchantId, Long langId) throws Exception {
        String sql = "SELECT ip.id incentiveProgramId,ipd.name,ip.TYPE incentiveType, ipd.short_description ,ipd.image_url,ip.FROM_DATE,ip.TO_DATE,ipd.description," +
                " ipdi.manufacturer_id,ipdi.category_id," +
                "    CASE " +
                "    when ip.type = 2 then ipdi.packing_id" +
                "    else null END productPackingId," +
                "    CASE" +
                "    when ip.type = 2 then pp.PRODUCT_ID" +
                "    else null" +
                "    END productId " +
                " FROM incentive_program_v2 ip" +
                " JOIN incentive_program_description_v2 ipd on ipd.incentive_program_id = ip.id and ipd.language_id = :langId" +
                " JOIN incentive_program_merchant_group ipmg on ip.id = ipmg.incentive_program_id " +
                " JOIN merchant_group_merchant mgm on mgm.merchant_group_id = ipmg.merchant_group_id and mgm.merchant_id = :merchantId" +
                " JOIN incentive_program_detail_information ipdi ON ipdi.incentive_program_id = ip.id" +
                " JOIN packing_product pp ON pp.PACKING_PRODUCT_ID = ipdi.packing_id" +
                " where " +
                " ip.status =1" +
                " AND DATE(ip.FROM_DATE) <= DATE(CURRENT_DATE())" +
                " AND DATE(ip.TO_DATE) >= DATE(CURRENT_DATE()) " +
                " GROUP BY ip.id";

        return findAll(null, IncentiveDescriptionDTO.class, sql, langId, merchantId);
    }

    @Override
    public List<IncentiveAppliedDTO> getIncentiveProgramAppliedFromProductId(Long merchantId, Long productId, List<Integer> lstType, Integer displayArea, Long langId) throws Exception {
        params = new HashMap<>();
        StringBuilder sqlRun = new StringBuilder();
        String sqlRoot = "SELECT ip.id incentiveProgramId,ip.type incentiveType,ipdi.packing_id productPackingId,ipd.name,ipd.description" +
                " FROM incentive_program_merchant_group ipmg " +
                " JOIN merchant_group_merchant mgm ON ipmg.merchant_group_id = mgm.merchant_group_id " +
                " JOIN incentive_program_v2 ip ON ip.id = ipmg.incentive_program_id AND mgm.MERCHANT_ID = :merchantId " +
                " JOIN incentive_program_detail_information ipdi  on ipdi.incentive_program_id = ip.id " +
                " JOIN incentive_program_description_v2 ipd on ip.ID = ipd.incentive_program_id and ipd.language_id =:langId " +
                " WHERE " +
                " ip.status = 1 " +
                " AND DATE(ip.FROM_DATE) <= DATE(CURRENT_DATE()) " +
                " AND DATE(ip.TO_DATE) >= DATE(CURRENT_DATE())" +
                " AND ipdi.packing_id in (select packing_product_id from packing_product where product_id = " + productId + ")";
        params.put("merchantId", merchantId);
        params.put("langId", langId);
        sqlRun.append(sqlRoot);
        if (Utils.isNotNull(lstType)) {
            sqlRun.append(" AND ip.type IN (").append(Utils.join(",", lstType)).append(")");
        }
        if (displayArea != null) {
            sqlRun.append(" AND ip.DISPLAY_AREA = :displayArea ");
            params.put("displayArea", displayArea);
        }
        sqlRun.append(" GROUP BY ip.id,ipdi.packing_id ");
        return findAll(null, IncentiveAppliedDTO.class, sqlRun.toString(), params);
    }

    @Override
    public IncentiveAppliedDTO getIncentiveProgramAppliedFromPackingId(Long merchantId, Long packingId, List<Integer> lstType, Integer displayArea, Long langId) throws Exception {
        params = new HashMap<>();
        StringBuilder sqlRun = new StringBuilder();
        String sqlRoot = "SELECT ip.id incentiveProgramId,ip.type incentiveType, " +
                " ipdi.packing_id productPackingId,ipd.name,ipd.description,ipdi.discount_type " +
                " FROM incentive_program_merchant_group ipmg " +
                " JOIN merchant_group_merchant mgm ON ipmg.merchant_group_id = mgm.merchant_group_id " +
                " JOIN incentive_program_v2 ip ON ip.id = ipmg.incentive_program_id AND mgm.MERCHANT_ID = :merchantId " +
                " JOIN incentive_program_detail_information ipdi  on ipdi.incentive_program_id = ip.id " +
                " JOIN incentive_program_description_v2 ipd on ip.ID = ipd.incentive_program_id and ipd.language_id =:langId " +
                " WHERE " +
                " ip.status = 1 " +
                " %s " +
                " and DATE(ip.from_date) <= DATE(CURRENT_DATE()) " +
                " and ( ip.to_date is null " +
                " or DATE(ip.TO_DATE) >= DATE(current_date())) " +
                " AND ipdi.packing_id = :packingProductId";

        Long paramMerchantId = checkMerchantId(merchantId);
        params.put("merchantId", paramMerchantId);

        params.put("langId", langId);
        params.put("packingProductId", packingId);
        sqlRun.append(sqlRoot);
        if (Utils.isNotNull(lstType)) {
            sqlRun.append(" AND ip.type IN (").append(Utils.join(",", lstType)).append(")");
        }
        if (displayArea != null) {
            sqlRun.append(" AND ip.DISPLAY_AREA = :displayArea ");
            params.put("displayArea", displayArea);
        }
        sqlRun.append(" GROUP BY ip.id,ipdi.packing_id ");

        String subWhereQuery = merchantId.equals(paramMerchantId) ? " " : " AND ip.apply_l2 = true ";
        return findOne(IncentiveAppliedDTO.class, String.format(sqlRun.toString(), subWhereQuery), params);
    }

    @Override
    public List<IncentiveAppliedDTO> getBannerList(Long merchantId, Long langId) throws Exception {
        Long paramMerchantId = checkMerchantId(merchantId);

        String sql = "SELECT ip.id incentiveProgramId,ip.type incentiveType,ipd.name,ipd.image_url,ipd.description," +
                " ipdi.category_id,ipdi.manufacturer_id," +
                " CASE " +
                " WHEN ip.type = 2 THEN ipdi.packing_id" +
                " else null" +
                " END productPackingId," +
                " CASE " +
                " WHEN ip.type = 2 THEN pp.product_id" +
                " else null" +
                " END productId" +
                " FROM incentive_program_merchant_group ipmg " +
                " JOIN merchant_group_merchant mgm ON ipmg.merchant_group_id = mgm.merchant_group_id " +
                " JOIN incentive_program_v2 ip ON ip.id = ipmg.incentive_program_id AND mgm.MERCHANT_ID = :merchantId" +
                " JOIN incentive_program_detail_information ipdi on ipdi.incentive_program_id  = ip.id" +
                " JOIN packing_product pp on pp.PACKING_PRODUCT_ID = ipdi.packing_id" +
                " JOIN incentive_program_description_v2 ipd on ip.ID = ipd.incentive_program_id and ipd.language_id =:langId " +
                " WHERE " +
                " ip.status = 1 " +
                " and DATE(ip.from_date) <= DATE(CURRENT_DATE()) and ( ip.to_date is null or DATE(ip.TO_DATE ) >=DATE(current_date())) " +
                " AND ip.DISPLAY_AREA = 2 " +
                " AND (ip.type = 1 or ((ip.type = 2 and ipdi.discount_type in(1,2) and ifnull(ipdi.sold,0) <ipdi.limited_quantity)) or ((ip.type = 2 and ipdi.discount_type = 3 " +
                " AND ifnull(ipdi.sold,0) <ipdi.limited_packing_quantity))) " +
                " %s " +
                "  GROUP BY ip.id ";
        String subQuery = !paramMerchantId.equals(merchantId) ? " AND ip.apply_l2 = true " : " ";
        return findAll(null, IncentiveAppliedDTO.class, String.format(sql, subQuery), paramMerchantId, langId);
    }

    private Long checkMerchantId(Long merchantId) {
        Merchant merchant = find(Merchant.class, merchantId);
        Long paramMerchantId = merchantId;
        if (merchant != null) {
            if (merchant.getMerchantTypeId() == 2) {
                paramMerchantId = merchant.getParentMarchantId();
            }
        }
        return paramMerchantId;
    }

    @Override
    public DataFlashSaleDTO getPackingFlashSaleCurrentTime(Pageable pageable, Long merchantId, Long langId) throws Exception {
        DataFlashSaleDTO dataFlashSaleDTO = new DataFlashSaleDTO();
        CurrentTimeFlashSaleDTO timeFlashSaleDTO = flashSaleCurrentTime();
        Long paramMerchantId = checkMerchantId(merchantId);
        String subWhereQuery = paramMerchantId.equals(merchantId) ? " " : " AND ip.apply_l2 = true ";
        String sql = String.format(getPackingFlashSaleByTime(timeFlashSaleDTO.getSqlWhere()), subWhereQuery, " limit 10 ");
        dataFlashSaleDTO.setDuration(timeFlashSaleDTO.getDuration());

        params = new HashMap<>();
        params.put("merchantId", paramMerchantId);
        params.put("langId", langId);
        dataFlashSaleDTO.setLstPacking(findByNative(null, PackingProductFLashSaleDTO.class, sql, params));
        return dataFlashSaleDTO;
    }

    @Override
    public DataFlashSaleDTO getFlashSaleByTime(Pageable pageable, Long merchantId, String milestone, Long langId) throws Exception {
        CurrentTimeFlashSaleDTO timeFlashSaleDTO = flashSaleCurrentTime();
        DataFlashSaleDTO dataFlashSaleDTO = new DataFlashSaleDTO();
        String sqlWhere = convertMilestoneToSql(milestone);
        if (StringUtils.isEmpty(sqlWhere)) {
            return null;
        }
        Long paramMerchantId = checkMerchantId(merchantId);
        String subWhereQuery = paramMerchantId.equals(merchantId) ? " " : " AND ip.apply_l2 = true ";
        String sql = String.format(getPackingFlashSaleByTime(sqlWhere), subWhereQuery, " ");
        dataFlashSaleDTO.setDuration(timeFlashSaleDTO.getMilestone().equalsIgnoreCase(milestone) ? timeFlashSaleDTO.getDuration() : -1);
        dataFlashSaleDTO.setLstPacking(findAll(pageable, PackingProductFLashSaleDTO.class, sql, paramMerchantId, langId));
        return dataFlashSaleDTO;
    }

    @Override
    public PackingProductDTO getPackingFlashSaleByIncentiveId(Long incentiveId, Long packingId, String flashSaleTime) throws Exception {

        String sql = "SELECT " +
                " pp.PRODUCT_ID," +
                " pp.PACKING_PRODUCT_ID," +
                " pp.code packingProductCode," +
                " ipdi.discount_type," +
                " ipdi.discount_price, ipdi.sold,ipdi.limited_quantity,ipdi.limited_packing_quantity,  " +
                " CASE" +
                " WHEN ipdi.discount_type = 1 THEN ipdi.discount_percent" +
                " WHEN ipdi.discount_type = 2 THEN (ipdi.discount_price / pprice.price) * 100" +
                " ELSE NULL" +
                "    END discountPercent," +
                "    pprice.price," +
                "    pp.MARKET_PRICE orgPrice," +
                "    pt.quantity," +
                "    pp.packing_url" +
                " FROM incentive_program_detail_information ipdi " +
                " JOIN incentive_program_flash_sale_date fsd ON fsd.incentive_program_detail_information_id = ipdi.id" +
                " JOIN incentive_program_v2 ip ON ip.id = ipdi.incentive_program_id  and ip.id = :incentiveId " +
                " JOIN packing_product pp ON pp.PACKING_PRODUCT_ID = ipdi.packing_id and pp.packing_product_id  = :packingId" +
                " JOIN packing_type pt ON pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID" +
                " join packing_price pprice on " +
                " pprice.packing_product_id = pp.packing_product_id " +
                " WHERE" +
                " ipdi.is_flash_sale = 1 AND ip.status = 1" +
                " AND DATE(fsd.date) = DATE(CURRENT_DATE())" +
                " and date(current_date()) >= date(pprice.from_date) " +
                " and (date(pprice.to_date)>= date(current_date()) " +
                " or pprice.to_date is null) and pprice.price_type =2" +
                flashSaleTime +
                " GROUP BY ip.id , pp.PACKING_PRODUCT_ID";
        return findOne(PackingProductDTO.class, sql, incentiveId, packingId);
    }

    @Override
    public CurrentTimeFlashSaleDTO flashSaleCurrentTime() {
        //TODO: Sua lai mui gio server
        int index;
        String[] timeline = new String[]{"zeroToNine", "nineToTwelve", "twelveToFifteen", "fifteenToEighteen", "eighteenToTwentyOne", "twentyOneToTwentyFour"};
        CurrentTimeFlashSaleDTO saleDate = new CurrentTimeFlashSaleDTO();
        Calendar nowCalendar = SpringUtils.getCurrentTime();
        int hour = nowCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = nowCalendar.get(Calendar.MINUTE);
        int second = nowCalendar.get(Calendar.SECOND);
        int totalSecond = hour * 60 * 60 + minute * 60 + second;
        if (totalSecond <= 32400) {
            saleDate.setSqlWhere(" and fsd.zero_to_nine =1");
            saleDate.setDuration(32400 - totalSecond);
            index = 1;
        } else if (totalSecond <= 43200) {
            saleDate.setSqlWhere(" and fsd.nine_to_twelve =1");
            saleDate.setDuration(43200 - totalSecond);
            index = 2;
        } else if (totalSecond <= 54000) {
            saleDate.setSqlWhere(" and fsd.twelve_to_fifteen =1");
            saleDate.setDuration(54000 - totalSecond);
            index = 3;
        } else if (totalSecond <= 64800) {
            saleDate.setSqlWhere(" and fsd.fifteen_to_eighteen =1");
            saleDate.setDuration(64800 - totalSecond);
            index = 4;
        } else if (totalSecond <= 75600) {
            saleDate.setSqlWhere(" and fsd.eighteen_to_twenty_one =1");
            saleDate.setDuration(75600 - totalSecond);
            index = 5;
        } else {
            saleDate.setSqlWhere(" and fsd.twenty_one_to_twenty_four =1");
            saleDate.setDuration(86400 - totalSecond);
            index = 6;
        }
        for (int i = index; i <= timeline.length; i++) {
            saleDate.getTimeline().add(timeline[i - 1]);
        }
        saleDate.setMilestone(timeline[index - 1]);
        return saleDate;
    }


    @Override
    public List<Long> getIncentiveIdApplied(Long merchantId, List<Integer> lstType, List<Integer> displayArea, Boolean notInDisplay) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        StringBuilder sqlRun = new StringBuilder();
        String sqlRoot = "SELECT " +
                " ipmg.incentive_program_id " +
                " FROM " +
                " incentive_program_merchant_group ipmg " +
                " JOIN" +
                " merchant_group_merchant mgm ON ipmg.merchant_group_id = mgm.merchant_group_id " +
                " join incentive_program_v2 ip on ip.id = ipmg.incentive_program_id " +
                " AND mgm.MERCHANT_ID = :merchantId " +
                " where  ip.status = 1 " +
                " %s " +
                " and DATE(ip.from_date) <= DATE(CURRENT_DATE()) and ( ip.to_date is null or DATE(ip.TO_DATE ) >=current_date())  ";

        Long paramMerchantId = checkMerchantId(merchantId);
        params.put("merchantId", paramMerchantId);

        sqlRun.append(sqlRoot);
        if (Utils.isNotNull(lstType)) {
            sqlRun.append(" AND ip.type IN (").append(Utils.join(",", lstType)).append(")");
        }
        if (Utils.isNotNull(displayArea)) {
            if (notInDisplay) {
                sqlRun.append(" AND ip.DISPLAY_AREA NOT IN (").append(Utils.join(",", displayArea)).append(")");
            } else {
                sqlRun.append(" AND ip.DISPLAY_AREA IN (").append(Utils.join(",", displayArea)).append(")");
            }
            params.put("displayArea", displayArea);
        }

        sqlRun.append(" GROUP BY ipmg.incentive_program_id ");
        String subQueryWhere = merchantId.equals(paramMerchantId) ? " " : " AND ip.apply_l2 = true ";

        return findAll(null, Long.class, String.format(sqlRun.toString(), subQueryWhere), params);
    }

    private void writeImage(BufferedImage image, String type, String filename) throws IOException {
        File imageFile = new File(String.format("%s/%s/%s", appConfiguration.getUploadPath(), INCENTIVE_DESCRIPTION_IMAGES_FOLDER, filename));
        FileUtils.forceMkdirParent(imageFile);
        ImageIO.write(image, type, imageFile);
    }

    private String generateImageUrl(String path, String filename) {
        return String.format("/%s/%s", path, filename);
    }

    private IncentivePackingGroup findPackingGroup(Incentive ictProgram) throws Exception {
        String sql = "from IncentivePackingGroup where status = 1 and ictProgramId = :ictProgramId";
        params = new HashMap<>();
        params.put("ictProgramId", ictProgram.getId());
        List<IncentivePackingGroup> lst = findAll(sql, null, params, 0);
        if (lst == null || lst.size() == 0)
            return null;
        if (lst.size() > 1) {
            throw new CustomErrorException("IncentivePackingGroupError");
        }
        return lst.get(0);
    }

    private IncentivePackingSale findPackingSale(Incentive ictProgram) throws Exception {
        String sql = "from IncentivePackingSale where status = 1 and incentiveProgramId = :ictProgramId";
        params = new HashMap<>();
        params.put("ictProgramId", ictProgram.getId());
        List<IncentivePackingSale> lst = findAll(sql, null, params, 0);
        if (lst == null || lst.size() == 0)
            return null;
        if (lst.size() > 1) {
            throw new CustomErrorException("IncentivePackingGroupError");
        }
        return lst.get(0);
    }

    private String getPackingFlashSaleByTime(String currentTime) {
        return " select " +
                " pp.PRODUCT_ID productId, pp.PACKING_PRODUCT_ID packingProductId ,pd.PRODUCT_NAME packingName, " +
                " ipdi.discount_type discountType, ipdi.discount_price discountPrice, " +
                " case " +
                " when ipdi.discount_type = 1 then ipdi.discount_percent " +
                " when ipdi.discount_type = 2 then (ipdi.discount_price / price.price) * 100 " +
                " else null end discountPercent, " +
                " price.price, " +
                " pp.MARKET_PRICE orgPrice, " +
                " pt.quantity, " +
                " pp.packing_url packingUrl, " +
                " ipdi.sold," +
                " ipdi.limited_quantity limitedQuantity, " +
                " fsd.zero_to_nine zeroToNine, " +
                " fsd.nine_to_twelve nineToTwelve, " +
                " fsd.twelve_to_fifteen twelveToFifteen, " +
                " fsd.fifteen_to_eighteen fifteenToEighteen, " +
                " fsd.eighteen_to_twenty_One eighteenToTwentyOne, " +
                " fsd.twenty_One_To_Twenty_Four twentyOneToTwentyFour " +
                " from " +
                " incentive_program_detail_information ipdi " +
                " join incentive_program_flash_sale_date fsd on " +
                " fsd.incentive_program_detail_information_id = ipdi.id " +
                " join incentive_program_v2 ip on " +
                " ip.id = ipdi.incentive_program_id " +
                " join incentive_program_merchant_group ipmg on " +
                " ipmg.incentive_program_id = ip.id " +
                " join merchant_group_merchant mgm on " +
                " mgm.merchant_group_id = ipmg.merchant_group_id " +
                " and mgm.merchant_id = :merchantId " +
                " join packing_product pp on " +
                " pp.PACKING_PRODUCT_ID = ipdi.packing_id " +
                " join packing_price price on " +
                " price.packing_product_id = pp.PACKING_PRODUCT_ID " +
                " join product_description pd on pd.PRODUCT_ID = pp.PRODUCT_ID and pd.LANGUAGE_ID =:langId" +
                " join product p on " +
                " p.PRODUCT_ID = pp.PRODUCT_ID and p.status  = 1 " +
                " join packing_type pt on " +
                " pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
                " where " +
                " ipdi.is_flash_sale = 1 " +
                " and ip.status = 1 " +
                " %s " +
                " and date(fsd.date) = date(current_date()) " +
                currentTime +
                " and price.price_type = 2 " +
                " and date(current_date()) >= date(price.from_date) " +
                " and (date(price.to_date)>= date(current_date()) " +
                " or price.to_date is null) " +
                " group by " +
                " ip.id," +
                " pp.PACKING_PRODUCT_ID " +
                " %s ";
    }

    private String convertMilestoneToSql(String milestone) {
        switch (milestone) {
            case "zeroToNine":
                return "  and fsd.zero_to_nine =1 ";
            case "nineToTwelve":
                return " and fsd.nine_to_twelve =1";
            case "twelveToFifteen":
                return " and fsd.twelve_to_fifteen =1";
            case "fifteenToEighteen":
                return " and fsd.fifteen_to_eighteen =1";
            case "eighteenToTwentyOne":
                return " and fsd.eighteen_to_twenty_one =1";
            case "twentyOneToTwentyFour":
                return " and fsd.twenty_one_to_twenty_four =1";
            default:
                return "";
        }
    }
}
