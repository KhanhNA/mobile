package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.resource.BannerResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BannerAlbumService {

    Page<BannerResource> findAll(Pageable pageable);

    Page<BannerResource> findByQuery(BannerResource resource, Pageable pageable);

}
