package com.nextsolutions.dcommerce.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.nextsolutions.dcommerce.repository.KpiMonthRepository;
import com.nextsolutions.dcommerce.service.KpiMonthService;
import com.nextsolutions.dcommerce.shared.dto.KpiMonthDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nextsolutions.dcommerce.model.charts.ChartBarVertical;
import com.nextsolutions.dcommerce.model.charts.ChartBarVerticalResult;
import com.nextsolutions.dcommerce.model.charts.ChartBarVerticalResultSerie;
import com.nextsolutions.dcommerce.model.charts.KpiMonthChart;
import com.nextsolutions.dcommerce.model.charts.MatCardChartsLinearGauge;
import com.nextsolutions.dcommerce.model.charts.MatListItemChartsLinearGauge;

@Service
public class KpiMonthServiceImpl implements KpiMonthService {

	@Autowired
	private KpiMonthRepository kpiMonthRepository;

	@Override
	public List<KpiMonthDto> findAll(KpiMonthDto dto) throws Exception {
		return this.kpiMonthRepository.findAll(dto);
	}

	@Override
	public ResponseEntity<Object> hhptdsBonusPercentTotalAmountInMonth() {
		List<KpiMonthChart> kpiMonths = this.kpiMonthRepository.findAllInMonthByKpiType("HHPTDS_TotalAmountInMonth");
		MatCardChartsLinearGauge matCardChartsLinearGauge = new MatCardChartsLinearGauge();
		matCardChartsLinearGauge.setTitle(kpiMonths.get(0).getKpiCode());
		List<MatListItemChartsLinearGauge> matListItemChartsLinearGauges = new ArrayList<>();
		BigDecimal max = Collections.max(kpiMonths, Comparator.comparing(kpi -> kpi.getDone())).getDone();
		for (KpiMonthChart kpiMonth : kpiMonths) {
			MatListItemChartsLinearGauge matListItemChartsLinearGauge = new MatListItemChartsLinearGauge();
			matListItemChartsLinearGauge.setLineOne(kpiMonth.getMerchantName());
			matListItemChartsLinearGauge.setLineTwo("???");
			matListItemChartsLinearGauge.setValue(kpiMonth.getDone());
			matListItemChartsLinearGauge.setPreviousValue(kpiMonth.getPlan());
			matListItemChartsLinearGauge.setMin(new BigDecimal(0));
			matListItemChartsLinearGauge.setMax(max.add(kpiMonth.getPlan()));
			matListItemChartsLinearGauge.setUnits(kpiMonth.getPlan());
			if (kpiMonth.getDone().compareTo(kpiMonth.getPlan()) >= 0) {
				matListItemChartsLinearGauge.setAvatarColor("primary");
				matListItemChartsLinearGauge.setAvatarIcon("mood");
				matListItemChartsLinearGauge.setScheme("#4DD0E1");
			} else {
				if ((kpiMonth.getDone().compareTo(new BigDecimal(0)) > 0)
						&& (matListItemChartsLinearGauges.size() <= 5)) {
					matListItemChartsLinearGauge.setAvatarColor("accent");
					matListItemChartsLinearGauge.setAvatarIcon("sentiment_satisfied");
					matListItemChartsLinearGauge.setScheme("#BA68C8");
				} else {
					matListItemChartsLinearGauge.setAvatarColor("warn");
					matListItemChartsLinearGauge.setAvatarIcon("sentiment_very_dissatisfied");
					matListItemChartsLinearGauge.setScheme("#FF7043");
				}
			}
			matListItemChartsLinearGauges.add(matListItemChartsLinearGauge);
		}
		matCardChartsLinearGauge.setItems(matListItemChartsLinearGauges);
		return new ResponseEntity<>(matCardChartsLinearGauge, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> hhptkbL2Active() {
		List<KpiMonthChart> kpiMonthsInMonth = this.kpiMonthRepository.findAllInMonthByKpiType("HHPTKB_L2_Active");
		List<KpiMonthChart> kpiMonthsInYear = this.kpiMonthRepository.findAllInYearByKpiType("HHPTKB_L2_Active");
		ChartBarVertical chartBarVertical = new ChartBarVertical();
		chartBarVertical.setTitle(kpiMonthsInMonth.get(0).getKpiCode());
		List<ChartBarVerticalResult> results = new ArrayList<>();
		for (KpiMonthChart kpiMonth : kpiMonthsInMonth) {
			ChartBarVerticalResult result = new ChartBarVerticalResult();
			result.setName(kpiMonth.getMerchantName());

			List<ChartBarVerticalResultSerie> series = new ArrayList<>();
			ChartBarVerticalResultSerie serieInMonth = new ChartBarVerticalResultSerie();
			serieInMonth.setName(
					String.format("%2d/%4d", kpiMonth.getMonth().getMonthValue(), kpiMonth.getMonth().getYear()));
			serieInMonth.setValue(kpiMonth.getDone());
			series.add(serieInMonth);
			ChartBarVerticalResultSerie serieInYear = new ChartBarVerticalResultSerie();
			serieInYear.setName(String.format("%4d", kpiMonth.getMonth().getYear()));
			Optional<KpiMonthChart> temp = kpiMonthsInYear.stream()
					.filter(kpi -> kpi.getMerchantId().equals(kpiMonth.getMerchantId())).findFirst();
			serieInYear.setValue(temp.isPresent() ? temp.get().getDone() : new BigDecimal(0));
			series.add(serieInYear);

			result.setSeries(series);
			results.add(result);
		}
		chartBarVertical.setResults(results);
		return new ResponseEntity<>(chartBarVertical, HttpStatus.OK);
	}

}
