package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.service.MerchantQueryService;
import com.nextsolutions.dcommerce.ui.model.request.MerchantRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantResource;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MerchantQueryServiceImpl implements MerchantQueryService {

    private final MerchantJpaRepository merchantJpaRepository;

    @Override
    public Page<MerchantResource> findByQuery(MerchantRequestModel query, Pageable pageable) {
        return merchantJpaRepository.findByQuery(query, pageable);
    }
}
