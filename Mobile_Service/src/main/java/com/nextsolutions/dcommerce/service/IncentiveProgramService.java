package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.coupon.CouponIncentiveDTO;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDetailInformationDto;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramGroupDto;
import com.nextsolutions.dcommerce.shared.dto.incentive.IncentiveProgramDto;
import com.nextsolutions.dcommerce.ui.model.incentive.IncentiveProgramQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IncentiveProgramService {
    IncentiveProgramDto createIncentiveProgram(IncentiveProgramDto incentiveProgramDto);

    boolean existsIncentiveProgramCodeOnCreate(String code);

    boolean existsIncentiveProgramCodeOnUpdate(Long id, String code);

    boolean existsCouponCodeOnUpdate(Long id, String code) throws Exception;

    IncentiveProgramDto updateIncentiveProgram(Long id, IncentiveProgramDto incentiveProgramDto);

    IncentiveProgramDto getIncentiveProgram(Long id);

    Page<IncentiveProgramDto> getIncentivePrograms(IncentiveProgramQuery query, Pageable pageable);

    IncentiveProgramDto toggleStatus(Long id) throws Exception;

    IncentiveProgramDto updateIncentiveProgramDetailInformation(Long id, IncentiveProgramDetailInformationDto incentiveProgramDetailInformationDto);

    IncentiveProgramDto updateIncentiveProgramGroup(Long id, IncentiveProgramGroupDto incentiveProgramGroupDto);

    boolean isExistsPackingIdInOtherIncentivePrograms(Long id, Long packingId);

    CouponIncentiveDTO updateCouponCart(Long id, CouponIncentiveDTO couponIncentiveDTO) throws Exception;

    CouponIncentiveDTO getCouponCart(Long id) throws Exception;

    IncentiveProgramDto updateIncentiveProgramMerchantGroup(Long id, IncentiveProgramDto incentiveProgramDto);

    IncentiveProgramDto updateIncentiveProgramFlashSale(Long id, List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroupDto);
}
