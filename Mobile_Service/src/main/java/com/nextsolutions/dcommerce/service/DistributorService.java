package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.Distributor;
import com.nextsolutions.dcommerce.shared.dto.DistributorResource;
import com.nextsolutions.dcommerce.shared.dto.distributor.DistributorDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface DistributorService {

    void deactivate(Long id, String token);

    Page<DistributorDto> searchDistributors(DistributorDto distributorDto, Pageable pageable);

    Optional<Distributor> findByPhoneNumber(String phoneNumber);

    DistributorDto createDistributor(DistributorDto distributorDto);

    DistributorDto getDistributor(Long id);

    DistributorDto updateDistributor(Long id, DistributorDto distributorDto);

    boolean existsByUsername(String username);

    boolean existsByUsername(Long id, String username);

    boolean existsByCode(String code);

    boolean existsByCode(Long id, String code);

    boolean existsByPhoneNumber(String phoneNumber);

    boolean existsByPhoneNumber(Long id, String phoneNumber);

    boolean existsByEmail(String email);

    boolean existsByEmail(Long id, String email);

    boolean existsByTaxCode(String taxCode);

    boolean existsByTaxCode(Long id, String taxCode);
}
