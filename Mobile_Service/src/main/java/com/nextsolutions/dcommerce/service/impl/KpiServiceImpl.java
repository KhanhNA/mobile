package com.nextsolutions.dcommerce.service.impl;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.nextsolutions.dcommerce.model.Kpi;
import com.nextsolutions.dcommerce.model.KpiMonth;
import com.nextsolutions.dcommerce.repository.KpiMonthRepository;
import com.nextsolutions.dcommerce.service.BaseAuthenticationServiceImpl;
import com.nextsolutions.dcommerce.service.KpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nextsolutions.dcommerce.repository.KpiRepository;
import com.nextsolutions.dcommerce.shared.dto.KpiDto;
import com.nextsolutions.dcommerce.shared.mapper.KpiMapper;

@Service
@Transactional
public class KpiServiceImpl extends CommonServiceImpl implements KpiService {

	@Autowired
	private KpiRepository kpiRepository;

	@Autowired
	private KpiMonthRepository kpiMonthRepository;

	@Autowired
	private KpiMapper kpiMapper;

	@Autowired
    BaseAuthenticationServiceImpl baseAuthenticationService;

	@Override
	public ResponseEntity<?> getAll() {

		return new ResponseEntity<>(this.kpiMapper.kpisToKpiDtos(this.kpiRepository.findAll()), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> getOne(Long id) {
		Optional<Kpi> kpi = this.kpiRepository.findById(id);
		return new ResponseEntity(this.kpiMapper.kpi2KpiDto(kpi.get()), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<?> create(KpiDto kpiDto) {
		try {
			String username = this.baseAuthenticationService.getCurrentOAuth2Details().getPrincipal().getUsername();

			Kpi kpi = this.kpiMapper.kpiDtoToKpi(kpiDto);
			kpi.setCreateDate(LocalDateTime.now());
			kpi.setCreateUser(username);
			this.kpiRepository.save(kpi);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<?> update(Long id, KpiDto kpiDto) {
		try {
			String username = this.baseAuthenticationService.getCurrentOAuth2Details().getPrincipal().getUsername();

			Optional<Kpi> kpiUpdate = this.kpiRepository.findById(id);
			Kpi kpi = this.kpiMapper.kpiDtoToKpi(kpiDto);
			kpi.setId(id);
			kpi.setCreateUser(kpiUpdate.get().getCreateUser());
			kpi.setCreateDate(kpiUpdate.get().getCreateDate());
			kpi.setUpdateDate(LocalDateTime.now());
			kpi.setUpdateUser(username);

			this.kpiRepository.save(kpi);
			return new ResponseEntity(this.kpiMapper.kpi2KpiDto(kpi), HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity("Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<?> delete(Long id) {
		try {
			List<KpiMonth> kpiMonths = this.kpiMonthRepository.findAllByKpiId(Collections.singletonList(id));
			if (!kpiMonths.isEmpty()) {
				return new ResponseEntity<>("attached", HttpStatus.INTERNAL_SERVER_ERROR);
			}
			Optional<Kpi> delKpi = this.kpiRepository.findById(id);
			if (delKpi.get() == null) {
				return new ResponseEntity<>("notExist", HttpStatus.INTERNAL_SERVER_ERROR);
			}
			this.kpiRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<?> getListToDate(String type) {
		return new ResponseEntity<>(this.kpiRepository.findByType(type),HttpStatus.OK);
	}
}
