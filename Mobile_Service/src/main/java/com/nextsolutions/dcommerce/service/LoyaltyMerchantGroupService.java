package com.nextsolutions.dcommerce.service;


import com.nextsolutions.dcommerce.model.loyalty.LoyaltyMerchantGroup;

import java.util.List;

public interface LoyaltyMerchantGroupService extends CommonService {
    void save(List<LoyaltyMerchantGroup> loyaltyMerchantGroups, Long loyaltyProgramId) throws Exception;
    void delete(Long id) throws Exception;

}
