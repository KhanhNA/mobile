package com.nextsolutions.dcommerce.service.chat;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.shared.dto.chat.ChatAccountDto;
import com.nextsolutions.dcommerce.shared.dto.chat.ChatLoginResponseDataDto;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.shared.dto.wallet.ResponseCreateClientDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

public interface ChatService extends CommonService {
    ResponseCreateClientDto registerAccount(MerchantCrudResource merchant) throws Exception;

    ResponseEntity<String> registerAccount(Merchant merchant) throws Exception;

    void createAccount(ChatAccountDto chatAccountDto);

    String createDiscussion(Long merchantId, String name, Boolean isSave) throws Exception;

    void sendMessageToRoomID(String roomId, String message) throws Exception;

    String getRoomID(String userName, String roomId);

    ChatLoginResponseDataDto getUserInfo();

    HttpHeaders getHeaderLogin(ChatLoginResponseDataDto loginInfo);

    boolean existsChatAccount(String username);
}
