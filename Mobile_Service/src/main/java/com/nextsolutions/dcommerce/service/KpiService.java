package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.KpiDto;
import org.springframework.http.ResponseEntity;

public interface KpiService<T> extends CommonService {

    ResponseEntity<?> getAll();

    ResponseEntity getOne(Long id);

    ResponseEntity create(KpiDto kpiDto);

    ResponseEntity update(Long id, KpiDto kpiDto);

    ResponseEntity<?> delete(Long id);

    ResponseEntity<?> getListToDate(String type);
}
