package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.wallet.TransferRequestBuilder;
import com.nextsolutions.dcommerce.shared.dto.wallet.WalletAccountRequestBuilder;
import org.springframework.http.HttpHeaders;

import java.util.Map;

public interface PaymentGatewayService {

    Long createWalletAccount(WalletAccountRequestBuilder accountRequest);

    Map<String, Object> updateWalletAccount(Long id, WalletAccountRequestBuilder accountRequest);

    boolean transfer(TransferRequestBuilder transferRequest, HttpHeaders headers);

    boolean existsWalletAccount(Long walletId);
}
