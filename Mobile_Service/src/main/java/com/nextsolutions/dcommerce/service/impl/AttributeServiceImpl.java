package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.repository.AttributeRepository;
import com.nextsolutions.dcommerce.repository.AttributeValueRepository;
import com.nextsolutions.dcommerce.repository.AutoIncrementJpaRepository;
import com.nextsolutions.dcommerce.service.AttributeService;
import com.nextsolutions.dcommerce.shared.constant.AutoIncrementType;
import com.nextsolutions.dcommerce.shared.dto.attribute.AttributeChangeDTO;
import com.nextsolutions.dcommerce.shared.dto.attribute.AttributeValueChangeDTO;
import com.nextsolutions.dcommerce.shared.dto.form.AttributeForm;
import com.nextsolutions.dcommerce.shared.dto.form.AttributeValueForm;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeDescriptionResource;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeResource;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeValueDescriptionResource;
import com.nextsolutions.dcommerce.shared.dto.AttributeDto;
import com.nextsolutions.dcommerce.shared.dto.AttributeValueDto;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.mapper.AttributeMapper;
import com.nextsolutions.dcommerce.shared.mapper.AttributeValueMapper;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import lombok.RequiredArgsConstructor;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AttributeServiceImpl implements AttributeService {

    private final AttributeRepository attributeRepository;
    private final AttributeValueRepository attributeValueRepository;
    private final AttributeMapper attributeMapper;
    private final AttributeValueMapper attributeValueMapper;
    private final AutoIncrementJpaRepository autoIncrementJpaRepository;
    private final CommonServiceImpl commonService;

    @Override
    public List<AttributeResource> findAllByLanguageId(Integer languageId) {
        return attributeRepository.findAllByLanguageId(languageId);
    }

    @Override
    public List<AttributeResource> findAllMerchantGroupAttributes(Integer languageId) {
        return attributeRepository.findAllMerchantGroupAttributesAvailable(languageId);
    }

    @Override
    public List<AttributeResource> findAllProductsAttributes(Integer languageId) {
        return attributeRepository.findAllProductAttributesAvailable(languageId);
    }

    @Override
    public AttributeForm findById(Long id) {
        return attributeRepository.findByIdAndActivated(id)
                .map(attributeMapper::toAttributeForm).orElse(null);
    }

    @Override
    public AttributeValueForm findByIdAndValueId(Long id, Long valueId) {
        return attributeValueRepository.findByIdAndActivated(id, valueId)
                .map(attributeValueMapper::toAttributeValueForm).orElse(null);
    }

    @Override
    public Page<AttributeDto> getAttributeTable(AttributeDto attributeDto, Pageable pageable) throws Exception {
        StringBuilder query = new StringBuilder("Select a.id, a.code, ad.name, a.type, a.request, a.status_process statusProcess, a.value ");
        query.append("FROM Attribute a JOIN attribute_description ad WHERE a.id = ad.attribute_id and a.status != 0 AND ad.language_id = :languageId");
        HashMap<String, Object> params = new HashMap<>();
        params.put("languageId", attributeDto.getLanguageId());
        List<AttributeDto> listResult = commonService.findAll(pageable, AttributeDto.class, query.toString() + SpringUtils.getOrderBy(pageable), params);
        String countQuery = String.format("Select count(*) from (%s) x", query);
        Integer count = commonService.getRowCountV2(countQuery, params);
        return new PageImpl<>(listResult, pageable, count);
    }

    @Override
    public List<AttributeValueDto> getAttributeValueTable(Long id, Integer languageId) {
        return attributeValueRepository.getAttributeValueTable(id, languageId);
    }

    @Override
    @Transactional
    public AttributeForm requestCreateAttribute(AttributeForm form) throws IOException {
        Attribute attribute = new Attribute();
        AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.ATTRIBUTE.value());
        Long code = Long.parseLong(autoIncrement.getValue()) + 1;

        attribute.setType(form.getType());
        attribute.setCode(code.toString());
        attribute.setStatus(1);
        attribute.setRequest(1);
        attribute.setStatusProcess(0);

        attributeRepository.save(attribute);
        autoIncrement.setValue(code.toString());
        autoIncrementJpaRepository.save(autoIncrement);
        List<AttributeDescription> attributeDescriptions = Optional
                .ofNullable(form.getDescriptions())
                .map(resources -> resources.stream().map(resource -> {
                    AttributeDescription description = new AttributeDescription();
                    description.setAttributeId(attribute.getId());
                    description.setLanguageId(resource.getLanguageId());
                    description.setName(resource.getName());
                    return description;
                }).collect(Collectors.toList()))
                .orElse(null);
        attribute.setDescriptions(attributeDescriptions);
        attributeRepository.save(attribute);
        return attributeMapper.toAttributeForm(attribute);
    }

    @Override
    @Transactional
    public AttributeForm requestUpdateAttribute(Long id, AttributeForm form) {
        Attribute attribute = attributeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));

        AttributeChangeDTO valueChange = attributeMapper.toAttributeChangeDTO(form);
        valueChange.setStatus(1);

        ObjectMapper mapper = new ObjectMapper();
        String value = "";
        try {
            value = mapper.writeValueAsString(valueChange);
        } catch (IOException e) {
            e.printStackTrace();
        }
        attribute.setValue(value);
        attribute.setRequest(2);
        attribute.setStatusProcess(0);
        attributeRepository.save(attribute);
        return attributeMapper.toAttributeForm(attribute);
    }

    @Override
    public void inactiveAttribute(Long id) {
        attributeRepository.inactive(id);
        attributeValueRepository.inactive(id);
    }

    @Override
    public AttributeForm requestInactiveAttribute(Long id) {
        Attribute attribute = attributeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        attribute.setStatusProcess(0);
        attribute.setRequest(3);
        attributeRepository.save(attribute);
        return attributeMapper.toAttributeForm(attribute);
    }

    @Override
    @Transactional
    public AttributeValueForm requestCreateAttributeValue(AttributeValueForm form) {
        AttributeValue attributeValue = new AttributeValue();
        AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.ATTRIBUTE_VALUE.value());
        Long code = Long.parseLong(autoIncrement.getValue()) + 1;
        attributeValue.setCode(code.toString());
        attributeValue.setAttributeId(form.getAttributeId());
        attributeValue.setStatus(1);
        attributeValue.setRequest(1);
        attributeValue.setStatusProcess(0);
        attributeValueRepository.save(attributeValue);
        autoIncrement.setValue(code.toString());
        autoIncrementJpaRepository.save(autoIncrement);
        List<AttributeValueDescription> attributeDescriptions = Optional
                .ofNullable(form.getDescriptions())
                .map(resources -> resources.stream().map(resource -> {
                    AttributeValueDescription description = new AttributeValueDescription();
                    description.setAttributeValueId(attributeValue.getId());
                    description.setLanguageId(resource.getLanguageId());
                    description.setName(resource.getName());
                    return description;
                }).collect(Collectors.toList()))
                .orElse(null);
        attributeValue.setDescriptions(attributeDescriptions);

        attributeValueRepository.save(attributeValue);
        return attributeValueMapper.toAttributeValueForm(attributeValue);
    }

    @Override
    public AttributeValueForm requestUpdateAttributeValue(Long id, Long valueId, AttributeValueForm form) {
        AttributeValue attributeValue = attributeValueRepository.findByIdAndAttributeId(valueId, id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find attribute value with id = %s and attribute id = %s", valueId, id)));
        AttributeValueChangeDTO value = attributeValueMapper.toAttributeValueForm(form);
        value.setStatus(1);
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(value);
        } catch (IOException e) {
            e.printStackTrace();
        }
        attributeValue.setValue(json);
        attributeValue.setRequest(2);
        attributeValue.setStatusProcess(0);

        attributeValueRepository.save(attributeValue);
        return attributeValueMapper.toAttributeValueForm(attributeValue);
    }

    @Override
    public AttributeValueForm requestInactiveAttributeValue(Long id, Long valueId) {
        AttributeValue attributeValue = attributeValueRepository.findByIdAndAttributeId(valueId, id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find attribute value with id = %s and attribute id = %s", valueId, id)));
        attributeValue.setStatusProcess(0);
        attributeValue.setRequest(3);
        attributeValueRepository.save(attributeValue);
        return attributeValueMapper.toAttributeValueForm(attributeValue);
    }

    @Override
    public AttributeValueForm managerApprovedValues(Long id, Long valueId) {
        AttributeValue attributeValue = attributeValueRepository.findByIdAndAttributeId(valueId, id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find attribute value with id = %s and attribute id = %s", valueId, id)));
        attributeValue.setRequest(0);
        attributeValue.setStatusProcess(1);
        attributeValueRepository.save(attributeValue);
        return attributeValueMapper.toAttributeValueForm(attributeValue);
    }

    @Override
    public AttributeValueForm managerRejectValues(Long id, Long valueId) {
        AttributeValue attributeValue = attributeValueRepository.findByIdAndAttributeId(valueId, id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find attribute value with id = %s and attribute id = %s", valueId, id)));
        attributeValue.setRequest(0);
        attributeValue.setStatusProcess(-1);
        attributeValueRepository.save(attributeValue);
        return attributeValueMapper.toAttributeValueForm(attributeValue);
    }

    @Override
    public void inactiveAttributeValue(Long id, Long valueId) {
        attributeValueRepository.inactive(id, valueId);
    }

    @Override
    public AttributeValueForm managerUpdateAttributeValue(Long id, Long valueId, AttributeValueForm form) {
        AttributeValue attributeValue = attributeValueRepository.findByIdAndAttributeId(valueId, id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Cannot find attribute value with id = %s and attribute id = %s", valueId, id)));
        attributeValue.setAttributeId(form.getAttributeId());
        attributeValue.setStatus(1);
        attributeValue.setRequest(0);
        attributeValue.setStatusProcess(1);

        List<AttributeValueDescription> attributeValueDescriptions = new LinkedList<>();
        Map<Long, AttributeValueDescriptionResource> resourceMap = new HashMap<>();

        form.getDescriptions().forEach(resource -> {
            if (resource.getId() == null) {
                AttributeValueDescription description = new AttributeValueDescription();
                description.setAttributeValueId(valueId);
                description.setName(resource.getName());
                description.setLanguageId(resource.getLanguageId());
                attributeValueDescriptions.add(description);
            } else {
                resourceMap.put(resource.getId(), resource);
            }
        });

        List<AttributeValueDescription> descriptions = attributeValue.getDescriptions();

        descriptions.forEach(attributeDescription -> {
            if (resourceMap.containsKey(attributeDescription.getId())) {
                AttributeValueDescriptionResource resource = resourceMap.get(attributeDescription.getId());
                attributeDescription.setAttributeValueId(attributeValue.getId());
                attributeDescription.setLanguageId(resource.getLanguageId());
                attributeDescription.setName(resource.getName());
            }
        });

        if (!attributeValueDescriptions.isEmpty()) {
            descriptions.addAll(attributeValueDescriptions);
        }

        attributeValueRepository.save(attributeValue);
        return attributeValueMapper.toAttributeValueForm(attributeValue);
    }

    @Override
    public AttributeForm managerApproved(Long id) {
        Attribute attribute = attributeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        attribute.setRequest(0);
        attribute.setStatusProcess(1);
        attributeRepository.save(attribute);
        return attributeMapper.toAttributeForm(attribute);
    }

    @Override
    public AttributeForm managerReject(Long id) {
        Attribute attribute = attributeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        attribute.setRequest(0);
        attribute.setStatusProcess(-1);
        attributeRepository.save(attribute);
        return attributeMapper.toAttributeForm(attribute);
    }

    @Override
    @Transactional
    public AttributeForm managerUpdate(Long id, AttributeForm form) {
        Attribute attribute = attributeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id " + id + " not found"));
        attribute.setType(form.getType());
        attribute.setStatus(1);
        attribute.setRequest(0);
        attribute.setStatusProcess(1);

        List<AttributeDescription> attributeDescriptions = new LinkedList<>();
        Map<Long, AttributeDescriptionResource> resourceMap = new HashMap<>();

        form.getDescriptions().forEach(resource -> {
            if (resource.getId() == null) {
                AttributeDescription description = new AttributeDescription();
                description.setAttributeId(id);
                description.setName(resource.getName());
                description.setLanguageId(resource.getLanguageId());
                attributeDescriptions.add(description);
            } else {
                resourceMap.put(resource.getId(), resource);
            }
        });


        List<AttributeDescription> descriptions = attribute.getDescriptions();
        descriptions.forEach(attributeDescription -> {
            if (resourceMap.containsKey(attributeDescription.getId())) {
                AttributeDescriptionResource resource = resourceMap.get(attributeDescription.getId());
                attributeDescription.setAttributeId(attribute.getId());
                attributeDescription.setLanguageId(resource.getLanguageId());
                attributeDescription.setName(resource.getName());
            }
        });

        if (!attributeDescriptions.isEmpty()) {
            descriptions.addAll(attributeDescriptions);
        }

        attributeRepository.save(attribute);
        return attributeMapper.toAttributeForm(attribute);
    }

}
