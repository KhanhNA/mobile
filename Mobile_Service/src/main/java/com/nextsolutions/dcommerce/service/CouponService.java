package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.Coupon;

import java.util.List;

public interface CouponService {

    Coupon saveCoupon(Coupon coupon) throws Exception;
    void addMerchant(List<Long> merchantIds, Long couponId)  throws Exception;
    void delMerchant(Long id) throws Exception;
    void delCoupon(Long id) throws Exception;
}
