package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.service.MerchantBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class MerchantBaseServiceImpl implements MerchantBaseService {

    @PersistenceContext
    protected EntityManager entityManager;
    @Autowired
    private MerchantJpaRepository repository;

    @Override
    public Page<Merchant> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Merchant get(Long aLong) {
        return repository.findById(aLong).orElse(null);
    }

    @Override
    public void save(Merchant object) {
        repository.save(object);
    }

    @Override
    public void delete(Long aLong) {
        repository.deleteById(aLong);
    }


    @Override
    public Merchant findByActiveCode(String code) {
        return repository.findByActiveCode(code);
    }

    @Override
    public Merchant findByMobilePhone(String sdt) {
        return repository.findByMobilePhone(sdt)
                .orElse(null);
    }

    @Override
    public Merchant findByUserName(String userName) {
        return repository.findByUserName(userName);
    }

}
