package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.GrouponRegister;
import com.nextsolutions.dcommerce.model.GrouponRegisterDetail;
import com.nextsolutions.dcommerce.shared.dto.GrouponRegistryDTO;
import com.nextsolutions.dcommerce.shared.dto.GrouponRegistryStatusDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GrouponService {

    GrouponRegister register(GrouponRegisterDetail grouponRegisterDetail) throws Exception;

    GrouponRegistryStatusDTO getGrouponStatus(Long registerId) throws Exception;

    Page<GrouponRegistryDTO> getListRegistry(Pageable pageable,Long registerId) throws Exception;

    List<GrouponRegister> getListGrouponProgram(Long parentMerchantId) throws  Exception;

}
