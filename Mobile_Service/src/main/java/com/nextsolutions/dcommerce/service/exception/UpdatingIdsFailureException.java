package com.nextsolutions.dcommerce.service.exception;

import java.util.List;

public class UpdatingIdsFailureException extends RuntimeException {

    private String entityName;
    private final List<Long> ids;

    public UpdatingIdsFailureException(String entityName, List<Long> ids) {
        super(entityName);
        this.entityName = entityName;
        this.ids = ids;
    }

    public List<Long> getIds() {
        return ids;
    }

    public String getEntityName() {
        return entityName;
    }
}
