package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingGroup;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingGroupPacking;
import com.nextsolutions.dcommerce.service.LoyaltyPackingGroupService;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

@Service
public class LoyaltyPackingGroupServiceImpl extends CommonServiceImpl implements LoyaltyPackingGroupService {
    @Override
    public void insertPackingGroup(LoyaltyPackingGroup dto) throws Exception {
        LoyaltyPackingGroup ipg, tmp;
        if(dto == null){
            throw new CustomErrorException("Wrongformat");
        }
        if (dto.getId() != null) {
            ipg = find(LoyaltyPackingGroup.class, dto.getId());
            if (ipg == null) {
                throw new CustomErrorException("LoyaltyPackingGroupNotExists");

            }
        }else {
            ipg = new LoyaltyPackingGroup();
        }
        ipg = Utils.getData(dto, ipg);
        ipg.setStatus(1);
        save(ipg);
    }

    @Override
    public void savePackingGroupDetail(List<LoyaltyPackingGroupPacking> dtos, Long pgId) throws Exception {
        if (pgId == null) {
            throw new CustomErrorException("PackingGroupNull");
        }
        LoyaltyPackingGroup loyaltyPackingGroup = find(LoyaltyPackingGroup.class, pgId);
        if (loyaltyPackingGroup == null) {
            throw new CustomErrorException("PackingGroupNotExists");
        }
        dtos = Utils.safe(dtos);
        LoyaltyPackingGroupPacking entity;
        Long id;
        for (LoyaltyPackingGroupPacking loyaltyPackingGroupPacking : dtos) {
            entity = loyaltyPackingGroupPacking;

            id = loyaltyPackingGroupPacking.getId();
            if (id != null) {
                entity = find(LoyaltyPackingGroupPacking.class, id);
                if (entity == null) {
                    throw new CustomErrorException("PackingGroupDetailNotExists");
                }
                entity = Utils.getData(loyaltyPackingGroupPacking, entity);
            }
            entity.setLoyaltyPackingGroupId(pgId);
            save(entity);
        }
    }

    @Override
    public void delPackingGrp(Long loyaltyPgId) throws Exception {
        if (loyaltyPgId == null) {
            throw new CustomErrorException("loyaltyPgIdNull");
        }
        LoyaltyPackingGroup loyaltyPg = find(LoyaltyPackingGroup.class, loyaltyPgId);
        if (loyaltyPg == null) {
            throw new CustomErrorException("LoyaltyPackingGroupNotFound");
        }
        loyaltyPg.setStatus(0);
        save(loyaltyPg);
    }

    @Override
    public void delPackingGrpPacking(Long loyaltyPgdId) throws Exception {
        if (loyaltyPgdId == null) {
            throw new CustomErrorException("loyaltyPgdIdNull");
        }
        LoyaltyPackingGroupPacking loyaltyPgp = find(LoyaltyPackingGroupPacking.class, loyaltyPgdId);
        if (loyaltyPgp == null) {
            throw new CustomErrorException("LoyaltyPackingGroupPackingNotFound");
        }
        remove(loyaltyPgp);
    }

    @Override
    public Page<PackingProduct> list(Pageable pageable, Long loyaltyPackingGroupId) throws Exception {
        String sql = "select a.* " +
                " from packing_product a left join loyalty_packing_group_packing b on a.PACKING_PRODUCT_ID = b.PACKING_PRODUCT_ID " +
                "                                   and b.loyalty_packing_group_id = :loyaltyPackingGroupId " +
                " where b.PACKING_PRODUCT_ID is null";
        HashMap<String, Object> params = new HashMap<>();
        Query query = entityManager.createNativeQuery(sql, PackingProduct.class);
        query.setParameter("loyaltyPackingGroupId", loyaltyPackingGroupId);
        String sqlCount = String.format("select count(*) from (%s) a ", sql);

        Query queryCount = entityManager.createNativeQuery(sqlCount);
        queryCount.setParameter("loyaltyPackingGroupId", loyaltyPackingGroupId);
        BigInteger count = (BigInteger)queryCount.getSingleResult();
        params.put("loyaltyPackingGroupId", loyaltyPackingGroupId);
        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<PackingProduct> merchants = query.getResultList();
        return new PageImpl<>(merchants, pageable, count.intValue());
    }
}
