package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.OverviewStatistics;

public interface DashboardService {

    OverviewStatistics getOverviewStatistics();

    String getSqlTotalRegister(String fromDate, String toDate);

    String getSqlTotalActive(String fromDate, String toDate);

    String getSqlTotalInactive(String fromDate, String toDate);

    String getSqlInactiveRevenue(String fromDate, String toDate);

    String getSqlTotalRevenue(String fromDate, String toDate);

    String getSqlTotalOrder(String fromDate, String toDate);

    String getSqlTotalPending(String fromDate, String toDate);

    String getSqlTotalOrderOfMerchant(String fromDate, String toDate);

    String getSqlTotalRevenueOfMerchant(String fromDate, String toDate);

}
