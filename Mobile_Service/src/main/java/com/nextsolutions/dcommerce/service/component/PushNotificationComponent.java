package com.nextsolutions.dcommerce.service.component;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.NotificationMerchant;
import com.nextsolutions.dcommerce.model.firebase.NotificationAction;
import com.nextsolutions.dcommerce.model.firebase.PushNotificationRequest;
import com.nextsolutions.dcommerce.model.firebase.TokenFbMerchant;
import com.nextsolutions.dcommerce.model.order.Order;
import com.nextsolutions.dcommerce.service.*;
import com.nextsolutions.dcommerce.service.event.PushNotificationConfirmOrder;
import com.nextsolutions.dcommerce.service.event.PushNotificationOrderEvent;
import com.nextsolutions.dcommerce.service.event.PushNotificationOrderSuccessEvent;
import com.nextsolutions.dcommerce.service.event.PushNotificationRegisterEvent;
import com.nextsolutions.dcommerce.service.firebase.PushNotificationService;
import com.nextsolutions.dcommerce.service.impl.HunterServiceImpl;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PushNotificationComponent {
    private final CommonService commonService;
    private final PushNotificationService pushNotificationService;
    private final NotificationMerchantService notificationService;
    private final MessageSource messageSource;
    private final OrderV2Service orderService;
    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);

    @Async
    @EventListener
    @Transactional
    public void pushNotificationOrderEventListener(PushNotificationOrderEvent pushNotificationEvent) {
        try {
            if (pushNotificationEvent.getOrder().getParentMerchantId() != null) {
                String sql = "select * from token_fb_merchant where merchant_id =:merchantId";
                List<TokenFbMerchant> arrToken = commonService.findAll(null, TokenFbMerchant.class, sql, pushNotificationEvent.getOrder().getParentMerchantId());
                if (arrToken != null && arrToken.size() > 0) {
                    NotificationMerchant notificationMerchant = new NotificationMerchant();
                    notificationMerchant.setTitle(messageSource.getMessage("ord.ordered", new Object[]{pushNotificationEvent.getOrder().getMerchantName()}, LocaleContextHolder.getLocale()));
                    notificationMerchant.setMessage(messageSource.getMessage("ord.confirm", null, LocaleContextHolder.getLocale()));
                    notificationMerchant.setTypeId(NotificationAction.L2_ORDER.getValue());
                    notificationMerchant.setActionId(pushNotificationEvent.getOrder().getOrderId());
                    notificationMerchant.setClick_action("com.ts.dcommerce.TARGET_ORDER_DETAIL");
                    notificationMerchant.setOrderPackingName(pushNotificationEvent.getOrder().calculatePackings.get(0).getProductName());
                    notificationMerchant.setOrderQuantity(pushNotificationEvent.getOrder().calculatePackings.get(0).getOrderQuantity());
                    notificationMerchant.setOrderPackingUrl(pushNotificationEvent.getOrder().calculatePackings.get(0).getPackingUrl());
                    notificationMerchant.setMerchantName(pushNotificationEvent.getFullName());
                    notificationMerchant.setMerchantNumberPhone(pushNotificationEvent.getMobilePhone());
                    notificationMerchant.setOrderCode(pushNotificationEvent.getOrder().orderNo);
                    notificationMerchant.setIsSeen(0);
                    for (TokenFbMerchant tokenFbMerchant : arrToken) {
                        notificationMerchant.setToken(tokenFbMerchant.getToken());
                        notificationMerchant.setMerchantReceiverId(tokenFbMerchant.getMerchantId());
                        // Push notification to device
                        pushNotificationService.sendPushNotificationToToken(Utils.getData(notificationMerchant, PushNotificationRequest.class));
                    }
                    // Save notification
                    notificationService.save(notificationMerchant);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Async
    @EventListener
    @Transactional
    public void pushNotificationCreateL2(PushNotificationRegisterEvent registerEvent) throws Exception {
        String sql = "select * from token_fb_merchant where merchant_id =:merchantId";
        List<TokenFbMerchant> arrToken = commonService.findAll(null, TokenFbMerchant.class, sql, registerEvent.getParentMerchantId());
        Merchant merchantRegister = registerEvent.getL2Merchant();
        try {
            NotificationMerchant notificationMerchant = null;
            if (arrToken != null && arrToken.size() > 0) {
                notificationMerchant = new NotificationMerchant();
                notificationMerchant.setTitle(Translator.toLocale("register.success", new Object[]{merchantRegister.getFullName()}));
                notificationMerchant.setMessage(Translator.toLocale("phone.number", new Object[]{merchantRegister.getMobilePhone()}));
                notificationMerchant.setMerchantName(merchantRegister.getFullName());
                notificationMerchant.setMerchantNumberPhone(merchantRegister.getMobilePhone());
                notificationMerchant.setTypeId(NotificationAction.INVITE.getValue());
                notificationMerchant.setActionId(merchantRegister.getMerchantId());
                notificationMerchant.setClick_action("com.ts.dcommerce.TARGET_MERCHANT_DETAIL");
                notificationMerchant.setIsSeen(0);
                notificationMerchant.setMerchantReceiverId(registerEvent.getParentMerchantId());
                for (TokenFbMerchant tokenFbMerchant : arrToken) {
                    // Send notification to LV1
                    notificationMerchant.setToken(tokenFbMerchant.getToken());
                    pushNotificationService.sendPushNotificationToToken(Utils.getData(notificationMerchant, PushNotificationRequest.class));
                }
            }
            if (notificationMerchant != null) {
                notificationService.save(notificationMerchant);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Async
    @EventListener
    @Transactional
    public void pushNotificationOrderSuccess(PushNotificationOrderSuccessEvent orderSuccessEvent) throws Exception {
        Order order = orderSuccessEvent.getOrder();
        OrderV2Dto orderNotify = orderService.getOrder(order.getOrderId(), LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage()));
        try {
            if (order.getMerchantId() != null) {
                String sql_fb = "select * from token_fb_merchant where merchant_id =:merchantId";
                List<TokenFbMerchant> arrToken = orderService.findAll(null, TokenFbMerchant.class, sql_fb, order.getMerchantId());
                if (arrToken != null && arrToken.size() > 0) {
                    NotificationMerchant notificationMerchant = new NotificationMerchant();
                    notificationMerchant.setTitle(order.getOrderStatus() == 2 ? messageSource.getMessage("ord.success", null, LocaleContextHolder.getLocale()) : messageSource.getMessage("ord.reject", null, LocaleContextHolder.getLocale()));
                    notificationMerchant.setMessage(messageSource.getMessage("ord.detail", null, LocaleContextHolder.getLocale()));
                    notificationMerchant.setTypeId(order.getOrderStatus() == 2 ? NotificationAction.ORDER_SUCCESS.getValue() : NotificationAction.L1_ORDER_REJECT.getValue());
                    notificationMerchant.setActionId(order.getOrderId());
                    notificationMerchant.setOrderCode(order.getOrderNo());
                    notificationMerchant.setClick_action("com.ts.dcommerce.TARGET_ORDER_DETAIL");
                    notificationMerchant.setIsSeen(0);
                    if (orderNotify != null) {
                        notificationMerchant.setOrderPackingName(orderNotify.productName);
                        notificationMerchant.setOrderPackingUrl(orderNotify.packingUrl);
                        notificationMerchant.setOrderQuantity(orderNotify.orderQuantity);
                    }
                    for (TokenFbMerchant tokenFbMerchant : arrToken) {
                        notificationMerchant.setToken(tokenFbMerchant.getToken());
                        notificationMerchant.setMerchantReceiverId(tokenFbMerchant.getMerchantId());
                        // Push notification to device
                        pushNotificationService.sendPushNotificationToToken(Utils.getData(notificationMerchant, PushNotificationRequest.class));
                    }
                    // Save notification
                    notificationService.save(notificationMerchant);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Async
    @EventListener
    @Transactional
    public void pushNotificationConfirmOrder(PushNotificationConfirmOrder event) throws Exception {
        try {
            OrderV2Dto orderRequest = event.getOrderDto();
            OrderV2Dto orderDetail = orderService.getOrder(event.getOrderDto().getOrderId(), LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage()));
            if (orderDetail != null && orderDetail.getOrderId() != null) {
                if (orderDetail.getMerchantId() != null) {
                    String sql = "select * from token_fb_merchant where merchant_id =:merchantId";
                    List<TokenFbMerchant> arrToken = commonService.findAll(null, TokenFbMerchant.class, sql, orderDetail.getMerchantId());
                    if (arrToken != null && arrToken.size() > 0) {
                        NotificationMerchant notificationMerchant = new NotificationMerchant();
                        notificationMerchant.setTitle(orderRequest.getOrderStatus() == 1 ? messageSource.getMessage("ord.approved", null, LocaleContextHolder.getLocale()) : messageSource.getMessage("ord.reject", null, LocaleContextHolder.getLocale()));
                        notificationMerchant.setMessage(messageSource.getMessage("ord.detail", null, LocaleContextHolder.getLocale()));
                        notificationMerchant.setTypeId(orderRequest.getOrderStatus() == 1 ? NotificationAction.L1_ORDER_APPROVE.getValue() : NotificationAction.L1_ORDER_REJECT.getValue());
                        notificationMerchant.setActionId(orderDetail.getOrderId());
                        notificationMerchant.setOrderCode(orderDetail.getOrderNo());
                        notificationMerchant.setOrderPackingName(orderDetail.productName);
                        notificationMerchant.setOrderPackingUrl(orderDetail.packingUrl);
                        notificationMerchant.setOrderQuantity(orderDetail.orderQuantity);
                        notificationMerchant.setClick_action("com.ts.dcommerce.TARGET_ORDER_DETAIL");
                        notificationMerchant.setIsSeen(0);
                        for (TokenFbMerchant tokenFbMerchant : arrToken) {
                            notificationMerchant.setToken(tokenFbMerchant.getToken());
                            notificationMerchant.setMerchantReceiverId(tokenFbMerchant.getMerchantId());
                            // Push notification to device
                            pushNotificationService.sendPushNotificationToToken(Utils.getData(notificationMerchant, PushNotificationRequest.class));
                        }
                        // Save notification
                        notificationService.save(notificationMerchant);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }


    }
}
