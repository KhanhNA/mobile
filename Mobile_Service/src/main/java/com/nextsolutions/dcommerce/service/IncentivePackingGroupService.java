package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingGroup;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingGroupPacking;
import com.nextsolutions.dcommerce.shared.dto.MerchantGroupDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IncentivePackingGroupService extends CommonService {
    void insertPackingGroup(IncentivePackingGroup dto) throws Exception;

    void savePackingGroupDetail(List<IncentivePackingGroupPacking> dtos,
                                Long pgId) throws Exception;

    void delPackingGrp(Long ictPgId) throws Exception;

    void delPackingGrpPacking(Long ictPgdId) throws Exception;

    Page<PackingProduct> list(Pageable pageable, Long ictPackingGroupId) throws Exception;

    Page<MerchantGroupDTO> listGroupApply(Pageable pageable, Long incentiveId, Long langId) throws Exception;
}
