package com.nextsolutions.dcommerce.service.activemq;


import com.nextsolutions.dcommerce.shared.dto.recommend.RecommendProduct;

public interface RecommendService {
    boolean recommend(RecommendProduct activeMQRequestBuilder);
}
