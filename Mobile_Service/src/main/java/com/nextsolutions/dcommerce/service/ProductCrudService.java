package com.nextsolutions.dcommerce.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nextsolutions.dcommerce.shared.dto.AppUnitDto;
import com.nextsolutions.dcommerce.shared.dto.CurrencyDTO;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDto;
import com.nextsolutions.dcommerce.shared.dto.form.DistributorForm;
import com.nextsolutions.dcommerce.shared.dto.product.PackingPriceDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductQueryDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductTableDto;
import com.nextsolutions.dcommerce.shared.dto.resource.*;
import com.nextsolutions.dcommerce.ui.model.request.CategoryGroupPriceRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.PackingProductRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductBasicInfoRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductImagesRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductBasicInfoResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductFormResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductImagesResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductPackingResponseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface ProductCrudService {

    ProductBasicInfoResponseModel saveProductBasicInfo(ProductBasicInfoRequestModel basicInfo);

    ProductFormResponseModel getProductForm(Long id, Long categoryId, Long languageId);

    PackingProductRequestModel savePackingProducts(Long id, PackingProductRequestModel packingProductRequestModel);

    Map<String, String> savePackingImage(MultipartFile imageFile) throws IOException;

    Page<ProductTableDto> findAll(Pageable pageable);

    void deleteProductById(Long id);

    Page<ProductTableDto> findByQuery(ProductQueryDto query, Pageable pageable);

    ProductImagesResponseModel saveProductImages(Long id, MultipartFile[] multipartFiles, ProductImagesRequestModel productRequest) throws IOException;

    boolean existsCode(String code);

    boolean isDuplicateCode(Long id, String code);

    Map<String, String> saveProductDescriptionImage(MultipartFile imageFile) throws IOException;

/*
    Map<String, Integer> applyGroupPrice(CategoryGroupPriceRequestModel request);
*/

    Page<ProductPackingResponseModel> findByCategoryIdAndPackingTypeId(Long categoryId, Long packingTypeId, Pageable pageable);

    Page<PackingPriceDto> getHistoryGroupPrice(PackingPriceDto requestModel, Pageable pageable) throws Exception;

    ProductGeneralResource getProductGeneralResource(Long id);

    ProductDataResource getProductDataResource(Long id);

    ProductAttributeResource getProductAttributeResource(Long id);

    ProductPackageResource getProductPackageResource(Long id);

    void updateDistributor(Long id, DistributorForm distributor);

    boolean isSalePriceLessThanImportPrice(Long productId, BigDecimal salePrice) throws Exception;

    Void applyGroupPrice(CategoryGroupPriceRequestModel requestModel) throws Exception;

    Boolean newPrice(PackingPriceDto request);

    List<CurrencyDTO> getCurrency() throws Exception;

    Boolean editPrice(PackingPriceDto request) throws JsonProcessingException;

    Boolean approvedNew(PackingPriceDto request);

    PackingPriceDto approvedEdit(PackingPriceDto request);

    PackingPriceDto rejectRequest(PackingPriceDto request);

    List<AppUnitDto> getUnits() throws Exception;

    Page<ManufacturerResource> getManufacturer(ManufacturerResource resource, Pageable pageable) throws Exception;


}
