package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.service.WareHouseService;
import com.nextsolutions.dcommerce.shared.dto.MerchantGroupDTO;
import com.nextsolutions.dcommerce.shared.dto.MerchantWareHouseDTO;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class WarehouseServiceImpl extends CommonServiceImpl implements WareHouseService {
    @Override
    public List<MerchantWareHouseDTO> getAll() throws Exception {
        String sql = "select distinct(store_code) warehouseCode, 'abc-Le Van Thiem' address from merchant_warehouse";
        return findAll(null, MerchantWareHouseDTO.class, sql, new HashMap());
    }

    @Override
    public List<MerchantGroupDTO> getAllByWarehouseCode(List<String> lstWarehouseCode) throws Exception {
        String sql = "select " +
                "mgm.MERCHANT_GROUP_ID id,mgd.name " +
                "from " +
                "merchant_group_merchant mgm " +
                "join merchant_group mg on " +
                "mg.ID = mgm.MERCHANT_GROUP_ID and mg.STATUS <> 0 " +
                "join merchant_group_description mgd on mgd.MERCHANT_GROUP_ID = mgm.MERCHANT_GROUP_ID " +
                "where " +
                "merchant_id in ( " +
                "select distinct (merchant_id) " +
                "from merchant_warehouse " +
                "where store_code in (" + Utils.joinString(",", lstWarehouseCode) + ")) " +
                "and mgd.language_id = :langId " +
                "group by id ";
        return findAll(null, MerchantGroupDTO.class, sql, LanguageUtils.getCurrentLanguageId());
    }
}
