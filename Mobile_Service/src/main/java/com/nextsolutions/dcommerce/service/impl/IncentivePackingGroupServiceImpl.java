package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingGroup;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingGroupPacking;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingSale;
import com.nextsolutions.dcommerce.service.IncentivePackingGroupService;
import com.nextsolutions.dcommerce.shared.dto.MerchantGroupDTO;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

@Service
public class IncentivePackingGroupServiceImpl extends CommonServiceImpl implements IncentivePackingGroupService {


    @Override
    public void insertPackingGroup(IncentivePackingGroup dto) throws Exception {
        IncentivePackingGroup ipg, tmp;
        if (dto == null) {
            throw new CustomErrorException("Wrongformat");
        }
        if (dto.getId() != null) {
            ipg = find(IncentivePackingGroup.class, dto.getId());
            if (ipg == null) {
                throw new CustomErrorException("IncentivePackingGroupNotExists");

            }
        } else {
            ipg = new IncentivePackingGroup();
        }
        ipg = Utils.getData(dto, ipg);
        ipg.setStatus(1);
        save(ipg);
    }

    @Override
    public void savePackingGroupDetail(List<IncentivePackingGroupPacking> dtos, Long pgId) throws Exception {
        if (pgId == null) {
            throw new CustomErrorException("PackingGroupNull");
        }
        IncentivePackingGroup ictPackingGroup = find(IncentivePackingGroup.class, pgId);
        if (ictPackingGroup == null) {
            throw new CustomErrorException("PackingGroupNotExists");
        }
        dtos = Utils.safe(dtos);
        IncentivePackingGroupPacking entity;
        Long id;
        for (IncentivePackingGroupPacking ictPackingGroupPacking : dtos) {
            entity = ictPackingGroupPacking;

            id = ictPackingGroupPacking.getId();
            if (id != null) {
                entity = find(IncentivePackingGroupPacking.class, id);
                if (entity == null) {
                    throw new CustomErrorException("PackingGroupDetailNotExists");
                }
                entity = Utils.getData(ictPackingGroupPacking, entity);
            }
            entity.setIncentivePackingGroupId(pgId);
            save(entity);
        }
    }

    @Override
    public void delPackingGrp(Long ictPgId) throws Exception {
        if (ictPgId == null) {
            throw new CustomErrorException("ictPgIdNull");
        }
        IncentivePackingGroup ictPg = find(IncentivePackingGroup.class, ictPgId);
        if (ictPg == null) {
            throw new CustomErrorException("IncentivePackingGroupNotFound");
        }
        ictPg.setStatus(0);
        delPackingGroupSale(ictPgId);
        save(ictPg);
    }

    private void delPackingGroupSale(Long ictPgId) {
        String sql = "UPDATE incentive_packing_sale SET status = 0 WHERE incentive_packing_group_id =:ictPgId";
        Query query = entityManager.createNativeQuery(sql, IncentivePackingSale.class);
        query.setParameter("ictPgId", ictPgId);
        query.executeUpdate();
    }

    @Override
    public void delPackingGrpPacking(Long ictPgdId) throws Exception {
        if (ictPgdId == null) {
            throw new CustomErrorException("ictPgdIdNull");
        }
        IncentivePackingGroupPacking ictPgp = find(IncentivePackingGroupPacking.class, ictPgdId);
        if (ictPgp == null) {
            throw new CustomErrorException("IncentivePackingGroupPackingNotFound");
        }
        remove(ictPgp);
    }

    @Override
    public Page<PackingProduct> list(Pageable pageable, Long ictPackingGroupId) throws Exception {
        String sql = "select a.* " +
                " from packing_product a left join incentive_packing_group_packing b on a.PACKING_PRODUCT_ID = b.PACKING_PRODUCT_ID " +
                "                                   and b.incentive_packing_group_id = :ictPackingGroupId " +
                " where b.PACKING_PRODUCT_ID is null";
        HashMap<String, Object> params = new HashMap<>();
        Query query = entityManager.createNativeQuery(sql, PackingProduct.class);
        query.setParameter("ictPackingGroupId", ictPackingGroupId);
        String sqlCount = String.format("select count(*) from (%s) a ", sql);

        Query queryCount = entityManager.createNativeQuery(sqlCount);
        queryCount.setParameter("ictPackingGroupId", ictPackingGroupId);
        BigInteger count = (BigInteger) queryCount.getSingleResult();
        params.put("ictPackingGroupId", ictPackingGroupId);
        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<PackingProduct> merchants = query.getResultList();
        return new PageImpl<>(merchants, pageable, count.intValue());
    }

    @Override
    public Page<MerchantGroupDTO> listGroupApply(Pageable pageable, Long incentiveId,Long langId) throws Exception {

        String sql = "select img.*,mgd.name,mg.code from Incentive_Merchant_Group img " +
                " JOIN merchant_group mg on img.merchant_group_id = mg.id " +
                " JOIN merchant_group_description mgd on img.merchant_group_id = mgd.merchant_group_id " +
                " WHERE " +
                "    img.incentive_program_id = :incentiveId  and mgd.language_id = :language_id";

        Query query = entityManager.createNativeQuery(sql, "MerchantGroupList");
        query.setParameter("incentiveId", incentiveId);
        query.setParameter("language_id", langId);
        //
        String sqlCount = String.format("select count(*) from (%s) a ", sql);
        Query queryCount = entityManager.createNativeQuery(sqlCount);
        queryCount.setParameter("incentiveId", incentiveId);
		queryCount.setParameter("language_id", langId);
        BigInteger count = (BigInteger) queryCount.getSingleResult();

        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<MerchantGroupDTO> packingProducts = query.getResultList();
        return new PageImpl<>(packingProducts, pageable, count.intValue());
    }
}
