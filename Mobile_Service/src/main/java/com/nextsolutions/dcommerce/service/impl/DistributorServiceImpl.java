package com.nextsolutions.dcommerce.service.impl;

import static org.springframework.data.domain.Example.of;

import java.util.Collections;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.nextsolutions.dcommerce.configuration.properties.OAuthConfiguration;
import com.nextsolutions.dcommerce.model.AutoIncrement;
import com.nextsolutions.dcommerce.model.Distributor;
import com.nextsolutions.dcommerce.model.Role;
import com.nextsolutions.dcommerce.model.UserEntity;
import com.nextsolutions.dcommerce.repository.AutoIncrementJpaRepository;
import com.nextsolutions.dcommerce.repository.DistributorRepository;
import com.nextsolutions.dcommerce.service.AccountService;
import com.nextsolutions.dcommerce.service.DistributorService;
import com.nextsolutions.dcommerce.service.PaymentGatewayService;
import com.nextsolutions.dcommerce.shared.constant.AutoIncrementType;
import com.nextsolutions.dcommerce.shared.dto.distributor.DistributorDto;
import com.nextsolutions.dcommerce.shared.dto.wallet.ClientType;
import com.nextsolutions.dcommerce.shared.dto.wallet.SavingsProductType;
import com.nextsolutions.dcommerce.shared.dto.wallet.WalletAccountRequestBuilder;
import com.nextsolutions.dcommerce.shared.mapper.DistributorMapper;
import com.nextsolutions.dcommerce.utils.StringUtils;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DistributorServiceImpl implements DistributorService {

    private final DistributorRepository distributorRepository;
    private final DistributorMapper distributorMapper;
    private final AccountService accountService;
    private final PaymentGatewayService paymentGatewayService;
    private final OAuthConfiguration oAuthConfiguration;
    private final AutoIncrementJpaRepository autoIncrementJpaRepository;

    private Long createWalletAccount(Distributor distributor) {
        return paymentGatewayService.createWalletAccount(WalletAccountRequestBuilder
                .builder()
                .firstName(distributor.getName())
                .lastName("distributor")
                .clientTypeId(ClientType.DISTRIBUTOR.value())
                .savingsProductId(SavingsProductType.DISTRIBUTOR.value())
                .email(distributor.getEmail())
                .externalId(distributor.getId())
                .username(distributor.getUsername())
                .build());
    }

    private void createUser(Distributor distributor) {
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName(distributor.getName());
        userEntity.setLastName("");
        userEntity.setUsername(distributor.getUsername());
        userEntity.setEnabled(true);
        userEntity.setRoles(Collections.singletonList(new Role(oAuthConfiguration.getDistributorRole())));
        accountService.createAccount(userEntity);
    }

    @Override
    @Transactional
    public void deactivate(Long id, String token) {
        String username = distributorRepository.findUsernameById(id);
        if (username != null) {
            accountService.deactivateAccount(username, token);
        }
        distributorRepository.inactivateById(id);
    }

    @Override
    public Page<DistributorDto> searchDistributors(DistributorDto distributorDto, Pageable pageable) {
        ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                .withMatcher("code", matcher -> matcher.ignoreCase().contains())
                .withMatcher("name", matcher -> matcher.ignoreCase().contains())
                .withMatcher("phoneNumber", matcher -> matcher.ignoreCase().contains())
                .withMatcher("email", matcher -> matcher.ignoreCase().contains())
                .withMatcher("status", ExampleMatcher.GenericPropertyMatcher::exact);
        Distributor distributor = distributorMapper.toDistributor(distributorDto);
        distributor.setEnabled(true);
        Page<Distributor> distributorPage = distributorRepository.findAll(of(distributor, exampleMatcher), pageable);
        return distributorPage.map(distributorMapper::toDistributorDto);
    }

    @Override
    public Optional<Distributor> findByPhoneNumber(String phoneNumber) {
        return distributorRepository.findByPhoneNumber(phoneNumber);
    }

    @Override
    @Transactional
    public DistributorDto createDistributor(DistributorDto distributorDto) {
        AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.NCC.value());
        Long code = Long.parseLong(autoIncrement.getValue()) + 1;
        Distributor distributor = distributorMapper.toDistributor(distributorDto);
        distributor.setCode(code.toString());
        distributor.setDistributorId(StringUtils.randomUUID());
        distributorRepository.save(distributor);

        Long walletId = createWalletAccount(distributor);

        createUser(distributor);

        distributor.setWalletId(walletId);

        distributorRepository.save(distributor);
        autoIncrement.setValue(code.toString());
        autoIncrementJpaRepository.save(autoIncrement);
        return distributorMapper.toDistributorDto(distributor);
    }

    @Override
    public DistributorDto getDistributor(Long id) {
        Distributor distributor = distributorRepository.findById(id).orElseThrow(RuntimeException::new);
        return distributorMapper.toDistributorDto(distributor);
    }

    @Override
    @Transactional
    public DistributorDto updateDistributor(Long id, DistributorDto distributorDto) {
        return distributorRepository.findById(id)
                .map(distributor -> {
                    distributor.setAddress(distributorDto.getAddress());
                    distributor.setEmail(distributorDto.getEmail());
                    distributor.setEnabled(distributorDto.getEnabled());
                    distributor.setPhoneNumber(distributorDto.getPhoneNumber());
                    distributor.setAboutUs(distributorDto.getAboutUs());
                    distributor.setName(distributorDto.getName());
                    distributor.setTaxCode(distributorDto.getTaxCode());
                    distributor.setIsSynchronization(false);

                    if (distributor.getUsername() == null) { // TODO: create user << refactor later
                        distributor.setUsername(distributorDto.getUsername());
                        createUser(distributor);
                    } else {
                        if (!distributor.getUsername().equals(distributorDto.getUsername())) {
                            accountService.deactivateAccount(distributor.getUsername());
                            distributor.setUsername(distributorDto.getUsername());
                            createUser(distributor);
                        }
                    }

                    if (distributor.getWalletId() == null) { // TODO: create wallet account << refactor later
                        Long walletId = createWalletAccount(distributor);
                        distributor.setWalletId(walletId);
                    }

                    distributorRepository.save(distributor);
                    return distributorMapper.toDistributorDto(distributor);
                })
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsByUsername(String username) {
        return accountService.existsAccount(username);
    }

    @Override
    public boolean existsByUsername(Long id, String username) {
        return distributorRepository.findById(id)
                .map(distributor -> {
                    if (distributor.getUsername() == null) {
                        return accountService.existsAccount(username);
                    } else {
                        if (!distributor.getUsername().equals(username)) {
                            return accountService.existsAccount(username);
                        } else {
                            return false;
                        }
                    }
                })
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsByCode(String code) {
        return distributorRepository.findByCode(code).isPresent();
    }

    @Override
    public boolean existsByCode(Long id, String code) {
        return distributorRepository.findByCode(code)
                .map(distributor -> !distributor.getId().equals(id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsByPhoneNumber(String phoneNumber) {
        return distributorRepository.findByPhoneNumber(phoneNumber).isPresent();
    }

    @Override
    public boolean existsByPhoneNumber(Long id, String phoneNumber) {
        return distributorRepository.findByPhoneNumber(phoneNumber)
                .map(distributor -> !distributor.getId().equals(id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsByEmail(String email) {
        return distributorRepository.findByEmail(email).isPresent();
    }

    @Override
    public boolean existsByEmail(Long id, String email) {
        return distributorRepository.findByEmail(email)
                .map(distributor -> !distributor.getId().equals(id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsByTaxCode(String taxCode) {
        return distributorRepository.findByTaxCode(taxCode).isPresent();
    }

    @Override
    public boolean existsByTaxCode(Long id, String taxCode) {
        return distributorRepository.findByTaxCode(taxCode)
                .map(distributor -> !distributor.getId().equals(id))
                .orElseThrow(EntityNotFoundException::new);
    }
}
