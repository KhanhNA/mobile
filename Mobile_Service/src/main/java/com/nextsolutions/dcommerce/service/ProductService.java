package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.ProductDescription;
import com.nextsolutions.dcommerce.shared.dto.*;
import com.nextsolutions.dcommerce.shared.dto.incentivev2.ProductIncentiveInformationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface ProductService {

    Page<ProductDto> getAllProductCus(Pageable pageable, int langId) throws Exception;

    ProductDetailDto getProductDetail(long id, Long langId) throws Exception;

    ProductDetailDTOV2 getProductDetailV2(Long merchantId, Long id, Long langId) throws Exception;

    HashMap<Long, ProductDescription> getProductDesc(Long id) throws Exception;

    ProductDto save(ProductDto entity) throws Exception;

    ProductDto del(ProductDto entity) throws Exception;

    Page<ProductInfoDTO> findAllByCategory(Pageable pageable,Long merchantId, Long categoryId, Long langId) throws Exception;

    Page<ProductDto> getAllProductCusV2(Pageable pageable, Long langId, String keyWord);

    Page<IncentiveDescriptionDTO> getIncentiveFromPacking(Pageable pageable, Long id, Long langId);

    Page<PackingProductDTO> getGroupon(Pageable pageable, Long langId);

    List<PackingProductDTO> getPriceForPacking(List<Long> lstPackingId) throws Exception;

    ProductCustomDTO getProductOfMerchant(Pageable pageable, List<String> lstPackingId, Long merchantId, Long langId, String keyWord, int filterType) throws IOException;

    ProductCustomDTO getPredictionProduct(Pageable pageable, List<String> lstPackingId, Long merchantId, Long langId, String keyWord) throws IOException;

    Page<PackingSupperSaleDTO> getProductSupperSale(Pageable pageable, Long merchantId, Long langId) throws Exception;

    Page<ProductInfoDTO> getProductAll(Pageable pageable, Long merchantId, Long langId) throws Exception;

    Page<ProductInfoDTO> getProductAllNotIn(Pageable pageable, RequestProductDTO request, Long langId) throws Exception;

    Page<ProductInfoDTO> getProductByManufacturerId(Pageable pageable, Long merchantId, Long manufacturerId, Long langId) throws Exception;

    List<ProductIncentiveInformationDTO> getProductByIncentiveId(Long incentiveProgramId, Long langId) throws Exception;
}
