package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.kpi.KpiL1Level;
import com.nextsolutions.dcommerce.model.kpi.KpiMerchant;
import com.nextsolutions.dcommerce.service.FileService;
import com.nextsolutions.dcommerce.service.KpiMerchantService;
import com.nextsolutions.dcommerce.service.OrderV2Service;
import com.nextsolutions.dcommerce.shared.dto.kpi.KpiMerchantDTO;
import com.nextsolutions.dcommerce.shared.dto.kpi.KpiType;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.Query;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class KpiMerchantServiceImpl extends CommonServiceImpl implements KpiMerchantService {
    private final FileService fileService;
    private final OrderV2Service orderService;

    @Override
    @Transactional
    public KpiMerchant createKpiMerchant(KpiMerchant request) throws Exception {
        String filePath = request.getFilePath();
        if (!StringUtils.isEmpty(request.getFilePath())) {
            try {
                String relativeFilePath = fileService.savePdf(filePath);
                request.setFilePath(relativeFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        KpiMerchant kpiMerchant = save(request);
        if (Utils.isNotNull(request.getLstLevel())) {
            for (KpiL1Level levelDTO : request.getLstLevel()) {
                levelDTO.setKpiId(kpiMerchant.getId());
                save(levelDTO);
            }
        }
        return kpiMerchant;
    }

    @Override
    public Page<KpiMerchant> getListKpiL1(Pageable pageable, KpiMerchantDTO requestDTO) throws Exception {
        HashMap<String, Object> params = new HashMap<>();

        StringBuilder query = new StringBuilder();
        query.append("select * from kpi_l1 ");
        query.append("where 1 = 1 ");
        if (!StringUtils.isEmpty(requestDTO.getName())) {
            query.append(" and lower(name) like lower(concat('%',\"").append(requestDTO.getName()).append("\",'%'))");
        }
        if (requestDTO.getFromDate() != null) {
            query.append("and date(from_date) >= date( :fromDate ) ");
            params.put("fromDate", requestDTO.getFromDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }
        if (requestDTO.getToDate() != null) {
            query.append("and date(to_date) <= date( :toDate ) ");
            params.put("toDate", requestDTO.getToDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }

        List<KpiMerchant> lst = findAll(pageable, KpiMerchant.class, query.toString(), params);
        for (KpiMerchant kpiDTO : lst) {
            kpiDTO.setLstLevel(getKpiL1Levels(kpiDTO));
        }
        int count = getRowCountV2(String.format("select count(*) from (%s) x", query.toString()), params);
        return new PageImpl<>(lst, pageable, count);
    }

    @Override
    public KpiMerchant getKpiMerchantById(Long id) throws Exception {
        KpiMerchant kpiMerchant = find(KpiMerchant.class, id);
        if (kpiMerchant == null) {
            throw new CustomErrorException("Kpi not exist");
        }

        List<KpiL1Level> lstLevel = getKpiL1Levels(kpiMerchant);
        kpiMerchant.setLstLevel(lstLevel);
        return kpiMerchant;
    }

    private List<KpiL1Level> getKpiL1Levels(KpiMerchant kpiMerchant) throws Exception {
        String query = "select * from kpi_l1_level where kpi_id  = :kpiId order by target asc ";
        return findAll(null, KpiL1Level.class, query, kpiMerchant.getId());
    }

    @Override
    @Transactional
    public KpiMerchant editKpiMerchant(KpiMerchant request) {
        if (request.getId() == null) {
            throw new CustomErrorException("Id is not null");
        }
        KpiMerchant kpiMerchant = find(KpiMerchant.class, request.getId());
        if (kpiMerchant == null) {
            throw new CustomErrorException("Kpi not exist");
        }
        String filePath = request.getFilePath();
        if (!StringUtils.isEmpty(request.getFilePath())) {
            try {
                String relativeFilePath = fileService.savePdf(filePath);
                request.setFilePath(relativeFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Utils.copyNonNullProperties(request, kpiMerchant);
        save(kpiMerchant);
        if (Utils.isNotNull(kpiMerchant.getLstLevel())) {
            for (KpiL1Level levelRequest : request.getLstLevel()) {
                if (levelRequest.getId() != null) {
                    // Update
                    KpiL1Level level = find(KpiL1Level.class, levelRequest.getId());
                    Utils.copyNonNullProperties(levelRequest, level);
                    save(level);
                } else {
                    //Create new
                    levelRequest.setKpiId(kpiMerchant.getId());
                    save(levelRequest);
                }
            }
        }
        return request;
    }

    @Override
    public List<KpiMerchant> getKpiMerchant(Long merchantId) throws Exception {
        String query = "select * from kpi_l1 " +
                "where " +
                "date(current_date()) >= date(from_date) " +
                "and date(current_date()) < date(to_date) ";
        List<KpiMerchant> lstKpi = findAll(null, KpiMerchant.class, query);
        if (Utils.isNotNull(lstKpi)) {
            for (KpiMerchant kpiDTO : lstKpi) {
                kpiDTO.setLstLevel(getKpiL1Levels(kpiDTO));
                if (kpiDTO.getTypeId().equals(KpiType.BONUS_SALES.value())) {
                    kpiDTO.setCurrent(orderService.getAmountOrderSuccess(merchantId, kpiDTO.getFromDate(), kpiDTO.getToDate()));
                } else if (kpiDTO.getTypeId().equals(KpiType.BONUS_ACQUIRE_L2.value())) {
                    kpiDTO.setCurrent(new BigDecimal(getAcquireL2Success(merchantId, kpiDTO.getFromDate(), kpiDTO.getToDate())));
                }
            }
        }
        return lstKpi;
    }

    private BigInteger getAcquireL2Success(Long merchantId, LocalDateTime fromDate, LocalDateTime toDate) {
        String sql = "select count(distinct(m.MERCHANT_ID))  from merchant m " +
                "join orders orders on orders.MERCHANT_ID = m.MERCHANT_ID " +
                "where m.PARENT_MARCHANT_ID  = :merchantId " +
                "and DATE(orders.ORDER_DATE) >= DATE(:fromDate) and DATE(orders.ORDER_DATE) <= DATE(:toDate) ";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("merchantId", merchantId);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        return (BigInteger) query.getSingleResult();
    }
}
