package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.model.loyalty.Loyalty;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyMerchantGroup;
import com.nextsolutions.dcommerce.service.LoyaltyMerchantGroupService;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoyaltyMerchantGroupServiceImpl extends CommonServiceImpl implements LoyaltyMerchantGroupService {

    @Override
    public void save(List<LoyaltyMerchantGroup> loyaltyMerchantGroups, Long loyaltyId) throws Exception {
        Loyalty loyalty = null;
        if(loyaltyId == null || Utils.isEmpty(loyaltyMerchantGroups)){
            throw new CustomErrorException("bodyNull");
        }
        loyalty = find(Loyalty.class, loyaltyId);
        if(loyalty == null){

            throw new CustomErrorException("LoyaltyIdNotExist");

        }
        LoyaltyMerchantGroup entity;
        MerchantGroup merchantGroup;
        for(LoyaltyMerchantGroup element: loyaltyMerchantGroups)
        {
            if(element.getId() != null){
                entity = find(LoyaltyMerchantGroup.class, element.getId());
                if(entity == null) {
                    throw new CustomErrorException("LoyaltyMerchantGroupIdNotExist");
                }
            } else {
                entity = new LoyaltyMerchantGroup();
            }
            merchantGroup = find(MerchantGroup.class, element.getMerchantGroup().getId());
            if(merchantGroup == null) {
                throw new CustomErrorException("merchantGroupNotExist");
            }
            entity.setLoyaltyId(loyaltyId);
            entity.setMerchantGroup(merchantGroup);
            entity.setStatus(1);
            save(entity);
        }

    }

    @Override
    public void delete(Long id) throws Exception {
        LoyaltyMerchantGroup entity = find(LoyaltyMerchantGroup.class, id);
        if(entity == null) {
            throw new CustomErrorException("LoyaltyMerchantGroupNotExist");
        }

        remove(entity);
    }

}
