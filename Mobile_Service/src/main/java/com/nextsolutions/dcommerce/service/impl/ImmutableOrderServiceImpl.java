package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.LogisticsConfiguration;
import com.nextsolutions.dcommerce.repository.OrderRepository;
import com.nextsolutions.dcommerce.service.ImmutableOrderService;
import com.nextsolutions.dcommerce.ui.model.request.CancelOrderRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.OrderRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderDetailsResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.SimpleApiResponseModel;
import lombok.RequiredArgsConstructor;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ImmutableOrderServiceImpl implements ImmutableOrderService {

    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;
    private final LogisticsConfiguration logisticsConfiguration;

    @Override
    public Page<OrderResponseModel> findByParams(OrderRequestModel orderRequestModel, Pageable pageable) {
        return orderRepository.findByParams(orderRequestModel, pageable);
    }

    @Override
    public OrderDetailsResponseModel getOrderDetailsById(Long id, HttpHeaders headers) {
        List<String> authorization = headers.get("Authorization");
        HttpHeaders httpHeaders = new HttpHeaders();
        assert authorization != null;
        httpHeaders.set("Authorization", authorization.get(0));

        OrderDetailsResponseModel orderDetails = orderRepository.getOrderDetailsById(id);

        try {
            ResponseEntity<String> exchange = restTemplate.exchange(String.format("%s/{code}/status", logisticsConfiguration.getMerchantOrdersUrl()),
                    HttpMethod.GET, new HttpEntity<>(httpHeaders), String.class, orderDetails.getLogisticsCode());
            if (exchange.hasBody()) {
                String body = exchange.getBody();
                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode node = objectMapper.readTree(body);
                if (node.has("status")) {
                    orderDetails.setLogisticsOrderStatus(node.asInt());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            orderDetails.setLogisticsOrderStatus(-1);
        }
        return orderDetails;
    }

    @Override
    @Transactional
    public SimpleApiResponseModel cancelOrder(Long id, CancelOrderRequestModel request, HttpHeaders headers) {
        List<String> authorization = headers.get("Authorization");
        if (orderRepository.cancelOrderByIdWithReason(id, request.getReason())) {
            HttpHeaders httpHeaders = new HttpHeaders();
            assert authorization != null;
            httpHeaders.set("Authorization", authorization.get(0));
            restTemplate.exchange(String.format("%s/cancel/{code}", logisticsConfiguration.getMerchantOrdersUrl()),
                    HttpMethod.POST, new HttpEntity<>(httpHeaders), Object.class, request.getLogisticsCode());
        }
        return new SimpleApiResponseModel("Order canceled");
    }
}
