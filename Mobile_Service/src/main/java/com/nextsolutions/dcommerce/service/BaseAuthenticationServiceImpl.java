package com.nextsolutions.dcommerce.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nextsolutions.dcommerce.configuration.AuthenticationFacade;
import com.nextsolutions.dcommerce.shared.dto.OAuth2AuthenticationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class BaseAuthenticationServiceImpl {
    @Autowired
    protected AuthenticationFacade authenticationFacade;

    public OAuth2AuthenticationDto getCurrentOAuth2Details() throws IOException {
        if (this.authenticationFacade.getAuthentication() instanceof OAuth2Authentication) {
            OAuth2Authentication auth2Authentication = (OAuth2Authentication) this.authenticationFacade
                    .getAuthentication();
            Authentication authentication = auth2Authentication.getUserAuthentication();
            if (authentication.getDetails() instanceof Map<?, ?>) {
                ObjectMapper mapper = new ObjectMapper();
                Map<String, Object> map = (Map<String, Object>) authentication.getDetails();
                String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
                return mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY).readValue(json,
                        OAuth2AuthenticationDto.class);
            }
        }
        return null;
    }
}
