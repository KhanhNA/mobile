package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.repository.MerchantAdhereRepository;
import com.nextsolutions.dcommerce.model.MerchantAdhere;
import com.nextsolutions.dcommerce.service.MerchantAdhereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MerchantAdhereServiceImpl implements MerchantAdhereService {

    @Autowired
    private MerchantAdhereRepository repository;

    @Override
    public Page<MerchantAdhere> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public MerchantAdhere get(Integer integer) {
        return repository.findById(integer).orElse(null);
    }

    @Override
    public void save(MerchantAdhere object) {
        repository.save(object);
    }

    @Override
    public void delete(Integer integer) {
        repository.deleteById(integer);
    }
}
