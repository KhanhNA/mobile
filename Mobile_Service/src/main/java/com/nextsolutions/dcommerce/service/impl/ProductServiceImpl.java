package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.RecommendProductConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Product;
import com.nextsolutions.dcommerce.model.ProductDescription;
import com.nextsolutions.dcommerce.repository.CategoryRepository;
import com.nextsolutions.dcommerce.repository.PackingProductRepository;
import com.nextsolutions.dcommerce.repository.ProductRepository;
import com.nextsolutions.dcommerce.service.IncentiveService;
import com.nextsolutions.dcommerce.service.ProductService;
import com.nextsolutions.dcommerce.shared.constant.IncentiveType;
import com.nextsolutions.dcommerce.shared.constant.PriceType;
import com.nextsolutions.dcommerce.shared.dto.*;
import com.nextsolutions.dcommerce.shared.dto.incentivev2.DiscountType;
import com.nextsolutions.dcommerce.shared.dto.incentivev2.ProductIncentiveInformationDTO;
import com.nextsolutions.dcommerce.utils.CommonRuntimeData;
import com.nextsolutions.dcommerce.utils.StringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.persistence.Query;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl extends CommonServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final PackingProductRepository packingProductRepository;
    private final CategoryRepository categoryRepository;
    private final RestTemplate restTemplate;
    private final RecommendProductConfiguration recommendProductConfiguration;
    private final IncentiveService incentiveService;
    private HashMap<String, Object> params;

    @Override

    public Page<ProductDto> getAllProductCus(Pageable pageable, int langId) throws Exception {
        List<ProductDto> commonData = CommonRuntimeData.getProductDto(langId);
        if (commonData == null) {
            StringBuilder sqlStringBuilder = new StringBuilder();
            sqlStringBuilder.append(
                    "SELECT a.*, pt.QUANTITY, a.PRODUCT_CODE productCode, pd.PRODUCT_NAME productName, pd.PRODUCT_ID id, a.PACKING_URL urlImage FROM"
                            + "                 (SELECT"
                            + "                                 p.*, pp.PACKING_PRODUCT_ID, pp.PRICE, pp.PACKING_TYPE_ID, pp.MARKET_PRICE, pp.DISCOUNT_PERCENT, pp.PACKING_URL, pp.ORG_PRICE"
                            + "                 FROM  " + "                                 product p"
                            + "                 JOIN packing_product pp ON pp.PACKING_PRODUCT_ID = (SELECT"
                            + "                                 packing_product.PACKING_PRODUCT_ID"
                            + "                 FROM" + "                     packing_product"
                            + "                 WHERE  "
                            + "                     packing_product.PRODUCT_ID = p.PRODUCT_ID"
                            + "                 LIMIT 1)) AS a JOIN product_description pd ON pd.product_id = a.product_id"
                            + "                                LEFT JOIN packing_type pt ON pt.packing_type_id  = a.PACKING_TYPE_ID where pd.LANGUAGE_ID =:langId;");
            String sqlString = sqlStringBuilder.toString();
            commonData = this.findAll(null, ProductDto.class, sqlString, langId);
            commonData = CommonRuntimeData.storeProductDto(langId, commonData);
        }
        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = firstResult + pageable.getPageSize();
        List<ProductDto> resultList;
        if ((commonData == null) || (firstResult >= commonData.size())) {
            resultList = new ArrayList<>();
        } else {
            if (maxResults >= commonData.size()) {
                maxResults = commonData.size();
            }
            resultList = commonData.subList(firstResult, maxResults);
        }
        return new PageImpl<>(resultList, pageable, commonData.size());
    }

    @Override
    public ProductDetailDto getProductDetail(long id, Long langId) throws Exception {
        return this.productRepository.getProductDetail(id, langId);
    }

    @Override
    public ProductDetailDTOV2 getProductDetailV2(Long merchantId, Long id, Long langId) throws Exception {
        ProductDetailDTOV2 productDetail = findOne(ProductDetailDTOV2.class, getSqlProductDetail(), langId, langId, id);
        List<Long> lstPackingId = findAll(null, Long.class, getSqlPackingByProductId(), id);
        if (Utils.isNotNull(lstPackingId)) {
            for (Long packingId : lstPackingId) {
                boolean isPackingIncentive = false;
                IncentiveAppliedDTO incentiveProgramApplied = incentiveService.getIncentiveProgramAppliedFromPackingId(merchantId, packingId,
                        null, null, langId);
                PackingProductDTO dto = new PackingProductDTO();
                if (incentiveProgramApplied != null) {
                    if (incentiveProgramApplied.getIncentiveType() == IncentiveType.GROUP.value()
                            || incentiveProgramApplied.getIncentiveType() == IncentiveType.PRODUCT.value()) {
                        dto = findOne(PackingProductDTO.class, getSqlPackingDetailWithIncentiveId(),
                                incentiveProgramApplied.getIncentiveProgramId(), packingId);
                        if (dto != null) {
                            isPackingIncentive = true;
                        }
                    } else if (incentiveProgramApplied.getIncentiveType() == IncentiveType.FLASH_SALE.value()) {
                        CurrentTimeFlashSaleDTO timeFlashSaleDTO = incentiveService.flashSaleCurrentTime();
                        dto = incentiveService.getPackingFlashSaleByIncentiveId(incentiveProgramApplied.getIncentiveProgramId(), packingId, timeFlashSaleDTO.getSqlWhere());
                        if (dto != null) {
                            isPackingIncentive = true;
                            dto.setDuration(timeFlashSaleDTO.getDuration());
                        }
                    }
                    //Todo: Kiểm tra số suất khuyến mại

                    if (isPackingIncentive) {
                        int sold = dto.getSold() != null ? dto.getSold() : 0;
                        int limitedQuantity;
                        if (incentiveProgramApplied.getDiscountType() == DiscountType.HAS_INCENTIVE_PACKING.value()) {
                            limitedQuantity = dto.getLimitedPackingQuantity();
                        } else {
                            limitedQuantity = dto.getLimitedQuantity();
                        }
                        if (sold >= limitedQuantity) {
                            dto = findOne(PackingProductDTO.class, getSqlPackingDetailById(), packingId, PriceType.SALE_OUT.value());
                        } else {
                            productDetail.getMapIncentives().add(incentiveProgramApplied);
                        }
                    } else {
                        dto = findOne(PackingProductDTO.class, getSqlPackingDetailById(), packingId, PriceType.SALE_OUT.value());
                    }
                } else {
                    dto = findOne(PackingProductDTO.class, getSqlPackingDetailById(), packingId, PriceType.SALE_OUT.value());
                }
                productDetail.getPackingProductList().add(dto);
            }
        }
        productDetail.setImageList(getPackingImage(id, langId));

        return productDetail;
    }

    @Override
    public HashMap<Long, ProductDescription> getProductDesc(Long id) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("productId", id);
        List<ProductDescription> lst = this.findAll("from ProductDescription where productId = :productId", null,
                params, 0);
        lst = Utils.safe(lst);
        HashMap<Long, ProductDescription> ret = new HashMap<>();
        for (ProductDescription pd : lst) {
            ret.put(pd.getLanguageId(), pd);

        }
        return ret;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProductDto save(ProductDto entity) throws Exception {
        Product p = Utils.getData(entity, Product.class);
        this.save(p);
        Map<String, ProductDescription> descDto = Utils.safe(entity.getObjLang());
        descDto.forEach((k, v) -> {
            v.setLanguageId(Long.parseLong(k));
            Product product = new Product();
            product.setId(v.getId());
            v.setProduct(product);
            this.save(v);
        });

        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProductDto del(ProductDto entity) throws Exception {
        if ((entity == null) || (entity.getId() == null)) {
            throw new CustomErrorException("notExists", "product");
        }
        Product p = this.find(Product.class, entity.getId());
        if (p == null) {
            throw new CustomErrorException("notExists", "product");
        }
        p.setAvailable(false);
        this.save(p);
        return entity;
    }

    @Override
    public Page<ProductInfoDTO> findAllByCategory(Pageable pageable, Long merchantId, Long categoryId, Long langId) throws Exception {
        List<Long> lstCategory = findCategoryHierarchyById(categoryId);
        String whereByCategory = " and p.CATEGORY_ID in (" + Utils.join(",", lstCategory) + ") ";
        String queryProduct = String.format(getSqlProductV2(), whereByCategory + " group by pp.PRODUCT_ID ");

        List<Long> lstIncentiveId = incentiveService.getIncentiveIdApplied(merchantId, null, null, false);
        params = new HashMap<>();
        params.put("langId", langId);
        params.put("priceType", PriceType.SALE_OUT.value());

        String sqlCountProduct = String.format("select count(*) from (%s) x", queryProduct);
        int count = getRowCountV2(sqlCountProduct, params);

        List<ProductInfoDTO> lstProduct = findByNative(pageable, ProductInfoDTO.class, queryProduct, params);
        if (Utils.isNotNull(lstIncentiveId)) {
            getListPackingByProductIdAndIncentiveId(lstProduct, lstIncentiveId, langId);
        } else {
            getListPackingByProductId(langId, lstProduct);
        }

        return new PageImpl<>(lstProduct, pageable, count);
    }


    private void addPackingProduct(List<ProductDto> productDtos) {
        List<PackingProductDTO> packingProducts = this.packingProductRepository
                .findAllByProductIdInV2(productDtos.stream().map(ProductDto::getId).collect(Collectors.toList()));
        productDtos.forEach(x -> x.setPackingProductList(
                packingProducts.stream().filter(y -> y.getProductId().equals(x.getId())).collect(Collectors.toList())));
    }

    @Override
    public Page<ProductDto> getAllProductCusV2(Pageable pageable, Long langId, String keyWord) {
        Page<ProductDto> productDtoPage = this.packingProductRepository.findAll(pageable, langId, keyWord);
        List<ProductDto> content = productDtoPage.getContent();
        this.addPackingProduct(content);
        return new PageImpl<>(content, pageable, productDtoPage.getTotalElements());
    }


    @Override
    public Page<IncentiveDescriptionDTO> getIncentiveFromPacking(Pageable pageable, Long id, Long langId) {
        List<IncentiveDescriptionDTO> commonData = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        sql.append(" select ip.id incentiveProgramId,ipd.name, ipd.description, pip.incentive_program_type incentiveType from packing_incentive_program pip ");
        sql.append(" join incentive_program ip on ip.id = pip.incentive_program_id ");
        sql.append(" join incentive_program_description ipd on pip.incentive_program_id = ipd.incentive_program_id ");
        sql.append(" where pip.packing_product_id = :id and lang_id = :langId ");
        sql.append(" and DATE(ip.from_date) <= DATE(current_date()) and ( ip.to_date is null or DATE(ip.TO_DATE ) >=DATE(current_date())) ");
        try {
            commonData = this.findAll(pageable, IncentiveDescriptionDTO.class, sql.toString(), id, langId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new PageImpl<>(commonData, pageable, commonData.size());
    }

    @Override
    public Page<PackingProductDTO> getGroupon(Pageable pageable, Long langId) {
        List<PackingProductDTO> commonData = new ArrayList<>();
        String sqlString = "SELECT pd.PRODUCT_NAME,pp.PRODUCT_ID,pp.PACKING_URL,pp.packing_product_id,ifnull(temp.promotion_sale_price,pp.PRICE) price,pp.DISCOUNT_PERCENT, pp.ORG_PRICE,pp.MARKET_PRICE,pt.QUANTITY,TIMESTAMPDIFF(MINUTE,current_timestamp(),pip.sale_to_date)  duration  FROM dcommerce.packing_product pp JOIN packing_incentive_program pip ON pip.packing_product_id = pp.packing_product_id \n"
                + " LEFT JOIN packing_product temp on temp.PACKING_PRODUCT_ID = pp.PACKING_PRODUCT_ID and DATE(temp.from_date) <= DATE(CURRENT_DATE()) and ( current_date() <DATE(temp.to_date+1) or temp.TO_DATE is null ) "
                + " JOIN incentive_program ip ON ip.id = pip.incentive_program_id "
                + " JOIN product_description pd on pd.PRODUCT_ID = pp.PRODUCT_ID "
                + " JOIN packing_type pt on pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
                "  WHERE pip.incentive_program_type = 3 and pip.sale_from_date <= CURDATE() AND   (pip.sale_to_date IS NULL OR pip.sale_to_date >= CURDATE()) and pd.LANGUAGE_ID = :langId";
        try {
            commonData = this.findAll(pageable, PackingProductDTO.class, sqlString, langId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new PageImpl<>(commonData, pageable, commonData.size());
    }

    @Override
    public List<PackingProductDTO> getPriceForPacking(List<Long> lstPackingId) throws Exception {
        String sql = "select pp.PACKING_PRODUCT_ID packingProductId, ifnull(temp.PROMOTION_SALE_PRICE,pp.price) price from packing_product pp" +
                " LEFT JOIN packing_product temp ON temp.PACKING_PRODUCT_ID = pp.PACKING_PRODUCT_ID " +
                " AND DATE(CURRENT_DATE()) >= DATE(temp.from_date) and (DATE(temp.to_date )>= DATE(CURRENT_DATE()) or temp.TO_DATE is null)" +
                " where pp.PACKING_PRODUCT_ID in  " +
                " (" + Utils.join(",", lstPackingId) + ")";
        return findAll(null, PackingProductDTO.class, sql);
    }

    @Override
    public ProductCustomDTO getProductOfMerchant(Pageable pageable, List<String> lstPackingId, Long merchantId, Long langId, String keyWord, int filterType) throws IOException {
        //Get prediction
//        List<String> lstPredictionId = new LinkedList<>();
//        getLstPrediction(lstPackingId, merchantId, lstPredictionId);
        //
        Page<ProductDto> productDtoPage = this.packingProductRepository.findAllNotIn(pageable, lstPackingId, langId, filterType);
        List<ProductDto> content = productDtoPage.getContent();
        this.addPackingProduct(content);
        return ProductCustomDTO.builder()
                .lstPrediction(lstPackingId)
                .productDtoPage(new PageImpl<>(content, pageable, productDtoPage.getTotalElements()))
                .build();
    }

    @Override
    public ProductCustomDTO getPredictionProduct(Pageable pageable, List<String> lstPackingId, Long merchantId, Long langId, String keyWord) throws IOException {
        //Get prediction
        List<String> lstPredictionId = new LinkedList<>();
        getLstPrediction(lstPackingId, merchantId, lstPredictionId);
        //
        Page<ProductDto> productDtoPage = this.packingProductRepository.findAllIn(pageable, lstPredictionId, langId);
        List<ProductDto> content = productDtoPage.getContent();
        this.addPackingProduct(content);
        //
        if (lstPredictionId.isEmpty() && Utils.isNotEmpty(content)) {
            content.forEach(productDto -> {
                if (productDto.getPackingProductList() != null && productDto.getPackingProductList().size() > 0) {
                    lstPredictionId.add(productDto.getPackingProductList().get(0).getPackingProductId().toString());
                }
            });
        }
        return ProductCustomDTO.builder()
                .lstPrediction(lstPredictionId)
                .productDtoPage(new PageImpl<>(content, pageable, productDtoPage.getTotalElements()))
                .build();


    }

    @Override
    public Page<PackingSupperSaleDTO> getProductSupperSale(Pageable pageable, Long merchantId, Long langId) throws Exception {
        List<Integer> lstType = new ArrayList<>();
        lstType.add(1);
        lstType.add(2);
        List<Long> lstIncentiveId = incentiveService.getIncentiveIdApplied(merchantId, lstType, Stream.of(1).collect(Collectors.toList()), false);
        if (!Utils.isNotNull(lstIncentiveId)) {
            return new PageImpl<>(new ArrayList<>());
        }

        return productRepository.getProductSupperSale(pageable, lstIncentiveId, langId);
    }

    @Override
    public Page<ProductInfoDTO> getProductAll(Pageable pageable, Long merchantId, Long langId) throws Exception {
        String queryProduct = String.format(getSqlProduct(), getSqlSelectProductInfo(), "");

        String sqlCountProduct = String.format(getSqlProduct(), " count(*) ", " ");
        int count = getRowCountV2(sqlCountProduct, params);

        return new PageImpl<>(getProductByQuery(pageable, queryProduct, langId), pageable, count);
    }

    private List<ProductInfoDTO> getProductByQuery(Pageable pageable, String queryProduct, Long langId) throws Exception {
        List<ProductInfoDTO> lstProduct = findByNative(pageable, ProductInfoDTO.class, queryProduct, getParamsPacking(langId));
        if (Utils.isNotNull(lstProduct)) {
            getPackingFromProductId(lstProduct, langId);
        }
        return lstProduct;
    }

    @Override
    public Page<ProductInfoDTO> getProductAllNotIn(Pageable pageable, RequestProductDTO request, Long langId) throws Exception {
        HashMap<String, Object> params = getParamsPacking(langId);
        String sqlOrderBy = "";
        String sqlProduct = "";
        String sqlWhereAndOrderBy = "";
        if (request.getFilterType() == 1) {
            // Name
            sqlOrderBy = " order by pd.PRODUCT_NAME  ";
        } else if (request.getFilterType() == 2) {
            // Rating
            sqlOrderBy = " order by p.REVIEW_AVG desc  ";
        } else if (request.getFilterType() == 3) {
            // Price asc
            sqlOrderBy = " order by price.price asc ";
        } else if (request.getFilterType() == 4) {
            // Price desc
            sqlOrderBy = " order by price.price desc  ";
        }
        if (Utils.isNotNull(request.getLstPackingId())) {
            sqlWhereAndOrderBy = " AND pp.PACKING_PRODUCT_ID not in(" + Utils.join(",", request.getLstPackingId()) + ")  group by pp.PRODUCT_ID " + sqlOrderBy;
        } else {
            sqlWhereAndOrderBy = " group by pp.PRODUCT_ID " + sqlOrderBy;
        }
        sqlProduct = String.format(getSqlProduct(), getSqlSelectProductInfo(), sqlWhereAndOrderBy);

        List<ProductInfoDTO> lstProduct = findByNative(pageable, ProductInfoDTO.class, sqlProduct, params);
        if (Utils.isNotNull(lstProduct)) {
            getPackingFromProductId(lstProduct, langId);
        }

        String sqlCountProduct = String.format("select count(*) from (%s) x", sqlProduct);
        int countProduct = getRowCountV2(sqlCountProduct, getParamsPacking(langId));

        return new PageImpl<>(lstProduct, pageable, countProduct);
    }

    private void getPackingFromProductId(List<ProductInfoDTO> lstProduct, Long langId) throws Exception {
        for (ProductInfoDTO product : lstProduct) {
            List<PackingProductInformationDTO> lstPacking = packingProductRepository.getListPackingByProductId(product.getProductId(), langId);
            product.setPackingProductList(lstPacking);
        }
    }

    @Override
    public Page<ProductInfoDTO> getProductByManufacturerId(Pageable pageable, Long merchantId, Long manufacturerId, Long langId) throws Exception {
        if (manufacturerId == null) {
            throw new CustomErrorException("manufacturerId is not null");
        }
        List<Long> lstIncentiveId = incentiveService.getIncentiveIdApplied(merchantId, null, null, false);

        params = new HashMap<>();
        params.put("langId", langId);
        params.put("priceType", PriceType.SALE_OUT.value());

        String queryProduct = String.format(getSqlProductV2(), "and p.MANUFACTURER_ID = " + manufacturerId + " group by pp.PRODUCT_ID ");
        String sqlCountProduct = String.format("select count(*) from (%s) x", queryProduct);
        int count = getRowCountV2(sqlCountProduct, params);
        List<ProductInfoDTO> lstProduct = findByNative(pageable, ProductInfoDTO.class, queryProduct, params);
        if (Utils.isNotNull(lstIncentiveId)) {
            getListPackingByProductIdAndIncentiveId(lstProduct, lstIncentiveId, langId);
        } else {
            getListPackingByProductId(langId, lstProduct);
        }

        return new PageImpl<>(lstProduct, pageable, count);
    }

    private void getListPackingByProductId(Long langId, List<ProductInfoDTO> lstProduct) {
        for (ProductInfoDTO product : lstProduct) {
            List<PackingProductInformationDTO> lstPacking = packingProductRepository.getListPackingByProductId(product.getProductId(), langId);
            if (Utils.isNotNull(lstPacking)) {
                product.setPackingProductList(lstPacking);
                //Update product info
                updateProductInformation(product, lstPacking);
            }
        }
    }

    private void getListPackingByProductIdAndIncentiveId(List<ProductInfoDTO> lstProduct, List<Long> lstIncentiveId, Long langId) {
        for (ProductInfoDTO product : lstProduct) {
            List<PackingProductInformationDTO> lstPacking = packingProductRepository.getListPackingByProductIdAndIncentiveId(lstIncentiveId, product.getProductId(), langId);
            if (Utils.isNotNull(lstPacking)) {
                product.setPackingProductList(lstPacking);
                updateProductInformation(product, lstPacking);
            }
        }
    }

    private void updateProductInformation(ProductInfoDTO product, List<PackingProductInformationDTO> lstPacking) {
        //Update product info
        PackingProductInformationDTO packingInfo = lstPacking.get(0);
        product.setPackingProductId(packingInfo.getPackingProductId());
        product.setPackingProductCode(packingInfo.getPackingProductCode());
        product.setUrlImage(packingInfo.getPackingUrl());
        product.setQuantity(packingInfo.getQuantity());
        product.setProductPrice(packingInfo.getPrice());
        product.setOrgPrice(packingInfo.getOrgPrice());
        product.setDiscountPercent(packingInfo.getDiscountPercent());
    }

    @Override
    public List<ProductIncentiveInformationDTO> getProductByIncentiveId(Long incentiveProgramId, Long langId) throws Exception {
        List<PackingProductDTO> lstPacking = findAll(null, PackingProductDTO.class, queryProductByIncentiveId(), incentiveProgramId, langId);
        if (Utils.isNotNull(lstPacking)) {
            return groupProductByPacking(lstPacking);
        }
        return null;
    }


    private HashMap<String, Object> getParamsPacking(Long langId) {
        params = new HashMap<>();
        params.put("langId", langId);
        params.put("priceType", PriceType.SALE_OUT.value());
        return params;
    }

    private ProductIncentiveInformationDTO mappingProductInfo(PackingProductDTO packingDTO) {
        ProductIncentiveInformationDTO detail = new ProductIncentiveInformationDTO();
        detail.setId(packingDTO.getProductId());
        detail.setProductId(packingDTO.getProductId());
        detail.setPackingProductId(packingDTO.getPackingProductId());
        detail.setProductName(packingDTO.getProductName());
        detail.setUrlImage(packingDTO.getPackingUrl());
        detail.setReviewAvg(packingDTO.getReviewAvg());
        detail.setReviewCount(packingDTO.getReviewCount());
        detail.setProductPrice(packingDTO.getPrice());
        detail.setOrgPrice(packingDTO.getOrgPrice());
        detail.setQuantity(packingDTO.getQuantity());
        detail.setDiscountPercent(packingDTO.getDiscountPercent());
        return detail;
    }

    private void getLstPrediction(List<String> lstPackingId, Long merchantId, List<String> lstPredictionId) {
        if (lstPackingId == null) {
            try {
                ResponseEntity<String> response = restTemplate.exchange(
                        recommendProductConfiguration.getApiPrediction(),
                        HttpMethod.POST,
                        new HttpEntity<>(UserPrediction.builder()
                                .user(merchantId)
                                .num(20).build()),
                        String.class);
                //
                JSONObject jsonObject = new JSONObject(response.getBody());
                JSONArray jsonArray = jsonObject.getJSONArray("itemScores");
                if (!StringUtils.isEmpty(jsonArray.toString()) && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        if (object.has("item") && Utils.isNumeric(object.getString("item"))) {
                            lstPredictionId.add(object.getString("item"));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            lstPredictionId.addAll(lstPackingId);
        }
    }

    private String getSqlProductDetail() {
        return "SELECT " +
                "    pd.product_id ," +
                "    pd.product_name ," +
                "    pd.DESCRIPTION desProduct," +
                "    p.review_avg review_Avg," +
                "    p.review_count reviewCount," +
                "    mfd.name manufacturerName," +
                "    mfd.MANUFACTURER_ID ," +
                "    manu.MANUFACTURER_IMAGE manufacturerImg," +
                "    seller.name sellerName" +
                " FROM" +
                "    product p" +
                "    LEFT JOIN" +
                "    distributor seller ON p.DISTRIBUTOR_ID = seller.ID" +
                "    JOIN" +
                "    product_description pd ON pd.product_id = p.product_id AND pd.LANGUAGE_ID = :langId" +
                " LEFT JOIN" +
                " manufacturer manu ON manu.MANUFACTURER_ID = p.MANUFACTURER_ID" +
                " LEFT JOIN" +
                " manufacturer_description mfd ON mfd.MANUFACTURER_ID = manu.MANUFACTURER_ID AND mfd.LANGUAGE_ID = :langId" +
                " WHERE p.status =1 and " +
                " p.product_id = :productId";
    }

    private String getSqlPackingDetailWithIncentiveId() {
        return "select " +
                "pp.PACKING_PRODUCT_ID,price.price, " +
                "pp.code packingProductCode,pp.PACKING_URL, " +
                "pt.quantity,pp.MARKET_PRICE orgPrice,ipdi.limited_quantity, " +
                "ipdi.limited_packing_quantity ,ipdi.sold, " +
                "case " +
                "when ipdi.discount_type = 1 then ipdi.discount_percent " +
                "when ipdi.discount_type = 2 then (ipdi.discount_price / price.price) * 100 end discount_percent " +
                "from " +
                "packing_product pp " +
                "join packing_type pt on pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
                "join packing_price price on price.packing_product_id = pp.packing_product_id " +
                "left join incentive_program_detail_information ipdi on " +
                "ipdi.packing_id = pp.PACKING_PRODUCT_ID and ipdi.incentive_program_id = :incentiveId " +
                "where " +
                "pp.packing_product_id = :packingProductId " +
                "and date(current_date()) >= date(price.from_date) " +
                "and (date(price.to_date)>= date(current_date()) " +
                "or price.to_date is null) " +
                "and price.price_type = 2 ";
    }

    private String getSqlPackingDetailById() {
        return "select " +
                " pp.PACKING_PRODUCT_ID, " +
                " pprice.price, " +
                " pp.code packingProductCode, " +
                " pp.PACKING_URL, " +
                " pt.quantity, " +
                " pp.MARKET_PRICE orgPrice " +
                " from packing_product pp " +
                " join packing_type pt on " +
                " pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
                " join packing_price pprice on pprice.packing_product_id = pp.PACKING_PRODUCT_ID " +
                " where " +
                " pp.packing_product_id = :packingId " +
                " and pprice.price_type = :priceType " +
                " and date(current_date()) >= date(pprice.from_date) " +
                " and (date(pprice.to_date)>= date(current_date()) " +
                " or pprice.to_date is null) ";
    }

    private String getSqlListImage() {
        return "SELECT " +
                " pi.PRODUCT_IMAGE" +
                " FROM" +
                " product_image pi" +
                " JOIN" +
                " product_image_description pid ON pid.PRODUCT_IMAGE_ID = pi.PRODUCT_IMAGE_ID" +
                " WHERE" +
                " pi.PRODUCT_ID = :id" +
                " AND pid.LANGUAGE_ID = :langId";
    }

    private List<String> getPackingImage(Long id, Long langId) {
        Query getImageQuery = entityManager.createNativeQuery(getSqlListImage());
        getImageQuery.setParameter("id", id);
        getImageQuery.setParameter("langId", langId);
        return (List<String>) getImageQuery.getResultList();
    }


    private String getSqlSupperSaleWithIncentive(List<Long> lstIncentiveId) {
        return "SELECT " +
                "    pp.PRODUCT_ID productId,pp.PACKING_PRODUCT_ID packingProductId, " +
                "    pd.PRODUCT_NAME productName,pp.PACKING_URL packingUrl,pt.QUANTITY, " +
                "    ipdi.discount_price discountPrice," +
                "    ipdi.discount_type discountType,ipdi.sold,ipdi.limited_quantity limitedQuantity,price.price ," +
                "    CASE " +
                "    when ipdi.discount_type = 3 then pp.MARKET_PRICE " +
                "    else price.PRICE " +
                "    END orgPrice, " +
                "    CASE" +
                "    when ipdi.discount_type =1 then ipdi.discount_percent" +
                "    when ipdi.discount_type =2 then (ipdi.discount_price/ price.price)*100 " +
                "    ELSE 0 " +
                "    END discountPercent " +
                " FROM" +
                "  incentive_program_detail_information ipdi " +
                "  JOIN packing_product pp ON ipdi.packing_id = pp.PACKING_PRODUCT_ID" +
                "  JOIN product_description pd ON pp.PRODUCT_ID = pd.PRODUCT_ID" +
                "  JOIN packing_type pt ON pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID" +
                "  JOIN packing_price price on price.packing_product_id = pp.packing_product_id" +
                "  WHERE" +
                "  ipdi.incentive_program_id in (" + Utils.join(",", lstIncentiveId) + ")" +
                "  AND pd.LANGUAGE_ID = :langId" +
                " and date(current_date()) >= date(price.from_date) " +
                " and (date(price.to_date)>= date(current_date()) " +
                " or price.to_date is null) " +
                " and price.price_type = 2 " +
                " and ((ipdi.discount_type in (1,2) and ifnull(ipdi.sold,0) < ipdi.limited_quantity) " +
                " or ( ipdi.discount_type = 3 and ifnull(ipdi.sold,0) < ipdi.limited_packing_quantity )) ";
    }

    private List<Long> findCategoryHierarchyById(Long categoryId) {
        List<CategoryHierarchy> categories = categoryRepository.getCategoryHierarchyById(categoryId);
        List<Long> categoryIds = categories.stream().map(CategoryHierarchy::getId).collect(Collectors.toList());
        if (categoryIds.isEmpty())
            categoryIds.add(categoryId);
        return categoryIds;
    }

    private String getSqlPackingByProductId() {
        return "select pp.packing_product_id n" +
                " from packing_product pp " +
                " join packing_price price on " +
                " price.packing_product_id = pp.PACKING_PRODUCT_ID " +
                " where " +
                " product_id = :id " +
                " and date(current_date()) >= date(price.from_date) " +
                " and (date(price.to_date)>= date(current_date()) " +
                " or price.to_date is null) " +
                " and price.price_type = 2";
    }


    private String getSqlSelectProductInfo() {
        return "pp.PACKING_PRODUCT_ID packingProductId,pp.code packingProductCode, " +
                "pp.PACKING_URL urlImage,pt.quantity, " +
                "price.price productPrice,pp.MARKET_PRICE orgPrice, " +
                "pp.PRODUCT_ID productId, " +
                "pp.PRODUCT_ID id, " +
                "pd.product_name productName, " +
                "p.review_avg reviewAvg, " +
                "p.review_count reviewCount ";
    }


    private String getSqlProduct() {
        return "select " +
                " %s " +
                "from " +
                "product p " +
                "join packing_product pp on " +
                "p.PRODUCT_ID = pp.PRODUCT_ID " +
                "join product_description pd on " +
                "pd.PRODUCT_ID = p.PRODUCT_ID " +
                "and pd.LANGUAGE_ID = :langId " +
                "join packing_type pt on " +
                "pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
                "join packing_price price on " +
                "price.packing_product_id = pp.PACKING_PRODUCT_ID " +
                "and date(price.from_date) <= date(current_date()) " +
                "and (date(price.to_date)>= date(current_date()) " +
                "or price.to_date is null) " +
                "and price.price_type = :priceType " +
                "where " +
                "p.STATUS = 1 " +
                " %s ";
    }

    private String getSqlProductV2() {
        return "select " +
                "p.PRODUCT_ID productId, " +
                "p.PRODUCT_ID id, " +
                "p.review_avg reviewAvg, " +
                "p.review_count reviewCount,pd.product_name productName " +
                "from " +
                "product p " +
                "join packing_product pp on " +
                "p.PRODUCT_ID = pp.PRODUCT_ID " +
                "join product_description  pd on pd.PRODUCT_ID  = p.PRODUCT_ID and LANGUAGE_ID  = :langId " +
                "join packing_price price on " +
                "price.packing_product_id = pp.PACKING_PRODUCT_ID " +
                "and date(price.from_date) <= date(current_date()) " +
                "and (date(price.to_date)>= date(current_date()) " +
                "or price.to_date is null) " +
                "and price.price_type = :priceType " +
                "where " +
                "p.STATUS = 1 " +
                "%s ";
    }

    private List<ProductIncentiveInformationDTO> groupProductByPacking(List<PackingProductDTO> lstPacking) throws Exception {
        // Add packing
        List<ProductIncentiveInformationDTO> lstProduct = new ArrayList<>();
        if (Utils.isNotNull(lstPacking)) {
            Map<Long, List<PackingProductDTO>> groupByProductResult =
                    lstPacking.stream().collect(Collectors.groupingBy(PackingProductDTO::getProductId));
            for (Map.Entry<Long, List<PackingProductDTO>> entry : groupByProductResult.entrySet()) {
                ProductIncentiveInformationDTO productDt = mappingProductInfo(entry.getValue().get(0));
                productDt.setPackingProductList(entry.getValue());
                lstProduct.add(productDt);
            }
        }
        return lstProduct;
    }

    private String queryProductByIncentiveId() {
        return "select " +
                "pp.PRODUCT_ID, " +
                "pp.PRODUCT_ID id, " +
                "pd.product_name, " +
                "p.review_avg review_Avg, " +
                "p.review_count reviewCount , " +
                "pp.PACKING_PRODUCT_ID,price.price orgPrice, " +
                "pp.code packingProductCode,pp.PACKING_URL, " +
                "pt.quantity,ipdi.limited_quantity, " +
                "ipdi.limited_packing_quantity ,ipdi.sold, " +
                "case " +
                "when ipdi.discount_type = 1 then ipdi.discount_percent " +
                "when ipdi.discount_type = 2 then (ipdi.discount_price / price.price) * 100 end discountPercent " +
                "from " +
                "packing_product pp " +
                "join packing_type pt on pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
                "join packing_price price on price.packing_product_id = pp.packing_product_id " +
                "join incentive_program_detail_information ipdi on " +
                "ipdi.packing_id = pp.PACKING_PRODUCT_ID and ipdi.incentive_program_id = :incentiveProgramId " +
                "join product p on pp.PRODUCT_ID = p.PRODUCT_ID and pp.PACKING_PRODUCT_ID = ipdi.packing_id and p.STATUS = 1 " +
                "join product_description pd on pd.PRODUCT_ID = p.PRODUCT_ID and pd.LANGUAGE_ID = :langId " +
                "where " +
                "date(current_date()) >= date(price.from_date) " +
                "and (date(price.to_date)>= date(current_date()) " +
                "or price.to_date is null) " +
                "and price.price_type = 2 " +
                "and (ipdi.discount_type in(1,2) " +
                "and ifnull(ipdi.sold,0) <ipdi.limited_quantity) " +
                "or ( ipdi.discount_type = 3 and ifnull(ipdi.sold,0) <ipdi.limited_packing_quantity) ";
    }
}
