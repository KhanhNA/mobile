package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Coupon;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.service.CouponService;
import com.nextsolutions.dcommerce.utils.Utils;
import com.nextsolutions.dcommerce.model.CouponMerchant;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
public class CouponServiceImpl extends CommonServiceImpl implements CouponService {
    @Override
    public Coupon saveCoupon(Coupon dto) throws Exception {
        Coupon coupon;
        if(dto == null) {
            throw new CustomErrorException("BodyIsNull");
        }
        if (dto.getId() != null) {
            coupon = find(Coupon.class, dto.getId());
            if (coupon == null){
                throw new CustomErrorException("CouponNotExists");
            }
            coupon = Utils.getData(dto, coupon);
            coupon.setUpdateUser("thonv");
            coupon.setUpdateDate(LocalDateTime.now());
        } else {
            coupon = new Coupon();
            coupon = Utils.getData(dto, coupon);
            coupon.setCreateUser("thonv");
            coupon.setCreateDate(LocalDateTime.now());
        }
        if (Utils.isEmpty(dto.getCode())) {
            coupon.setCode(Utils.genRandomCode());
        }
        if (dto.getCouponType() == null || (dto.getCouponType() != 1 && dto.getCouponType() != 2)) {
            throw new CustomErrorException("couponTypeWrongFormat");
        }
        coupon.setStatus(1);
        coupon = save(coupon);
        return coupon;
    }

    public void addMerchant(List<Long> merchantIds, Long couponId) throws Exception{
        Coupon coupon = find(Coupon.class, couponId);
        if(coupon == null){
            throw new CustomErrorException("CouponNotExists");
        }
        Merchant merchant;
        String sql = "select 1 from CouponMerchant a where couponId = :couponId and a.merchant.merchantId = :merchantId and isUsed = 0 and status = 1";
        HashMap<String, Object> params = new HashMap<>();
        for(Long merchantId: merchantIds){
            merchant = find(Merchant.class, merchantId);
            if(merchant == null){
                throw new CustomErrorException("MerchantNotExists");
            }
            params.clear();
            params.put("couponId", couponId);
            params.put("merchantId", merchant.getMerchantId());
            List<Long> lst = findAll(sql, null, params, 1);
            if(lst != null && lst.size() > 0) {
                continue;
            }
            CouponMerchant couponMerchant = new CouponMerchant();
            couponMerchant.setMerchant(merchant);
            couponMerchant.setCouponId(couponId);
            couponMerchant.setIsUsed(CouponMerchant.IsUsedType.NEW);
            couponMerchant.setCreateDate(LocalDateTime.now());
            couponMerchant.setCreateUser("thonv");
            couponMerchant.setStatus(1);
            save(couponMerchant);
        }
    }

    @Override
    public void delMerchant(Long id) throws Exception {
        if(id == null) {
            throw new CustomErrorException("BodyIsNull");
        }
        CouponMerchant couponMerchant = find(CouponMerchant.class, id);
        if (couponMerchant == null){
            throw new CustomErrorException("CouponMerchantNotExist");
        }
        couponMerchant.setStatus(0);
        couponMerchant.setUpdateDate(LocalDateTime.now());
        couponMerchant.setUpdateUser("thonv");
        save(couponMerchant);
    }



    @Override
    public void delCoupon(Long id) throws Exception {
        if(id == null) {
            throw new CustomErrorException("BodyIsNull");
        }
        Coupon coupon = find(Coupon.class, id);
        if (coupon == null){
            throw new CustomErrorException("CouponNotExist");
        }
        coupon.setStatus(0);
        coupon.setUpdateDate(LocalDateTime.now());
        coupon.setUpdateUser("thonv");
        save(coupon);
    }

}
