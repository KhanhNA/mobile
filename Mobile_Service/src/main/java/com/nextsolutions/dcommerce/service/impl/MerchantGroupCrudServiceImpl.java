package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.repository.AutoIncrementJpaRepository;
import com.nextsolutions.dcommerce.repository.MerchantGroupAttributeRepository;
import com.nextsolutions.dcommerce.repository.MerchantGroupMerchantRepository;
import com.nextsolutions.dcommerce.repository.MerchantGroupRepository;
import com.nextsolutions.dcommerce.service.MerchantGroupCrudService;
import com.nextsolutions.dcommerce.shared.constant.AutoIncrementType;
import com.nextsolutions.dcommerce.shared.dto.form.MerchantGroupAttributeForm;
import com.nextsolutions.dcommerce.shared.dto.form.MerchantGroupForm;
import com.nextsolutions.dcommerce.ui.model.request.MerchantGroupQueryRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupDescriptionResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupMerchantResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.mapper.MerchantGroupMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MerchantGroupCrudServiceImpl implements MerchantGroupCrudService {

    private final MerchantGroupRepository merchantGroupRepository;
    private final MerchantGroupMerchantRepository merchantGroupMerchantRepository;
    private final MerchantGroupMapper merchantGroupMapper;
    private final MerchantGroupAttributeRepository merchantGroupAttributeRepository;
    private final AutoIncrementJpaRepository autoIncrementJpaRepository;

    @Override
    public Page<MerchantGroupResource> findAll(String keyword, Pageable pageable, Integer languageId) {
        return merchantGroupRepository.findAllByNativeSql(keyword, pageable, languageId);
    }

    @Override
    public List<MerchantGroupResource> findAll(String keyword, Integer languageId, Long merchantId) {
        return merchantGroupRepository.findAllByNativeSql(keyword, languageId, merchantId);
    }

    @Override
    public Page<MerchantGroupResource> findByQuery(MerchantGroupQueryRequestModel query, Pageable pageable) {
        return merchantGroupRepository.findByQuery(query, pageable);
    }

    @Override
    public MerchantGroupForm getMerchantGroupFormById(Long id) {
        return merchantGroupRepository.findByIdAndActivated(id)
                .map(this::toMerchantGroupForm)
                .orElseThrow(() -> new EntityNotFoundException("Merchant group with id = " + id + " not found"));
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(rollbackFor = Exception.class)
    public MerchantGroupForm createMerchantGroup(MerchantGroupForm form) {
        MerchantGroup merchantGroup = new MerchantGroup();
        AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.MERCHANT_GROUPS.value());
        Long code = Long.valueOf(autoIncrement.getValue()) + 1;
        merchantGroup.setCode(code.toString());
        merchantGroup.setStatus(1);

        merchantGroupRepository.save(merchantGroup);
        autoIncrement.setValue(code.toString());
        autoIncrementJpaRepository.save(autoIncrement);
        merchantGroup.setDescriptions(Optional
                .ofNullable(form.getDescriptions())
                .map(resources -> resources.stream().map(resource -> {
                    MerchantGroupDescription description = new MerchantGroupDescription();
                    description.setName(resource.getName());
                    description.setMerchantGroupId(merchantGroup.getId());
                    description.setLanguageId(resource.getLanguageId());
                    return description;
                }).collect(Collectors.toList()))
                .orElseThrow(() -> new RuntimeException("Merchant group description is empty"))
        );

        merchantGroup.setAttributes(toMerchantGroupAttributes(form, merchantGroup));

        merchantGroupRepository.save(merchantGroup);

        return toMerchantGroupForm(merchantGroup);
    }

    @Override
    @Transactional
    public MerchantGroupForm updateMerchantGroup(Long id, MerchantGroupForm form) {
        MerchantGroup merchantGroup = merchantGroupRepository.findByIdAndActivated(id)
                .orElseThrow(() -> new EntityNotFoundException("Merchant group with id = " + id + " not found"));

        Map<Long, MerchantGroupDescriptionResource> merchantDescriptionFormMap = form.getDescriptions()
                .stream()
                .collect(Collectors.toMap(MerchantGroupDescriptionResource::getId, resource -> resource));

        merchantGroup.getDescriptions().forEach(merchantGroupDescription -> {
            if (merchantDescriptionFormMap.containsKey(merchantGroupDescription.getId())) {
                MerchantGroupDescriptionResource resource = merchantDescriptionFormMap.get(merchantGroupDescription.getId());
                merchantGroupDescription.setMerchantGroupId(merchantGroup.getId());
                merchantGroupDescription.setLanguageId(resource.getLanguageId());
                merchantGroupDescription.setName(resource.getName());
            }
        });
        merchantGroupAttributeRepository.deleteByMerchantGroupId(merchantGroup.getId());

        merchantGroup.setAttributes(toMerchantGroupAttributes(form, merchantGroup));

        merchantGroupRepository.saveAndFlush(merchantGroup);

        return toMerchantGroupForm(merchantGroup);
    }

    @Override
    public void inactiveMerchantGroup(Long id) {
        merchantGroupRepository.inactiveMerchantGroup(id);
    }

    @Override
    public List<MerchantGroupMerchantResource> findAllMerchantGroupMerchantById(Long id) {
        return merchantGroupRepository.findAllMerchantGroupMerchantById(id);
    }

    @Override
    @Transactional
    public void saveMerchantGroupMerchants(Long id, Map<Long, MerchantGroupMerchantResource> resources) {
        List<MerchantGroupMerchant> merchantGroupMerchants = merchantGroupMerchantRepository.findByMerchantGroupId(id);
        Iterator<MerchantGroupMerchant> merchantGroupMerchantIterator = merchantGroupMerchants.iterator();
        while (merchantGroupMerchantIterator.hasNext()) {
            MerchantGroupMerchant merchantGroupMerchant = merchantGroupMerchantIterator.next();
            Long merchantId = merchantGroupMerchant.getMerchantId();
            if (resources.containsKey(merchantId)) {
                merchantGroupMerchant.setMerchantId(resources.get(merchantId).getMerchantId());
                resources.remove(merchantId);
            } else {
                merchantGroupMerchantRepository.delete(merchantGroupMerchant);
                merchantGroupMerchantIterator.remove();
            }
        }
        List<MerchantGroupMerchant> merchantGroupMerchantWillAdding = resources.values()
                .stream()
                .map(merchantResource -> MerchantGroupMerchant.builder().merchantId(merchantResource.getMerchantId()).merchantGroupId(id).build()).collect(Collectors.toList());
        if (!merchantGroupMerchantWillAdding.isEmpty()) {
            merchantGroupMerchants.addAll(merchantGroupMerchantWillAdding);
        }
        merchantGroupMerchantRepository.saveAll(merchantGroupMerchants);
    }

    private List<MerchantGroupAttribute> toMerchantGroupAttributes(MerchantGroupForm form, MerchantGroup merchantGroup) {
        return Optional
                .ofNullable(form.getAttributes())
                .map(attributes -> attributes.values().stream()
                        .map(attributeForm -> {
                            MerchantGroupAttribute merchantGroupAttribute = new MerchantGroupAttribute();
                            merchantGroupAttribute.setAttributeId(attributeForm.getMerchantGroupAttributeId());
                            merchantGroupAttribute.setAttributeValueId(attributeForm.getMerchantGroupAttributeValueId());
                            merchantGroupAttribute.setMerchantGroupId(merchantGroup.getId());
                            return merchantGroupAttribute;
                        }).collect(Collectors.toList()))
                .orElse(null);
    }

    private MerchantGroupForm toMerchantGroupForm(MerchantGroup merchantGroup) {
        MerchantGroupForm merchantGroupForm = merchantGroupMapper.toMerchantGroupForm(merchantGroup);
        merchantGroupForm.setAttributes(Optional
                .ofNullable(merchantGroup.getAttributes())
                .map(attributes -> attributes
                        .stream()
                        .collect(Collectors.toMap(MerchantGroupAttribute::getAttributeId, attribute -> {
                            MerchantGroupAttributeForm form = new MerchantGroupAttributeForm();
                            form.setId(attribute.getId());
                            form.setMerchantGroupAttributeId(attribute.getAttributeId());
                            form.setMerchantGroupAttributeValueId(attribute.getAttributeValueId());
                            form.setMerchantGroupId(attribute.getMerchantGroupId());
                            return form;
                        })))
                .orElse(null));
        return merchantGroupForm;
    }
}
