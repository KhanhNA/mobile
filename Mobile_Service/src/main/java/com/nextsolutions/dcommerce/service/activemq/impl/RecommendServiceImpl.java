package com.nextsolutions.dcommerce.service.activemq.impl;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.nextsolutions.dcommerce.configuration.properties.RecommendProductConfiguration;
import com.nextsolutions.dcommerce.service.activemq.RecommendService;
import com.nextsolutions.dcommerce.shared.dto.recommend.RecommendProduct;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RecommendServiceImpl implements RecommendService {
	private final RestTemplate recommendProductRestTemplate;
	private final RecommendProductConfiguration recommendProductConfiguration;

	@Override
	public boolean recommend(RecommendProduct recommendProductRequest) {
		try {
			ResponseEntity<String> response = recommendProductRestTemplate.exchange(getAuthenticateUri(),
					HttpMethod.POST, new HttpEntity<>(recommendProductRequest), String.class);
			if (response.getStatusCode().is2xxSuccessful()) {
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	private String getMessage(Exception e) {
		if (e instanceof HttpClientErrorException) {
			HttpClientErrorException httpClientErrorException = (HttpClientErrorException) e;
			return httpClientErrorException.getResponseBodyAsString();
		}
		if (e instanceof HttpServerErrorException) {
			HttpServerErrorException serverErrorException = (HttpServerErrorException) e;
			return serverErrorException.getResponseBodyAsString();
		}
		return e.getMessage();
	}

	public String getAuthenticateUri() {
		return UriComponentsBuilder.fromUriString(recommendProductConfiguration.getApi())
				.queryParam("accessKey", recommendProductConfiguration.getAccessKey()).toUriString();
	}
}
