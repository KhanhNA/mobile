package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.OAuthConfiguration;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.Role;
import com.nextsolutions.dcommerce.model.UserEntity;
import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.service.AccountService;
import com.nextsolutions.dcommerce.service.MerchantCrudService;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.dto.HunterDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.ui.model.request.MerchantCrudRequestModel;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
public class MerchantCrudServiceImpl implements MerchantCrudService {

    private final MerchantJpaRepository merchantJpaRepository;
    private final AccountService accountService;
    private final OAuthConfiguration oAuthConfiguration;
    private final CommonServiceImpl commonService;

    @Override
    public Page<MerchantCrudResource> findByQuery(MerchantCrudRequestModel query, Pageable pageable) {
        return merchantJpaRepository.findByQuery(query, pageable);
    }

    @Override
    @Transactional
    public void deactivate(Long id, String username) {
        Merchant merchant = merchantJpaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Merchant with id " + id + " doesn't exists"));
        accountService.deactivateAccount(merchant.getUserName());
        merchantJpaRepository.deactivate(id, username);
    }

    @Override
    @Transactional
    public void activate(Long id, String username) {
        List<Role> lstRoleL1 = new ArrayList<>();
        if (Utils.isNotNull(oAuthConfiguration.getMerchantRoles())) {
            oAuthConfiguration.getMerchantRoles().forEach(roleId -> lstRoleL1.add(new Role(roleId)));
        }

        Merchant merchant = merchantJpaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Merchant with id " + id + " doesn't exists"));
        UserEntity userEntity = UserEntity.builder()
                .firstName(merchant.getFullName())
                .lastName("L1")
                .username(merchant.getUserName())
                .enabled(true)
                .roles(lstRoleL1)
                .build();
        accountService.createAccount(userEntity);
        merchantJpaRepository.activate(id, username);
    }

    @Override
    @Transactional
    public void reactivate(Long id, String username) {
        Merchant merchant = merchantJpaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Merchant with id " + id + " doesn't exists"));
        accountService.activateAccount(merchant.getUserName());
        merchantJpaRepository.reactivate(id, username);
    }

    @Override
    @Transactional
    public void rejectActivate(Long id, String username) {
        merchantJpaRepository.rejectActivate(id, username);
    }

    @Override
    @Transactional
    public void rejectAction(Long id, String username) {
        merchantJpaRepository.rejectAction(id, username);
    }

    @Override
    @Transactional
    public void updateWalletId(Long id, Long walletId) {
        merchantJpaRepository.updateWalletId(id, walletId);
    }

    @Override
    public boolean updateContractNumber(Long id, HunterDto hunterDto) {
        Integer result = merchantJpaRepository.updateContractNumber(id, hunterDto.getContractNumber());
        return result == 1;
    }

    @Override
    public Merchant confirmKpi(Long id) {
        Merchant merchant = merchantJpaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Merchant with id " + id + " doesn't exists"));
        merchant.setConfirmKpi(!merchant.getConfirmKpi());
        merchantJpaRepository.save(merchant);
        return merchant;
    }

    @Override
    public Page<MerchantDto> getAll(MerchantDto keyWord, Pageable pageable) throws Exception {
        String select = " SELECT " +
                "m.merchant_id id, " +
                "m.merchant_code merchantCode, " +
                "m.user_name username, " +
                "m.full_name fullName, " +
                "m.mobile_phone mobilePhone, " +
                "m.address address, " +
                "m.create_date registerDate, " +
                "m.wallet_client_id walletId, " +
                "m.status status, " +
                "m.action action ";
        StringBuilder query = new StringBuilder();
        HashMap<String, Object> params = new HashMap<>();
        query.append(select);
        query.append("FROM merchant m WHERE m.merchant_type_id is not null and m.status != 2 ");
        if (Objects.nonNull(keyWord) && keyWord.getUserName() != null) {
            query.append("AND (m.user_name like CONCAT('%', :username ,'%') or m.mobile_phone like CONCAT('%', :phoneNumber ,'%'))");
            params.put("username", keyWord.getUserName());
            params.put("phoneNumber", keyWord.getUserName());
        }
        String selectAll = query.toString() + SpringUtils.getOrderBy(pageable);
        String getCount = String.format("Select count(*) from (%s) x", query.toString());
        List<MerchantDto> result = commonService.findAll(pageable, MerchantDto.class, selectAll, params);
        Integer count = commonService.getRowCountV2(getCount, params);
        return new PageImpl<>(result, pageable, count);
    }
}
