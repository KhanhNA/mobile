package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.MerchantActiveCode;

public interface MerchantGenCodeService extends BaseService<MerchantActiveCode, Long> {
    MerchantActiveCode findByActiveCode(String code);

}
