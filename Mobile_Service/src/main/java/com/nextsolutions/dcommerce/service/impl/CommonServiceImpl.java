package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.model.DescEntityBase;
import com.nextsolutions.dcommerce.model.Language;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.utils.CommonRuntimeData;
import com.nextsolutions.dcommerce.utils.Utils;
import org.hibernate.internal.SessionImpl;
import org.hibernate.transform.Transformers;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.util.*;

@Primary
@Service
public class CommonServiceImpl implements CommonService {

    @PersistenceContext
    protected EntityManager entityManager;

    public <T> Page<T> findAll(T param, Pageable pageable, Integer maxResult) throws Exception {
        String sql = String.format(" from %s t where (1=1) ", param.getClass().getName());
        HashMap<String, Object> params = new HashMap<>();
        StringBuilder where = Utils.buildWhere(param, "t ", params);
        HashMap<String, Object> otherCond = Utils.getOtherCond(param);
        String orderBy = (String) otherCond.get("ORDER_BY ");
        sql = sql + where.toString();
        if (orderBy != null) {
            sql += orderBy;
        }
        List<T> lst = Utils.safe(findAll(sql, pageable, params, maxResult));
        Integer count = lst.size();

        if (maxResult == null || maxResult == 0) {
            String sqlCount = String.format("select count(t) from %s t where (1=1) ", param.getClass().getName()) + where.toString();
            count = getRowCount(sqlCount, params);
        }
        //}
        return new PageImpl<>(lst, pageable, count);

    }

    public <T> Page<T> findAll(String sql, T param, Pageable pageable, Integer maxResult) throws Exception {
//        String sql = String.format(" from %s t where (1=1) ", param.getClass().getName());
        HashMap<String, Object> params = new HashMap<>();

        List<T> lst = Utils.safe(findAll(sql, pageable, params, maxResult));
        Integer count = lst.size();

        if (maxResult == null || maxResult == 0) {
            String sqlCount = String.format("select count(t) from %s t where (1=1) ", sql);
            count = getRowCount(sqlCount, params);
        }
        //}
        return new PageImpl<>(lst, pageable, count);

    }

    //findAll: tim kiem theo entity theo pageable
    public <T> List<T> findAll(String sql, Pageable pageable, HashMap<String, Object> params, Integer maxResult) {
        Query query = entityManager.createQuery(sql);
        if (params != null && !params.isEmpty()) {
            for (HashMap.Entry me : params.entrySet()) {
                query.setParameter((String) me.getKey(), me.getValue());
            }
        }

        if (pageable != null && (maxResult == null || maxResult == 0)) {
            int firstResult = pageable.getPageNumber() * pageable.getPageSize();
            int maxResults = pageable.getPageSize();

            query.setFirstResult(firstResult);
            query.setMaxResults(maxResults);
        } else if (pageable != null && maxResult == -1) {
            query.setMaxResults(Integer.MAX_VALUE);
        } else if (maxResult != null && maxResult > 0) {
            query.setMaxResults(maxResult);
        }

        return query.getResultList();
    }


    public Integer getRowCount(String sql, HashMap<String, Object> params) {
        Query query = entityManager.createQuery(sql);
        if (params != null) {
            for (HashMap.Entry me : params.entrySet()) {
                query.setParameter((String) me.getKey(), me.getValue());
            }
        }
        Number count = (Number) query.getSingleResult();
        if (count == null) {
            count = 0;
        }
        return count.intValue();
    }

    public Integer getRowCountV2(String sql, HashMap<String, Object> params) {
        Query query = entityManager.createNativeQuery(sql);
        if (params != null) {
            for (HashMap.Entry me : params.entrySet()) {
                query.setParameter((String) me.getKey(), me.getValue());
            }
        }
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    public Integer getRowCountV2(String sql, Object... params) {
        Query query = entityManager.createNativeQuery(sql);
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                query.setParameter(i + 1, params[i]);
            }
        }
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    public Integer getNumBerValue(String sql, HashMap<String, Object> params) {
        Query query = entityManager.createNativeQuery(sql);
        if (params != null) {
            for (HashMap.Entry me : params.entrySet()) {
                query.setParameter((String) me.getKey(), me.getValue());
            }
        }
        BigDecimal count = (BigDecimal) query.getSingleResult();
        return count.intValue();
    }

    @Override
    public Long getRowCountReturnLong(String sql, HashMap<String, Object> params) {
        Query query = entityManager.createNativeQuery(sql);
        if (params != null) {
            for (HashMap.Entry me : params.entrySet()) {
                query.setParameter((String) me.getKey(), me.getValue());
            }
        }
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.longValue();
    }

    //tim kiem theo entity tat ca
    public <T> List<T> findAll(Class<T> clazz, String sql, HashMap<String, Object> params, Integer maxResult) {
        return findAll(sql, null, params, maxResult);

    }

    //findall: tim kiem theo resultset, tham so truyen vao la cac tham so goi ham
    public <T> List<T> findAll(Pageable pageable, Class<T> clazz, String sql, Object... params) throws Exception {

        sql = convertNaming2Quest(sql, null, null);
        List<Object> lstparams = new ArrayList<>();
        if (params != null) {
            for (Object obj : params) {
                lstparams.add(obj);
            }
        }
        HashMap<String, String> columnsMap = new HashMap<>();
        ResultSet rs = getResultSet(sql, lstparams, columnsMap);
        return findAll(pageable, clazz, rs, columnsMap);
    }

    //findall: tim kiem theo resultset, tham so truyen vao la 1 list
    public <T> List<T> findAll(Pageable pageable, Class<T> clazz, String sql, List<Object> params) throws Exception {

        sql = convertNaming2Quest(sql, null, null);
        HashMap<String, String> columnsMap = new HashMap<>();
        ResultSet rs = getResultSet(sql, params, columnsMap);
        return findAll(pageable, clazz, rs, columnsMap);

    }

    //findall: tim kiem cau lenh sql viet theo kieu naming, tham so truyen vao la hashmap
    public <T> List<T> findAll(Pageable pageable, Class<T> clazz, String sql, HashMap params) throws Exception {

        List<Object> paramsOut = new ArrayList<>();
        sql = convertNaming2Quest(sql, params, paramsOut);
        HashMap<String, String> columnsMap = new HashMap<>();
        ResultSet rs = getResultSet(sql, paramsOut, columnsMap);
        return findAll(pageable, clazz, rs, columnsMap);

    }

    //getResult: dau ra la resultset va bang ax cot: columns
    public ResultSet getResultSet(String sql, List<Object> params, HashMap columnsMap) throws Exception {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        PreparedStatement st = cnn.prepareStatement(sql);
        int i = 1;
        for (Object obj : Utils.safe(params)) {
            st.setObject(i++, obj);
        }

        ResultSet rs = st.executeQuery();
        if (rs != null) {
            ResultSetMetaData metaData = rs.getMetaData();
            columnsMap.putAll(loadColumn(metaData));
        }
        return rs;

    }

    //findAll: tim kiem tham so truyen vao la Resultset
    public <T> List<T> findAll(Pageable pageable, Class<T> clazz, ResultSet rs, HashMap columnsMap) throws Exception {
        int row = 0;

        List<T> result = new ArrayList<>();
        int firstResult = 0;
        int maxResults = 0;
        if (pageable != null) {
            firstResult = pageable.getPageNumber() * pageable.getPageSize();
            maxResults = firstResult + pageable.getPageSize();
        }
        while (rs.next()) {
            if (row < firstResult) {
                row++;
                continue;
            }
            if (maxResults != 0 && row >= maxResults) {
                break;
            }
            result.add(convert2Class(clazz, rs, columnsMap, row++));
        }
        return result;
    }

    public <T> T findOne(Class<T> clazz, ResultSet rs, HashMap columnsMap) throws Exception {
        return convert2Class(clazz, rs, columnsMap, 0);
    }

    //tim kiem count phan tu tiep theo
    public <T> List<T> findNext(Class<T> clazz, ResultSet rs, HashMap columnsMap, Integer count) throws Exception {
        int row = 0;

        List<T> result = new ArrayList<>();

        while (rs.next()) {

            if (count != 0 && row >= count) {
                break;
            }
            result.add(convert2Class(clazz, rs, columnsMap, row++));
        }
        return result;

    }

    public Long getSequence(String s) {
        s = String.format("select nextval(%s)", s);
        Query q = entityManager.createNativeQuery(s);

        return ((BigInteger) q.getSingleResult()).longValue();

    }

    public <T> T save(T entity) {
        return entityManager.merge(entity);
    }

    public <T> T find(Class<T> entityClass, Object primaryKey) {
        return entityManager.find(entityClass, primaryKey);
    }

    public <T> List<T> findByNative(Pageable pageable, Class<T> clazz, String sql, HashMap<String, Object> params) {
        Query nativeQuery = entityManager.createNativeQuery(sql);
        if (pageable != null) {
            int firstResult = pageable.getPageNumber() * pageable.getPageSize();
            int maxResults = pageable.getPageSize();
            nativeQuery.setFirstResult(firstResult);
            nativeQuery.setMaxResults(maxResults);
        }
        params.forEach(nativeQuery::setParameter);
        return nativeQuery.unwrap(org.hibernate.query.NativeQuery.class)
                .setResultTransformer(Transformers.aliasToBean(clazz)).getResultList();
    }

    public <T> List<T> findByNative(Pageable pageable, Class<T> clazz, String sql, String paramName, Object paramValue) {
        Query nativeQuery = entityManager.createNativeQuery(sql);
        if (pageable != null) {
            int firstResult = pageable.getPageNumber() * pageable.getPageSize();
            int maxResults = pageable.getPageSize();
            nativeQuery.setFirstResult(firstResult);
            nativeQuery.setMaxResults(maxResults);
        }
        nativeQuery.setParameter(paramName, paramValue);
        return nativeQuery.unwrap(org.hibernate.query.NativeQuery.class)
                .setResultTransformer(Transformers.aliasToBean(clazz)).getResultList();
    }


    public <T> HashMap<String, DescEntityBase> getDescFromIds(Class<T> clazz, Integer langId, List<Long> packingIds) throws Exception {
        String className = clazz.getName();

        String sql = String.format("from %s where languageId = :languageId and objectId in(:ids)", className);
        HashMap<String, Object> params = new HashMap<>();
        params.put("languageId", langId);
        params.put("ids", packingIds);
        List<DescEntityBase> pds = Utils.safe(findAll(clazz, sql, params, 0));
        String key;
        HashMap<String, DescEntityBase> result = new HashMap<>();
        for (DescEntityBase pd : pds) {

            key = String.format("%s_%d_%d", pd.getName(), langId, pd.getObjectId());

            CommonRuntimeData.cacheLanguage.put(key, pd);
            result.put(key, pd);
        }
        return result;
    }


    private HashMap loadColumn(ResultSetMetaData metaData) throws Exception {
        HashMap<String, String> columns = new HashMap<>();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            columns.put(metaData.getColumnLabel(i).replaceAll("_", "").toLowerCase(), metaData.getColumnLabel(i));
        }
        return columns;
    }

    private <T> T convert2Class(Class<T> clazz, ResultSet rs, HashMap columnNames, int row)
            throws Exception {
        if (clazz == Long.class) {
            return (T) rs.getObject(1);
        }
        Field[] fields = clazz.getDeclaredFields();
        String fieldName;
        T returnClass = clazz.newInstance();
        for (Field field : fields) {
            int modifier = field.getModifiers();
            if (Modifier.isStatic(modifier) || Modifier.isFinal(modifier)) {
                continue;
            }
            if (Modifier.isPrivate(modifier)) {
                field.setAccessible(true);
            }
            fieldName = field.getName();
            String columnName = (String) columnNames.get(fieldName.toLowerCase());
            if (StringUtils.isEmpty(columnName)) {
                continue;
            }

            Object value = rs.getObject(columnName, field.getType());
            field.set(returnClass, value);

        }
        return returnClass;
    }


    public String validateData(Object dto, Action actionType) {
        return null;
    }


    public String convertNaming2Quest(String sql, HashMap paramIn, List paramsOut) {
        int pos;
        String paramName;
        Object value;
        while ((pos = sql.indexOf(":")) != -1) {
            int end = sql.substring(pos).indexOf(" ");
            if (end == -1)
                end = sql.length();
            else
                end += pos;
            if (paramIn != null && paramsOut != null) {
                paramName = sql.substring(pos + 1, end);
                value = paramIn.get(paramName);

                paramsOut.add(value);

            }
            sql = sql.substring(0, pos) + "?" + sql.substring(end);
        }
        return sql;
    }

    public <Dto, T> Dto save(Dto dto, Class<T> clazz, Action type) throws Exception {

        T instance = clazz.newInstance();
        HashMap<String, Field> destFields = new HashMap<>();
        instance = Utils.getData(dto, clazz, destFields);
        Field fieldUser = destFields.get(LogCol.createDate);
        Field fieldDate = destFields.get(LogCol.createDate);
        if (type == Action.INSERT) {
            fieldUser = destFields.get(LogCol.createDate);
            fieldDate = destFields.get(LogCol.createDate);
        } else if (type == Action.CHANGE) {
            fieldUser = destFields.get(LogCol.updateUser);
            fieldDate = destFields.get(LogCol.updateDate);
        }
        if (fieldDate != null) {
            fieldDate.setAccessible(true);

            fieldDate.set(instance, Utils.getLocalDatetime());
        }

        save(instance);
        return dto;
    }

    @Override
    public void remove(Object entity) {
        entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
    }

    @Override
    public HashMap<String, Language> loadLanguages(String langCode) throws Exception {
        String sql = "from Language";
        HashMap<String, Object> params = new HashMap<>();
        if (!Utils.isEmpty(langCode)) {
            sql += " where code = :code";
            params.put("code", langCode);
        }
        List<Language> lst = Utils.safe(findAll(sql, null, params, 0));
        HashMap<String, Language> ret = new HashMap<>();
        for (Language lang : lst) {
            ret.put(lang.getCode(), lang);
        }
        return ret;

    }

    @Override
    public void executeSql(String sql, HashMap<String, Object> param) {
        Query query = entityManager.createNativeQuery(sql);
        if (param != null && param.size() > 0) {
            for (Map.Entry<String, Object> entry : param.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        query.executeUpdate();

    }

    @Override
    public void executeSql(String sql, String paramName, Object param) {
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(paramName, param);
        query.executeUpdate();
    }

    @Override
    public void executeSql(String sql, Object... params) {
        Query query = entityManager.createNativeQuery(sql);
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                query.setParameter(i + 1, params[i]);
            }
        }
        query.executeUpdate();
    }

    @Override
    public Object executeSqlForResults(String sql, HashMap<String, Object> param) {
        Query query = entityManager.createNativeQuery(sql);
        if (param != null && param.size() > 0) {
            for (Map.Entry<String, Object> entry : param.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.getSingleResult();
    }

    @Override
    public <T> T findOne(Class<T> clazz, String sql, Object... params) throws Exception {
        sql = convertNaming2Quest(sql, null, null);
        List<Object> lstparams = new ArrayList<>();
        if (params != null) {
            for (Object obj : params) {
                lstparams.add(obj);
            }
        }
        HashMap<String, String> columnsMap = new HashMap<>();
        ResultSet rs = getResultSet(sql, lstparams, columnsMap);
        if (rs.next()) {
            return findOne(clazz, rs, columnsMap);
        }
        return null;

    }

    @Override
    public <T> T findOne(Class<T> clazz, String sql, HashMap params) throws Exception {

        List<Object> paramsOut = new ArrayList<>();
        sql = convertNaming2Quest(sql, params, paramsOut);
        HashMap<String, String> columnsMap = new HashMap<>();
        ResultSet rs = getResultSet(sql, paramsOut, columnsMap);
        if (rs.next()) {
            return findOne(clazz, rs, columnsMap);
        }
        return null;
    }

    @Override
    public <T> T findOneV2(Class<T> clazz, String sql, Object... params) throws Exception {
        List<Object> lstParams = new ArrayList<>();
        if (params != null) {
            Collections.addAll(lstParams, params);
        }
        HashMap<String, String> columnsMap = new HashMap<>();
        ResultSet rs = getResultSet(sql, lstParams, columnsMap);
        if (rs.next()) {
            return findOne(clazz, rs, columnsMap);
        }
        return null;
    }

    @Override
    public <T> T findOneV2(Class<T> clazz, String sql, HashMap params) throws Exception {
        List<Object> lstParams = new ArrayList<>();
        HashMap<String, String> columnsMap = new HashMap<>();
        ResultSet rs = getResultSet(sql, lstParams, columnsMap);
        if (rs.next()) {
            return findOne(clazz, rs, columnsMap);
        }
        return null;
    }


}
