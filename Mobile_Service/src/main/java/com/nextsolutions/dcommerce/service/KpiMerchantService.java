package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.kpi.KpiMerchant;
import com.nextsolutions.dcommerce.shared.dto.kpi.KpiMerchantDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KpiMerchantService {
    KpiMerchant createKpiMerchant(KpiMerchant request) throws Exception;

    Page<KpiMerchant> getListKpiL1(Pageable request, KpiMerchantDTO kpiMerchantDTO) throws Exception;

    KpiMerchant getKpiMerchantById(Long id) throws Exception;

    KpiMerchant editKpiMerchant(KpiMerchant request);

    List<KpiMerchant> getKpiMerchant(Long merchantId) throws Exception;
}
