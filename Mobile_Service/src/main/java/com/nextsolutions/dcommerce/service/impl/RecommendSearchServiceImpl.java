package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.RecommendSearch;
import com.nextsolutions.dcommerce.repository.RecommendSearchRepository;
import com.nextsolutions.dcommerce.service.RecommendSearchService;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.dto.resource.RecommendSearchResource;
import com.nextsolutions.dcommerce.ui.model.request.RecommendSearchRequestModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RecommendSearchServiceImpl extends CommonServiceImpl implements RecommendSearchService {

    private final RecommendSearchRepository recommendSearchRepository;

    @Override
    public Page<RecommendSearchResource> findByQuery(RecommendSearchResource resource, Pageable pageable) throws Exception {
        return recommendSearchRepository.findByQuery(resource,pageable);
    }

    @Override
    @Transactional
    public void createRecommendSearch(RecommendSearchRequestModel request) {
        RecommendSearch recommendSearch = new RecommendSearch();
        recommendSearch.setRecommendId(request.getRecommendId());
        recommendSearch.setFromDate(request.getFromDate());
        recommendSearch.setToDate(request.getToDate());
        recommendSearch.setType(request.getType());
        recommendSearch.setStatus(1);
        recommendSearchRepository.save(recommendSearch);
    }

    @Override
    public void inactive(Long id) {
        recommendSearchRepository.inactive(id);
    }

    @Override
    public RecommendSearchResource getRecommendSearch(Long id) throws Exception {
        return recommendSearchRepository.getRecommendSearch(id);
    }

    @Override
    public List<RecommendSearchResource> getRecommendSearch(Long merchantId, Long langId) throws Exception {
        if (merchantId == null || langId == null) {
            throw new CustomErrorException(CustomErrorException.USER_ERROR, "merchantId or langId is null");
        }else {
            HashMap<String, Object> param = new HashMap<>();
            param.put("langId", langId);
            param.put("merchantId", merchantId);
            String sqlGetProduct = "SELECT pp.PACKING_PRODUCT_ID as RECOMMEND_ID, pd.PRODUCT_NAME as NAME, pp.PACKING_URL as imageUrl, pd.PRODUCT_ID ,1 as TYPE FROM " +
                    " product_description pd " +
                    " JOIN " +
                    " packing_product pp ON pp.product_id = pd.product_id " +
                    " WHERE pd.LANGUAGE_ID =:langId AND pp.PACKING_PRODUCT_ID IN (SELECT DISTINCT " +
                    " (rs.recommend_id) " +
                    " FROM " +
                    " recommend_search rs " +
                    " JOIN recommend_search_group rsg ON rs.id = rsg.recommend_search_id and rs.type = 1  " +
                    " JOIN merchant_group_merchant mgm ON mgm.MERCHANT_GROUP_ID = rsg.MERCHANT_GROUP_ID and mgm.MERCHANT_ID =:merchantId" +
                    " where rs.from_date <= DATE(CURRENT_DATE()) and (rs.to_date >= DATE(CURRENT_DATE()) or rs.TO_DATE is null) ); ";

            String sqlGetCategories = "SELECT c.name, c.url_image as imageUrl, c.CATEGORY_ID as RECOMMEND_ID, 2 as TYPE" +
                    " FROM category c " +
                    " JOIN category_description cd ON c.CATEGORY_ID = cd.CATEGORY_ID " +
                    " AND cd.LANGUAGE_ID = 1 " +
                    " WHERE c.CATEGORY_ID IN (SELECT DISTINCT " +
                    " (rs.recommend_id) " +
                    " FROM " +
                    " recommend_search rs " +
                    " JOIN recommend_search_group rsg ON rs.recommend_id = rsg.recommend_search_id and rs.type = 2" +
                    " JOIN merchant_group_merchant mgm ON mgm.MERCHANT_GROUP_ID = rsg.MERCHANT_GROUP_ID and mgm.MERCHANT_ID = 13350 " +
                    " where rs.from_date <= DATE(CURRENT_DATE()) and (rs.to_date >= DATE(CURRENT_DATE()) or rs.TO_DATE is null) ); ";

            List<RecommendSearchResource> recommendSearch = findAll(null, RecommendSearchResource.class, sqlGetProduct, param);
            recommendSearch.addAll(findAll(null, RecommendSearchResource.class, sqlGetCategories, param));
            return  recommendSearch;
        }
    }

    @Override
    public RecommendSearch updateRecommendSearch(Long id, RecommendSearchRequestModel request) {
        RecommendSearch recommendSearch = recommendSearchRepository.findById(id).orElseThrow(() -> createEntityNotFoundException(id));
        recommendSearch.setType(request.getType());
        recommendSearch.setFromDate(request.getFromDate());
        recommendSearch.setToDate(request.getToDate());
        recommendSearch.setRecommendId(request.getRecommendId());
        recommendSearchRepository.save(recommendSearch);
        return recommendSearch;
    }

    private EntityNotFoundException createEntityNotFoundException(Long id) {
        return new EntityNotFoundException("Hunter with id " + id + " doesn't exist.");
    }

}
