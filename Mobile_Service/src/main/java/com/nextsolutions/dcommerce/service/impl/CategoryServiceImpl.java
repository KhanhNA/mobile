package com.nextsolutions.dcommerce.service.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.*;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.AutoIncrement;
import com.nextsolutions.dcommerce.repository.AutoIncrementJpaRepository;
import com.nextsolutions.dcommerce.repository.CategoryDescriptionRepository;
import com.nextsolutions.dcommerce.repository.CategoryRepository;
import com.nextsolutions.dcommerce.shared.constant.AutoIncrementType;
import com.nextsolutions.dcommerce.shared.dto.CategoryDescriptionDto;
import com.nextsolutions.dcommerce.shared.projection.CategoryProductProjection;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nextsolutions.dcommerce.model.Category;
import com.nextsolutions.dcommerce.model.CategoryDescription;
import com.nextsolutions.dcommerce.service.CategoryService;
import com.nextsolutions.dcommerce.shared.dto.CategoryDto;

import lombok.RequiredArgsConstructor;
import org.springframework.util.MimeTypeUtils;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDescriptionRepository categoryDescriptionRepository;
    private final CategoryRepository categoryRepository;
    private final AppConfiguration appConfiguration;
    private final AutoIncrementJpaRepository autoIncrementJpaRepository;

    @Autowired
    private final CommonServiceImpl commonService;
    private final static String PARENT_PATH = "/images/categories";

    @Override
    public Page<CategoryDescription> findAll(Pageable pageable) {
        return this.categoryDescriptionRepository.findAll(pageable);
    }

    @Override
    public CategoryDescription get(Long id) {
        return this.categoryDescriptionRepository.findById(id).orElse(null);
    }

    @Override
    public void save(CategoryDescription object) {
        this.categoryDescriptionRepository.save(object);
    }

    @Override
    public void delete(Long id) {
        this.categoryDescriptionRepository.deleteById(id);
    }

    @Override
    public ResponseEntity<Object> getCategoryTreeFormat(Long id, Integer langId) {
        return new ResponseEntity<>(this.categoryRepository.getCategoryTreeFormat(id, langId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> getAllRoot(Integer langId) {
        List<CategoryDto> list = new ArrayList<>();
        List<Category> roots = this.categoryRepository.getAllRoot(langId);
        for (int i = 0; i < roots.size(); i++) {
            Category category = roots.get(i);
            CategoryDto categoryDto = new CategoryDto(category);
            String xLevel = "0." + (i + 1);
            categoryDto.setTreeStruct(xLevel);
            list.add(categoryDto);
            List<CategoryDto> tempCategories = this.categoryRepository.getCategoryTreeFormat(category.getId(), langId);
            if ((tempCategories != null) && !tempCategories.isEmpty()) {
                list.addAll(this.recursive(tempCategories, xLevel, langId));
            }
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    private List<CategoryDto> recursive(List<CategoryDto> categories, String prefix, Integer langId) {
        List<CategoryDto> list = new ArrayList<>();
        for (int i = 0; i < categories.size(); i++) {
            CategoryDto categoryDto = categories.get(i);
            String xLevel = prefix + "." + i;
            categoryDto.setTreeStruct(xLevel);
            list.add(categoryDto);
            List<CategoryDto> tempCategories = this.categoryRepository.getCategoryTreeFormat(categoryDto.getId(),
                    langId);
            if ((tempCategories != null) && !tempCategories.isEmpty()) {
                list.addAll(this.recursive(tempCategories, xLevel, langId));
            }
        }
        return list;
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws CustomErrorException {
        Optional<Category> oCategory = this.categoryRepository.findById(id);
        if (!oCategory.isPresent()) {
            throw new CustomErrorException(HttpStatus.INTERNAL_SERVER_ERROR.toString(), "ID is not exists");
        }
        Category category = oCategory.get();
        CategoryDto categoryDto = new CategoryDto(category);
        return new ResponseEntity<>(categoryDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> create(Category category) throws Exception {
        category.setVisible(true);
        saveImageFile(category);
        CategoryProductProjection parent = null;
        if(category.getParent() != null && category.getParent().getId() != null) {
            parent = getLevelCategory(category.getParent().getId());
        }
        Long code = getCodeCategory(parent);
        category.setCode(code.toString());
        final Category c = this.categoryRepository.save(category);
        List<CategoryDescription> cds = category.getDescriptions();
        cds.forEach(x -> {
            x.setCategory(c);
        });
        this.categoryDescriptionRepository.saveAll(cds);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    private CategoryProductProjection getLevelCategory(Long id) {
        CategoryProductProjection category = categoryRepository.
                getByCategoryIdAndLanguageId(id, LanguageUtils.
                        getLanguageId(LocaleContextHolder.getLocale().getLanguage()));
        return category;

    }

    private Long getCodeCategory(CategoryProductProjection categoryProductProjection) throws Exception {
        if (Objects.nonNull(categoryProductProjection) && categoryProductProjection.getLevel() != null) {
            String query = "Select MAX(CAST(code AS UNSIGNED)) code from category where parent_id = :id";
            List<Category> category = commonService.findAll(null, Category.class, query, categoryProductProjection.getId());
            if (Utils.isNotEmpty(category) && Objects.nonNull(category.get(0)) && category.get(0).getCode() !=null) {
                return Long.parseLong(category.get(0).getCode()) + 1;
            } else {
                return Long.parseLong(categoryProductProjection.getCode()) * 100 + 1;
            }
        } else {
            AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.MCH.value());
            Long code = Long.parseLong(autoIncrement.getValue()) + 1;
            autoIncrement.setValue(code.toString());
            autoIncrementJpaRepository.save(autoIncrement);
            return code;
        }
    }

    private void saveImageFile(Category category) {
        String image = category.getImage();
        if (image.startsWith("data:")) {
            try {
                image = image.replaceFirst("(data:.*;base64,)", "");
                byte[] decode = Base64.getDecoder().decode(image);
                String type = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(decode));
                String subtype = MimeTypeUtils.parseMimeType(type).getSubtype();
                if (subtype.equals("x-wav")) {
                    subtype = "webp";
                }
                String filename = File.separator + UUID.randomUUID().toString().replaceAll("-", "") + "." + subtype;
                try (OutputStream out = new FileOutputStream(appConfiguration.getUploadPath() + PARENT_PATH + filename)) {
                    out.write(decode);
                    category.setImage(PARENT_PATH + filename);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public ResponseEntity<Object> update(Category category) throws CustomErrorException {
        category.setVisible(true);
        saveImageFile(category);
        final Category c = this.categoryRepository.save(category);
        List<CategoryDescription> cds = category.getDescriptions();
        cds.forEach(x -> {
            x.setCategory(c);
        });
        this.categoryDescriptionRepository.saveAll(cds);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> activeDeactive(Long id, Boolean visible) throws CustomErrorException {
        Optional<Category> oCategory = this.categoryRepository.findById(id);
        if (!oCategory.isPresent()) {
            throw new CustomErrorException(HttpStatus.INTERNAL_SERVER_ERROR.toString(), "ID is not exists");
        }
        Category category = oCategory.get();
        category.setVisible(visible);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public CategoryDescriptionDto findByCategoryIdAndLanguageId(Long categoryId, Long languageId) {
        return categoryDescriptionRepository.findByCategoryIdAndLanguageId(categoryId, languageId)
                .map(CategoryDescriptionDto::new).orElse(null);
    }

    @Override
    public Page<CategoryDto> getListCategoryByLevel(Pageable pageable, Long level, Long code) throws Exception {
        return categoryRepository.getListCategoryByLevel(pageable, level, code);
    }

}
