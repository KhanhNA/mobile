package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.MerchantAddress;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MerchantAddressService extends BaseService<MerchantAddress, Long> {


    Page<MerchantAddress> findAllByMerchant(Pageable pageable, Long merchantId);

    MerchantAddress findByMerchantIdAndAddress(Long merchantId, String address);

    void deleteMerchantAddress(MerchantAddress merchantAddress );

    long countByMerchantId(Long merchantId);


}
