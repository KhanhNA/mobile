package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.DescEntityBase;
import com.nextsolutions.dcommerce.model.Language;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;

public interface CommonService {
    public enum LogCol {
        createUser("createUser"),
        createDate("createDate"),
        updateUser("updateUser"),
        updateDate("updateDate"),
        ;
        private String col;

        LogCol(String c) {
            c = col;
        }

        public String getCol() {
            return col;
        }
    }

    public enum Action {
        SELECT, INSERT, CHANGE, DELETE, ACTIVE_CODE
    }

    <T> List<T> findAll(String sql, Pageable pageable, HashMap<String, Object> params, Integer maxResult);


    Integer getRowCount(String sql, HashMap<String, Object> params);

    Integer getRowCountV2(String sql, HashMap<String, Object> params);

    Long getRowCountReturnLong(String sql, HashMap<String, Object> params);

    //tim kiem theo entity tat ca
    <T> Page<T> findAll(T param, Pageable pageable, Integer maxResult) throws Exception;

    <T> List<T> findAll(Class<T> clazz, String sql, HashMap<String, Object> params, Integer maxResult);

    //findall: tim kiem theo resultset, tham so truyen vao la cac tham so goi ham
    <T> List<T> findAll(Pageable pageable, Class<T> clazz, String sql, Object... params) throws Exception;

    //findall: tim kiem theo resultset, tham so truyen vao la 1 list
    <T> List<T> findAll(Pageable pageable, Class<T> clazz, String sql, List<Object> params) throws Exception;

    //findall: tim kiem cau lenh sql viet theo kieu naming, tham so truyen vao la hashmap
    <T> List<T> findAll(Pageable pageable, Class<T> clazz, String sql, HashMap params) throws Exception;

    //getResult: dau ra la resultset va bang ax cot: columns
    ResultSet getResultSet(String sql, List<Object> params, HashMap columnsMap) throws Exception;

    //findAll: tim kiem tham so truyen vao la Resultset
    <T> List<T> findAll(Pageable pageable, Class<T> clazz, ResultSet rs, HashMap columnsMap) throws Exception;

    //tim kiem count phan tu tiep theo
    <T> List<T> findNext(Class<T> clazz, ResultSet rs, HashMap columnsMap, Integer count) throws Exception;

    Long getSequence(String s);

    <T> T save(T entity);

    <Dto, T> Dto save(Dto dto, Class<T> clazz, Action type) throws Exception;

    <T> T find(Class<T> entityClass, Object primaryKey);

    <T> List<T> findByNative(Pageable pageable, Class<T> clazz, String sql, HashMap<String, Object> params);

    <T> HashMap<String, DescEntityBase> getDescFromIds(Class<T> clazz, Integer langId, List<Long> packingIds) throws Exception;

    String validateData(Object dto, Action actionType);

    void remove(Object entity);

    HashMap<String, Language> loadLanguages(String langCode) throws Exception;

    void executeSql(String sql, HashMap<String, Object> param);


    void executeSql(String sql, String paramName, Object param);

    void executeSql(String sql, Object... params);

    Object executeSqlForResults(String sql, HashMap<String, Object> param);

    <T> T findOne(Class<T> clazz, String sql, Object... params) throws Exception;

    <T> T findOne(Class<T> clazz, String sql, HashMap params) throws Exception;

    <T> T findOneV2(Class<T> clazz, String sql, Object... params) throws Exception;

    <T> T findOneV2(Class<T> clazz, String sql, HashMap params) throws Exception;


}
