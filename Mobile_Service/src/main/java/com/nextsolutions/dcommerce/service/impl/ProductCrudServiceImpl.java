package com.nextsolutions.dcommerce.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.util.JsonMapper;
import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.configuration.properties.OAuthConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.repository.AutoIncrementJpaRepository;
import com.nextsolutions.dcommerce.repository.PackingProductRepository;
import com.nextsolutions.dcommerce.repository.ProductImageRepository;
import com.nextsolutions.dcommerce.repository.ProductRepository;
import com.nextsolutions.dcommerce.repository.custom.AttributeRepositoryCustomImpl;
import com.nextsolutions.dcommerce.service.ProductCrudService;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.constant.AutoIncrementType;
import com.nextsolutions.dcommerce.shared.constant.PriceType;
import com.nextsolutions.dcommerce.shared.dto.AppUnitDto;
import com.nextsolutions.dcommerce.shared.dto.CurrencyDTO;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDto;
import com.nextsolutions.dcommerce.shared.dto.composition.AttributeComposition;
import com.nextsolutions.dcommerce.shared.dto.form.DistributorForm;
import com.nextsolutions.dcommerce.shared.dto.product.*;
import com.nextsolutions.dcommerce.shared.dto.resource.*;
import com.nextsolutions.dcommerce.shared.mapper.PackingPriceMapper;
import com.nextsolutions.dcommerce.shared.mapper.ProductMapper;
import com.nextsolutions.dcommerce.shared.projection.PackingProductProjection;
import com.nextsolutions.dcommerce.ui.model.request.CategoryGroupPriceRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.PackingProductRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductBasicInfoRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductImagesRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductBasicInfoResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductFormResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductImagesResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductPackingResponseModel;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.UnitUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductCrudServiceImpl implements ProductCrudService {

    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final ProductImageRepository productImageRepository;
    private final PackingProductRepository packingProductRepository;
    private final AppConfiguration appConfiguration;
    private final AutoIncrementJpaRepository autoIncrementJpaRepository;
    private final OAuthConfiguration oAuthConfiguration;
    private final PackingPriceMapper packingPriceMapper;
    @PersistenceContext
    private EntityManager em;

    private final CommonServiceImpl commonService;

    private static final String PRODUCT_IMAGES_FOLDER = "images/products";
    private static final String PACKING_PRODUCT_DESCRIPTION_IMAGES_FOLDER = "images/products/descriptions";
    private static final String PACKING_PRODUCT_IMAGES_FOLDER = "images/products/packings";
    private HashMap<String, Object> params;

    @Override
    @Transactional
    public ProductBasicInfoResponseModel saveProductBasicInfo(ProductBasicInfoRequestModel basicInfo) {
        Product product;
        if (basicInfo.getId() != null) {
            product = productRepository.findById(basicInfo.getId()).orElseThrow(() -> new EntityNotFoundException(
                    String.format("Product with id %s not found", basicInfo.getId())));
            packingProductRepository.findAllByProductId(product.getId()).forEach(packingProduct -> {
                packingProduct.setWaningThreshold(basicInfo.getWarningThreshold());
                packingProduct.setLifecycle(basicInfo.getLifecycle());
                packingProductRepository.save(packingProduct);
            });
        } else {
            AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.SKUs.value());
            Long skus = Long.parseLong(autoIncrement.getValue()) + 1;
            product = new Product();
            product.setSku(skus.toString());
            autoIncrement.setValue(skus.toString());
            autoIncrementJpaRepository.save(autoIncrement);
        }
        product.setCategoryId(basicInfo.getCategoryId());
        product.setManufacturerId(basicInfo.getManufacturerId());
        product.setProductTypeId(basicInfo.getProductTypeId());
        product.setBrand(basicInfo.getBrand());
        product.setColor(basicInfo.getColor());
        product.setOrigin(basicInfo.getOrigin());
        product.setImportPrice(basicInfo.getImportPrice());
        product.setNotes(basicInfo.getNotes());
        product.setVat(basicInfo.getVat());
        product.setReturning(basicInfo.isReturning());
        product.setCode(basicInfo.getCode());
        product.setName(basicInfo.getName());
        product.setWidth(basicInfo.getWidth());
        product.setHeight(basicInfo.getHeight());
        product.setWeight(basicInfo.getWeight());
        product.setLength(basicInfo.getLength());
        product.setIsSynchronization(false);
        product.setStatus(1);
        //TODO: Fix NPP
        product.setDistributorId(oAuthConfiguration.getDistributorId());
        product.setDistributorCode(oAuthConfiguration.getDistributorCode());
        switch (basicInfo.getLengthClass()) {
            case "m":
                product.setCalculatedWidth(UnitUtils.convertMetersToMillimeters(product.getWidth()));
                product.setCalculatedHeight(UnitUtils.convertMetersToMillimeters(product.getHeight()));
                product.setCalculatedLength(UnitUtils.convertMetersToMillimeters(product.getLength()));
                break;
            case "cm":
                product.setCalculatedWidth(UnitUtils.convertCentimetersToMillimeters(product.getWidth()));
                product.setCalculatedHeight(UnitUtils.convertCentimetersToMillimeters(product.getHeight()));
                product.setCalculatedLength(UnitUtils.convertCentimetersToMillimeters(product.getLength()));
                break;
            default:
                product.setCalculatedWidth(product.getWidth());
                product.setCalculatedHeight(product.getHeight());
                product.setCalculatedLength(product.getLength());
                break;
        }

        switch (basicInfo.getWeightClass()) {
            case "kg":
                product.setCalculatedWeight(UnitUtils.convertKilogramsToGrams(product.getWeight()));
                break;
            case "mg":
                product.setCalculatedWeight(UnitUtils.convertMilligramsToGrams(product.getWeight()));
                break;
            case "lb":
                product.setCalculatedWeight(UnitUtils.convertPoundsToGrams(product.getWeight()));
                break;
            case "oz":
                product.setCalculatedWeight(UnitUtils.convertOuncesToGrams(product.getWeight()));
                break;
            default:
                product.setCalculatedWeight(product.getWeight());
                break;
        }

        product.setWeightClass(basicInfo.getWeightClass());
        product.setLengthClass(basicInfo.getLengthClass());
        product.setWarningThreshold(basicInfo.getWarningThreshold());
        product.setLifecycle(basicInfo.getLifecycle());

        productRepository.save(product);

        List<ProductAttribute> productAttributes = product.getProductAttributes();
        if (Objects.isNull(productAttributes) || productAttributes.isEmpty()) {
            product.setProductAttributes(basicInfo.getProductAttributes().stream().map(productAttributeDto -> {
                ProductAttribute productAttribute = new ProductAttribute();
                productAttribute.setProductId(product.getId());
                productAttribute.setProductOptionId(productAttributeDto.getProductOptionId());
                productAttribute.setProductOptionValueId(productAttributeDto.getProductOptionValueId());
                return productAttribute;
            }).collect(Collectors.toList()));
        } else {
            Map<Long, ProductAttributeDto> productAttributeMap = new LinkedHashMap<>();
            List<ProductAttribute> addingProductAttributes = new LinkedList<>();
            basicInfo.getProductAttributes().parallelStream().forEach(productAttributeDto -> {
                if (productAttributeDto.getId() == null) {
                    ProductAttribute productAttribute = new ProductAttribute();
                    productAttribute.setProductId(product.getId());
                    productAttribute.setProductOptionId(productAttributeDto.getProductOptionId());
                    productAttribute.setProductOptionValueId(productAttributeDto.getProductOptionValueId());
                    addingProductAttributes.add(productAttribute);
                } else {
                    productAttributeMap.put(productAttributeDto.getId(), productAttributeDto);
                }
            });
            Iterator<ProductAttribute> productAttributeIterator = productAttributes.iterator();
            while (productAttributeIterator.hasNext()) {
                ProductAttribute productAttribute = productAttributeIterator.next();
                ProductAttributeDto productAttributeDto = productAttributeMap.get(productAttribute.getId());
                if (productAttributeDto == null) {
                    productAttributeIterator.remove();
                } else {
                    productAttribute.setProductOptionId(productAttributeDto.getProductOptionId());
                    productAttribute.setProductOptionValueId(productAttributeDto.getProductOptionValueId());
                    productAttributeMap.remove(productAttribute.getId());
                }
            }
            productAttributes.addAll(addingProductAttributes);
            productAttributeMap.values().forEach(productAttributeDto -> {
                ProductAttribute productAttribute = new ProductAttribute();
                productAttribute.setProductId(product.getId());
                productAttribute.setProductOptionId(productAttributeDto.getProductOptionId());
                productAttribute.setProductOptionValueId(productAttributeDto.getProductOptionValueId());
                productAttributes.add(productAttribute);
            });
        }

        List<ProductDescription> productDescriptions = product.getProductDescriptions();
        if (Objects.isNull(productDescriptions) || productDescriptions.isEmpty()) {
            product.setProductDescriptions(basicInfo.getProductDescriptions().stream().map(productDescriptionDto -> {
                ProductDescription productDescription = new ProductDescription();
                productDescription.setLanguageId(productDescriptionDto.getLanguageId());
                productDescription.setProductName(productDescriptionDto.getProductName());
                productDescription.setTitle(productDescriptionDto.getTitle());
                productDescription.setProductId(product.getId());
                productDescription.setDescription(productDescriptionDto.getDescription());
                return productDescription;
            }).collect(Collectors.toList()));
        } else {
            Map<Long, ProductDescriptionDto> productDescriptionMapLanguage = basicInfo.getProductDescriptions().stream()
                    .collect(Collectors.toMap(ProductDescriptionDto::getLanguageId,
                            productDescriptionDto -> productDescriptionDto));
            productDescriptions.forEach(productDescription -> {
                ProductDescriptionDto productDescriptionDto = productDescriptionMapLanguage
                        .get(productDescription.getLanguageId());
                productDescription.setLanguageId(productDescriptionDto.getLanguageId());
                productDescription.setProductName(productDescriptionDto.getProductName());
                productDescription.setTitle(productDescriptionDto.getTitle());
                productDescription.setDescription(productDescriptionDto.getDescription());
            });
        }
        productRepository.save(product);
        return productMapper.productToProductBasicInfoResponse(product);
    }

    @Override
    public ProductFormResponseModel getProductForm(Long id, Long categoryId, Long languageId) {
        return this.productRepository.findByIdAndCategoryIdAndLanguageId(id, categoryId, languageId);
    }

    private void setProductImagesProperty(MultipartFile[] multipartFiles, List<ProductImageDto> productImagesDto,
                                          Product product) throws IOException {
        // TODO: Function should do one thing?
        List<ProductImage> productImages = product.getProductImages();
        if (Objects.isNull(productImages)) {
            this.setProductImagesPropertyForCreatingProduct(multipartFiles, productImagesDto, product);
        } else {
            if (Objects.nonNull(productImagesDto)) {
                // TODO: refactor it later
                for (ProductImageDto productImageDto : productImagesDto) {
                    this.setImageUrl(multipartFiles, productImageDto);
                }

                this.removeProductImageNonMatchesFromProductImages(productImagesDto, productImages);

                for (ProductImageDto productImageDto : productImagesDto) {
                    if (Objects.isNull(productImageDto.getId())) {
                        this.addProductImageToProductImages(product, productImages, productImageDto);
                    } else {
                        this.setChangedProductImages(product, productImages, productImageDto);
                    }
                }

                product.setProductImages(productImages);
            } else {
                // TODO: remove all product images
                this.productImageRepository.deleteAll(productImages);
                product.getProductImages().clear();
            }
        }
    }

    private void removeProductImageNonMatchesFromProductImages(List<ProductImageDto> productImagesDto,
                                                               List<ProductImage> productImages) {
        List<ProductImage> productImageNonMatches = this.getProductImageNonMatches(productImagesDto, productImages);
        this.deleteProductImageIfExists(productImages, productImageNonMatches);
    }

    private List<ProductImage> getProductImageNonMatches(List<ProductImageDto> productImagesDto,
                                                         List<ProductImage> productImages) {
        return productImages.stream().filter(
                productImage -> productImagesDto.stream().noneMatch(x -> productImage.getId().equals(x.getId())))
                .collect(Collectors.toList());
    }

    private void deleteProductImageIfExists(List<ProductImage> productImages,
                                            List<ProductImage> productImageNonMatches) {
        if (productImages.removeAll(productImageNonMatches)) {
            this.productImageRepository.deleteAll(productImageNonMatches);
            this.deleteProductImageFiles(productImageNonMatches);
        }
    }

    private void deleteProductImageFiles(List<ProductImage> nonMatches) {
        nonMatches.forEach(productImage -> {
            try {
                String productImageUrl = productImage.getProductImageUrl();
                String filePath = productImageUrl.replaceAll("(/)(.*)", this.appConfiguration.getUploadPath() + "$2");
                Files.deleteIfExists(Paths.get(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void addProductImageToProductImages(Product product, List<ProductImage> productImages,
                                                ProductImageDto productImageDto) {
        ProductImage productImage = new ProductImage();
        this.setProductImageProperties(product, productImageDto, productImage);
        productImages.add(productImage);
    }

    private void setChangedProductImages(Product product, List<ProductImage> productImages,
                                         ProductImageDto productImageDto) {
        for (ProductImage productImage : productImages) {
            if (productImage.getId().equals(productImageDto.getId())) {
                this.setProductImageProperties(product, productImageDto, productImage);
            }
        }
    }

    private void setProductImagesPropertyForCreatingProduct(MultipartFile[] multipartFiles,
                                                            List<ProductImageDto> productImagesDto, Product product) throws IOException {
        List<ProductImage> productImages = new ArrayList<>();
        if (Objects.nonNull(multipartFiles) && Objects.nonNull(productImagesDto)) {
            for (int i = 0; i < multipartFiles.length; i++) {
                String filename = this.generateFilename(multipartFiles[i]);
                this.transferImageFile(multipartFiles[i], PRODUCT_IMAGES_FOLDER, filename);

                ProductImageDto productImageDto = productImagesDto.get(i);
                ProductImage productImage = new ProductImage();
                String imageUrl = this.generateImageUrl(PRODUCT_IMAGES_FOLDER, filename);
                productImageDto.setImageUrl(imageUrl);
                this.setProductImageProperties(product, productImageDto, productImage);
                productImages.add(productImage);
            }
        }
        product.setProductImages(productImages);
    }

    private void setProductImageProperties(Product product, ProductImageDto productImageDto,
                                           ProductImage productImage) {
        if (Objects.nonNull(productImageDto.getId())) {
            productImage.setId(productImageDto.getId());
        }
        productImage.setImageCrop(productImageDto.getCrop());
        productImage.setImageType(productImageDto.getType());
        productImage.setDefaultImage(productImageDto.isDefaultImage());
        productImage.setProductImageUrl(productImageDto.getImageUrl());
        if (productImageDto.isDefaultImage()) {
            product.setImageDefaultUrl(productImageDto.getImageUrl());
        }
        productImage.setProduct(product);
        this.addProductImageDescriptions(productImageDto, productImage);
    }

    private void addProductImageDescriptions(ProductImageDto productImageDto, ProductImage productImage) {
        List<ProductImageDescription> productImageDescriptions = productImage.getImageDescriptions();
        List<ProductImageDescriptionDto> productImageDtoDescriptions = productImageDto.getDescriptions();
        if (Objects.isNull(productImageDescriptions)) {
            this.createProductImageDescriptionsForCreatingProduct(productImage, productImageDtoDescriptions);
        } else {
            this.setProductImageDescriptionsForUpdatingProductImage(productImage, productImageDescriptions,
                    productImageDtoDescriptions);
        }
    }

    private void setChangedProductImageDescriptions(ProductImage productImage,
                                                    List<ProductImageDescriptionDto> productImageDtoDescriptions,
                                                    ProductImageDescription productImageDescription) {
        productImageDtoDescriptions.forEach(productImageDescriptionDto -> this
                .setChangedProductImageDescription(productImage, productImageDescription, productImageDescriptionDto));
    }

    private void setProductImageDescriptionsForUpdatingProductImage(ProductImage productImage,
                                                                    List<ProductImageDescription> productImageDescriptions,
                                                                    List<ProductImageDescriptionDto> productImageDtoDescriptions) {
        if (Objects.nonNull(productImageDtoDescriptions)) {
            productImageDescriptions.forEach(productImageDescription -> {
                this.setChangedProductImageDescriptions(productImage, productImageDtoDescriptions,
                        productImageDescription);
            });
        } else {
            // TODO: delete product descriptions later
        }
    }

    private void setChangedProductImageDescription(ProductImage productImage,
                                                   ProductImageDescription productImageDescription, ProductImageDescriptionDto productImageDescriptionDto) {
        if (productImageDescription.getId().equals(productImageDescriptionDto.getId())) {
            productImageDescription.setTitle(productImageDescriptionDto.getTitle());
            productImageDescription.setName(productImageDescriptionDto.getName());
            productImageDescription.setDescription(productImageDescriptionDto.getDescription());
            productImageDescription.setLanguageId(productImageDescriptionDto.getLanguageId());
            productImageDescription.setProductImage(productImage);
        }
    }

    private void createProductImageDescriptionsForCreatingProduct(ProductImage productImage,
                                                                  List<ProductImageDescriptionDto> productImageDtoDescriptions) {
        List<ProductImageDescription> productImageDescriptions = this.mapToProductImageDescription(productImage,
                productImageDtoDescriptions);
        productImage.setImageDescriptions(productImageDescriptions);
    }

    private List<ProductImageDescription> mapToProductImageDescription(ProductImage productImage,
                                                                       List<ProductImageDescriptionDto> productImageDtoDescriptions) {
        return productImageDtoDescriptions.stream().map(descriptionDto -> {
            ProductImageDescription productImageDescription = new ProductImageDescription();
            productImageDescription.setTitle(descriptionDto.getTitle());
            productImageDescription.setName(descriptionDto.getName());
            productImageDescription.setDescription(descriptionDto.getDescription());
            productImageDescription.setLanguageId(descriptionDto.getLanguageId());
            productImageDescription.setProductImage(productImage);
            return productImageDescription;
        }).collect(Collectors.toList());
    }

    private void setImageUrl(MultipartFile[] multipartFiles, ProductImageDto productImageDto) throws IOException {
        for (MultipartFile multipartFile : multipartFiles) {
            String originalFilename = multipartFile.getOriginalFilename();
            if (Objects.nonNull(originalFilename) && originalFilename.equals(productImageDto.getName())) {
                String filename = this.generateFilename(multipartFile);
                this.transferImageFile(multipartFile, PRODUCT_IMAGES_FOLDER, filename);

                String imageUrl = this.generateImageUrl(PRODUCT_IMAGES_FOLDER, filename);
                productImageDto.setImageUrl(imageUrl);
            }
        }
    }

    private void setProductDescriptionsProperty(ProductFormDto productForm, Product product) {
        List<ProductDescriptionDto> productDescriptionDtos = productForm.getProductDescriptions();
        List<ProductDescription> productDescriptions = productDescriptionDtos.stream().map(productDescriptionDto -> {
            ProductDescription productDescription = new ProductDescription();
            if (Objects.nonNull(productDescriptionDto.getId())) {
                productDescription.setId(productDescriptionDto.getId());
            }
            productDescription.setProduct(product);
            productDescription.setTitle(productDescriptionDto.getTitle());
            productDescription.setProductName(productDescriptionDto.getProductName());
            productDescription.setDescription(productDescriptionDto.getDescription());
            productDescription.setLanguageId(productDescriptionDto.getLanguageId());

            return productDescription;
        }).collect(Collectors.toList());
        product.setProductDescriptions(productDescriptions);
    }

    private String generateImageUrl(String path, String filename) {
        return String.format("/%s/%s", path, filename);
    }

    private void transferImageFile(MultipartFile multipartFile, String parent, String filename) throws IOException {
        File imageFile = new File(String.format("%s/%s/%s", this.appConfiguration.getUploadPath(), parent, filename));
        FileUtils.forceMkdirParent(imageFile);
        multipartFile.transferTo(imageFile);
    }

    private String generateFilename(MultipartFile multipartFile) {
        String name = UUID.randomUUID().toString().replaceAll("-", "");
        String originalFilename = multipartFile.getOriginalFilename();
        if (Objects.nonNull(originalFilename)) {
            String type = originalFilename.replaceAll("(.*)(\\..*)", "$2");
            return name + type;
        }
        return "";
    }

    @Override
    @Transactional
    public PackingProductRequestModel savePackingProducts(Long id, PackingProductRequestModel productRequest) {
        Product product = this.productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Product with id %s not found", id)));
        List<PackingProduct> packingProducts = product.getPackingProducts();
        List<PackingProductDto> packingProductsDto = productRequest.getPackingProducts();
        if (packingProducts == null) {
            // todo: create
            product.setPackingProducts(generatePackingProducts(packingProductsDto));
            this.productRepository.save(product);
        } else {
            // todo: update
            if (packingProductsDto == null || packingProductsDto.isEmpty()) {
                // todo: clear all
                packingProductRepository.deleteAll(packingProducts);
                packingProducts.clear();
            } else {
                // todo: remove non matches
                Iterator<PackingProduct> packingProductIterator = packingProducts.iterator();
                while (packingProductIterator.hasNext()) {
                    PackingProduct packingProduct = packingProductIterator.next();
                    boolean noneMatch = packingProductsDto.stream().noneMatch(packingProductDto -> packingProduct
                            .getPackingProductId().equals(packingProductDto.getId()));
                    if (noneMatch) {
                        packingProductIterator.remove();
                        packingProduct.setIsSynchronization(false);
                        packingProductRepository.inactive(packingProduct.getPackingProductId());
                    }
                }
                Map<Long, PackingProduct> packingProductMap = packingProducts.stream().collect(
                        Collectors.toMap(PackingProduct::getPackingProductId, packingProduct -> packingProduct));
                packingProductsDto.forEach(packingProductDto -> {
                    Long packingProductDtoId = packingProductDto.getId();
                    if (packingProductDtoId == null) {
                        // todo: create new
                        PackingProduct packing = generatePackingProduct(packingProductDto);
                        packingProductRepository.save(packing);
                    } else {
                        // todo: update exists id
                        PackingProduct packingProduct = packingProductMap.get(packingProductDtoId);
                        if (packingProduct != null) {
                            setPropertiesPackingProduct(packingProductDto, packingProduct);
                            packingProductRepository.save(packingProduct);
                        }
                    }
                });
            }
        }
        return productRequest;
    }

    private List<PackingProduct> generatePackingProducts(List<PackingProductDto> packingProductsDto) {
        return packingProductsDto.stream().map(this::generatePackingProduct).collect(Collectors.toList());
    }

    private PackingProduct generatePackingProduct(PackingProductDto packingProductDto) {
        PackingProduct packingProduct = new PackingProduct();
        AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.PACK.value());
        String pack = autoIncrement.getValue().replace("P", "");
        Long code = Long.valueOf(pack) + 1;
        String value = "P" + code;
        autoIncrement.setValue(value);
        packingProduct.setCode(value);
        autoIncrementJpaRepository.save(autoIncrement);
        setPropertiesPackingProduct(packingProductDto, packingProduct);
        return packingProduct;
    }

    private void setPropertiesPackingProduct(PackingProductDto packingProductDto, PackingProduct packingProduct) {
        packingProduct.setName(packingProductDto.getName());
        packingProduct.setProductId(packingProductDto.getProductId());
        packingProduct.setPackingTypeId(packingProductDto.getPackingTypeId());
        packingProduct.setPackingUrl(packingProductDto.getImageUrl());
        packingProduct.setOrgPrice(packingProductDto.getOriginalPrice());
        packingProduct.setDiscountOriginalPrice(packingProductDto.getDiscountOriginalPrice());
        packingProduct.setDiscountPercentOriginalPrice(packingProductDto.getDiscountPercentOriginalPrice());
        packingProduct.setDiscountFromDate(packingProductDto.getDiscountFromDate());
        packingProduct.setDiscountToDate(packingProductDto.getDiscountToDate());
        packingProduct.setPrice(packingProductDto.getSalePrice());
        packingProduct.setPromotionSalePrice(packingProductDto.getPromotionSalePrice());
        packingProduct.setDiscountPercent(packingProductDto.getPromotionPercentSalePrice());
        packingProduct.setFromDate(packingProductDto.getPromotionFromDate());
        packingProduct.setToDate(packingProductDto.getPromotionToDate());
        packingProduct.setMarketPrice(packingProductDto.getMarketPrice());
        packingProduct.setWidth(packingProductDto.getWidth());
        packingProduct.setHeight(packingProductDto.getHeight());
        packingProduct.setWeight(packingProductDto.getWeight());
        packingProduct.setLength(packingProductDto.getLength());
        packingProduct.setWeightClass(packingProductDto.getWeightClass());
        packingProduct.setLengthClass(packingProductDto.getLengthClass());
        packingProduct.setUom(packingProductDto.getUom());
        packingProduct.setIsSynchronization(false);
        switch (packingProduct.getLengthClass()) {
            case "m":
                packingProduct.setCalculatedWidth(UnitUtils.convertMetersToMillimeters(packingProduct.getWidth()));
                packingProduct.setCalculatedHeight(UnitUtils.convertMetersToMillimeters(packingProduct.getHeight()));
                packingProduct.setCalculatedLength(UnitUtils.convertMetersToMillimeters(packingProduct.getLength()));
                break;
            case "cm":
                packingProduct.setCalculatedWidth(UnitUtils.convertCentimetersToMillimeters(packingProduct.getWidth()));
                packingProduct.setCalculatedHeight(UnitUtils.convertCentimetersToMillimeters(packingProduct.getHeight()));
                packingProduct.setCalculatedLength(UnitUtils.convertCentimetersToMillimeters(packingProduct.getLength()));
                break;
            default:
                packingProduct.setCalculatedWidth(packingProduct.getWidth());
                packingProduct.setCalculatedHeight(packingProduct.getHeight());
                packingProduct.setCalculatedLength(packingProduct.getLength());
                break;
        }

        switch (packingProduct.getWeightClass()) {
            case "kg":
                packingProduct.setCalculatedWeight(UnitUtils.convertKilogramsToGrams(packingProduct.getWeight()));
                break;
            case "mg":
                packingProduct.setCalculatedWeight(UnitUtils.convertMilligramsToGrams(packingProduct.getWeight()));
                break;
            case "lb":
                packingProduct.setCalculatedWeight(UnitUtils.convertPoundsToGrams(packingProduct.getWeight()));
                break;
            case "oz":
                packingProduct.setCalculatedWeight(UnitUtils.convertOuncesToGrams(packingProduct.getWeight()));
                break;
            default:
                packingProduct.setCalculatedWeight(packingProduct.getWeight());
                break;
        }
        //TODO: FIX NPP
        packingProduct.setDistributorId(oAuthConfiguration.getDistributorId());
        packingProduct.setDistributorCode(oAuthConfiguration.getDistributorCode());
        packingProduct.setDistributorWalletId(oAuthConfiguration.getDistributorWalletId());
    }

    @Override
    public Map<String, String> savePackingImage(MultipartFile imageFile) throws IOException {
        String filename = this.generateFilename(imageFile);
        this.transferImageFile(imageFile, PACKING_PRODUCT_IMAGES_FOLDER, filename);
        return this.generateResult(PACKING_PRODUCT_IMAGES_FOLDER, filename);
    }

    private Map<String, String> generateResult(String path, String filename) {
        Map<String, String> result = new HashMap<>();
        result.put("filename", filename);
        result.put("imageUrl", this.generateImageUrl(path, filename));
        return result;
    }

    @Override
    public Page<ProductTableDto> findAll(Pageable pageable) {
        Page<Product> productPage = this.productRepository.findAll(pageable);
        List<ProductTableDto> productTableDtos = this.productMapper.toProductTableDto(productPage.getContent());
        return new PageImpl<>(productTableDtos, pageable, productPage.getTotalElements());
    }

    @Transactional
    @Override
    public void deleteProductById(Long id) {
        this.productRepository.inactive(id);
    }

    @Override
    public Page<ProductTableDto> findByQuery(ProductQueryDto query, Pageable pageable) {
        return this.productRepository.findByQuery(query, pageable);
    }

    @Override
    @Transactional
    public ProductImagesResponseModel saveProductImages(Long id, MultipartFile[] multipartFiles,
                                                        ProductImagesRequestModel productImagesRequestModel) throws IOException {
        Product product = productRepository.findById(productImagesRequestModel.getProductId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Product with id %s not found", id)));
        this.setProductImagesProperty(multipartFiles, productImagesRequestModel.getProductImages(), product);
        product.setIsSynchronization(false);
        productRepository.saveAndFlush(product);
        return productMapper.toProductImagesResponse(product);
    }

    @Override
    public boolean existsCode(String code) {
        return productRepository.existsByCode(code);
    }

    @Override
    public boolean isDuplicateCode(Long id, String code) {
        Product product = productRepository.findByCode(code).orElse(null);
        if (product == null) {
            return false;
        }
        return !product.getId().equals(id);
    }

    @Override
    public Map<String, String> saveProductDescriptionImage(MultipartFile imageFile) throws IOException {
        Map<String, String> result = new HashMap<>();
        String originalFilename = imageFile.getOriginalFilename();
        String name = UUID.randomUUID().toString().replaceAll("-", "");
        String extension = Objects.requireNonNull(originalFilename).replaceAll("(.*)(\\..*)", "$2");
        BufferedImage defaultImage = ImageIO.read(imageFile.getInputStream());
        writeImage(defaultImage, extension.substring(1), name + "-default" + extension);
        result.put("default",
                this.generateImageUrl(PACKING_PRODUCT_DESCRIPTION_IMAGES_FOLDER, name + "-default" + extension));
        int[] sizes = {64, 128, 256, 512, 1024};
        for (int size : sizes) {
            BufferedImage image64 = resize(defaultImage, size,
                    defaultImage.getHeight() * size / defaultImage.getWidth());
            String filename = name + "-" + size + extension;
            writeImage(image64, extension.substring(1), filename);
            result.put(String.valueOf(size),
                    this.generateImageUrl(PACKING_PRODUCT_DESCRIPTION_IMAGES_FOLDER, filename));
        }
        return result;
    }

    @Override
    public Page<ProductPackingResponseModel> findByCategoryIdAndPackingTypeId(Long categoryId, Long packingTypeId,
                                                                              Pageable pageable) {
        return this.productRepository.findByCategoryIdAndPackingTypeId(categoryId, packingTypeId, pageable);
    }

    @Override
    public Page<PackingPriceDto> getHistoryGroupPrice(PackingPriceDto requestModel, Pageable pageable)
            throws Exception {
        StringBuilder selectPackingPrice = new StringBuilder();
        selectPackingPrice.append(
                "Select p.packing_price_id id, p.packing_product_id packingProductId, p.price price, pp.PACKING_URL imageUrl, ");
        selectPackingPrice.append(
                " p.from_date fromDate, p.to_date toDate, p.price_type priceType, pp.code code, p.currency_id currencyId, p.value_change valueChange, ");
        selectPackingPrice
                .append(" p.currency_code currencyCode, p.latest_modified_time lastModifiedTime, pd.product_name name, p.unit_id unitId, ");
        selectPackingPrice.append(" p.status status, p.action action");
        selectPackingPrice.append(" from packing_price p join packing_product pp ");
        selectPackingPrice.append(" on pp.packing_product_id = p.packing_product_id ");
        selectPackingPrice.append(" join product pd on pd.product_id = pp.product_id where 1=1");
        String code = requestModel.getCode();
        Integer priceType = requestModel.getPriceType();
        Long action = requestModel.getAction();
        Long status = requestModel.getStatus();
        LocalDateTime fromDate = requestModel.getFromDate();
        LocalDateTime toDate = requestModel.getToDate();
        if (Objects.nonNull(code)) {
            selectPackingPrice.append(" and lower(code) like lower(concat('%','").append(code).append("','%'))");
        }

        if (priceType != null) {
            selectPackingPrice.append(" and p.price_type = :priceType");
        }

        if (action != null) {
            selectPackingPrice.append(" and p.action = :action");
        }
        if (status != null) {
            selectPackingPrice.append(" and p.status = :status");
        }
        if (fromDate != null && toDate != null) {
            selectPackingPrice.append(" and date( p.from_date ) >= date( :fromDate ) and date( p.to_date ) <= date( :toDate )");
        } else {
            if (fromDate != null) {
                selectPackingPrice.append(" and date( p.from_date ) >= date( :fromDate )");
            }
            if (toDate != null) {
                selectPackingPrice.append(" and date( p.to_date ) <= date( :toDate )");
            }
        }
        HashMap<String, Object> params = new HashMap<>();
        if (fromDate != null && toDate != null) {
            params.put("fromDate", fromDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            params.put("toDate", toDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        } else {
            if (fromDate != null) {
                params.put("fromDate", fromDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            }
            if (toDate != null) {
                params.put("toDate", toDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

            }
        }
        if (priceType != null) {
            params.put("priceType", priceType);
        }
        if (action != null) {
            params.put("action", action);
        }
        if (status != null) {
            params.put("status", status);
        }
        String query = selectPackingPrice.toString() + SpringUtils.getOrderBy(pageable);

        List<PackingPriceDto> listPackingPrice = commonService.findAll(pageable, PackingPriceDto.class, query, params);
        Integer total = commonService.getRowCountV2(String.format("select count(*) from (%s) x", query), params);
        return new PageImpl<>(listPackingPrice, pageable, total);
    }

    @Override
    public ProductGeneralResource getProductGeneralResource(Long id) {
        return productRepository.findById(id).map(productMapper::toProductGeneralResource)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public ProductDataResource getProductDataResource(Long id) {
        return productRepository.findById(id).map(productMapper::toProductDataResource)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public ProductAttributeResource getProductAttributeResource(Long id) {
        return productRepository.findById(id).map(productMapper::toProductAttributeResource)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public ProductPackageResource getProductPackageResource(Long id) {
        return packingProductRepository.findAllPackingByProductId(id);
    }

    @Override
    @Transactional
    public void updateDistributor(Long id, DistributorForm distributor) {
        productRepository.updateDistributorIdById(id, distributor.getId(), distributor.getCode());
        List<PackingProduct> packingProducts = packingProductRepository.findAllByProductId(id);
        packingProducts.forEach(packingProduct -> {
            packingProductRepository.updateDistributorById(packingProduct.getPackingProductId(), distributor.getId(),
                    distributor.getCode(), distributor.getWalletId());
        });
    }

    @Override
    public boolean isSalePriceLessThanImportPrice(Long productId, BigDecimal salePrice) throws Exception {
        StringBuilder select = new StringBuilder("Select import_price from product where product_id = :id");
        List<Product> product = commonService.findAll(null, Product.class, select.toString(), productId);
        if (salePrice.compareTo(product.get(0).getImportPrice()) == -1) {
            return true;
        } else
            return false;
    }

    @Override
    @Transactional
    public Void applyGroupPrice(CategoryGroupPriceRequestModel requestModel) throws Exception {
        List<PackingProductProjection> packingProductPrices = packingProductRepository.getPackingProductPrices(
                requestModel.getCategoryId(), requestModel.getProductIds(), requestModel.getPackingTypeId());
        if (packingProductPrices != null) {
            for (PackingProductProjection p : packingProductPrices) {
                PackingPrice packingPrice = new PackingPrice();
                packingPrice.setCurrencyCode("VND");
                packingPrice.setPackingProductId(p.getPackingProductId());
                packingPrice.setCreatedTime(LocalDateTime.now());
                packingPrice.setFromDate(requestModel.getStartDate());
                packingPrice.setToDate(requestModel.getEndDate());
                packingPrice.setLatestModifiedTime(LocalDateTime.now());
                packingPrice.setPriceType(requestModel.getType());
                packingPrice.setCurrency(null);
                packingPrice.setPackingProduct(null);
                BigDecimal priceApply;
                if (requestModel.getType().equals(2)) {
                    priceApply = BigDecimal.valueOf(BigDecimal.valueOf(p.getPrice()).doubleValue()
                            - requestModel.getAmountApply().doubleValue());
                } else if (requestModel.getType().equals(1)) {
                    priceApply = requestModel.getAmountApply();
                } else {
                    priceApply = BigDecimal
                            .valueOf(p.getPrice() * (1 - (requestModel.getAmountApply().doubleValue() / 100)));
                }
                packingPrice.setPrice(priceApply);
                commonService.save(packingPrice);
            }
        }
        return null;
    }

    @Override
    @Transactional
    public Boolean newPrice(PackingPriceDto request) {
        if (request.getPackingProductId() == null) {
            throw new CustomErrorException("PackingProductId is not null");
        }
        int count = checkPriceExist(request, null);
        if (count == 0) {
            commonService.save(PackingPrice.builder().packingProductId(request.getPackingProductId())
                    .price(request.getPrice()).fromDate(request.getFromDate()).toDate(request.getToDate())
                    .priceType(request.getPriceType()).exchangeRate(request.getExchangeRate())
                    .unitId(request.getUnitId())
                    .action(1L)
                    .status(0L)
                    .isSynchronization(true)
                    .currencyCode(request.getCurrency().getCode()).currencyId(request.getCurrency().getId()).build());
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public Boolean editPrice(PackingPriceDto request) throws JsonProcessingException {
        if (request.getPackingProductId() == null || request.getId() == null) {
            throw new CustomErrorException("PackingProductId and packingPriceId is not null");
        }
        PackingPrice packingPrice = commonService.find(PackingPrice.class, request.getId());
        if (packingPrice == null) {
            throw new CustomErrorException("PackingPrice not found");
        }
        // TODO: Nhớ lấy thời gian theo quốc gia
        int count = checkPriceExist(request, Collections.singletonList(packingPrice.getId()));
        if (count == 0) {
            ObjectMapper objectMapper = new ObjectMapper();
            String valueChange = objectMapper.writeValueAsString(request);
            packingPrice.setAction(2L);
            packingPrice.setValueChange(valueChange);
            packingPrice.setStatus(0L);
            packingPrice.setIsSynchronization(true);
            commonService.save(packingPrice);
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public Boolean approvedNew(PackingPriceDto request) {
        if (request.getPackingProductId() == null || request.getId() == null) {
            throw new CustomErrorException("PackingProductId and packingPriceId is not null");
        }
        PackingPrice packingPrice = commonService.find(PackingPrice.class, request.getId());
        if (packingPrice == null) {
            throw new CustomErrorException("PackingPrice not found");
        }
        packingPrice.setStatus(1L);
        packingPrice.setIsSynchronization(false);
        packingPrice.setAction(0L);
        commonService.save(packingPrice);
        return true;
    }

    @Override
    @Transactional
    public PackingPriceDto approvedEdit(PackingPriceDto request) {
        if (request.getPackingProductId() == null || request.getId() == null) {
            throw new CustomErrorException("PackingProductId and packingPriceId is not null");
        }
        PackingPrice packingPrice = commonService.find(PackingPrice.class, request.getId());
        if (packingPrice == null) {
            throw new CustomErrorException("PackingPrice not found");
        }
        Utils.copyNonNullProperties(request, packingPrice);
        packingPrice.setAction(0L);
        packingPrice.setStatus(1L);
        packingPrice.setIsSynchronization(false);
        commonService.save(packingPrice);
        return packingPriceMapper.toPackingPriceDto(packingPrice);
    }

    @Override
    @Transactional
    public PackingPriceDto rejectRequest(PackingPriceDto request) {
        if (request.getPackingProductId() == null || request.getId() == null) {
            throw new CustomErrorException("PackingProductId and packingPriceId is not null");
        }
        PackingPrice packingPrice = commonService.find(PackingPrice.class, request.getId());
        if (packingPrice == null) {
            throw new CustomErrorException("PackingPrice not found");
        }
        packingPrice.setStatus(-1L);
        packingPrice.setAction(0L);
        commonService.save(packingPrice);
        return packingPriceMapper.toPackingPriceDto(packingPrice);
    }

    @Override
    public List<AppUnitDto> getUnits() throws Exception {
        String select = "Select * from app_unit";
        List<AppUnitDto> result = commonService.findAll(null, AppUnitDto.class, select, new HashMap());
        return result;
    }

    @Override
    public Page<ManufacturerResource> getManufacturer(ManufacturerResource resource, Pageable pageable) {
        String selectClause = "SELECT m.MANUFACTURER_ID id, m.code code, md.name name, md.LANGUAGE_ID languageId, m.DATE_CREATED createdDate, " +
                " m.TEL tel, m.EMAIL email, md.ADDRESS address, m.status status, m.action action, m.tax_code taxCode ";
        String selectCountClause = "SELECT count(1) ";
        String fromClause = " FROM manufacturer m JOIN manufacturer_description md ON m.MANUFACTURER_ID = md.MANUFACTURER_ID ";

        StringBuilder whereClause = new StringBuilder();

        whereClause.append(" WHERE md.LANGUAGE_ID = :languageId and m.is_active != 0 and status = 1");
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());

        String nativeQueryString = selectClause + fromClause + whereClause.toString() + SpringUtils.getOrderBy(pageable);
        Query nativeQuery = em.createNativeQuery(nativeQueryString, "ManufacturerDtoMapping");
        nativeQuery.setParameter("languageId", languageId);
        nativeQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        nativeQuery.setMaxResults(pageable.getPageSize());

        List<ManufacturerResource> resultList = nativeQuery.getResultList();
        return new PageImpl<>(resultList, pageable, 1);
    }

    private int checkPriceExist(PackingPriceDto request, List<Long> idNotIn) {
        params = new HashMap<>();
        String whereNotIn = " ";
        String sqlSelect = "select * from packing_price ip where IF(:toDate IS NULL,"
                + " (IF(ip.to_date IS NULL, TRUE, date(ip.to_date) >= date(:fromDate))),"
                + " (date(ip.from_date) <= date(:toDate) and "
                + " (date(ip.to_date) >= date(:fromDate) OR ip.TO_DATE IS NULL)))"
                + " and packing_product_id = :packingProductId AND ip.price_type = :type  %s  limit 1";
        if (request.getToDate() == null) {
            params.put("toDate", null);
        } else {
            params.put("toDate", request.getToDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }
        params.put("fromDate", request.getFromDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        params.put("packingProductId", request.getPackingProductId());
        params.put("type", request.getPriceType());
        if (Utils.isNotNull(idNotIn)) {
            whereNotIn = " AND packing_price_id not in (" + Utils.join(",", idNotIn) + ") ";
        }
        return commonService.getRowCountV2(
                String.format("select count(*)  from (%s) x", String.format(sqlSelect, whereNotIn)), params);
    }

    @Override
    public List<CurrencyDTO> getCurrency() throws Exception {
        return commonService.findAll(null, CurrencyDTO.class,
                "select CURRENCY_ID id, CURRENCY_CODE code, CURRENCY_NAME name from currency", new HashMap<>());
    }

    private void writeImage(BufferedImage image, String type, String filename) throws IOException {
        File imageFile = new File(String.format("%s/%s/%s", this.appConfiguration.getUploadPath(),
                PACKING_PRODUCT_DESCRIPTION_IMAGES_FOLDER, filename));
        FileUtils.forceMkdirParent(imageFile);
        ImageIO.write(image, type, imageFile);
    }

    private static BufferedImage resize(BufferedImage image, int width, int height) {
        Image imageToScale = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.SCALE_SMOOTH);
        Graphics2D graphics2D = resized.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(imageToScale, 0, 0, null);
        graphics2D.dispose();
        return resized;
    }

    private PackingPrice getPackingPriceOfPacking(Long packingId, Double price) {
        String sql = "SELECT * FROM packing_price" + " WHERE packing_product_id = :packingId AND price = :price "
                + " AND DATE(from_date) <= DATE(CURRENT_DATE()) AND (DATE(to_date) >= DATE(CURRENT_DATE()) OR to_date IS NULL)";
        try {
            return commonService.findOne(PackingPrice.class, sql, packingId, price);
        } catch (Exception e) {
            throw new CustomErrorException(e.getMessage());
        }
    }
}
