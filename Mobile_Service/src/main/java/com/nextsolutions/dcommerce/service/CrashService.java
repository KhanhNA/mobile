package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.AndroidCrashLogDto;

public interface CrashService {
    void save(AndroidCrashLogDto dto) throws Exception;
}
