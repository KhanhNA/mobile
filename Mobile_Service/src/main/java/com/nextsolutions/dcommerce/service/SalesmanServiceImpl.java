package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.shared.dto.SalesmanDto;
import com.nextsolutions.dcommerce.shared.mapper.SalesmanMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SalesmanServiceImpl implements SalesmanService {

    private final MerchantJpaRepository merchantJpaRepository;
    private final SalesmanMapper salesmanMapper;

    @Override
    public SalesmanDto createSalesman(SalesmanDto salesmanDto) {
        salesmanMapper.toMerchant(salesmanDto);
        return null;
    }
}
