package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.model.MerchantActiveCode;
import com.nextsolutions.dcommerce.repository.MerchantActiveCodeRepository;
import com.nextsolutions.dcommerce.service.MerchantGenCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MerchantGenCodeServiceImpl implements MerchantGenCodeService {
    @Autowired
    MerchantActiveCodeRepository merchantActiveCodeRepository;

    @Override
    public Page<MerchantActiveCode> findAll(Pageable pageable) {
        return merchantActiveCodeRepository.findAll(pageable);
    }

    @Override
    public MerchantActiveCode get(Long aLong) {
        return merchantActiveCodeRepository.findById(aLong).orElse(null);
    }

    @Override
    public void save(MerchantActiveCode object) {
        merchantActiveCodeRepository.save(object);
    }

    @Override
    public void delete(Long aLong) {
        merchantActiveCodeRepository.deleteById(aLong);
    }

    @Override
    public MerchantActiveCode findByActiveCode(String code) {
        return merchantActiveCodeRepository.findByActiveCode(code);
    }
}
