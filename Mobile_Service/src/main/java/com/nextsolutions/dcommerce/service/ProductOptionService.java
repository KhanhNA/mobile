package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.ProductOptionDto;
import com.nextsolutions.dcommerce.shared.dto.ProductOptionValueDto;
import com.nextsolutions.dcommerce.ui.model.request.ProductOptionQueryRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductOptionRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductOptionValueRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionTableResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionValueTableResponseModel;

import java.util.Collection;
import java.util.List;

public interface ProductOptionService {
    Collection<ProductOptionResponseModel> findAllByLanguageId(Long languageId);

    List<ProductOptionTableResponseModel> findAllByQueryAndLanguage(ProductOptionQueryRequestModel query, Long languageId);

    List<ProductOptionValueTableResponseModel> findAllProductOptionValues(Long productOptionId, Long languageId);

    ProductOptionDto findById(Long id);

    ProductOptionValueDto findByIdAndProductOptionId(Long id, Long productOptionId);

    ProductOptionDto createProductOption(ProductOptionRequestModel request);

    ProductOptionDto updateProductOption(Long id, ProductOptionRequestModel request);

    void deleteProductOption(Long id);

    ProductOptionValueDto createProductOptionValue(Long productOptionId, ProductOptionValueRequestModel request);

    ProductOptionValueDto updateProductOptionValue(Long id, Long productOptionId, ProductOptionValueRequestModel request);

    void deleteProductOptionValue(Long id, Long productOptionId);
}
