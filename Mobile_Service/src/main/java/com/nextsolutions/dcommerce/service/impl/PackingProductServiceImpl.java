package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.repository.PackingProductRepository;
import com.nextsolutions.dcommerce.service.PackingProductService;
import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class PackingProductServiceImpl implements PackingProductService {

    private final PackingProductRepository packingProductRepository;

    @Override
    public Page<PackingProductSearchResult> searchPackingProduct(String keyword, Long incentiveProgramId, Pageable pageable) {
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());
        return packingProductRepository.searchPackingProduct(keyword, languageId, incentiveProgramId, pageable);
    }

    @Override
    public Page<PackingProductSearchResult> searchPackingProductByKeyWord(String keyword, Pageable pageable) {
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());
        return packingProductRepository.searchPackingProductByKeyWord(keyword, languageId, pageable);
    }

    @Override
    public List<PackingProductSearchResult> searchPackingProductByCategoryId(Long categoryId, Long incentiveProgramId) {
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());
        return packingProductRepository.findAllPackingProductByCategoryIdAndLanguageId(categoryId, languageId, incentiveProgramId);
    }

    @Override
    public List<PackingProductSearchResult> searchPackingProductByManufacturerId(Long manufacturerId, Long incentiveProgramId) {
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());
        return packingProductRepository.findAllPackingProductByManufacturerIdAndLanguageId(manufacturerId, languageId, incentiveProgramId);
    }
}
