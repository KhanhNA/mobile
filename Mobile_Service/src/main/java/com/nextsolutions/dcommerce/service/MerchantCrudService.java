package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.HunterDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.ui.model.request.MerchantCrudRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MerchantCrudService {
    Page<MerchantCrudResource> findByQuery(MerchantCrudRequestModel query, Pageable pageable);

    void deactivate(Long id, String username);

    void activate(Long id, String username);

    void reactivate(Long id, String username);

    void rejectActivate(Long id, String userName);

    void rejectAction(Long id, String userName);

    void updateWalletId(Long id, Long walletId);

    boolean updateContractNumber(Long id, HunterDto hunterDto);

    Merchant confirmKpi(Long id);

    Page<MerchantDto> getAll(MerchantDto keyWord, Pageable pageable) throws Exception;
}
