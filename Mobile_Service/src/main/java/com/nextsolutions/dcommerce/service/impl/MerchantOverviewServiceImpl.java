package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.service.MerchantOverviewService;
import com.nextsolutions.dcommerce.shared.dto.OverviewMerchantDTO;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class MerchantOverviewServiceImpl extends CommonServiceImpl implements MerchantOverviewService {
    private HashMap<String, Object> param;

    @Override
    public OverviewMerchantDTO getOverview(Long parentMerchantId) {
        param = new HashMap<>();
        String sqlTotalRegister = "select count(*) from merchant where PARENT_MARCHANT_ID = :parentMerchantId and status =1";
        String sqlActive = "select  count(distinct MERCHANT_ID) as phatSinhDoanhSo from orders where " +
                " MERCHANT_ID in (select MERCHANT_ID from merchant where PARENT_MARCHANT_ID = :parentMerchantId and status =1) " +
                " and ORDER_DATE between FIRST_DAY(NOW()) and LAST_DAY(NOW())";
        param.put("parentMerchantId", parentMerchantId);
        Integer totalRegister = getRowCountV2(sqlTotalRegister, param);
        Integer totalActive = getRowCountV2(sqlActive, param);
        if (totalRegister == null && totalActive == null) {
            return null;
        } else {
            OverviewMerchantDTO overviewMerchantDTO = new OverviewMerchantDTO();
            overviewMerchantDTO.setTotalRegister(totalRegister);
            overviewMerchantDTO.setTotalActive(totalActive);
            return overviewMerchantDTO;
        }
    }

    @Override
    public List<OverviewMerchantDTO> getOverviewByMerchantId(Long merchantId) throws Exception {
        param = new HashMap<>();
        String sql = "SELECT  m.MERCHANT_ID merchantId,m.ACTIVE_DATE activeDate,sum(o.amount) as totalAmount  FROM dcommerce.merchant  m " +
                " JOIN Orders o ON o.MERCHANT_ID = m.MERCHANT_ID " +
                " where m.merchant_id = :merchantId " +
                " group by m.ACTIVE_DATE,m.MERCHANT_ID";
        param.put("merchantId", merchantId);
        return getResponseEntity(param, sql);
    }

    @Override
    public OverviewMerchantDTO getOverviewL2ByMerchantId(Long parentMerchantId, Long merchantId) throws Exception {
        param = new HashMap<>();
        String sql = "select m.merchant_id, m.ACTIVE_DATE activeDate, " +
                " (select count(o.order_id) from orders o where o.merchant_id = :merchantId and o.order_status = 0 and o.PARENT_MERCHANT_ID = :parentMerchantId ) as pendingOrderEL2,  " +
                " (select count(o.order_id) from orders o where o.merchant_id = :parentMerchantId and o.order_status = 1 ) as totalOrder, " +
                " (select count(o.order_id) from orders o where o.order_status = 0 and o.PARENT_MERCHANT_ID = :parentMerchantId ) as totalOrderL2 " +
                " from merchant m where m.merchant_id = :parentMerchantId ";
        param.put("merchantId", merchantId);
        param.put("parentMerchantId", parentMerchantId);
        return getResponseEntity(param, sql).get(0);
    }

    @Override
    public OverviewMerchantDTO getOverviewL1ByMerchantId(Long merchantId) throws Exception {
        param = new HashMap<>();
        String sql = "select m.merchant_id, m.ACTIVE_DATE activeDate, " +
                "(select count(o.order_id) from orders o where o.order_status = 0 and o.PARENT_MERCHANT_ID = :merchantId )as pendingOrderEL2 " +
                "from merchant m where m.merchant_id = :merchantId ";
        param.put("merchantId", merchantId);
        return getResponseEntity(param, sql).get(0);
    }


    private List<OverviewMerchantDTO> getResponseEntity(HashMap<String, Object> param, String sql) throws Exception {
        return findAll(null, OverviewMerchantDTO.class, sql, param);
    }
}
