package com.nextsolutions.dcommerce.service.event;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import com.nextsolutions.dcommerce.ui.controller.v1.MerchantRestController;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class PushNotificationRegisterEvent extends ApplicationEvent {
    private Long parentMerchantId;
    private Merchant l2Merchant;

    public PushNotificationRegisterEvent(Object source, Long parentMerchantId, Merchant merchantDto) {
        super(source);
        this.l2Merchant = merchantDto;
        this.parentMerchantId = parentMerchantId;
    }
}
