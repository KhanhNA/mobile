package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.ui.model.request.MerchantRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MerchantQueryService {
    Page<MerchantResource> findByQuery(MerchantRequestModel query, Pageable pageable);
}
