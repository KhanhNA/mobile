package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.OverviewMerchantDTO;

import java.util.List;

public interface MerchantOverviewService  {
    OverviewMerchantDTO getOverview(Long parentMerchantId);

    List<OverviewMerchantDTO> getOverviewByMerchantId(Long merchantId) throws Exception;

    OverviewMerchantDTO getOverviewL2ByMerchantId(Long parentMerchantId, Long merchantId) throws Exception;

    OverviewMerchantDTO getOverviewL1ByMerchantId(Long merchantId) throws Exception;
}
