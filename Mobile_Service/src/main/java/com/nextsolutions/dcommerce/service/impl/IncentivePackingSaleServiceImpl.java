package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.PackingProductEntity;
import com.nextsolutions.dcommerce.model.incentive.*;
import com.nextsolutions.dcommerce.service.IncentivePackingSaleService;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
public class IncentivePackingSaleServiceImpl extends CommonServiceImpl implements IncentivePackingSaleService {


    @Override
    public Page<IncentivePackingSale> getPackingSale(Boolean isPackingGroup, Long ictPgId, Pageable pageable, Long status) {
        String sql = "SELECT * " +
                "FROM incentive_packing_sale " +
                "WHERE incentive_program_id= :ictPgId " +
                "AND status = :status ";
        if (isPackingGroup) {
            sql += "AND incentive_packing_group_id IS NOT NULL";
        } else {
            sql += "AND packing_product_id IS NOT NULL";
        }
        String sqlCount = String.format("SELECT COUNT(*) FROM (%s) a", sql);

        Query query = entityManager.createNativeQuery(sql, IncentivePackingSale.class);
        Query queryCount = entityManager.createNativeQuery(sqlCount);

        query.setParameter("ictPgId", ictPgId);
        query.setParameter("status", status);
        queryCount.setParameter("ictPgId", ictPgId);
        queryCount.setParameter("status", status);

        BigInteger total = (BigInteger) queryCount.getSingleResult();

        query.setFirstResult(pageable.getPageSize() * pageable.getPageNumber());
        query.setMaxResults(pageable.getPageSize());

        List<IncentivePackingSale> list = query.getResultList();

        return new PageImpl<>(list, pageable, total.intValue());
    }

    @Override
    public void saveIctPackingIct(List<IncentivePackingIncentive> dtos, Long lvlId) throws Exception {
        if (lvlId == null) {
            throw new CustomErrorException("PackingLevelNull");
        }
        IncentiveLevel ictLvl = find(IncentiveLevel.class, lvlId);
        if (ictLvl == null) {
            throw new CustomErrorException("PackingLevelNotExists");
        }
        dtos = Utils.safe(dtos);
        IncentivePackingIncentive entity;
        Long id;
        for (IncentivePackingIncentive ictPckIct : dtos) {
            entity = ictPckIct;

            id = ictPckIct.getId();
            if (id != null) {
                entity = find(IncentivePackingIncentive.class, id);
                if (entity == null) {
                    throw new CustomErrorException("PackingIctNotExists");
                }
                entity = Utils.getData(ictPckIct, entity);
            }
            id = entity.getPackingProduct().getPackingProductId();
            if (id == null) {
                throw new CustomErrorException("PackingProductIdNotExists");
            }
            PackingProductEntity packingProduct = find(PackingProductEntity.class, id);
            if (packingProduct == null) {
                throw new CustomErrorException("PackingProductIdNotExists");
            }
            entity.setPackingProduct(packingProduct);
            entity.setIncentiveLevelId(lvlId);
            entity.setStatus(1L);
            entity.setQuantity(ictPckIct.getQuantity());
            entity.setMaxQuantity(ictPckIct.getMaxQuantity());
            ictLvl.setHasIctPacking(1);
            save(ictLvl);
            save(entity);
        }
    }

    @Override
    public void savePackingSale(List<IncentivePackingSale> ictPackingSales, Long ictPrgId, Integer type) throws Exception {
        if (ictPrgId == null) {
            throw new CustomErrorException("IncentiveProgramIdNull");
        }
        Incentive ictPrg = find(Incentive.class, ictPrgId);
        if (ictPrg == null) {
            throw new CustomErrorException("IncentiveProgramNotFound");
        }
        if (type == null) {
            throw new CustomErrorException("GroupTypeNull");
        }
        PackingProduct pd;
        IncentivePackingGroup ipg;
        IncentivePackingSale ips;
        Object entity;
        String sqlCond = "";
        HashMap<String, Object> params = new HashMap<>();
        for (IncentivePackingSale ictPackingSale : ictPackingSales) {
            entity = null;
            sqlCond = "";
            params.clear();
            ips = null;
            params.put("ictPrgId", ictPrgId);
            if (type == 1) {//packing
                entity = find(PackingProduct.class, ictPackingSale.getPackingProductId());
                sqlCond = " packingProductId = :packingProductId and obj_type = 1";
                params.put("packingProductId", ((PackingProduct) entity).getPackingProductId());
            } else if (type == 2) {
                entity = find(IncentivePackingGroup.class, ictPackingSale.getIctPackingGroupId());
                sqlCond = " ictPackingGroupId = :ictPackingGroupId and obj_type = 2";
                params.put("ictPackingGroupId", ((IncentivePackingGroup) entity).getId());
            }
            if (entity == null) {
                throw new CustomErrorException("idNotExist");
            }
            //tim
            List<IncentivePackingSale> tmp = findAll(IncentivePackingSale.class,
                    "from IncentivePackingSale " +
                            " where incentiveProgramId = :ictPrgId " +
                            "       and " + sqlCond, params, 1);
            if (tmp == null || tmp.size() == 0) {
                ips = new IncentivePackingSale();
            } else if (tmp.size() == 1) {
                ips = tmp.get(0);
            } else if (tmp.size() > 1) {
                for (IncentivePackingSale i : tmp) {
                    remove(i);
                }
                ips = new IncentivePackingSale();
            }

            ips.setIncentiveProgramId(ictPrgId);
            if (type == 1) {
                ips.setPackingProduct((PackingProduct) entity);
            } else {
                ips.setIctPackingGroup((IncentivePackingGroup) entity);
                ((IncentivePackingGroup) entity).setIsAssign(1);
                save(entity);
            }
            ips.setObjType(type);
            ips.setStatus(1L);
            ips.setFromDate(ictPackingSale.getFromDate());
            ips.setToDate(ictPackingSale.getToDate());

            save(ips);
        }
    }

    @Override
    public void saveIctLvl(List<IncentiveLevel> dtos, Long sId) throws Exception {
        if (sId == null) {
            throw new CustomErrorException("PackingSaleNull");
        }
        IncentivePackingSale ictPackingSale = find(IncentivePackingSale.class, sId);
        if (ictPackingSale == null) {
            throw new CustomErrorException("PackingSaleNotExists");
        }
        dtos = Utils.safe(dtos);
        IncentiveLevel entity;
        Long id;
        for (IncentiveLevel ictLevel : dtos) {
            entity = ictLevel;

            id = ictLevel.getId();
            if (id != null) {
                entity = find(IncentiveLevel.class, id);
                if (entity == null) {
                    throw new CustomErrorException("PackingLevelNotExists");
                }
                entity = Utils.getData(ictLevel, entity);
            }
            entity.setIncentivePackingSaleId(sId);
            entity.setHasIctPacking(0);
            entity.setStatus(1);
            entity.setIncentiveProgramId(ictPackingSale.getIncentiveProgramId());
            entity.setPackingProductId(ictPackingSale.getPackingProductId());
            entity.setIctPackingGroupId(ictPackingSale.getIctPackingGroupId());
            entity.setObjType(ictPackingSale.getObjType());
            save(entity);
        }
    }

    @Override
    public void delIctLvl(Long id) throws Exception {
        IncentiveLevel entity;
        if (id == null) {
            throw new CustomErrorException("IdNull");
        }
        entity = find(IncentiveLevel.class, id);
        if (entity == null) {
            throw new CustomErrorException("PackingLevelNotExists");
        }
        entity.setStatus(0);
        save(entity);

    }

    @Override
    public void delIctPackingIct(Long id) throws Exception {
        IncentivePackingIncentive entity;
        if (id == null) {
            throw new CustomErrorException("IdNull");
        }
        entity = find(IncentivePackingIncentive.class, id);
        if (entity == null) {
            throw new CustomErrorException("IncentivePackingIncentiveNotExists");
        }
        entity.setStatus(0L);
        save(entity);

    }

    @Override
    public void delIctPackingSale(Long id) throws Exception {
        IncentivePackingSale entity;
        if (id == null) {
            throw new CustomErrorException("IdNull");
        }
        entity = find(IncentivePackingSale.class, id);
        if (entity == null) {
            throw new CustomErrorException("IncentivePackingSaleNotExists");
        }
        entity.setStatus(0L);
        entity.setToDate(LocalDateTime.now());
        if (entity.getObjType() == 2) {//group
            entity.getIctPackingGroup().setIsAssign(0);
            save(entity.getIctPackingGroup());
        }
        save(entity);
    }

    @Override
    public Page<PackingProduct> list(Pageable pageable, List<Long> exceptIds, String text) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        StringBuilder sqlPacking = new StringBuilder();
        sqlPacking.append("select a.*,concat(a.name,' - ',p.PRODUCT_NAME) as packingName ");
        sqlPacking.append(" from packing_product a ");
        sqlPacking.append("  JOIN product p on a.product_id = p.product_id ");
        sqlPacking.append(" where (a.PACKING_PRODUCT_ID not in ");
        sqlPacking.append(" ( ");
        sqlPacking.append(Utils.join(",", exceptIds));
        sqlPacking.append(" )) ");

        if (!Utils.isEmpty(text)) {
            sqlPacking.append(" and (a.code like concat('%',").append("'").append(text).append("'").append(",'%') or (p.product_name like concat('%',").append("'"+text+"'").append(",'%'))) ");
        }

        String sqlCount = String.format("select count(*) from (%s) a ", sqlPacking.toString());

//        Query query = entityManager.createNativeQuery(sqlPacking.toString(), PackingProduct.class);
        Query queryCount = entityManager.createNativeQuery(sqlCount);
//        params.put("exceptIds", exceptIds);
//        query.setParameter("exceptIds", exceptIds);
//        queryCount.setParameter("exceptIds", exceptIds);

//        if (!Utils.isEmpty(text)) {
//            params.put("text", "'" + text + "'");
//            queryCount.setParameter("text", text);
//        }

        BigInteger count = (BigInteger) queryCount.getSingleResult();
//        params.put("exceptIds", exceptIds);

        List<PackingProduct> packingProducts = findAll(pageable, PackingProduct.class, sqlPacking.toString(), params);
        return new PageImpl<>(packingProducts, pageable, count.intValue());
    }

    @Override
    public void saveIncentiveLvlForOrderValue(List<IncentiveLevel> dtos, Long incentiveId) throws Exception {
        if (incentiveId == null) {
            throw new CustomErrorException("incentiveIdNull");
        }

        Incentive incentive = find(Incentive.class, incentiveId);
        if (incentive == null) {
            throw new CustomErrorException("incentiveNotExist");
        }

        dtos = Utils.safe(dtos);
        IncentiveLevel entity;
        Long id;
        for (IncentiveLevel incentiveLevel : dtos) {
            entity = incentiveLevel;
            id = incentiveLevel.getId();
            if (id != null) {
                entity = find(IncentiveLevel.class, id);
                if (entity == null) throw new CustomErrorException("IncentiveLevelNotExist");

                entity = Utils.getData(incentiveLevel, entity);
            }
            entity.setStatus(1);
            entity.setMinQuantity(null);
            entity.setIncentiveProgramId(incentiveId);
            entity.setIncentivePackingSaleId(null);
            entity.setHasIctPacking(0);
            entity.setIctPackingGroupId(null);
            entity.setObjType(null);
            save(entity);
        }
    }

    @Override
    public Page<IncentiveLevel> getIncentiveLvlList(Long incentiveId, Long status, Pageable pageable) {

        String sql = " SELECT ictLvl.*" +
                " FROM incentive_level ictLvl INNER JOIN incentive_program" +
                " ON ictLvl.incentive_program_id =  incentive_program.id AND incentive_program.type = :forOrderValue" +
                " WHERE (1=1) AND ictLvl.incentive_program_id=:incentiveId " +
                " AND ictLvl.status=:status " +
                " AND ictLvl.incentive_packing_group_id is NULL " +
                " AND ictLvl.packing_product_id is NULL ";

        Query query = entityManager.createNativeQuery(sql, IncentiveLevel.class);
        query.setParameter("incentiveId", incentiveId);
        query.setParameter("status", status);
        query.setParameter("forOrderValue", Incentive.TYPE_ORDER_VALUE);

        String sqlCount = String.format(" SELECT COUNT(*) FROM (%s) a ", sql);
        Query queryCount = entityManager.createNativeQuery(sqlCount);
        queryCount.setParameter("incentiveId", incentiveId);
        queryCount.setParameter("status", status);
        queryCount.setParameter("forOrderValue", Incentive.TYPE_ORDER_VALUE);

        BigInteger count = (BigInteger) queryCount.getSingleResult();

        int firstResult = pageable.getPageSize() * pageable.getPageNumber();
        int maxResult = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);

        List<IncentiveLevel> list = query.getResultList();

        return new PageImpl<>(list, pageable, count.intValue());
    }


}
