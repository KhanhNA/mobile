package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.UserEntity;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import org.springframework.http.ResponseEntity;

public interface AccountService {

    void createAccount(UserEntity userEntity);

    void changePassword(UserEntity userEntity, String token);

    void deactivateAccount(String username, String token);

    void deactivateAccount(String username);

    boolean existsAccount(String username, String token);

    boolean existsAccount(String username);

    UserEntity findByUsername(String username);

    void activateAccount(String username);

    ResponseEntity<Object> resetPassword( MerchantDto userName) throws Exception;

}
