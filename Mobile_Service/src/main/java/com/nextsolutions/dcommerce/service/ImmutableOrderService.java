package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.ui.model.request.CancelOrderRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.OrderRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderDetailsResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.SimpleApiResponseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;

public interface ImmutableOrderService {
    Page<OrderResponseModel> findByParams(OrderRequestModel orderRequestModel, Pageable pageable);

    OrderDetailsResponseModel getOrderDetailsById(Long id, HttpHeaders headers);

    SimpleApiResponseModel cancelOrder(Long id, CancelOrderRequestModel request, HttpHeaders headers) throws Exception;
}
