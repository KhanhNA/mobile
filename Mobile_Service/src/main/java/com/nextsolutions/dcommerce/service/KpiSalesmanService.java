package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.KpiSalesmanDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface KpiSalesmanService {

    KpiSalesmanDTO createKpiSalesman(KpiSalesmanDTO kpiSalesmanDTO);

    KpiSalesmanDTO updateKpiSalesman(Long id, KpiSalesmanDTO kpiSalesmanDTO);

    Page<KpiSalesmanDTO> kpiSalesmans(KpiSalesmanDTO kpiSalesmanDTO,Pageable pageable) throws Exception;

    KpiSalesmanDTO kpiSalesmanActive() throws Exception;
}
