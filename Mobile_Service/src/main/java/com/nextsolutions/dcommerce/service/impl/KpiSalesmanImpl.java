package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.model.KpiSalesman;
import com.nextsolutions.dcommerce.repository.KpiSalesmanRepository;
import com.nextsolutions.dcommerce.service.FileService;
import com.nextsolutions.dcommerce.service.KpiSalesmanService;
import com.nextsolutions.dcommerce.shared.dto.KpiSalesmanDTO;
import com.nextsolutions.dcommerce.shared.dto.product.PackingPriceDto;
import com.nextsolutions.dcommerce.shared.mapper.KpiSalesmanMapper;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import javax.persistence.EntityNotFoundException;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor
public class KpiSalesmanImpl implements KpiSalesmanService {
    private final KpiSalesmanRepository kpiSalesmanRepository;
    private final KpiSalesmanMapper kpiSalesmanMapper;
    private final static String PARENT_PATH = "/file/salesman";
    private final AppConfiguration appConfiguration;
    private final FileService fileService;
    private final CommonServiceImpl commonService;

    @Override
    public KpiSalesmanDTO createKpiSalesman(KpiSalesmanDTO kpiSalesmanDTO) {
        KpiSalesman kpiSalesman = new KpiSalesman();
        kpiSalesman.setDescription(kpiSalesmanDTO.getDescription());
        kpiSalesman.setFromDate(kpiSalesmanDTO.getFromDate());
        kpiSalesman.setToDate(kpiSalesmanDTO.getToDate());
        kpiSalesman.setName(kpiSalesmanDTO.getName());
        String filePath = kpiSalesmanDTO.getFilePath();
        if(filePath != null) {
            try {
                String relativeFilePath = fileService.savePdf(filePath);
                kpiSalesman.setFilePath(relativeFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        kpiSalesman.setUserModified(SpringUtils.getCurrentUsername());
        kpiSalesman.setCreatedDate(LocalDateTime.now());
        kpiSalesmanRepository.save(kpiSalesman);
        return kpiSalesmanMapper.toKpiSalesman(kpiSalesman);
    }

    @Override
    public KpiSalesmanDTO updateKpiSalesman(Long id, KpiSalesmanDTO kpiSalesmanDTO) {
        KpiSalesman kpiSalesman = kpiSalesmanRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Id" + id + "not found"));
        String filePath = kpiSalesmanDTO.getFilePath();
        if(filePath != null) {
            try {
                String relativeFilePath = fileService.savePdf(filePath);
                kpiSalesman.setFilePath(relativeFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        kpiSalesman.setDescription(kpiSalesmanDTO.getDescription());
        kpiSalesmanRepository.save(kpiSalesman);
        return kpiSalesmanMapper.toKpiSalesman(kpiSalesman);
    }

    @Override
    public Page<KpiSalesmanDTO> kpiSalesmans(KpiSalesmanDTO kpiSalesmanDTO, Pageable pageable) throws Exception {
        StringBuilder select = new StringBuilder();
        select.append("Select * from kpi_salesman where 1=1 ");
        String name = kpiSalesmanDTO.getName();
        LocalDateTime fromDate = kpiSalesmanDTO.getFromDate();
        LocalDateTime toDate = kpiSalesmanDTO.getToDate();
        HashMap<String, Object> params = new HashMap<>();
        if (Objects.nonNull(name)) {
            select.append(" and lower(name) like lower(concat('%',\"").append(name).append("\",'%'))");
        }
        if (fromDate != null && toDate != null) {
            select.append(" and date( from_date ) >= date( :fromDate ) and date( to_date ) <= date( :toDate )");
        } else {
            if (fromDate != null) {
                select.append(" and date( from_date ) >= date( :fromDate )");
            }
            if (toDate != null) {
                select.append(" and date( to_date ) <= date( :toDate )");
            }
        }
        select.append(" order by created_date desc");

        //params.put("date", LocalDateTime.now());
        if (fromDate != null && toDate != null) {
            params.put("fromDate", fromDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            params.put("toDate", toDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        } else {
            if (fromDate != null) {
                params.put("fromDate", fromDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            }
            if (toDate != null) {
                params.put("toDate", toDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            }
        }

        String query = select.toString();

        List<KpiSalesmanDTO> listKpiSalesman = commonService.findAll(pageable, KpiSalesmanDTO.class, query, params);
        Integer total = commonService.getRowCountV2(String.format("select count(*) from (%s) x", query), params);
        return new PageImpl<>(listKpiSalesman, pageable, total);

    }

    @Override
    public KpiSalesmanDTO kpiSalesmanActive() throws Exception {
        StringBuilder select = new StringBuilder();
        select.append("Select * from kpi_salesman where date(from_date) <= date( :date ) order by from_date desc  limit 1");
        List<KpiSalesmanDTO> kpiSalesman = commonService.findAll(null, KpiSalesmanDTO.class, select.toString(), LocalDateTime.now());
        return kpiSalesman.get(0);
    }

}
