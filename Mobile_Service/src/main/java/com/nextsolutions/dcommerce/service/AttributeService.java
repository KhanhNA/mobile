package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.form.AttributeForm;
import com.nextsolutions.dcommerce.shared.dto.form.AttributeValueForm;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeResource;
import com.nextsolutions.dcommerce.shared.dto.AttributeDto;
import com.nextsolutions.dcommerce.shared.dto.AttributeValueDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;

public interface AttributeService {
    List<AttributeResource> findAllByLanguageId(Integer languageId);

    List<AttributeResource> findAllMerchantGroupAttributes(Integer languageId);

    List<AttributeResource> findAllProductsAttributes(Integer languageId);

    AttributeForm findById(Long id);

    AttributeValueForm findByIdAndValueId(Long id, Long valueId);

    Page<AttributeDto> getAttributeTable(AttributeDto attributeDto, Pageable pageable) throws Exception;

    List<AttributeValueDto> getAttributeValueTable(Long id, Integer languageId);

    AttributeForm requestCreateAttribute(AttributeForm form) throws IOException;

    AttributeForm requestUpdateAttribute(Long id, AttributeForm form);

    void inactiveAttribute(Long id);

    AttributeForm requestInactiveAttribute(Long id);

    AttributeValueForm requestCreateAttributeValue(AttributeValueForm form);

    AttributeValueForm requestUpdateAttributeValue(Long id, Long valueId, AttributeValueForm form);

    AttributeValueForm requestInactiveAttributeValue(Long id, Long valueId);

    AttributeValueForm managerApprovedValues(Long id, Long valueId);

    AttributeValueForm managerRejectValues(Long id, Long valueId);

    void inactiveAttributeValue(Long id, Long valueId);

    AttributeValueForm managerUpdateAttributeValue(Long id, Long valueId, AttributeValueForm form);

    AttributeForm managerApproved(Long id);

    AttributeForm managerReject(Long id);

    AttributeForm managerUpdate(Long id, AttributeForm form);
}
