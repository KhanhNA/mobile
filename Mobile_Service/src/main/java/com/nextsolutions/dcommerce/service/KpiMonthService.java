package com.nextsolutions.dcommerce.service;

import java.util.List;

import com.nextsolutions.dcommerce.shared.dto.KpiMonthDto;
import org.springframework.http.ResponseEntity;

public interface KpiMonthService {
	List<KpiMonthDto> findAll(KpiMonthDto dto) throws Exception;

	ResponseEntity<Object> hhptdsBonusPercentTotalAmountInMonth();

	ResponseEntity<Object> hhptkbL2Active();
}
