package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PackingProductService {
    Page<PackingProductSearchResult> searchPackingProduct(String keyword, Long incentiveProgramId, Pageable pageable);

    Page<PackingProductSearchResult> searchPackingProductByKeyWord(String keyword,  Pageable pageable);

    List<PackingProductSearchResult> searchPackingProductByCategoryId(Long categoryId, Long incentiveProgramId);

    List<PackingProductSearchResult> searchPackingProductByManufacturerId(Long manufacturerId, Long incentiveProgramId);
}
