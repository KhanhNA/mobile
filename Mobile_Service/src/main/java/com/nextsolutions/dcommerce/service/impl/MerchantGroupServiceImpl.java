package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.AppParam;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.model.MerchantGroupMerchant;
import com.nextsolutions.dcommerce.service.MerchantGroupService;
import com.nextsolutions.dcommerce.shared.dto.MerchantGroupDTO;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
public class MerchantGroupServiceImpl extends CommonServiceImpl implements MerchantGroupService {
    @Override
    public void save(List<MerchantGroup> merchantGroups) throws Exception {

//        MerchantGroup entity;
        MerchantGroup merchantGroup;
        for (MerchantGroup element : merchantGroups) {
            if (element.getId() != null) {
                merchantGroup = find(MerchantGroup.class, element.getId());
                if (merchantGroup == null) {
                    throw new CustomErrorException("MerchantGroupIdNotExist");
                }
            } else {
                merchantGroup = new MerchantGroup();

                merchantGroup.setFromDate(LocalDateTime.now());
                merchantGroup.setToDate(null);
            }
            merchantGroup = Utils.getData(element, merchantGroup);
            merchantGroup.setStatus(1);
            save(merchantGroup);
        }

    }

    @Override
    public void delete(Long id) throws Exception {
        MerchantGroup entity = find(MerchantGroup.class, id);
        if (entity == null) {
            throw new CustomErrorException("incentiveMerchantGroupNotExist");
        }
        entity.setToDate(LocalDateTime.now());
        entity.setStatus(0);
        save(entity);
    }

    @Override
    public Page<Merchant> list(Pageable pageable, Long merchantGroupId) throws Exception {
        String sql = "select a.* " +
                " from merchant a left join merchant_group_merchant b on a.MERCHANT_ID = b.MERCHANT_ID and b.MERCHANT_GROUP_ID = :merchantGroupId" +
                " where b.MERCHANT_ID is null";
        HashMap<String, Object> params = new HashMap<>();
        Query query = entityManager.createNativeQuery(sql, Merchant.class);
        query.setParameter("merchantGroupId", merchantGroupId);
        String sqlCount = String.format("select count(*) from (%s) a ", sql);

        Query queryCount = entityManager.createNativeQuery(sqlCount);
        queryCount.setParameter("merchantGroupId", merchantGroupId);
        BigInteger count = (BigInteger) queryCount.getSingleResult();
        params.put("merchantGroupId", merchantGroupId);
        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<Merchant> merchants = query.getResultList();
        return new PageImpl<>(merchants, pageable, count.intValue());

    }

    public void addMerchant(List<Long> merchantIds, Long merchantGroupId) throws Exception {
        MerchantGroup merchantGroup = find(MerchantGroup.class, merchantGroupId);
        if (merchantGroup == null) {
            throw new CustomErrorException("MerchantGroupNotExists");
        }
        Merchant merchant;
        String sql = "select 1 from MerchantGroupMerchant a where merchantGroupId = :merchantGroupId and a.merchant.merchantId = :merchantId";
        HashMap<String, Object> params = new HashMap<>();
        for (Long merchantId : merchantIds) {
            merchant = find(Merchant.class, merchantId);
            if (merchant == null) {
                throw new CustomErrorException("MerchantNotExists");
            }
            params.clear();
            params.put("merchantGroupId", merchantGroupId);
            params.put("merchantId", merchant.getMerchantId());
            List<Long> lst = findAll(sql, null, params, 1);
            if (lst != null && lst.size() > 0) {
                continue;
            }
            MerchantGroupMerchant merchantGroupMerchant = new MerchantGroupMerchant();
            merchantGroupMerchant.setMerchant(merchant);
            merchantGroupMerchant.setMerchantGroupId(merchantGroupId);
            save(merchantGroupMerchant);
        }
    }

    public void deleteMerchantGroupMerchant(Long merchantGroupMerchantid) throws Exception {
        MerchantGroupMerchant entity = find(MerchantGroupMerchant.class, merchantGroupMerchantid);
        if (entity == null) {
            throw new CustomErrorException("MerchantGroupMerchantNotExist");
        }
        remove(entity);
    }


    @Override
    public Page<MerchantGroup> listGroup(Pageable pageable, List<Long> exceptIds, String text) throws Exception {
        String sql = "select a.* " +
                " from merchant_group a " +
                " where a.ID not in :exceptIds";

        if (!Utils.isEmpty(text)) {
            sql = sql + " and (a.code like concat('%',:text,'%') or (a.name like concat('%',:text,'%')))";

        }

        Query query = entityManager.createNativeQuery(sql, MerchantGroup.class);
        query.setParameter("exceptIds", exceptIds);
        String sqlCount = String.format("select count(*) from (%s) a ", sql);

        Query queryCount = entityManager.createNativeQuery(sqlCount);
        queryCount.setParameter("exceptIds", exceptIds);

        if (!Utils.isEmpty(text)) {
            query.setParameter("text", text);
            queryCount.setParameter("text", text);
        }

        BigInteger count = (BigInteger) queryCount.getSingleResult();

        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<MerchantGroup> packingProducts = query.getResultList();
        return new PageImpl<>(packingProducts, pageable, count.intValue());
    }

    @Override
    public Page<MerchantGroupDTO> listGroupDTO(Pageable pageable, List<Long> exceptIds, String text) throws Exception {
        String sql = "SELECT mgd.merchant_group_id id,mgd.name, mg.code FROM merchant_group mg JOIN " +
                " merchant_group_description mgd on mg.id = mgd.merchant_group_id and mg.id not in :exceptIds" +
                " WHERE " +
                "    mg.status = 1 AND mgd.language_id = 1 ";

        if (!Utils.isEmpty(text)) {
            sql = sql + " and (mg.code like concat('%',:text,'%') or (mgd.name like concat('%',:text,'%')))";

        }
        Query query = entityManager.createNativeQuery(sql, "MerchantGroupList");
        query.setParameter("exceptIds", exceptIds);
        //

        String sqlCount = String.format("select count(*) from (%s) a ", sql);
        Query queryCount = entityManager.createNativeQuery(sqlCount);
        queryCount.setParameter("exceptIds", exceptIds);

        if (!Utils.isEmpty(text)) {
            query.setParameter("text", text);
            queryCount.setParameter("text", text);
        }
        BigInteger count = (BigInteger) queryCount.getSingleResult();

        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<MerchantGroupDTO> packingProducts = query.getResultList();
        return new PageImpl<>(packingProducts, pageable, count.intValue());
    }

    @Override
    public List<AppParam> listGroupParam() throws Exception {
        String sql = "from AppParam where groupCode = 'MERCHANT_GROUP' and status = 1 order by type, ord";
        List<AppParam> lst = findAll(AppParam.class, sql, null, 0);
        return lst;

    }
}
