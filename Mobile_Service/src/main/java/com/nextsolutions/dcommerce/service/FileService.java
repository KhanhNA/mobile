package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.utils.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Base64;

@Service
public class FileService {

    private final Tika tika;
    private final AppConfiguration appConfiguration;

    public FileService(AppConfiguration appConfiguration) {
        this.appConfiguration = appConfiguration;
        this.tika = new Tika();
    }

    public String detectType(byte[] fileArray) {
        return tika.detect(fileArray);
    }

    public String savePdf(String fileBase64) throws IOException {
        if(fileBase64.startsWith("data:")) {
            fileBase64 = fileBase64.replaceFirst("(data:.*;base64,)", "");
            String fileName = StringUtils.randomUUID() + ".pdf";
            byte[] fileArray = Base64.getDecoder().decode(fileBase64);
            File file = new File(appConfiguration.getFullAttachmentPdfPath() + "/" + fileName);
            FileUtils.writeByteArrayToFile(file, fileArray);
            return appConfiguration.getAttachmentPdfPath() + "/" + fileName;
        } else {
            return fileBase64;
        }
    }
}
