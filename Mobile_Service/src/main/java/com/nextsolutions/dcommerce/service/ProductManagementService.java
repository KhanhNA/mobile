package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.ProductDataDto;
import com.nextsolutions.dcommerce.shared.dto.ProductGeneralDto;
import com.nextsolutions.dcommerce.shared.dto.product.*;
import com.nextsolutions.dcommerce.shared.dto.product.packing.PackingProductDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductManagementService {
    ProductGeneralDto createProduct(ProductGeneralDto productGeneralDto);

    ProductGeneralDto updateProductGeneral(Long id, ProductGeneralDto productGeneralDto);

    ProductGeneralDto getProductGeneral(Long id);

    ProductDataDto updateProductData(Long id, ProductDataDto productDataDto);

    ProductDataDto getProductData(Long id);

    ProductLinksDto getProductLinksDto(Long id);

    ProductLinksDto updateProductLinks(Long id, ProductLinksDto productLinksDto);

    Page<ProductLinksManufacturerDto> getProductLinksManufacturers(String keyword, Pageable pageable);

    Page<ProductLinksDistributorDto> getProductLinksDistributors(String keyword, Pageable pageable);

    List<ProductOptionsDto> getProductOptions(Long id);

    void updateProductOptions(Long id, List<ProductOptionsDto> productOptionsDtos);

    Page<PackingProductDto> getPackingProducts(Long id,  Pageable pageable);
}
