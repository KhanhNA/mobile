package com.nextsolutions.dcommerce.service.exception;

import com.nextsolutions.dcommerce.shared.dto.resource.SimpleApiResource;

public class DuplicateEntityException extends RuntimeException {
    private final SimpleApiResource simpleApiResource;

    public DuplicateEntityException(SimpleApiResource simpleApiResource) {
        this.simpleApiResource = simpleApiResource;
    }

    public SimpleApiResource getSimpleApiResource() {
        return simpleApiResource;
    }
}
