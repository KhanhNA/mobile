package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.AppParam;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.shared.dto.MerchantGroupDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MerchantGroupService extends CommonService {
    void save(List<MerchantGroup> merchantGroups) throws Exception;

    void delete(Long id) throws Exception;

    Page<Merchant> list(Pageable pageable, Long merchantGroupId) throws Exception;

    void addMerchant(List<Long> merchantIds, Long merchantGroupId) throws Exception;

    void deleteMerchantGroupMerchant(Long merchantGroupMerchantid) throws Exception;

    Page<MerchantGroup> listGroup(Pageable pageable, List<Long> exceptIds, String text) throws Exception;

    Page<MerchantGroupDTO> listGroupDTO(Pageable pageable, List<Long> exceptIds, String text) throws Exception;

    List<AppParam> listGroupParam() throws Exception;
}
