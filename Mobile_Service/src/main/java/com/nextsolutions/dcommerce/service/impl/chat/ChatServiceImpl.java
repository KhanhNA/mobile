package com.nextsolutions.dcommerce.service.impl.chat;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.nextsolutions.dcommerce.configuration.properties.ChatConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.service.chat.ChatService;
import com.nextsolutions.dcommerce.service.impl.CommonServiceImpl;
import com.nextsolutions.dcommerce.shared.dto.PostMessageDTO;
import com.nextsolutions.dcommerce.shared.dto.chat.ChatAccountDto;
import com.nextsolutions.dcommerce.shared.dto.chat.ChatLoginResponseDataDto;
import com.nextsolutions.dcommerce.shared.dto.chat.ChatLoginResponseDto;
import com.nextsolutions.dcommerce.shared.dto.chat.DiscussionDTO;
import com.nextsolutions.dcommerce.shared.dto.chat.GroupNotificationDTO;
import com.nextsolutions.dcommerce.shared.dto.chat.User;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.shared.dto.wallet.ResponseCreateClientDto;

import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class ChatServiceImpl extends CommonServiceImpl implements ChatService {
    private HttpHeaders httpHeaders;
    private final RestTemplate restTemplate;
    private final ChatConfiguration chatConfiguration;
    private static final Logger log = LogManager.getLogger(ChatServiceImpl.class);

    HttpHeaders createRocketHeaders() {
        return new HttpHeaders() {{
            setContentType(MediaType.APPLICATION_JSON);
        }};
    }


    @Override
    public ResponseCreateClientDto registerAccount(MerchantCrudResource merchant) throws Exception {

        String url = chatConfiguration.getBaseUrl() + "/v1/users.register";
        HttpMethod method = HttpMethod.POST;
        ChatAccountDto chatAccountDto = new ChatAccountDto();
        chatAccountDto.username = merchant.getUsername();
        chatAccountDto.name = merchant.getFullName();
        chatAccountDto.email = merchant.getUsername() + "@gmail.com";
        chatAccountDto.pass = "abc@123";//merchant.getPassword();
        HttpEntity<String> requestBody = new HttpEntity<>(chatAccountDto.toJson(), createRocketHeaders());
        RestTemplate restTemplate = new RestTemplate();
        try {
            //1. register on chat app
            ResponseEntity<String> response = restTemplate.exchange(url, method, requestBody, String.class);
            //2. register on posts app
        } catch (HttpClientErrorException e) {
            System.out.println(e.getStatusCode());
            System.out.println(e.getResponseBodyAsString());
            throw e;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }

        return null;
    }

    @Override
    public ResponseEntity<String> registerAccount(Merchant merchant) throws Exception {
        String url = chatConfiguration.getBaseUrl() + "/v1/users.register";
        HttpMethod method = HttpMethod.POST;
        ChatAccountDto chatAccountDto = new ChatAccountDto();
        chatAccountDto.username = merchant.getUserName();
        chatAccountDto.name = merchant.getFullName();
        chatAccountDto.email = merchant.getUserName() + "@gmail.com";
        chatAccountDto.pass = "abc@123";//merchant.getPassword();
        HttpEntity<String> requestBody = new HttpEntity<>(chatAccountDto.toJson(), createRocketHeaders());
        RestTemplate restTemplate = new RestTemplate();
        try {
            //1. register on chat app
            return restTemplate.exchange(url, method, requestBody, String.class);
            //2. register on posts app

        } catch (HttpClientErrorException e) {
            System.out.println(e.getStatusCode());
            System.out.println(e.getResponseBodyAsString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void createAccount(ChatAccountDto chatAccountDto) {
        String url = chatConfiguration.getBaseUrl() + "/v1/users.register";
        restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(chatAccountDto, createRocketHeaders()), Object.class);
    }

    @Override
    public String createDiscussion(Long merchantId, String name, Boolean isSave) throws Exception {
        String urlCreate = chatConfiguration.getBaseUrl() + "/v1/rooms.createDiscussion";

        try {
            String[] invUser = new String[1];
            invUser[0] = name;
            GroupNotificationDTO dto = GroupNotificationDTO.builder().prid(chatConfiguration.getPrid())
                    .t_name("Mstore Notification")
                    .users(invUser)
                    .build();
            ChatLoginResponseDataDto loginInfo = getLoginInfo();
            // Header
            httpHeaders = new HttpHeaders();
            httpHeaders.set("X-Auth-Token", loginInfo.getAuthToken());
            httpHeaders.set("X-User-Id", loginInfo.getUserId());
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> requestBody = new HttpEntity<>(new Gson().toJson(dto), httpHeaders);

            ResponseEntity<DiscussionDTO> response = restTemplate.exchange(urlCreate, HttpMethod.POST, requestBody, DiscussionDTO.class);
            if (isSave && response.getBody() != null && !StringUtils.isEmpty(response.getBody().getDiscussion().getRid())) {
                // Save roomId
                executeSql("update merchant set discussion_id = '" + response.getBody().getDiscussion().getRid() + "' where MERCHANT_ID = :merchantId", "merchantId", merchantId);
            }
            return response.getBody().getDiscussion().getRid();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public void sendMessageToRoomID(String roomId, String message) throws Exception {

        ChatLoginResponseDataDto loginInfo = getLoginInfo();
        String urlPostMessage = chatConfiguration.getBaseUrl() + "/v1/chat.postMessage";
        httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Auth-Token", loginInfo.getAuthToken());
        httpHeaders.set("X-User-Id", loginInfo.getUserId());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        //
        PostMessageDTO dto = PostMessageDTO.builder()
                .alias("mStore")
                .roomId(roomId)
                .text(message)
                .build();

        HttpEntity<String> requestBody = new HttpEntity<>(new Gson().toJson(dto), httpHeaders);

        restTemplate.exchange(urlPostMessage, HttpMethod.POST, requestBody, String.class);
    }

    @Override
    public String getRoomID(String userName, String roomId) {
        if (roomId.isEmpty()) {
            if (userName.isEmpty()) {
                throw new CustomErrorException("Username is not null");
            }
            // Get info user
            if (getUserId(userName) != null) {
                throw new CustomErrorException("Username already exist");
            } else {
                //Create account
                Merchant merchant = new Merchant();
                merchant.setUserName(userName);
                merchant.setFullName(userName);
                try {
                    if (registerAccount(merchant) != null) {
                        return createDiscussion(0L, userName, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return roomId;
    }

    @Override
    public ChatLoginResponseDataDto getUserInfo() {
        return getLoginInfo();
    }

    @Override
    public HttpHeaders getHeaderLogin(ChatLoginResponseDataDto loginInfo) {
        httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Auth-Token", loginInfo.getAuthToken());
        httpHeaders.set("X-User-Id", loginInfo.getUserId());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    @Override
    public boolean existsChatAccount(String username) {
        ChatLoginResponseDataDto info = getLoginInfo();
        HttpHeaders headers = getHeaderLogin(info);
        try {
            ResponseEntity<Object> response = restTemplate.exchange(
                    UriComponentsBuilder.fromUriString(chatConfiguration.getBaseUrl() + "/v1/users.info")
                            .queryParam("username", username)
                            .toUriString(),
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    Object.class);
            return response.getStatusCode().is2xxSuccessful();
        } catch (Exception e) {
            log.info("Exception: {}", e);
            return false;
        }
    }


    private ChatLoginResponseDataDto getLoginInfo() {
        String urlLogin = chatConfiguration.getBaseUrl() + "/v1/login";
        httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("user", chatConfiguration.getUsername());
        body.add("password", chatConfiguration.getPassword());
        ResponseEntity<ChatLoginResponseDto> response = restTemplate
                .postForEntity(urlLogin,
                        new HttpEntity<>(body, httpHeaders), ChatLoginResponseDto.class);

        return response.getBody().getData();
    }

    private String getUserId(String userName) {
        ChatLoginResponseDataDto loginInfo = getLoginInfo();
        String urlUserInfo = chatConfiguration.getBaseUrl() + "/v1/users.info";
        httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Auth-Token", loginInfo.getAuthToken());
        httpHeaders.set("X-User-Id", loginInfo.getUserId());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(urlUserInfo)
                .queryParam("username", userName);
        HttpEntity<?> entity = new HttpEntity<>(httpHeaders);

        try {
            HttpEntity<User> userInfo = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    entity,
                    User.class);

            if (userInfo.getBody() != null && !userInfo.getBody().getUser().get_id().isEmpty()) {
                return userInfo.getBody().getUser().get_id();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
