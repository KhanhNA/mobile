package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Language;
import com.nextsolutions.dcommerce.model.loyalty.*;
import com.nextsolutions.dcommerce.repository.LoyaltyRepository;
import com.nextsolutions.dcommerce.service.LoyaltyService;
import com.nextsolutions.dcommerce.shared.dto.LoyaltyDto;
import com.nextsolutions.dcommerce.shared.dto.LoyaltyLevelDto;
import com.nextsolutions.dcommerce.shared.dto.LoyaltyMerchantTypeDto;
import com.nextsolutions.dcommerce.shared.dto.LoyaltyPackingSaleDto;
import com.nextsolutions.dcommerce.utils.CommonRuntimeData;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class LoyaltyServiceImpl extends CommonServiceImpl implements LoyaltyService {

    private final AppConfiguration appConfiguration;
    static final String LOYALTY_DESCRIPTION_IMAGES_FOLDER = "images/loyalty/descriptions";

    @Override
    public List<LoyaltyDto> findActive(Integer langId) throws Exception {

        List<LoyaltyDto> commonData = CommonRuntimeData.getLoyaltyDto(langId);
        if (commonData != null) {
            return commonData;
        }

        StringBuilder sb = new StringBuilder("select r from Loyalty r where r.fromDate <= curdate() and (r.toDate =0 or r.toDate >= CURDATE())");
        List<Loyalty> loyalties = Utils.safe(findAll(Loyalty.class, sb.toString(), null, 0));
        List<LoyaltyDto> loyaltyDtos = new ArrayList<>();

        //1 chuong trinh khuyen mai co 3 phan chinh: muc, sp ban, sp km
        List<LoyaltyLevel> ils = null;
        List<LoyaltyPackingSale> ipss = null;
        List<LoyaltyMerchantType> merchantTypes = null;

        List<LoyaltyLevelDto> ilDtos = null;
        List<LoyaltyPackingSaleDto> ipsDtos = null;
        List<LoyaltyMerchantTypeDto> imtDtos = null;

        LoyaltyDto dto;
        LoyaltyLevelDto ilDto;
        LoyaltyPackingSaleDto ipsDto;
        for (Loyalty loyalty : loyalties) {
            //chuyeen add loyalty vao list dto
//			dto = new LoyaltyDto();
            dto = Utils.getData(loyalty, LoyaltyDto.class);
            loyaltyDtos.add(dto);
            // lay ds loai dai ly ap dung chuong trinh khuyen mai nay
            merchantTypes = Utils.safe(loyalty.getMerchantTypes());
            imtDtos = new ArrayList<>();
            dto.merchantTypes = imtDtos;
            for (LoyaltyMerchantType mt : merchantTypes) {
//				LoyaltyMerchantTypeDto imtDto = new LoyaltyMerchantTypeDto();
                LoyaltyMerchantTypeDto imtDto = Utils.getData(mt, LoyaltyMerchantTypeDto.class);
                imtDtos.add(imtDto);
            }
            //1. lay ds Loyalty_level de chuyen vao dto
            ils = Utils.safe(loyalty.getLoyaltyLevels());

            ilDtos = new ArrayList<>();
            ipsDtos = new ArrayList<>();
            dto.setLoyaltyLevels(ilDtos);//set muc cho chuong trinh khuyen mai
            dto.setLoyaltyPackingSales(ipsDtos); //set sp ban cho ctkm

            for (LoyaltyLevel il : ils) {//duyet ds cac muc
//				ilDto = new LoyaltyLevelDto();
                ilDto = Utils.getData(il, LoyaltyLevelDto.class);
                ilDto.setLoyalty(dto);
                ilDtos.add(ilDto);
            }
            ipss = Utils.safe(loyalty.getLoyaltyPackingSales());

            for (LoyaltyPackingSale ips : ipss) {
//					ipsDto = new LoyaltyPackingSaleDto();
                ipsDto = Utils.getData(ips, LoyaltyPackingSaleDto.class);
                ipsDtos.add(ipsDto);
            }

        }
        CommonRuntimeData.storeLoyaltyDto(1, loyaltyDtos);
        return loyaltyDtos;

    }

    @Override
    public Integer activateProgram(Long loyaltyPgId) throws Exception {
        if (loyaltyPgId == null) {
            throw new CustomErrorException("loyaltyPgIdNull");
        }
        Loyalty loyalty = find(Loyalty.class, loyaltyPgId);
        if (loyalty == null) {
            throw new CustomErrorException("LoyaltyNotExist");
        }

        if (loyalty.getToDate() != null && loyalty.getToDate().isBefore(LocalDateTime.now())) {
            throw new CustomErrorException("ExpiredLoyaltyProgram");
        }

        loyalty.setStatus(Loyalty.STATUS_ACT);

        String sql = "select distinct packing_product_id, min(from_date) as sale_from_date, max(to_date) as sale_to_date " +
                " from ( " +
                " select packing_product_id, from_date, to_date  " +
                " from loyalty_packing_sale where OBJ_TYPE = 1 and LOYALTY_ID = ? " +
                " " +
                " union all  " +
                " select p.packing_product_id, from_date, to_date  " +
                " from loyalty_packing_sale s join loyalty_packing_group_packing p  " +
                "  on s.LOYALTY_PACKING_GROUP_ID = p.loyalty_packing_group_id " +
                " where OBJ_TYPE = 2 and s.loyalty_id = ? " +
                ") a  group by packing_product_id ";
        List<PackingLoyalty> packingIds = findAll(null, PackingLoyalty.class, sql, loyaltyPgId, loyaltyPgId);
        PackingLoyalty packingLoyalty;
        sql = "delete from PackingLoyalty where loyaltyId = :loyaltyId";
        entityManager.createQuery(sql)
                .setParameter("loyaltyId", loyaltyPgId)
                .executeUpdate();
        if (loyalty.getType() == Loyalty.TYPE_ORDER) {//KM theo don hang
            PackingLoyalty packing = new PackingLoyalty();
            packing.setLoyaltyId(loyaltyPgId);
            packing.setPackingProductId(-1L);
            packing.setFromDate(loyalty.getFromDate());
            packing.setToDate(loyalty.getToDate());
            packing.setSaleFromDate(loyalty.getFromDate());
            packing.setSaleToDate(loyalty.getToDate());
            packing.setLoyaltyType(loyalty.getType());
            save(packing);
            return 1;
        }
        for (PackingLoyalty packing : packingIds) {
            packingLoyalty = new PackingLoyalty();
            packingLoyalty.setLoyaltyId(loyaltyPgId);
            packingLoyalty.setPackingProductId(packing.getPackingProductId());
            packingLoyalty.setFromDate(loyalty.getFromDate());
            packingLoyalty.setToDate(loyalty.getToDate());
            packingLoyalty.setSaleFromDate(packing.getSaleFromDate());
            packingLoyalty.setSaleToDate(packing.getSaleToDate());
            packingLoyalty.setLoyaltyType(loyalty.getType());
            save(packingLoyalty);
        }
        return 1;
    }

    @Override
    public Integer deactivateProgram(Long loyaltyId) throws Exception {
        if (loyaltyId == null) {
            throw new CustomErrorException("loyaltyIdNull");
        }
        Loyalty loyalty = find(Loyalty.class, loyaltyId);
        if (loyalty == null) {
            throw new CustomErrorException("LoyaltyNotExist");
        }
        loyalty.setStatus(Loyalty.STATUS_DEACT);
        PackingLoyalty packingLoyalty;
        String sql = "delete from PackingLoyalty where loyaltyId = :loyaltyId";
        entityManager.createQuery(sql)
                .setParameter("loyaltyId", loyaltyId)
                .executeUpdate();

        return 1;
    }


    @Override
    public List<Loyalty> getLoyaltyFromPackingId(Long loyaltyId) throws Exception {
        String sql = "select * " +
                "from loyalty ip " +
                "where ip.id in( " +
                "select loyalty_id from loyalty_packing_sale " +
                " where OBJ_TYPE = 1 and PACKING_PRODUCT_ID = :packingId " +
                "		and from_date <= curdate() and (to_date is null or to_date >= curDate()) " +
                "union all " +
                "select s.loyalty_id " +
                "from loyalty_packing_sale s join loyalty_packing_group_packing p on s.loyalty_packing_group_id = p.id " +
                "where OBJ_TYPE = 2 and p.PACKING_PRODUCT_ID = :packingId1 and s.from_date >= curdate()" +
                " and (s.to_date is null or s.to_date >= curDate())" +
                ")";
        HashMap<String, Object> params = new HashMap<>();
        params.put("packingId", loyaltyId);
        params.put("packingId1", loyaltyId);
        return findAll(null, Loyalty.class, sql, params);
    }


    @Override
    public Loyalty insert(LoyaltyDto dto) throws Exception {
        Loyalty entity = null;

        if (dto.id == null) {
            entity = new Loyalty();
        } else {
            entity = find(Loyalty.class, dto.id);
            LocalDateTime fromDate = entity.getFromDate();
            if (fromDate != null) {
                if (!fromDate.equals(dto.getFromDate())) {
                    throw new CustomErrorException("Can'tChangeFromDate");
                }
            }

            LocalDateTime toDate = entity.getToDate();
            if (toDate != null) {
                if(toDate.isBefore(LocalDateTime.now())){
                    throw new CustomErrorException("ExpiredLoyalty");
                }
            }
        }

        entity = Utils.getData(dto, Loyalty.class);
//        entity.setId(null);

        entity.setStatus(Loyalty.STATUS_DRAFT);
        entity.setName(dto.getDescriptions().get("vi").getName());
        entity = save(entity);

        Iterator iterator = Utils.safe(dto.descriptions).entrySet().iterator();
        List<LoyaltyDescription> descriptions = new ArrayList<>();

        LoyaltyDescription description;
        HashMap<String, Language> langs = loadLanguages("");
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            description = (LoyaltyDescription) entry.getValue();
            description.setLoyaltyId(entity.getId());
            description.setLangCode((String) entry.getKey());
            description.setLangId(langs.get((String) entry.getKey()).getId());
            save(description);
        }

        //neu la khuyen mai don hang thi insert vao LoyaltyPackingGroup 1 bang ghi mac dinh khuyen mai don hang
        // insert vao LoyaltyPackingSale 1 ban ghi mac dinh khuyen mai theo don hang
        if (dto.getType() != 2) { //km don hang
            return entity;
        }
        LoyaltyPackingGroup loyaltyPackingGroup = findPackingGroup(entity);
        if (loyaltyPackingGroup == null) {
            loyaltyPackingGroup = new LoyaltyPackingGroup();
        }
        loyaltyPackingGroup.setIsAssign(1);
        loyaltyPackingGroup.setStatus(1);
        loyaltyPackingGroup.setCode("ORDER");
        loyaltyPackingGroup.setName("Promotion on order");
        loyaltyPackingGroup.setCreateDate(LocalDateTime.now());
        loyaltyPackingGroup.setFromDate(entity.getFromDate());
        loyaltyPackingGroup.setToDate(entity.getToDate());
        loyaltyPackingGroup.setCreateUser("thonv");
        loyaltyPackingGroup.setLoyaltyId(entity.getId());
        loyaltyPackingGroup = save(loyaltyPackingGroup);
        //LoyaltyPackingSale
        LoyaltyPackingSale loyaltyPackingSale = findPackingSale(entity);
        if (loyaltyPackingSale == null) {
            loyaltyPackingSale = new LoyaltyPackingSale();
        }
        loyaltyPackingSale.setFromDate(entity.getFromDate());
        loyaltyPackingSale.setToDate(entity.getToDate());
        loyaltyPackingSale.setStatus(1L);
        loyaltyPackingSale.setLoyaltyPackingGroup(loyaltyPackingGroup);
        loyaltyPackingSale.setLoyaltyId(entity.getId());
        loyaltyPackingSale.setObjType(3);//loai khuyen mai theo don hang
        loyaltyPackingSale.setCreateDate(LocalDateTime.now());
        loyaltyPackingSale.setCreateUser("thonv");
        save(loyaltyPackingSale);
        return entity;
    }

    @Override
    public Map<String, String> uploadFile(MultipartFile imageFile) throws IOException {
        Map<String, String> map = new HashMap<>();
        String originalFilename = imageFile.getOriginalFilename();
        String name = UUID.randomUUID().toString().replaceAll("-", "");
        String extension = Objects.requireNonNull(originalFilename).replaceAll("(.*)(\\..*)", "$2");
        BufferedImage defaultImage = ImageIO.read(imageFile.getInputStream());
        writeImage(defaultImage, extension.substring(1), name + "-default" + extension);
        map.put("default", this.generateImageUrl(LOYALTY_DESCRIPTION_IMAGES_FOLDER, name + "-default" + extension));
        return map;
    }

    private void writeImage(BufferedImage image, String type, String filename) throws IOException {
        File imageFile = new File(String.format("%s/%s/%s", appConfiguration.getUploadPath(), LOYALTY_DESCRIPTION_IMAGES_FOLDER, filename));
        FileUtils.forceMkdirParent(imageFile);
        ImageIO.write(image, type, imageFile);
    }

    private String generateImageUrl(String path, String filename) {
        return String.format("/%s/%s", path, filename);
    }

    private LoyaltyPackingGroup findPackingGroup(Loyalty loyalty) throws Exception {
        String sql = "from LoyaltyPackingGroup where status = 1 and loyaltyId = :loyaltyId";
        HashMap<String, Object> params = new HashMap<>();
        params.put("loyaltyId", loyalty.getId());
        List<LoyaltyPackingGroup> lst = findAll(sql, null, params, 0);
        if (lst == null || lst.size() == 0)
            return null;
        if (lst.size() > 1) {
            throw new CustomErrorException("LoyaltyPackingGroupError");
        }
        return lst.get(0);
    }

    private LoyaltyPackingSale findPackingSale(Loyalty loyalty) throws Exception {
        String sql = "from LoyaltyPackingSale where status = 1 and loyaltyId = :loyaltyId";

        HashMap<String, Object> params = new HashMap<>();
        params.put("loyaltyId", loyalty.getId());
        List<LoyaltyPackingSale> lst = findAll(sql, null, params, 0);
        if (lst == null || lst.size() == 0)
            return null;
        if (lst.size() > 1) {
            throw new CustomErrorException("LoyaltyPackingSaleError");
        }
        return lst.get(0);
    }
}
