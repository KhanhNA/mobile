package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.MerchantGroupDTO;
import com.nextsolutions.dcommerce.shared.dto.MerchantWareHouseDTO;

import java.util.List;

public interface WareHouseService {
    List<MerchantWareHouseDTO> getAll() throws Exception;

    List<MerchantGroupDTO> getAllByWarehouseCode(List<String> lstWarehouseCode) throws Exception;
}
