package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.SalesmanDto;

public interface SalesmanService {
    SalesmanDto createSalesman(SalesmanDto salesmanDto);
}
