package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.HunterDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.ui.model.request.MerchantQueryRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.HunterRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.HunterResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import java.util.List;

public interface HunterService {
    Page<HunterResource> findByQuery(HunterRequestModel hunterQuery, Pageable pageable);

    HunterDto requestCreateHunter(HunterDto hunterDto);

    Page<MerchantCrudResource> findMerchants(Long hunterId, MerchantDto merchantDto, Pageable pageable);

    String getMerchantToViewChange(Long id);

    HunterDto getHunterById(Long id);

    HunterDto requestUpdate(Long id, HunterDto hunterDto);

    HunterDto updateByManager(Long id, HunterDto hunterDto);

    Merchant createByManager(Long id);

    Merchant managerReject(Long id);

    Merchant requestDelete(Long id);

    Void deleteHunter(Long id, String userName, String token);

    Boolean existsByUsername(String userName);

    Boolean existsPhoneNumber(String phoneNumber);


    ResponseEntity exportHunter(HunterRequestModel hunterQuery, Pageable pageable);

    Boolean existsBankAccountNo(String bankAccountNo);

    Boolean existsUpdateBankNo(Long id,String bankAccountNo);

    Boolean existsWalletId(Long walletClientId);

    Boolean existsUpdateWalletId(Long id,Long walletClientId);

    List<HunterDto> getParentMerchant() throws Exception;

    Boolean existsContractNumber(String contractNumber);

    Boolean existsContractNumber(String contractNumber, Long id);

    Boolean existsIdentityNumber(String identityNumber);

    Boolean existsIdentityNumber(String identityNumber, Long id);
}
