package com.nextsolutions.dcommerce.service.impl;

import com.google.gson.Gson;
import com.nextsolutions.dcommerce.configuration.properties.OAuthConfiguration;
import com.nextsolutions.dcommerce.exception.AccountServiceException;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.UserEntity;
import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.service.AccountService;
import com.nextsolutions.dcommerce.shared.dto.BaseResponse;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.authenticator.SingleSignOn;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final RestTemplate restTemplate;
    private final OAuth2RestTemplate oAuth2RestTemplate;
    private final OAuthConfiguration oAuthConfiguration;
    private final MerchantJpaRepository merchantJpaRepository;

    @Override
    public void createAccount(UserEntity userEntity) {
        try {
            ResponseEntity<UserEntity> response = this.restTemplate.exchange(oAuthConfiguration.getUserUrl(),
                    HttpMethod.POST,
                    new HttpEntity<>(userEntity, generateOAuthHeaders()),
                    UserEntity.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                UserEntity userResponse = response.getBody();
                if (Objects.nonNull(userResponse)) {
                    userEntity.setId(userResponse.getId());
                }
            }
        } catch (HttpServerErrorException e) {
            String response = e.getResponseBodyAsString();
            Gson gson = new Gson();
            BaseResponse p = gson.fromJson(String.format(response, userEntity.getUsername()), BaseResponse.class);
            if (p != null && !StringUtils.isEmpty(p.getMessage())) {
                throw new AccountServiceException(p.getMessage());
            } else {
                throw new AccountServiceException("Server response error");
            }
        } catch (Exception e) {
            throw new AccountServiceException(e.getMessage());
        }
    }

    @Override
    public void changePassword(UserEntity userEntity, String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(token);
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(oAuthConfiguration.getUserPasswordChangingUrl())
                .queryParam("oldPassword", userEntity.getOldPassword())
                .queryParam("newPassword", userEntity.getNewPassword())
                .queryParam("newConfirmPassword", userEntity.getNewConfirmPassword()).build();
        HttpEntity<UserEntity> entity = new HttpEntity<>(httpHeaders);
        restTemplate.exchange(builder.toUriString(),
                HttpMethod.POST,
                entity,
                UserEntity.class);
    }

    @Override
    public void deactivateAccount(String username, String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", token);
        HttpEntity<Object> entity = new HttpEntity<>(username, httpHeaders);
        this.restTemplate.postForEntity(oAuthConfiguration.getUserDeactivationUrl(), entity, Object.class);
    }

    @Override
    public void deactivateAccount(String username) {
        OAuth2AuthenticationDetails details =
                (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(details.getTokenValue());
        HttpEntity<Object> entity = new HttpEntity<>(username, httpHeaders);
        this.restTemplate.postForEntity(oAuthConfiguration.getUserDeactivationUrl(), entity, Object.class);
    }

    @Override
    public boolean existsAccount(String username, String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", token);
        try {
            ResponseEntity<Object> response = this.restTemplate.exchange(oAuthConfiguration.getUserStatusUrl(),
                    HttpMethod.GET,
                    new HttpEntity<>(httpHeaders),
                    Object.class,
                    username);
            return response.getStatusCode().is2xxSuccessful(); // user exists
        } catch (Exception e) {
            return false; // user doesn't exists
        }
    }

    @Override
    public boolean existsAccount(String username) {
        OAuth2AuthenticationDetails details =
                (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(details.getTokenValue());
        try {
            ResponseEntity<Object> response = this.restTemplate.exchange(oAuthConfiguration.getUserStatusUrl(),
                    HttpMethod.GET,
                    new HttpEntity<>(httpHeaders),
                    Object.class,
                    username);
            return response.getStatusCode().is2xxSuccessful();
        } catch (Exception e) {
            return false; // user doesn't exists
        }
    }

    @Override
    public UserEntity findByUsername(String username) {
        OAuth2AuthenticationDetails details =
                (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(details.getTokenValue());
        ResponseEntity<UserEntity> response = this.restTemplate.exchange(oAuthConfiguration.getUserStatusUrl(),
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders),
                UserEntity.class,
                username);
        return response.getBody();
    }

    @Override
    public void activateAccount(String username) {
        OAuth2AuthenticationDetails details =
                (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(details.getTokenValue());
        HttpEntity<Object> entity = new HttpEntity<>(username, httpHeaders);
        this.restTemplate.postForEntity(oAuthConfiguration.getUserActivationUrl(), entity, Object.class);
    }

    private HttpHeaders generateOAuthHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(oAuth2RestTemplate.getAccessToken().getValue());
        headers.set("Accept-Language", LocaleContextHolder.getLocale().getLanguage());
        return headers;
    }


    @Override
    public ResponseEntity<Object> resetPassword(MerchantDto userName)
            throws Exception {
        Merchant merchant = merchantJpaRepository.findByUserName(userName.getUserName());
        if (merchant == null) {
            throw new CustomErrorException("merchant.not.exist");
        }
        try {
            ResponseEntity<UserEntity> response = this.restTemplate.exchange(oAuthConfiguration.getResetPasswordUrl(),
                    HttpMethod.POST,
                    new HttpEntity<>(userName.getUserName(), generateOAuthHeaders()),
                    UserEntity.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (HttpServerErrorException e) {
            String response = e.getResponseBodyAsString();
            Gson gson = new Gson();
            BaseResponse p = gson.fromJson(String.format(response, userName.getUserName()), BaseResponse.class);
            if (p != null && !StringUtils.isEmpty(p.getMessage())) {
                throw new AccountServiceException(p.getMessage());
            } else {
                throw new AccountServiceException("Server response error");
            }
        } catch (Exception e) {
            throw new AccountServiceException(e.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    }

}
