package com.nextsolutions.dcommerce.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class Translator {

    private static MessageSource messageSource;

    @Autowired
    Translator(MessageSource messageSource) {
        Translator.messageSource = messageSource;
    }

    public static String toLocales(String msgCode, String lang) {
        Locale locale = new Locale(lang);
        return messageSource.getMessage(msgCode, null, locale);
    }

    public static String toLocale(String msgCode, Object[] args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(msgCode, args, locale);
    }

    public static String toLocale(String msgCode, String... msg) {
        Locale locale = LocaleContextHolder.getLocale();
        return String.format(messageSource.getMessage(msgCode, null, locale), msg);
    }
}
