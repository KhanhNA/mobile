package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.model.AndroidCrashLog;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.service.CrashService;
import com.nextsolutions.dcommerce.shared.dto.AndroidCrashLogDto;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CrashServiceImpl extends  CommonServiceImpl implements CrashService, CommonService {


    @Override
    public void save(AndroidCrashLogDto dto) throws Exception {
        AndroidCrashLog log = Utils.getData(dto, AndroidCrashLog.class);
        save(log);
    }
}
