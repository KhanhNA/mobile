package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingGroup;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingGroupPacking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LoyaltyPackingGroupService  extends CommonService {
    void insertPackingGroup(LoyaltyPackingGroup dto) throws Exception;
    void savePackingGroupDetail(List<LoyaltyPackingGroupPacking> dtos,
                                Long pgId) throws Exception;
    void delPackingGrp(Long ictPgId) throws Exception;
    void delPackingGrpPacking(Long ictPgdId) throws Exception ;
    Page<PackingProduct> list(Pageable pageable, Long ictPackingGroupId) throws Exception;
}
