package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.OrderTemplate;
import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import com.nextsolutions.dcommerce.shared.dto.OrderTemplateDetailDTO;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderService extends CommonService {

    OrderV2Dto getOrders(Pageable pageable, OrderV2Dto orderDto, boolean hasPacking, Long langId) throws Exception;

    void changeOrder(OrderV2Dto orderDto) throws Exception;

    boolean changeOrderStatus(OrderDto orderDto) throws Exception;

    void deleteOrder(Long orderId) throws Exception;

    void deleteOrderTemplate(Long orderTemplateId) throws Exception;

    OrderTemplate editOrderTemplate(Long orderTemplateId, String name) throws Exception;

    Page<OrderTemplateDetailDTO> getOrderTemplateByMerchantId(Pageable pageable, Long merchantId, Long langId);
}
