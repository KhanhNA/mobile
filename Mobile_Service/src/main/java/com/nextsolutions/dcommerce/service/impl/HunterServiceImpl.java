
package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.configuration.properties.OAuthConfiguration;
import com.nextsolutions.dcommerce.model.AutoIncrement;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.Role;
import com.nextsolutions.dcommerce.model.UserEntity;
import com.nextsolutions.dcommerce.repository.AutoIncrementJpaRepository;
import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.service.AccountService;
import com.nextsolutions.dcommerce.service.HunterService;
import com.nextsolutions.dcommerce.service.Translator;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.constant.AutoIncrementType;
import com.nextsolutions.dcommerce.shared.dto.HunterChangeDto;
import com.nextsolutions.dcommerce.shared.dto.HunterDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.dto.SalemanExportDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.HunterResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.shared.mapper.HunterMapper;
import com.nextsolutions.dcommerce.ui.model.request.HunterRequestModel;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.*;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class HunterServiceImpl implements HunterService {

    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    private final CommonServiceImpl commonService;

    @Autowired
    private FileStorageService fileStorageService;

    private final MerchantJpaRepository merchantJpaRepository;
    private final AutoIncrementJpaRepository autoIncrementJpaRepository;
    private final AccountService accountService;
    private final HunterMapper hunterMapper;
    private final AppConfiguration appConfiguration;
    private final static String PARENT_PATH = "/images/salesman";
    private final OAuthConfiguration oAuthConfiguration;
    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public Page<HunterResource> findByQuery(HunterRequestModel hunterQuery, Pageable pageable) {
        return merchantJpaRepository.findByQuery(hunterQuery, pageable);
    }

    @Override
    @Transactional
    public HunterDto requestCreateHunter(HunterDto hunterDto) {
        AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.SALESMAN.value());
        Long code = Long.parseLong(autoIncrement.getValue()) + 1;
        Merchant hunter = new Merchant();

        hunter.setUuid(UUID.randomUUID().toString().replaceAll("-", ""));
        hunter.setMerchantTypeId(0L);
        hunter.setUserName(hunterDto.getUsername());
        hunter.setDateBirth(hunterDto.getBirthDate());
        hunter.setGender(hunterDto.getGender());
        hunter.setMobilePhone(hunterDto.getPhoneNumber());
        hunter.setDefaultBankAccountNo(hunterDto.getBankAccountNo());
        hunter.setWalletClientId(hunterDto.getWalletClientId());
        hunter.setStatus(hunterDto.getStatus());


        hunter.setParentMarchantId(hunterDto.getParentMarchantId());
        hunter.setContractNumber(hunterDto.getContractNumber());
        hunter.setCardType(hunterDto.getCardType());
        hunter.setFatherName(hunterDto.getFatherName());
        hunter.setReligion(hunterDto.getReligion());
        hunter.setIdentityCreateDate(hunterDto.getIdentityCreateDate());
        hunter.setIdentityNumber(hunterDto.getIdentityNumber());
        hunter.setHeight(hunterDto.getHeight());
        hunter.setFirstName(hunterDto.getFirstName());
        hunter.setLastName(hunterDto.getLastName());
        hunter.setBloodType(hunterDto.getBloodType());
        hunter.setSignificantFigure(hunterDto.getSignificantFigure());
        hunter.setIdentityImgFront(getImageFile(hunterDto.getIdentityImgFront()));
        hunter.setIdentityImgBack(getImageFile(hunterDto.getIdentityImgBack()));
        hunter.setFullName(hunterDto.getFirstName() + " " + hunterDto.getLastName());
        hunter.setMerchantCode(code.toString());
        hunter.setAction(3);
        hunter.setStatus(0);
        if (hunterDto.getStatus() != null && hunterDto.getStatus().equals(1)) {
            hunter.setActiveDate(LocalDateTime.now());
        }
        autoIncrement.setValue(code.toString());
        merchantJpaRepository.save(hunter);
        autoIncrementJpaRepository.save(autoIncrement);
        return hunterMapper.toHunterDto(hunter);
    }

    @Override
    public Page<MerchantCrudResource> findMerchants(Long hunterId, MerchantDto merchantDto, Pageable pageable) {
        return merchantJpaRepository.findMerchants(hunterId, merchantDto, pageable);
    }

    @Override
    public String getMerchantToViewChange(Long id) {
        StringBuilder selectJsonValue = new StringBuilder("Select value_change from merchant where MERCHANT_ID = :id");
        Query query = entityManager.createNativeQuery(selectJsonValue.toString());
        query.setParameter("id", id);
        String json = (String) query.getSingleResult();
        return json;
    }

    @Override
    public HunterDto getHunterById(Long id) {
        return merchantJpaRepository.findById(id)
                .map(hunterMapper::toHunterDto)
                .orElseThrow(() -> createEntityNotFoundException(id));
    }

    private String getImageFile(String image) {
        if (image.startsWith("data:")) {
            try {
                File x = new File(appConfiguration.getUploadPath() + PARENT_PATH);
                if (!x.exists()) {
                    FileUtils.forceMkdir(x);
                }
                image = image.replaceFirst("(data:.*;base64,)", "");
                byte[] decode = Base64.getMimeDecoder().decode(image);
                String type = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(decode));
                String subtype = MimeTypeUtils.parseMimeType(type).getSubtype();
                if (subtype.equals("x-wav")) {
                    subtype = "webp";
                }
                String filename = "/" + UUID.randomUUID().toString().replaceAll("-", "") + "." + subtype;
                try (OutputStream out = new FileOutputStream(appConfiguration.getUploadPath() + PARENT_PATH + filename)) {
                    out.write(decode);
                    return PARENT_PATH + filename;
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        } else {
            return image;
        }
        return null;
    }


    @Override
    public HunterDto requestUpdate(Long id, HunterDto hunterDto) {
        Merchant hunter = merchantJpaRepository.findById(id)
                .orElseThrow(() -> createEntityNotFoundException(id));
        HunterChangeDto hunterChangeDto = new HunterChangeDto();
        hunterChangeDto.setBirthDate(hunterDto.getBirthDate().toString());
        hunterChangeDto.setGender(hunterDto.getGender());
        hunterChangeDto.setUsername(hunterDto.getUsername());
        hunterChangeDto.setBankAccountNo(hunterDto.getBankAccountNo());
        hunterChangeDto.setWalletClientId(hunterDto.getWalletClientId());
        hunterChangeDto.setParentMarchantId(hunterDto.getParentMarchantId());
        hunterChangeDto.setContractNumber(hunterDto.getContractNumber());
        hunterChangeDto.setPhoneNumber(hunterDto.getPhoneNumber());
        hunterChangeDto.setCardType(hunterDto.getCardType());
        hunterChangeDto.setFatherName(hunterDto.getFatherName());
        hunterChangeDto.setReligion(hunterDto.getReligion());
        hunterChangeDto.setIdentityCreateDate(hunterDto.getIdentityCreateDate().toString());
        hunterChangeDto.setIdentityNumber(hunterDto.getIdentityNumber());
        hunterChangeDto.setHeight(hunterDto.getHeight());
        hunterChangeDto.setFirstName(hunterDto.getFirstName());
        hunterChangeDto.setLastName(hunterDto.getLastName());
        hunterChangeDto.setBloodType(hunterDto.getBloodType());
        hunterChangeDto.setSignificantFigure(hunterDto.getSignificantFigure());
        hunterChangeDto.setIdentityImgFront(getImageFile(hunterDto.getIdentityImgFront()));
        hunterChangeDto.setIdentityImgBack(getImageFile(hunterDto.getIdentityImgBack()));
        hunterChangeDto.setFullName(hunterDto.getFirstName() + hunterDto.getLastName());
        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonString = mapper.writeValueAsString(hunterChangeDto);
            hunter.setValueChange(jsonString);
            hunter.setAction(4);
        } catch (IOException e) {
            e.printStackTrace();
        }
        merchantJpaRepository.save(hunter);
        return hunterMapper.toHunterDto(hunter);
    }

    @Override
    public HunterDto updateByManager(Long id, HunterDto hunterDto) {
        Merchant hunter = merchantJpaRepository.findById(id)
                .orElseThrow(() -> createEntityNotFoundException(id));
        hunter.setUuid(UUID.randomUUID().toString().replaceAll("-", ""));
        hunter.setDateBirth(hunterDto.getBirthDate());
        hunter.setGender(hunterDto.getGender());
        hunter.setDefaultBankAccountNo(hunterDto.getBankAccountNo());
        hunter.setWalletClientId(hunterDto.getWalletClientId());
        hunter.setParentMarchantId(hunterDto.getParentMarchantId());
        hunter.setMobilePhone(hunterDto.getPhoneNumber());
        hunter.setContractNumber(hunterDto.getContractNumber());
        hunter.setCardType(hunterDto.getCardType());
        hunter.setFatherName(hunterDto.getFatherName());
        hunter.setReligion(hunterDto.getReligion());
        hunter.setIdentityCreateDate(hunterDto.getIdentityCreateDate());
        hunter.setIdentityNumber(hunterDto.getIdentityNumber());
        hunter.setHeight(hunterDto.getHeight());
        hunter.setFirstName(hunterDto.getFirstName());
        hunter.setLastName(hunterDto.getLastName());
        hunter.setBloodType(hunterDto.getBloodType());
        hunter.setSignificantFigure(hunterDto.getSignificantFigure());
        hunter.setIdentityImgFront(getImageFile(hunterDto.getIdentityImgFront()));
        hunter.setIdentityImgBack(getImageFile(hunterDto.getIdentityImgBack()));
        hunter.setFullName(hunterDto.getFirstName() + hunterDto.getLastName());
        hunter.setAction(0);
        merchantJpaRepository.save(hunter);
        return hunterMapper.toHunterDto(hunter);
    }

    @Override
    public Merchant createByManager(Long id) {
        List<Role> lstRole = new ArrayList<>();
        oAuthConfiguration.getSalesManRoles().forEach(roleId -> {
            lstRole.add(new Role(roleId));
        });

        Merchant hunter = merchantJpaRepository.findById(id)
                .orElseThrow(() -> createEntityNotFoundException(id));
        hunter.setAction(0);
        hunter.setStatus(1);
        merchantJpaRepository.save(hunter);
        UserEntity userEntity = UserEntity
                .builder()
                .username(hunter.getUserName())
                .firstName(hunter.getFirstName())
                .lastName(hunter.getLastName())
                .enabled(true)
                .roles(lstRole)
                .build();
        accountService.createAccount(userEntity);
        return hunter;
    }

    @Override
    public Merchant managerReject(Long id) {
        Merchant hunter = merchantJpaRepository.findById(id)
                .orElseThrow(() -> createEntityNotFoundException(id));
        Integer action = hunter.getAction();
        if (action == 2 || action == 4) {
            hunter.setAction(0);
        } else if (action == 3) {
            hunter.setStatus(-1);
            hunter.setAction(0);
        }
        merchantJpaRepository.save(hunter);
        return hunter;
    }

    @Override
    public Merchant requestDelete(Long id) {
        Merchant hunter = merchantJpaRepository.findById(id)
                .orElseThrow(() -> createEntityNotFoundException(id));
        hunter.setAction(2);
        merchantJpaRepository.save(hunter);
        return hunter;
    }

    @Override
    @Transactional
    public Void deleteHunter(Long id, String userName, String token) {
        accountService.deactivateAccount(userName, token);
        merchantJpaRepository.deactivate(id, userName);
        return null;
    }

    @Override
    public Boolean existsByUsername(String userName) {
        Merchant merchant = merchantJpaRepository.findByUserName(userName);
        if (Objects.nonNull(merchant)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean existsPhoneNumber(String phoneNumber) {
        Optional<Merchant> merchant = merchantJpaRepository.findByMobilePhone(phoneNumber);
        if (merchant.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public ResponseEntity<Object> exportHunter(HunterRequestModel hunterQuery, Pageable pageable) {
        try {
//            ClassPathResource classPathResource = new ClassPathResource("template/SalemanTemplate_" + LocaleContextHolder.getLocale().getLanguage() + ".xlsx");
            ClassLoader cl = this.getClass().getClassLoader();
            InputStream inputStream = cl.getResourceAsStream("template/SalemanTemplate_" + LocaleContextHolder.getLocale().getLanguage() + ".xlsx");
            Workbook workbook = new XSSFWorkbook(inputStream);
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            Collection<SalemanExportDTO> listExportData = this.merchantJpaRepository.findSalemanExportData(hunterQuery, pageable);

            if (listExportData == null || listExportData.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                int coll, lastColl = 11;
                int curRow = 1;

                Sheet sheet = workbook.getSheetAt(0);
                CellStyle textStyle = workbook.createCellStyle();
                textStyle.setBorderBottom(BorderStyle.THIN);
                textStyle.setBorderTop(BorderStyle.THIN);
                textStyle.setBorderRight(BorderStyle.THIN);
                textStyle.setBorderLeft(BorderStyle.THIN);
                textStyle.setAlignment(HorizontalAlignment.LEFT);
                textStyle.setVerticalAlignment(VerticalAlignment.CENTER);

                CellStyle numberStyle = workbook.createCellStyle();
                numberStyle.setBorderBottom(BorderStyle.THIN);
                numberStyle.setBorderTop(BorderStyle.THIN);
                numberStyle.setBorderRight(BorderStyle.THIN);
                numberStyle.setBorderLeft(BorderStyle.THIN);
                numberStyle.setAlignment(HorizontalAlignment.CENTER);
                numberStyle.setVerticalAlignment(VerticalAlignment.CENTER);

                for (SalemanExportDTO obj : listExportData) {
                    coll = 0;
                    Row row = sheet.createRow(curRow++);
                    Cell cells[] = new Cell[lastColl + 1];
                    // Mã
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getCode());
                    cells[coll].setCellStyle(numberStyle);
                    coll++;
                    // Ten tai khoan
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getUsername());
                    cells[coll].setCellStyle(textStyle);
                    coll++;
                    // Ten day du
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getFullName());
                    cells[coll].setCellStyle(textStyle);
                    coll++;
                    //SDT
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getPhoneNumber());
                    cells[coll].setCellStyle(numberStyle);
                    coll++;
                    // Dia chi
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getAddress());
                    cells[coll].setCellStyle(textStyle);
                    coll++;
                    // Ngay dang ky
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getRegisterDate());
                    cells[coll].setCellStyle(numberStyle);
                    coll++;
                    // Tinh trang
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getStatus().equals(1) ?
                            Translator.toLocale("saleman.status.active") : Translator.toLocale("saleman.status.pending"));
                    cells[coll].setCellStyle(textStyle);
                    coll++;
                    // Nguoi phu trach
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getParent());
                    cells[coll].setCellStyle(textStyle);
                    coll++;
                    // CMND
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getIdNumber());
                    cells[coll].setCellStyle(numberStyle);
                    coll++;
                    // Ma hop dong
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getContractNumber());
                    cells[coll].setCellStyle(textStyle);
                    coll++;
                    // Tai khoan ngan hang
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getBankAccountNo());
                    cells[coll].setCellStyle(numberStyle);
                    coll++;
                    // Tai khoan vi
                    cells[coll] = row.createCell(coll);
                    cells[coll].setCellValue(obj.getClientWalletId());
                    cells[coll].setCellStyle(numberStyle);
                }
            }
            workbook.write(out);
            Resource result = new ByteArrayResource(out.toByteArray());

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            httpHeaders.add("Content-Disposition", "attachment; filename=SalemanExport.xlsx");
            return new ResponseEntity<>(result, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public Boolean existsBankAccountNo(String bankAccountNo) {
        Optional<Merchant> merchant = merchantJpaRepository.findByDefaultBankAccountNo(bankAccountNo);
        if (merchant.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean existsUpdateBankNo(Long id, String bankAccountNo) {
        return merchantJpaRepository.findByDefaultBankAccountNo(bankAccountNo)
                .map(distributor -> !distributor.getMerchantId().equals(id))
                .orElseThrow(javax.persistence.EntityNotFoundException::new);
    }

    @Override
    public Boolean existsWalletId(Long walletClientId) {
        Optional<Merchant> merchant = merchantJpaRepository.findByWalletClientId(walletClientId);
        if (merchant.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean existsUpdateWalletId(Long id, Long walletClientId) {
        return merchantJpaRepository.findByWalletClientId(walletClientId)
                .map(distributor -> !distributor.getMerchantId().equals(id))
                .orElseThrow(javax.persistence.EntityNotFoundException::new);

    }

    @Override
    public List<HunterDto> getParentMerchant() throws Exception {
        StringBuilder select = new StringBuilder();
        select.append("Select full_name fullName, merchant_id hunterId from merchant where MERCHANT_TYPE_ID = 3");
        List<HunterDto> listParentMerchant = commonService.findAll(null, HunterDto.class, select.toString(), new HashMap());

        return listParentMerchant;
    }

    @Override
    public Boolean existsContractNumber(String contractNumber) {
        Optional<Merchant> merchant = merchantJpaRepository.findByContractNumber(contractNumber);
        return merchant.isPresent();
    }

    @Override
    public Boolean existsContractNumber(String contractNumber, Long id) {
        return merchantJpaRepository.findByContractNumber(contractNumber)
                .map(distributor -> !distributor.getMerchantId().equals(id))
                .orElseThrow(javax.persistence.EntityNotFoundException::new);
    }

    @Override
    public Boolean existsIdentityNumber(String identityNumber) {
        Optional<Merchant> merchant = merchantJpaRepository.findByIdentityNumber(identityNumber);
        return merchant.isPresent();
    }

    @Override
    public Boolean existsIdentityNumber(String identityNumber, Long id) {
        return merchantJpaRepository.findByIdentityNumber(identityNumber)
                .map(distributor -> !distributor.getMerchantId().equals(id))
                .orElseThrow(javax.persistence.EntityNotFoundException::new);
    }

    private EntityNotFoundException createEntityNotFoundException(Long id) {
        return new EntityNotFoundException("Hunter with id " + id + " doesn't exist.");
    }

}
