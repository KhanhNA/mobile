package com.nextsolutions.dcommerce.service;


import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyLevel;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingSale;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LoyaltyPackingSaleService extends CommonService {
    void savePackingSale(List<LoyaltyPackingSale> loyaltyPackingSales, Long loyaltyPrgId, Integer type) throws Exception;

    void saveLoyaltyLvl(List<LoyaltyLevel> dtos, Long sId) throws Exception;

    void delLoyaltyLvl(Long id) throws Exception;

    void delLoyaltyPackingSale(Long id) throws Exception;

    Page<LoyaltyPackingSale> getLoyaltyPackingSaleList(Class clazz, Long loyaltyId, Long status, Pageable pageable) throws Exception;

    Page<PackingProduct> list(Pageable pageable, List<Long> exceptIds, String text) throws Exception;

    void saveLoyaltyLvlForOrderValue(List<LoyaltyLevel> dtos, Long loyaltyId) throws Exception;

    Page<LoyaltyLevel> getLoyaltyLvlList(Long loyaltyId, Long parseLong, Pageable pageable);
}
