package com.nextsolutions.dcommerce.service;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BaseService<T, ID extends Serializable> {

	Page<T> findAll(Pageable pageable);

	T get(ID id);

	void save(T object);

	void delete(ID id);

}
