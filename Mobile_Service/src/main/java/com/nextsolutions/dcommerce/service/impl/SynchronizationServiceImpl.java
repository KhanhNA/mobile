package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.repository.custom.SynchronizationRepositoryCustom;
import com.nextsolutions.dcommerce.service.SynchronizationService;
import com.nextsolutions.dcommerce.ui.model.request.SynchronizationResource;
import com.nextsolutions.dcommerce.service.exception.UpdatingIdsFailureException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class SynchronizationServiceImpl implements SynchronizationService {

    private final SynchronizationRepositoryCustom synchronizationRepositoryCustom;

    @Override
    public Map<String, List<Long>> synchronize(List<SynchronizationResource> resources) throws ClassNotFoundException {
        Map<String, List<Long>> entitySynchronizingFailure = new HashMap<>();
        for (SynchronizationResource resource : resources) {
            try {
                synchronizationRepositoryCustom.synchronize(resource.getEntityName(), resource.getIds());
            } catch (UpdatingIdsFailureException e) {
                entitySynchronizingFailure.put(e.getEntityName(), e.getIds());
            }
        }
        return entitySynchronizingFailure;
    }
}
