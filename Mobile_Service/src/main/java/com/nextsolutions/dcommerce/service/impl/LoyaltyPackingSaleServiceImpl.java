package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.loyalty.*;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyLevel;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingSale;
import com.nextsolutions.dcommerce.service.LoyaltyPackingSaleService;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
public class LoyaltyPackingSaleServiceImpl extends CommonServiceImpl implements LoyaltyPackingSaleService {

    @Override
    public void savePackingSale(List<LoyaltyPackingSale> loyaltyPackingSales, Long loyaltyPrgId, Integer type) throws Exception {
        if (loyaltyPrgId == null) {
            throw new CustomErrorException("LoyaltyIdNull");
        }
        Loyalty loyaltyPrg = find(Loyalty.class, loyaltyPrgId);
        if (loyaltyPrg == null) {
            throw new CustomErrorException("LoyaltyNotFound");
        }
        if (type == null) {
            throw new CustomErrorException("GroupTypeNull");
        }
        LoyaltyPackingSale loyaltyPS;
        Object entity;
        String sqlCond = "";
        HashMap<String, Object> params = new HashMap<>();
        for (LoyaltyPackingSale loyaltyPackingSale : loyaltyPackingSales) {
            entity = null;
            sqlCond = "";
            params.clear();
            loyaltyPS = null;
            params.put("loyaltyPrgId", loyaltyPrgId);
            if (type == 1) {//packing
                entity = find(PackingProduct.class, loyaltyPackingSale.getPackingProductId());
                sqlCond = " packingProductId = :packingProductId and obj_type = 1";
                params.put("packingProductId", ((PackingProduct) entity).getPackingProductId());
            } else if (type == 2) {
                entity = find(LoyaltyPackingGroup.class, loyaltyPackingSale.getLoyaltyPackingGroupId());
                sqlCond = " loyaltyPackingGroupId = :loyaltyPackingGroupId and obj_type = 2";
                params.put("loyaltyPackingGroupId", ((LoyaltyPackingGroup) entity).getId());
            }
            if (entity == null) {
                throw new CustomErrorException("idNotExist");
            }
            //tim
            List<LoyaltyPackingSale> tmp = findAll(LoyaltyPackingSale.class,
                    "from LoyaltyPackingSale " +
                            " where loyaltyId = :loyaltyPrgId " +
                            "       and " + sqlCond, params, 1);
            if (tmp == null || tmp.size() == 0) {
                loyaltyPS = new LoyaltyPackingSale();
            } else if (tmp.size() == 1) {
                loyaltyPS = tmp.get(0);
            } else if (tmp.size() > 1) {
                for (LoyaltyPackingSale i : tmp) {
                    remove(i);
                }
                loyaltyPS = new LoyaltyPackingSale();
            }

            loyaltyPS.setLoyaltyId(loyaltyPrgId);
            if (type == 1) {
                loyaltyPS.setPackingProduct((PackingProduct) entity);
            } else {
                loyaltyPS.setLoyaltyPackingGroup((LoyaltyPackingGroup) entity);
                ((LoyaltyPackingGroup) entity).setIsAssign(1);
                save(entity);
            }
            loyaltyPS.setObjType(type);
            loyaltyPS.setStatus(1L);
            loyaltyPS.setFromDate(loyaltyPackingSale.getFromDate());
            loyaltyPS.setToDate(loyaltyPackingSale.getToDate());

            save(loyaltyPS);
        }
    }

    @Override
    public void saveLoyaltyLvl(List<LoyaltyLevel> dtos, Long sId) throws Exception {
        if (sId == null) {
            throw new CustomErrorException("PackingSaleNull");
        }
        LoyaltyPackingSale loyaltyPackingSale = find(LoyaltyPackingSale.class, sId);
        if (loyaltyPackingSale == null) {
            throw new CustomErrorException("PackingSaleNotExists");
        }
        dtos = Utils.safe(dtos);
        LoyaltyLevel entity;
        Long id;
        for (LoyaltyLevel loyaltyLevel : dtos) {
            entity = loyaltyLevel;

            id = loyaltyLevel.getId();
            if (id != null) {
                entity = find(LoyaltyLevel.class, id);
                if (entity == null) {
                    throw new CustomErrorException("LoyaltyLevelNotExists");
                }
                entity = Utils.getData(loyaltyLevel, entity);
            }
            entity.setLoyaltyPackingSaleId(sId);
            entity.setHasLoyaltyPacking(0);
            entity.setStatus(1);
            entity.setLoyaltyId(loyaltyPackingSale.getLoyaltyId());
//            entity.setPackingProduct_loyaltyLvl(loyaltyPackingSale.getPackingProduct());
            entity.setLoyaltyPackingGroupId(loyaltyPackingSale.getLoyaltyPackingGroupId());
            entity.setObjType(loyaltyPackingSale.getObjType());
            save(entity);
        }
    }

    @Override
    public void saveLoyaltyLvlForOrderValue(List<LoyaltyLevel> dtos, Long loyaltyId) throws Exception {
        if (loyaltyId == null) {
            throw new CustomErrorException("LoyaltyIdNull");
        }

        Loyalty loyalty = find(Loyalty.class, loyaltyId);
        if (loyalty == null) {
            throw new CustomErrorException("LoyaltyNotExist");
        }

        dtos = Utils.safe(dtos);
        LoyaltyLevel entity;
        Long id;
        for (LoyaltyLevel loyaltyLevel : dtos) {
            entity = loyaltyLevel;
            id = loyaltyLevel.getId();
            if (id != null) {
                entity = find(LoyaltyLevel.class, id);
                if (entity == null) throw new CustomErrorException("LoyaltyLevelNotExist");

                entity = Utils.getData(loyaltyLevel, entity);
            }
            entity.setStatus(1);
            entity.setMinQuantity(null);
            entity.setLoyaltyId(loyaltyId);
            entity.setLoyaltyPackingSaleId(null);
            entity.setHasLoyaltyPacking(0);
//            entity.setPackingProduct_loyaltyLvl(null);
            entity.setLoyaltyPackingGroupId(null);
            entity.setObjType(null);
            save(entity);
        }

    }


    @Override
    public void delLoyaltyLvl(Long id) throws Exception {
        LoyaltyLevel entity;
        if (id == null) {
            throw new CustomErrorException("IdNull");
        }
        entity = find(LoyaltyLevel.class, id);
        if (entity == null) {
            throw new CustomErrorException("LoyalLevelNotExists");
        }
        entity.setStatus(0);
        save(entity);

    }

    @Override
    public void delLoyaltyPackingSale(Long id) throws Exception {
        LoyaltyPackingSale entity;
        if (id == null) {
            throw new CustomErrorException("IdNull");
        }
        entity = find(LoyaltyPackingSale.class, id);
        if (entity == null) {
            throw new CustomErrorException("LoyaltyPackingSaleNotExists");
        }
        entity.setStatus(0L);
        entity.setToDate(LocalDateTime.now());
        if (entity.getObjType() == 2) {//group
            entity.getLoyaltyPackingGroup().setIsAssign(0);
            save(entity.getLoyaltyPackingGroup());
        }
        save(entity);
    }

    @Override
    public Page<LoyaltyPackingSale> getLoyaltyPackingSaleList(Class clazz, Long loyaltyId, Long status, Pageable pageable) throws Exception {
        String sql = " SELECT *" +
                " FROM loyalty_packing_sale " +
                " WHERE (1=1) AND loyalty_id=:loyaltyId " +
                " AND status=:status ";

        if (clazz.equals(LoyaltyPackingGroup.class)) {
            sql = sql + " AND loyalty_packing_group_id is NOT NULL ";
        } else if (clazz.equals(PackingProduct.class)) {
            sql = sql + " AND packing_product_id is NOT NULL ";
        }

        String sqlCount = String.format("select count(*) from (%s) a ", sql);

        Query query = entityManager.createNativeQuery(sql, LoyaltyPackingSale.class);
        Query queryCount = entityManager.createNativeQuery(sqlCount);

        query.setParameter("loyaltyId", loyaltyId);
        query.setParameter("status", status);
        queryCount.setParameter("loyaltyId", loyaltyId);
        queryCount.setParameter("status", status);

        BigInteger count = (BigInteger) queryCount.getSingleResult();

        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<LoyaltyPackingSale> list = query.getResultList();

        return new PageImpl<>(list, pageable, count.intValue());
    }

    @Override
    public Page<LoyaltyLevel> getLoyaltyLvlList(Long loyaltyId, Long status, Pageable pageable) {

        String sql = "   SELECT *" +
                " FROM loyalty_level " +
                " WHERE (1=1) AND loyalty_id=:loyaltyId " +
                " AND status=:status " +
                " AND loyalty_packing_group_id is NULL " +
                " AND packing_product_id is NULL ";
        String sqlCount = String.format(" SELECT COUNT(*) FROM (%s) a ", sql);
        Query query = entityManager.createNativeQuery(sql, LoyaltyLevel.class);
        Query queryCount = entityManager.createNativeQuery(sqlCount);

        query.setParameter("loyaltyId", loyaltyId);
        query.setParameter("status", status);
        queryCount.setParameter("loyaltyId", loyaltyId);
        queryCount.setParameter("status", status);

        BigInteger count = (BigInteger) queryCount.getSingleResult();

        int firstResult = pageable.getPageSize() * pageable.getPageNumber();
        int maxResult = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);

        List<LoyaltyLevel> list = query.getResultList();

        return new PageImpl<>(list, pageable, count.intValue());
    }


    @Override
    public Page<PackingProduct> list(Pageable pageable, List<Long> exceptIds, String text) throws Exception {
        String sql = "select a.* " +
                " from packing_product a " +
                " where (a.PACKING_PRODUCT_ID not in :exceptIds)";
        if (!Utils.isEmpty(text)) {
            sql = sql + " and (a.code like concat('%',:text,'%') or (a.name like concat('%',:text,'%')))";

        }

        String sqlCount = String.format("select count(*) from (%s) a ", sql);

        Query query = entityManager.createNativeQuery(sql, PackingProduct.class);
        Query queryCount = entityManager.createNativeQuery(sqlCount);

        query.setParameter("exceptIds", exceptIds);
        queryCount.setParameter("exceptIds", exceptIds);

        if (!Utils.isEmpty(text)) {
            query.setParameter("text", text);
            queryCount.setParameter("text", text);
        }

        BigInteger count = (BigInteger) queryCount.getSingleResult();
//        params.put("exceptIds", exceptIds);
        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<PackingProduct> packingProducts = query.getResultList();
        return new PageImpl<>(packingProducts, pageable, count.intValue());
    }


}
