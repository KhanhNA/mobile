package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.FileConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.service.MerchantService;
import com.nextsolutions.dcommerce.shared.dto.AttributeDescriptionDTO;
import com.nextsolutions.dcommerce.shared.dto.AttributeValueDescriptionDTO;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantOnMapDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
public class MerchantServiceImpl extends CommonServiceImpl implements MerchantService {
    @Autowired
    FileConfiguration fileConfiguration;

    @Override
    public Page<MerchantDto> findAll(Pageable pageable, MerchantDto dto, Integer isPage) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        String sql = " from Merchant m where (1=1) ";
        StringBuilder where = Utils.buildWhere(dto, "m", params);
        sql = sql + where.toString() + " order  by create_date  desc ";

        Pageable pageable1 = pageable;
        if (isPage == null || isPage != 1) {
            pageable1 = null;
        }
        List<Merchant> merchants = Utils.safe(findAll(sql, pageable1, params, 0));
        List<MerchantDto> merchantDtos = new ArrayList<>();
        MerchantDto merchantDto;
        for (Merchant merchant : merchants) {
            merchantDto = Utils.getData(merchant, MerchantDto.class);
            merchantDto.password = null;
            merchantDto.merchantImgUrl = !StringUtils.isEmpty(merchant.getMerchantImgUrl()) ? fileConfiguration.getUrlImage() + merchant.getMerchantImgUrl() : "";
            merchantDtos.add(merchantDto);
        }
        Integer count;
        String sqlCount = "select count(m) from Merchant m where (1=1) " + where.toString();
        count = getRowCount(sqlCount, params);
        return new PageImpl<>(merchantDtos, pageable, count);
    }

    @Override
    public void register(MerchantDto dto) throws Exception {
        String error = validateData(dto, Action.INSERT);
        if (!Utils.isEmpty(error)) {
            throw new CustomErrorException(CustomErrorException.USER_ERROR, error);
        }
        String activeCode = "";
        activeCode = Utils.genRandomCode();
        dto.setActiveCode(activeCode);
        while (!Utils.isEmpty(validateData(dto, Action.ACTIVE_CODE))) {
            activeCode = Utils.genRandomCode();
            dto.setActiveCode(activeCode);
        }
        dto.registerDate = Utils.getLocalDatetime();
        dto.merchantCode = dto.mobilePhone;
        dto.userName = dto.mobilePhone;
        dto.merchantTypeId = 2L; //fix codeget
        dto.activeStatus = 0;
        dto.status = 2; //chi bang 0 khi hủy

        Merchant merchant = Utils.getData(dto, Merchant.class);


        save(merchant);

    }

    @Override
    public void active(MerchantDto dto) throws Exception {
        String error = validateData(dto, Action.CHANGE);
        if (!Utils.isEmpty(error)) {
            throw new CustomErrorException(error);
        }
        Merchant merchant = find(Merchant.class, dto.merchantId);
        if (Utils.isEmpty(merchant.getActiveCode()) || !merchant.getActiveCode().equals(dto.activeCode)) {
            throw new CustomErrorException("activeCodeWrong");
        }
        merchant.setActiveDate(Utils.getLocalDatetime());
        merchant.setActiveStatus(1);


        save(merchant);

    }

    @Override
    public void deactive(MerchantDto dto) throws Exception {
        String error = validateData(dto, Action.CHANGE);
        if (!Utils.isEmpty(error)) {
            throw new CustomErrorException(error);
        }
        Merchant merchant = find(Merchant.class, dto.merchantId);
        merchant.setStatus(0);
        merchant.setDeactiveDate(Utils.getLocalDatetime());
        merchant.setActiveStatus(2);//deactive

        save(merchant);

    }

    @Override
    public String validateData(Object obj, Action actionType) {
        String error;

        if (obj == null || !(obj instanceof MerchantDto)) {
            return "invalideDto";
        }
        MerchantDto dto = (MerchantDto) obj;
        if (Action.CHANGE == actionType) {
            if (dto.merchantId == null) {
                return "merchantNull";
            }
        }

        if (Action.INSERT == actionType) {
            if (dto.parentMarchantId == null) {
                return "merchantNull";
            }
            if (Utils.isEmpty(dto.mobilePhone)) {
                return "mobilePhoneNull";
            }
            String sql = "from Merchant m where m.mobilePhone=:mobilePhone";
            HashMap<String, Object> params = new HashMap<>();
            params.put("mobilePhone", dto.mobilePhone);
            List<Merchant> merchants = Utils.safe(findAll(Merchant.class, sql, params, 1));
            if (!merchants.isEmpty()) {
                return "merchantExists";
            }
        }
        if (Action.ACTIVE_CODE == actionType) {
            String sql = "from Merchant m where m.activeCode=:activeCode  and status = 1";
            HashMap<String, Object> params = new HashMap<>();
            params.put("activeCode", dto.activeCode);
            List<Merchant> merchants = Utils.safe(findAll(Merchant.class, sql, params, 1));
            if (!merchants.isEmpty()) {
                return "merchantExists";
            }
        }

        return null;
    }

    public Page<Merchant> listMerchant(Pageable pageable, List<Long> exceptIds, String text) throws Exception {
        String sql = "select a.* " +
                " from merchant a " +
                " where a.MERCHANT_ID not in :exceptIds";

        if (!Utils.isEmpty(text)) {
            sql = sql + " and (a.MERCHANT_code like concat('%',:text,'%') or (a.MERCHANT_name like concat('%',:text,'%')))";

        }

        Query query = entityManager.createNativeQuery(sql, Merchant.class);
        query.setParameter("exceptIds", exceptIds);
        String sqlCount = String.format("select count(*) from (%s) a ", sql);

        Query queryCount = entityManager.createNativeQuery(sqlCount);
        queryCount.setParameter("exceptIds", exceptIds);

        if (!Utils.isEmpty(text)) {
            query.setParameter("text", text);
            queryCount.setParameter("text", text);
        }

        BigInteger count = (BigInteger) queryCount.getSingleResult();

        int firstResult = pageable.getPageNumber() * pageable.getPageSize();
        int maxResults = pageable.getPageSize();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        List<Merchant> packingProducts = query.getResultList();
        return new PageImpl<>(packingProducts, pageable, count.intValue());
    }

    @Override
    public void deleteToken(Long id, String token) {
        entityManager.createNativeQuery("delete from token_fb_merchant where merchant_id = :merchantId and token =:token")
                .setParameter("merchantId", id)
                .setParameter("token", token)
                .executeUpdate();
    }

    @Override
    public ResponseEntity<Object> createL1(MerchantDto merchantDto, MultipartFile[] multipartFiles) throws Exception {
        return null;
    }

    @Override
    public List<AttributeDescriptionDTO> getAttribute(Long merchantId, Long langId) throws Exception {
        String sqlAttribute = "SELECT attd.*,att.required FROM attribute att " +
                " JOIN attribute_description attd ON att.id = attd.attribute_id " +
                " where att.type in (1,3) and att.status =1 and attd.language_id = :langId";

        String sqlValue = " SELECT av.id,avd.name, " +
                " case " +
                " when mae.att_value_id is null then 0 " +
                " when mae.att_value_id <> avd.attribute_value_id " +
                " then 0 else 1 " +
                " END isSelect " +
                "FROM " +
                " attribute_value av " +
                " JOIN attribute_value_description avd ON av.id = avd.attribute_value_id AND avd.language_id = :langId " +
                " LEFT JOIN merchant_attribute_extent mae on mae.att_value_id = av.id and mae.merchant_id = :merchantId" +
                " where av.attribute_id = :attId ";
        List<AttributeDescriptionDTO> lstAttribute = findAll(null, AttributeDescriptionDTO.class, sqlAttribute, langId);
        if (lstAttribute != null && lstAttribute.size() > 0) {
            for (AttributeDescriptionDTO dto : lstAttribute) {
                try {
                    List<AttributeValueDescriptionDTO> values;
                    values = findAll(null, AttributeValueDescriptionDTO.class, sqlValue, langId, merchantId, dto.getAttributeId());
                    if (values != null && values.size() > 0)
                        dto.setLstValue(values);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return lstAttribute;
    }

    @Override
    public Merchant findByUserName(String userName, Long langId) throws Exception {
        HashMap<String, Object> param = new HashMap<>();
        String sql = "select * from merchant where user_name = :userName";
        param.put("userName", userName);
        Merchant merchant = findOne(Merchant.class, sql, param);
        if (merchant != null && merchant.getMerchantId() != null) {
            merchant.setMerchantImgUrl(!StringUtils.isEmpty(merchant.getMerchantImgUrl()) ? fileConfiguration.getUrlImage() + merchant.getMerchantImgUrl() : "");
            merchant.setAttValues(getAttribute(merchant.getMerchantId(), langId));
            merchant.setGroupValues(getGroupMerchant(merchant.getMerchantId(), langId));
        }
        return merchant;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<MerchantGroupResource> getGroupMerchant(Long merchantId, Long languageId) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT mg.id id, mg.code code, mgd.name name, ");
        sb.append(" CASE" +
                " WHEN mgm.merchant_group_id is null THEN 0 " +
                " ELSE 1  END isSelect ");
        sb.append(" FROM merchant_group mg JOIN merchant_group_description mgd ON mg.id = mgd.merchant_group_id ");
        sb.append(" left JOIN merchant_group_merchant mgm on mgm.MERCHANT_GROUP_ID = mg.ID and mgm.MERCHANT_ID= :merchantId ");
        sb.append(" WHERE mg.status <> 0 AND mgd.language_id = :languageId ");

        Query query = entityManager.createNativeQuery(sb.toString(), "MerchantGroupResource");
        query.setParameter("languageId", languageId);
        query.setParameter("merchantId", merchantId);

        return query.getResultList();
    }

    @Override
    public Page<MerchantOnMapDTO> getDataOnMap(Pageable pageable, Long parentId, Integer status, Integer action, boolean isPageable) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("PARENT_MARCHANT_ID", parentId);
        params.put("status", status);
        params.put("action", action);
        String sql = "select MERCHANT_ID merchantId,USER_NAME userName, FULL_NAME fullName,MERCHANT_IMG_URL merchantImgUrl ,ADDRESS, MOBILE_PHONE mobilePhone, status , lat , longitude from merchant where PARENT_MARCHANT_ID = :PARENT_MARCHANT_ID and status =:status and action = :action";
        int count = getRowCountV2(String.format("select count(*) from (%s) a", sql), params);

        if (isPageable) {
            pageable.isUnpaged();
        }
        return new PageImpl<>(findAll(pageable, MerchantOnMapDTO.class, sql, params), pageable, count);
    }

    @Override
    public List<AttributeDescriptionDTO> getAttributeOfMerchant(Long merchantId, Long langId) throws Exception {

        String sqlAttribute = "SELECT attd.*,att.required FROM attribute att " +
                " JOIN attribute_description attd ON att.id = attd.attribute_id " +
                " where att.type =1 and att.status =1 and attd.language_id = :langId";

        String sqlValue = " SELECT avd.id,ifnull(mae.att_value_id,avd.attribute_value_id) att_value_id,avd.name, " +
                " case " +
                " when mae.att_value_id is null then 0 " +
                " when mae.att_value_id <> avd.attribute_value_id " +
                " then 0 else 1 " +
                " END isSelect " +
                "FROM " +
                " attribute_value av " +
                " JOIN attribute_value_description avd ON av.id = avd.attribute_value_id AND avd.language_id = :langId " +
                " LEFT JOIN merchant_attribute_extent mae on mae.att_value_id = av.id and mae.merchant_id = :merchantId" +
                " where av.attribute_id = :attId ";

        List<AttributeDescriptionDTO> lstAttribute = findAll(null, AttributeDescriptionDTO.class, sqlAttribute, langId);
        if (lstAttribute != null && lstAttribute.size() > 0) {
            for (AttributeDescriptionDTO dto : lstAttribute) {
                try {
                    List<AttributeValueDescriptionDTO> values = new LinkedList<>();
                    values = findAll(null, AttributeValueDescriptionDTO.class, sqlValue, langId, merchantId, dto.getAttributeId());
                    if (values != null && values.size() > 0)
                        dto.setLstValue(values);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return lstAttribute;
    }

    @Override
    @Transactional
    public Merchant updateMerchantInfo(MerchantDto merchantRequest) {
        if (merchantRequest.merchantId == null) {
            throw new CustomErrorException("Merchant Id is not null");
        }
        Merchant merchant = find(Merchant.class, merchantRequest.merchantId);
        if (merchant == null) {
            throw new CustomErrorException("Merchant is not exist");
        }
        Utils.copyNonNullProperties(merchantRequest, merchant);
        save(merchant);
        return merchant;
    }
}
