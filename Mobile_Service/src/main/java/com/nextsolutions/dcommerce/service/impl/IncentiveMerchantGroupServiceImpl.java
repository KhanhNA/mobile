package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.incentive.IncentiveMerchantGroup;
import com.nextsolutions.dcommerce.service.IncentiveMerchantGroupService;
import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.model.incentive.Incentive;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncentiveMerchantGroupServiceImpl extends CommonServiceImpl implements IncentiveMerchantGroupService {
    @Override
    public void save(List<IncentiveMerchantGroup> ictMerchantGroups, Long ictProgramId) throws Exception {
        Incentive ictProgram = null;
        if(ictProgramId == null || Utils.isEmpty(ictMerchantGroups)){
            throw new CustomErrorException("bodyNull");
        }
        ictProgram = find(Incentive.class, ictProgramId);
        if(ictProgram == null){

            throw new CustomErrorException("IncentiveProgramIdNotExist");

        }
        IncentiveMerchantGroup entity;
        MerchantGroup merchantGroup;
        for(IncentiveMerchantGroup element: ictMerchantGroups)
        {
            if(element.getId() != null){
                entity = find(IncentiveMerchantGroup.class, element.getId());
                if(entity == null) {
                    throw new CustomErrorException("IncentiveMerchantGroupIdNotExist");
                }
            } else {
                entity = new IncentiveMerchantGroup();
            }
            merchantGroup = find(MerchantGroup.class, element.getMerchantGroup().getId());
            if(merchantGroup == null) {
                throw new CustomErrorException("merchantGroupNotExist");
            }
            entity.setIctProgramId(ictProgramId);
            entity.setMerchantGroup(merchantGroup);
            entity.setStatus(1);
            save(entity);
        }

    }

    @Override
    public void delete(Long id) throws Exception {
        IncentiveMerchantGroup entity = find(IncentiveMerchantGroup.class, id);
        if(entity == null) {
            throw new CustomErrorException("incentiveMerchantGroupNotExist");
        }

        remove(entity);
    }


}
