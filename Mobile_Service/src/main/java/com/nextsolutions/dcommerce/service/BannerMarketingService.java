package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.shared.dto.BannerAlbumDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BannerMarketingService extends CommonService {
    void save(BannerAlbumDto dto, List<MultipartFile> files) throws Exception;
    void saveMG(Long albumId, List<MerchantGroup> dtos) throws Exception;
    void deleteAlbumDetail(Long id) throws Exception;
    void deleteBanner(Long id) throws Exception;

}
