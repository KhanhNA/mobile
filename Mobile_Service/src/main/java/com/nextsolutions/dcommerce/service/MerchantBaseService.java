package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.Merchant;

public interface MerchantBaseService extends BaseService<Merchant, Long> {
    Merchant findByActiveCode(String code);

    Merchant findByMobilePhone(String code);

    Merchant findByUserName(String userName);
}
