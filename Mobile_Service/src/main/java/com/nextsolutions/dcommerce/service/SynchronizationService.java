package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.ui.model.request.SynchronizationResource;

import java.util.List;
import java.util.Map;

public interface SynchronizationService {
    Map<String, List<Long>> synchronize(List<SynchronizationResource> resources) throws ClassNotFoundException;
}
