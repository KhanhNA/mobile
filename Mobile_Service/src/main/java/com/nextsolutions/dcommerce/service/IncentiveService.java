package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.incentive.Incentive;
import com.nextsolutions.dcommerce.shared.dto.*;
import com.nextsolutions.dcommerce.shared.dto.coupon.CouponIncentiveDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface IncentiveService extends CommonService {

    List<Incentive> getIctPrgFromPackingId(Long ictPrgId) throws Exception;

    Incentive insert(IncentiveProgramDto dto) throws Exception;

    Map<String, String> uploadFile(MultipartFile image) throws IOException;

    List<IncentiveDescriptionDTO> getIncentiveFromId(List<Long> id, Long langId) throws Exception;

    List<CouponIncentiveDTO> getCouponCode(Long merchantId, Long langId) throws Exception;

    List<IncentiveDescriptionDTO> getIncentive(Long merchantId, Long langId) throws Exception;

    List<IncentiveAppliedDTO> getIncentiveProgramAppliedFromProductId(Long merchantId, Long productId, List<Integer> lstType, Integer displayArea, Long langId) throws Exception;

    IncentiveAppliedDTO getIncentiveProgramAppliedFromPackingId(Long merchantId, Long productId, List<Integer> lstType, Integer displayArea, Long langId) throws Exception;

    List<Long> getIncentiveIdApplied(Long merchantId, List<Integer> lstType, List<Integer> displayArea, Boolean notInDisplay) throws Exception;

    List<IncentiveAppliedDTO> getBannerList(Long merchantId, Long langId) throws Exception;

    DataFlashSaleDTO getPackingFlashSaleCurrentTime(Pageable pageable, Long merchantId, Long langId) throws Exception;

    PackingProductDTO getPackingFlashSaleByIncentiveId(Long incentiveId, Long packingId, String flashSaleTime) throws Exception;

    CurrentTimeFlashSaleDTO flashSaleCurrentTime();

    DataFlashSaleDTO getFlashSaleByTime(Pageable pageable, Long merchantId, String milestone, Long langId) throws Exception;

}
