package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.model.Banner;
import com.nextsolutions.dcommerce.model.BannerDescription;
import com.nextsolutions.dcommerce.model.BannerDetails;
import com.nextsolutions.dcommerce.model.BannerMerchantGroup;
import com.nextsolutions.dcommerce.repository.BannerMerchantGroupRepository;
import com.nextsolutions.dcommerce.repository.BannerRepository;
import com.nextsolutions.dcommerce.service.BannerService;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerDescriptionResource;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerDetailsResource;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import com.nextsolutions.dcommerce.shared.mapper.BannerMapper;
import com.nextsolutions.dcommerce.shared.mapper.MerchantGroupMapper;
import com.nextsolutions.dcommerce.ui.model.request.BannerFormRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.BannerFormResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URLConnection;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BannerServiceImpl extends CommonServiceImpl implements BannerService {

    private final BannerRepository bannerRepository;
    private final BannerMapper bannerMapper;
    private final MerchantGroupMapper merchantGroupMapper;
    private final BannerMerchantGroupRepository bannerMerchantGroupRepository;
    private final AppConfiguration appConfiguration;

    private static final String PARENT_PATH = "/banners";
    private static final String DETAILS_PATH = PARENT_PATH + "/details";

    @Override
    public Page<BannerResource> findByQuery(BannerResource resource, Pageable pageable) {
        return bannerRepository.findByQuery(resource, pageable);
    }

    @Override
    public void inactiveBanner(Long id) {
        bannerRepository.inactive(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createBanner(BannerFormRequestModel request) {
        Banner banner = new Banner();
        banner.setCode(request.getCode());
        banner.setFromDate(request.getFromDate());
        banner.setToDate(request.getToDate());
        banner.setStatus(1);
        bannerRepository.save(banner); // save to database to get new bannerId

        Long bannerId = banner.getId();
        banner.setDescriptions(toBannerDescriptions(request.getDescriptions(), bannerId));
        banner.setMerchantGroups(toMerchantGroups(request.getMerchantGroupIds(), bannerId));
        banner.setDetails(toBannerDetails(request.getDetails(), bannerId));

        bannerRepository.save(banner);
    }

    private List<BannerDescription> toBannerDescriptions(List<BannerDescriptionResource> bannerDescriptionResources, Long bannerId) {
        return Optional.ofNullable(bannerDescriptionResources)
                .map(resources -> resources.stream()
                        .map(resource -> {
                            BannerDescription bannerDescription = new BannerDescription();
                            bannerDescription.setBannerId(bannerId);
                            bannerDescription.setName(resource.getName());
                            bannerDescription.setLanguageId(resource.getLanguageId());
                            return bannerDescription;
                        }).collect(Collectors.toList()))
                .orElse(null);
    }

    private List<BannerDetails> toBannerDetails(List<BannerDetailsResource> bannerDetailsResources, Long bannerId) {
        return Optional.ofNullable(bannerDetailsResources)
                .map(detailsResources -> detailsResources.stream()
                        .map(resource -> toBannerDetails(resource, bannerId))
                        .collect(Collectors.toList())
                ).orElse(null);
    }

    private BannerDetails toBannerDetails(BannerDetailsResource resource, Long bannerId) {
        BannerDetails bannerDetails = new BannerDetails();
        bannerDetails.setBannerId(bannerId);
        bannerDetails.setDescription(resource.getDescription());
        bannerDetails.setLinkTarget(resource.getLinkTarget());
        bannerDetails.setLanguageId(resource.getLanguageId());
        String realImageUrl = generateRealImageUrl(resource.getImageUrl());
        if (!realImageUrl.isEmpty()) bannerDetails.setImageUrl(realImageUrl);
        bannerDetails.setStatus(true);
        return bannerDetails;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BannerFormResponseModel updateBanner(Long id, BannerFormRequestModel request) {
        Banner banner = bannerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("%s not found", id)));
        banner.setCode(request.getCode());
        banner.setFromDate(request.getFromDate());
        banner.setToDate(request.getToDate());

        Map<Long, BannerDescriptionResource> bannerDescriptionMap = request.getDescriptions()
                .stream()
                .collect(Collectors.toMap(BannerDescriptionResource::getId, descriptionResource -> descriptionResource));

        banner.getDescriptions().forEach(bannerDescription -> {
            Long bannerDescriptionId = bannerDescription.getId();
            if (bannerDescriptionMap.containsKey(bannerDescriptionId)) {
                BannerDescriptionResource bannerDescriptionResource = bannerDescriptionMap.get(bannerDescriptionId);
                bannerDescription.setName(bannerDescriptionResource.getName());
            }
        });

        List<BannerDetailsResource> requestDetails = request.getDetails();

        List<BannerDetailsResource> bannerDetailsAdding = new LinkedList<>();
        Map<Long, BannerDetailsResource> bannerDetailsResourceMap = new HashMap<>();

        requestDetails.forEach(bannerDetailsResource -> {
            Long bannerId = bannerDetailsResource.getId();
            if (bannerId == null) {
                bannerDetailsAdding.add(bannerDetailsResource);
            } else {
                bannerDetailsResourceMap.put(bannerId, bannerDetailsResource);
            }
        });

        // TODO: UPDATE OR INACTIVE
        List<BannerDetails> details = banner.getDetails();
        details.forEach(bannerDetails -> {
            if (bannerDetailsResourceMap.containsKey(bannerDetails.getId())) {
                BannerDetailsResource bannerDetailsResource = bannerDetailsResourceMap.get(bannerDetails.getId());
                bannerDetails.setLinkTarget(bannerDetailsResource.getLinkTarget());
                bannerDetails.setDescription(bannerDetailsResource.getDescription());
                String realImageUrl = generateRealImageUrl(bannerDetailsResource.getImageUrl());
                if (!realImageUrl.isEmpty()) bannerDetails.setImageUrl(realImageUrl);
            } else {
                bannerDetails.setStatus(false);
            }
        });

        // TODO: ADD NEW
        bannerDetailsAdding.forEach(bannerDetailsResource -> {
            details.add(toBannerDetails(bannerDetailsResource, id));
        });

        bannerMerchantGroupRepository.deleteAllByBannerId(banner.getId());
        banner.setMerchantGroups(toMerchantGroups(request.getMerchantGroupIds(), banner.getId()));

        bannerRepository.save(banner);
        return mapBannerToBannerFormResponse(banner);
    }

    private List<BannerMerchantGroup> toMerchantGroups(List<Long> merchantGroupIds, Long bannerId) {
        return Optional.ofNullable(merchantGroupIds)
                .map(ids -> ids.stream()
                        .map(merchantGroupId -> {
                            BannerMerchantGroup bannerMerchantGroup = new BannerMerchantGroup();
                            bannerMerchantGroup.setBannerId(bannerId);
                            bannerMerchantGroup.setMerchantGroupId(merchantGroupId);
                            return bannerMerchantGroup;
                        })
                        .collect(Collectors.toList()))
                .orElse(null);
    }

    @Override
    public BannerFormResponseModel getBannerFormResponse(Long id) {
        Banner banner = bannerRepository.findByIdAndMustActive(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("%s not found", id)));
        return mapBannerToBannerFormResponse(banner);
    }

    @Override
    public Map<String, String> saveImageFileToLocalServer(MultipartFile image) throws IOException {
        Map<String, String> result = new HashMap<>();
        String detailsPath = String.format("%s%s", appConfiguration.getUploadPath(), DETAILS_PATH);
        File directoryPath = new File(detailsPath);
        if (!directoryPath.exists())
            if (directoryPath.mkdirs())
                throw new RuntimeException("Cannot create directory");

        String fileName = new Date().getTime() + "_" + image.getOriginalFilename();
        String filePath = String.format("%s/%s", detailsPath, fileName);
        image.transferTo(new File(filePath));
        result.put("default", String.format("%s/%s", DETAILS_PATH, fileName));
        return result;
    }

    @Override
    public List<BannerDetailsResource> getMarketing(Long merchantId, Long langId) throws Exception {
        String sql = "select bd.* from banner banner join banner_merchant_group bmg on banner.id = bmg.banner_id " +
                " join banner_details bd on bd.banner_id = banner.id and bd.status =1 and bd.language_id =:langId " +
                " where" +
                " bmg.merchant_group_id in ( select merchant_group_id from merchant_group_merchant where merchant_id = " + merchantId + " ) " +
                " and " +
                " banner.status =1 " +
                " and DATE(CURRENT_DATE()) >= date(banner.from_date) and (DATE(CURRENT_DATE()) <= DATE(banner.to_date) or banner.to_date is null) ";

        return findAll(null, BannerDetailsResource.class, sql, langId);
    }

    private BannerFormResponseModel mapBannerToBannerFormResponse(Banner banner) {
        BannerFormResponseModel bannerFormResponseModel = bannerMapper.toBannerFormResponseModel(banner);
        List<BannerMerchantGroup> merchantGroups = banner.getMerchantGroups();
        List<MerchantGroupResource> merchantGroupResources = Optional
                .ofNullable(merchantGroups)
                .map(bannerMerchantGroups -> bannerMerchantGroups.stream()
                        .map(BannerMerchantGroup::getMerchantGroup)
                        .map(merchantGroupMapper::toMerchantGroupResource)
                        .collect(Collectors.toList()))
                .orElse(null);
        bannerFormResponseModel.setMerchantGroups(merchantGroupResources);
        return bannerFormResponseModel;
    }

    private String generateRealImageUrl(String imageUrl) {
        if (imageUrl != null) {
            if (imageUrl.startsWith("data:")) {
                try {
                    imageUrl = imageUrl.replaceFirst("(data:.*;base64,)", "");
                    byte[] decode = Base64.getDecoder().decode(imageUrl);
                    String type = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(decode));
                    String subtype = MimeTypeUtils.parseMimeType(type).getSubtype();
                    if (subtype.equals("x-wav")) {
                        subtype = "webp";
                    }
                    String randomUUID = UUID.randomUUID().toString().replaceAll("-", "");
                    String fileName = randomUUID + "." + subtype;

                    File parentDirectory = new File(appConfiguration.getUploadPath() + PARENT_PATH);
                    if (!parentDirectory.exists())
                        if (!parentDirectory.mkdirs())
                            throw new IOException("Cannot create banner parent directory");

                    String imageURI = PARENT_PATH + "/" + fileName;
                    try (OutputStream out = new FileOutputStream(appConfiguration.getUploadPath() + imageURI)) {
                        out.write(decode);
                    }
                    return imageURI;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                return imageUrl;
            }
        }
        return "";
    }
}
