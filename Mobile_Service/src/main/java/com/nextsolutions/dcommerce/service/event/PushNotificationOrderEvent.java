package com.nextsolutions.dcommerce.service.event;

import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class PushNotificationOrderEvent extends ApplicationEvent {
    private OrderV2Dto order;
    private String fullName;
    private String mobilePhone;

    public PushNotificationOrderEvent(Object source, OrderV2Dto order, String fullName, String mobilePhone) {
        super(source);
        this.order = order;
        this.fullName = fullName;
        this.mobilePhone = mobilePhone;
    }
}
