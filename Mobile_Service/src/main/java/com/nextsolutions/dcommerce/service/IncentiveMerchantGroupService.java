package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.incentive.IncentiveMerchantGroup;

import java.util.List;

public interface IncentiveMerchantGroupService extends CommonService {
    void save(List<IncentiveMerchantGroup> ictMerchantGroups, Long ictProgramId) throws Exception;
    void delete(Long id) throws Exception;

}
