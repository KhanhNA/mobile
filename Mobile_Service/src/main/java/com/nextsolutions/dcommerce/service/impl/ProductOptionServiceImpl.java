package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.repository.ProductAttributeRepository;
import com.nextsolutions.dcommerce.repository.ProductOptionRepository;
import com.nextsolutions.dcommerce.repository.ProductOptionValueRepository;
import com.nextsolutions.dcommerce.service.ProductOptionService;
import com.nextsolutions.dcommerce.shared.dto.ProductOptionDto;
import com.nextsolutions.dcommerce.shared.dto.ProductOptionValueDto;
import com.nextsolutions.dcommerce.ui.model.request.*;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionTableResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionValueTableResponseModel;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.mapper.ProductOptionMapper;
import com.nextsolutions.dcommerce.shared.mapper.ProductOptionValueMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductOptionServiceImpl implements ProductOptionService {

    private final ProductOptionRepository productOptionRepository;
    private final ProductOptionMapper productOptionMapper;
    private final ProductOptionValueRepository productOptionValueRepository;
    private final ProductOptionValueMapper productOptionValueMapper;
    private final ProductAttributeRepository productAttributeRepository;

    @Override
    public Collection<ProductOptionResponseModel> findAllByLanguageId(Long languageId) {
        return productOptionRepository.findAllByLanguageId(languageId);
    }

    @Override
    public List<ProductOptionTableResponseModel> findAllByQueryAndLanguage(ProductOptionQueryRequestModel query, Long languageId) {
        return productOptionRepository.findAllByQueryAndLanguage(query, languageId);
    }

    @Override
    public List<ProductOptionValueTableResponseModel> findAllProductOptionValues(Long productOptionId, Long languageId) {
        return productOptionRepository.findAllProductOptionValues(productOptionId, languageId);
    }

    @Override
    public ProductOptionDto findById(Long id) {
        return productOptionRepository.findById(id).map(this.productOptionMapper::toProductOptionDto).orElse(null);
    }

    @Override
    public ProductOptionValueDto findByIdAndProductOptionId(Long id, Long productOptionId) {
        return productOptionValueRepository.findByIdAndOptionId(id, productOptionId)
                .map(this.productOptionValueMapper::toProductOptionValueDto).orElse(null);
    }

    @Override
    @Transactional
    public ProductOptionDto createProductOption(ProductOptionRequestModel request) {
        ProductOption productOption = new ProductOption();
        productOption.setCode(request.getCode());
        productOption.setDescriptions(request.getDescriptions().stream().map(productOptionDescriptionRequestModel -> {
            ProductOptionDescription productOptionDescription = createProductOptionDescription(productOptionDescriptionRequestModel);
            productOptionDescription.setOption(productOption);
            return productOptionDescription;
        }).collect(Collectors.toList()));
        productOptionRepository.save(productOption);
        return Optional.of(productOption).map(productOptionMapper::toProductOptionDto)
                .orElseThrow(() -> new RuntimeException("ERROR"));
    }

    @Override
    @Transactional
    public ProductOptionDto updateProductOption(Long id, ProductOptionRequestModel request) {
        ProductOption productOption = productOptionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Product Option with id = %s not found", id)));
        productOption.setCode(request.getCode());
        List<ProductOptionDescription> productOptionDescriptions = productOption.getDescriptions();
        if (productOptionDescriptions == null || productOptionDescriptions.isEmpty()) {
            // TODO: create
            productOption.setDescriptions(request.getDescriptions().stream().map(productOptionDescriptionRequestModel -> {
                ProductOptionDescription productOptionDescription = createProductOptionDescription(productOptionDescriptionRequestModel);
                productOptionDescription.setOption(productOption);
                return productOptionDescription;
            }).collect(Collectors.toList()));
        } else {
            // TODO: update if exists
            Map<Long, ProductOptionDescription> productOptionDescriptionMap = productOptionDescriptions.stream()
                    .collect(Collectors.toMap(ProductOptionDescription::getId,
                            productOptionDescription -> productOptionDescription));
            request.getDescriptions()
                    .forEach(productOptionDescriptionRequestModel -> {
                        if (productOptionDescriptionMap.containsKey(productOptionDescriptionRequestModel.getId())) {
                            ProductOptionDescription productOptionDescription = productOptionDescriptionMap.get(productOptionDescriptionRequestModel.getId());
                            productOptionDescription.setOption(productOption);
                            productOptionDescription.setName(productOptionDescriptionRequestModel.getName());
                        } else {
                            ProductOptionDescription productOptionDescription
                                    = createProductOptionDescription(productOptionDescriptionRequestModel);
                            productOptionDescription.setOption(productOption);
                            productOptionDescriptions.add(productOptionDescription);
                        }
                    });
        }
        productOptionRepository.save(productOption);
        return Optional.of(productOption).map(productOptionMapper::toProductOptionDto)
                .orElseThrow(() -> new RuntimeException("ERROR"));
    }

    @Override
    @Transactional
    public void deleteProductOption(Long id) {
        productAttributeRepository.deleteAllByProductOptionId(id);
        productOptionRepository.deleteById(id);
    }

    @Override
    @Transactional
    public ProductOptionValueDto createProductOptionValue(Long productOptionId, ProductOptionValueRequestModel request) {
        ProductOptionValue productOptionValue = new ProductOptionValue();
        productOptionValue.setCode(request.getCode());
        productOptionValue.setProductOptionId(productOptionId);
        productOptionValue.setDescriptions(request.getDescriptions().stream()
                .map(descriptionRequest -> {
                    ProductOptionValueDescription description = createProductValueDescription(descriptionRequest);
                    description.setOptionValue(productOptionValue);
                    return description;
                }).collect(Collectors.toList()));
        productOptionValueRepository.save(productOptionValue);
        return Optional.of(productOptionValue).map(this.productOptionValueMapper::toProductOptionValueDto)
                .orElseThrow(() -> new RuntimeException("NMQ"));
    }

    private ProductOptionValueDescription createProductValueDescription(ProductOptionValueDescriptionRequestModel request) {
        ProductOptionValueDescription productOptionValueDescription = new ProductOptionValueDescription();
        productOptionValueDescription.setName(request.getName());
        productOptionValueDescription.setLanguageId(request.getLanguageId());
        return productOptionValueDescription;
    }

    @Override
    public ProductOptionValueDto updateProductOptionValue(Long id, Long productOptionId, ProductOptionValueRequestModel request) {
        ProductOptionValue productOptionValue = productOptionValueRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Product Option with id = %s not found", id)));
        productOptionValue.setCode(request.getCode());
        productOptionValue.setProductOptionId(productOptionId);
        List<ProductOptionValueDescription> productOptionValueDescriptions = productOptionValue.getDescriptions();
        if (productOptionValueDescriptions == null || productOptionValueDescriptions.isEmpty()) {
            // TODO: create
            productOptionValue.setDescriptions(request.getDescriptions().stream()
                    .map(descriptionRequest -> {
                        ProductOptionValueDescription description = createProductValueDescription(descriptionRequest);
                        description.setOptionValue(productOptionValue);
                        return description;
                    }).collect(Collectors.toList()));
        } else {
            // TODO: update if exists
            Map<Long, ProductOptionValueDescription> productOptionValueDescriptionMap = productOptionValueDescriptions.stream()
                    .collect(Collectors.toMap(ProductOptionValueDescription::getId,
                            productOptionValueDescription -> productOptionValueDescription));
            request.getDescriptions()
                    .forEach(productOptionValueDescriptionRequestModel -> {
                        if (productOptionValueDescriptionMap.containsKey(productOptionValueDescriptionRequestModel.getId())) {
                            ProductOptionValueDescription productOptionValueDescription =
                                    productOptionValueDescriptionMap.get(productOptionValueDescriptionRequestModel.getId());
                            productOptionValueDescription.setOptionValue(productOptionValue);
                            productOptionValueDescription.setName(productOptionValueDescriptionRequestModel.getName());
                        } else {
                            ProductOptionValueDescription description =
                                    createProductValueDescription(productOptionValueDescriptionRequestModel);
                            description.setOptionValue(productOptionValue);
                            productOptionValueDescriptions.add(description);
                        }
                    });
        }
        productOptionValueRepository.save(productOptionValue);
        return Optional.of(productOptionValue).map(productOptionValueMapper::toProductOptionValueDto)
                .orElseThrow(() -> new RuntimeException("ERROR"));
    }

    @Override
    public void deleteProductOptionValue(Long id, Long productOptionId) {
        productOptionValueRepository.deleteByIdAndProductOptionId(id, productOptionId);
    }

    private ProductOptionDescription createProductOptionDescription(
            ProductOptionDescriptionRequestModel productOptionDescriptionRequestModel) {
        ProductOptionDescription productOptionDescription = new ProductOptionDescription();
        productOptionDescription.setLanguageId(productOptionDescriptionRequestModel.getLanguageId());
        productOptionDescription.setName(productOptionDescriptionRequestModel.getName());
        return productOptionDescription;
    }
}
