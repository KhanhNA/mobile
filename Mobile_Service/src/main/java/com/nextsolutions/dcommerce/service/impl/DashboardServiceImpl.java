package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.repository.OverviewRepository;
import com.nextsolutions.dcommerce.service.DashboardService;
import com.nextsolutions.dcommerce.shared.dto.OverviewStatistics;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class DashboardServiceImpl implements DashboardService {

    private final OverviewRepository overviewRepository;

    public DashboardServiceImpl(OverviewRepository overviewRepository) {
        this.overviewRepository = overviewRepository;
    }

    @Override
    public OverviewStatistics getOverviewStatistics() {
        return overviewRepository.getOverviewStatistics();
    }

    @Override
    public String getSqlTotalRegister(String fromDate, String toDate) {
        return "SELECT count(*)  FROM merchant where PARENT_MARCHANT_ID =:parentMerchantId and status not in (0) AND " + SpringUtils.whereDate(fromDate, toDate, "create_date");
    }

    @Override
    public String getSqlTotalActive(String fromDate, String toDate) {
        return "SELECT count(*)  FROM merchant where PARENT_MARCHANT_ID =:parentMerchantId and status = 1 and confirm_kpi = true AND " + SpringUtils.whereDate(fromDate, toDate, "active_date");
    }

    @Override
    public String getSqlTotalInactive(String fromDate, String toDate) {
        return "SELECT count(*)  FROM merchant where PARENT_MARCHANT_ID =:parentMerchantId and ((status = 0 and action =3 AND " + SpringUtils.whereDate(fromDate, toDate, "create_date") + " ) or ( STATUS = 2  AND " + SpringUtils.whereDate(fromDate, toDate, "deactive_date") + " ) )";
    }

    @Override
    public String getSqlInactiveRevenue(String fromDate, String toDate) {
        return " select count(distinct(m.MERCHANT_ID))  from merchant m " +
                " where m.PARENT_MARCHANT_ID =:parentMerchantId " +
                " and m.status=1  and m.CONFIRM_KPI = true " +
                " and m.merchant_id not in (select distinct(merchant_id) from orders where order_status = 2 and DATE(orders.ORDER_DATE) >= DATE('" + fromDate + "') and DATE(orders.ORDER_DATE) <= DATE('" + toDate + "'))";
    }

    @Override
    public String getSqlTotalRevenue(String fromDate, String toDate) {
        return "select ifnull(sum(amount),0) from orders where MERCHANT_ID in (select merchant_id from merchant " +
                "where PARENT_MARCHANT_ID =:parentMerchantId and status = 1 and confirm_kpi = true )  and DATE(ORDER_DATE) >= DATE('" + fromDate + "') " +
                "and  DATE(ORDER_DATE) <= DATE('" + toDate + "') " +
                "and order_status = 2";
    }

    @Override
    public String getSqlTotalOrder(String fromDate, String toDate) {
        return " SELECT COUNT(*) FROM orders o " +
                " WHERE DATE(o.ORDER_DATE) >= DATE('" + fromDate + "') AND DATE(o.ORDER_DATE) <= DATE('" + toDate + "') " +
                " AND o.merchant_id IN (SELECT merchant_id FROM merchant WHERE PARENT_MARCHANT_ID = :parentMerchantId and status=1  and CONFIRM_KPI = true  )" +
                " AND o.order_status = 2 ";
    }

    @Override
    public String getSqlTotalPending(String fromDate, String toDate) {
        return "SELECT count(*) FROM merchant where PARENT_MARCHANT_ID =:parentMerchantId and status = 1 and CONFIRM_KPI = false and " + SpringUtils.whereDate(fromDate, toDate, "active_date");
    }

    @Override
    public String getSqlTotalOrderOfMerchant(String fromDate, String toDate) {
        return " SELECT COUNT(*) FROM orders o " +
                " WHERE DATE(o.ORDER_DATE) >= DATE('" + fromDate + "') AND DATE(o.ORDER_DATE) <= DATE('" + toDate + "') " +
                " AND order_status = 2 AND o.merchant_id = :merchantId";
    }

    @Override
    public String getSqlTotalRevenueOfMerchant(String fromDate, String toDate) {
        return "select ifnull(sum(amount),0) from orders where MERCHANT_ID = :merchantId " +
                " and order_status = 2 and DATE(ORDER_DATE) >= DATE('" + fromDate + "') and  DATE(ORDER_DATE) <= DATE('" + toDate + "')";
    }


}
