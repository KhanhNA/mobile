package com.nextsolutions.dcommerce.service.event;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.order.Order;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class PushNotificationOrderSuccessEvent extends ApplicationEvent {
    private Order order;

    public PushNotificationOrderSuccessEvent(Object source, Order order) {
        super(source);
        this.order = order;
    }
}
