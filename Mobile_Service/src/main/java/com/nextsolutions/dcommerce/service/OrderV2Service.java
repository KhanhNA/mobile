package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.order.Order;
import com.nextsolutions.dcommerce.shared.dto.OrderTemplateDetailDTO;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface OrderV2Service extends CommonService {
    void calculate(OrderV2Dto orderDto, Long langId, String token) throws Exception;

    Order saveOrder(OrderV2Dto orderV2Dto) throws Exception;

    Page<OrderV2Dto> getListOrder(Pageable pageable, OrderV2Dto orderDTO, Long langId) throws Exception;

    OrderV2Dto getOrder(Long orderId, Long langId) throws Exception;

    Page<OrderTemplateDetailDTO> getOrderTemplate(Pageable pageable, OrderV2Dto entityOrder, Long langId) throws Exception;

    BigDecimal getAmountOrderSuccess(Long merchantId, LocalDateTime fromDate, LocalDateTime toDate);
}
