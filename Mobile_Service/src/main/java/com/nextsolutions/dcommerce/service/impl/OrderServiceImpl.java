package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.FileConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.OrderTemplate;
import com.nextsolutions.dcommerce.model.order.Order;
import com.nextsolutions.dcommerce.model.order.OrderPackingV2;
import com.nextsolutions.dcommerce.repository.OrderRepository;
import com.nextsolutions.dcommerce.repository.OrderTemplateRepository;
import com.nextsolutions.dcommerce.service.OrderService;
import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import com.nextsolutions.dcommerce.shared.dto.OrderPackingDTO;
import com.nextsolutions.dcommerce.shared.dto.OrderTemplateDetailDTO;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestHeader;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Service
@RequiredArgsConstructor
public class OrderServiceImpl extends CommonServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderTemplateRepository orderTemplateRepository;
    private final FileConfiguration fileConfiguration;

    @Override
    public OrderV2Dto getOrders(Pageable pageable, OrderV2Dto orderDto, boolean hasPacking, Long langId) throws Exception {
        if (orderDto.orderId == null) {
            throw new CustomErrorException(CustomErrorException.USER_ERROR, "orderId is not null");
        }
        HashMap<String, Object> params = new HashMap<>();
        StringBuilder sqlQuery = new StringBuilder(" Select o.*, m.FULL_NAME as merchantFullName, m.MERCHANT_IMG_URL as merchantImgUrl, pm.PAYMENT_METHOD_NAME paymentMethodName, " +
                " o.RECV_ADDR recvAddr " +
                " from orders o " +
                " JOIN merchant m ON m.MERCHANT_ID = o.MERCHANT_ID " +
                " LEFT JOIN payment_method pm on pm.PAYMENT_METHOD_ID = o.PAYMENT_METHOD_ID " +
                " where o.order_id = :orderId");
        orderDto.setIncentiveAmount(null);
        sqlQuery.append(" ORDER BY o.ORDER_DATE DESC ");
        params.put("orderId", orderDto.orderId);
        OrderV2Dto orderV2Dto = findOne(OrderV2Dto.class, sqlQuery.toString(), params);
        orderV2Dto.merchantImgUrl = !StringUtils.isEmpty(orderV2Dto.merchantImgUrl) ? fileConfiguration.getUrlImage() + orderV2Dto.merchantImgUrl : "";
        if (orderV2Dto.getDeliveryDate() != null) {
            orderV2Dto.setDeliveryDateStr(orderV2Dto.getDeliveryDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        }

        if (hasPacking) {
            String sqlOrderPacking = "select op.ORDER_PACKING_ID orderPackingId,op.PACKING_PRODUCT_ID packingProductId," +
                    " o.ORDER_ID," +
                    " pp.PACKING_URL packingUrl," +
                    " op.IS_INCENTIVE isIncentive," +
                    " pd.PRODUCT_NAME productName," +
                    " case " +
                    " when op.IS_INCENTIVE = 1 then op.price " +
                    " else op.price_sale " +
                    " end price, " +
                    " op.VAT vat," +
                    " op.ORDER_QUANTITY orderQuantity," +
                    " pt.QUANTITY quantity," +
                    " op.BONUS_QUANTITY bonusQuantity," +
                    " p.PRODUCT_ID productId" +
                    " from orders o" +
                    " JOIN order_packing_v2 op ON o.ORDER_ID = op.ORDER_ID" +
                    " JOIN product p ON p.PRODUCT_ID = op.PRODUCT_ID" +
                    " JOIN packing_product pp ON pp.PACKING_PRODUCT_ID = op.PACKING_PRODUCT_ID and pp.PRODUCT_ID = p.PRODUCT_ID" +
                    " JOIN product_description pd ON pd.PRODUCT_ID = p.PRODUCT_ID" +
                    " JOIN packing_type pt on pp.PACKING_TYPE_ID = pt.PACKING_TYPE_ID" +
                    " WHERE o.ORDER_ID =:orderId AND pd.LANGUAGE_ID =:langId";

            List<OrderPackingV2> dtos = findAll(pageable, OrderPackingV2.class, sqlOrderPacking, orderV2Dto.getOrderId(), langId);
            List<Long> mapIncentive = findAll(null, Long.class, "SELECT incentive_program_id FROM order_incentive where order_id =:orderId", orderV2Dto.getOrderId());
            orderV2Dto.setMapIncentives(mapIncentive);
            if (dtos != null && dtos.size() > 0) {
                for (OrderPackingV2 packingDTO : dtos) {
                    if (packingDTO.getIsIncentive() == OrderPackingDTO.SALE) {
                        if (orderV2Dto.orderPackings == null) {
                            orderV2Dto.orderPackings = new ArrayList<>();
                        }
                        orderV2Dto.calculatePackings.add(packingDTO);
                    } else if (packingDTO.getIsIncentive() == OrderPackingDTO.INCENTIVE) {
                        if (orderV2Dto.incentivePackings == null) {
                            orderV2Dto.incentivePackings = new ArrayList<>();
                        }
                        packingDTO.setBonusQuantity(packingDTO.getBonusQuantity());
                        orderV2Dto.incentivePackings.add(packingDTO);
                    }
                }
            }
        }
        return orderV2Dto;
    }

    @Override
    public void changeOrder(OrderV2Dto orderDto) throws Exception {
        if (orderDto.orderId == null) {
            throw new CustomErrorException(CustomErrorException.USER_ERROR, "orderIdNull");
        }
        Order order = find(Order.class, orderDto.orderId);
        if (order == null) {
            throw new CustomErrorException(CustomErrorException.USER_ERROR, "orderNotExists");
        }
        if (orderDto.getOrderStatus() != null && !order.getOrderStatus().equals(orderDto.getOrderStatus())) {
            if (order.getOrderStatus() != 0 && order.getOrderStatus() != -2) { //phai la trang thai chua duyet
                throw new CustomErrorException(CustomErrorException.USER_ERROR, "orderStatusCouldNotChange");
            }
            order.setOrderStatus(orderDto.getOrderStatus());


        }
        if (orderDto.getPaymentStatus() != null && !order.getPaymentStatus().equals(orderDto.getPaymentStatus())) {
            if (order.getPaymentStatus() != 0 || orderDto.getPaymentStatus() != 1) { //phai la trang thai chua thanh toan
                throw new CustomErrorException(CustomErrorException.USER_ERROR, "paymentStatusCouldNotChange");
            }
            order.setPaymentStatus(1);
            order.setPaymentDate(Utils.getLocalDatetime());

        }
        if (orderDto.getDeliveryStatus() != null && !order.getDeliveryStatus().equals(orderDto.getDeliveryStatus())) {
            if (order.getDeliveryStatus() != 0 || orderDto.getDeliveryStatus() != 1) { //phai la trang thai chua thanh toan
                throw new CustomErrorException(CustomErrorException.USER_ERROR, "deliveryStatusCouldNotChange");
            }
            order.setDeliveryStatus(1);
            order.setDeliveryDate(Utils.getLocalDatetime());


        }
        order.setReason(orderDto.getReason());
        save(order);
    }

    @Override
    public boolean changeOrderStatus(OrderDto orderDto) {
        return orderRepository.changeStatusOrder(orderDto);
    }

    @Override
    public void deleteOrder(Long orderId) throws Exception {
        orderRepository.deleteById(orderId);
    }

    @Override
    public void deleteOrderTemplate(Long orderTemplateId) throws Exception {
        OrderTemplate temp = find(OrderTemplate.class, orderTemplateId);
        if (temp != null) {
            executeSql("DELETE FROM order_template WHERE id = :orderTempLateId ", "orderTempLateId", orderTemplateId);
        } else {
            throw new CustomErrorException("order template id is not exist");
        }

    }

    @Override
    public OrderTemplate editOrderTemplate(Long orderTemplateId, String name) throws Exception {
        OrderTemplate temp = find(OrderTemplate.class, orderTemplateId);
        if (temp != null) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("name", name);
            params.put("id", orderTemplateId);
            executeSql("UPDATE order_template SET name = :name where id = :id ", params);
            temp.setName(name);
            return temp;
        } else {
            throw new CustomErrorException("order template id is not exist");
        }
    }

    @Override
    public Page<OrderTemplateDetailDTO> getOrderTemplateByMerchantId(Pageable pageable, Long merchantId, Long langId) {
        return orderTemplateRepository.getOrderTemplateDetailByMerchantId(merchantId, langId, pageable);
    }


}
