package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.configuration.properties.LogisticsConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.CouponMerchantHis;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.OrderIncentive;
import com.nextsolutions.dcommerce.model.OrderTransferAmount;
import com.nextsolutions.dcommerce.model.order.Order;
import com.nextsolutions.dcommerce.model.order.OrderPackingV2;
import com.nextsolutions.dcommerce.repository.OrderTemplateRepository;
import com.nextsolutions.dcommerce.service.OrderV2Service;
import com.nextsolutions.dcommerce.service.PaymentGatewayService;
import com.nextsolutions.dcommerce.service.event.PushNotificationOrderEvent;
import com.nextsolutions.dcommerce.shared.constant.PriceType;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDetailInformationDto;
import com.nextsolutions.dcommerce.shared.dto.OrderPackingDTO;
import com.nextsolutions.dcommerce.shared.dto.OrderTemplateDetailDTO;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import com.nextsolutions.dcommerce.shared.dto.coupon.CouponIncentiveDTO;
import com.nextsolutions.dcommerce.shared.dto.coupon.CouponType;
import com.nextsolutions.dcommerce.shared.dto.incentivev2.DiscountType;
import com.nextsolutions.dcommerce.shared.dto.incentivev2.OrderPackingV2DTO;
import com.nextsolutions.dcommerce.shared.dto.logistic.LogisticOrderDto;
import com.nextsolutions.dcommerce.shared.dto.logistic.LogistictOrderDetailDto;
import com.nextsolutions.dcommerce.shared.dto.logistic.OutputOrderDto;
import com.nextsolutions.dcommerce.shared.dto.wallet.ProductType;
import com.nextsolutions.dcommerce.shared.dto.wallet.TransferRequestBuilder;
import com.nextsolutions.dcommerce.utils.StringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderV2ServiceImpl extends CommonServiceImpl implements OrderV2Service {
    private HttpHeaders headers;

    private final LogisticsConfiguration logisticsConfiguration;

    private final PaymentGatewayService paymentGatewayService;

    private final OrderTemplateRepository orderTemplateRepository;

    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void calculate(OrderV2Dto orderDto, Long langId, String token) throws Exception {
        Merchant merchant = find(Merchant.class, orderDto.getMerchantId());
        if (merchant == null) {
            throw new CustomErrorException("merchantIsNull");
        }
        if (orderDto.getIsSaveOrder() == null) {
            throw new CustomErrorException("isSaveOrder is not null");
        }
        if (Utils.isNotNull(orderDto.getOrderPackings())) {
            for (OrderPackingV2DTO packingDTO : orderDto.orderPackings) {
                getMoreInfo(orderDto, packingDTO, langId);
            }
            // TODO:Kiem tra co ma giam gia coupon
            calCouponIncentive(orderDto);
            orderDto.setOrderDate(Utils.getLocalDatetime());
            orderDto.setParentMerchantId(merchant.getParentMarchantId());
        }
        if (orderDto.getIsSaveOrder() == 1) {
            orderDto.setOrderId(getSequence("orders_seq"));
            orderDto.setOrderNo(Utils.getOrderNo(merchant, orderDto));
            orderDto.orderStatus = merchant.getMerchantTypeId() == 1 ? 1 : 0;
            orderDto.setMerchantCode(merchant.getMerchantCode());
            orderDto.setMerchantName(merchant.getUserName());
            orderDto.setPhone(merchant.getMobilePhone());

            // TODO: Lưu các khuyến mại được áp dụng
            if (Utils.isNotNull(orderDto.getMapIncentives())) {
                orderDto.getMapIncentives().forEach(incentiveId ->
                {
                    OrderIncentive orderIncentive = new OrderIncentive();
                    orderIncentive.setIncentiveProgramId(incentiveId);
                    orderIncentive.setOrderId(orderDto.getOrderId());
                    orderIncentive.setOrderNo(orderDto.getOrderNo());
                    save(orderIncentive);
                });
                // TODO: Lưu coupon sử dụng
                if (Utils.isNotNull(orderDto.getLstCouponCode())) {
                    orderDto.getLstCouponCode().forEach(code ->
                    {
                        CouponMerchantHis couponMerchantHis = new CouponMerchantHis();
                        couponMerchantHis.setCouponCode(code);
                        couponMerchantHis.setOrderId(orderDto.getOrderId());
                        couponMerchantHis.setMerchantId(orderDto.getMerchantId());
                        save(couponMerchantHis);
                    });

                }
            }
            if (merchant.getMerchantTypeId() == 1) {
                Order order = saveOrder(orderDto);
                orderDto.setOrderDate(order.getOrderDate());
                //TODO: Luu giao dich va chuyen tien cho NPP
                if (merchant.getWalletClientId() == null) {
                    throw new CustomErrorException("Payment account not exist");
                }
                List<OrderTransferAmount> transferDetails = new LinkedList<>();
                Map<Long, BigDecimal> sum = orderDto.calculatePackings.stream().collect(Collectors.groupingBy(OrderPackingV2::getDistributorWalletId,
                        Collectors.reducing(BigDecimal.ZERO,
                                OrderPackingV2::getAmount,
                                BigDecimal::add)));
                sum.forEach((distributorWalletId, amount) -> {
                    OrderTransferAmount transferAmount = OrderTransferAmount.builder()
                            .orderId(order.getOrderId())
                            .distributorWalletId(distributorWalletId)
                            .productId(ProductType.WALLET.value())
                            .amount(amount).status(0).build();
                    transferDetails.add(save(transferAmount));
                });
                //TODO: Chuyen tien ve tai khoan vi cua NPP
                if (order.getPaymentMethodId() != null && order.getPaymentMethodId() == 2
                        && transferDetails.size() > 0 && StringUtils.notEmpty(orderDto.tokenMifos)) {
                    transferAmount(order, merchant.getWalletClientId(), orderDto.tokenMifos, transferDetails);
                }
                //TODO: Send order to system logistic
                OutputOrderDto[] out = callServiceLogistic(token, orderDto, merchant.getFullName());
                if (out == null) {
                    throw new CustomErrorException("sendLogisticError");
                }
                order.setLogisticCode(out[0].getCode());
                order.setLogisticQRCode(out[0].getQrCode());

                orderDto.setLogisticCode(out[0].getCode());
                orderDto.setLogisticQRCode(out[0].getQrCode());

                // TODO:Update ordered
                save(order);
            } else {
                saveOrder(orderDto);
                //TODO: Push notification to L1
                applicationEventPublisher.publishEvent(new PushNotificationOrderEvent(this, orderDto, merchant.getFullName(), merchant.getMobilePhone()));
            }
        }

    }

    private void transferAmount(Order orderDto, Long merchantWalletId, String token, List<OrderTransferAmount> transferAmounts) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);
        //
        if (transferAmounts != null && transferAmounts.size() > 0) {
            //Todo: Transfer amount to distributor
            if (transferAmountToDistributor(orderDto.getOrderId(), merchantWalletId, transferAmounts, headers)) {
                //Todo: Update transfer amount status
                for (OrderTransferAmount transferAmount : transferAmounts) {
                    transferAmount.setStatus(1);
                    save(transferAmount);
                }
            }
        }
    }

    private boolean transferAmountToDistributor(Long orderId, Long clientId, List<OrderTransferAmount> details, HttpHeaders headers) {
        return paymentGatewayService.transfer(TransferRequestBuilder
                .builder()
                .merchantWalletId(clientId)
                .description("OrderId: " + orderId.toString())
                .details(details)
                .build(), headers);
    }

    private void calCouponIncentive(OrderV2Dto orderDto) throws Exception {
        if (Utils.isNotNull(orderDto.getLstCouponCode())) {
            List<String> lstCouponRequest = Utils.removeDuplicates(orderDto.getLstCouponCode());
            List<String> lstCouponInvalid = new ArrayList<>();
            for (String couponCode : lstCouponRequest) {
                CouponIncentiveDTO couponIncentiveDTO = checkCouponCode(couponCode, orderDto.merchantId);
                if (couponIncentiveDTO.getRequiredMinimumAmount() != null && orderDto.amount.compareTo(couponIncentiveDTO.getRequiredMinimumAmount()) >= 0) {
                    if (couponIncentiveDTO.getType() != null && couponIncentiveDTO.getType() == CouponType.DISCOUNT_PERCENT.value()
                            && couponIncentiveDTO.getDiscountPercent() != null) {
                        // TODO: Giam gia theo % don hang
                        double percentSale = couponIncentiveDTO.getDiscountPercent().doubleValue() / 100;
                        BigDecimal salePrice = (orderDto.getAmount().multiply(new BigDecimal(Double.toString(percentSale))));
                        BigDecimal discount = salePrice.compareTo(couponIncentiveDTO.getLimitedPrice()) < 0 ? salePrice : couponIncentiveDTO.getLimitedPrice();

                        orderDto.incentiveCoupon = orderDto.incentiveCoupon.add(discount);
                        orderDto.amount = orderDto.amount.compareTo(discount) > 0 ? orderDto.amount.subtract(discount) : BigDecimal.ZERO;

                        orderDto.addOrderIncentive(couponIncentiveDTO.getIncentiveProgramId());
                        lstCouponInvalid.add(couponCode);
                    } else if (couponIncentiveDTO.getType() != null && couponIncentiveDTO.getType() == CouponType.DISCOUNT_PRICE.value()
                            && couponIncentiveDTO.getDiscountPrice() != null) {
                        // TODO: Giam gia theo don vi
                        BigDecimal discount = couponIncentiveDTO.getDiscountPrice();
                        orderDto.amount = discount.compareTo(orderDto.amount) > 0 ? BigDecimal.ZERO : orderDto.amount.subtract(discount);
                        orderDto.incentiveCoupon = orderDto.incentiveCoupon.add(discount);
                        orderDto.addOrderIncentive(couponIncentiveDTO.getIncentiveProgramId());
                        lstCouponInvalid.add(couponCode);
                    }
                }
            }
            orderDto.setLstCouponCode(lstCouponInvalid);
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Order saveOrder(OrderV2Dto orderV2Dto) throws Exception {
        // lưu đơn hàng
        Order order = Utils.getData(orderV2Dto, Order.class);
        save(order);
        // Lưu các sản phẩm trong đơn hàng
        orderV2Dto.incentivePackings.forEach(incentivePacking -> {
            incentivePacking.setOrderId(order.getOrderId());
            save(incentivePacking);
        });
        try {
            for (int i = 0; i < orderV2Dto.calculatePackings.size(); i++) {
                if (i == 0) {
                    orderV2Dto.calculatePackings.get(i).setIsDefault(1);
                }
                orderV2Dto.calculatePackings.get(i).setOrderId(order.getOrderId());
                save(orderV2Dto.calculatePackings.get(i));
                //Todo: Cập nhật lại số suất khuyến mại
                if (orderV2Dto.calculatePackings.get(i).getIncentiveProgramId() != null
                        && orderV2Dto.calculatePackings.get(i).getIncentiveProgramDetailId() != null) {
                    updateQuantityIncentive(orderV2Dto.calculatePackings.get(i));
                }
            }
        } catch (Exception e) {
            throw new CustomErrorException("limited_slot_promotion");
        }

        return order;
    }

    private void updateQuantityIncentive(OrderPackingV2 orderPacking) throws Exception {
        int addQuantity = orderPacking.getBonusQuantity() != null ? orderPacking.getBonusQuantity() : orderPacking.getOrderQuantity();
        String updateSql = "update incentive_program_detail_information set sold = ifnull(sold,0) + ? where id = ? ";
        executeSql(updateSql, addQuantity, orderPacking.getIncentiveProgramDetailId());
    }

    @Override
    public Page<OrderV2Dto> getListOrder(Pageable pageable, OrderV2Dto orderDTO, Long langId) throws Exception {
        if (orderDTO.getOrderType() == null) {
            throw new CustomErrorException("OrderType is not null");
        }
        HashMap<String, Object> params = new HashMap<>();
        String sqlRun = " SELECT " +
                "    o.ORDER_ID orderId,o.ORDER_DATE orderDate,o.ORDER_STATUS orderStatus," +
                "    o.ORDER_NO orderNo,o.AMOUNT amount,o.MERCHANT_ID merchantId," +
                "    m.FULL_NAME merchantName, m.MERCHANT_IMG_URL as merchantImgUrl, o.PARENT_MERCHANT_ID parentMerchantId," +
                "    op.order_packing_id orderPackingId,op.PACKING_PRODUCT_ID packingProductId," +
                "    op.ORDER_QUANTITY orderQuantity,pp.PACKING_URL packingUrl," +
                "    op.origin_price orgPrice,op.price_sale price," +
                "    pd.product_name productName,m.MERCHANT_CODE merchantCode," +
                "    (SELECT COUNT(*) FROM order_packing_v2" +
                "    WHERE ORDER_ID = o.ORDER_ID) totalOrderProductQuantity" +
                " FROM " +
                "    orders o JOIN order_packing_v2 op ON o.ORDER_ID = op.ORDER_ID" +
                "    JOIN packing_product pp ON pp.PACKING_PRODUCT_ID = op.PACKING_PRODUCT_ID" +
                " JOIN packing_type pt ON pt.PACKING_TYPE_ID = pp.packing_type_id" +
                "    JOIN merchant m ON m.MERCHANT_ID = o.MERCHANT_ID" +
                "    JOIN product_description pd ON pd.PRODUCT_ID = op.PRODUCT_ID" +
                " WHERE op.is_default = 1 AND pd.language_id = :langId and o.order_status = :orderStatus ";
        String sqlWhere = "";
        params.put("langId", langId);
        params.put("orderStatus", orderDTO.getOrderStatus());

        if (orderDTO.getOrderType() == 1) {
            sqlWhere = " and o.merchant_id = :merchantId ";
            params.put("merchantId", orderDTO.getMerchantId());
        } else if (orderDTO.getOrderType() == 2) {
            sqlWhere = " and o.PARENT_MERCHANT_ID = :parentMerchantId ";
            params.put("parentMerchantId", orderDTO.getParentMerchantId());
        }

        List<OrderV2Dto> lst = findAll(pageable, OrderV2Dto.class, sqlRun + sqlWhere + " ORDER BY o.ORDER_DATE DESC ", params);
        String sqlCount = String.format("SELECT COUNT(*) FROM (%s) a", sqlRun + sqlWhere + " ORDER BY o.ORDER_DATE DESC ");

        long count = getRowCountReturnLong(sqlCount, params);
        return new PageImpl<>(lst, pageable, count);
    }

    @Override
    public OrderV2Dto getOrder(Long orderId, Long langId) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        String sqlRun = " SELECT " +
                "    o.ORDER_ID orderId,o.ORDER_DATE orderDate,o.ORDER_STATUS orderStatus," +
                "    o.ORDER_NO orderNo,o.AMOUNT amount,o.merchant_id, " +
                "    op.order_packing_id orderPackingId,op.PACKING_PRODUCT_ID packingProductId," +
                "    op.ORDER_QUANTITY orderQuantity,pp.PACKING_URL packingUrl," +
                "    pp.MARKET_PRICE orgPrice,pp.PRICE price," +
                "    pd.product_name productName" +
                "    FROM " +
                "    orders o JOIN order_packing_v2 op ON o.ORDER_ID = op.ORDER_ID" +
                "    JOIN packing_product pp ON pp.PACKING_PRODUCT_ID = op.PACKING_PRODUCT_ID" +
                "    JOIN packing_type pt ON pt.PACKING_TYPE_ID = pp.packing_type_id" +
                "    JOIN product_description pd ON pd.PRODUCT_ID = op.PRODUCT_ID" +
                "    WHERE o.ORDER_ID = :orderId AND pd.language_id = :langId ";
        params.put("orderId", orderId);
        params.put("langId", langId);

        return findOne(OrderV2Dto.class, sqlRun, params);
    }

    @Override
    public Page<OrderTemplateDetailDTO> getOrderTemplate(Pageable pageable, OrderV2Dto orderDto, Long langId) throws Exception {
        return orderTemplateRepository.getOrderTemplateDetailByMerchantId(orderDto.merchantId, langId, pageable);
    }

    @Override
    public BigDecimal getAmountOrderSuccess(Long merchantId, LocalDateTime fromDate, LocalDateTime toDate) {
        String sql = "select sum(AMOUNT) " +
                "from orders " +
                "where " +
                "merchant_id = :merchantId " +
                "and ORDER_STATUS = 2 " +
                "and date(order_success_date) >= date(:fromDate) " +
                "and date(order_success_date) <= date(:toDate) ";
        ;
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("merchantId", merchantId);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        return (BigDecimal) query.getSingleResult();
    }

    private IncentiveProgramDetailInformationDto checkIncentiveByPackingId(Long packingProductId, Long merchantId) throws Exception {
        String sqlIncentive = "Select ipd.* " +
                " from incentive_program_detail_information  ipd " +
                " JOIN incentive_program_v2 ip on ipd.incentive_program_id = ip.ID" +
                " JOIn incentive_program_merchant_group ipmg on ip.ID = ipmg.incentive_program_id" +
                " JOIN merchant_group_merchant mgm on mgm.MERCHANT_GROUP_ID = ipmg.merchant_group_id and mgm.MERCHANT_ID = ? " +
                " where ipd.packing_id = ? " +
                " %s " +
                " and ip.status = 1 " +
                " and DATE(ip.from_date) <= DATE(current_date()) and ( ip.to_date is null or DATE(ip.TO_DATE ) >=DATE(current_date())) " +
                " group by ipmg.incentive_program_id";
        Long paramMerchantId = checkMerchantId(merchantId);
        String subQueryWhere = merchantId.equals(paramMerchantId) ? " " : " AND ip.apply_l2 = true ";
        return findOne(IncentiveProgramDetailInformationDto.class, String.format(sqlIncentive, subQueryWhere), paramMerchantId, packingProductId);
    }

    private Long checkMerchantId(Long merchantId) {
        Merchant merchant = find(Merchant.class, merchantId);
        Long paramMerchantId = merchantId;
        if (merchant != null) {
            if (merchant.getMerchantTypeId() == 2) {
                paramMerchantId = merchant.getParentMarchantId();
            }
        }
        return paramMerchantId;
    }

    private CouponIncentiveDTO checkCouponCode(String couponCode, Long merchantId) throws Exception {
        String sql = " SELECT  ci.* FROM merchant_group_merchant mgm " +
                " JOIN incentive_program_merchant_group img ON mgm.MERCHANT_GROUP_ID = img.MERCHANT_GROUP_ID " +
                " JOIN incentive_program_v2 ip ON ip.ID = img.INCENTIVE_PROGRAM_ID " +
                " JOIN coupon_incentive ci ON ci.incentive_program_id = ip.ID and lower(ci.code) = lower('" + couponCode + "') " +
                " WHERE " +
                " mgm.merchant_id = :merchantId " +
                " and DATE(ip.FROM_DATE) <= DATE(CURRENT_DATE()) " +
                " and DATE(ip.TO_DATE) >= DATE(CURRENT_DATE())" +
                " and ip.status =1 " +
                " and (select count(*) from coupon_merchant_his where coupon_code = ci.code and merchant_id = :merchant_id ) < ci.limited_quantity " +
                "limit 1 ";

        CouponIncentiveDTO dto = findOne(CouponIncentiveDTO.class, sql, merchantId, merchantId);
        if (dto == null) {
            throw new CustomErrorException("invalid_coupon");
        }
        return dto;
    }

    private void getMoreInfo(OrderV2Dto orderDto, OrderPackingV2DTO orderPackingRequest, Long langId) throws Exception {
        Long packingProductId = orderPackingRequest.getPackingProductId();
        Integer orderQuantity = orderPackingRequest.getOrderQuantity();

        OrderPackingV2 packingProduct = getPackingInfo(packingProductId, langId);
        packingProduct.setOrderQuantity(orderQuantity);
        packingProduct.setAmount(packingProduct.getPrice().multiply(new BigDecimal("" + orderQuantity)));
        //Todo: Tinh khuyen mai theo packing
        IncentiveProgramDetailInformationDto incentivePackingSale = checkIncentiveByPackingId(packingProductId, orderDto.merchantId);
        if (incentivePackingSale != null) {
            //Todo: Kiểm tra số suất khuyến mại
            boolean isHasIncentive = false;
            int sold = incentivePackingSale.getSold() != null ? incentivePackingSale.getSold() : 0;
            if (incentivePackingSale.getDiscountType() == DiscountType.DISCOUNT_PERCENT.value()
                    || incentivePackingSale.getDiscountType() == DiscountType.DISCOUNT_PRICE.value()) {
                packingProduct.setRemainIncentive(incentivePackingSale.getLimitedQuantity() - sold);
                sold += packingProduct.getOrderQuantity();
                isHasIncentive = sold <= incentivePackingSale.getLimitedQuantity();
            } else if (incentivePackingSale.getDiscountType() == DiscountType.HAS_INCENTIVE_PACKING.value()) {
                sold += incentivePackingSale.getBonusPackingQuantity();
                isHasIncentive = sold <= incentivePackingSale.getLimitedPackingQuantity();
            }
            if (isHasIncentive) {
                if (incentivePackingSale.getDiscountType() == DiscountType.DISCOUNT_PERCENT.value()
                        && incentivePackingSale.getDiscountPercent() != null) {
                    double sale = incentivePackingSale.getDiscountPercent().doubleValue() / 100;
                    BigDecimal discountAPacking = (packingProduct.getPrice().multiply(new BigDecimal(Double.toString(sale))));
                    BigDecimal totalDiscount = discountAPacking.multiply(new BigDecimal("" + orderQuantity));
                    packingProduct.setIncentiveAmount(totalDiscount);
                    packingProduct.setIncentiveProgramDetailId(incentivePackingSale.getId());
                    packingProduct.setIncentiveProgramId(incentivePackingSale.getIncentiveProgramId());

                    orderPackingRequest.setHasIncentive(true);

                    packingProduct.setAmount(packingProduct.getAmount().compareTo(totalDiscount) > 0
                            ? packingProduct.getAmount().subtract(totalDiscount) : BigDecimal.ZERO);
                    orderDto.incentiveAmount = (orderDto.incentiveAmount.add(totalDiscount));
                    orderDto.addOrderIncentive(incentivePackingSale.getIncentiveProgramId());
                    // Update price
                    packingProduct.setOrgPrice(packingProduct.getPrice());
                    packingProduct.setPriceSale(packingProduct.getPrice().subtract(discountAPacking));

                } else if (incentivePackingSale.getDiscountType() == DiscountType.DISCOUNT_PRICE.value()
                        && incentivePackingSale.getDiscountPrice() != null) {
                    BigDecimal orderQuantityBigType = new BigDecimal("" + orderQuantity);
                    BigDecimal discountAPacking = incentivePackingSale.getDiscountPrice();
                    // Total discount
                    BigDecimal totalDiscount = discountAPacking.multiply(orderQuantityBigType);
                    packingProduct.setIncentiveAmount(totalDiscount);
                    packingProduct.setAmount(packingProduct.getAmount().compareTo(totalDiscount) > 0
                            ? packingProduct.getAmount().subtract(totalDiscount) : BigDecimal.ZERO);
                    packingProduct.setIncentiveProgramDetailId(incentivePackingSale.getId());
                    packingProduct.setIncentiveProgramId(incentivePackingSale.getIncentiveProgramId());

                    orderPackingRequest.setHasIncentive(true);

                    orderDto.incentiveAmount = (orderDto.incentiveAmount.add(totalDiscount));
                    orderDto.addOrderIncentive(incentivePackingSale.getIncentiveProgramId());
                    // Update price
                    packingProduct.setOrgPrice(packingProduct.getPrice());
                    packingProduct.setPriceSale(packingProduct.getPrice().subtract(discountAPacking));
                } else if (incentivePackingSale.getDiscountType() == DiscountType.HAS_INCENTIVE_PACKING.value()
                        && incentivePackingSale.getBonusPackingId() != null) {
                    if (incentivePackingSale.getRequiredMinimumPurchaseQuantity() != null
                            && packingProduct.getOrderQuantity() >= incentivePackingSale.getRequiredMinimumPurchaseQuantity()) {
                        OrderPackingV2 bonusPacking = getPackingInfo(incentivePackingSale.getBonusPackingId(), langId);
                        if (bonusPacking != null) {
                            bonusPacking.setIncentiveProgramDetailId(incentivePackingSale.getId());
                            bonusPacking.setIncentiveProgramId(incentivePackingSale.getIncentiveProgramId());
                            bonusPacking.setBonusQuantity(incentivePackingSale.getBonusPackingQuantity());
                            bonusPacking.setIsIncentive(OrderPackingDTO.INCENTIVE);
                            orderDto.incentivePackings.add(bonusPacking);

                            packingProduct.setIncentiveProgramDetailId(incentivePackingSale.getId());
                            packingProduct.setIncentiveProgramId(incentivePackingSale.getIncentiveProgramId());

                            orderPackingRequest.setHasIncentive(true);

                            packingProduct.setBonusQuantity(incentivePackingSale.getBonusPackingQuantity());
                            orderDto.addOrderIncentive(incentivePackingSale.getIncentiveProgramId());
                        }
                    }
                }
            } else if (orderPackingRequest.isHasIncentive()) {
                throw new CustomErrorException("limited_slot_promotion");
            }
        }
        // update orderDTO
        orderDto.amount = orderDto.amount.add(packingProduct.getAmount());
        orderDto.orgAmount = orderDto.orgAmount.add(packingProduct.getPrice().multiply(new BigDecimal("" + orderQuantity)));
        packingProduct.setIsIncentive(OrderPackingDTO.SALE);
        orderDto.calculatePackings.add(packingProduct);

    }

    private OutputOrderDto[] callServiceLogistic(String token, OrderV2Dto order, String fullName) {
        // HttpHeaders
        headers = new HttpHeaders();
        headers.set("Authorization", token);

        LogisticOrderDto dto = new LogisticOrderDto();
        dto.setMerchantCode(order.getMerchantCode());
        dto.setMerchantName(fullName);
        dto.setPhone(order.getPhone());
        dto.setOrderId(order.getOrderId());
        dto.setOrderCode(order.getOrderNo());
        dto.setFromStoreCode(order.getMerchantWareHouseCode());
        dto.setAmount(order.getAmount());
        dto.setOrderDate(order.getOrderDate());
        List<LogistictOrderDetailDto> lstDetailDtos = new ArrayList<>();
        for (OrderPackingV2 packing : order.calculatePackings) {
            if (packing.getIsIncentive() == OrderPackingDTO.SALE) {
                LogistictOrderDetailDto detailDto = new LogistictOrderDetailDto();
                detailDto.setQuantity(packing.getOrderQuantity());
                detailDto.setProductPackingCode(packing.getPackingProductCode());
                detailDto.setPackingName(packing.getProductName());
                detailDto.setType(packing.getIsIncentive());
                detailDto.setPrice(packing.getPrice());
                lstDetailDtos.add(detailDto);
            }
        }
        dto.setOrderDetails(lstDetailDtos);

        RestTemplate restTemplate = new RestTemplate();
        List<LogisticOrderDto> lst = new ArrayList<>();
        lst.add(dto);
        HttpEntity<List<LogisticOrderDto>> requestBody = new HttpEntity<>(lst, headers);

        ResponseEntity<OutputOrderDto[]> result
                = restTemplate.postForEntity(logisticsConfiguration.getMerchantOrdersUrl(), requestBody, OutputOrderDto[].class);
        return result.getBody();

    }


    private OrderPackingV2 getPackingInfo(Long packingId, Long langId) throws Exception {
        String sqlInfo = "select pp.PRODUCT_ID,pp.PACKING_PRODUCT_ID,pd.PRODUCT_NAME,pp.PACKING_URL,pt.QUANTITY,  " +
                " pp.MARKET_PRICE orgPrice,pprice.PRICE, pprice.PRICE priceSale, " +
                " pp.DISTRIBUTOR_CODE distributorCode ,pp.DISTRIBUTOR_ID distributorId,pp.DISTRIBUTOR_WALLET_ID distributorWalletId, " +
                " pp.CODE packingProductCode  " +
                " from packing_product pp  " +
                " join product_description pd on pp.product_id  = pd.PRODUCT_ID  " +
                " join packing_type pt on pt.PACKING_TYPE_ID  = pp.PACKING_TYPE_ID  " +
                " join packing_price pprice on pprice.packing_product_id = pp.PACKING_PRODUCT_ID " +
                " where pp.PACKING_PRODUCT_ID = ? and pd.LANGUAGE_ID = ? " +
                " and pprice.price_type = ? " +
                " and date(current_date()) >= date(pprice.from_date) and (date(pprice.to_date)>= date(current_date()) or pprice.to_date is null)";
        return findOneV2(OrderPackingV2.class, sqlInfo, packingId, langId, PriceType.SALE_OUT.value());
    }


}
