package com.nextsolutions.dcommerce.service.event;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;
@Getter
public class PushNotificationConfirmOrder extends ApplicationEvent {
    private OrderV2Dto orderDto;
    public PushNotificationConfirmOrder(Object source, OrderV2Dto orderV2Dto) {
        super(source);
        this.orderDto = orderV2Dto;
    }
}
