package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.model.loyalty.Loyalty;
import com.nextsolutions.dcommerce.shared.dto.LoyaltyDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface LoyaltyService extends CommonService {

    List<LoyaltyDto> findActive(Integer langId) throws Exception;

    Integer activateProgram(Long id) throws Exception;

    Integer deactivateProgram(Long id) throws Exception;

    List<Loyalty> getLoyaltyFromPackingId(Long ictPrgId) throws Exception;

    Loyalty insert(LoyaltyDto dto) throws Exception;

    Map<String, String> uploadFile(MultipartFile image) throws IOException;

}
