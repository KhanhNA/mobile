package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.repository.ProductRepository;
import com.nextsolutions.dcommerce.service.ProductManagementService;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.shared.dto.ProductDataDto;
import com.nextsolutions.dcommerce.shared.dto.ProductDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.ProductGeneralDto;
import com.nextsolutions.dcommerce.shared.dto.product.*;
import com.nextsolutions.dcommerce.shared.dto.product.packing.PackingProductDto;
import com.nextsolutions.dcommerce.shared.mapper.ProductManagementMapper;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.UnitUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductManagementServiceImpl implements ProductManagementService {

    private final ProductRepository productRepository;
    private final ProductManagementMapper productManagementMapper;

    @Override
    @Transactional
    public ProductGeneralDto createProduct(ProductGeneralDto productGeneralDto) {
        Product product = productManagementMapper.toProduct(productGeneralDto);
        Product createdProduct = productRepository.save(product);
        createdProduct.getProductDescriptions().forEach(productDescription -> productDescription.setProductId(product.getId()));
        Product result = productRepository.save(createdProduct);
        return productManagementMapper.toProductGeneralDto(result);
    }

    @Override
    @Transactional
    public ProductGeneralDto updateProductGeneral(Long id, ProductGeneralDto productGeneralDto) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> createEntityNotFoundException(id));
        product.setCode(productGeneralDto.getCode());
        product.setName(productGeneralDto.getInternationalName());

        List<ProductDescription> productDescriptions = product.getProductDescriptions();
        if (Objects.isNull(productDescriptions) || productDescriptions.isEmpty()) {
            product.setProductDescriptions(productGeneralDto.getDescriptions()
                    .stream()
                    .map(productDescriptionDto -> {
                        ProductDescription productDescription = new ProductDescription();
                        productDescription.setProductId(id);
                        mapMultipleProductDescriptionProperties(productDescriptionDto, productDescription);
                        return productDescription;
                    }).collect(Collectors.toList()));
        } else {
            Map<Long, ProductDescriptionDto> productDescriptionMapLanguage = productGeneralDto.getDescriptions()
                    .stream()
                    .collect(Collectors.toMap(
                            productDescriptionDto -> productDescriptionDto.getLanguageId().longValue(),
                            productDescriptionDto -> productDescriptionDto)
                    );
            productDescriptions.forEach(productDescription -> {
                ProductDescriptionDto productDescriptionDto = productDescriptionMapLanguage.get(productDescription.getLanguageId());
                mapMultipleProductDescriptionProperties(productDescriptionDto, productDescription);
                productDescriptionMapLanguage.remove(productDescription.getLanguageId());
            });

            List<ProductDescription> newProductDescriptions = productDescriptionMapLanguage.values()
                    .stream()
                    .map(productDescriptionDto -> {
                        ProductDescription productDescription = new ProductDescription();
                        productDescription.setProductId(id);
                        mapMultipleProductDescriptionProperties(productDescriptionDto, productDescription);
                        return productDescription;
                    }).collect(Collectors.toList());
            productDescriptions.addAll(newProductDescriptions);
        }

        Product updated = productRepository.saveAndFlush(product);
        return productManagementMapper.toProductGeneralDto(updated);
    }

    @Override
    public ProductGeneralDto getProductGeneral(Long id) {
        return productRepository.findById(id)
                .map(productManagementMapper::toProductGeneralDto)
                .orElseThrow(() -> createEntityNotFoundException(id));
    }

    @Override
    @Transactional
    public ProductDataDto updateProductData(Long id, ProductDataDto productDataDto) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> createEntityNotFoundException(id));
        product.setCode(productDataDto.getCode());
        product.setBrand(productDataDto.getBrand());
        product.setOrigin(productDataDto.getOrigin());
        product.setSku(productDataDto.getSku());

        product.setLength(productDataDto.getLength());
        product.setWidth(productDataDto.getWidth());
        product.setHeight(productDataDto.getHeight());
        product.setLengthClass(productDataDto.getLengthClass());

        switch (product.getLengthClass()) {
            case "m":
                product.setCalculatedWidth(UnitUtils.convertMetersToMillimeters(product.getWeight()));
                product.setCalculatedHeight(UnitUtils.convertMetersToMillimeters(product.getHeight()));
                product.setCalculatedLength(UnitUtils.convertMetersToMillimeters(product.getLength()));
                break;
            case "cm":
                product.setCalculatedWidth(UnitUtils.convertCentimetersToMillimeters(product.getWeight()));
                product.setCalculatedHeight(UnitUtils.convertCentimetersToMillimeters(product.getHeight()));
                product.setCalculatedLength(UnitUtils.convertCentimetersToMillimeters(product.getLength()));
                break;
            default:
                product.setCalculatedWidth(product.getWeight());
                product.setCalculatedHeight(product.getHeight());
                product.setCalculatedLength(product.getLength());
                break;
        }

        product.setWeight(productDataDto.getWeight());
        product.setWeightClass(productDataDto.getWeightClass());

        switch (product.getWeightClass()) {
            case "kg":
                product.setCalculatedWeight(UnitUtils.convertKilogramsToGrams(product.getWeight()));
                break;
            case "mg":
                product.setCalculatedWeight(UnitUtils.convertMilligramsToGrams(product.getWeight()));
                break;
            case "lb":
                product.setCalculatedWeight(UnitUtils.convertPoundsToGrams(product.getWeight()));
                break;
            case "oz":
                product.setCalculatedWeight(UnitUtils.convertOuncesToGrams(product.getWeight()));
                break;
            default:
                product.setCalculatedWeight(product.getWeight());
                break;
        }

        product.setLifecycle(productDataDto.getLifecycle());
        product.setStatus(productDataDto.getStatus());
        product.setDateAvailable(productDataDto.getDateAvailable());

        productRepository.saveAndFlush(product);

        return productManagementMapper.toProductDataDto(product);
    }

    @Override
    public ProductDataDto getProductData(Long id) {
        return productRepository.findById(id)
                .map(productManagementMapper::toProductDataDto)
                .orElseThrow(() -> createEntityNotFoundException(id));
    }

    @Override
    public ProductLinksDto getProductLinksDto(Long id) {
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());
        ProductLinks productLinks = productRepository.getProductLinks(id, languageId);
        return productManagementMapper.toProductLinksDto(productLinks);
    }

    @Override
    @Transactional
    public ProductLinksDto updateProductLinks(Long id, ProductLinksDto productLinksDto) {
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());
        ProductLinks productLinks = productManagementMapper.toProductLinks(productLinksDto);
        productRepository.updateProductLinks(id, productLinks);
        ProductLinks productLinksInDb = productRepository.getProductLinks(id, languageId);
        return productManagementMapper.toProductLinksDto(productLinksInDb);
    }

    @Override
    public Page<ProductLinksManufacturerDto> getProductLinksManufacturers(String keyword, Pageable pageable) {
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());
        Page<ManufacturerLink> manufacturerLinksPage = productRepository.getProductLinksManufacturers(keyword, languageId, pageable);
        return manufacturerLinksPage.map(productManagementMapper::toProductLinksManufacturerDto);
    }

    @Override
    public Page<ProductLinksDistributorDto> getProductLinksDistributors(String keyword, Pageable pageable) {
        Long languageId = LanguageUtils.getLanguageId(LocaleContextHolder.getLocale().getLanguage());
        Page<DistributorLink> distributorLinksPage = productRepository.getProductLinksDistributors(keyword, languageId, pageable);
        return distributorLinksPage.map(productManagementMapper::toProductLinksDistributorDto);
    }

    @Override
    public List<ProductOptionsDto> getProductOptions(Long id) {
        List<ProductOptions> productOptions = productRepository.getProductOptions(id);
        return productManagementMapper.toProductOptionsDtos(productOptions);
    }

    @Override
    public void updateProductOptions(Long id, List<ProductOptionsDto> productOptionsDtos) {
        List<ProductOptions> options = productOptionsDtos.stream()
                .map(productManagementMapper::toProductOptions)
                .collect(Collectors.toList());

        productRepository.updateProductOptions(id, options);
    }

    @Override
    public Page<PackingProductDto> getPackingProducts(Long id, Pageable pageable) {
        Page<PackingProduct> packingProductPage = productRepository.findAllPackingProducts(id, pageable);
        return packingProductPage.map(productManagementMapper::toPackingProductDto);
    }

    private EntityNotFoundException createEntityNotFoundException(Long id) {
        return new EntityNotFoundException("Product with id = " + id + " not found");
    }

    private void mapMultipleProductDescriptionProperties(
            ProductDescriptionDto productDescriptionDto,
            ProductDescription productDescription
    ) {
        productDescription.setLanguageId(productDescriptionDto.getLanguageId());
        productDescription.setProductName(productDescriptionDto.getLocalizedName());
        productDescription.setTitle(productDescriptionDto.getTitle());
        productDescription.setDescription(productDescriptionDto.getDescription());
        productDescription.setMetaTitle(productDescriptionDto.getMetaTagTitle());
        productDescription.setMetaDescription(productDescriptionDto.getMetaTagDescription());
        productDescription.setMetaKeywords(productDescriptionDto.getMetaTagKeywords());
    }
}
