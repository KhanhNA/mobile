package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.GrouponRegister;
import com.nextsolutions.dcommerce.model.GrouponRegisterDetail;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.incentive.IncentiveLevel;
import com.nextsolutions.dcommerce.model.incentive.Incentive;
import com.nextsolutions.dcommerce.service.GrouponService;
import com.nextsolutions.dcommerce.shared.dto.GrouponRegistryDTO;
import com.nextsolutions.dcommerce.shared.dto.GrouponRegistryStatusDTO;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
public class GrouponServiceImpl extends CommonServiceImpl implements GrouponService {

    @Override
    public GrouponRegister register(GrouponRegisterDetail groupon) throws Exception {
//        if (groupon.getIncentiveProgramId() == null || groupon.getMerchantId() == null || groupon.getPackingProductId() == null
//                || groupon.getRegisterQuantity() == null) {
//            throw new CustomErrorException("DataError");
//        }
//        Incentive incentive = getIncentivProgramGroupon(groupon.getIncentiveProgramId());
//        if (incentive == null) {
//            throw new CustomErrorException("incentiveProgramIsNull");
//        }
//        if (incentive.getType() != 3) {
//            throw new CustomErrorException("incentiveProgramNotIsGroup");
//        }
//
//        Merchant merchant = find(Merchant.class, groupon.getMerchantId());
//        if (merchant == null) {
//            throw new CustomErrorException("merchantIsNull");
//        }
//        PackingProduct packingProduct = find(PackingProduct.class, groupon.getPackingProductId());
//        if (packingProduct == null) {
//            throw new CustomErrorException("packingProductIsNull");
//        }
//
//        Long merchantL1Id = merchant.getParentMarchantId() == null ? merchant.getMerchantId() : merchant.getParentMarchantId();
//
//        GrouponRegisterDetail entityDetail = getDetail(groupon.getIncentiveProgramId(), groupon.getMerchantId(), groupon.getPackingProductId());
//        GrouponRegister grouponRegister = getGrouponTotal(groupon.getIncentiveProgramId(), merchantL1Id, groupon.getPackingProductId());
//        if (grouponRegister == null) {
//            grouponRegister = new GrouponRegister();
//            grouponRegister.setIncentiveProgramId(groupon.getIncentiveProgramId());
//            grouponRegister.setMerchantL1Id(merchantL1Id);
//            grouponRegister.setPackingProductId(groupon.getPackingProductId());
//            grouponRegister.setTotalQuantity(0d);
//            grouponRegister.setTotalAmount(0d);
//            grouponRegister.setCreateDate(LocalDateTime.now());
//            grouponRegister.setCreateUser("thonv");
//        } else {
//            grouponRegister.setUpdateDate(LocalDateTime.now());
//            grouponRegister.setUpdateUser("thonv");
//        }
//        Integer oldRegisterQuantity = 0, newQuantity = 0;
//        if (entityDetail == null) {
//            entityDetail = new GrouponRegisterDetail();
//            entityDetail.setCreateDate(LocalDateTime.now());
//            entityDetail.setCreateUser("thonv");
//            oldRegisterQuantity = 0;
//            newQuantity = groupon.getRegisterQuantity();
//        } else {
//            entityDetail.setUpdateDate(LocalDateTime.now());
//            entityDetail.setUpdateUser("thonv");
//            oldRegisterQuantity = entityDetail.getRegisterQuantity();
//            newQuantity = groupon.getRegisterQuantity() - entityDetail.getRegisterQuantity();
//        }
//        grouponRegister.setTotalQuantity(grouponRegister.getTotalQuantity() + newQuantity);
//        grouponRegister.setTotalAmount(grouponRegister.getTotalQuantity() * packingProduct.getPrice());
//
//        updateGrouponLevel(grouponRegister, packingProduct);
//
//        grouponRegister = save(grouponRegister);
//        entityDetail.setIncentiveProgramId(groupon.getIncentiveProgramId());
//        entityDetail.setMerchantId(groupon.getMerchantId());
//        entityDetail.setPackingProductId(groupon.getPackingProductId());
//        entityDetail.setRegisterQuantity(groupon.getRegisterQuantity());
//        entityDetail.setRegisterAmount(groupon.getRegisterQuantity() * packingProduct.getPrice());
//        entityDetail.setLastRegisterDate(LocalDateTime.now());
//        entityDetail.setStatus(1);
//        entityDetail.setGrouponRegisterId(grouponRegister.getId());
//        entityDetail.setMerchantL1Id(merchantL1Id);
//        save(entityDetail);
//        return grouponRegister;
        return null;
    }

    @Override
    public GrouponRegistryStatusDTO getGrouponStatus(Long registerId) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT ");
        sql.append(" gr.id,gr.total_amount totalAmount,gr.total_quantity totalQuantity, ");
        sql.append(" gr.total_amount_next totalAmountNext,gr.total_quantity_next totalQuantityNext,gr.incentive_amount incentiveAmount, ");
        sql.append(" gr.incentive_percent incentivePercent,gr.incentive_amount_next incentiveAmountNext, ");
        sql.append(" gr.incentive_percent_next incentivePercentNext,gr.packing_product_id packingProductId, ");
        sql.append(" pp.PRODUCT_ID productId,pp.PRICE price, ");
        sql.append(" pp.PACKING_URL url,pt.QUANTITY,  pp.NAME productName ");
        sql.append(" FROM ");
        sql.append(" groupon_register gr ");
        sql.append(" JOIN packing_product pp on gr.packing_product_id = pp.packing_product_id ");
        sql.append(" JOIN packing_type pt on pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID ");
        sql.append(" WHERE ");
        sql.append(" id = :id ");
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", registerId);
        List<GrouponRegistryStatusDTO> registryDTOList = findAll(null, GrouponRegistryStatusDTO.class, sql.toString(), params);
        if (registryDTOList != null && registryDTOList.size() > 0) {
            return registryDTOList.get(0);
        }
        return null;
    }

    @Override
    public Page<GrouponRegistryDTO> getListRegistry(Pageable pageable, Long registerId) throws Exception {
        String sqlFrom = " FROM groupon_register_detail grd \n" +
                "JOIN merchant m on m.MERCHANT_ID = grd.merchant_id\n" +
                " where grd.groupon_register_id = :id ";
        StringBuilder sql = new StringBuilder();
        sql.append(sqlSelectListRegistry());
        sql.append(sqlFrom);

        HashMap<String, Object> params = new HashMap<>();
        params.put("id", registerId);

        List<GrouponRegistryDTO> registryDTOList = findAll(pageable, GrouponRegistryDTO.class, sql.toString(), params);
        //
        String sqlCount = sql.toString().replaceAll(sqlSelectListRegistry(), "select count(*) ");
        Query nativeQueryCount = entityManager.createNativeQuery(sqlCount);
        nativeQueryCount.setParameter("id", registerId);
        BigInteger total = (BigInteger) nativeQueryCount.getSingleResult();
        return new PageImpl<>(registryDTOList, pageable, total.intValue());
    }

    @Override
    public List<GrouponRegister> getListGrouponProgram(Long parentMerchantId) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        String sql = "SELECT gr.incentive_program_id incentiveProgramId," +
                " CONCAT(ip.name, '- ',gr.packing_product_id) promotionName,gr.id " +
                " FROM dcommerce.groupon_register gr JOIN " +
                " incentive_program ip ON ip.id = gr.incentive_program_id\n" +
                " WHERE merchant_l1_id = :parentMerchantId";
        params.put("parentMerchantId", parentMerchantId);
        return findAll(null, GrouponRegister.class, sql, params);
    }

    private String sqlSelectListRegistry() {
        return "SELECT grd.merchant_id merchantId, m.full_name merchantName,grd.register_quantity registerQuantity,grd.register_amount registerAmount ";
    }

    private void updateGrouponLevel(GrouponRegister grouponRegister, PackingProduct packingProduct) throws Exception {
//        String sql = "from IncentiveLevel " +
//                "       where incentiveProgramId = :incentiveProgramId " +
//                "           and packingProductId = :packingProductId " +
//                "           and status = 1 " +
////                "           and minQuantity > :minQuantity " +
//                "       order by minQuantity";
//        HashMap<String, Object> params = new HashMap<>();
//        params.put("incentiveProgramId", grouponRegister.getIncentiveProgramId());
//        params.put("packingProductId", grouponRegister.getPackingProductId());
//
//        List<IncentiveLevel> lstIctLevels = Utils.safe(findAll(sql, null, params, 0));
//        if (lstIctLevels.size() == 0) {
//            throw new CustomErrorException("IncentiveLevelNotExist");
//        }
//        IncentiveLevel lvlGet = null, lvlNext = null;
//        Long count;
//        double totalQuantity = grouponRegister.getTotalQuantity();
//        double totalIncentive = 0, incentive = 0;
//        boolean isFirst = true;
//        int ind = 0;
//        while (ind < lstIctLevels.size()) {
//            if (grouponRegister.getTotalQuantity() >= lstIctLevels.get(ind).getMinQuantity()) {
//                lvlGet = lstIctLevels.get(ind);
//                ind++;
//                continue;
//            } else {
//                lvlNext = lstIctLevels.get(ind);
//
//                break;
//            }
//
//
//        }
//
//
//        if (lvlGet != null) {
//            count = (long) (grouponRegister.getTotalQuantity() / lvlGet.getMinQuantity());
//            totalIncentive = lvlGet.getIncentiveAmount() * count + lvlGet.getIncentivePercent() * grouponRegister.getTotalAmount();
//            if (lvlGet.getIncentiveAmountMax() != null && lvlGet.getIncentiveAmountMax() > 0 && totalIncentive > lvlGet.getIncentiveAmountMax()) {
//                totalIncentive = lvlGet.getIncentiveAmountMax();
//            }
//
//            grouponRegister.setIncentiveAmount(lvlGet.getIncentiveAmount());
//            grouponRegister.setIncentivePercent(lvlGet.getIncentivePercent());
//            grouponRegister.setIncentiveLevelId(lvlGet.getId());
//            grouponRegister.setTotalIncentive(totalIncentive);
//        }
//
//        if (ind == lstIctLevels.size() && lvlGet != null) { //truong hop het cac muc, kiem tra xem co tinh luy ke khong
//            if (lvlGet.getIncentiveAmount() > 0 && (lvlGet.getIsMultiple() != null && lvlGet.getIsMultiple() == 1)) {
//                count = (long) (grouponRegister.getTotalQuantity() / lvlGet.getMinQuantity()) + 1;
//                totalIncentive = lvlGet.getIncentiveAmount() * count + lvlGet.getIncentivePercent() * grouponRegister.getTotalAmount();
//                if (lvlGet.getIncentiveAmountMax() == null || lvlGet.getIncentiveAmountMax() == 0 || totalIncentive < lvlGet.getIncentiveAmountMax()) {
//                    grouponRegister.setIncentiveAmountNext(lvlGet.getIncentiveAmount() * count);
//                    grouponRegister.setIncentivePercentNext(lvlGet.getIncentivePercent());
//                    grouponRegister.setTotalQuantityNext(new Double(lvlGet.getMinQuantity() * count));
//                    grouponRegister.setTotalAmountNext(lvlGet.getMinQuantity() * packingProduct.getPrice());
//
//                    grouponRegister.setIncentiveLevelIdNext(lvlGet.getId());
//                }
//            }
//        } else {
//            if (lvlNext != null && (lvlGet == null || (lvlNext.getMinQuantity() > lvlGet.getMinQuantity()))) {
//                grouponRegister.setIncentiveAmountNext(lvlNext.getIncentiveAmount());
//                grouponRegister.setIncentivePercentNext(lvlNext.getIncentivePercent());
//                grouponRegister.setTotalQuantityNext(new Double(lvlNext.getMinQuantity()));
//                grouponRegister.setTotalAmountNext(lvlNext.getMinQuantity() * packingProduct.getPrice());
//
//                grouponRegister.setIncentiveLevelIdNext(lvlNext.getId());
//            } else {
//                grouponRegister.setIncentiveAmountNext(0d);
//                grouponRegister.setIncentivePercentNext(0d);
//                grouponRegister.setTotalQuantityNext(0d);
//                grouponRegister.setTotalAmountNext(0d);
//
//                grouponRegister.setIncentiveLevelIdNext(null);
//            }
//
//
//        }
    }

    private GrouponRegisterDetail getDetail(Long ictProgramId, Long merchantId, Long packingProductId) {
        String sql = "from GrouponRegisterDetail " +
                "       where incentiveProgramId = :incentiveProgramId " +
                "           and merchantId = :merchantId " +
                "           and packingProductId = :packingProductId " +
                "           and status = 1";
        HashMap<String, Object> params = new HashMap<>();
        params.put("incentiveProgramId", ictProgramId);
        params.put("merchantId", merchantId);
        params.put("packingProductId", packingProductId);
        List<GrouponRegisterDetail> lstGroupons = findAll(sql, null, params, 1);
        GrouponRegisterDetail entity = null;

        if (lstGroupons != null && lstGroupons.size() > 0) {
            entity = lstGroupons.get(0);
        }
        return entity;
    }

    private GrouponRegister getGrouponTotal(Long incentiveProgramId, Long merchantL1Id, Long packingProductId) {
        String sql = "from GrouponRegister " +
                "       where incentiveProgramId = :incentiveProgramId " +
                "           and merchantL1Id = :merchantL1Id " +
                "           and packingProductId = :packingProductId ";
        HashMap<String, Object> params = new HashMap<>();
        params.put("incentiveProgramId", incentiveProgramId);
        params.put("merchantL1Id", merchantL1Id);
        params.put("packingProductId", packingProductId);
        List<GrouponRegister> lstGroupons = findAll(sql, null, params, 1);
        GrouponRegister entity = null;

        if (lstGroupons != null && lstGroupons.size() > 0) {
            entity = lstGroupons.get(0);
        }
        return entity;
    }

    private Incentive getIncentivProgramGroupon(Long incentiveProgramId) {
        String sql = "from IncentiveProgram " +
                "       where id = :incentiveProgramId " +
                "           and type = 3 and status = 1" +
                "           and fromDate <= curDate() and (toDate is null or toDate >= curDate()) ";
        HashMap<String, Object> params = new HashMap<>();
        params.put("incentiveProgramId", incentiveProgramId);

        List<Incentive> lstIctPrograms = findAll(sql, null, params, 1);
        Incentive entity = null;

        if (lstIctPrograms != null && lstIctPrograms.size() > 0) {
            entity = lstIctPrograms.get(0);
        }
        return entity;
    }
}
