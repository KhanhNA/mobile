package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.ManufacturerDto;
import com.nextsolutions.dcommerce.shared.dto.TemporaryManufacturerDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.ManufacturerResource;
import com.nextsolutions.dcommerce.ui.model.request.TemporaryManufacturerRequestModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManufacturerService {
    ManufacturerDto findById(Long id);

    ManufacturerDto requestCreateManufacturer(ManufacturerDto manufacturerDto);

    Page<ManufacturerResource> getManufacturers(ManufacturerResource resource, Pageable pageable);

    ManufacturerDto requestUpdateManufacturer(Long id, ManufacturerDto form);

    ManufacturerDto updateManufacturerByManager(Long id, TemporaryManufacturerRequestModel form);

    void requestDelete(Long id);

    void inactiveManufacturer(Long id);

    boolean existsByCode(String code);

    boolean existsByCode(Long id, String code);

    boolean existsByTel(String tel);

    boolean existsByTel(Long id, String tel);

    boolean existsByTaxCode(String taxCode);

    boolean existsByEmail(String email);

    boolean existsByTaxCode(Long id,String taxCode);

    boolean existsByEmail(Long id,String email);
    TemporaryManufacturerDTO getManufacturersToCompare(Long id);

    ManufacturerDto managerReject(Long id);

    ManufacturerDto managerApproved(Long id);
}
