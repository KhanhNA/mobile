package com.nextsolutions.dcommerce.service.impl;

import com.nextsolutions.dcommerce.repository.MerchantAddressRepository;
import com.nextsolutions.dcommerce.model.MerchantAddress;
import com.nextsolutions.dcommerce.service.MerchantAddressService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MerchantAddressServiceImpl implements MerchantAddressService {

    private final MerchantAddressRepository merchantAddressRepository;

    public MerchantAddressServiceImpl(MerchantAddressRepository merchantAddressRepository) {
        this.merchantAddressRepository = merchantAddressRepository;
    }


    @Override
    public Page<MerchantAddress> findAll(Pageable pageable) {
        return merchantAddressRepository.findAll(pageable);
    }

    @Override
    public MerchantAddress get(Long aLong) {
        return merchantAddressRepository.findById(aLong).orElse(null);
    }

    @Override
    public void save(MerchantAddress object) {
        if (object.getIsSelect() == 1) {
            MerchantAddress merchantAddress;
            if (object.getId() != null) {
                merchantAddress = merchantAddressRepository.getMerchantAddressSelected(object.getMerchantId(), object.getId());
            } else {
                merchantAddress = merchantAddressRepository.getMerchantAddressSelected(object.getMerchantId());
            }

            if (merchantAddress != null) {
                merchantAddress.setIsSelect(0);
                merchantAddressRepository.save(merchantAddress);
            }
        }
        merchantAddressRepository.save(object);
    }

    @Override
    public void delete(Long aLong) {
        merchantAddressRepository.deleteById(aLong);
    }

    @Override
    public Page<MerchantAddress> findAllByMerchant(Pageable pageable, Long merchantId) {
        return merchantAddressRepository.findByMerchantId(pageable, merchantId);
    }

    @Override
    public MerchantAddress findByMerchantIdAndAddress(Long merchantId, String address) {
        return merchantAddressRepository.findByMerchantIdAndAddress(merchantId, address);
    }


    @Override
    public void deleteMerchantAddress(MerchantAddress merchantAddress) {
        merchantAddressRepository.delete(merchantAddress);
    }

    @Override
    public long countByMerchantId(Long merchantId) {
        return merchantAddressRepository.countMerchantIdBy(merchantId);
    }
}
