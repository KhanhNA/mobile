package com.nextsolutions.dcommerce.service;

import com.nextsolutions.dcommerce.shared.dto.AttributeDescriptionDTO;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.MerchantOnMapDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MerchantService {

    void register(MerchantDto dto) throws Exception;

    void active(MerchantDto dto) throws Exception;

    void deactive(MerchantDto dto) throws Exception;

    Page<MerchantDto> findAll(Pageable pageable, MerchantDto dto, Integer isPage) throws Exception;

    Page<Merchant> listMerchant(Pageable pageable, List<Long> exceptIds, String text) throws Exception;

    void deleteToken(Long id, String token);

    ResponseEntity<Object> createL1(MerchantDto merchantDto, MultipartFile[] multipartFiles) throws Exception;

    List<AttributeDescriptionDTO> getAttribute(Long merchantId, Long langId) throws Exception;

    Merchant findByUserName(String userName, Long langId) throws Exception;

    @SuppressWarnings("unchecked")
    List<MerchantGroupResource> getGroupMerchant(Long merchantId, Long languageId);

    Page<MerchantOnMapDTO> getDataOnMap(Pageable pageable, Long parentId, Integer status, Integer action, boolean isPageable) throws Exception;

    List<AttributeDescriptionDTO> getAttributeOfMerchant(Long merchantId, Long langId) throws Exception;

    Merchant updateMerchantInfo(MerchantDto merchantRequest);

}
