package com.nextsolutions.dcommerce.service.impl;


import com.nextsolutions.dcommerce.configuration.properties.AppConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.MerchantImages;
import com.nextsolutions.dcommerce.shared.dto.FileUploadDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.UUID;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;
    private final String tempExportExcel;
    private final String uploadPath;
    private static MimetypesFileTypeMap mimeType = new MimetypesFileTypeMap();

    @Autowired
    public FileStorageService(AppConfiguration appConfiguration) {
        this.fileStorageLocation = Paths.get(appConfiguration.getUploadPath()).toAbsolutePath().normalize();
        this.tempExportExcel = appConfiguration.getUploadPath();
        this.uploadPath = appConfiguration.getUploadPath();
    }

    public String getTempExportExcel() {
        return this.tempExportExcel;
    }

    public Resource loadFileAsResource(String fileName) throws Exception {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new CustomErrorException("File not found " + fileName);
            }
        } catch (Exception ex) {
            throw new CustomErrorException("File not found " + fileName + "\n" + ex.getMessage());
        }
    }

    public HttpHeaders loadHttpHeadersPDF(Resource resource) throws Exception {
        mimeType.addMimeTypes("application/pdf pdf"); //set header pdf load pdf
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, mimeType.getContentType(resource.getFile()));
        return headers;
    }
    public HttpHeaders downloadHttpHeaders(Resource resource) throws Exception {
        //mimeType.addMimeTypes("application/pdf pdf"); //download
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"");
        return headers;
    }

    public HttpHeaders loadHttpHeaders(Resource resource) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, new MimetypesFileTypeMap().getContentType(resource.getFile()));
        return headers;
    }




    public void storeFile(MultipartFile file) throws IOException {
        Path filePath = Paths.get(fileStorageLocation + "/" + UUID.randomUUID().toString() + "_" + file.getOriginalFilename());

        Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
    }

    public Path saveFileToStorage(MultipartFile file, String directory) throws IOException {
        Path directoryPath = Paths.get(this.fileStorageLocation + "/" + directory);
        Files.createDirectories(directoryPath);
        String fileName = UUID.randomUUID().toString().replaceAll("-", "") + "_" + file.getOriginalFilename();
        Path filePath = Paths.get(directoryPath + "/" + fileName);
        Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        return filePath;
    }

    public String toRelativePath(Path path) {
        String replacement = uploadPath.endsWith("/") ? "/" : "";
        return path.toString().replaceAll(uploadPath, replacement);
    }

    public String storeCustomFilePath(MultipartFile file) throws IOException {
        String fileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
        Path filePath = Paths.get(fileStorageLocation + "/" + fileName);

        Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        return fileName;
    }

    public ArrayList<FileUploadDTO> storeFileRandomName(MultipartFile[] files) throws IOException {
        ArrayList<FileUploadDTO> dtos = new ArrayList<>();
        FileUploadDTO fileUploadDTO;
        if (files != null && files.length > 0) {
            for (MultipartFile file : files) {
                String generalName = "";
                generalName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
                Path filePath = Paths.get(fileStorageLocation + "/" + generalName);
                Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
                fileUploadDTO = new FileUploadDTO();
                fileUploadDTO.setFileName(generalName);
                dtos.add(fileUploadDTO);
            }
        }

        return dtos;
    }

    /*
    Luu anh CMND
     */
    public ArrayList<String> saveFileIdentity(Long merchantId, MultipartFile[] files) throws Exception {
        String pathIdentity = "merchant/" + merchantId + "/identity";
        String local = uploadPath + pathIdentity;
        ArrayList<String> filesUpload = new ArrayList<>();
        Path path = Paths.get(local).toAbsolutePath().normalize();
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
        } catch (Exception ex) {
            throw new CustomErrorException("Could not create the directory where the uploaded files will be stored.\n" + ex.getMessage());
        }
        if (files != null && files.length > 0) {
            for (MultipartFile file : files) {
                String generalName = "";
                generalName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
                Path filePath = Paths.get(path + "/" + generalName);
                Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
                filesUpload.add(pathIdentity + "/" + generalName);
            }
        }
        return filesUpload;
    }

    /*
    Luu avatar
     */
    public String saveImgAvatar(Long merchantId, MultipartFile file) throws Exception {
        String pathAvatar = "merchant/" + merchantId + "/avatar";
        String localStore = uploadPath + pathAvatar;
        String generalName = "";
        Path path = Paths.get(localStore).toAbsolutePath().normalize();
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
        } catch (Exception ex) {
            throw new CustomErrorException("Could not create the directory where the uploaded files will be stored.\n" + ex.getMessage());
        }
        if (file != null) {
            generalName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
            Path filePath = Paths.get(path + "/" + generalName);
            Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        }
        return pathAvatar + "/" + generalName;
    }

    /*
       Luu hinh anh hop dong
        */
    public ArrayList<MerchantImages> saveImgContract(Long merchantId, Integer type, MultipartFile[] files) throws Exception {
        String pathContract = "merchant/" + merchantId + "/contract";
        String localContract = uploadPath + pathContract;
        ArrayList<MerchantImages> filesUpload = new ArrayList<>();
        Path path = Paths.get(localContract).toAbsolutePath().normalize();
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
        } catch (Exception ex) {
            throw new CustomErrorException("Could not create the directory where the uploaded files will be stored.\n" + ex.getMessage());
        }
        if (files != null && files.length > 0) {
            for (MultipartFile file : files) {
                String generalName = "";
                generalName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
                Path filePath = Paths.get(path + "/" + generalName);
                Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
                filesUpload.add(MerchantImages.builder()
                        .merchantId(merchantId)
                        .url(pathContract + "/" + generalName)
                        .type(type)
                        .build());
            }
        }
        return filesUpload;
    }

}
