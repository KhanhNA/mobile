package com.nextsolutions.dcommerce.ui.model.incentive;

import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class IncentiveProgramGroupResponse {
    private Long id;
    private Long incentiveProgramId;
    private Long categoryId;
    private String categoryCode;
    private String categoryName;
    private Long manufacturerId;
    private String manufacturerCode;
    private String manufacturerName;
    private Integer discountType;
    private BigDecimal discountPrice;
    private BigDecimal discountPercent;
    private BigDecimal defaultLimitedQuantity;
    private List<IncentiveProgramDetailInformationResponse> incentiveProgramDetailInformationGroup;
    private List<PackingProductSearchResult> packingProductDetails;
}
