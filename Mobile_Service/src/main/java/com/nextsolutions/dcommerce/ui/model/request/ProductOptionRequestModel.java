package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

import java.util.List;

@Data
public class ProductOptionRequestModel {
    private Long id;
    private String code;
    private List<ProductOptionDescriptionRequestModel> descriptions;
}
