package com.nextsolutions.dcommerce.ui.model.incentive;

import com.nextsolutions.dcommerce.shared.constant.RequestAction;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueIncentiveProgramCodeValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueIncentiveProgramCode {
    String message() default "{com.nextsolutions.validation.constraints.UniqueIncentiveProgramCode.message}";

    RequestAction action() default RequestAction.CREATE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
