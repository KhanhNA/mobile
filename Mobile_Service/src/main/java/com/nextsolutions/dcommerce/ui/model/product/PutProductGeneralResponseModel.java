package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PutProductGeneralResponseModel {
    private Long id;
    private String code;
    private String internationalName;
    private List<PutProductDescriptionResponseModel> descriptions;
}
