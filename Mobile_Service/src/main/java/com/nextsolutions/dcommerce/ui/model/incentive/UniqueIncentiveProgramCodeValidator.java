package com.nextsolutions.dcommerce.ui.model.incentive;

import com.nextsolutions.dcommerce.service.IncentiveProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Map;

public class UniqueIncentiveProgramCodeValidator implements ConstraintValidator<UniqueIncentiveProgramCode, String> {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private IncentiveProgramService incentiveProgramService;

    UniqueIncentiveProgramCode incentiveProgramCode;

    @Override
    public void initialize(UniqueIncentiveProgramCode incentiveProgramCode) {
        this.incentiveProgramCode = incentiveProgramCode;
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) return false;
        switch (incentiveProgramCode.action()) {
            case CREATE:
                return !incentiveProgramService.existsIncentiveProgramCodeOnCreate(value);
            case UPDATE:
                Map<String, String> pathVariable = (Map<String, String>) request
                        .getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
                Long id = Long.parseLong(pathVariable.get("id"));
                return !incentiveProgramService.existsIncentiveProgramCodeOnUpdate(id, value);
            default:
                return false;
        }
    }
}
