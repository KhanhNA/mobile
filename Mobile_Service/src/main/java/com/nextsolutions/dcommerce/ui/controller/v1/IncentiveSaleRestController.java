package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.incentive.IncentiveLevel;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingIncentive;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingSale;
import com.nextsolutions.dcommerce.service.IncentivePackingSaleService;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping(ApiConstant.API_V1 + "/incentive")
public class IncentiveSaleRestController {


    @Autowired
    private IncentivePackingSaleService ictPackingSaleService;


    @GetMapping("/incentive-packing-sale/packing-group-sale")
    public ResponseEntity getPackingGroupSale(@RequestParam("incentiveProgramId") Long ictPgId,
                                              Pageable pageable,
                                              @RequestParam Long status){
        return ResponseEntity.ok(this.ictPackingSaleService.getPackingSale(true, ictPgId, pageable, status));
    }

    @GetMapping("/incentive-packing-sale/packing-product-sale")
    public ResponseEntity getPackingProductSale(@RequestParam("incentiveProgramId") Long ictPgId,
                                              Pageable pageable,
                                              @RequestParam Long status){
        return ResponseEntity.ok(this.ictPackingSaleService.getPackingSale(false, ictPgId, pageable, status));
    }

    @GetMapping("/lvl/forOrderValue")
    public ResponseEntity<?> getLoyaltyLvlForOrderValue(@RequestParam Long incentiveProgramId,
                                                        Pageable pageable,
                                                        @RequestParam String status) throws Exception {
        return new ResponseEntity<>(
                ictPackingSaleService.getIncentiveLvlList(incentiveProgramId, Long.parseLong(status), pageable), HttpStatus.OK);
    }


    @PostMapping("/lvl/{sId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> saveIctLvl(@RequestBody List<IncentiveLevel> dtos,
                                        @PathVariable Long sId) throws Exception {

        ictPackingSaleService.saveIctLvl(dtos, sId);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/lvl/forOrderValue/{loyaltyId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> saveLoyaltyLvlForOrderValue(@RequestBody List<IncentiveLevel> dtos,
                                                         @PathVariable Long loyaltyId) throws Exception {

        ictPackingSaleService.saveIncentiveLvlForOrderValue(dtos, loyaltyId);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @DeleteMapping("/lvl/del/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delIctLvl(@PathVariable Long id) throws Exception {
        ictPackingSaleService.delIctLvl(id);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/pckIct/{lvlId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> saveIctPackingIct(@RequestBody List<IncentivePackingIncentive> dtos,
                                               @PathVariable Long lvlId) throws Exception {

        ictPackingSaleService.saveIctPackingIct(dtos, lvlId);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @DeleteMapping("/pckIct/del/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delIctPackingIct(@PathVariable Long id) throws Exception {

        ictPackingSaleService.delIctPackingIct(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @DeleteMapping("/packingSale/del/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delIctPackingSale(@PathVariable Long id) throws Exception {

        ictPackingSaleService.delIctPackingSale(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


    @PostMapping("/packingSale/{ictPrgId}/{type}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> savePackingSale(@RequestBody List<IncentivePackingSale> ictPackingSales,
                                             @PathVariable Long ictPrgId, @PathVariable Integer type) throws Exception {
        ictPackingSaleService.savePackingSale(ictPackingSales,ictPrgId,type);
        return new ResponseEntity<>(ictPackingSales, HttpStatus.OK);
    }

    @PostMapping("/packingForSale/list")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> list(Pageable pageable, String exceptIds, String text) throws Exception {

        if(Utils.isEmpty(exceptIds)){
            throw new CustomErrorException("exceptIdsIsNull");
        }
        List<Long> list = Arrays.stream(exceptIds.split(",")).map(Long::valueOf).collect(Collectors.toList());

        return ResponseEntity.ok(ictPackingSaleService.list(pageable, list, text));

    }

}
