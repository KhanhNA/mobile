package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CancelOrderRequestModel {
    @NotNull(message = "Logistics code is required field")
    private String logisticsCode;

    @NotBlank(message = "Reason is required field")
    @Size(max = 255, message = "Reason is limited at 255 characters")
    private String reason;
}
