package com.nextsolutions.dcommerce.ui.controller.v2;

import com.nextsolutions.dcommerce.service.ProductCrudService;
import com.nextsolutions.dcommerce.shared.dto.AppUnitDto;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDto;
import com.nextsolutions.dcommerce.shared.dto.form.DistributorForm;
import com.nextsolutions.dcommerce.shared.dto.product.ProductQueryDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductTableDto;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeResource;
import com.nextsolutions.dcommerce.shared.dto.resource.ManufacturerResource;
import com.nextsolutions.dcommerce.ui.model.request.PackingProductRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductBasicInfoRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductImagesRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V2;

@RestController
@RequestMapping(API_V2)
@RequiredArgsConstructor
public class ProductCrudController {

    private final ProductCrudService productCrudService;

    @GetMapping(value = "/products", params = {"categoryId", "packingTypeId"})
    @PreAuthorize("hasAuthority('get/products')")
    public ResponseEntity<Page<ProductPackingResponseModel>> findByCategoryIdAndPackingTypeId(@RequestParam Long categoryId,
                                                                                              @RequestParam Long packingTypeId,
                                                                                              Pageable pageable) {
        return ResponseEntity.ok(productCrudService.findByCategoryIdAndPackingTypeId(categoryId, packingTypeId, pageable));
    }


    @GetMapping(value = "/products/query")
    @PreAuthorize("hasAuthority('get/products/query')")
    public ResponseEntity<Page<ProductTableDto>> findAll(ProductQueryDto query, Pageable pageable) {
        return ResponseEntity.ok(productCrudService.findByQuery(query, pageable));
    }


    @GetMapping("/products/{id}")
    @PreAuthorize("hasAuthority('get/products/{id}')")
    public ResponseEntity<ProductFormResponseModel> getProduct(@PathVariable Long id,
                                                               @RequestParam Long categoryId,
                                                               @RequestParam Long languageId) {
        return ResponseEntity.ok(productCrudService.getProductForm(id, categoryId, languageId));
    }

    @PostMapping("/products/create-basic-info")
    @PreAuthorize("hasAuthority('post/products/create-basic-info')")
    public ResponseEntity<ProductBasicInfoResponseModel> createProductBasicInfo(
            @Valid @RequestBody ProductBasicInfoRequestModel basicInfo) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(productCrudService.saveProductBasicInfo(basicInfo));
    }

    @PatchMapping("/products/update-basic-info")
    @PreAuthorize("hasAuthority('patch/products/update-basic-info')")
    public ResponseEntity<ProductBasicInfoResponseModel> updateProductBasicInfo(
            @Valid @RequestBody ProductBasicInfoRequestModel basicInfo) {
        return ResponseEntity.ok(productCrudService.saveProductBasicInfo(basicInfo));
    }

    @PatchMapping("/products/{id}/product-images")
    @PreAuthorize("hasAuthority('patch/products/{id}/product-images')")
    public ResponseEntity<ProductImagesResponseModel> saveProductImages(
            @PathVariable Long id,
            @RequestPart("productImage") @Nullable MultipartFile[] multipartFiles,
            @Valid @RequestPart("productImages") ProductImagesRequestModel productRequest) throws IOException {
        return ResponseEntity.ok(productCrudService.saveProductImages(id, multipartFiles, productRequest));
    }

    @PutMapping("/products/{id}/packing-products")
    @PreAuthorize("hasAuthority('put/products/{id}/packing-products')")
    public ResponseEntity<?> savePackingProducts(
            @PathVariable("id") Long id,
            @Valid @RequestBody PackingProductRequestModel packingProductRequestModel) {
        return ResponseEntity.ok(productCrudService.savePackingProducts(id, packingProductRequestModel));
    }

    @PutMapping("/products/{id}/distributor")
    @PreAuthorize("hasAuthority('put/products/{id}/distributor')")
    public ResponseEntity<Void> saveDistributor(@PathVariable("id") Long id,
                                                @Valid @RequestBody DistributorForm distributor) {
        productCrudService.updateDistributor(id, distributor);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/products/packing/upload")
    @PreAuthorize("hasAuthority('post/products/packing/upload')")
    public ResponseEntity<Map<String, String>> uploadPackingImage(
            @RequestPart("packingImage") MultipartFile imageFile) throws IOException {
        return ResponseEntity.ok(productCrudService.savePackingImage(imageFile));
    }

    @PostMapping("/products/descriptions/upload")
    @PreAuthorize("hasAuthority('post/products/packing/upload')")
    public ResponseEntity<Map<String, String>> uploadProductDescriptionImage(
            @RequestPart("productDescription") MultipartFile imageFile) throws IOException {
        return ResponseEntity.ok(productCrudService.saveProductDescriptionImage(imageFile));
    }

    @DeleteMapping("/products/{id}")
    @PreAuthorize("hasAuthority('delete/products/{id}')")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        productCrudService.deleteProductById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/products/exists", params = "code")
    @PreAuthorize("hasAuthority('get/products/exists')")
    public ResponseEntity<ValidatorResponseModel> existsCode(@RequestParam String code) {
        if (productCrudService.existsCode(code)) {
            return new ResponseEntity<>(new ValidatorResponseModel("duplicateCode", true), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ValidatorResponseModel("duplicateCode", false), HttpStatus.OK);
        }
    }

    @GetMapping(value = "/products/{id}/exists", params = "code")
    @PreAuthorize("hasAuthority('get/products/{id}/exists')")
    public ResponseEntity<ValidatorResponseModel> existsCode(@PathVariable("id") Long productId, @RequestParam String code) {
        if (productCrudService.isDuplicateCode(productId, code)) {
            return new ResponseEntity<>(new ValidatorResponseModel("duplicateCode", true), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ValidatorResponseModel("duplicateCode", false), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/products/compare-price/{productId}/{salePrice}", method = RequestMethod.HEAD)
    public ResponseEntity<Void> isSalePriceLessThanImportPrice(@PathVariable Long productId, @PathVariable BigDecimal salePrice) throws Exception {
        return this.productCrudService.isSalePriceLessThanImportPrice(productId, salePrice)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @GetMapping(value = "/products/unit")
    public ResponseEntity<List<AppUnitDto>> getUnits() throws Exception {
        return ResponseEntity.ok(this.productCrudService.getUnits());
    }

    @GetMapping(value = "products/manufacturer")
    public ResponseEntity<Page<ManufacturerResource>> getManufacturer(ManufacturerResource resource, Pageable pageable) throws Exception {
        return ResponseEntity.ok(this.productCrudService.getManufacturer(resource, pageable));
    }
}
