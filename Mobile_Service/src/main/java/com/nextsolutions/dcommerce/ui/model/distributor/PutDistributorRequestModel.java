package com.nextsolutions.dcommerce.ui.model.distributor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PutDistributorRequestModel {

    private Long id;

    private String distributorId;

    private String code;

    @NotNull
    @Size(min = 6, max = 45)
    private String name;

    @NotNull
    @Size(min = 6, max = 32)
    private String username;

    private Long walletId;

    @NotNull
    @Size(max = 45)
    private String taxCode;

    @Size(max = 255)
    private String address;

    @NotNull
    private String phoneNumber;

    @NotNull
    @Size(max = 255)
    private String email;

    private Boolean enabled;

    private String aboutUs;
}
