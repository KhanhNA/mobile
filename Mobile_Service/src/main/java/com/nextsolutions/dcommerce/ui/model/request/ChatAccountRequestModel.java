package com.nextsolutions.dcommerce.ui.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChatAccountRequestModel {
    @NotBlank(message = "{com.nextsolutions.validation.constraints.NotBlank.message}")
    @Size(min = 6, max = 32, message = "{com.nextsolutions.validation.constraints.Size.message}")
    private String username;

    @NotBlank(message = "{com.nextsolutions.validation.constraints.NotBlank.message}")
    @Size(min = 6, max = 32, message = "{com.nextsolutions.validation.constraints.Size.message}")
    private String fullName;
}
