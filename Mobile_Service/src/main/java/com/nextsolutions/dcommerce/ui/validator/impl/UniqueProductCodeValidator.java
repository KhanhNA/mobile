package com.nextsolutions.dcommerce.ui.validator.impl;

import com.nextsolutions.dcommerce.repository.ProductRepository;
import com.nextsolutions.dcommerce.ui.validator.UniqueProductCode;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueProductCodeValidator implements ConstraintValidator<UniqueProductCode, String> {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !productRepository.existsByCode(value);
    }
}
