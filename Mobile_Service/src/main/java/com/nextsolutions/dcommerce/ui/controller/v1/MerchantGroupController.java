package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.MerchantGroupCrudService;
import com.nextsolutions.dcommerce.shared.dto.form.MerchantGroupForm;
import com.nextsolutions.dcommerce.ui.model.request.MerchantGroupQueryRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupMerchantResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class MerchantGroupController {

    private final MerchantGroupCrudService merchantGroupCrudService;

    @GetMapping("/merchant-groups")
    public ResponseEntity<Page<MerchantGroupResource>> getMerchantGroups(String keyword,
                                                                         Pageable pageable,
                                                                         @RequestParam("languageId") Integer languageId) {
        return ResponseEntity.ok(merchantGroupCrudService.findAll(keyword, pageable, languageId));
    }

    @GetMapping("/merchant-groups/all")
    public ResponseEntity<List<MerchantGroupResource>> findAll(String keyword, @RequestHeader(name = "Accept-Language") Integer langId, Long merchantId) {
        return ResponseEntity.ok(merchantGroupCrudService.findAll(keyword, langId, merchantId));
    }

    @GetMapping("/merchant-groups/query")
    public ResponseEntity<Page<MerchantGroupResource>> getMerchantGroups(MerchantGroupQueryRequestModel query, Pageable pageable) {
        return ResponseEntity.ok(merchantGroupCrudService.findByQuery(query, pageable));
    }

    @GetMapping("/merchant-groups/{id}")
    public ResponseEntity<MerchantGroupForm> getMerchantGroupForm(@PathVariable Long id) {
        return ResponseEntity.ok(merchantGroupCrudService.getMerchantGroupFormById(id));
    }

    @PostMapping("/merchant-groups")
    public ResponseEntity<MerchantGroupForm> createMerchantGroup(@RequestBody MerchantGroupForm form) {
        return ResponseEntity.status(HttpStatus.CREATED).body(merchantGroupCrudService.createMerchantGroup(form));
    }

    @PutMapping("/merchant-groups/{id}")
    public ResponseEntity<MerchantGroupForm> updateMerchantGroup(@PathVariable Long id, @RequestBody MerchantGroupForm form) {
        return ResponseEntity.ok(merchantGroupCrudService.updateMerchantGroup(id, form));
    }

    @DeleteMapping("/merchant-groups/{id}")
    public ResponseEntity<Void> deleteMerchantGroup(@PathVariable Long id) {
        merchantGroupCrudService.inactiveMerchantGroup(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/merchant-groups/{id}/merchants")
    public ResponseEntity<List<MerchantGroupMerchantResource>> getMerchantGroupMerchants(@PathVariable Long id) {
        return ResponseEntity.ok(this.merchantGroupCrudService.findAllMerchantGroupMerchantById(id));
    }

    @PutMapping("/merchant-groups/{id}/merchants")
    public ResponseEntity<Void> saveMerchantGroupMerchants(@PathVariable Long id,
                                                           @RequestBody Map<Long, MerchantGroupMerchantResource> resources) {
        merchantGroupCrudService.saveMerchantGroupMerchants(id, resources);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
