package com.nextsolutions.dcommerce.ui.model.incentive;

import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class IncentiveProgramDetailInformationResponse {
    private Long id;
    private Long incentiveProgramId;
    private Long packingId;
    private String packingCode;
    private String productName;
    private BigDecimal packingOriginalPrice;
    private BigDecimal packingSalePrice;
    private Integer discountType;
    private BigDecimal discountPrice;
    private BigDecimal discountPercent;
    private BigDecimal limitedQuantity;
    private Boolean isFlashSale;
    private Long bonusPackingId;
    private Integer requiredMinimumPurchaseQuantity;
    private Integer bonusPackingQuantity;
    private Integer limitedPackingQuantity;
    private PackingProductSearchResult packingProduct;
    private PackingProductSearchResult bonusPackingProduct;
}
