package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.incentive.Incentive;
import com.nextsolutions.dcommerce.service.IncentiveService;
import com.nextsolutions.dcommerce.service.impl.FileStorageService;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDto;
import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;


@RestController
@RequestMapping(API_V1 + "/incentive")
public class IncentiveRestController {


    @Autowired
    private IncentiveService ictService;

    @Autowired
    private FileStorageService fileService;

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> insert(@RequestPart("body") IncentiveProgramDto dto, @RequestPart(value = "files", required = false) MultipartFile file) throws Exception {
        validateInputIncentiveProgram(dto);
        String fieldName = "";

        if (file != null) {
            fieldName = fileService.storeCustomFilePath(file);
        }
        dto.setImgUrl(fieldName);
        Incentive entity = ictService.insert(dto);
        return new ResponseEntity<>(entity.getId(), HttpStatus.OK);
    }

    @PostMapping("/descriptions/upload")
    public ResponseEntity<?> uploadDescriptionImage(@RequestPart("description") MultipartFile image)
            throws IOException {
        return ResponseEntity.ok(ictService.uploadFile(image));
    }


    @GetMapping("/from/{ictPackingId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getIctPrgFromPackingId(@PathVariable Long ictPackingId) throws Exception {
        List<Incentive> lstProgrm = ictService.getIctPrgFromPackingId(ictPackingId);
        return new ResponseEntity<>(lstProgrm, HttpStatus.OK);
    }

    @PostMapping("/from-id")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getIncentiveFromId(@RequestBody List<Long> ids, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return new ResponseEntity<>(ictService.getIncentiveFromId(ids, langId), HttpStatus.OK);
    }

    @GetMapping("/getCouponCode")
    public ResponseEntity<?> getCouponIncentiveOfMerchant(@RequestParam Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return new ResponseEntity<>(ictService.getCouponCode(merchantId, langId), HttpStatus.OK);
    }

    @GetMapping("/getList")
    public ResponseEntity<?> getIncentiveProgram(@RequestParam Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return new ResponseEntity<>(ictService.getIncentive(merchantId, langId), HttpStatus.OK);
    }

    @GetMapping("/getBanner")
    public ResponseEntity<?> getBanner(@RequestParam Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return new ResponseEntity<>(ictService.getBannerList(merchantId, langId), HttpStatus.OK);
    }

    @GetMapping("/getFlashSale")
    public ResponseEntity<?> getFlashSaleCurrentTime(Pageable pageable, @RequestParam Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return new ResponseEntity<>(ictService.getPackingFlashSaleCurrentTime(pageable, merchantId, langId), HttpStatus.OK);
    }

    @GetMapping("/getFlashSaleByTime")
    public ResponseEntity<?> getFlashSaleByTime(Pageable pageable, @RequestParam Long merchantId, @RequestParam String milestone, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return Optional.ofNullable(ictService.getFlashSaleByTime(pageable, merchantId, milestone, langId))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/flashSale/currentTime")
    public ResponseEntity<?> getCurrentTime() {
        return new ResponseEntity<>(ictService.flashSaleCurrentTime(), HttpStatus.OK);
    }

    private void validateInputIncentiveProgram(IncentiveProgramDto dto) throws Exception {
        Incentive entity;
        if (dto == null) {
            throw new CustomErrorException("BodyIsNull");
        }
        if (dto.id != null) {
            entity = ictService.find(Incentive.class, dto.id);
            if (entity == null) {
                throw new CustomErrorException("IncentiveProgramNotExist");
            }
        }

        if (dto.getFromDate() == null) {
            throw new CustomErrorException("FromDateIsNull");
        }
        if (dto.getFromDate().toLocalDate().compareTo(LocalDate.now().plusDays(1)) < 0 && dto.getId() == null) {
            throw new CustomErrorException("FromDateMustTomorrow");
        }
    }

}
