package com.nextsolutions.dcommerce.ui.model.response;

import com.nextsolutions.dcommerce.shared.dto.resource.BannerDescriptionResource;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerDetailsResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class BannerFormResponseModel {
    private Long id;
    private String code;
    private List<BannerDescriptionResource> descriptions;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private List<MerchantGroupResource> merchantGroups;
    private List<BannerDetailsResource> details;
}
