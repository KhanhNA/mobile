package com.nextsolutions.dcommerce.ui.controller.v1;


import com.nextsolutions.dcommerce.model.loyalty.LoyaltyMerchantGroup;
import com.nextsolutions.dcommerce.service.LoyaltyMerchantGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/loyalty")
public class LoyaltyMerchantGroupRestController {
    @Autowired
    private LoyaltyMerchantGroupService loyaltyMerchantGroupService;

    @PostMapping("/merchantGroup/{loyaltyProgramId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@RequestBody List<LoyaltyMerchantGroup> loyaltyMerchantGroups, @PathVariable Long loyaltyProgramId) throws Exception {
        loyaltyMerchantGroupService.save(loyaltyMerchantGroups, loyaltyProgramId);
        return ResponseEntity.ok("");

    }
    @DeleteMapping("/merchantGroup/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@PathVariable Long id) throws Exception {
        loyaltyMerchantGroupService.delete(id);
        return ResponseEntity.ok("");

    }

}
