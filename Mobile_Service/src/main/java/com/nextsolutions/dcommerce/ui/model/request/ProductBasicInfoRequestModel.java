package com.nextsolutions.dcommerce.ui.model.request;

import com.nextsolutions.dcommerce.shared.dto.product.ProductAttributeDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductDescriptionDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductBasicInfoRequestModel {

    private Long id;

    @NotNull
    private String brand;

    private String color;

    @NotNull
    private String origin;

    private String notes;

    private boolean returning;

    private BigDecimal importPrice;

    @NotNull
    private Long categoryId;

    @NotNull
    private BigDecimal vat;

    @NotEmpty(message = "error.code.empty")
    @Length(message = "error.code.length")
    private String code;

    @NotEmpty(message = "error.name.empty")
    @Size(min = 1, max = 255, message = "error.name.size")
    private String name;

    @NotNull(message = "error.productTypeId.null")
    private Long productTypeId;

    @NotNull(message = "error.manufacturerId.null")
    private Long manufacturerId;

    private String sku;

    @Min(value = 0, message = "error.width.min")
    @Max(value = 999999, message = "error.width.max")
    private BigDecimal width;

    @Min(value = 0, message = "error.height.min")
    @Max(value = 999999, message = "error.height.max")
    private BigDecimal height;

    @Min(value = 0, message = "error.weight.min")
    @Max(value = 999999, message = "error.weight.max")
    private BigDecimal weight;

    @Min(value = 0, message = "error.length.min")
    @Max(value = 999999, message = "error.length.max")
    private BigDecimal length;

    private String lengthClass;

    private String weightClass;

    private Integer warningThreshold;

    private Integer lifecycle;

    private List<ProductAttributeDto> productAttributes;

    @Valid
    private List<ProductDescriptionDto> productDescriptions;

}
