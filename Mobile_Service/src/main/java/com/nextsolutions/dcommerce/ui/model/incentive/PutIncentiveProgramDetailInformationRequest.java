package com.nextsolutions.dcommerce.ui.model.incentive;

import lombok.Data;

import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.List;

@Data
public class PutIncentiveProgramDetailInformationRequest {
    private Long packingId;
    private Integer discountType;
    private BigDecimal discountPrice;
    private BigDecimal discountPercent;
    private BigDecimal limitedQuantity;
    private Boolean isFlashSale;
    private Long bonusPackingId;
    private Integer requiredMinimumPurchaseQuantity;
    private Integer bonusPackingQuantity;
    private Integer limitedPackingQuantity;
}
