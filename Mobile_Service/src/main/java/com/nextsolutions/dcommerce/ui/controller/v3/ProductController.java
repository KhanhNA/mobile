package com.nextsolutions.dcommerce.ui.controller.v3;

import com.nextsolutions.dcommerce.service.ProductCrudService;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductAttributeResource;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductDataResource;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductGeneralResource;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductPackageResource;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V3;

@RestController
@RequestMapping(API_V3)
@RequiredArgsConstructor
public class ProductController {

    private final ProductCrudService productCrudService;

    @GetMapping("/products/{id}/general")
    public ResponseEntity<ProductGeneralResource> getProductGeneral(@PathVariable Long id) {
        return ResponseEntity.ok(productCrudService.getProductGeneralResource(id));
    }

    @GetMapping("/products/{id}/data")
    public ResponseEntity<ProductDataResource> getProductData(@PathVariable Long id) {
        return ResponseEntity.ok(productCrudService.getProductDataResource(id));
    }

    @GetMapping("/products/{id}/attributes")
    public ResponseEntity<ProductAttributeResource> getProductAttributes(@PathVariable Long id) {
        return ResponseEntity.ok(productCrudService.getProductAttributeResource(id));
    }

    @GetMapping("/products/{id}/packages")
    public ResponseEntity<ProductPackageResource> getProductPackages(@PathVariable Long id) {
        return ResponseEntity.ok(productCrudService.getProductPackageResource(id));
    }
}
