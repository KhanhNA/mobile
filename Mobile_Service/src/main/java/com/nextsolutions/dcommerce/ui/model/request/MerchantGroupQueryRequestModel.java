package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

@Data
public class MerchantGroupQueryRequestModel {
    private String code;
    private String name;
    private Integer languageId;
}
