package com.nextsolutions.dcommerce.ui.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateHunterResponseModel {
    private Long id;
    private String code;
    private String phoneNumber;
    private String username;
    private String fullName;
    private Integer status;
    private String email;
    private LocalDateTime birthDate;
    private Integer gender;
    private String address;
    private String avatar;
    private String identityNumber;
    private String identityImgFront;
    private String identityImgBack;
}
