package com.nextsolutions.dcommerce.ui.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductOptionQueryRequestModel {
    private String code;
    private String name;
}
