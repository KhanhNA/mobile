package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.loyalty.Loyalty;
import com.nextsolutions.dcommerce.service.LoyaltyService;
import com.nextsolutions.dcommerce.shared.dto.LoyaltyDto;
import com.nextsolutions.dcommerce.service.impl.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@RequestMapping("/api/v1/loyalty")
@RestController
public class LoyaltyController {

    @Autowired
    private LoyaltyService loyaltyService;

    @Autowired
    private FileStorageService fileService;

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> insert(@RequestPart("body") LoyaltyDto dto, @RequestPart(value = "files", required = false) MultipartFile file) throws Exception {
        validateInputLoyalty(dto);
        String fieldName = "";
        if (file != null) {
            fieldName = fileService.storeCustomFilePath(file);
        }
        dto.setImgUrl(fieldName);
        Loyalty entity = loyaltyService.insert(dto);
        return new ResponseEntity<>(entity.getId(), HttpStatus.OK);
    }

    @PostMapping("/act/{loyaltyPgId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> activateLoyalty(@PathVariable Long loyaltyPgId) throws Exception {
        Integer changes = loyaltyService.activateProgram(loyaltyPgId);
        return new ResponseEntity<>(changes, HttpStatus.OK);
    }

    @PostMapping("/deact/{loyaltyPgId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> deactivateLoyalty(@PathVariable Long loyaltyPgId) throws Exception {
        Integer changes = loyaltyService.deactivateProgram(loyaltyPgId);
        return new ResponseEntity<>(changes, HttpStatus.OK);
    }

    @GetMapping("/from/{loyaltyPackingId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getLoyaltyPrgFromPackingId(@PathVariable Long loyaltyPackingId) throws Exception {
        List<Loyalty> lst = loyaltyService.getLoyaltyFromPackingId(loyaltyPackingId);
        return new ResponseEntity<>(lst, HttpStatus.OK);
    }

    @PostMapping("/descriptions/upload")
    public ResponseEntity<?> uploadDescriptionImage(@RequestPart("description") MultipartFile image)
            throws IOException {
            return ResponseEntity.ok(loyaltyService.uploadFile(image));
    }


    private void validateInputLoyalty(LoyaltyDto dto) throws Exception {
        Loyalty entity;
        if (dto == null) {
            throw new CustomErrorException("BodyIsNull");
        }
        if (dto.id != null) {
            entity = loyaltyService.find(Loyalty.class, dto.id);
            if (entity == null) {
                throw new CustomErrorException("LoyaltyNotExist");
            }
        }

        if (dto.getFromDate() == null) {
            throw new CustomErrorException("FromDateIsNull");
        }
        if (dto.getFromDate().toLocalDate().compareTo(LocalDate.now().plusDays(1)) < 0 && dto.getId() == null) {
            throw new CustomErrorException("FromDateMustTomorrow");
        }
    }

}
