package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PutProductGeneralRequestModel {
    @Pattern(regexp = "^[a-zA-Z0-9]{3,10}$", message = "code must be formatted like ^[a-zA-Z0-9]{3,10}$")
    private String code;

    @NotBlank(message = "internationalName is required")
    private String internationalName;

    @Valid
    @NotEmpty(message = "descriptions is required")
    private List<PutProductDescriptionRequestModel> descriptions;
}
