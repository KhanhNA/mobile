package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.Category;
import com.nextsolutions.dcommerce.model.RecommendSearch;
import com.nextsolutions.dcommerce.service.CategoryService;
import com.nextsolutions.dcommerce.service.RecommendSearchService;
import com.nextsolutions.dcommerce.shared.dto.CategoryDto;
import com.nextsolutions.dcommerce.shared.dto.resource.RecommendSearchResource;
import com.nextsolutions.dcommerce.ui.model.request.RecommendSearchRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.RecommendSearchResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class RecommendSearchController {
    private final RecommendSearchService recommendSearchService;
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/recommend-search")
    public ResponseEntity<Page<RecommendSearchResource>> findByQuery(RecommendSearchResource recommendSearchResource, Pageable pageable) throws Exception {
        return ResponseEntity.ok(this.recommendSearchService.findByQuery(recommendSearchResource,pageable));
    }

    @PostMapping("/recommend-search")
    public ResponseEntity<Void> createRecommendSearch(@RequestBody RecommendSearchRequestModel request) {
        this.recommendSearchService.createRecommendSearch(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @PostMapping("/recommend-search/listCategory")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Page<CategoryDto>> getListCategoryByLevel(Pageable pageable, Long level, Long code) throws Exception {
        return ResponseEntity.ok(categoryService.getListCategoryByLevel(pageable, level, code));
    }

    @DeleteMapping("/recommend-search/{id}")
    public ResponseEntity<Void> deleteRecommendSearch(@PathVariable Long id) {
        this.recommendSearchService.inactive(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/recommend-search/{id}")
    public ResponseEntity<RecommendSearchResource> getRecommendSearch(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(this.recommendSearchService.getRecommendSearch(id));
    }

    @GetMapping("/recommend-search/merchant")
    public ResponseEntity<List<RecommendSearchResource>> getRecommendSearch(@RequestParam("merchantId") Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return ResponseEntity.ok(this.recommendSearchService.getRecommendSearch(merchantId, langId));
    }

    @PutMapping("/recommend-search/update/{id}")
    public ResponseEntity<RecommendSearch> updateRecommendSearch(@PathVariable Long id, @RequestBody RecommendSearchRequestModel request) {
        return ResponseEntity.ok(this.recommendSearchService.updateRecommendSearch(id, request));
    }
}
