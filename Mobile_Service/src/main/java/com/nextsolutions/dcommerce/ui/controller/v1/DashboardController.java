package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.service.DashboardService;
import com.nextsolutions.dcommerce.shared.dto.DashboardHunterDTO;
import com.nextsolutions.dcommerce.shared.dto.OverviewStatistics;
import com.nextsolutions.dcommerce.shared.dto.RevenueDTO;
import com.nextsolutions.dcommerce.utils.DateUtils;
import com.nextsolutions.dcommerce.utils.DateValidatorUsingDateFormat;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

@RestController("dashboardControllers")
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class DashboardController {

    private final DashboardService dashboardService;
    private final CommonService commonService;

    @GetMapping("/overview-statistics")
    public ResponseEntity<OverviewStatistics> getOverviewStatistics() {
        return ResponseEntity.ok(dashboardService.getOverviewStatistics());
    }


    @GetMapping("/hunter-dashboard")
    @Transactional
    public ResponseEntity<?> getDashboard(@RequestParam("parentMerchantId") Long parentMerchantId, @RequestParam String fromDate, @RequestParam String toDate) throws Exception {
        if (commonService.find(Merchant.class, parentMerchantId) == null) {
            throw new CustomErrorException("Merchant not exist");
        }

        DashboardHunterDTO dashboardHunterDTO = new DashboardHunterDTO();
        HashMap<String, Object> params = new HashMap<>();
        params.put("parentMerchantId", parentMerchantId);

        // Tong so dai ly cho duyet
        dashboardHunterDTO.setTotalPending(commonService.getRowCountReturnLong(dashboardService.getSqlTotalPending(fromDate, toDate), params));
        // Tong so dai ly da kich hoat
        dashboardHunterDTO.setTotalActive(commonService.getRowCountReturnLong(dashboardService.getSqlTotalActive(fromDate, toDate), params));
        // Tong so dai ly ngung hoat dong
        dashboardHunterDTO.setTotalInactive(commonService.getRowCountReturnLong(dashboardService.getSqlTotalInactive(fromDate, toDate), params));
        // Khong phat sinh doanh so
        dashboardHunterDTO.setTotalMerchantInactiveRevenue(commonService.getRowCountReturnLong(dashboardService.getSqlInactiveRevenue(fromDate, toDate), params));
        // Tong don hang
        dashboardHunterDTO.setTotalOrder(commonService.getRowCountReturnLong(dashboardService.getSqlTotalOrder(fromDate, toDate), params));
        // Tong doanh so
        dashboardHunterDTO.setTotalRevenue((BigDecimal) commonService.executeSqlForResults(dashboardService.getSqlTotalRevenue(fromDate, toDate), params));

        dashboardHunterDTO.setUpdateAt(SpringUtils.getCurrentLocalDateTime());

        return new ResponseEntity<>(dashboardHunterDTO, HttpStatus.OK);
    }

    @GetMapping("/merchant-revenue")
    public ResponseEntity<?> getDashboard(@RequestParam Long merchantId, @RequestParam Integer year, @RequestParam int type) throws Exception {
        if (commonService.find(Merchant.class, merchantId) == null) {
            throw new CustomErrorException("Merchant not exist");
        }
        HashMap<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT MONTH(o.order_date) month, year(o.order_date) year,SUM(o.amount) amount ");
        params.put("year", year);
        if (type == 0) {
            // Hunter
            sql.append(" FROM orders o where year(o.order_date) = :year and o.MERCHANT_ID in (select merchant_id from merchant where PARENT_MARCHANT_ID = :parentMerchantId ) ");
            params.put("parentMerchantId", merchantId);
        } else if (type == 1) {
            // Merchant
            if (commonService.find(Merchant.class, merchantId) == null) {
                throw new CustomErrorException("Merchant not exist");
            }
            sql.append(" FROM orders o where year(o.order_date) = :year and o.MERCHANT_ID = :merchantId ");
            params.put("merchantId", merchantId);
        }
        sql.append(" GROUP BY MONTH(o.order_date),year(o.order_date) ");
        sql.append(" order by MONTH(o.order_date) asc,year(o.order_date) asc ");

        return new ResponseEntity<>(commonService.findAll(null, RevenueDTO.class, sql.toString(), params), HttpStatus.OK);
    }

    @GetMapping("/merchant-dashboard")
    public ResponseEntity<?> merchantDashboard(@RequestParam("merchantId") Long merchantId, @RequestParam String fromDate, @RequestParam String toDate) throws Exception {
        if (getMerchant(merchantId) == null) {
            throw new CustomErrorException("Merchant not exist");
        }
        DateValidatorUsingDateFormat.validDate(fromDate, toDate);
        HashMap<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);

        DashboardHunterDTO hunterDTO = new DashboardHunterDTO();
        hunterDTO.setTotalOrder(commonService.getRowCountReturnLong(dashboardService.getSqlTotalOrderOfMerchant(fromDate, toDate), params));
        hunterDTO.setTotalRevenue((BigDecimal) commonService.executeSqlForResults(dashboardService.getSqlTotalRevenueOfMerchant(fromDate, toDate), params));
        return new ResponseEntity<>(hunterDTO, HttpStatus.OK);
    }


    private Merchant getMerchant(Long merchantId) {
        return commonService.find(Merchant.class, merchantId);
    }
}
