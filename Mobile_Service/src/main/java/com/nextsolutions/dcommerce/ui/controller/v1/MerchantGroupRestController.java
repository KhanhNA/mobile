package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.AppParam;
import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.service.MerchantGroupService;
import com.nextsolutions.dcommerce.shared.dto.MerchantGroupWithAppParamDto;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/merchantGroup")
public class MerchantGroupRestController {

    @Autowired
    private MerchantGroupService merchantGroupService;

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@RequestBody List<MerchantGroup> merchantGroups) throws Exception {
        merchantGroupService.save(merchantGroups);
        return ResponseEntity.ok("");

    }

    @PostMapping("/addMerchant/{merchantGroupId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> addMerchant(@RequestBody List<Long> merchantIds, @PathVariable Long merchantGroupId) throws Exception {
        merchantGroupService.addMerchant(merchantIds, merchantGroupId);
        return ResponseEntity.ok("");


    }

    @PostMapping("/list")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> list(Pageable pageable, Long merchantGroupId) throws Exception {
        ;
        return ResponseEntity.ok(merchantGroupService.list(pageable, merchantGroupId));

    }


    @PostMapping("/listGroup")
    public ResponseEntity<?> listGroup(Pageable pageable, String exceptIds, String text) throws Exception {

        if(Utils.isEmpty(exceptIds)){
            throw new CustomErrorException("exceptIdsIsNull");
        }
        List<Long> list = Arrays.stream(exceptIds.split(",")).map(Long::valueOf).collect(Collectors.toList());

        return ResponseEntity.ok(merchantGroupService.listGroupDTO(pageable, list, text));

    }


    @PostMapping("/listGroupAndAppParam")
    public ResponseEntity<?> listGroupAndAppParam(Pageable pageable, String exceptIds, String text) throws Exception {

        if(Utils.isEmpty(exceptIds)){
            throw new CustomErrorException("exceptIdsIsNull");
        }
        List<Long> list = Arrays.stream(exceptIds.split(",")).map(Long::valueOf).collect(Collectors.toList());

        List<AppParam> lst = merchantGroupService.listGroupParam();
        MerchantGroupWithAppParamDto dto = new MerchantGroupWithAppParamDto();
        HashMap<String, String> tmp;
        for(AppParam appParam: lst){
            tmp = null;
            switch (appParam.getType()) {
                case "LOC_BUS":
                    tmp = dto.getLocationBusinesses();
                    break;
                case "AREA":
                    tmp = dto.getAreas();
                    break;
                case "INFAS":
                    tmp = dto.getElectricalInfrastructures();
                    break;
                case "DISPLAY":
                    tmp = dto.getDisplayDevices();
                    break;
                case "REFREG":
                    tmp = dto.getRefrigerationEquipments();
                    break;
            }
            if(tmp != null) {
                tmp.put(appParam.getCode(), appParam.getName());
            }
        }
        dto.setMerchantGroups(merchantGroupService.listGroup(pageable, list, text));
        return ResponseEntity.ok(dto);

    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@PathVariable Long id) throws Exception {
        merchantGroupService.delete(id);
        return ResponseEntity.ok("");

    }
    @DeleteMapping("/merchant/{merchantGroupMerchantid}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> deleteMerchantGroupMerchant(@PathVariable Long merchantGroupMerchantid) throws Exception {
        merchantGroupService.deleteMerchantGroupMerchant(merchantGroupMerchantid);
        return ResponseEntity.ok("");

    }
}
