package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Kpi;
import com.nextsolutions.dcommerce.utils.Utils;
import com.nextsolutions.dcommerce.model.KpiDescription;
import com.nextsolutions.dcommerce.service.KpiService;
import com.nextsolutions.dcommerce.shared.dto.KpiDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/v1/kpi")
public class KpiController {

    @Autowired
    private KpiService kpiService;

    @GetMapping("/getAll")
    public ResponseEntity<?> getAll() {
        return kpiService.getAll();
    }

//    @GetMapping("/find")
//    public ResponseEntity<?> find(
//            @RequestParam("text") String searchKey,
//            @RequestParam("pageNumb") Integer pageNumb,
//            @RequestParam("pageSize") Integer pageSize) {
//        return kpiService.find(text, new PageRequest(pageNumb,pageSize));
//    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") Long id) {
        return kpiService.getOne(id);
    }

    @GetMapping("/getToDate")
    public ResponseEntity<?> getListToDate(@RequestParam("type") String type) {
        return kpiService.getListToDate(type);
    }



    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> create(@RequestPart("body") KpiDto kpiDto) throws Exception {
        Kpi entity = null;

        boolean isEdit = kpiDto.getId() != null ? true : false;
        List<KpiDescription> descriptions = null;
        if (isEdit) {
            entity = kpiService.find(Kpi.class, kpiDto.getId());
            descriptions = entity.getDescriptionsList();
        }
        if (kpiDto == null) {
            throw new CustomErrorException("KpiNotExist");
        }
        entity = Utils.getData(kpiDto, Kpi.class);
        Iterator iterator = Utils.safe(kpiDto.getDescriptions()).entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            if (isEdit && descriptions != null && descriptions.size() > 0) {
                for (KpiDescription old_description : descriptions) {
                    if (entry.getKey().equals(old_description.getLangCode())) {
                        old_description.setName(((KpiDescription) entry.getValue()).getName());
//                        kpiService.save(old_description);
                        break;
                    }
                }
                entity.setDescriptionsList(descriptions);
                entity = kpiService.save(entity);
            } else {
                entity = kpiService.save(entity);
                KpiDescription description = null;
                description = (KpiDescription) entry.getValue();

                description.setKpiId(entity.getId());
                description.setLangCode((String) entry.getKey());

                kpiService.save(description);
            }
        }
        return new ResponseEntity<>(entity.getId(), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id, @RequestBody KpiDto kpiDto) {
        return kpiService.update(id, kpiDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        return kpiService.delete(id);
    }

}
