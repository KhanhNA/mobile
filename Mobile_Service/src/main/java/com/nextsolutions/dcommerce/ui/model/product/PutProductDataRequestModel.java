package com.nextsolutions.dcommerce.ui.model.product;

import com.nextsolutions.dcommerce.ui.validator.UniqueProductCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PutProductDataRequestModel {

    @NotNull
    @Size(min = 1, max = 10)
    @UniqueProductCode
    private String code;

    @NotNull
    @Size(min = 1, max = 255)
    private String brand;

    @NotNull
    @Size(min = 1, max = 255)
    private String origin;

    @NotNull
    @Size(min = 1, max = 255)
    @Pattern(regexp = "^[a-zA-Z0-9-]+$")
    private String sku;

    @NotNull
    @Size(min = 1, max = 255)
    @Pattern(regexp = "^[a-zA-Z0-9-]+$")
    private String barcode;

    private String manufacturerPartNumber;

    private String isbn;

    @NotNull
    @Range(min = 1, max = Integer.MAX_VALUE)
    private BigDecimal length;

    @NotNull
    @Range(min = 1, max = Integer.MAX_VALUE)
    private BigDecimal width;

    @NotNull
    @Range(min = 1, max = Integer.MAX_VALUE)
    private BigDecimal height;

    @NotNull
    private String lengthClass;

    @NotNull
    @Range(min = 1, max = Integer.MAX_VALUE)
    private BigDecimal weight;

    @NotNull
    private String weightClass;

    @NotNull
    private Integer lifecycle;

    private Integer status;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateAvailable;
}
