package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.AndroidCrashLog;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.shared.dto.AndroidCrashLogDto;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@Transactional
@RequiredArgsConstructor
public class CrashRestController {

    private final CommonService commonService;

    @PostMapping("/crash")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> register(@RequestBody AndroidCrashLogDto dto) throws Exception {
        AndroidCrashLog log = Utils.getData(dto, AndroidCrashLog.class);
        commonService.save(log);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/sendError")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> sendError(@RequestBody AndroidCrashLogDto dto) throws Exception {
        AndroidCrashLog log = Utils.getData(dto, AndroidCrashLog.class);
        commonService.save(log);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

}
