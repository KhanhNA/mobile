package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HunterRequestModel extends MerchantRequestModel {
    private Integer status;
}
