package com.nextsolutions.dcommerce.ui.validator;


import com.nextsolutions.dcommerce.ui.validator.impl.UniquePhoneNumberValidator;
import com.nextsolutions.dcommerce.ui.validator.impl.UniqueProductCodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueProductCodeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueProductCode {
    String message() default "{com.nextsolutions.validation.constraints.UniqueProductCode.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
