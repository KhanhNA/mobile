package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.configuration.properties.ChatConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Count;
import com.nextsolutions.dcommerce.model.NotificationMerchant;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.service.chat.ChatService;
import com.nextsolutions.dcommerce.service.impl.HunterServiceImpl;
import com.nextsolutions.dcommerce.shared.dto.NotificationMerchantDTO;
import com.nextsolutions.dcommerce.shared.dto.chat.ChatLoginResponseDataDto;
import com.nextsolutions.dcommerce.shared.dto.chat.DiscussionMessageDTO;
import com.nextsolutions.dcommerce.shared.dto.chat.SendMessageResponseDTO;
import com.nextsolutions.dcommerce.utils.StringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/notification")
@Transactional
@RequiredArgsConstructor
public class NotificationRestController {

    private final CommonService commonService;
    private final ChatService chatService;
    private final RestTemplate restTemplate;
    private final ChatConfiguration chatConfiguration;
    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);

    @Transactional(rollbackFor = Exception.class)
    @GetMapping("/getList")
    public Page<NotificationMerchantDTO> findAll(Pageable pageable, @RequestParam("merchantId") Long merchantId, @RequestParam(value = "typeId", required = false) List<Integer> typeId) throws Exception {
        StringBuilder sqlRun = new StringBuilder();
        HashMap<String, Object> params = new HashMap<>();
        sqlRun.append("select * from notification_merchant where merchant_receiver_id = :merchantId");
        params.put("merchantId", merchantId);
        if (typeId != null) {
            sqlRun.append(" and type_id in (")
                    .append(Utils.join(",", typeId))
                    .append(") ");
        }
        sqlRun.append(" order by create_date desc");
        //
        int totalCount = commonService.getRowCountV2(String.format("select count(*) from (%s) x", sqlRun.toString()), params);
        List<NotificationMerchantDTO> commonData = commonService.findAll(pageable, NotificationMerchantDTO.class, sqlRun.toString(), params);
        return new PageImpl<>(commonData, pageable, totalCount);
    }

    @Transactional(rollbackFor = Exception.class)
    @GetMapping("/update")
    public ResponseEntity<?> update(@RequestParam("notificationId") Long notificationId) throws Exception {
        if (notificationId == null) {
            throw new CustomErrorException("Id is not null");
        }
        NotificationMerchant dto = commonService.find(NotificationMerchant.class, notificationId);
        if (dto == null) {
            throw new CustomErrorException("Notification not exist");
        }
        dto.setIsSeen(1);
        NotificationMerchant commonData = commonService.save(dto);
        return Optional.ofNullable(commonData)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @Transactional(rollbackFor = Exception.class)
    @GetMapping("/count")
    public ResponseEntity<?> getCount(@RequestParam("merchantId") Long merchantId) throws Exception {
        HashMap<String, Object> param = new HashMap<>();
        param.put("merchantId", merchantId);
        int countRow = commonService.getRowCountV2("select count(*) from notification_merchant where merchant_receiver_id = :merchantId and is_seen = 0 ", param);
        Count count = new Count();
        count.setCountNumber(countRow);
        return Optional.of(count)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }

    @GetMapping("/sent-message")
    public ResponseEntity<?> sendMessage(@RequestParam("userName") String userName, @RequestParam("roomId") String roomId, @RequestParam("message") String message) throws Exception {
        String rId = chatService.getRoomID(userName, roomId);
        SendMessageResponseDTO sendMessageResponseDTO = new SendMessageResponseDTO();
        if (StringUtils.notEmpty(rId)) {
            chatService.sendMessageToRoomID(rId, message);
            sendMessageResponseDTO.setRID(rId);
            sendMessageResponseDTO.setStatus("Success");
        } else {
            sendMessageResponseDTO.setStatus("Error");
        }
        return Optional.of(sendMessageResponseDTO)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }

    @GetMapping("/getDiscussion")
    public ResponseEntity<?> getAllMessage(String roomId, int page) throws Exception {
        DiscussionMessageDTO discussionMessageDTO = DiscussionMessageDTO.builder().build();
        String groupMessage = chatConfiguration.getBaseUrl() + "/v1/groups.messages";
        ChatLoginResponseDataDto loginInfo = chatService.getUserInfo();

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(groupMessage)
                .queryParam("offset", 20 * page)
                .queryParam("count", 20)
                .queryParam("roomId", roomId);

        HttpEntity<?> entity = new HttpEntity<>(chatService.getHeaderLogin(loginInfo));
        try {
            HttpEntity<DiscussionMessageDTO> userInfo = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    entity,
                    DiscussionMessageDTO.class);

            if (userInfo.getBody() != null) {
                discussionMessageDTO = userInfo.getBody();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Optional.of(discussionMessageDTO)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
}
