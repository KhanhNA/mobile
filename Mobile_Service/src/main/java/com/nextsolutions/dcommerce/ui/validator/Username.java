package com.nextsolutions.dcommerce.ui.validator;

import com.nextsolutions.dcommerce.ui.validator.impl.UsernameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UsernameValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Username {
    String message() default "{com.nextsolutions.validation.constraints.Username.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
