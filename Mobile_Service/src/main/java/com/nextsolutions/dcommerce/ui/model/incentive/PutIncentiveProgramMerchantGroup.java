package com.nextsolutions.dcommerce.ui.model.incentive;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class PutIncentiveProgramMerchantGroup {
    private List<String> warehouseCodes;

    private List<Long> merchantGroupIds;

    private Boolean applyL2;
}
