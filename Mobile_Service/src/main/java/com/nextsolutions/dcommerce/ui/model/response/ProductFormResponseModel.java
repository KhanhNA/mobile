package com.nextsolutions.dcommerce.ui.model.response;

import com.nextsolutions.dcommerce.shared.dto.product.PackingProductDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductAttributeDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductImageDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductFormResponseModel {
    private Long id;
    private String code;
    private String brand;
    private String color;
    private String origin;
    private String notes;
    private boolean returning;
    private String name;
    private Long productTypeId;
    private BigDecimal importPrice;
    private BigDecimal vat;
    private Long manufacturerId;
    private String sku;
    private BigDecimal width;
    private BigDecimal height;
    private BigDecimal weight;
    private BigDecimal length;
    private String lengthClass;
    private String weightClass;
    private Integer warningThreshold;
    private Integer lifecycle;
    private Long categoryId;
    private Long distributorId;
    private List<ProductImageDto> productImages;
    private List<ProductDescriptionDto> productDescriptions;
    private List<PackingProductDto> packingProducts;
    private List<ProductAttributeDto> productAttributes;
}
