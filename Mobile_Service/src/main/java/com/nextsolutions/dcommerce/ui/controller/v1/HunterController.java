package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.service.HunterService;
import com.nextsolutions.dcommerce.shared.dto.HunterDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.dto.resource.HunterResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.shared.mapper.HunterMapper;
import com.nextsolutions.dcommerce.ui.model.request.CreateHunterRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.HunterRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.MerchantQueryRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.UpdateHunterRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.CreateHunterResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.HunterResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class HunterController {

    private final HunterService hunterService;
    private final HunterMapper hunterMapper;

    @GetMapping("/hunters")
    @PreAuthorize("hasAuthority('get/hunters')")
    public ResponseEntity<Page<HunterResource>> getHunters(HunterRequestModel query, Pageable pageable) {
        return ResponseEntity.ok(hunterService.findByQuery(query, pageable));
    }

    @PostMapping("/hunters")
    @PreAuthorize("hasAuthority('post/hunters')")
    public ResponseEntity<CreateHunterResponseModel> requestCreateHunter(@Valid @RequestBody CreateHunterRequestModel hunterRequestModel) {
        HunterDto hunterDto = hunterMapper.toHunterDto(hunterRequestModel);
        HunterDto hunterDtoValue = hunterService.requestCreateHunter(hunterDto);
        CreateHunterResponseModel hunterResponseModel = hunterMapper.toCreateHunterResponseModel(hunterDtoValue);
        return ResponseEntity.status(HttpStatus.CREATED).body(hunterResponseModel);
    }

    @GetMapping("/hunters/{id}")
    @PreAuthorize("hasAuthority('get/hunters/{id}')")
    public ResponseEntity<HunterDto> getHunter(@PathVariable Long id) {
        HunterDto hunter = hunterService.getHunterById(id);
        return ResponseEntity.ok(hunter);
    }

    @PutMapping("/hunters/{id}")
    @PreAuthorize("hasAuthority('put/hunters/{id}')")
    public ResponseEntity<HunterResponseModel> requestUpdate(
            @PathVariable Long id,
            @Valid @RequestBody CreateHunterRequestModel requestModel
    ) {
        HunterDto hunterDto = hunterMapper.toHunterDto(requestModel);
        HunterDto updatedHunter = hunterService.requestUpdate(id, hunterDto);
        HunterResponseModel responseModel = hunterMapper.toHunterResponseModel(updatedHunter);
        return ResponseEntity.ok(responseModel);
    }

    @GetMapping("/hunters/{hunterId}/merchants")
    @PreAuthorize("hasAuthority('get/hunters/{hunterId}/merchants')")
    public ResponseEntity<Page<MerchantCrudResource>> getMerchantsByHunterId(@PathVariable("hunterId") Long hunterId,
                                                                             MerchantQueryRequestModel merchantQueryRequestModel,
                                                                             Pageable pageable) {
        MerchantDto merchantDto = hunterMapper.toHunterDto(merchantQueryRequestModel);
        return ResponseEntity.ok(hunterService.findMerchants(hunterId, merchantDto, pageable));
    }

    @GetMapping("/hunters/view/{id}")
    @PreAuthorize("hasAuthority('get/hunters/view/{id}')")
    public ResponseEntity<String> getMerchantToViewChange(@PathVariable long id) {
        return ResponseEntity.ok(hunterService.getMerchantToViewChange(id));
    }

    @PutMapping("/hunters/updateByManager/{id}")
    @PreAuthorize("hasAuthority('put/hunters/updateByManager/{id}')")
    public ResponseEntity<HunterResponseModel> updateByManager(@PathVariable Long id, @Valid @RequestBody CreateHunterRequestModel requestModel) {
        HunterDto hunterDto = hunterMapper.toHunterDto(requestModel);
        HunterDto updatedHunter = hunterService.updateByManager(id, hunterDto);
        HunterResponseModel responseModel = hunterMapper.toHunterResponseModel(updatedHunter);
        return ResponseEntity.ok(responseModel);
    }

    @PutMapping("/hunters/createByManager/{id}")
    @PreAuthorize("hasAuthority('put/hunters/createByManager/{id}')")
    public ResponseEntity<Merchant> createByManager(@PathVariable Long id) {
        return ResponseEntity.ok(this.hunterService.createByManager(id));
    }

    @PutMapping("/hunters/managerReject/{id}")
    @PreAuthorize("hasAuthority('put/hunters/managerReject/{id}')")
    public ResponseEntity<Merchant> managerReject(@PathVariable Long id) {
        return ResponseEntity.ok(this.hunterService.managerReject(id));
    }

    @DeleteMapping("/hunters/{id}/{userName}")
    @PreAuthorize("hasAuthority('delete/hunters/{id}/{userName}')")
    public ResponseEntity<Void> deleteHunter(@PathVariable("id") Long id, @PathVariable("userName") String userName, @RequestHeader("Authorization") String token) {
        hunterService.deleteHunter(id, userName, token);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/hunters/requestDelete/{id}")
    @PreAuthorize("hasAuthority('put/hunters/requestDelete/{id}')")
    public ResponseEntity<Merchant> requestDelete(@PathVariable Long id) {
        return ResponseEntity.ok(this.hunterService.requestDelete(id));
    }

    @GetMapping(value = "/hunters/username")
    @PreAuthorize("hasAuthority('get/hunters/username')")
    public ResponseEntity<Void> existsUsername(@RequestParam String username) {
        return this.hunterService.existsByUsername(username)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/hunters/phone/{phone}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/hunters/phone/{phone}')")
    public ResponseEntity<Void> existsPhoneNumber(@PathVariable String phone) {
        return this.hunterService.existsPhoneNumber(phone)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @GetMapping("/hunters/export")
//    @PreAuthorize("hasAuthority('get/hunters/export')")
    public ResponseEntity<Object> exportHunters(HunterRequestModel query, Pageable pageable) {
        return this.hunterService.exportHunter(query, pageable);
    }


    @RequestMapping(value = "/hunters/bankAccountNo/{bankAccountNo}", method = RequestMethod.HEAD)
    public ResponseEntity<Void> existsBankNo(@PathVariable String bankAccountNo) {
        return this.hunterService.existsBankAccountNo(bankAccountNo)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }
    @RequestMapping(value = "/hunters/{id}/bankAccountNo/{bankAccountNo}", method = RequestMethod.HEAD)
    //@PreAuthorize("hasAuthority('head/hunters/{id}/bankAccountNo/{code}')")
    public ResponseEntity<Void> existsUpdateBankNo(@PathVariable Long id, @PathVariable String bankAccountNo) {
        return hunterService.existsUpdateBankNo(id, bankAccountNo)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/hunters/walletClientId/{walletClientId}", method = RequestMethod.HEAD)
    public ResponseEntity<Void> existsWalletId(@PathVariable Long walletClientId) {
        return this.hunterService.existsWalletId(walletClientId)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }
    @RequestMapping(value = "/hunters/{id}/walletClientId/{code}", method = RequestMethod.HEAD)
    public ResponseEntity<Void> existsUpdateWalletId(@PathVariable Long id, @PathVariable Long code)  {
        return this.hunterService.existsUpdateWalletId(id,code)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/hunters/contractNumber/{contractNumber}", method = RequestMethod.HEAD)
    public ResponseEntity<Void> existsContractNumber(@PathVariable String contractNumber) {
        return this.hunterService.existsContractNumber(contractNumber)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/hunters/{id}/contractNumber/{contractNumber}", method = RequestMethod.HEAD)
    public ResponseEntity<Void> existsContractNumber(@PathVariable String contractNumber, @PathVariable Long id) {
        return this.hunterService.existsContractNumber(contractNumber, id)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/hunters/identityNumber/{identityNumber}", method = RequestMethod.HEAD)
    public ResponseEntity<Void> existsIdentityNumber(@PathVariable String identityNumber) {
        return this.hunterService.existsIdentityNumber(identityNumber)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/hunters/{id}/identityNumber/{identityNumber}", method = RequestMethod.HEAD)
    public ResponseEntity<Void> existsIdentityNumber(@PathVariable String identityNumber, @PathVariable Long id) {
        return this.hunterService.existsIdentityNumber(identityNumber, id)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @GetMapping("/hunters/parent-merchant")
    public ResponseEntity<List<HunterDto>> getParentMerchant() throws Exception {
        return ResponseEntity.ok(this.hunterService.getParentMerchant());
    }

}
