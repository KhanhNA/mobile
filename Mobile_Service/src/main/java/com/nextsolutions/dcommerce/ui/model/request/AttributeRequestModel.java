package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

@Data
public class AttributeRequestModel {
    private String name;
    private String code;
}
