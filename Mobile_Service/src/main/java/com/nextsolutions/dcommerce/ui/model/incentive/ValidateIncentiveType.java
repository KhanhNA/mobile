package com.nextsolutions.dcommerce.ui.model.incentive;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = IncentiveTypeValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateIncentiveType {
    String message() default "{com.nextsolutions.validation.constraints.IncentiveType.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
