package com.nextsolutions.dcommerce.ui.controller.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import com.nextsolutions.dcommerce.model.CommonOptionDescription;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.service.impl.FileStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Collectors;


@RestController
@RequestMapping(ApiConstant.API_V1)
@RequiredArgsConstructor
public class CommonRestController {

    private static final String PKG_DTO = "com.nextsolutions.dcommerce.model.";

    private final CommonService commonService;

    private final ObjectMapper mapper;

    @PostMapping("/common/insert")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> insert(@RequestBody Object entityName, String page) throws Exception {
        Class clazz = Class.forName(PKG_DTO + page);
        //resolve spring generic problem
        Object req = mapper.convertValue(entityName, clazz);
        commonService.save(req);
        return new ResponseEntity<>(req, HttpStatus.OK);
    }

    @PostMapping("/common/list")
    @Transactional(rollbackFor = Exception.class)
    public Page<?> list(@RequestBody(required = false) Object request, @RequestParam String entityName, Pageable pageable, Integer maxResult) throws Exception {

        Class clazz = Class.forName(PKG_DTO + entityName);
        //resolve spring generic problem
        Object req = null;
        if (request != null) {
            req = mapper.convertValue(request, clazz);
        } else {
            req = clazz.newInstance();
        }
//        commonService.save(req, clazz, CommonService.Action.  INSERT);

        return commonService.findAll(req, pageable, maxResult);
    }

    @PostMapping("/common/listAll")
    @Transactional(rollbackFor = Exception.class)
    public Page<?> list(@RequestBody(required = false) Object request, @RequestParam String entityName) throws Exception {

        Class clazz = Class.forName(PKG_DTO + entityName);
        //resolve spring generic problem
        Object req = null;
        if (request != null) {
            req = mapper.convertValue(request, clazz);
        } else {
            req = clazz.newInstance();
        }
//        commonService.save(req, clazz, CommonService.Action.  INSERT);

        return commonService.findAll(req, Pageable.unpaged(), Integer.MAX_VALUE);
    }

    @PostMapping("/loadConfig")
    @Transactional(rollbackFor = Exception.class)
    public HashMap<String, Object> loadConfig() throws Exception {
        HashMap<String, Object> config = new HashMap<>();
        config.put("clientId", "Dcommerce");
        config.put("clientSecret", "A31b4c24l3kj35d4AKJQ");
        config.put("accessTokenUri", "14.162.183.148:9999");
        config.put("userAuthorizationUri", "14.162.183.148:9999");
        return config;
    }

    @Autowired
    private FileStorageService fileService;

    @PostMapping(value = "/api/files")
    @ResponseStatus(HttpStatus.OK)
    @Transactional(rollbackFor = Exception.class)
    public void handleFileUpload(@RequestPart("body") Object body, @RequestPart("files") MultipartFile file, @RequestParam String entityName) throws Exception {
        fileService.storeFile(file);
        if (entityName != null && !"".equals(entityName)) {
            Class clazz = Class.forName(PKG_DTO + entityName);
            //resolve spring generic problem
            Object req = mapper.convertValue(body, clazz);
            commonService.save(req);
        }
    }

    @PostMapping(value = "/upload")
    @ResponseStatus(HttpStatus.OK)
    @Transactional(rollbackFor = Exception.class)
    public void uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        fileService.storeFile(file);
    }

    @PostMapping(value = "/uploadV2")
    @ResponseStatus(HttpStatus.OK)
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> uploadFile2(@RequestPart("files") MultipartFile[] files) throws Exception {

        return Optional.ofNullable(fileService.storeFileRandomName(files))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping(value = "/common/option")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getCommonOption(@RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        String sql = "select cod.*,co.type,co.code from common_option_description cod join common_option co on co.id = cod.common_option_id where cod.lang_id = :langId";
        HashMap<String, Object> param = new HashMap<>();
        param.put("langId", langId);
        List<CommonOptionDescription> lstOptions = commonService.findAll(null, CommonOptionDescription.class, sql, param);
        Map<String, List<CommonOptionDescription>> results =
                lstOptions.stream().collect(Collectors.groupingBy(CommonOptionDescription::getCode));
        return new ResponseEntity<>(results, HttpStatus.OK);
    }
}
