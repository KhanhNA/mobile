package com.nextsolutions.dcommerce.ui.model.incentive;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class IncentiveProgramDescriptionResponse {
    private Long id;
    private Long incentiveProgramId;
    private String name;
    private String description;
    private String languageCode;
    private Long languageId;
    private String shortDescription;
    private String imageUrl;
}
