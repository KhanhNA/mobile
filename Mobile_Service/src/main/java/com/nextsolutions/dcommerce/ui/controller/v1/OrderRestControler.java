package com.nextsolutions.dcommerce.ui.controller.v1;

import com.google.gson.Gson;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.model.NotificationMerchant;
import com.nextsolutions.dcommerce.model.OrderTemplate;
import com.nextsolutions.dcommerce.model.OrderTransferAmount;
import com.nextsolutions.dcommerce.model.firebase.NotificationAction;
import com.nextsolutions.dcommerce.model.firebase.PushNotificationRequest;
import com.nextsolutions.dcommerce.model.firebase.TokenFbMerchant;
import com.nextsolutions.dcommerce.model.order.Order;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.service.NotificationMerchantService;
import com.nextsolutions.dcommerce.service.OrderService;
import com.nextsolutions.dcommerce.service.PaymentGatewayService;
import com.nextsolutions.dcommerce.service.chat.ChatService;
import com.nextsolutions.dcommerce.service.event.PushNotificationConfirmOrder;
import com.nextsolutions.dcommerce.service.event.PushNotificationOrderSuccessEvent;
import com.nextsolutions.dcommerce.service.firebase.PushNotificationService;
import com.nextsolutions.dcommerce.shared.dto.BaseResponse;
import com.nextsolutions.dcommerce.shared.dto.HisOrderTransfer;
import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import com.nextsolutions.dcommerce.shared.dto.chat.MessageType;
import com.nextsolutions.dcommerce.shared.dto.chat.NotificationCenterDataDTO;
import com.nextsolutions.dcommerce.shared.dto.wallet.TransferRequestBuilder;
import com.nextsolutions.dcommerce.utils.StringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/v1")
@Transactional
@RequiredArgsConstructor
public class OrderRestControler {

    private final OrderService orderService;
    private final CommonService commonService;
    private final ApplicationEventPublisher applicationEventPublisher;

    @GetMapping("/orders/del-template")
    public ResponseEntity<?> deleteOrderTemplate(@RequestParam(name = "orderTemplateId") Long orderTemplateId) throws Exception {
        orderService.deleteOrderTemplate(orderTemplateId);
        return new ResponseEntity<>(
                BaseResponse.builder().code(HttpStatus.OK.value()).message("del-success").build(),
                HttpStatus.OK);
    }

    @GetMapping("/orders/edit-template")
    public ResponseEntity<?> editOrderTemplate(@RequestParam(name = "orderTemplateId") Long orderTemplateId, @RequestParam(name = "name") String name) throws Exception {
        return Optional.ofNullable(orderService.editOrderTemplate(orderTemplateId, name))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("orders/delete")
    @Transactional(rollbackFor = {Exception.class})
    public ResponseEntity<?> delete(@RequestParam Long orderId) throws Exception {
        Order order = orderService.find(Order.class, orderId);
        if (order != null) {
            orderService.deleteOrder(orderId);
        }
        return Optional.ofNullable(order)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping("/orders/changeOrder")
    public ResponseEntity<?> changeOrder(@RequestBody OrderV2Dto orderDto) throws Exception {
        orderService.changeOrder(orderDto);
        //Send notification
        applicationEventPublisher.publishEvent(new PushNotificationConfirmOrder(this, orderDto));
        // End send
        return new ResponseEntity<>(orderDto, HttpStatus.OK);
    }


    @PostMapping("/orders/changeOrderWithoutNoti")
    public ResponseEntity<?> changeOrderWithoutNoti(@RequestBody OrderV2Dto orderDto) throws Exception {
        orderService.changeOrder(orderDto);
        return new ResponseEntity<>(orderDto, HttpStatus.OK);
    }


    @PostMapping("/orders/detail")
    public OrderV2Dto viewOrderDetail(Pageable pageable, @RequestBody OrderV2Dto entityOrder, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return orderService.getOrders(pageable, entityOrder, true, langId);

    }

    @PostMapping("/orders/save-template")
    public ResponseEntity<OrderTemplate> saveTemplate(@RequestBody OrderTemplate orderTemplate) throws Exception {
        return new ResponseEntity<>(commonService.save(orderTemplate), HttpStatus.OK);

    }

    @PostMapping("/changeStatus")
    public ResponseEntity changeStatus(@RequestBody OrderDto orderDto) throws Exception {
        if (orderService.changeOrderStatus(orderDto)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/get-order-transfer")
    public ResponseEntity<?> getListOrderTransfer(Long merchantId) throws Exception {
        String sql =
                "  SELECT ota.id, ota.order_id orderId, sum(ota.amount) amount,ota.create_date createDate" +
                        " FROM " +
                        " order_transfer_amount ota " +
                        " JOIN orders o on ota.order_id = o.order_id and o.MERCHANT_ID = :merchantId " +
                        " WHERE ota.status = 1" +
                        " group by ota.order_id  order by ota.create_date desc ";

        return new ResponseEntity<>(commonService.findAll(null, HisOrderTransfer.class, sql, merchantId), HttpStatus.OK);
    }

    @RequestMapping(value = "/getOrderTemplateDetail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<?> getOrderTemplate(Pageable pageable, Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return orderService.getOrderTemplateByMerchantId(pageable, merchantId, langId);
    }
}
