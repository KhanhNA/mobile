package com.nextsolutions.dcommerce.ui.model.response;

import com.nextsolutions.dcommerce.shared.dto.product.PackingProductDto;
import lombok.Data;

import java.util.List;

@Data
public class PackingProductResponseModel {
    private Long productId;
    private List<PackingProductDto> packingProducts;
}
