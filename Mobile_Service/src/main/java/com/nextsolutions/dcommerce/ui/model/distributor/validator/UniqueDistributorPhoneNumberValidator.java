package com.nextsolutions.dcommerce.ui.model.distributor.validator;

import com.nextsolutions.dcommerce.service.DistributorService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueDistributorPhoneNumberValidator implements ConstraintValidator<UniqueDistributorPhoneNumber, String> {

    private final DistributorService distributorService;

    public UniqueDistributorPhoneNumberValidator(DistributorService distributorService) {
        this.distributorService = distributorService;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !distributorService.findByPhoneNumber(value).isPresent();
    }
}
