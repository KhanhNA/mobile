package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RecommendSearchRequestModel {
    private Long id;
    private Long recommendId;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private Integer type;
}
