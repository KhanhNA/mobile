package com.nextsolutions.dcommerce.ui.model.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ValidatorResponseModel {
    private final String code;
    private final boolean status;
}
