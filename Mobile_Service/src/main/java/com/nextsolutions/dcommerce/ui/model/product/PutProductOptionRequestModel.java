package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PutProductOptionRequestModel {
    private Long id;
    private Long attributeId;
    private Long attributeValueId;
}
