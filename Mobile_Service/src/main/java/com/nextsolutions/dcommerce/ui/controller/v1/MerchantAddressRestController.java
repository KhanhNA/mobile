package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.MerchantAddress;
import com.nextsolutions.dcommerce.service.MerchantAddressService;
import com.nextsolutions.dcommerce.shared.dto.BaseResponse;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1")

@Transactional
public class MerchantAddressRestController {
    @Autowired
    MerchantAddressService merchantAddressService;


    @GetMapping("/merchantAddress")
    public Page<MerchantAddress> findAll(Pageable pageable, @RequestParam Long merchantId) {
        pageable = PageRequest.of(0, Integer.MAX_VALUE);
        return merchantAddressService.findAllByMerchant(pageable, merchantId);
    }

    @PostMapping("/newAddress")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> register(@RequestBody MerchantAddress dto) throws Exception {
        merchantAddressService.save(dto);
        return Optional.ofNullable(dto)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }

    @GetMapping("/merchant-address/delete")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@RequestParam(name = "addressId") Long addressId) throws Exception {
        MerchantAddress merchantAddress = merchantAddressService.get(addressId);
        if (merchantAddress == null) {
            throw new CustomErrorException("addressId is not exist");
        }
        merchantAddressService.deleteMerchantAddress(merchantAddress);
        return new ResponseEntity<>(BaseResponse.builder().code(HttpStatus.OK.value()).message("Delete success").build(), HttpStatus.OK);
    }

    @PostMapping("/merchant-address/update")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@RequestBody MerchantAddress merchantAddressDTO) throws Exception {
        if (merchantAddressDTO.getMerchantId() == null || merchantAddressDTO.getId() == null) {
            throw new CustomErrorException("MerchantId and addressId is not null");
        }

        MerchantAddress merchantAddressCopy = merchantAddressService.get(merchantAddressDTO.getId());
        Utils.copyNonNullProperties(merchantAddressDTO, merchantAddressCopy);
        merchantAddressService.save(merchantAddressCopy);
        //
        return Optional.of(merchantAddressCopy)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
}
