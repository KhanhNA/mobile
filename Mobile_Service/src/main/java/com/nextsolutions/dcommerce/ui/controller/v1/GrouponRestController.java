package com.nextsolutions.dcommerce.ui.controller.v1;

import com.google.gson.Gson;
import com.nextsolutions.dcommerce.model.GrouponRegister;
import com.nextsolutions.dcommerce.model.GrouponRegisterDetail;
import com.nextsolutions.dcommerce.model.firebase.NotificationAction;
import com.nextsolutions.dcommerce.model.NotificationMerchant;
import com.nextsolutions.dcommerce.model.firebase.PushNotificationRequest;
import com.nextsolutions.dcommerce.service.GrouponService;
import com.nextsolutions.dcommerce.service.NotificationMerchantService;
import com.nextsolutions.dcommerce.shared.dto.GrouponRegistryDTO;
import com.nextsolutions.dcommerce.shared.dto.GrouponRegistryStatusDTO;
import com.nextsolutions.dcommerce.service.firebase.PushNotificationService;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/groupon")
@Transactional
public class GrouponRestController {
    @Autowired
    private GrouponService grouponService;
    @Autowired
    private PushNotificationService pushNotificationService;
    @Autowired
    private NotificationMerchantService notificationService;

    @PostMapping("/register")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> register(@RequestBody GrouponRegisterDetail grouponRegisterDetail) throws Exception {
        GrouponRegister grouponRegister = grouponService.register(grouponRegisterDetail);
        try {
            if (grouponRegister != null) {
                //
                NotificationMerchant notificationMerchant = new NotificationMerchant();
//                notificationMerchant.setTitle(grouponRegisterDetail.getMerchantId() + " đã đăng ký mua groupon");
//                notificationMerchant.setMessage("Tổng số lượng hiện tại: " + grouponRegister.getTotalQuantity() + "\n" + "Số lượng cần đạt: " + grouponRegister.getTotalQuantityNext());
                notificationMerchant.setSenderId(grouponRegisterDetail.getMerchantId());
                notificationMerchant.setTopic("groupon_" + grouponRegisterDetail.getIncentiveProgramId() + "_" + grouponRegister.getId());
                notificationMerchant.setTypeId(NotificationAction.PROMOTION.getValue());
                notificationMerchant.setActionId(grouponRegister.getId());
                notificationMerchant.setClick_action("com.ts.dcommerce.TARGET_GROUPON");
                notificationMerchant.setIsSeen(0);
                // Send custom data
                GrouponRegistryStatusDTO statusDTO = new GrouponRegistryStatusDTO();
                statusDTO.setTotalQuantity(grouponRegisterDetail.getRegisterQuantity());
                statusDTO.setTotalQuantityNext(grouponRegister.getTotalQuantityNext());
                statusDTO.setIncentiveAmountNext(grouponRegister.getIncentiveAmountNext());
                statusDTO.setIncentivePercentNext(grouponRegister.getIncentivePercentNext());

                //Send notification to topic
                pushNotificationService.sendPushNotificationToTopic(Utils.getData(notificationMerchant,PushNotificationRequest.class));
                // Save notification
                notificationService.save(notificationMerchant);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(grouponRegister);

    }

    @PostMapping("/testRegister")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> testRegister() throws Exception {
        GrouponRegisterDetail groupon = new GrouponRegisterDetail();
        groupon.setIncentiveProgramId(3L);
        groupon.setPackingProductId(187L);
        groupon.setMerchantId(86L);
        groupon.setRegisterQuantity(100);
        GrouponRegister grouponRegister = grouponService.register(groupon);

        return ResponseEntity.ok(grouponRegister);

    }

    @GetMapping("/registerStatus")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> registerStatus(@RequestParam("registerId") Long registerId) throws Exception {
        GrouponRegistryStatusDTO registryStatusDTO = grouponService.getGrouponStatus(registerId);
        return Optional.of(registryStatusDTO)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/listRegistry")
    @Transactional(rollbackFor = Exception.class)
    public Page<GrouponRegistryDTO> getListRegistry(Pageable pageable, @RequestParam("registerId") Long registerId) throws Exception {
        return grouponService.getListRegistry(pageable, registerId);
    }

    @GetMapping("/listGrouponProgram")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getListGroupon(@RequestParam("parentMerchantId") Long parentMerchantId) throws Exception {
        return Optional.of(grouponService.getListGrouponProgram(parentMerchantId))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }


}
