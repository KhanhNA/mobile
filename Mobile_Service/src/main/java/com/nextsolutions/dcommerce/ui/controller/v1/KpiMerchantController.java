package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.kpi.KpiMerchant;
import com.nextsolutions.dcommerce.service.KpiMerchantService;
import com.nextsolutions.dcommerce.shared.dto.kpi.KpiMerchantDTO;
import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ApiConstant.API_V1)
@RequiredArgsConstructor
public class KpiMerchantController {

    private final KpiMerchantService kpiMerchantService;

    @PostMapping("/kpi-l1")
    public ResponseEntity<KpiMerchant> createKpiL1(@RequestBody @Valid KpiMerchant request) throws Exception {
        return ResponseEntity.ok(kpiMerchantService.createKpiMerchant(request));
    }

    @PostMapping("/edit-kpi-l1")
    public ResponseEntity<KpiMerchant> editKpiL1(@RequestBody @Valid KpiMerchant request) throws Exception {
        return ResponseEntity.ok(kpiMerchantService.editKpiMerchant(request));
    }

    @GetMapping("/list-kpi-l1")
    public Page<KpiMerchant> getKpiL1(Pageable pageable, KpiMerchantDTO merchantKpiRequest) throws Exception {
        return kpiMerchantService.getListKpiL1(pageable, merchantKpiRequest);
    }

    @PatchMapping("/kpi-l1/{id}")
    public KpiMerchant getKpiL1(@PathVariable Long id) throws Exception {
        return kpiMerchantService.getKpiMerchantById(id);
    }


    @GetMapping("/kpi-merchant")
    public List<KpiMerchant> getKpiMerchant(Long merchantId) throws Exception {
        return kpiMerchantService.getKpiMerchant(merchantId);
    }

}
