package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.MerchantAddress;
import com.nextsolutions.dcommerce.service.MerchantAddressService;
import com.nextsolutions.dcommerce.service.WareHouseService;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")

@Transactional
public class WarehouseController {
    @Autowired
    WareHouseService wareHouseService;


    @GetMapping("merchant-warehouses")
    public ResponseEntity<?> findAll() throws Exception {

        return Optional.ofNullable(wareHouseService.getAll())
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping(value = "merchant-groups", params = {"warehouseCode"})
    public ResponseEntity<?> getByStoreCode(@RequestParam List<String> warehouseCode) throws Exception {
        return Optional.ofNullable(wareHouseService.getAllByWarehouseCode(warehouseCode))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }


}
