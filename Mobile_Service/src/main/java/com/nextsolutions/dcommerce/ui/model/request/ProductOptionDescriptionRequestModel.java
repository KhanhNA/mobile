package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

@Data
public class ProductOptionDescriptionRequestModel {
    private Long id;
    private Long productOptionId;
    private String name;
    private Long languageId;
}
