package com.nextsolutions.dcommerce.ui.model.distributor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetDistributorRequestModel {
    private String code;
    private String name;
    private String phoneNumber;
    private String email;
}
