package com.nextsolutions.dcommerce.ui.controller.v2;

import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import com.nextsolutions.dcommerce.model.PackingType;
import com.nextsolutions.dcommerce.repository.PackingTypeRepository;
import com.nextsolutions.dcommerce.shared.dto.resource.SimpleApiResource;
import com.nextsolutions.dcommerce.ui.model.response.PackingTypeLookupResponseModel;
import com.nextsolutions.dcommerce.service.exception.DuplicateEntityException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(ApiConstant.API_V2)
@RequiredArgsConstructor
public class PackingTypeController {

    private final PackingTypeRepository packingTypeRepository;

    @GetMapping("packing-types")
    public ResponseEntity<List<PackingTypeLookupResponseModel>> findAllPackingTypes() {
        return new ResponseEntity<>(packingTypeRepository.findAllPackingTypeLookup(), HttpStatus.OK);
    }

    @PostMapping("packing-types")
    public ResponseEntity<Map<String, Object>> findAllPackingTypes(@RequestBody PackingType packingType) {
        String code = "S" + packingType.getQuantity();
        PackingType packingTypeDb = packingTypeRepository.findByCode(code);
        if(packingTypeDb != null) {
            throw new DuplicateEntityException(SimpleApiResource.builder()
                    .code("409001")
                    .message("Packing Type is exists")
                    .value(packingTypeDb)
                    .build());
        }
        packingType.setCode(code);
        packingTypeRepository.save(packingType);
        Map<String, Object> result = new HashMap<>();
        result.put("packingType", packingType);
        result.put("packingTypes", packingTypeRepository.findAllPackingTypeLookup());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
