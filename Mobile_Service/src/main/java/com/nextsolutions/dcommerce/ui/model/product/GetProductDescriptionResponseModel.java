package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetProductDescriptionResponseModel {
    private Long id;
    private String localizedName;
    private String title;
    private Integer languageId;
    private String description;
    private String metaTagTitle;
    private String metaTagDescription;
    private String metaTagKeywords;
    private List<String> productTags;
}
