package com.nextsolutions.dcommerce.ui.controller.v1;


import com.nextsolutions.dcommerce.service.impl.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class FileController {

    @Autowired
    private FileStorageService fileStorageService;

//    @Autowired
//    public FileController(FileService fileService) {
//        this.fileService = fileService;
//    }

    @Value("classpath:no_image_available.svg")
    Resource noImageAvailable;

    @GetMapping("/files/**")
    public ResponseEntity<Object> get(HttpServletRequest request) {
        String relativeUrl = extractId(request);
        try {
            Resource resource = this.fileStorageService.loadFileAsResource(relativeUrl);
            HttpHeaders httpHeaders = this.fileStorageService.loadHttpHeaders(resource);
            return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/filesPdf/**")
    public ResponseEntity<Object> getLoadDF(HttpServletRequest request) {
        String relativeUrl = extractId(request);
        try {
            Resource resource = this.fileStorageService.loadFileAsResource(relativeUrl);
            HttpHeaders httpHeaders = this.fileStorageService.loadHttpHeadersPDF(resource);
            httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"");
            return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/download/files/**")
    public ResponseEntity<Object> getDownLoadPDF(HttpServletRequest request) {
        String relativeUrl = extractId(request);
        try {
            Resource resource = this.fileStorageService.loadFileAsResource(relativeUrl);
            HttpHeaders httpHeaders = this.fileStorageService.downloadHttpHeaders(resource);
            return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/static/**")
    public ResponseEntity<Object> getImage(HttpServletRequest request) {
        try {
            Resource resource = this.fileStorageService.loadFileAsResource(extractId(request));
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Cache-Control", "max-age=31536000");
            return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                    .headers(httpHeaders)
                    .contentType(MediaTypeFactory
                            .getMediaType(resource)
                            .orElse(MediaType.APPLICATION_OCTET_STREAM))
                    .body(resource);
        } catch (Exception e) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Cache-Control", "max-age=31536000");
            httpHeaders.add("Content-Type", "image/svg+xml");
            return new ResponseEntity<>(noImageAvailable, httpHeaders, HttpStatus.OK);
        }
    }

    private String extractId(HttpServletRequest request) {
        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE); // /elements/CATEGORY1/CATEGORY1_1/ID
        String bestMatchPattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE); // /elements/**
        return new AntPathMatcher().extractPathWithinPattern(bestMatchPattern, path); // CATEGORY1/CATEGORY1_1/ID
    }

    @PostMapping(value = "/api/files2")
    @ResponseStatus(HttpStatus.OK)
    public void handleFileUpload(@RequestPart("body") Object body, @RequestPart("files") MultipartFile file) throws IOException {
        fileStorageService.storeFile(file);
    }

    @PostMapping(value = "/files/{directory:[a-z]+}")
    public ResponseEntity<Map<String, String>> handleEditorFileUpload(
            @PathVariable String directory,
            @RequestPart("file") MultipartFile file) throws IOException {
        Path filePath = fileStorageService.saveFileToStorage(file, directory);
        return ResponseEntity.ok(Collections.singletonMap("default", fileStorageService.toRelativePath(filePath)));
    }
}
