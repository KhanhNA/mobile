package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.MerchantQueryService;
import com.nextsolutions.dcommerce.ui.model.request.MerchantRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantResource;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class MerchantQueryController {

    private final MerchantQueryService merchantQueryService;

    @GetMapping("/merchants/query")
    public ResponseEntity<Page<MerchantResource>> findByQuery(MerchantRequestModel query, Pageable pageable) {
        return ResponseEntity.ok(merchantQueryService.findByQuery(query, pageable));
    }
}
