package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.ProductDescription;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.service.ProductService;
import com.nextsolutions.dcommerce.shared.dto.*;
import com.nextsolutions.dcommerce.shared.dto.incentivev2.ProductIncentiveInformationDTO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class ProductRestController {

    private final ProductService productService;
    private final CommonService commonService;

    @GetMapping("/products")
    public Page<ProductDto> findAll(Pageable pageable) throws Exception {
        return productService.getAllProductCus(pageable, 1);
    }

    @GetMapping("/products/getAll")
    public Page<ProductDto> findAllV2(Pageable pageable, @RequestParam Long langId) {
        return productService.getAllProductCusV2(pageable, langId, "");
    }

    @GetMapping("/products/all")
    public Page<ProductInfoDTO> getAll(Pageable pageable, @RequestParam Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return productService.getProductAll(pageable, merchantId, langId);
    }

    @PostMapping("/products/all-not-in")
    public Page<ProductInfoDTO> getAllNotIn(Pageable pageable, @RequestBody RequestProductDTO request, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return productService.getProductAllNotIn(pageable, request, langId);
    }

    @GetMapping("/products/manufacturer")
    public Page<ProductInfoDTO> getProductByManufacturerId(Pageable pageable, Long merchantId, Long manufacturerId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return productService.getProductByManufacturerId(pageable, merchantId, manufacturerId, langId);
    }

    @PostMapping("/products/product-of-merchant")
    public ProductCustomDTO getProductOfMerchant(Pageable pageable, @RequestBody UserPrediction userPrediction, @RequestHeader(name = "Accept-Language") Long langId) throws IOException {
        return productService.getProductOfMerchant(pageable, userPrediction.getLstPrediction(), userPrediction.getMerchantId(), langId, "", userPrediction.getFilterType());
    }

    @PostMapping("/products/prediction")
    public ProductCustomDTO getPredictionProduct(Pageable pageable, @RequestBody UserPrediction userPrediction, @RequestHeader(name = "Accept-Language") Long langId) throws IOException {
        return productService.getPredictionProduct(pageable, userPrediction.getLstPrediction(), userPrediction.getMerchantId(), langId, "");
    }

    @GetMapping("/products/search")
    public Page<ProductDto> searchByKey(Pageable pageable, @RequestParam Long langId, @RequestParam String keyWord) {
        return productService.getAllProductCusV2(pageable, langId, keyWord);
    }

    @GetMapping("/products/groupon")
    public Page<PackingProductDTO> groupon(Pageable pageable, @RequestParam Long langId) {
        return productService.getGroupon(pageable, langId);
    }

    @GetMapping("/products/category")
    public Page<ProductInfoDTO> findAllByCategory(Pageable pageable, @RequestParam Long categoryId, @RequestParam Long merchantId, @RequestParam Long langId) throws Exception {
        return productService.findAllByCategory(pageable, merchantId, categoryId, langId);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<ProductDetailDto> getProduct(@PathVariable("id") Long id, @RequestParam Long langId) throws Exception {
        ProductDetailDto productDetailDto = productService.getProductDetail(id, langId);
        return Optional.ofNullable(productDetailDto)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/productsV2/detail")
    public ResponseEntity<ProductDetailDTOV2> getProductV2(@NonNull @RequestParam Long productId, @NonNull @RequestParam Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        ProductDetailDTOV2 productDetailDto = productService.getProductDetailV2(merchantId, productId, langId);
        return Optional.ofNullable(productDetailDto)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/products/incentive/{packingProductId}")
    public Page<IncentiveDescriptionDTO> getIncentivePacking(Pageable pageable, @PathVariable("packingProductId") Long id, @RequestHeader(name = "Accept-Language") Long langId) {
        return productService.getIncentiveFromPacking(pageable, id, langId);
    }

    @GetMapping("/products/desc")
    public ResponseEntity<HashMap<Long, ProductDescription>> getProductDesc(@RequestParam Long id) throws Exception {
        HashMap<Long, ProductDescription> ret = productService.getProductDesc(id);
        return Optional.ofNullable(ret)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/products/suggestion")
    public ResponseEntity<?> suggestionSearch(@RequestParam String keyWord, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        HashMap<String, Object> param = new HashMap<>();
        String sql = "select " +
                "pd.product_name productName, " +
                "pp.packing_product_id packingProductId, " +
                "pp.product_id productId, " +
                "pt.quantity " +
                "from " +
                "product_description pd " +
                "join product p on p.PRODUCT_ID = pd.PRODUCT_ID " +
                "join packing_product pp on " +
                "pd.product_id = pp.PRODUCT_ID " +
                "join packing_type pt on " +
                "pt.PACKING_TYPE_ID = pp.PACKING_TYPE_ID " +
                "join packing_price price on " +
                "price.packing_product_id = pp.PACKING_PRODUCT_ID " +
                "and price.price_type = 2 " +
                "and date(current_date()) >= date(price.from_date) and (date(price.to_date)>= date(current_date()) or price.to_date is null) " +
                "where " +
                "p.status =1 and " +
                "pd.LANGUAGE_ID = :langId " +
                "and (lower(pd.PRODUCT_NAME) like :keyWord " +
                "or lower(pp.code) like :keyWord1 ) " +
                "group by " +
                "pp.product_id " +
                "limit 20 ";
        param.put("langId", langId);
        param.put("keyWord", "%" + keyWord.toLowerCase() + "%");
        param.put("keyWord1", "%" + keyWord.toLowerCase() + "%");
        List<SuggestionSearchDTO> suggestionSearchDTOList = commonService.findAll(null, SuggestionSearchDTO.class, sql, param);
        return Optional.ofNullable(suggestionSearchDTOList)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/products/supperSale")
    public Page<PackingSupperSaleDTO> getProductSupperSale(Pageable pageable, @RequestParam Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return productService.getProductSupperSale(pageable, merchantId, langId);
    }


    @GetMapping("/products/getPoint")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getLoyalPoint(@RequestParam("packingProductId") Long packingProductId) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        params.put("packingProductId", packingProductId);
        //
        String sql = "SELECT lp.PACKING_PRODUCT_ID,ll.MIN_AMOUNT,ll.MIN_QUANTITY,ll.LOYALTY_COIN ,ll.LOYALTY_POINT " +
                " FROM loyalty_packing_sale lp" +
                " JOIN loyalty_level ll on ll.LOYALTY_PACKING_SALE_ID = lp.id" +
                " WHERE DATE(FROM_DATE)  <= DATE(CURRENT_DATE())" +
                " AND (DATE(CURRENT_DATE()) <= DATE(TO_DATE)" +
                " OR TO_DATE IS NULL) and lp.status =1 and lp.PACKING_PRODUCT_ID =:packingProductId";
        List<LoyaltyPointDTO> arrPoint = commonService.findAll(null, LoyaltyPointDTO.class, sql, params);
        return Optional.ofNullable(arrPoint)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping("/products/getPricePacking")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getPriceForPacking(@RequestBody List<Long> lstPackingId) throws Exception {
        if (lstPackingId == null || lstPackingId.size() == 0) {
            throw new CustomErrorException("List packingId is not null");
        }
        List<PackingProductDTO> data = productService.getPriceForPacking(lstPackingId);
        return Optional.ofNullable(data)
                .map(result -> new ResponseEntity<>(

                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PatchMapping("/product/{incentiveProgramId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> groupDetail(@PathVariable Long incentiveProgramId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        List<ProductIncentiveInformationDTO> data = productService.getProductByIncentiveId(incentiveProgramId, langId);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
