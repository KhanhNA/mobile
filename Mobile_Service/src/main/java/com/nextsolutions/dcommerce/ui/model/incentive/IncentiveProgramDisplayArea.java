package com.nextsolutions.dcommerce.ui.model.incentive;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = IncentiveProgramDisplayAreaValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IncentiveProgramDisplayArea {
    String message() default "{com.nextsolutions.validation.constraints.IncentiveProgramDisplayArea.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
