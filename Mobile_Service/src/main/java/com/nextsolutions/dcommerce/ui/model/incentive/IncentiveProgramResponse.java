package com.nextsolutions.dcommerce.ui.model.incentive;

import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDetailInformationDto;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramGroupDto;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class IncentiveProgramResponse {
    private Long id;
    private String code;
    private Integer type;
    private Integer status;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private String createdUser;
    private LocalDateTime createdDate;
    private String latestModifiedUser;
    private LocalDateTime latestModifiedDate;
    private Integer displayArea;
    private List<IncentiveProgramDescriptionResponse> descriptions;
    private IncentiveProgramDetailInformationResponse incentiveProgramDetailInformation;
    private List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroup;
    private IncentiveProgramGroupDto incentiveProgramGroup;
    private List<String> warehouseCodes;
    private List<Long> merchantGroupIds;
    private Boolean applyL2;
}
