package com.nextsolutions.dcommerce.ui.model.response;

import com.nextsolutions.dcommerce.shared.dto.OrderPackingInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailsResponseModel {

    private String store;
    private LocalDateTime orderDate;
    private String paymentMethod;
    private String shippingMethod;
    private String merchantName;
    private String merchantGroup;
    private String email;
    private String phoneNumber;

    private Long orderId;
    private String logisticsCode;

    private int logisticsOrderStatus;
    private List<OrderPackingInfo> details;

    public OrderDetailsResponseModel(String store, LocalDateTime orderDate, String paymentMethod, String shippingMethod, String merchantName, String merchantGroup, String email, String phoneNumber, Long orderId, String logisticsCode) {
        this.store = store;
        this.orderDate = orderDate;
        this.paymentMethod = paymentMethod;
        this.shippingMethod = shippingMethod;
        this.merchantName = merchantName;
        this.merchantGroup = merchantGroup;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.orderId = orderId;
        this.logisticsCode = logisticsCode;
    }
}
