package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class MerchantCrudRequestModel extends MerchantRequestModel {
    private Integer status;
    private Integer action;
    private List<Long> includedIds;
    private List<Long> excludedIds;
}
