package com.nextsolutions.dcommerce.ui.model.incentive;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IncentiveTypeValidator implements ConstraintValidator<ValidateIncentiveType, Integer> {
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        if (value == null) return false;
        switch (value) {
            case 1:
            case 2:
            case 3:
            case 4:
                return true;
            default:
                return false;
        }
    }
}
