package com.nextsolutions.dcommerce.ui.model.incentive;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class IncentiveProgramFlashSaleDateResponse {
    private Long id;
    private Long incentiveProgramDetailInformationId;
    private LocalDateTime date;
    private boolean zeroToNine;
    private boolean nineToTwelve;
    private boolean twelveToFifteen;
    private boolean fifteenToEighteen;
    private boolean eighteenToTwentyOne;
    private boolean twentyOneToTwentyFour;
}
