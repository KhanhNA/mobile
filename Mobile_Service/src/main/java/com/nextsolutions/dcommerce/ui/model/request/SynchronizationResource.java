package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@Setter
public class SynchronizationResource {

    @NotBlank
    private String entityName;

    @NotEmpty
    private List<Long> ids;
}
