package com.nextsolutions.dcommerce.ui.model.request;

import com.nextsolutions.dcommerce.shared.dto.resource.BannerDescriptionResource;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerDetailsResource;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class BannerFormRequestModel {
    private Long id;
    private String code;
    private List<BannerDescriptionResource> descriptions;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private List<Long> merchantGroupIds;
    private List<BannerDetailsResource> details;
}
