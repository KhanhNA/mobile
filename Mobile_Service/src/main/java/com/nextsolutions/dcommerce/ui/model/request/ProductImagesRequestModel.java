package com.nextsolutions.dcommerce.ui.model.request;

import com.nextsolutions.dcommerce.shared.dto.product.ProductImageDto;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class ProductImagesRequestModel {
    private Long productId;

    @Valid
    private List<ProductImageDto> productImages;
}
