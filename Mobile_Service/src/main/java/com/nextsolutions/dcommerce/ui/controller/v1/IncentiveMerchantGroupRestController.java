package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.incentive.IncentiveMerchantGroup;
import com.nextsolutions.dcommerce.service.IncentiveMerchantGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/incentive")
public class IncentiveMerchantGroupRestController {

    @Autowired
    private IncentiveMerchantGroupService ictMerchantGroupService;

    @PostMapping("/merchantGroup/{ictProgramId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@RequestBody List<IncentiveMerchantGroup> ictMerchantGroups, @PathVariable Long ictProgramId) throws Exception {
        ictMerchantGroupService.save(ictMerchantGroups, ictProgramId);
        return ResponseEntity.ok("");

    }
    @DeleteMapping("/merchantGroup/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@PathVariable Long id) throws Exception {
        ictMerchantGroupService.delete(id);
        return ResponseEntity.ok("");

    }


}
