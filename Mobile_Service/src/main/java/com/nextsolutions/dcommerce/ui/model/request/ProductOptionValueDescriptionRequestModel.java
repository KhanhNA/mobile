package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

@Data
public class ProductOptionValueDescriptionRequestModel {
    private Long id;
    private String name;
    private Long languageId;
}
