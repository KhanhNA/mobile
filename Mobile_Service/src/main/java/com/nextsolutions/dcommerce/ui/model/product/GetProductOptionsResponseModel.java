package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetProductOptionsResponseModel {
    private Long id;
    private Long productId;
    private Long attributeId;
    private Long attributeValueId;
}
