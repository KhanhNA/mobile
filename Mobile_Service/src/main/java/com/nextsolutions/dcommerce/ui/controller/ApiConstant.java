package com.nextsolutions.dcommerce.ui.controller;

public interface ApiConstant {
    String API_V1 = "/api/v1";
    String API_V2 = "/api/v2";
    String API_V3 = "/api/v3";
}
