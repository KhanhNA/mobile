package com.nextsolutions.dcommerce.ui.controller.v2;

import com.nextsolutions.dcommerce.service.ManufacturerService;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDto;
import com.nextsolutions.dcommerce.shared.dto.TemporaryManufacturerDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.ManufacturerResource;
import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import com.nextsolutions.dcommerce.ui.model.request.TemporaryManufacturerRequestModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApiConstant.API_V2)
@RequiredArgsConstructor
public class ManufacturerController {

    private final ManufacturerService manufacturerService;


    @GetMapping("/manufacturers")
    @PreAuthorize("hasAuthority('get/manufacturers')")
    public ResponseEntity<Page<ManufacturerResource>> getManufacturers(ManufacturerResource resource, Pageable pageable) {
        return ResponseEntity.ok(manufacturerService.getManufacturers(resource, pageable));
    }


    @GetMapping("/manufacturers/{id}")
    @PreAuthorize("hasAuthority('get/manufacturers/{id}')")
    public ResponseEntity<ManufacturerDto> getAttributeForm(@PathVariable Long id) {
        return ResponseEntity.ok(manufacturerService.findById(id));
    }

    @PostMapping("/manufacturers/requestCreate")
    @PreAuthorize("hasAuthority('post/manufacturers/requestCreate')")
    public ResponseEntity<ManufacturerDto> requestCreateManufacturer(@RequestBody @Valid ManufacturerDto form) {
        return ResponseEntity.status(HttpStatus.CREATED).body(manufacturerService.requestCreateManufacturer(form));
    }

    @PutMapping("/manufacturers/requestUpdate/{id}")
    @PreAuthorize("hasAuthority('put/manufacturers/requestUpdate/{id}')")
    public ResponseEntity<ManufacturerDto> requestUpdateManufacturer(@PathVariable Long id, @RequestBody @Valid ManufacturerDto form) {
        return ResponseEntity.ok(manufacturerService.requestUpdateManufacturer(id, form));
    }

    @PutMapping("/manufacturers/requestDelete/{id}")
    @PreAuthorize("hasAuthority('put/manufacturers/requestDelete/{id}')")
    public ResponseEntity<Void> requestDelete(@PathVariable Long id) {
        manufacturerService.requestDelete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/manufacturers/{id}")
    @PreAuthorize("hasAuthority('delete/manufacturers/{id}')")
    public ResponseEntity<Void> inactiveManufacturer(@PathVariable Long id) {
        manufacturerService.inactiveManufacturer(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @RequestMapping(value = "/manufacturers/code/{code}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/manufacturers/code/{code}')")
    public ResponseEntity<Void> existsByCode(@PathVariable String code) {
        return manufacturerService.existsByCode(code)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/manufacturers/{id}/code/{code}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/manufacturers/{id}/code/{code}')")
    public ResponseEntity<Void> existsByCode(@PathVariable Long id, @PathVariable String code) {
        return manufacturerService.existsByCode(id, code)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/manufacturers/tel/{tel}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/manufacturers/tel/{tel}')")
    public ResponseEntity<Void> existsByTel(@PathVariable String tel) {
        return manufacturerService.existsByTel(tel)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/manufacturers/{id}/tel/{tel}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/manufacturers/{id}/tel/{tel}')")
    public ResponseEntity<Void> existsByTel(@PathVariable Long id, @PathVariable String tel) {
        return manufacturerService.existsByTel(id, tel)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/manufacturers/taxCode/{taxCode}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/manufacturers/taxCode/{taxCode}')")
    public ResponseEntity<Void> existByTaxCode(@PathVariable String taxCode) {
        return manufacturerService.existsByTaxCode(taxCode)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/manufacturers/email/{email}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/manufacturers/email/{email}')")
    public ResponseEntity<Void> existByEmail(@PathVariable String email) {
        return manufacturerService.existsByEmail(email)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/manufacturers/{id}/taxCode/{taxCode}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/manufacturers/{id}/taxCode/{taxCode}')")
    public ResponseEntity<Void> existsByTaxCode(@PathVariable Long id, @PathVariable String taxCode) {
        return manufacturerService.existsByTaxCode(id, taxCode)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/manufacturers/{id}/email/{email}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/manufacturers/{id}/email/{email}')")
    public ResponseEntity<Void> existsByEmail(@PathVariable Long id, @PathVariable String email) {
        return manufacturerService.existsByEmail(id, email)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @GetMapping("/manufacturers/compare/{id}")
    @PreAuthorize("hasAuthority('get/manufacturers/compare/{id}')")
    public ResponseEntity<TemporaryManufacturerDTO> getManufacturersToCompare(@PathVariable Long id) {
        return ResponseEntity.ok(manufacturerService.getManufacturersToCompare(id));
    }

    @PutMapping("/manufacturers/manageUpdate/{id}")
    @PreAuthorize("hasAuthority('put/manufacturers/manageUpdate/{id}')")
    public ResponseEntity<ManufacturerDto> updateManufacturerByManager(@PathVariable Long id, @RequestBody @Valid TemporaryManufacturerRequestModel form) {
        return ResponseEntity.ok(manufacturerService.updateManufacturerByManager(id, form));
    }

    @PutMapping("/manufacturers/manageReject/{id}")
    @PreAuthorize("hasAuthority('put/manufacturers/manageReject/{id}')")
    public ResponseEntity<ManufacturerDto> managerReject(@PathVariable Long id) {
        return ResponseEntity.ok(manufacturerService.managerReject(id));
    }

    @PutMapping("/manufacturers/managerApproved/{id}")
    @PreAuthorize("hasAuthority('put/manufacturers/managerApproved/{id}')")
    public ResponseEntity<ManufacturerDto> managerApproved(@PathVariable Long id) {
        return ResponseEntity.ok(manufacturerService.managerApproved(id));
    }
}
