package com.nextsolutions.dcommerce.ui.validator.impl;

import com.nextsolutions.dcommerce.ui.validator.Username;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameValidator implements ConstraintValidator<Username, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.trim().isEmpty()) {
            return false;
        }
        return value.matches("^[a-zA-Z0-9_]*$");
    }
}
