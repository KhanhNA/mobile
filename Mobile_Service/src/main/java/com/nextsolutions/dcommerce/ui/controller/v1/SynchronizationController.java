package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.SynchronizationService;
import com.nextsolutions.dcommerce.ui.model.request.SynchronizationResource;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class SynchronizationController {

    private final SynchronizationService synchronizationService;

    @PostMapping("/synchronize")
    public ResponseEntity<Map<String, List<Long>>> synchronizeAllRelatedProducts(@RequestBody @Valid List<SynchronizationResource> resources) throws ClassNotFoundException {
        if (resources.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        Map<String, List<Long>> entityUpdatingFailure = synchronizationService.synchronize(resources);
        if (entityUpdatingFailure.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else if(entityUpdatingFailure.size() == resources.size()) {
            return new ResponseEntity<>(entityUpdatingFailure, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(entityUpdatingFailure, HttpStatus.OK);
        }
    }

}
