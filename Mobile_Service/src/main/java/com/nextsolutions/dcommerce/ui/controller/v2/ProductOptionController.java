package com.nextsolutions.dcommerce.ui.controller.v2;

import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import com.nextsolutions.dcommerce.service.ProductOptionService;
import com.nextsolutions.dcommerce.shared.dto.ProductOptionDto;
import com.nextsolutions.dcommerce.shared.dto.ProductOptionValueDto;
import com.nextsolutions.dcommerce.ui.model.request.ProductOptionQueryRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductOptionRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ProductOptionValueRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionTableResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductOptionValueTableResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(ApiConstant.API_V2)
@RequiredArgsConstructor
public class ProductOptionController {

    private final ProductOptionService productOptionService;

    @GetMapping("/product-options")
    public Collection<ProductOptionResponseModel> findAll(@RequestParam Long languageId) {
        return productOptionService.findAllByLanguageId(languageId);
    }

    @GetMapping("/product-options/{id}")
    public ProductOptionDto findById(@PathVariable Long id) {
        return productOptionService.findById(id);
    }

    @GetMapping("/product-options/query")
    public List<ProductOptionTableResponseModel> getProductOptionByQuery(ProductOptionQueryRequestModel query,
                                                                         @RequestParam Long languageId) {
        return productOptionService.findAllByQueryAndLanguage(query, languageId);
    }

    @GetMapping("/product-options/{id}/values")
    public List<ProductOptionValueTableResponseModel> getProductOptionValues(@PathVariable("id") Long productOptionId,
                                                                             @RequestParam Long languageId) {
        return productOptionService.findAllProductOptionValues(productOptionId, languageId);
    }

    @GetMapping("/product-options/{productOptionId}/values/{id}")
    public ProductOptionValueDto getProductOptionByQuery(@PathVariable Long productOptionId,
                                                         @PathVariable("id") Long id) {
        return productOptionService.findByIdAndProductOptionId(id, productOptionId);
    }

    @PostMapping("/product-options")
    public ResponseEntity<ProductOptionDto> createProductOption(@RequestBody ProductOptionRequestModel request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(productOptionService.createProductOption(request));
    }

    @PatchMapping("/product-options/{id}")
    public ResponseEntity<ProductOptionDto> updateProductOption(@PathVariable Long id,
                                                                @RequestBody ProductOptionRequestModel request) {
        return ResponseEntity.ok(productOptionService.updateProductOption(id, request));
    }

    @DeleteMapping("/product-options/{id}")
    public ResponseEntity<Void> deleteProductOption(@PathVariable Long id) {
        productOptionService.deleteProductOption(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/product-options/{id}/values")
    public ResponseEntity<ProductOptionValueDto> createProductOption(@PathVariable("id") Long productOptionId,
                                                                     @RequestBody ProductOptionValueRequestModel request) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(productOptionService.createProductOptionValue(productOptionId, request));
    }

    @PatchMapping("/product-options/{productOptionId}/values/{id}")
    public ResponseEntity<ProductOptionValueDto> createProductOption(@PathVariable Long productOptionId,
                                                                     @PathVariable("id") Long id,
                                                                     @RequestBody ProductOptionValueRequestModel request) {
        return ResponseEntity.ok(productOptionService.updateProductOptionValue(id, productOptionId, request));
    }

    @DeleteMapping("/product-options/{productOptionId}/values/{id}")
    public ResponseEntity<ProductOptionValueDto> createProductOption(@PathVariable Long productOptionId,
                                                                     @PathVariable("id") Long id) {
        productOptionService.deleteProductOptionValue(id, productOptionId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
