package com.nextsolutions.dcommerce.ui.validator.impl;

import com.nextsolutions.dcommerce.ui.validator.Phone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<Phone, String> {

    private Phone phone;

    @Override
    public void initialize(Phone phone) {
        this.phone = phone;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(value == null || value.trim().isEmpty()) {
            return false;
        }
        return value.matches(phone.pattern());
    }
}
