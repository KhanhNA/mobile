package com.nextsolutions.dcommerce.ui.model.request;

import com.nextsolutions.dcommerce.shared.dto.product.PackingProductDto;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class PackingProductRequestModel {

    @NotNull
    private Long productId;

    @Valid
    private List<PackingProductDto> packingProducts;
}
