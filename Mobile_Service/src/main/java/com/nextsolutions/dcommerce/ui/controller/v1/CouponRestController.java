package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.Coupon;
import com.nextsolutions.dcommerce.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/coupon")
@Transactional
public class CouponRestController {
    @Autowired
    private CouponService couponService;
    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> saveCoupon(@RequestBody Coupon coupon) throws Exception {

        coupon = couponService.saveCoupon(coupon);

        return ResponseEntity.ok(coupon);

    }

    @PostMapping("/addMerchant/{copounId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> addMerchant(@RequestBody List<Long> merchantIds, @PathVariable Long copounId) throws Exception {

        couponService.addMerchant(merchantIds, copounId);

        return ResponseEntity.ok("");

    }
    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delCoupon(@PathVariable Long id) throws Exception {

        couponService.delCoupon(id);

        return ResponseEntity.ok("");

    }

    @DeleteMapping("/delMerchant/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delMerchant(@PathVariable Long id) throws Exception {

        couponService.delMerchant(id);

        return ResponseEntity.ok("");

    }
}
