package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.shared.dto.CategoryDescriptionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.Category;
import com.nextsolutions.dcommerce.model.CategoryDescription;
import com.nextsolutions.dcommerce.service.CategoryService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CategoryRestController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categories")
	@PreAuthorize("hasAuthority('get/categories')")
    public Page<CategoryDescription> findAll(Pageable pageable) {
        pageable = PageRequest.of(0, Integer.MAX_VALUE);
        return this.categoryService.findAll(pageable);
    }

    @GetMapping("/categories/tree")
	@PreAuthorize("hasAuthority('get/categories/tree')")
    public ResponseEntity<Object> getProduct(@RequestParam(value = "id", required = false) Long id,
                                             @RequestParam(value = "langId", required = true) int langId) {
        return this.categoryService.getCategoryTreeFormat(id, langId);
    }

    @GetMapping("/categories/all-root")
	@PreAuthorize("hasAuthority('get/categories/all-root')")
    public ResponseEntity<Object> getAllRoot(@RequestParam(value = "langId", required = true) int langId) {
        return this.categoryService.getAllRoot(langId);
    }

    @GetMapping("/categories/{id}")
	@PreAuthorize("hasAuthority('get/categories/{id}')")
    public ResponseEntity<Object> getById(@PathVariable(required = true) Long id) throws CustomErrorException {
        return this.categoryService.findById(id);
    }

    @PostMapping("/categories")
    @PreAuthorize("hasAuthority('post/categories')")
    @Transactional(rollbackFor = CustomErrorException.class)
    public ResponseEntity<Object> createProduct(@RequestBody Category category) throws Exception {
        return this.categoryService.create(category);
    }

    @PatchMapping("/categories")
    @PreAuthorize("hasAuthority('patch/categories')")
    @Transactional(rollbackFor = CustomErrorException.class)
    public ResponseEntity<Object> updateProduct(@RequestBody Category category) throws CustomErrorException {
        return this.categoryService.update(category);
    }

    @DeleteMapping("/categories/{id}")
    @PreAuthorize("hasAuthority('delete/categories/{id}')")
    @Transactional(rollbackFor = CustomErrorException.class)
    public ResponseEntity<Void> deleteProduct(@PathVariable() Long id) throws CustomErrorException {
        return this.categoryService.activeDeactive(id, false);
    }

    @GetMapping("/categories/{id}/description")
    @PreAuthorize("hasAuthority('get/categories/{id}/description')")
    public ResponseEntity<CategoryDescriptionDto> findByCategoryIdAndLanguageId(
            @PathVariable Long id, @RequestParam("languageId") Long languageId) {
        return ResponseEntity.ok(categoryService.findByCategoryIdAndLanguageId(id, languageId));
    }
}
