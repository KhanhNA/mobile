package com.nextsolutions.dcommerce.ui.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HunterResponseModel {
    private Long id;
    private String fullName;
    private String phoneNumber;
    private String username;
    private String address;
    private String email;
    private Integer gender;
    private String avatar;
    private String identityNumber;
    private String identityImgFront;
    private String identityImgBack;
    private LocalDateTime registerDate;
    private LocalDateTime birthDate;
    private Integer status;
    private String bankAccountNo;

    private Long walletClientId;

    private Long parentMarchantId;

    private String contractNumber;

    private Integer cardType;

    private String fatherName;

    private String religion;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime identityCreateDate;

    private Double height;

    private String bloodType;

    private String firstName;

    private String significantFigure;

    private String lastName;

}
