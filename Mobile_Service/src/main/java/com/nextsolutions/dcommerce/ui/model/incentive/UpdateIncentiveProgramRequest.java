package com.nextsolutions.dcommerce.ui.model.incentive;

import com.nextsolutions.dcommerce.shared.constant.RequestAction;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class UpdateIncentiveProgramRequest {
    @NotBlank(message = "{com.nextsolutions.validation.constraints.NotBlank.message}")
    @UniqueIncentiveProgramCode(action = RequestAction.UPDATE)
    private String code;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @NotNull(message = "{com.nextsolutions.validation.constraints.NotNull.message}")
    private LocalDateTime fromDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime toDate;

    @IncentiveProgramDisplayArea
    private Integer displayArea;

    @NotNull(message = "{com.nextsolutions.validation.constraints.NotNull.message}")
    private List<UpdateIncentiveProgramDescriptionRequest> descriptions;
}
