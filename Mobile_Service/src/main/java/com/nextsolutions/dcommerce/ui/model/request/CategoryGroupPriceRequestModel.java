package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class CategoryGroupPriceRequestModel {

    @NotNull
    private Long categoryId;

    @NotNull
    private Long packingTypeId;

    @NotNull
    private Integer type;

    @NotNull
    private BigDecimal amountApply;

    @NotNull
    private LocalDateTime startDate;

    private LocalDateTime endDate;

    @NotNull
    private List<Long> productIds;
}
