package com.nextsolutions.dcommerce.ui.controller.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nextsolutions.dcommerce.service.ProductCrudService;
import com.nextsolutions.dcommerce.shared.dto.CurrencyDTO;
import com.nextsolutions.dcommerce.shared.dto.product.PackingPriceDto;
import com.nextsolutions.dcommerce.ui.model.request.CategoryGroupPriceRequestModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V2;

@RestController
@RequestMapping(API_V2)
@RequiredArgsConstructor
public class CategoryGroupPriceController {

    private final ProductCrudService productCrudService;

    @PostMapping("/category-group-prices")
    public ResponseEntity<Void> applyGroupPrice(@RequestBody @Valid CategoryGroupPriceRequestModel request) throws Exception {
        return ResponseEntity.ok(this.productCrudService.applyGroupPrice(request));
    }

    @GetMapping("/history-group-prices")
    @PreAuthorize("hasAuthority('get/history-group-prices')")
    public ResponseEntity<Page<PackingPriceDto>> getHistoryGroupPrice(PackingPriceDto request, Pageable pageable) throws Exception {
        return ResponseEntity.ok(this.productCrudService.getHistoryGroupPrice(request, pageable));
    }

    @PostMapping("packing/new-price")
    @PreAuthorize("hasAuthority('post/packing/new-price')")
    public ResponseEntity<PackingPriceDto> newPrice(@RequestBody PackingPriceDto request) {
        return this.productCrudService.newPrice(request)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @PostMapping("packing/edit-price")
    @PreAuthorize("hasAuthority('post/packing/edit-price')")
    public ResponseEntity<PackingPriceDto> editPrice(@RequestBody PackingPriceDto request) throws JsonProcessingException {
        return this.productCrudService.editPrice(request)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @PutMapping("packing/approved-new")
    @PreAuthorize("hasAuthority('put/packing/approved-new')")
    public ResponseEntity<Boolean> approvedNew(@RequestBody PackingPriceDto request) {
        return ResponseEntity.ok(this.productCrudService.approvedNew(request));
    }

    @PutMapping("packing/approved-edit")
    @PreAuthorize("hasAuthority('put/packing/approved-edit')")
    public ResponseEntity<PackingPriceDto> approvedEdit(@RequestBody PackingPriceDto requets) {
        return ResponseEntity.ok(this.productCrudService.approvedEdit(requets));
    }

    @PutMapping("packing/reject-request")
    @PreAuthorize("hasAuthority('put/packing/reject-request')")
    public ResponseEntity<PackingPriceDto> rejectRequest(@RequestBody PackingPriceDto request) {
        return ResponseEntity.ok(this.productCrudService.rejectRequest(request));
    }

    @GetMapping("currency")
    public ResponseEntity<List<CurrencyDTO>> getCurrency() throws Exception {
        return ResponseEntity.ok(this.productCrudService.getCurrency());
    }
}
