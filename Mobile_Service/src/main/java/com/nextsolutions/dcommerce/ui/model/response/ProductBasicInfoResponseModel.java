package com.nextsolutions.dcommerce.ui.model.response;

import com.nextsolutions.dcommerce.shared.dto.product.ProductAttributeDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductDescriptionDto;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class ProductBasicInfoResponseModel {
    private Long id;
    private String brand;
    private String color;
    private String origin;
    private String notes;
    private boolean returning;
    private Long categoryId;
    private String code;
    private BigDecimal importPrice;
    private Double discountOriginalPrice;
    private Double discountPercentOriginalPrice;
    private LocalDateTime discountFromDate;
    private LocalDateTime discountToDate;
    private String name;
    private Long productTypeId;
    private Long manufacturerId;
    private String sku;
    private BigDecimal width;
    private BigDecimal height;
    private BigDecimal weight;
    private BigDecimal length;
    private String lengthClass;
    private String weightClass;
    private Integer warningThreshold;
    private Integer lifecycle;
    private List<ProductAttributeDto> productAttributes;
    private List<ProductDescriptionDto> productDescriptions;
}
