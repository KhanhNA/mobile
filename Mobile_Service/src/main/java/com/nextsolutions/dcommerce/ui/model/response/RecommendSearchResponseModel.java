package com.nextsolutions.dcommerce.ui.model.response;

import com.nextsolutions.dcommerce.shared.dto.resource.RecommendSearchResource;

import java.time.LocalDateTime;
import java.util.List;

public class RecommendSearchResponseModel {
    Long id;
    LocalDateTime fromDate;
    LocalDateTime toDate;
    Integer type;
    List<RecommendSearchResource> recommendSearchResources;
}
