package com.nextsolutions.dcommerce.ui.model.incentive;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IncentiveProgramDisplayAreaValidator implements ConstraintValidator<IncentiveProgramDisplayArea, Integer> {
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        if (value == null) return true;
        return value == 1 || value == 2;
    }
}
