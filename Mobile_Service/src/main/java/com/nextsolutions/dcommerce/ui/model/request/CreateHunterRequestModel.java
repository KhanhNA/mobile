package com.nextsolutions.dcommerce.ui.model.request;

import com.nextsolutions.dcommerce.ui.validator.Gender;
import com.nextsolutions.dcommerce.ui.validator.Phone;
import com.nextsolutions.dcommerce.ui.validator.Username;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class CreateHunterRequestModel {

    @NotBlank(message = "{com.nextsolutions.validation.constraints.NotBlank.message}")
    @Phone(pattern = "^[0-9]{9,13}$")
    private String phoneNumber;

    @NotBlank(message = "{com.nextsolutions.validation.constraints.NotBlank.message}")
    @Size(min = 6, max = 32, message = "{com.nextsolutions.validation.constraints.Size.message}")
    @Username
    private String username;

    private String fullName;

    private Integer status;

    @Email(message = "{com.nextsolutions.validation.constraints.Email.message}")
    @Size(max = 120, message = "{com.nextsolutions.validation.constraints.MaxLength.message}")
    private String email;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime birthDate;

    @Gender(required = true)
    private Integer gender;

    @Size(max = 255, message = "{com.nextsolutions.validation.constraints.MaxLength.message}")
    private String address;

    private String bankAccountNo;

    private Long walletClientId;

    private Long parentMarchantId;

    private String contractNumber;

    private Integer cardType;

    private String fatherName;

    private String identityNumber;

    private String religion;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime identityCreateDate;

    private Double height;

    private String bloodType;

    private String firstName;

    private String significantFigure;

    private String lastName;

    public String identityImgFront;

    public String identityImgBack;

}
