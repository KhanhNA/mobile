package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.DistributorService;
import com.nextsolutions.dcommerce.shared.dto.distributor.DistributorDto;
import com.nextsolutions.dcommerce.shared.mapper.DistributorMapper;
import com.nextsolutions.dcommerce.ui.model.distributor.*;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class DistributorController {

    private final DistributorService distributorService;
    private final DistributorMapper distributorMapper;

    @GetMapping("/distributors")
    @PreAuthorize("hasAuthority('get/distributors')")
    public ResponseEntity<Page<GetDistributorResponseModel>> getDistributors(
            GetDistributorRequestModel requestModel,
            Pageable pageable
    ) {
        DistributorDto distributorDto = distributorMapper.toDistributorDto(requestModel);
        Page<DistributorDto> distributorDtoPage = distributorService.searchDistributors(distributorDto, pageable);
        return ResponseEntity.ok(distributorDtoPage.map(distributorMapper::toGetDistributorResponseModel));
    }

    @GetMapping("/distributors/search")
    @PreAuthorize("hasAuthority('get/distributors/search')")
    public ResponseEntity<Page<GetDistributorResponseModel>> searchDistributors(
            GetDistributorRequestModel requestModel,
            Pageable pageable
    ) {
        DistributorDto distributorDto = distributorMapper.toDistributorDto(requestModel);
        Page<DistributorDto> distributorDtoPage = distributorService.searchDistributors(distributorDto, pageable);
        return ResponseEntity.ok(distributorDtoPage.map(distributorMapper::toGetDistributorResponseModel));
    }

    @PostMapping(value = "/distributors")
    @PreAuthorize("hasAuthority('post/distributors')")
    public ResponseEntity<PostDistributorResponseModel> createDistributor(
            @Valid @RequestBody PostDistributorRequestModel requestModel
    ) {
        DistributorDto distributorDto = distributorMapper.toDistributorDto(requestModel);
        DistributorDto createdDistributor = distributorService.createDistributor(distributorDto);
        PostDistributorResponseModel responseModel = distributorMapper.toPostDistributorResponseModel(createdDistributor);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseModel);
    }

    @GetMapping(value = "/distributors/{id}")
    @PreAuthorize("hasAuthority('get/distributors/{id}')")
    public ResponseEntity<GetDistributorResponseModel> getDistributor(@PathVariable Long id) {
        DistributorDto distributor = distributorService.getDistributor(id);
        GetDistributorResponseModel responseModel = distributorMapper.toGetDistributorResponseModel(distributor);
        return ResponseEntity.ok(responseModel);
    }

    @PutMapping("/distributors/{id}")
    @PreAuthorize("hasAuthority('put/distributors/{id}')")
    public ResponseEntity<PutDistributorResponseModel> updateDistributor(
            @PathVariable Long id,
            @Valid @RequestBody PutDistributorRequestModel requestModel
    ) {
        DistributorDto distributorDto = distributorMapper.toDistributorDto(requestModel);
        DistributorDto updatedDistributorDto = this.distributorService.updateDistributor(id, distributorDto);
        PutDistributorResponseModel responseModel = distributorMapper.toPutDistributorResponseModel(updatedDistributorDto);
        return ResponseEntity.ok(responseModel);
    }

    @DeleteMapping("/distributors/{id}")
    @PreAuthorize("hasAuthority('delete/distributors/{id}')")
    public ResponseEntity<Void> deleteDistributor(@PathVariable Long id, @RequestHeader("Authorization") String token) {
        this.distributorService.deactivate(id, token);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/distributors/username/{username}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/username/{username}')")
    public ResponseEntity<Void> existsUsername(@PathVariable String username) {
        return distributorService.existsByUsername(username)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/{id}/username/{username}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/{id}/username/{username}')")
    public ResponseEntity<Void> existsUsername(@PathVariable Long id, @PathVariable String username) {
        return distributorService.existsByUsername(id, username)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/code/{code}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/code/{code}')")
    public ResponseEntity<Void> existsByCode(@PathVariable String code) {
        return distributorService.existsByCode(code)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/{id}/code/{code}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/{id}/code/{code}')")
    public ResponseEntity<Void> existsByCode(@PathVariable Long id, @PathVariable String code) {
        return distributorService.existsByCode(id, code)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/phoneNumber/{phoneNumber}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/phoneNumber/{phoneNumber}')")
    public ResponseEntity<Void> existsByPhoneNumber(@PathVariable String phoneNumber) {
        return distributorService.existsByPhoneNumber(phoneNumber)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/{id}/phoneNumber/{phoneNumber}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/{id}/phoneNumber/{phoneNumber}')")
    public ResponseEntity<Void> existsByPhoneNumber(@PathVariable Long id, @PathVariable String phoneNumber) {
        return distributorService.existsByPhoneNumber(id, phoneNumber)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/email/{email}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/email/{email}')")
    public ResponseEntity<Void> existsByEmail(@PathVariable String email) {
        return distributorService.existsByEmail(email)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/{id}/email/{email}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/{id}/email/{email}')")
    public ResponseEntity<Void> existsByEmail(@PathVariable Long id, @PathVariable String email) {
        return distributorService.existsByEmail(id, email)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/taxCode/{taxCode}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/taxCode/{taxCode}')")
    public ResponseEntity<Void> existsByTaxCode(@PathVariable String taxCode) {
        return distributorService.existsByTaxCode(taxCode)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/distributors/{id}/taxCode/{taxCode}", method = RequestMethod.HEAD)
    @PreAuthorize("hasAuthority('head/distributors/{id}/taxCode/{taxCode}')")
    public ResponseEntity<Void> existsByTaxCode(@PathVariable Long id, @PathVariable String taxCode) {
        return distributorService.existsByTaxCode(id, taxCode)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }
}
