package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.KpiSalesmanService;
import com.nextsolutions.dcommerce.shared.dto.KpiSalesmanDTO;
import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ApiConstant.API_V1)
@RequiredArgsConstructor
public class KpiSalesmanController {

    private final KpiSalesmanService kpiSalesmanService;

    @PostMapping("/kpi-salesman")
    public ResponseEntity<KpiSalesmanDTO> creatKpiSalesman(@RequestBody @Valid KpiSalesmanDTO kpiSalesmanDTO) {
        return ResponseEntity.ok(this.kpiSalesmanService.createKpiSalesman(kpiSalesmanDTO));
    }

    @GetMapping("/kpi-salesman")
    public ResponseEntity<Page<KpiSalesmanDTO>> kpiSalesmans(KpiSalesmanDTO kpiSalesmanDTO,Pageable pageable) throws Exception {
        return ResponseEntity.ok(this.kpiSalesmanService.kpiSalesmans(kpiSalesmanDTO ,pageable));
    }

    @GetMapping("/kpi-active")
    public ResponseEntity<KpiSalesmanDTO> kpiSalesmanActive() throws Exception{
        return ResponseEntity.ok(this.kpiSalesmanService.kpiSalesmanActive());
    }

    @PutMapping("/kpi-salesman/{id}")
    public ResponseEntity<KpiSalesmanDTO> updateKpiSalesman(@PathVariable Long id,@Valid @RequestBody KpiSalesmanDTO kpiSalesmanDTO) {
        return ResponseEntity.ok(this.kpiSalesmanService.updateKpiSalesman(id, kpiSalesmanDTO));
    }

}
