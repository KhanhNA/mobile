package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingGroup;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingGroupPacking;
import com.nextsolutions.dcommerce.service.IncentivePackingGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(ApiConstant.API_V1 + "/incentive")
public class IncentiveGroupRestController {


    @Autowired
    private IncentivePackingGroupService ictPackingGroupService;


    @PostMapping("/pg")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> insertPackingGroup(@RequestBody IncentivePackingGroup dto) throws Exception {

        ictPackingGroupService.insertPackingGroup(dto);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("/pgd/{pgId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> savePackingGroupDetail(@RequestBody List<IncentivePackingGroupPacking> dtos,
                                                    @PathVariable Long pgId) throws Exception {
        ictPackingGroupService.savePackingGroupDetail(dtos, pgId);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/pgd/del/{ictPgdId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delPackingGrpPacking(@PathVariable Long ictPgdId) throws Exception {
        ictPackingGroupService.delPackingGrpPacking(ictPgdId);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/pg/del/{ictPgId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delPackingGrp(@PathVariable Long ictPgId) throws Exception {
        ictPackingGroupService.delPackingGrp(ictPgId);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/packingForGroup/list")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> list(Pageable pageable, Long ictPackingGroupId) throws Exception {
        return ResponseEntity.ok(ictPackingGroupService.list(pageable, ictPackingGroupId));

    }

    @GetMapping("/merchant-group")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> merchantGroupList(Pageable pageable, Long ict, @RequestHeader(name = "Accept-Language") String locate) throws Exception {
        long langId;
        if ("vi".equalsIgnoreCase(locate)) {
            langId = 1;
        } else if ("en".equalsIgnoreCase(locate)) {
            langId = 2;
        } else {
            langId = 3;
        }

        return ResponseEntity.ok(ictPackingGroupService.listGroupApply(pageable, ict, langId));

    }


}
