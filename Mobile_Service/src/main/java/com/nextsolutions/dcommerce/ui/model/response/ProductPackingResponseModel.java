package com.nextsolutions.dcommerce.ui.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductPackingResponseModel {
    private Long id;
    private String imageUrl;
    private String name;
    private String code;
    private String sku;
    private String packingCode;
    private String packingName;
    private BigDecimal originalPrice;
    private BigDecimal salePrice;
    private BigDecimal discountSalePrice;
    private Float discountPercent;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
}
