package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.IncentiveProgramDetailInformation;
import com.nextsolutions.dcommerce.service.IncentiveProgramService;
import com.nextsolutions.dcommerce.shared.dto.coupon.CouponIncentiveDTO;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDetailInformationDto;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramGroupDto;
import com.nextsolutions.dcommerce.shared.dto.incentive.IncentiveProgramDto;
import com.nextsolutions.dcommerce.shared.mapper.IncentiveProgramDetailInformationMapper;
import com.nextsolutions.dcommerce.shared.mapper.IncentiveProgramGroupMapper;
import com.nextsolutions.dcommerce.shared.mapper.IncentiveProgramMapper;
import com.nextsolutions.dcommerce.ui.model.incentive.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class IncentiveProgramController {

    private final IncentiveProgramService incentiveProgramService;
    private final IncentiveProgramMapper incentiveProgramMapper;
    private final IncentiveProgramGroupMapper incentiveProgramGroupMapper;
    private final IncentiveProgramDetailInformationMapper incentiveProgramDetailInformationMapper;

    @PostMapping("/incentive-programs")
    public ResponseEntity<IncentiveProgramResponse> createIncentiveProgram(
            @Valid @RequestBody CreateIncentiveProgramRequest requestModel
    ) {
        IncentiveProgramDto incentiveProgramDto = incentiveProgramMapper.toIncentiveProgramDto(requestModel);
        IncentiveProgramDto createdIncentiveProgramDto = incentiveProgramService.createIncentiveProgram(incentiveProgramDto);
        IncentiveProgramResponse incentiveProgramResponse = incentiveProgramMapper.toIncentiveProgramResponse(createdIncentiveProgramDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(incentiveProgramResponse);
    }

    @GetMapping("/incentive-programs/{id:[0-9]+}")
    public ResponseEntity<IncentiveProgramResponse> updateIncentiveProgram(@PathVariable Long id) {
        IncentiveProgramDto createdIncentiveProgramDto = incentiveProgramService.getIncentiveProgram(id);
        IncentiveProgramResponse incentiveProgramResponse = incentiveProgramMapper.toIncentiveProgramResponse(createdIncentiveProgramDto);
        return ResponseEntity.ok(incentiveProgramResponse);
    }

    @PutMapping("/incentive-programs/{id:[0-9]+}")
    public ResponseEntity<IncentiveProgramResponse> updateIncentiveProgram(
            @PathVariable Long id,
            @Valid @RequestBody UpdateIncentiveProgramRequest requestModel
    ) {
        IncentiveProgramDto incentiveProgramDto = incentiveProgramMapper.toIncentiveProgramDto(requestModel);
        IncentiveProgramDto createdIncentiveProgramDto = incentiveProgramService.updateIncentiveProgram(id, incentiveProgramDto);
        IncentiveProgramResponse incentiveProgramResponse = incentiveProgramMapper.toIncentiveProgramResponse(createdIncentiveProgramDto);
        return ResponseEntity.ok(incentiveProgramResponse);
    }

    @PutMapping(value = "/incentive-programs/{id:[0-9]+}", params = {"command"})
    public ResponseEntity<IncentiveProgramResponse> toggleStatus(
            @PathVariable Long id,
            @RequestParam String command
    ) throws Exception {
        if ("TOGGLE_STATUS".equals(command)) {
            IncentiveProgramDto incentiveProgramDto = incentiveProgramService.toggleStatus(id);
            IncentiveProgramResponse incentiveProgramResponse = incentiveProgramMapper.toIncentiveProgramResponse(incentiveProgramDto);
            return ResponseEntity.ok(incentiveProgramResponse);
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/incentive-programs")
    public ResponseEntity<Page<GetIncentiveProgramResponse>> getIncentivePrograms(IncentiveProgramQuery query, Pageable pageable) {
        Page<IncentiveProgramDto> incentiveProgramDtoPage = incentiveProgramService.getIncentivePrograms(query, pageable);
        return ResponseEntity.ok(incentiveProgramDtoPage.map(incentiveProgramMapper::toGetIncentiveProgramResponse));
    }

    @RequestMapping(value = "/incentive-programs/code/{code}", method = RequestMethod.HEAD)
    public ResponseEntity<IncentiveProgramResponse> isExistsCode(@PathVariable String code) {
        return incentiveProgramService.existsIncentiveProgramCodeOnCreate(code)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/incentive-programs/{id:[0-9]+}/code/{code}", method = RequestMethod.HEAD)
    public ResponseEntity<IncentiveProgramResponse> isExistsCode(@PathVariable Long id, @PathVariable String code) {
        return incentiveProgramService.existsIncentiveProgramCodeOnUpdate(id, code)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @PutMapping("/incentive-programs/{id:[0-9]+}/detail")
    public ResponseEntity<IncentiveProgramResponse> updateIncentiveProgramDetailInformation(@PathVariable Long id, @RequestBody @Valid PutIncentiveProgramDetailInformationRequest requestModel) {
        IncentiveProgramDetailInformationDto incentiveProgramDto = incentiveProgramDetailInformationMapper.toIncentiveProgramDetailInformationDto(requestModel);
        IncentiveProgramDto updatedIncentiveProgram = incentiveProgramService.updateIncentiveProgramDetailInformation(id, incentiveProgramDto);
        return ResponseEntity.ok(incentiveProgramMapper.toIncentiveProgramResponse(updatedIncentiveProgram));
    }

    @PutMapping("/incentive-programs/{id:[0-9]+}/group")
    public ResponseEntity<IncentiveProgramResponse> updateIncentiveProgramGroup(@PathVariable Long id, @RequestBody @Valid PutIncentiveProgramGroupRequest requestModel) {
        IncentiveProgramGroupDto incentiveProgramGroupDto = incentiveProgramGroupMapper.toIncentiveProgramGroupDto(requestModel);
        IncentiveProgramDto incentiveProgramDto = incentiveProgramService.updateIncentiveProgramGroup(id, incentiveProgramGroupDto);
        return ResponseEntity.ok(incentiveProgramMapper.toIncentiveProgramResponse(incentiveProgramDto));
    }

    @PutMapping("/incentive-programs/{id:[0-9]+}/merchant-group")
    public ResponseEntity<IncentiveProgramResponse> updateIncentiveProgramWithMerchantGroup(@PathVariable Long id, @RequestBody PutIncentiveProgramMerchantGroup requestModel) {
        IncentiveProgramDto incentiveProgramDto = incentiveProgramMapper.toIncentiveProgramDto(requestModel);
        IncentiveProgramDto updatedIncentiveProgramDto = incentiveProgramService.updateIncentiveProgramMerchantGroup(id, incentiveProgramDto);
        return ResponseEntity.ok(incentiveProgramMapper.toIncentiveProgramResponse(updatedIncentiveProgramDto));
    }

    @PutMapping("/incentive-programs/{id:[0-9]+}/flash-sale")
    public ResponseEntity<IncentiveProgramResponse> updateIncentiveProgramWithFlashSale(@PathVariable Long id, @RequestBody @Valid List<PutIncentiveProgramFlashSale> requestModel) {
        List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroupDto = incentiveProgramDetailInformationMapper.toIncentiveProgramDetailInformationGroupDto(requestModel);
        IncentiveProgramDto updatedIncentiveProgramDto = incentiveProgramService.updateIncentiveProgramFlashSale(id, incentiveProgramDetailInformationGroupDto);
        return ResponseEntity.ok(incentiveProgramMapper.toIncentiveProgramResponse(updatedIncentiveProgramDto));
    }

    @RequestMapping(value = "/incentive-programs/{id:[0-9]+}/detail/packingId/{packingId:[0-9]+}", method = RequestMethod.HEAD)
    public ResponseEntity<IncentiveProgramResponse> isExistsPackingIdInOtherIncentivePrograms(@PathVariable Long id, @PathVariable Long packingId) {
        return incentiveProgramService.isExistsPackingIdInOtherIncentivePrograms(id, packingId)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @PutMapping("/incentive-programs/{id:[0-9]+}/coupon")
    public ResponseEntity<CouponIncentiveDTO> updateCouponCart(@PathVariable Long id, @RequestBody @Valid CouponIncentiveDTO couponIncentiveDTO) throws Exception {
        return ResponseEntity.ok(this.incentiveProgramService.updateCouponCart(id, couponIncentiveDTO));
    }

    @GetMapping("/incentive-programs/{id:[0-9]+}/coupon")
    public ResponseEntity<CouponIncentiveDTO> getCouponCart(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(this.incentiveProgramService.getCouponCart(id));
    }

    @RequestMapping(value = "/incentive-programs/{id}/coupon-code/{code}", method = RequestMethod.HEAD)
    public ResponseEntity<CouponIncentiveDTO> isExistsCouponCode(@PathVariable Long id, @PathVariable String code) throws Exception {
        return incentiveProgramService.existsCouponCodeOnUpdate(id, code)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

}
