package com.nextsolutions.dcommerce.ui.model.incentive;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PutIncentiveProgramGroupRequest {
    private Long id;
    private Long incentiveProgramId;
    private Long categoryId;
    private Long manufacturerId;
    private Integer discountType;
    private BigDecimal discountPrice;
    private BigDecimal discountPercent;
    private BigDecimal defaultLimitedQuantity;
    private List<PutIncentiveProgramDetailInformationRequest> incentiveProgramDetailInformationGroup;
}
