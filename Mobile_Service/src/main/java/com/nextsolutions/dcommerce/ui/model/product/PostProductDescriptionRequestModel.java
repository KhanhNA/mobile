package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostProductDescriptionRequestModel {

    private String title;

    @NotBlank(message = "localizedName is required")
    private String localizedName;

    @NotNull(message = "languageId is required")
    private Long languageId;

    private String description;

    private String metaTagTitle;

    private String metaTagDescription;

    private String metaTagKeywords;

    private List<String> productTags;

}
