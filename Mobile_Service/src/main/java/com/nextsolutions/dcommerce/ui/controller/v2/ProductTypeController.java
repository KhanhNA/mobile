package com.nextsolutions.dcommerce.ui.controller.v2;

import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import com.nextsolutions.dcommerce.repository.ProductTypeRepository;
import com.nextsolutions.dcommerce.ui.model.response.ProductTypeLookupResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiConstant.API_V2)
@RequiredArgsConstructor
public class ProductTypeController {

    private final ProductTypeRepository productTypeRepository;

    @GetMapping("/product-types")
    public List<ProductTypeLookupResponseModel> getProductTypes(){
        return productTypeRepository.findAllProductTypesLookup();
    }
}
