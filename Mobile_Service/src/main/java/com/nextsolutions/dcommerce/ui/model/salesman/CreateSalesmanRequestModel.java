package com.nextsolutions.dcommerce.ui.model.salesman;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nextsolutions.dcommerce.ui.validator.UniquePhoneNumber;
import com.nextsolutions.dcommerce.utils.DeserializeDateHandler;
import com.nextsolutions.dcommerce.utils.SerializeDateHandler;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateSalesmanRequestModel {

    @NotNull
    @UniquePhoneNumber
    private String phoneNumber;

    @NotNull
    private String username;

    @NotNull
    @Size(min = 2, max = 45)
    private String fullName;

    private Integer status;

    private String email;

    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonDeserialize(using = DeserializeDateHandler.class)
    private LocalDateTime birthDate;

    private Integer gender;
    private String address;
}
