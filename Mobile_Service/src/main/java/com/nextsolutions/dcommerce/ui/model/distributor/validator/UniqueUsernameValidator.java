package com.nextsolutions.dcommerce.ui.model.distributor.validator;

import com.nextsolutions.dcommerce.service.AccountService;
import com.nextsolutions.dcommerce.service.DistributorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.security.Principal;

@RequiredArgsConstructor
public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

    private final AccountService accountService;

    @Autowired
    private HttpServletRequest request;

    @Override
    public void initialize(UniqueUsername uniqueUsername) {
        // this should autowire all dependencies of this class using
        // current application context
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !accountService.existsAccount(value, request.getHeader("authorization"));
    }
}
