package com.nextsolutions.dcommerce.ui.model.request;

import com.nextsolutions.dcommerce.shared.dto.ManufacturerDescriptionDto;
import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class TemporaryManufacturerRequestModel {
    Date createdDate;
    String code;
    String tel;
    String email;
    String taxCode;
    String imageUrl;
    List<ManufacturerDescriptionDto> descriptions;
}
