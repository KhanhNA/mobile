package com.nextsolutions.dcommerce.ui.controller.v2;

import com.nextsolutions.dcommerce.repository.CategoryRepository;
import com.nextsolutions.dcommerce.shared.projection.CategoryProductProjection;
import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiConstant.API_V2)
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryRepository categoryRepository;

    @GetMapping("/categories/{id}")
    public ResponseEntity<CategoryProductProjection> getCategory(@PathVariable Long id, @RequestParam Long languageId) {
        CategoryProductProjection category = categoryRepository.getByCategoryIdAndLanguageId(id, languageId);
        if(category == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(category);
        }
    }
}
