package com.nextsolutions.dcommerce.ui.validator.impl;

import com.nextsolutions.dcommerce.repository.MerchantJpaRepository;
import com.nextsolutions.dcommerce.ui.validator.UniquePhoneNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniquePhoneNumberValidator implements ConstraintValidator<UniquePhoneNumber, String> {

    private final MerchantJpaRepository merchantJpaRepository;

    public UniquePhoneNumberValidator(MerchantJpaRepository merchantJpaRepository) {
        this.merchantJpaRepository = merchantJpaRepository;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !merchantJpaRepository.findByMobilePhone(value).isPresent();
    }
}
