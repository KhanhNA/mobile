package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.AttributeService;
import com.nextsolutions.dcommerce.shared.dto.form.AttributeForm;
import com.nextsolutions.dcommerce.shared.dto.form.AttributeValueForm;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeResource;
import com.nextsolutions.dcommerce.shared.dto.AttributeDto;
import com.nextsolutions.dcommerce.shared.dto.AttributeValueDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class AttributeController {

    private final AttributeService attributeService;

    @GetMapping("/attributes/all")
    @PreAuthorize("hasAuthority('get/attributes/all')")
    public ResponseEntity<List<AttributeResource>> findAll(@RequestParam("languageId") Integer languageId) {
        return ResponseEntity.ok(attributeService.findAllByLanguageId(languageId));
    }

    @GetMapping("/attributes/merchant-groups")
    @PreAuthorize("hasAuthority('get/attributes/merchant-groups')")
    public ResponseEntity<List<AttributeResource>> findAllMerchantGroupAttributes(@RequestParam("languageId") Integer languageId) {
        return ResponseEntity.ok(attributeService.findAllMerchantGroupAttributes(languageId));
    }

    @GetMapping("/attributes/products")
    @PreAuthorize("hasAuthority('get/attributes/products')")
    public ResponseEntity<List<AttributeResource>> findAllProductsAttributes(@RequestParam("languageId") Integer languageId) {
        return ResponseEntity.ok(attributeService.findAllProductsAttributes(languageId));
    }

    @GetMapping("/attributes/{id}")
    @PreAuthorize("hasAuthority('get/attributes/{id}')")
    public ResponseEntity<AttributeForm> getAttributeForm(@PathVariable Long id) {
        return ResponseEntity.ok(attributeService.findById(id));
    }

    @GetMapping("/attributes")
    @PreAuthorize("hasAuthority('get/attributes')")
    public ResponseEntity<Page<AttributeDto>> getAttributeTable(AttributeDto attributeDto, Pageable pageable) throws Exception {
        return ResponseEntity.ok(attributeService.getAttributeTable(attributeDto, pageable));
    }

    @PostMapping("/attributes")
    @PreAuthorize("hasAuthority('post/attributes')")
    public ResponseEntity<AttributeForm> requestCreateAttribute(@RequestBody @Valid AttributeForm form) throws IOException {
        return ResponseEntity.status(HttpStatus.CREATED).body(attributeService.requestCreateAttribute(form));
    }

    @PutMapping("/attributes/{id}")
    @PreAuthorize("hasAuthority('put/attributes/{id}')")
    public ResponseEntity<AttributeForm> requestUpdateAttribute(@PathVariable Long id, @RequestBody @Valid AttributeForm form) {
        return ResponseEntity.ok(attributeService.requestUpdateAttribute(id, form));
    }

    @DeleteMapping("/attributes/{id}")
    @PreAuthorize("hasAuthority('delete/attributes/{id}')")
    public ResponseEntity<Void> inactiveAttribute(@PathVariable Long id) {
        attributeService.inactiveAttribute(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PatchMapping("/attributes/{id}")
    @PreAuthorize(("hasAuthority('patch/attributes/{id}')"))
    public ResponseEntity<AttributeForm> RequestInactiveAttribute(@PathVariable Long id) {
        attributeService.requestInactiveAttribute(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/attributes/{id}/values")
    @PreAuthorize("hasAuthority('get/attributes/{id}/values')")
    public ResponseEntity<List<AttributeValueDto>> getAttributeValueTable(@PathVariable Long id, @RequestParam("languageId") Integer languageId) {
        return ResponseEntity.ok(attributeService.getAttributeValueTable(id, languageId));
    }

    @GetMapping("/attributes/{id}/values/{valueId}")
    @PreAuthorize("hasAuthority('get/attributes/{id}/values/{valueId}')")
    public ResponseEntity<AttributeValueForm> getAttributeForm(@PathVariable Long id, @PathVariable Long valueId) {
        return ResponseEntity.ok(attributeService.findByIdAndValueId(id, valueId));
    }

    @PostMapping("/attributes/{id}/values")
    @PreAuthorize("hasAuthority('post/attributes/{id}/values')")
    public ResponseEntity<AttributeValueForm> requestCreateAttributeValue(@RequestBody @Valid AttributeValueForm form) {
        return ResponseEntity.status(HttpStatus.CREATED).body(attributeService.requestCreateAttributeValue(form));
    }

    @PutMapping("/attributes/{id}/values/{valueId}")
    @PreAuthorize("hasAuthority('put/attributes/{id}/values/{valueId}')")
    public ResponseEntity<AttributeValueForm> requestUpdateAttributeValue(
            @PathVariable Long id,
            @PathVariable Long valueId,
            @RequestBody @Valid AttributeValueForm form) {
        return ResponseEntity.ok(attributeService.requestUpdateAttributeValue(id, valueId, form));
    }

    @PatchMapping("/attributes/{id}/values/{valueId}")
    @PreAuthorize("hasAuthority('patch/attributes/{id}/values/{valueId}')")
    public ResponseEntity<AttributeValueForm> requestInactiveAttributeValue(@PathVariable Long id,
                                                                            @PathVariable Long valueId) {
        attributeService.requestInactiveAttributeValue(id, valueId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/attributes/{id}/values/{valueId}")
    @PreAuthorize("hasAuthority('delete/attributes/{id}/values/{valueId}')")
    public ResponseEntity<Void> inactiveAttributeValue(@PathVariable Long id,
                                                       @PathVariable Long valueId) {
        attributeService.inactiveAttributeValue(id, valueId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/attributes/{id}/manager-values/{valueId}")
    @PreAuthorize("hasAuthority('put/attributes/{id}/manager-values/{valueId}')")
    public ResponseEntity<AttributeValueForm> managerUpdateAttributeValue(@PathVariable Long id,
                                                                          @PathVariable Long valueId,
                                                                          @RequestBody @Valid AttributeValueForm form) {
        return ResponseEntity.ok(this.attributeService.managerUpdateAttributeValue(id, valueId, form));
    }

    @PutMapping("/attributes/{id}/approved-values/{valueId}")
    @PreAuthorize("hasAuthority('put/attributes/{id}/approved-values/{valueId}')")
    public ResponseEntity<AttributeValueForm> managerApprovedValues(@PathVariable Long id, @PathVariable Long valueId) {
        return ResponseEntity.ok(attributeService.managerApprovedValues(id, valueId));
    }

    @PutMapping("/attributes/{id}/rejection-values/{valueId}")
    @PreAuthorize("hasAuthority('put/attributes/{id}/rejection-values/{valueId}')")
    public ResponseEntity<AttributeValueForm> managerRejectValues(@PathVariable Long id, @PathVariable Long valueId) {
        return ResponseEntity.ok(attributeService.managerRejectValues(id, valueId));
    }

    @PutMapping("/attributes/{id}/manager-approval")
    @PreAuthorize("hasAuthority('put/attributes/{id}/manager-approval')")
    public ResponseEntity<AttributeForm> managerApproved(@PathVariable Long id) {
        return ResponseEntity.ok(this.attributeService.managerApproved(id));
    }

    @PutMapping("/attributes/{id}/manager-rejection")
    @PreAuthorize("hasAuthority('put/attributes/{id}/manager-rejection')")
    public ResponseEntity<AttributeForm> managerReject(@PathVariable Long id) {
        return ResponseEntity.ok(this.attributeService.managerReject(id));
    }

    @PutMapping("/attributes/{id}/manager-update")
    @PreAuthorize("hasAuthority('put/attributes/{id}/manager-update')")
    public ResponseEntity<AttributeForm> updateByManager(@PathVariable Long id, @RequestBody @Valid AttributeForm form) {
        return ResponseEntity.ok(this.attributeService.managerUpdate(id, form));
    }
}
