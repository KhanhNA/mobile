package com.nextsolutions.dcommerce.ui.model.response;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MerchantResponseModel {
    private Long id;
    private String code;
    private String username;
    private String fullName;
    private String address;
    private String phoneNumber;
    private Integer status;
    private LocalDateTime registerDate;
}
