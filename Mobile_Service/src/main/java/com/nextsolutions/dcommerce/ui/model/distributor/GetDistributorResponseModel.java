package com.nextsolutions.dcommerce.ui.model.distributor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetDistributorResponseModel {
    private Long id;
    private String distributorId;
    private String code;
    private String name;
    private String username;
    private Long walletId;
    private String address;
    private String phoneNumber;
    private String email;
    private Boolean enabled;
    private String aboutUs;
    private String taxCode;
    private LocalDateTime createdTime;
    private LocalDateTime latestModified;
}
