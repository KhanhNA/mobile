package com.nextsolutions.dcommerce.ui.model.distributor;

import com.nextsolutions.dcommerce.ui.model.distributor.validator.UniqueDistributorPhoneNumber;
import com.nextsolutions.dcommerce.ui.model.distributor.validator.UniqueUsername;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostDistributorRequestModel {

    private String code;

    @NotNull
    @Size(min = 1, max = 255)
    private String name;

    @NotNull
    @Size(min = 1, max = 45)
    @UniqueUsername
    private String username;

    private String address;

    @NotNull
    @Size(min = 9, max = 15)
    @UniqueDistributorPhoneNumber
    private String phoneNumber;

    @NotNull
    @Email
    @Size(min = 5, max = 255)
    private String email;

    @NotNull
    @Size(min = 1, max = 255)
    private String taxCode;

    @Size(max = 5000)
    private String aboutUs;

    private Boolean enabled;

}



