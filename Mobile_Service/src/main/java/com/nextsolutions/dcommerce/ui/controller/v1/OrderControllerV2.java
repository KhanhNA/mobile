package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.order.Order;
import com.nextsolutions.dcommerce.service.OrderV2Service;
import com.nextsolutions.dcommerce.service.event.PushNotificationOrderSuccessEvent;
import com.nextsolutions.dcommerce.service.impl.HunterServiceImpl;
import com.nextsolutions.dcommerce.shared.dto.OrderTemplateDetailDTO;
import com.nextsolutions.dcommerce.shared.dto.OrderV2Dto;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;


@RestController
@RequestMapping("/api/v1/orderV2")
@Transactional
@RequiredArgsConstructor
public class OrderControllerV2 {
    private final OrderV2Service orderService;
    private final ApplicationEventPublisher applicationEventPublisher;
    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);

    @PostMapping("/cals")
    @Transactional(rollbackFor = {Exception.class})
    public OrderV2Dto calculateOrder(@RequestBody OrderV2Dto orderDto, @RequestHeader(name = "Authorization") String token, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        orderService.calculate(orderDto, langId, token);
        return orderDto;
    }

    @PostMapping("/get-orders")
    public Page<OrderV2Dto> getOrders(Pageable pageable, @RequestBody OrderV2Dto orderDTO, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return orderService.getListOrder(pageable, orderDTO, langId);
    }

    @PostMapping("/get-template")
    public Page<OrderTemplateDetailDTO> getOrderTemplate(Pageable pageable, @RequestBody OrderV2Dto entityOrder, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return orderService.getOrderTemplate(pageable, entityOrder, langId);

    }

    @PostMapping("/update-status")
    public ResponseEntity<?> changeStatus(@RequestBody Order ord) throws Exception {
        String sql = " SELECT * FROM orders where LOGISTIC_CODE = :logisticCode ";
        Order order = orderService.findOne(Order.class, sql, ord.getLogisticCode());
        if (order != null) {
            if (ord.getOrderStatus() == 2 || ord.getOrderStatus() == -1) {
                order.setOrderStatus(ord.getOrderStatus());
                order.setDeliveryDate(LocalDateTime.now());
                order.setOrderSuccessDate(LocalDateTime.now());
            } else {
                throw new CustomErrorException("order status invalidate");
            }
        } else {
            throw new CustomErrorException("orderNo not exist or order status invalidate");
        }
        orderService.save(order);

        //Send notification
        applicationEventPublisher.publishEvent(new PushNotificationOrderSuccessEvent(this, order));
        // End send

        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}
