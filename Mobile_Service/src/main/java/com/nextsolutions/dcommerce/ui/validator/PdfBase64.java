package com.nextsolutions.dcommerce.ui.validator;

import com.nextsolutions.dcommerce.ui.validator.impl.PdfBase64Validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PdfBase64Validator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PdfBase64 {
    String message() default "{com.nextsolutions.validation.constraints.pdfBase64.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
