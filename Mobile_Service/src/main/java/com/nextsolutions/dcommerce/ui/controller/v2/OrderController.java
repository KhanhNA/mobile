package com.nextsolutions.dcommerce.ui.controller.v2;

import com.nextsolutions.dcommerce.service.ImmutableOrderService;
import com.nextsolutions.dcommerce.ui.model.request.CancelOrderRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.OrderRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderDetailsResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.SimpleApiResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V2;

@RestController
@RequestMapping(API_V2)
@RequiredArgsConstructor
public class OrderController {

    private final ImmutableOrderService orderService;

    @GetMapping("/orders")
    public ResponseEntity<Page<OrderResponseModel>> findByParams(OrderRequestModel orderRequestModel, Pageable pageable) {
        return ResponseEntity.ok(orderService.findByParams(orderRequestModel, pageable));
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<OrderDetailsResponseModel> getOrderDetails(@PathVariable Long id, @RequestHeader HttpHeaders headers) {
        return ResponseEntity.ok(orderService.getOrderDetailsById(id, headers));
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<SimpleApiResponseModel> cancelOrder(@PathVariable Long id,
                                                              @RequestBody CancelOrderRequestModel cancelRequest,
                                                              @RequestHeader HttpHeaders headers) throws Exception {
        return ResponseEntity.ok(orderService.cancelOrder(id, cancelRequest, headers));
    }

}
