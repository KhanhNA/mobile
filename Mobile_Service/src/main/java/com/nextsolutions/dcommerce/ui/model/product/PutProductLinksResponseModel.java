package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PutProductLinksResponseModel {
    private Long manufacturerId;
    private Long distributorId;
    private Long categoryId;
    private List<Long> relatedProductIds;
}
