package com.nextsolutions.dcommerce.ui.model.incentive;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UpdateIncentiveProgramDescriptionRequest {
    @NotBlank(message = "{com.nextsolutions.validation.constraints.NotBlank.message}")
    @Size(min = 1, max = 500, message = "{com.nextsolutions.validation.constraints.Size.message}")
    private String name;

    @Size(max = 4000, message = "{com.nextsolutions.validation.constraints.MaxLength.message}")
    private String description;

    @NotNull(message = "{com.nextsolutions.validation.constraints.NotNull.message}")
    private Long languageId;

    @NotNull(message = "{com.nextsolutions.validation.constraints.NotNull.message}")
    private String imageUrl;

    @NotNull(message = "{com.nextsolutions.validation.constraints.NotNull.message}")
    private String shortDescription;
}
