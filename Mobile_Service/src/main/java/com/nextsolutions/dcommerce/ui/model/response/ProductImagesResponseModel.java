package com.nextsolutions.dcommerce.ui.model.response;

import com.nextsolutions.dcommerce.shared.dto.product.ProductImageDto;
import lombok.Data;

import java.util.List;

@Data
public class ProductImagesResponseModel {
    private Long productId;
    private List<ProductImageDto> productImages;
}
