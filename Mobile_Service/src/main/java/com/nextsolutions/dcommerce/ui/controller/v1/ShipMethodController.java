package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.repository.ShipMethodRepository;
import com.nextsolutions.dcommerce.model.ShipMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/shipmethod")
public class ShipMethodController {
    @Autowired
    private ShipMethodRepository shipMethodRepository ;
    @GetMapping("/getAll")
    public List<ShipMethod> findAll(){
        return shipMethodRepository.findAll();
    }
}
