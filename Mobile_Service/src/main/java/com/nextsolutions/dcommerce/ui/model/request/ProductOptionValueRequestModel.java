package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;

import java.util.List;

@Data
public class ProductOptionValueRequestModel {
    private Long id;
    private String code;
    private List<ProductOptionValueDescriptionRequestModel> descriptions;
}
