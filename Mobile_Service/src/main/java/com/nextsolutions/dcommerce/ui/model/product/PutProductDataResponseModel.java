package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PutProductDataResponseModel {
    private String code;
    private String brand;
    private String origin;
    private String sku;
    private String barcode;
    private String manufacturerPartNumber;
    private String isbn;
    private BigDecimal length;
    private BigDecimal width;
    private BigDecimal height;
    private String lengthClass;
    private BigDecimal weight;
    private String weightClass;
    private Integer lifecycle;
    private Integer status;
    private LocalDateTime dateAvailable;
}
