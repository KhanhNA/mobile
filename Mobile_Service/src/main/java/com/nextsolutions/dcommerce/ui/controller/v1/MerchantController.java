package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.service.CommonService;
import com.nextsolutions.dcommerce.service.MerchantCrudService;
import com.nextsolutions.dcommerce.service.PaymentGatewayService;
import com.nextsolutions.dcommerce.shared.dto.BaseResponse;
import com.nextsolutions.dcommerce.shared.dto.HunterDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.shared.dto.wallet.ClientType;
import com.nextsolutions.dcommerce.shared.dto.wallet.SavingsProductType;
import com.nextsolutions.dcommerce.shared.dto.wallet.WalletAccountRequestBuilder;
import com.nextsolutions.dcommerce.ui.model.request.MerchantCrudRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.shared.dto.resource.SimpleApiResource;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class MerchantController {

    private final MerchantCrudService merchantCrudService;
    private final PaymentGatewayService paymentGatewayService;
    private final CommonService commonService;

    @GetMapping("/merchants")
    @PreAuthorize("hasAuthority('get/merchants')")
    public ResponseEntity<Page<MerchantCrudResource>> findByQuery(MerchantCrudRequestModel query, Pageable pageable) {
        return ResponseEntity.ok(merchantCrudService.findByQuery(query, pageable));
    }

    @PutMapping("/merchants/{id}")
    public ResponseEntity<Void> activate(@PathVariable Long id,
                                         @RequestParam(name = "command") String command,
                                         Principal principal) {
        switch (command) {
            case "activate":
                merchantCrudService.activate(id, principal.getName());
                break;
            case "reactivate":
                merchantCrudService.reactivate(id, principal.getName());
                break;
            case "deactivate":
                merchantCrudService.deactivate(id, principal.getName());
                break;
            case "rejectActivate":
                merchantCrudService.rejectActivate(id, principal.getName());
                break;
            case "rejectAction":
                merchantCrudService.rejectAction(id, principal.getName());
                break;
            default:
                return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/merchant/active-all")
    public ResponseEntity<?> activeAll(@RequestParam @NonNull Long merchantId, String userNameHunter) {
        Merchant merchant = commonService.find(Merchant.class, merchantId);
        int countSuccess = 0;
        if (merchant == null) {
            throw new CustomErrorException("Merchant is not exist");
        }
        //Todo: Active Account Login
        try {
            merchantCrudService.activate(merchantId, userNameHunter);
            countSuccess++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Todo: Active Payment Account
        try {
            WalletAccountRequestBuilder requestBuilder = WalletAccountRequestBuilder.builder()
                    .username(merchant.getUserName())
                    .firstName(merchant.getFullName())
                    .lastName("L1")
                    .externalId(merchant.getMerchantId())
                    .clientTypeId(ClientType.L1.value())
                    .savingsProductId(SavingsProductType.L1.value())
                    .email("fake@gmail.com")
                    .build();
            Long walletId = paymentGatewayService.createWalletAccount(requestBuilder);
            countSuccess++;
            merchantCrudService.updateWalletId(merchantId, walletId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (countSuccess == 2) {
            return ResponseEntity.ok(BaseResponse.builder().code(200).message("Active success").build());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Active fail");
    }

    @PostMapping("/merchants/{id}/contract-number")
    public ResponseEntity<Boolean> updateContractNumber(@PathVariable Long id, @RequestBody HunterDto hunterDto) {
        return ResponseEntity.ok(this.merchantCrudService.updateContractNumber(id, hunterDto));
    }

    @PostMapping("merchants/{id}/confirm-kpi")
    public ResponseEntity<Merchant> confirmKpi(@PathVariable Long id) {
        return ResponseEntity.ok(this.merchantCrudService.confirmKpi(id));
    }

    @GetMapping("merchants/all")
    public ResponseEntity<Page<MerchantDto>> getAll(MerchantDto keyWord, Pageable pageable) throws Exception {
        return ResponseEntity.ok(this.merchantCrudService.getAll(keyWord, pageable));
    }
}
