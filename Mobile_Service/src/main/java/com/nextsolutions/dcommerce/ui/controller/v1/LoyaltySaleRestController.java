package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyLevel;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingSale;
import com.nextsolutions.dcommerce.service.LoyaltyPackingSaleService;
import com.nextsolutions.dcommerce.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1 + "/loyalty")
public class LoyaltySaleRestController {


    @Autowired
    private LoyaltyPackingSaleService loyaltyPackingSaleService;

    @GetMapping("/lvl/{id}")
    public ResponseEntity<?> getLoyaltyLvl(@PathVariable Long id) throws Exception {
        return new ResponseEntity<>(loyaltyPackingSaleService.find(LoyaltyLevel.class, id), HttpStatus.OK);
    }

//    @GetMapping("/loyaltyPackingSale/lpg")
//    public ResponseEntity<?> getLoyaltyPackingGroup(@RequestParam String loyaltyId,
//                                                    Pageable pageable,
//                                                    @RequestParam String status) throws Exception {
//        return new ResponseEntity<>(
//                loyaltyPackingSaleService.getLoyaltyPackingSaleList(LoyaltyPackingGroup.class,
//                        Long.parseLong(loyaltyId), Long.parseLong(status), pageable), HttpStatus.OK);
//    }

    @GetMapping("/loyaltyPackingSale/lpp")
        public ResponseEntity<?> getLoyaltyPackingSale(@RequestParam String loyaltyId,
                                                   Pageable pageable,
                                                   @RequestParam String status) throws Exception {
        return new ResponseEntity<>(
                loyaltyPackingSaleService.getLoyaltyPackingSaleList(PackingProduct.class,
                        Long.parseLong(loyaltyId), Long.parseLong(status), pageable), HttpStatus.OK);
    }

    @GetMapping("/lvl/forOrderValue")
    public ResponseEntity<?> getLoyaltyLvlForOrderValue(@RequestParam Long loyaltyId,
                                                        Pageable pageable,
                                                        @RequestParam String status) throws Exception {
        return new ResponseEntity<>(
                loyaltyPackingSaleService.getLoyaltyLvlList(loyaltyId, Long.parseLong(status), pageable), HttpStatus.OK);
    }

    @PostMapping("/lvl/{sId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> saveLoyaltyLvl(@RequestBody List<LoyaltyLevel> dtos,
                                            @PathVariable Long sId) throws Exception {

        loyaltyPackingSaleService.saveLoyaltyLvl(dtos, sId);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/lvl/forOrderValue/{loyaltyId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> saveLoyaltyLvlForOrderValue(@RequestBody List<LoyaltyLevel> dtos,
                                                         @PathVariable Long loyaltyId) throws Exception {

        loyaltyPackingSaleService.saveLoyaltyLvlForOrderValue(dtos, loyaltyId);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @DeleteMapping("/lvl/del/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delLoyaltyLvl(@PathVariable Long id) throws Exception {
        loyaltyPackingSaleService.delLoyaltyLvl(id);
        return new ResponseEntity<>("", HttpStatus.OK);
    }


    @DeleteMapping("/packingSale/del/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delLoyaltyPackingSale(@PathVariable Long id) throws Exception {

        loyaltyPackingSaleService.delLoyaltyPackingSale(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


    @PostMapping("/packingSale/{LoyaltyPrgId}/{type}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> savePackingSale(@RequestBody List<LoyaltyPackingSale> LoyaltyPackingSales,
                                             @PathVariable Long LoyaltyPrgId, @PathVariable Integer type) throws Exception {


        loyaltyPackingSaleService.savePackingSale(LoyaltyPackingSales, LoyaltyPrgId, type);
        return new ResponseEntity<>(LoyaltyPackingSales, HttpStatus.OK);
    }

    @PostMapping("/packingForSale/list")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> list(Pageable pageable, String exceptIds, String text) throws Exception {

        if (Utils.isEmpty(exceptIds)) {
            throw new CustomErrorException("exceptIdsIsNull");
        }
        List<Long> list = Arrays.stream(exceptIds.split(",")).map(Long::valueOf).collect(Collectors.toList());

        return ResponseEntity.ok(loyaltyPackingSaleService.list(pageable, list, text));

    }

}
