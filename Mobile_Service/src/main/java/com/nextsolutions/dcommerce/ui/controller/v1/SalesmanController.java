package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.SalesmanService;
import com.nextsolutions.dcommerce.shared.dto.SalesmanDto;
import com.nextsolutions.dcommerce.shared.mapper.SalesmanMapper;
import com.nextsolutions.dcommerce.ui.model.salesman.CreateSalesmanRequestModel;
import com.nextsolutions.dcommerce.ui.model.salesman.CreateSalesmanResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class SalesmanController {

    private final SalesmanMapper salesmanMapper;
    private final SalesmanService salesmanService;

    @PostMapping("/salesmen")
    public ResponseEntity<CreateSalesmanResponseModel> createSalesman(
           @Valid @RequestBody CreateSalesmanRequestModel requestModel
    ) {
        SalesmanDto salesmanDto = salesmanMapper.toSalesmanDto(requestModel);
        SalesmanDto createdSalesmanDto = salesmanService.createSalesman(salesmanDto);
        CreateSalesmanResponseModel responseModel = salesmanMapper.toCreateSalesmanResponseModel(createdSalesmanDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseModel);
    }

}
