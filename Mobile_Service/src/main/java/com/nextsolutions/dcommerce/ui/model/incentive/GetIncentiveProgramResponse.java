package com.nextsolutions.dcommerce.ui.model.incentive;

import lombok.Data;

import javax.persistence.Entity;
import java.time.LocalDateTime;

@Data
public class GetIncentiveProgramResponse {
    private Long id;
    private String code;
    private Integer type;
    private Integer status;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private String createdUser;
    private LocalDateTime createdDate;
    private String latestModifiedUser;
    private LocalDateTime latestModifiedDate;
    private String name;
    private String description;
    private Boolean canActivate;
}
