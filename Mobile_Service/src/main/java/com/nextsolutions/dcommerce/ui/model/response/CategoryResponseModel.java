package com.nextsolutions.dcommerce.ui.model.response;

import com.nextsolutions.dcommerce.shared.dto.CategoryDescriptionDto;
import lombok.Data;

import java.util.List;

@Data
public class CategoryResponseModel {
    private Long id;
    private String code;
    private List<CategoryDescriptionDto> descriptions;
}
