package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.BannerService;
import com.nextsolutions.dcommerce.ui.model.request.BannerFormRequestModel;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerDetailsResource;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerResource;
import com.nextsolutions.dcommerce.ui.model.response.BannerFormResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class BannerController {

    private final BannerService bannerService;

    @GetMapping("/banners")
    @PreAuthorize("hasAuthority('get/banners')")
    public ResponseEntity<Page<BannerResource>> findByQuery(BannerResource resource, Pageable pageable) {
        return ResponseEntity.ok(this.bannerService.findByQuery(resource, pageable));
    }

    @DeleteMapping("/banners/{id}")
    @PreAuthorize("hasAuthority('delete/banners')")
    public ResponseEntity<Void> deleteBanner(@PathVariable Long id) {
        this.bannerService.inactiveBanner(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/banners")
    @PreAuthorize("hasAuthority('post/banners')")
    public ResponseEntity<Void> createBanner(@RequestBody BannerFormRequestModel request) {
        this.bannerService.createBanner(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @GetMapping("/banners/{id}")
    @PreAuthorize("hasAuthority('get/banners/{id}')")
    public ResponseEntity<BannerFormResponseModel> getBannerForm(@PathVariable Long id) {
        return ResponseEntity.ok(this.bannerService.getBannerFormResponse(id));
    }

    @PutMapping("/banners/{id}")
    @PreAuthorize("hasAuthority('put/banners/{id}')")
    public ResponseEntity<BannerFormResponseModel> updateBanner(@PathVariable Long id, @RequestBody BannerFormRequestModel request) {
        return ResponseEntity.ok(this.bannerService.updateBanner(id, request));
    }

    @PostMapping("/banners/upload")
    @PreAuthorize("hasAuthority('post/banners/upload')")
    public ResponseEntity<Map<String, String>> uploadImage(@RequestPart("image") MultipartFile image) throws IOException {
        return ResponseEntity.ok(this.bannerService.saveImageFileToLocalServer(image));
    }

    @GetMapping("/banners/getMarketing")
    public ResponseEntity<List<BannerDetailsResource>> getMarketing(@RequestParam("merchantId") Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return ResponseEntity.ok(this.bannerService.getMarketing(merchantId, langId));
    }
}
