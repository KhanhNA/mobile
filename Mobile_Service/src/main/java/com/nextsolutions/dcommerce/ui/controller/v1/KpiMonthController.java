package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.KpiMonthService;
import com.nextsolutions.dcommerce.shared.dto.KpiMonthDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/kpi-month")
public class KpiMonthController {

    @Autowired
    KpiMonthService kpiMonthService;

    @PostMapping
    public ResponseEntity<?> create(@RequestBody KpiMonthDto kpiDto) throws Exception {
        if(kpiDto != null && kpiDto.getMerchantId() != null){
            return new ResponseEntity<>(kpiMonthService.findAll(kpiDto), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
