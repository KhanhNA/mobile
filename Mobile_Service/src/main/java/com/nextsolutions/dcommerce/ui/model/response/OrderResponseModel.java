package com.nextsolutions.dcommerce.ui.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponseModel {
    private Long id;
    private Long merchantId;
    private String merchantName;
    private Integer status;
    private BigDecimal amount;
    private LocalDateTime dateAdded;
    private LocalDateTime dateModified;
    private String logisticsCode;
    private String reason;
}
