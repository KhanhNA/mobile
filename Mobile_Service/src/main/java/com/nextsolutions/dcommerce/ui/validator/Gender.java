package com.nextsolutions.dcommerce.ui.validator;

import com.nextsolutions.dcommerce.ui.validator.impl.GenderValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = GenderValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Gender {
    boolean required() default false;

    String message() default "{com.nextsolutions.validation.constraints.Gender.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
