package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class MerchantRequestModel {
    protected String code;
    protected String username;
    protected String fullName;
    protected String phoneNumber;
    protected String address;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    protected LocalDateTime registerDate;
    private boolean exists;
    private Long merchantGroupId;
    private Integer action;
    private Long hunterId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime fromRegisterDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime toRegisterDate;
    private String searchCode;
}
