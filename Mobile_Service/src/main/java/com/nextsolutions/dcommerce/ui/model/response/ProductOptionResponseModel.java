package com.nextsolutions.dcommerce.ui.model.response;

import lombok.Data;

import java.util.List;

@Data
public class ProductOptionResponseModel {
    private Long id;
    private String code;
    private String name;
    private Integer languageId;
    private List<ProductOptionValueResponseModel> values;
}
