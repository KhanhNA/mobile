package com.nextsolutions.dcommerce.ui.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductTypeLookupResponseModel {
    private Long id;
    private String code;
    private String name;
}
