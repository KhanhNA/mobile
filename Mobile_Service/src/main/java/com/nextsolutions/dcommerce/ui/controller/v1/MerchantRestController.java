package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.configuration.properties.FileConfiguration;
import com.nextsolutions.dcommerce.configuration.properties.OAuthConfiguration;
import com.nextsolutions.dcommerce.exception.CustomErrorException;
import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.model.firebase.NotificationAction;
import com.nextsolutions.dcommerce.model.firebase.PushNotificationRequest;
import com.nextsolutions.dcommerce.model.firebase.TokenFbMerchant;
import com.nextsolutions.dcommerce.repository.AutoIncrementJpaRepository;
import com.nextsolutions.dcommerce.repository.MerchantActiveCodeRepository;
import com.nextsolutions.dcommerce.repository.MerchantImageRepository;
import com.nextsolutions.dcommerce.service.*;
import com.nextsolutions.dcommerce.service.event.PushNotificationRegisterEvent;
import com.nextsolutions.dcommerce.service.firebase.PushNotificationService;
import com.nextsolutions.dcommerce.service.impl.FileStorageService;
import com.nextsolutions.dcommerce.service.impl.HunterServiceImpl;
import com.nextsolutions.dcommerce.shared.constant.AutoIncrementType;
import com.nextsolutions.dcommerce.shared.dto.*;
import com.nextsolutions.dcommerce.utils.DateValidatorUsingDateFormat;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import com.nextsolutions.dcommerce.utils.SpringUtils;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Transactional
public class MerchantRestController {
    private final MerchantService merchantService;
    private final MerchantBaseService merchantBaseService;
    private final MerchantActiveCodeRepository merchantActiveCodeRepository;
    private final CommonService commonService;
    private final AccountService accountService;
    private final FileStorageService fileService;
    private final PushNotificationService pushNotificationService;
    private final NotificationMerchantService notificationService;
    private final MerchantImageRepository imageRepository;
    private final MerchantAddressService merchantAddressService;
    private final MerchantOverviewService overviewService;
    private final OAuthConfiguration oAuthConfiguration;
    private final AutoIncrementJpaRepository autoIncrementJpaRepository;
    private final FileConfiguration fileConfiguration;
    private static final Logger log = LogManager.getLogger(HunterServiceImpl.class);
    private final ApplicationEventPublisher applicationEventPublisher;

    @PostMapping("/merchants")
    public Page<MerchantDto> findAll(@RequestBody MerchantDto dto,
                                     @RequestParam(value = "isPage", required = false) Integer isPage,
                                     Pageable pageable) throws Exception {
        return merchantService.findAll(pageable, dto, isPage);
    }

    @GetMapping("/merchant/user")
    @Transactional
    public ResponseEntity<?> findByUserName(@RequestParam("userName") String userName) throws Exception {
        Merchant result = merchantService.findByUserName(userName, LanguageUtils.getCurrentLanguageId());
        if (result == null) {
            throw new CustomErrorException("User not exist");
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/merchants/register")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> register(@RequestBody MerchantDto dto) throws Exception {
        merchantService.register(dto);
        return Optional.ofNullable(dto)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }

    @PostMapping("/merchants/genActiveCode")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> genActiveCode(@RequestBody MerchantActiveCode dto) {
        String activeCode;
        if (dto != null) {
            activeCode = Utils.genRandomCode();
            while (merchantActiveCodeRepository.findByActiveCode(activeCode) != null) {
                activeCode = Utils.genRandomCode();
            }
            dto.setActiveCode(activeCode);
            merchantActiveCodeRepository.save(dto);
        }
        return Optional.ofNullable(dto)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }


    @PostMapping("/merchants/active")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<MerchantDto> active(@RequestBody MerchantDto dto) throws Exception {
        merchantService.active(dto);
        return Optional.ofNullable(dto)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping("/merchants/deactive")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<MerchantDto> deactive(@RequestBody MerchantDto dto) throws Exception {
        merchantService.deactive(dto);
        return Optional.ofNullable(dto)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/merchants/inputCode")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> inputCode(@RequestParam String code) {
        Merchant parentMerchant = merchantBaseService.findByActiveCode(code);
        if (parentMerchant != null && parentMerchant.getMerchantTypeId() == 1) {
            return new ResponseEntity<>(parentMerchant, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(parentMerchant, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/merchants/inputCodeIos")
    @Transactional(rollbackFor = Exception.class)
    public Page<Merchant> inputCodeIos(@RequestParam String code, Pageable pageable) {
        ArrayList<Merchant> arrMerchant = new ArrayList<>();
        Merchant parentMerchant = merchantBaseService.findByActiveCode(code);
        if (parentMerchant != null && parentMerchant.getMerchantTypeId() == 1) {
            arrMerchant.add(parentMerchant);
            return new PageImpl<>(arrMerchant, pageable, 1);
        } else {
            return new PageImpl<>(arrMerchant, pageable, 0);
        }

    }

    @GetMapping("/merchants/overview")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getOverview(@RequestParam Long parentMerchantId) {
        return Optional.of(overviewService.getOverview(parentMerchantId))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/merchants/{merchantId}/overview")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getOverviewByMerchantId(@PathVariable Long merchantId) throws Exception {
        return Optional.of(overviewService.getOverviewByMerchantId(merchantId))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/merchants/overviewL2")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getOverviewL2(Long merchantId, @RequestParam Long parentMerchantId) throws Exception {

        return Optional.of(overviewService.getOverviewL2ByMerchantId(parentMerchantId, merchantId))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/merchants/overviewL1")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getOverviewL1(@RequestParam @NonNull Long merchantId) throws Exception {

        return Optional.of(overviewService.getOverviewL1ByMerchantId(merchantId))
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    private ResponseEntity<?> getResponseEntity(HashMap<String, Object> param, String sql) throws Exception {
        List<OverviewMerchantDTO> registryDTOLst = commonService.findAll(null, OverviewMerchantDTO.class, sql, param);
        if (registryDTOLst != null && registryDTOLst.size() > 0) {
            return new ResponseEntity<>(
                    registryDTOLst.get(0),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/merchants/getOrderL2")
    @Transactional(rollbackFor = Exception.class)
    public Page<?> getListOrderByMerchantId(Pageable pageable,
                                            @RequestParam Long merchantId,
                                            @RequestParam Long parentMerchantId, @RequestParam Integer orderStatus) throws Exception {
        String sqlWhere = " where ORDER_STATUS =:orderStatus and merchant_id = :merchantId and PARENT_MERCHANT_ID = :parentMerchantId";
        String sqlOrder = " order by ORDER_DATE desc";
        HashMap<String, Object> params = new HashMap<>();
        params.put("merchantId", merchantId);
        params.put("parentMerchantId", parentMerchantId);
        params.put("orderStatus", orderStatus);
        String sql = " select order_id orderId ,ORDER_DATE orderDate,order_status orderStatus,amount from orders " +
                sqlWhere +
                sqlOrder;
        List<OrderDto> orderDtoList = commonService.findAll(pageable, OrderDto.class, sql, params);
        String sqlCount = "select count(*) from orders " +
                sqlWhere;
        int count = commonService.getRowCountV2(sqlCount, params);
        return new PageImpl<>(orderDtoList, pageable, count);
    }

    @PostMapping("/merchants/createMerchantL2")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> createMerchantL2(@RequestBody Merchant merchantDto,
                                              @RequestParam String code) throws Exception {
        Merchant parentMerchant = merchantBaseService.findByActiveCode(code);
        if (parentMerchant != null && merchantDto != null) {
            if (!StringUtils.isEmpty(merchantDto.getMobilePhone()) && merchantBaseService.findByMobilePhone(merchantDto.getMobilePhone()) != null) {
                throw new CustomErrorException("Mobile phone is exist");
            }
            // Tạo đại lý cấp 2
            merchantDto.setUserName(merchantDto.getMobilePhone());
            merchantDto.setParentMarchantId(parentMerchant.getMerchantId());
            merchantDto.setMerchantCode(merchantDto.getMobilePhone());
            merchantDto.setParentMerchant(parentMerchant);
            merchantDto.setMerchantTypeId(2L);
            merchantDto.setTax(10D);
            merchantDto.setStatus(1);
            merchantBaseService.save(merchantDto);
            // Tao wallet
            createWallet(merchantDto);
            // Lay token va tao tai khoan dang nhap
            createAccountForMerchantL2(merchantDto);
            // Push notification to device L1
            applicationEventPublisher.publishEvent(new PushNotificationRegisterEvent(this, parentMerchant.getMerchantId(), merchantDto));
            return Optional.of(merchantDto)
                    .map(result -> new ResponseEntity<>(
                            result,
                            HttpStatus.OK))
                    .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        } else {
            return new ResponseEntity<>(parentMerchant, HttpStatus.BAD_REQUEST);
        }

    }


    private void createWallet(@RequestBody Merchant merchantDto) {
        Wallet wallet = new Wallet();
        wallet.setMerchantId(merchantDto.getMerchantId());
        wallet.setMerchantCode(merchantDto.getMerchantCode());
        wallet.setAmount(0D);
        commonService.save(wallet);
    }

    @PostMapping("/merchants/registerChat/{merchantId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> registerChat(@PathVariable Long merchantId) throws Exception {
        Merchant merchant = commonService.find(Merchant.class, merchantId);
        if (merchant == null) {
            throw new CustomErrorException("MerchantIdNotExist");
        }
        return new ResponseEntity<>(merchant, HttpStatus.OK);
    }

    @PostMapping(path = "/merchants/l1.update", consumes = {"multipart/form-data"})
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> createL1(@RequestPart("body") Merchant merchantDto,
                                      @RequestPart(name = "file", required = false) MultipartFile file) throws Exception {
        Merchant merchant;
        if (merchantDto.getMerchantId() != null) {
            merchant = commonService.find(Merchant.class, merchantDto.getMerchantId());
            if (merchant == null) {
                throw new CustomErrorException("Merchant Not Exist");
            }
            Utils.copyNonNullProperties(merchantDto, merchant);
            // Tạo đại lý cấp 1
            String urlAvatar = "";
            if (file != null) {
                urlAvatar = fileService.saveImgAvatar(merchant.getMerchantId(), file);
                if (!StringUtils.isEmpty(urlAvatar)) {
                    merchant.setMerchantImgUrl(urlAvatar);
                }
            }
            merchantBaseService.save(merchant);
            merchantDto.setMerchantImgUrl(fileConfiguration.getUrlImage() + urlAvatar);
        } else {
            throw new CustomErrorException("MerchantId is not null");
        }
        return new ResponseEntity<>(merchantDto, HttpStatus.OK);
    }

    private void createAccountForMerchantL2(Merchant merchantDto) {
        List<Role> lstRoleL2 = new ArrayList<>();
        oAuthConfiguration.getMerchantRoles().forEach(roleId -> lstRoleL2.add(new Role(roleId)));
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName("");
        userEntity.setLastName(merchantDto.getFullName());
        userEntity.setUsername(merchantDto.getUserName());
        userEntity.setEnabled(true);
        userEntity.setRoles(lstRoleL2);
        accountService.createAccount(userEntity);
    }

    @PostMapping("/merchants/changePass")
    public ResponseEntity<?> changePassword(@RequestBody UserEntity userEntity, @RequestHeader(name = "Authorization") String token) {
        accountService.changePassword(userEntity, token.replaceAll("bearer", ""));
        return Optional.of(userEntity)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }


    @PostMapping("/merchants/update")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> updateMerchant(@RequestBody MerchantDto merchantDto) throws Exception {
        if (merchantDto != null) {
            Merchant copyMerchant;
            copyMerchant = merchantBaseService.get(merchantDto.getMerchantId());
            if (Objects.nonNull(copyMerchant)) {
                Utils.copyNonNullProperties(merchantDto, copyMerchant);
                if (copyMerchant.getIdentityNumber() != null && !StringUtils.isEmpty(copyMerchant.getIdentityNumber())) {
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("identityNumber", copyMerchant.getIdentityNumber());
                    params.put("merchantId", copyMerchant.getMerchantId());
                    int countCMT = commonService.getRowCountV2("select count(*) from merchant where identity_number = :identityNumber and merchant_id <> :merchantId", params);
                    if (countCMT > 0) {
                        throw new CustomErrorException("Identity number already exist ");
                    }
                }
                merchantBaseService.save(copyMerchant);
                // Luu dia chi giao hang
                if (copyMerchant.getUserName() != null && !StringUtils.isEmpty(copyMerchant.getUserName())) {
                    saveDeliveryAddress(merchantDto, copyMerchant);
                }
                // TODO: Update attribute
                if (copyMerchant.getMerchantId() != null && Utils.isNotEmpty(merchantDto.getMapAttValues())) {
                    // Save merchant attribute
                    List<MerchantAttributeExtent> lstAtt = commonService.findAll(null, MerchantAttributeExtent.class, "select * from merchant_attribute_extent where merchant_id= :merchantId", copyMerchant.getMerchantId());
                    if (Utils.isNotEmpty(lstAtt)) {
                        lstAtt.forEach(commonService::remove);
                    }
                    //
                    merchantDto.getMapAttValues().forEach(dto -> {
                        MerchantAttributeExtent extent = MerchantAttributeExtent.builder().attId(dto.getAttId())
                                .attValueId(dto.getAttValueId())
                                .merchantId(copyMerchant.getMerchantId())
                                .build();
                        commonService.save(extent);

                    });

                }
                // TODO: Update merchant group
                if (copyMerchant.getMerchantId() != null && merchantDto.getGroupValues() != null && merchantDto.getGroupValues().size() > 0) {
                    // Save merchant group
                    List<MerchantGroupMerchant> lstGroup = commonService.findAll(null, MerchantGroupMerchant.class, "select * from merchant_group_merchant where merchant_id= :merchantId", copyMerchant.getMerchantId());
                    if (Utils.isNotEmpty(lstGroup)) {
                        lstGroup.forEach(commonService::remove);
                    }
                    //
                    merchantDto.getGroupValues().forEach(merchantGR -> {
                        MerchantGroupMerchant groupMerchant = MerchantGroupMerchant.builder().merchantId(copyMerchant.getMerchantId())
                                .merchantGroupId(merchantGR.getId())
                                .build();
                        commonService.save(groupMerchant);
                    });

                }
                return Optional.of(copyMerchant)
                        .map(result -> new ResponseEntity<>(
                                result,
                                HttpStatus.OK))
                        .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
            }
        }
        throw new CustomErrorException("Merchant is not exist");

    }


    @PostMapping("/merchants/update-info")
    @Transactional
    public ResponseEntity<?> updateMerchantInfo(@RequestBody MerchantDto merchantRequest) {
        return new ResponseEntity<>(merchantService.updateMerchantInfo(merchantRequest), HttpStatus.OK);
    }

    private void saveDeliveryAddress(MerchantDto merchantDto, Merchant copyMerchant) {
        if (!StringUtils.isEmpty(merchantDto.getAddress()) && merchantDto.getMerchantId() != null) {
            if (merchantAddressService.countByMerchantId(copyMerchant.getMerchantId()) == 0) {
                MerchantAddress merchantAddressSave = new MerchantAddress();
                merchantAddressSave.setMerchantId(merchantDto.getMerchantId());
                merchantAddressSave.setIsSelect(1);
                merchantAddressSave.setDistrict(merchantDto.getDistrict());
                merchantAddressSave.setProvince(merchantDto.getProvince());
                if (copyMerchant.getMobilePhone() != null && !StringUtils.isEmpty(copyMerchant.getMobilePhone())) {
                    merchantAddressSave.setPhoneNumber(copyMerchant.getMobilePhone());
                }
                if (copyMerchant.getFullName() != null && !StringUtils.isEmpty(copyMerchant.getFullName())) {
                    merchantAddressSave.setFullName(copyMerchant.getFullName());
                }
                merchantAddressSave.setAddress(merchantDto.getAddress());
                merchantAddressService.save(merchantAddressSave);
            }

        }
    }


    @PostMapping("/merchant/list")
    public ResponseEntity<?> listMerchant(Pageable pageable, String exceptIds, String text) throws Exception {

        if (Utils.isEmpty(exceptIds)) {
            throw new CustomErrorException("exceptIdsIsNull");
        }
        List<Long> list = Arrays.stream(exceptIds.split(",")).map(Long::valueOf).collect(Collectors.toList());

        return ResponseEntity.ok(merchantService.listMerchant(pageable, list, text));

    }

    @PostMapping("/merchants/create/L1")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> createL1(@Valid @RequestBody Merchant merchantDto) {
        createInfoL1(merchantDto);
        return new ResponseEntity<>(merchantDto, HttpStatus.OK);
    }


    @PostMapping("/merchants/update/token")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> updateToken(@RequestBody TokenFbMerchant tokenFbMerchant) {
        if (tokenFbMerchant.getMerchantId() == null || StringUtils.isEmpty(tokenFbMerchant.getToken())) {
            throw new CustomErrorException("MerchantId & Token is not null");
        }
        if (!tokenFbMerchant.getIsDelete()) {
            HashMap<String, Object> param = new HashMap<>();
            param.put("merchantId", tokenFbMerchant.getMerchantId());
            param.put("token", tokenFbMerchant.getToken());
            int count = commonService.getRowCountV2("select count(*) from token_fb_merchant where merchant_id = :merchantId and token = :token", param);
            if (count > 0) {
                return new ResponseEntity<>(tokenFbMerchant, HttpStatus.OK);
            } else {
                param.put("imei", tokenFbMerchant.getImei());
                if (commonService.getRowCountV2("select count(*) from token_fb_merchant where merchant_id = :merchantId and token = :token and imei = :imei", param) > 0) {
                    return new ResponseEntity<>(tokenFbMerchant, HttpStatus.OK);
                }
                param.remove("token");
                if (commonService.getRowCountV2("select count(*) from token_fb_merchant where merchant_id = :merchantId and imei = :imei", param) > 0) {
                    //Update
                    HashMap<String, Object> paramUpdate = new HashMap<>();
                    paramUpdate.put("token", tokenFbMerchant.getToken());
                    paramUpdate.put("merchantId", tokenFbMerchant.getMerchantId());
                    paramUpdate.put("imei", tokenFbMerchant.getImei());
                    commonService.executeSql("update token_fb_merchant set token = :token where merchant_id = :merchantId and imei = :imei", paramUpdate);
                    return new ResponseEntity<>(tokenFbMerchant, HttpStatus.OK);
                }
            }

            tokenFbMerchant = commonService.save(tokenFbMerchant);
        } else {
            merchantService.deleteToken(tokenFbMerchant.getMerchantId(), tokenFbMerchant.getToken());
        }
        return new ResponseEntity<>(tokenFbMerchant, HttpStatus.OK);
    }

    @PostMapping(path = "/merchant/create/info", consumes = {"multipart/form-data"})
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> createMerchant(@Valid @RequestPart("merchant") Merchant merchant,
                                            @RequestPart(name = "files") MultipartFile[] files) throws Exception {
        //
        createInfoL1(merchant);
        //
        ArrayList<String> arrFilesName = new ArrayList<>();
        if (merchant.getMerchantId() != null) {
            arrFilesName = fileService.saveFileIdentity(merchant.getMerchantId(), files);
        }
        if (arrFilesName != null && arrFilesName.size() == 2) {
            merchant.setIdentityImgFront(arrFilesName.get(0));
            merchant.setIdentityImgBack(arrFilesName.get(1));
        }


        // Create account payment
        AccountPayment accountPayment = new AccountPayment();
        accountPayment.setMerchantId(merchant.getMerchantId());
        accountPayment.setName(merchant.getUserName());

//        HttpHeaders headers = new HttpHeaders();
//        headers.setBasicAuth("mifos", "password");
//
//        ResponseEntity<String> response = walletRestTemplate.exchange(
//                "https://localhost:41019/payment-gateway/api/v1/accounts",
//                HttpMethod.POST,
//                new HttpEntity<>(accountPayment, headers),
//                String.class);
//
//        if (!response.getStatusCode().is2xxSuccessful()) {
//            throw new CustomErrorException(response.getStatusCodeValue() + "");
//        }

        return Optional.of(merchant)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping(path = "/merchant/image")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> updateMerchantImage(Long merchantId, Integer type, @RequestParam(required = false) Long[] idsDelete,
                                                 @RequestPart(name = "files", required = false) MultipartFile[] arrFile) throws Exception {
        if (merchantId == null) {
            throw new CustomErrorException("MerchantId is not null");
        }
        if (type == null) {
            throw new CustomErrorException("Type is not null");
        }

        ArrayList<MerchantImages> arrImgContracts = null;
        if (arrFile != null && arrFile.length > 0) {
            arrImgContracts = fileService.saveImgContract(merchantId, type, arrFile);
        }
        //
        if (arrImgContracts != null && arrImgContracts.size() > 0) {
            for (MerchantImages images : arrImgContracts) {
                commonService.save(images);
            }
        }

        if (idsDelete != null && idsDelete.length > 0) {
            for (Long aLong : idsDelete) {
                imageRepository.deleteById(aLong);
            }
        }
        return Optional.of(BaseResponse.builder().code(HttpStatus.OK.value())
                .message("Upload image success")
                .build())
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PostMapping("/merchant/save/store")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> updateMerchantStore(@RequestParam Long merchantId, @Valid @RequestBody List<MerchantWareHouse> stores) throws Exception {
        if (merchantId == null) {
            throw new CustomErrorException("MerchantId is not null");
        }
        Merchant merchant = commonService.find(Merchant.class, merchantId);
        if (merchant == null) {
            throw new CustomErrorException("Merchant not exist");
        }
        // Xoa ban ghi cu?
        String sqlCheck = "select * from merchant_warehouse where merchant_id = :merchantId";
        List<MerchantWareHouse> lstRegistered = commonService.findAll(null, MerchantWareHouse.class, sqlCheck, merchantId);
        if (lstRegistered != null && lstRegistered.size() > 0) {
            for (MerchantWareHouse store : lstRegistered) {
                commonService.remove(store);
            }
        }
        for (MerchantWareHouse wareHouse : stores) {
            wareHouse.setMerchantId(merchantId);
            commonService.save(wareHouse);
        }
        return new ResponseEntity<>(BaseResponse.builder().code(HttpStatus.OK.value()).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping("/merchant/get-warehouse")
    @Transactional(rollbackFor = Exception.class)
    public Page<MerchantDto> getMerchantOfWareHouse(Pageable pageable, @RequestParam String storeCode, @RequestParam(required = false) String keyword, @RequestParam(required = false) String registerFromDate, @RequestParam(required = false) String registerToDate) throws Exception {
        StringBuilder sqlBuilder = new StringBuilder();
        HashMap<String, Object> params = new HashMap<>();
        sqlBuilder.append("select m.* from merchant m join merchant_warehouse w on m.MERCHANT_ID = w.MERCHANT_ID AND w.store_code = :storeCode ");
        params.put("storeCode", storeCode);
        sqlBuilder.append("where 1=1 ");
        if (!StringUtils.isEmpty(keyword)) {
            sqlBuilder.append(" AND (LOWER(m.full_name) like  concat('%', :keyWord ,'%') OR LOWER(m.user_name) like concat('%', :keyWord ,'%') ");
            sqlBuilder.append(" OR LOWER(m.merchant_name) like concat('%', :keyWord ,'%') OR LOWER(m.merchant_code) like concat('%', :keyWord ,'%'))  ");
            params.put("keyWord", keyword.toLowerCase());
        }
        //
        DateValidatorUsingDateFormat.validDate(registerFromDate, registerToDate);
        if (!StringUtils.isEmpty(registerFromDate) && !StringUtils.isEmpty(registerToDate)) {
            sqlBuilder.append(" AND DATE(m.CREATE_DATE) between DATE(' ").append(registerFromDate).append(" ') and DATE(' ").append(registerToDate).append(" ') ");
        } else if (!StringUtils.isEmpty(registerFromDate) && StringUtils.isEmpty(registerToDate)) {
            sqlBuilder.append(" AND DATE(m.CREATE_DATE) >= date(' ").append(registerFromDate).append(" ')");
        } else if (StringUtils.isEmpty(registerFromDate) && !StringUtils.isEmpty(registerToDate)) {
            sqlBuilder.append(" AND DATE(m.CREATE_DATE) <= date(' ").append(registerToDate).append(" ')");
        }
        int countRow = commonService.getRowCountV2("select count(*) from (" + sqlBuilder.toString() + ")x ", params);
        return new PageImpl<>(commonService.findAll(pageable, MerchantDto.class, sqlBuilder.toString(), params), pageable, countRow);

    }

    @GetMapping("/merchants/search")
    @Transactional(rollbackFor = Exception.class)
    public Page<?> searchMerchant(Pageable pageable, @RequestParam(required = false) Long parentMerchantId
            , @RequestParam(required = false) Integer status
            , @RequestParam String keyword
            , @RequestParam(required = false) String fromDate
            , @RequestParam(required = false) String toDate
            , @RequestParam(required = false) Integer noRevenue
            , @RequestParam(required = false) Boolean confirmKpi) throws Exception {
        HashMap<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder();
        sql.append(" select * from merchant where 1=1 ");
        if (parentMerchantId != null) {
            sql.append(" AND PARENT_MARCHANT_ID = :parentMerchantId ");
            params.put("parentMerchantId", parentMerchantId);
        }
        if (status != null) {
            if (!StringUtils.isEmpty(fromDate) && !StringUtils.isEmpty(toDate)) {
                if (status == 1) {
                    sql.append(" AND status = :status ");
                    params.put("status", status);
                    if (confirmKpi != null) {
                        sql.append(" AND ").append(" confirm_kpi = :confirmKpi ");
                        params.put("confirmKpi", confirmKpi);

                    } else {
                        throw new CustomErrorException("confirmKpi not null when status = 1");
                    }
                    sql.append(" AND ").append(SpringUtils.whereDate(fromDate, toDate, "active_date"));
                } else {
                    sql.append(" AND ((status = 0 and action = 3 AND ")
                            .append(SpringUtils.whereDate(fromDate, toDate, "create_date"))
                            .append(" ) or ( STATUS = 2  AND ")
                            .append(SpringUtils.whereDate(fromDate, toDate, "deactive_date"))
                            .append(") )");
                }
                //
                if (noRevenue != null && noRevenue == 1) {
                    sql.append(" and merchant_id not in (select distinct(merchant_id) from orders where order_status = 2 and ")
                            .append(SpringUtils.whereDate(fromDate, toDate, "order_date")).append(")");
                }
            } else {
                if (status == 1) {
                    sql.append(" AND status = :status ");
                    params.put("status", status);
                    if (confirmKpi != null) {
                        sql.append(" AND ").append(" confirm_kpi = :confirmKpi ");
                        params.put("confirmKpi", confirmKpi);

                    } else {
                        throw new CustomErrorException("confirmKpi not null when status = 1");
                    }
                } else {
                    sql.append(" AND ((status = 0 and action = 3 ) ")
                            .append(" or ( STATUS = 2 ))");
                }
            }
        }

        sql.append(" AND( LOWER(full_name) like concat('%', :keyword ,'%') ");
        sql.append(" OR LOWER(user_name) like concat('%', :keyword ,'%') ");
        sql.append(" OR LOWER(address) like concat('%', :keyword ,'%') )");
        // Order by
        sql.append("order by create_date desc");

        params.put("keyword", keyword);


        //
        String sqlRun = sql.toString();
        if (status != null && status == 1 && noRevenue != null && noRevenue == 1) {
            sqlRun = sql.toString().replace(" AND " + SpringUtils.whereDate(fromDate, toDate, "active_date"), "");
        }
        int count = commonService.getRowCountV2(" select count(*) from (" + sqlRun + ")x ", params);
        List<MerchantDto> lstMerchant = commonService.findAll(pageable, MerchantDto.class, sqlRun, params);
        if (Utils.isNotNull(lstMerchant)) {
            lstMerchant.forEach(merchantDto -> {
                merchantDto.setMerchantImgUrl(fileConfiguration.getUrlImage() + merchantDto.getMerchantImgUrl());
            });
        }
        return new PageImpl<>(commonService.findAll(pageable, MerchantDto.class, sqlRun, params), pageable, count);
    }

    @GetMapping("/merchants/data-map")
    @Transactional(rollbackFor = Exception.class)
    public Page<?> getDataOnMap(Pageable pageable, Integer action, Long parentMerchantId, Integer status, boolean isPageable) throws Exception {
        return merchantService.getDataOnMap(pageable, parentMerchantId, status, action, isPageable);
    }

    @PostMapping("/merchants/update-status")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> updateStatus(@Valid @RequestBody MerchantUpdateStatusDTO merchantStatus) {
        Merchant merchant = commonService.find(Merchant.class, merchantStatus.getMerchantId());
        if (merchant != null) {
            if (!merchant.getParentMarchantId().equals(merchantStatus.getParentMarchantId())) {
                throw new CustomErrorException("Access denied for user");
            }
            if (merchantStatus.getStatus() == 1) {
                // yeu cau hoat dong
                updateAction(merchantStatus.getMerchantId(), 1, merchantStatus.getLockStartDate(), merchantStatus.getLockEndDate());
            } else if (merchantStatus.getStatus() == 2) {
                if (StringUtils.isEmpty(merchantStatus.getLockStartDate())) {
                    throw new CustomErrorException("LockStartDate is not null");
                }
                // Yeu cau tam ngung
                updateAction(merchantStatus.getMerchantId(), 2, merchantStatus.getLockStartDate(), merchantStatus.getLockEndDate());

            }
        } else {
            throw new CustomErrorException("Merchant not exist");
        }
        return new ResponseEntity<>(BaseResponse.builder().code(HttpStatus.OK.value()).message("Require success").build(), HttpStatus.OK);
    }

    @GetMapping("/merchants/getAttribute")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> getAttribute(Long merchantId, @RequestHeader(name = "Accept-Language") Long langId) throws Exception {
        return new ResponseEntity<>(merchantService.getAttributeOfMerchant(merchantId, langId), HttpStatus.OK);
    }

    @PostMapping("merchants/resetPassword")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> resetPassword(@RequestBody MerchantDto userName) throws Exception {
        return accountService.resetPassword(userName);
    }


    private void updateAction(Long merchantId, Integer action, LocalDateTime fromDate, LocalDateTime toDate) {
        HashMap<String, Object> params = new HashMap<>();
        if (!StringUtils.isEmpty(fromDate) && StringUtils.isEmpty(toDate)) {

            params.put("action", action);
            params.put("lockStartDate", fromDate);
            params.put("merchantId", merchantId);

            commonService.executeSql("update merchant set action = :action, lock_start_date =:lockStartDate, lock_end_date = null where merchant_id = :merchantId", params);


        } else if (!StringUtils.isEmpty(fromDate) && !StringUtils.isEmpty(toDate)) {

            params.put("action", action);
            params.put("lockStartDate", fromDate);
            params.put("lockEndDate", toDate);
            params.put("merchantId", merchantId);

            commonService.executeSql("update merchant set action = :action, lock_start_date =:lockStartDate, lock_end_date =:lockEndDate where merchant_id = :merchantId", params);
        }
    }

    private void createInfoL1(Merchant merchantRequest) {
        if (merchantRequest.getMerchantId() != null) {
            Merchant merchant = commonService.find(Merchant.class, merchantRequest.getMerchantId());
            if (merchant == null) {
                throw new CustomErrorException("MerchantId Not Exist");
            }
        } else if (merchantRequest.getMobilePhone() != null) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("mobile", merchantRequest.getMobilePhone());
            int count = commonService.getRowCountV2("select count(*) from Merchant where MOBILE_PHONE =:mobile", params);
            if (count > 0) {
                throw new CustomErrorException("Mobile phone already exist ");
            }
            params.clear();
            params.put("identityNumber", merchantRequest.getIdentityNumber());
            int countCMT = commonService.getRowCountV2("select count(*) from Merchant where identity_number =:identityNumber", params);
            if (countCMT > 0) {
                throw new CustomErrorException("Identity number already exist ");
            }
        }
        // Tạo đại lý cấp 1
        AutoIncrement autoIncrement = autoIncrementJpaRepository.findByCode(AutoIncrementType.L1.value());
        Long code = Long.parseLong(autoIncrement.getValue()) + 1;
        merchantRequest.setMerchantCode(code.toString());
        merchantRequest.setMerchantTypeId(1L);
        // TODO : Fix cung VAT
        merchantRequest.setTax(10D);
        //TODO : Duyet thu cong
        merchantRequest.setStatus(0);
        merchantRequest.setAction(3);
        // Gen Active code
        String activeCode = Utils.genRandomCode();
        while (merchantActiveCodeRepository.findByActiveCode(activeCode) != null) {
            activeCode = Utils.genRandomCode();
        }
        merchantRequest.setActiveCode(activeCode);
        autoIncrement.setValue(code.toString());
        merchantRequest.setConfirmKpi(false);
        merchantBaseService.save(merchantRequest);
        autoIncrementJpaRepository.save(autoIncrement);
        // Tao wallet
        // taoWallet(merchantDto);
    }
}
