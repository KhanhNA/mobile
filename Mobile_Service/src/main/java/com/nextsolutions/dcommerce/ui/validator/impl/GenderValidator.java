package com.nextsolutions.dcommerce.ui.validator.impl;

import com.nextsolutions.dcommerce.ui.validator.Gender;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GenderValidator implements ConstraintValidator<Gender, Integer> {

   private Gender gender;

   public void initialize(Gender gender) {
      this.gender = gender;
   }

   public boolean isValid(Integer gender, ConstraintValidatorContext context) {
      if(this.gender.required()) {
         if(gender == null) {
            return false;
         } else {
            switch (gender) {
               case -1:
               case 0:
               case 1:
                  return true;
               default:
                  return false;
            }
         }
      }
      return true;
   }
}
