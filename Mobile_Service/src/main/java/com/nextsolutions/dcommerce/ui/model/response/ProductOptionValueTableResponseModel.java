package com.nextsolutions.dcommerce.ui.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductOptionValueTableResponseModel {
    private Long id;
    private Long productOptionId;
    private String productOptionCode;
    private String code;
    private String name;
}
