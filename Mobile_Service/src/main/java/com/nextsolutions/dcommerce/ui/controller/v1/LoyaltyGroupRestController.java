package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingGroup;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingGroupPacking;
import com.nextsolutions.dcommerce.service.LoyaltyPackingGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1 + "/loyalty")
public class LoyaltyGroupRestController {

    @Autowired
    private LoyaltyPackingGroupService loyaltyPackingGroupService;


    @PostMapping("/pg")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> insertPackingGroup(@RequestBody LoyaltyPackingGroup dto) throws Exception {

        loyaltyPackingGroupService.insertPackingGroup(dto);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("/pgd/{pgId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> savePackingGroupDetail(@RequestBody List<LoyaltyPackingGroupPacking> dtos,
                                                    @PathVariable Long pgId) throws Exception {
        loyaltyPackingGroupService.savePackingGroupDetail(dtos, pgId);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/pgd/del/{loyaltyPgdId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delPackingGrpPacking(@PathVariable Long loyaltyPgdId) throws Exception {
        loyaltyPackingGroupService.delPackingGrpPacking(loyaltyPgdId);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/pg/del/{loyaltyPgId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delPackingGrp(@PathVariable Long loyaltyPgId) throws Exception {
        loyaltyPackingGroupService.delPackingGrp(loyaltyPgId);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/packingForGroup/list")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> list(Pageable pageable, Long ictPackingGroupId) throws Exception {
        ;
        return ResponseEntity.ok(loyaltyPackingGroupService.list(pageable, ictPackingGroupId));

    }


}
