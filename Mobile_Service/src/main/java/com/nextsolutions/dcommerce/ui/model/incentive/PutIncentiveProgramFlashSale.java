package com.nextsolutions.dcommerce.ui.model.incentive;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
public class PutIncentiveProgramFlashSale {
    @NotNull
    private Long packingId;

    @NotNull
    private Integer discountType;

    private BigDecimal discountPrice;

    private BigDecimal discountPercent;

    private BigDecimal limitedQuantity;

    private Boolean isFlashSale;

    @Valid
    private List<PutIncentiveProgramFlashSaleDate> flashSaleDates;
}
