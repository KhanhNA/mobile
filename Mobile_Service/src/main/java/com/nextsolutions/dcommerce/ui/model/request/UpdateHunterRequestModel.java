package com.nextsolutions.dcommerce.ui.model.request;

import com.nextsolutions.dcommerce.ui.validator.Gender;
import com.nextsolutions.dcommerce.ui.validator.Phone;
import com.nextsolutions.dcommerce.ui.validator.Username;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateHunterRequestModel {

    @NotNull
    @Phone(pattern = "^[0-9]{9,13}$")
    private String phoneNumber;

    private String username;

    private String fullName;

    private Integer status;

    @Email
    @Size(max = 120)
    private String email;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime birthDate;

    @Gender(required = true)
    private Integer gender;

    @Size(max = 255)
    private String address;

}
