package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.repository.ManufacturerRepository;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerSearchProjection;
import com.nextsolutions.dcommerce.ui.controller.ApiConstant;
import com.nextsolutions.dcommerce.utils.LanguageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("manufacturerControllerV1")
@RequestMapping(ApiConstant.API_V1)
@RequiredArgsConstructor
public class ManufacturerController {

    private final ManufacturerRepository manufacturerRepository;

    @GetMapping("/manufacturers/search")
    public ResponseEntity<Page<ManufacturerSearchProjection>> searchManufacturers(
            @RequestParam(name = "keyword", required = false, defaultValue = "") String keyword,
            Pageable pageable
    ) {
        Page<ManufacturerSearchProjection> searchProjectionPage = manufacturerRepository.searchManufacturer(keyword, LanguageUtils.getCurrentLanguageId(), pageable);
        return ResponseEntity.ok(searchProjectionPage);
    }
}
