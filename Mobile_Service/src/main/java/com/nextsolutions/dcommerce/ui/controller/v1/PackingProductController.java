package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.PackingProductService;
import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class PackingProductController {

    private final PackingProductService packingProductService;

    @GetMapping(value = "/packing-products/search", params = {"keyword", "incentiveProgramId"})
    public ResponseEntity<Page<PackingProductSearchResult>> searchPackingProductByIncentive(
            @RequestParam String keyword, @RequestParam Long incentiveProgramId, Pageable pageable) {
        return ResponseEntity.ok(packingProductService.searchPackingProduct(keyword, incentiveProgramId, pageable));
    }

    @GetMapping(value = "/packing-products/search", params = {"keyword"})
    public ResponseEntity<Page<PackingProductSearchResult>> searchPackingProductByKeyword(
            @RequestParam String keyword, Pageable pageable) {
        return ResponseEntity.ok(packingProductService.searchPackingProductByKeyWord(keyword, pageable));
    }

    @GetMapping(value = "/packing-products/search", params = {"categoryId", "incentiveProgramId"})
    public ResponseEntity<List<PackingProductSearchResult>> searchPackingProductByCategoryId(
            @RequestParam Long categoryId, @RequestParam Long incentiveProgramId
    ) {
        return ResponseEntity.ok(packingProductService.searchPackingProductByCategoryId(categoryId, incentiveProgramId));
    }

    @GetMapping(value = "/packing-products/search", params = {"manufacturerId", "incentiveProgramId"})
    public ResponseEntity<List<PackingProductSearchResult>> searchPackingProductByManufacturerId(
            @RequestParam Long manufacturerId, @RequestParam Long incentiveProgramId) {
        return ResponseEntity.ok(packingProductService.searchPackingProductByManufacturerId(manufacturerId, incentiveProgramId));
    }
}
