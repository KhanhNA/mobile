package com.nextsolutions.dcommerce.ui.model.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetProductLinksResponseModel {
    private Long manufacturerId;
    private Long categoryId;
    private Long distributorId;
    private List<Long> relatedProductIds;

    private GetProductLinksManufacturerResponseModel manufacturer;
    private GetProductLinksCategoryResponseModel category;
    private GetProductLinksDistributorResponseModel distributor;
    private List<GetProductLinksRelatedProductResponseModel> relatedProducts;
}
