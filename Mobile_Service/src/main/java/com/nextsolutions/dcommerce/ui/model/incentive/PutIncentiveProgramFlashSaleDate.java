package com.nextsolutions.dcommerce.ui.model.incentive;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class PutIncentiveProgramFlashSaleDate {
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime date;
    private boolean zeroToNine;
    private boolean nineToTwelve;
    private boolean twelveToFifteen;
    private boolean fifteenToEighteen;
    private boolean eighteenToTwentyOne;
    private boolean twentyOneToTwentyFour;
}
