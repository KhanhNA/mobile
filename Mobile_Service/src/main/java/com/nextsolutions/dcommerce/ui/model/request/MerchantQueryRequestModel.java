package com.nextsolutions.dcommerce.ui.model.request;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class MerchantQueryRequestModel {
    private String code;
    private String username;
    private String fullName;
    private String phoneNumber;
    private String address;
    private Integer gender;
    private Integer status;
    private String email;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime birthDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime registerDate;
}
