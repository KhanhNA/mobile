package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.service.ProductManagementService;
import com.nextsolutions.dcommerce.shared.dto.ProductDataDto;
import com.nextsolutions.dcommerce.shared.dto.ProductGeneralDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductLinksDistributorDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductLinksDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductLinksManufacturerDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductOptionsDto;
import com.nextsolutions.dcommerce.shared.dto.product.packing.PackingProductDto;
import com.nextsolutions.dcommerce.shared.mapper.ProductManagementMapper;
import com.nextsolutions.dcommerce.ui.model.product.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class ProductManagementController {

    private final ProductManagementService productManagementService;
    private final ProductManagementMapper productManagementMapper;

    @PostMapping("/products")
    public ResponseEntity<PostProductGeneralResponseModel> createProductGeneral(
            @Valid @RequestBody PostProductGeneralRequestModel requestModel
    ) {
        ProductGeneralDto productGeneralDto = productManagementMapper.toProductGeneralDto(requestModel);
        ProductGeneralDto generalDto = productManagementService.createProduct(productGeneralDto);
        PostProductGeneralResponseModel responseModel = productManagementMapper
                .toPostProductGeneralResponseModel(generalDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseModel);
    }

    @GetMapping("/products/{id}/general")
    public ResponseEntity<GetProductGeneralResponseModel> getProductGeneral(@PathVariable Long id) {
        ProductGeneralDto generalDto = productManagementService.getProductGeneral(id);
        GetProductGeneralResponseModel responseModel = productManagementMapper
                .toGetProductGeneralResponseModel(generalDto);
        return ResponseEntity.status(HttpStatus.OK).body(responseModel);
    }

    @PutMapping("/products/{id}/general")
    public ResponseEntity<PutProductGeneralResponseModel> updateProductGeneral(
            @PathVariable Long id,
            @Valid @RequestBody PutProductGeneralRequestModel requestModel
    ) {
        ProductGeneralDto productGeneralDto = productManagementMapper.toProductGeneralDto(requestModel);
        ProductGeneralDto updatedProductGeneralDto = productManagementService
                .updateProductGeneral(id, productGeneralDto);
        PutProductGeneralResponseModel responseModel = productManagementMapper
                .toPutProductGeneralResponseModel(updatedProductGeneralDto);
        return ResponseEntity.status(HttpStatus.OK).body(responseModel);
    }

    @GetMapping("/products/{id}/data")
    public ResponseEntity<GetProductDataResponseModel> getProductData(@PathVariable Long id) {
        ProductDataDto productDataDto = productManagementService.getProductData(id);
        GetProductDataResponseModel responseModel = productManagementMapper
                .toGetProductDataResponseModel(productDataDto);
        return ResponseEntity.ok(responseModel);
    }

    @PutMapping("/products/{id}/data")
    public ResponseEntity<PutProductDataResponseModel> updateProductData(
            @PathVariable Long id,
            @Valid @RequestBody PutProductDataRequestModel requestModel
    ) {
        ProductDataDto productDataDto = productManagementMapper.toProductDataDto(requestModel);
        ProductDataDto updatedProductDataDto = productManagementService.updateProductData(id, productDataDto);
        PutProductDataResponseModel responseModel = productManagementMapper
                .toPutProductDataResponseModel(updatedProductDataDto);
        return ResponseEntity.ok(responseModel);
    }

    @GetMapping("/products/{id}/links")
    public ResponseEntity<GetProductLinksResponseModel> getProductLinks(@PathVariable Long id) {
        ProductLinksDto productLinksDto = productManagementService.getProductLinksDto(id);
        GetProductLinksResponseModel responseModel = productManagementMapper
                .toGetProductLinksResponseModel(productLinksDto);
        return ResponseEntity.ok(responseModel);
    }

    @PutMapping("/products/{id}/links")
    public ResponseEntity<PutProductLinksResponseModel> updateProductLinks(
            @PathVariable Long id,
            @Valid @RequestBody PutProductLinksRequestModel requestModel
    ) {
        ProductLinksDto productLinksDto = productManagementMapper.toProductLinksDto(requestModel);
        ProductLinksDto updatedProductLinksDto = productManagementService.updateProductLinks(id, productLinksDto);
        PutProductLinksResponseModel responseModel = productManagementMapper
                .toPutProductLinksResponseModel(updatedProductLinksDto);
        return ResponseEntity.ok(responseModel);
    }


    @GetMapping("/products/links/manufacturers")
    public ResponseEntity<Page<GetProductLinksManufacturerResponseModel>> getManufacturerResponseModel(
            @RequestParam(name = "keyword", required = false, defaultValue = "") String keyword,
            Pageable pageable
    ) {
        Page<ProductLinksManufacturerDto> manufacturerDtoPage = productManagementService
                .getProductLinksManufacturers(keyword, pageable);
        return ResponseEntity.ok(manufacturerDtoPage
                .map(productManagementMapper::toGetProductLinksManufacturerResponseModel));
    }

    @GetMapping("/products/links/distributors")
    public ResponseEntity<Page<GetProductLinksDistributorResponseModel>> getDistributorResponseModel(
            @RequestParam(name = "keyword", required = false, defaultValue = "") String keyword,
            Pageable pageable
    ) {
        Page<ProductLinksDistributorDto> distributorDtoPage = productManagementService
                .getProductLinksDistributors(keyword, pageable);
        return ResponseEntity.ok(distributorDtoPage
                .map(productManagementMapper::toGetProductLinksDistributorResponseModel));
    }

    @GetMapping("/products/{id}/options")
    public ResponseEntity<Map<Long, GetProductOptionsResponseModel>> getProductOptions(@PathVariable Long id) {
        List<ProductOptionsDto> productOptions = productManagementService.getProductOptions(id);
        return ResponseEntity.ok(productOptions.stream().collect(Collectors.toMap(ProductOptionsDto::getAttributeId,
                productManagementMapper::toGetProductOptionsResponseModel)));
    }

    @PutMapping("/products/{id}/options")
    public ResponseEntity<Void> updateProductOptions(
            @PathVariable Long id,
            @RequestBody List<PutProductOptionRequestModel> requestModels
    ) {
        List<ProductOptionsDto> productOptionsDtos = requestModels.stream()
                .map(productManagementMapper::toProductOptionsDto)
                .collect(Collectors.toList());
        productManagementService.updateProductOptions(id, productOptionsDtos);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/products/{id}/packings")
    public ResponseEntity<Page<GetPackingProductResponseModel>> getPackings(@PathVariable Long id, Pageable pageable) {
        Page<PackingProductDto> packingProductPage = productManagementService.getPackingProducts(id, pageable);
        return ResponseEntity.ok(packingProductPage.map(productManagementMapper::toGetPackingProductResponseModel));
    }

}
