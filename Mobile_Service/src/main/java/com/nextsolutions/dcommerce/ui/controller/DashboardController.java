package com.nextsolutions.dcommerce.ui.controller;

import com.nextsolutions.dcommerce.service.KpiMonthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/dashboards")
@RequiredArgsConstructor
public class DashboardController {

	@Autowired
	private KpiMonthService kpiMonthService;

	@GetMapping("/hoa-hong-phat-trien-doanh-so/tra-thuong-phan-tram-tong-doanh-so-trong-thang")
	public ResponseEntity<Object> hhptdsBonusPercentTotalAmountInMonth() {
		return this.kpiMonthService.hhptdsBonusPercentTotalAmountInMonth();
	}

	@GetMapping("/hoa-hong-phat-trien-kenh-ban/l2-active")
	public ResponseEntity<Object> hhptkbL2Active() {
		return this.kpiMonthService.hhptkbL2Active();
	}
}
