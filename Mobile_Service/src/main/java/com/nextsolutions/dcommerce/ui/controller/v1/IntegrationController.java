package com.nextsolutions.dcommerce.ui.controller.v1;

import com.nextsolutions.dcommerce.model.Role;
import com.nextsolutions.dcommerce.model.UserEntity;
import com.nextsolutions.dcommerce.service.AccountService;
import com.nextsolutions.dcommerce.service.MerchantCrudService;
import com.nextsolutions.dcommerce.service.PaymentGatewayService;
import com.nextsolutions.dcommerce.service.chat.ChatService;
import com.nextsolutions.dcommerce.shared.dto.chat.ChatAccountDto;
import com.nextsolutions.dcommerce.shared.dto.wallet.ClientType;
import com.nextsolutions.dcommerce.shared.dto.wallet.SavingsProductType;
import com.nextsolutions.dcommerce.shared.dto.wallet.WalletAccountRequestBuilder;
import com.nextsolutions.dcommerce.ui.model.product.MerchantAccountRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.ChatAccountRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.WalletAccountRequestModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.nextsolutions.dcommerce.ui.controller.ApiConstant.API_V1;

@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class IntegrationController {

    private final AccountService accountService;
    private final PaymentGatewayService paymentGatewayService;
    private final ChatService chatService;
    private final MerchantCrudService merchantCrudService;

    @GetMapping("/integration/oauth/accounts")
    public ResponseEntity<Map<String, Object>> existsLoginAccount(@RequestParam String username) {
        UserEntity user = accountService.findByUsername(username);
        Map<String, Object> result = new LinkedHashMap<>();
        if (user != null) {
            result.put("exists", true);
            result.put("enabled", user.getEnabled());
        } else {
            result.put("exists", false);
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/integration/wallet/accounts")
    public ResponseEntity<Map<String, Boolean>> existsWalletAccount(@RequestParam Long walletId) {
        return ResponseEntity.ok(Collections.singletonMap("exists", paymentGatewayService.existsWalletAccount(walletId)));
    }

    @PostMapping("/integration/wallet/accounts")
    public ResponseEntity<Long> createWalletAccount(@RequestBody @Valid WalletAccountRequestModel requestModel) {
        WalletAccountRequestBuilder requestBuilder = WalletAccountRequestBuilder.builder()
                .username(requestModel.getUsername())
                .firstName(requestModel.getFullName())
                .lastName("L1")
                .externalId(requestModel.getId())
                .clientTypeId(ClientType.L1.value())
                .savingsProductId(SavingsProductType.L1.value())
                .email("fake@gmail.com")
                .build();
        Long walletId = paymentGatewayService.createWalletAccount(requestBuilder);
        merchantCrudService.updateWalletId(requestModel.getId(), walletId);
        return ResponseEntity.status(HttpStatus.CREATED).body(walletId);
    }

    @GetMapping("/integration/chat/accounts")
    public ResponseEntity<Map<String, Boolean>> existsWalletAccount(@RequestParam String username) {
        return ResponseEntity.ok(Collections.singletonMap("exists", chatService.existsChatAccount(username)));
    }

    @PostMapping("/integration/chat/accounts")
    public ResponseEntity<Void> createChatAccount(@RequestBody @Valid ChatAccountRequestModel requestModel) {
        ChatAccountDto chatAccountDto = new ChatAccountDto();
        chatAccountDto.setUsername(requestModel.getUsername());
        chatAccountDto.setName(requestModel.getFullName());
        chatAccountDto.setEmail(requestModel.getUsername() + "@gmail.com");
        chatAccountDto.setPass("abc@123");
        chatService.createAccount(chatAccountDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
