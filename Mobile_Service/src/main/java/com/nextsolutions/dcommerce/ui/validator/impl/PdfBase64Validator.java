package com.nextsolutions.dcommerce.ui.validator.impl;

import com.nextsolutions.dcommerce.service.FileService;
import com.nextsolutions.dcommerce.ui.validator.PdfBase64;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Base64;

public class PdfBase64Validator implements ConstraintValidator<PdfBase64, String> {

    private final FileService fileService;

    public PdfBase64Validator(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null) {
            return true;
        }
        if (value.startsWith("data:")) {
            value = value.replaceFirst("(data:.*;base64,)", "");
            byte[] fileBase64 = Base64.getDecoder().decode(value);
            String fileType = fileService.detectType(fileBase64);
            return "application/pdf".equalsIgnoreCase(fileType);
        }
        return false;
    }
}
