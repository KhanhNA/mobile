package com.nextsolutions.dcommerce.ui.model.salesman;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateSalesmanResponseModel {
    private Long id;
    private String code;
    private String phoneNumber;
    private String username;
    private String fullName;
    private Integer status;
    private String email;
    private LocalDateTime birthDate;
    private Integer gender;
    private String address;
    private String avatar;
    private String identityNumber;
    private String identityImgFront;
    private String identityImgBack;
}
