package com.nextsolutions.dcommerce.exception;

import com.nextsolutions.dcommerce.error.ApiError;
import com.nextsolutions.dcommerce.model.AndroidCrashLog;
import com.nextsolutions.dcommerce.repository.AndroidCrashLogRepository;
import com.nextsolutions.dcommerce.service.Translator;
import com.nextsolutions.dcommerce.service.exception.DuplicateEntityException;
import com.nextsolutions.dcommerce.service.exception.EntityNotFoundException;
import com.nextsolutions.dcommerce.service.impl.HunterServiceImpl;
import com.nextsolutions.dcommerce.shared.dto.BaseResponse;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestControllerAdvice
@RequiredArgsConstructor
public class RestResponseEntityExceptionHandler {

    private final AndroidCrashLogRepository crashRep;
    private static final Logger log = LogManager.getLogger(RestResponseEntityExceptionHandler.class);

    @ExceptionHandler(DuplicatePhoneNumberException.class)
    public ResponseEntity<Object> handleDuplicate(DuplicatePhoneNumberException e) {
        log.error(e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.CONFLICT).body("Duplicated phone number " + e.getPhoneNumber());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<FieldValidationError> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        log.error(ex.getMessage(), ex);
        BindingResult bindingResult = ex.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        Map<String, String> errors = new LinkedHashMap<>();
        for (FieldError error : fieldErrors)
            errors.put(error.getField(), error.getDefaultMessage());

        return new ResponseEntity<>(new FieldValidationError(400, "Validation error", errors),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(DuplicateEntityException.class)
    public ResponseEntity<?> handleDuplicateEntityException(DuplicateEntityException ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getSimpleApiResource());
    }

    @ExceptionHandler(AccountServiceException.class)
    public ResponseEntity<?> handleAccountServiceException(AccountServiceException ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
    }

    @ExceptionHandler({HttpClientErrorException.class})
    public ResponseEntity<ApiError> handleHttpClientErrorException(HttpClientErrorException ex, HttpServletRequest request) {
        log.error(ex.getMessage(), ex);
        ApiError apiError = new ApiError(ex.getRawStatusCode(), request.getRequestURI(), ex.getLocalizedMessage());
        apiError.setDeveloperMessage(ex.getResponseBodyAsString());
        return ResponseEntity.status(ex.getStatusCode()).body(apiError);
    }

    @ExceptionHandler({HttpServerErrorException.class})
    public ResponseEntity<ApiError> handleHttpServerErrorException(HttpServerErrorException ex, HttpServletRequest request) {
        log.error(ex.getMessage(), ex);
        ApiError apiError = new ApiError(ex.getRawStatusCode(), request.getRequestURI(), ex.getLocalizedMessage());
        apiError.setDeveloperMessage(ex.getResponseBodyAsString());
        return ResponseEntity.status(ex.getStatusCode()).body(apiError);
    }

    @ExceptionHandler({ActivateIncentiveProgramException.class})
    public ResponseEntity<ApiError> handleActivateIncentiveProgramException(ActivateIncentiveProgramException ex, HttpServletRequest request) {
        log.error(ex.getMessage(), ex);
        ApiError apiError = new ApiError(HttpStatus.CONFLICT.value(), request.getRequestURI(), ex.getLocalizedMessage());
        apiError.setDeveloperMessage(ex.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(apiError);
    }

    @ExceptionHandler(CustomErrorException.class)
    public ResponseEntity<Object> handleConflict(CustomErrorException ex) throws Exception {
        log.error(ex.getMessage(), ex);
        String message;
        BaseResponse baseResponse = new BaseResponse();
        try {
            message = Translator.toLocale(ex.getCode(), ex.getMessage());
        } catch (Exception e) {
            message = ex.getCode();
        }
        saveLog(message);
        baseResponse.setMessage(message);
        baseResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void saveLog(String msg) {
        AndroidCrashLog log = new AndroidCrashLog();
        log.setError(msg);
        log.setCreateDate(LocalDateTime.now());
        crashRep.save(log);
    }
}
