package com.nextsolutions.dcommerce.exception;

public class ActivateIncentiveProgramException extends RuntimeException {
    public ActivateIncentiveProgramException(String message) {
        super(message);
    }
}
