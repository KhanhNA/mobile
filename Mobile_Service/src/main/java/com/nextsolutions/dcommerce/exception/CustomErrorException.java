package com.nextsolutions.dcommerce.exception;

import com.nextsolutions.dcommerce.utils.Utils;
import lombok.*;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class CustomErrorException extends RuntimeException {

    public static final String SYSTEM_ERROR = "systemError";
    public static final String USER_ERROR = "userError";

    private LocalDateTime timestamp;
    private String code;
    private String[] error;

    public CustomErrorException(String code, String... error) {
        this.timestamp = Utils.getLocalDatetime();
        this.code = code;
        this.error = error;
    }

}
