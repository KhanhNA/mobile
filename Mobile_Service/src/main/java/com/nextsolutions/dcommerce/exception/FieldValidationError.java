package com.nextsolutions.dcommerce.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@NoArgsConstructor
public class FieldValidationError {
    private long timestamp = new Date().getTime();
    private int status;
    private String message;
    private Map<String, String> fieldErrors;

    public FieldValidationError(int status, String message, Map<String, String> fieldErrors) {
        this.status = status;
        this.message = message;
        this.fieldErrors = fieldErrors;
    }
}
