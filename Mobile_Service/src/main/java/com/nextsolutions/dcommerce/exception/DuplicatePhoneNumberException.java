package com.nextsolutions.dcommerce.exception;

public class DuplicatePhoneNumberException extends RuntimeException {
    private final String phoneNumber;

    public DuplicatePhoneNumberException(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
