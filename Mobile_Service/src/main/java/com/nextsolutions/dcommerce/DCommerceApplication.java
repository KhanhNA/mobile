package com.nextsolutions.dcommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@ComponentScan("com.nextsolutions.dcommerce.*")
//@EnableJms
//@EnableScheduling
@EnableAspectJAutoProxy
@EnableConfigurationProperties
public class DCommerceApplication {
    public static void main(String[] args) {
        SpringApplication.run(DCommerceApplication.class, args);
    }
}
