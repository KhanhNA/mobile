package com.nextsolutions.dcommerce.shared.dto.chat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendMessageResponseDTO {
    String status;
    String rID;
}
