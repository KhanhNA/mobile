package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;

@Getter
@Setter
public class OverviewMerchantDTO {
    private Integer totalRegister;
    private Integer totalOrderSuccess;
    private Integer totalActive;
    private Long merchantId;
    private LocalDateTime activeDate;
    private Double totalAmount;
    private Integer pendingOrderEL2;
    private Integer totalOrder;
    private Integer totalOrderL2;


}
