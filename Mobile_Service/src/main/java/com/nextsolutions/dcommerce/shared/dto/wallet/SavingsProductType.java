package com.nextsolutions.dcommerce.shared.dto.wallet;

public enum  SavingsProductType {
    L1(3), DISTRIBUTOR(5);
    private int value;

    SavingsProductType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
