package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nextsolutions.dcommerce.model.Coupon;
import com.nextsolutions.dcommerce.model.MerchantAddress;
import com.nextsolutions.dcommerce.model.PackingProductEntity;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingIncentive;
import com.nextsolutions.dcommerce.service.annotation.ColumnMapper;
import com.nextsolutions.dcommerce.utils.DeserializeDateHandler;
import com.nextsolutions.dcommerce.utils.Utils;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Đơn hàng: - merchant: đại lý nào đặt đơn hàng này - payment_method: thanh toán bằng phương thức nào - recv_store_id: khi order merchant sẽ chọn kho nào nhận đơn hàng này
 *
 * @author ts-client01
 * Create at 2019-06-20 13:41
 */
@Data

//@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDto implements Serializable {
    @ColumnMapper(map = "ORDER_ID")
    public Long orderId;
    /**
     * Mã đơn hàng
     */
    @ColumnMapper(map = "ORDER_NO")
    public String orderNo;

    @ColumnMapper(map = "MERCHANT_ID")
    public Long merchantId;

    /**
     * isaveOrder = 0, no save order,1:save order
     */
    public Integer isSaveOrder;

    @ColumnMapper(map = "MERCHANT_TYPE_ID")
    public Long merchantTypeId;

    @ColumnMapper(map = "MERCHANT_NAME")
    public String merchantName;

    public String phone;

    @ColumnMapper(map = "PAYMENT_METHOD_ID")
    private Long paymentMethodId;

    @ColumnMapper(map = "SHIP_METHOD_ID")
    private Long shipMethodId;

    @ColumnMapper(map = "ORDER_INVOICE_ID")
    public Long orderInvoiceId;


    @ColumnMapper(map = "SHIP_CHARGE")
    public Double shipCharge;

    @ColumnMapper(map = "SHP_CART_ID")
    public Long shpCartId;

    @ColumnMapper(map = "RECV_STORE_ID")
    public Long recvStoreId;

    @ColumnMapper(map = "amount")
    public Double amount;

    @ColumnMapper(map = "vat")
    public Double vat;

    @ColumnMapper(map = "INCENTIVE_AMOUNT")
    public Double incentiveAmount = 0d;

    @ColumnMapper(map = "QUANTITY")
    public Double quantity;

    @ColumnMapper(map = "RECV_LAT")
    public Double recvLat;

    @ColumnMapper(map = "RECV_LONG")
    public Double recvLong;

    @ColumnMapper(map = "RECV_ADDR")
    public String recvAddr;

    @ColumnMapper(map = "ORDER_STATUS")
    public Integer orderStatus;

    @ColumnMapper(map = "REMAIN_QUANTITY")
    public Double remainQuantity;

    @ColumnMapper(map = "REMAIN_AMOUNT")
    public Double remainAmount;

    @ColumnMapper(map = "IS_SUCCESS")
    public Boolean isSuccess;

    @ColumnMapper(map = "ORDER_TYPE")
    public Integer orderType;

    public Integer langId;

    @ColumnMapper(map = "PARENT_MERCHANT_ID")
    public Long parentMerchantId;

    @ColumnMapper(map = "WALLET_AMOUNT")
    public Double walletAmount;

    @ColumnMapper(map = "ORDER_DATE")
    public LocalDateTime orderDate;

    @ColumnMapper(map = "TAX")
    public Double tax;

    /**
     * Đã thanh toán chưa
     */
    @ColumnMapper(map = "PAYMENT_STATUS")
    public Integer paymentStatus;

    /**
     * Đơn hàng đã giao cho khách hàng
     */
    @ColumnMapper(map = "DELIVERY_STATUS")
    public Integer deliveryStatus;

    /**
     * Ngày thanh toán
     */
    @ColumnMapper(map = "PAYMENT_DATE")
    public LocalDateTime paymentDate;

    /**
     * Ngày giao hàng
     */
    @ColumnMapper(map = "MERCHANT_ADDRESS")
    private MerchantAddress merchantAddress;

    @JsonDeserialize(using = DeserializeDateHandler.class)
    @ColumnMapper(map = "DELIVERY_DATE")
    public LocalDateTime deliveryDate;

    // thong tin view cho don hang
    @ColumnMapper(map = "PACKING_URL")
    private String packingUrl;

    private Double packingOrgPrice;

    //số lượng sản phẩm trong 1 đơn hàng
    private Integer orderProductQuantity;

    @ColumnMapper(map = "PACKING_PRODUCT_ID")
    private Long packingProductId;

    @ColumnMapper(map = "ORDER_PACKING_ID")
    private Long orderPackingId;

    @ColumnMapper(map = "PRODUCT_NAME")
    private String productName;

    @ColumnMapper(map = "MERCHANT_IMG_URL")
    public String merchantImgUrl;

    @ColumnMapper(map = "MERCHANT_ADDRESS_ID")
    public Long merchantAddressId;

    @ColumnMapper(map = "MERCHANT_WAREHOUSE_CODE")
    public String merchantWareHouseCode;
    private String orderName;
    private Long orderTemplateId;
    private String reason;
    public String logisticCode;
    public String logisticQRCode;


    public Double productPrice;

    public Long productQuantity;

    public String merchantCode;

    public List<OrderPackingDTO> orderPackings;
    //    public HashMap<String, PackingDescription> packingDescriptions;
    public List<OrderPackingDTO> promotionPacking;

    public List<Long> packingIds;

    public Double incentiveCoupon = 0d;
    //coupon
    public Coupon coupon;
    public String couponCode;
    public Long coin;
    public Double point;
    public String paymentMethodName;
    public String tokenMifos;
    // Tong tien hang chua tru khuyen mai va + VAT
    private double orgAmount;
    // Có khuyến mại san pham hay không
    private boolean isIncentiveProduct;
    // Danh sach KM order dang duoc ap dung
    private List<Long> mapIncentives;






    public void addIncentiveAmount(Double incentive, Integer incentiveType) {
        if (incentiveType != null && incentiveType == IncentiveProgramDto.ON_WALLET) {
            if (walletAmount == null) {
                walletAmount = new Double(0);
            }
            walletAmount += incentive;
        } else {
            if (incentiveAmount == null) {
                incentiveAmount = new Double(0);
            }
            incentiveAmount += incentive;
        }
    }

    public void addPackingProduct(OrderPackingDTO packingProduct) {
        if (this.orderPackings == null) {
            this.orderPackings = new ArrayList<>();
        }
        this.orderPackings.add(packingProduct);
    }

    public void addPromotionPacking(OrderPackingDTO packingProduct) {
        if (this.promotionPacking == null) {
            this.promotionPacking = new ArrayList<>();
        }
        this.promotionPacking.add(packingProduct);
    }

    public void addOrderIncentive(Long incentiveId) {
        if (this.mapIncentives == null) {
            this.mapIncentives = new ArrayList<>();
        }
        this.mapIncentives.add(incentiveId);
    }

    public void addPromotionPacking(List<IncentivePackingIncentive> packingIncentives) {
        List<IncentivePackingIncentive> lst = Utils.safe(packingIncentives);
        for (IncentivePackingIncentive element : lst) {
            OrderPackingDTO dto = new OrderPackingDTO();
            PackingProductEntity pp = element.getPackingProduct();
            dto.setPackingCode(pp.getCode());
            dto.setPackingName(pp.getName());
//            dto.setQuantity(element.getQuantity());
            dto.setPrice(pp.getPrice());
            dto.setPackingProductId(pp.getPackingProductId());
            this.addPromotionPacking(dto);
        }
    }


    public OrderDto() {
    }

    public OrderDto(Long orderId, Integer orderStatus, Integer paymentStatus, Integer deliveryStatus) {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
        this.paymentStatus = paymentStatus;
        this.deliveryStatus = deliveryStatus;
    }
}
