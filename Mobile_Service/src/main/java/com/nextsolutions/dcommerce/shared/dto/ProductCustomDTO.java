package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductCustomDTO {
    Page<ProductDto> productDtoPage;
    private List<String> lstPrediction;
}
