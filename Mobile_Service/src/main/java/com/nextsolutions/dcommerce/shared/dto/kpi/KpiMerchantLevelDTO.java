package com.nextsolutions.dcommerce.shared.dto.kpi;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class KpiMerchantLevelDTO {
    private Long id;
    private String code;
    private Long kpiId;
    private BigDecimal target;
    private BigDecimal bonus;

}
