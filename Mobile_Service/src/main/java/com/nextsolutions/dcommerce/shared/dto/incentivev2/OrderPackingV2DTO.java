package com.nextsolutions.dcommerce.shared.dto.incentivev2;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderPackingV2DTO {
    private Integer orderQuantity;
    private Long packingProductId;
    private String productPackingCode;
    private boolean hasIncentive;
}
