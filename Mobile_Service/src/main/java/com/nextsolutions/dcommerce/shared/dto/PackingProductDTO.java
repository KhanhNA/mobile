package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PackingProductDTO {

    private Long productId;
    private String packingUrl;
    private Long packingProductId;
    private BigDecimal price;
    private BigDecimal discountPercent;
    private BigDecimal discountPrice;
    private Integer discountType;
    private Double orgPrice;
    private Double marketPrice;
    private Long quantity;
    private Long duration;
    private String productName;
    private String packingProductCode;
    private BigDecimal amount = new BigDecimal(0);
    private Long orderQuantity;
    private String distributorCode;
    private Long distributorId;
    private Long distributorWalletId;
    private BigDecimal incentiveAmount = new BigDecimal(0);
    private Integer bonusQuantity = 0;
    private Integer limitedQuantity;
    private Integer limitedPackingQuantity;
    private Integer sold;

    // Of product
    private Float reviewAvg;
    private Integer reviewCount;

    //
    public PackingProductDTO(Long productId, String packingUrl, Long packingProductId, BigDecimal price, BigDecimal discountPercent, Double orgPrice, Double marketPrice, Long quantity, String packingProductCode) {
        this.productId = productId;
        this.packingUrl = packingUrl;
        this.packingProductId = packingProductId;
        this.price = price;
        this.discountPercent = discountPercent;
        this.orgPrice = orgPrice;
        this.marketPrice = marketPrice;
        this.quantity = quantity;
        this.packingProductCode = packingProductCode;
    }

    public void setProductId(Number productId) {
        this.productId = productId.longValue();
    }

    public void setPackingProductId(Number packingProductId) {
        this.packingProductId = packingProductId.longValue();
    }

    public void setQuantity(Number quantity) {
        this.quantity = quantity.longValue();
    }

    public void setDuration(Number duration) {
        this.duration = duration.longValue();
    }

    public void setOrderQuantity(Number orderQuantity) {
        this.orderQuantity = orderQuantity.longValue();
    }

    public void setDistributorWalletId(Number distributorWalletId) {
        this.distributorWalletId = distributorWalletId.longValue();
    }
}
