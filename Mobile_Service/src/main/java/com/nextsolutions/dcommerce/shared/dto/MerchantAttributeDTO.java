package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class MerchantAttributeDTO {
    private Long id;
    private String code;
    private String name;
    private List<MerchantAttributeValueDTO> attValue;
}
