package com.nextsolutions.dcommerce.shared.dto.logistic;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class LogistictOrderDetailDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1769380618237422880L;
	private String productPackingCode;
	private String packingName;
	private Integer quantity;
	private BigDecimal price;
	private Integer type;//0-sp ban, 1- sp km
}
