package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.ManufacturerDescription;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDescriptionDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ManufacturerDescriptionMapper {
    ManufacturerDescriptionDto toManufacturerDescriptionResource(ManufacturerDescription description);
}
