package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.BannerDetails;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerDetailsResource;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BannerDetailsMapper {
    BannerDetailsResource toBannerDetailsResource(BannerDetails bannerDetails);
}
