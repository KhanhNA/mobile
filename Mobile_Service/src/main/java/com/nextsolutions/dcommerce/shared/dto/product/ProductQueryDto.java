package com.nextsolutions.dcommerce.shared.dto.product;

import lombok.Data;

@Data
public class ProductQueryDto {
    private Long categoryId;
    private String code;
    private String name;
    private Long languageId;
    private String sku;
}
