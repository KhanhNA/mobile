package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-27 15:57
 */
@Data
public class AndroidCrashLogDto {

    /**
     * 
     */
    public Long id;

    /**
     * 
     */
    public Long merchantId;

    /**
     * 
     */
    public String error;

    /**
     * 
     */
    public LocalDateTime createDate;
}
