package com.nextsolutions.dcommerce.shared.dto.incentive;

import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDetailInformationDto;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramGroupDto;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class IncentiveProgramDto {
    private Long id;
    private String name;
    private String description;
    private String code;
    private Integer type;
    private Integer status;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private Integer displayArea;
    private String createdUser;
    private LocalDateTime createdDate;
    private String latestModifiedUser;
    private LocalDateTime latestModifiedDate;
    private List<IncentiveProgramDescriptionDto> descriptions;
    private IncentiveProgramDetailInformationDto incentiveProgramDetailInformation;
    private List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroup;
    private IncentiveProgramGroupDto incentiveProgramGroup;
    private List<String> warehouseCodes;
    private List<Long> merchantGroupIds;
    private Boolean canActivate;
    private Boolean applyL2;
}
