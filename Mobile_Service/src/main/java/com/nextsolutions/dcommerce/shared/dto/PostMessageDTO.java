package com.nextsolutions.dcommerce.shared.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PostMessageDTO {
    private String alias;
    private String roomId;
    private String text;
}
