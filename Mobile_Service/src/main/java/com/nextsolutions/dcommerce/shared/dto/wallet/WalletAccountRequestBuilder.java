package com.nextsolutions.dcommerce.shared.dto.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WalletAccountRequestBuilder {
    private String username;
    private String email;
    @JsonProperty("firstname")
    private String firstName;
    @JsonProperty("lastname")
    private String lastName;
    private Integer clientTypeId; //  l1 = 1 , distributor = 2
    private Integer savingsProductId; // l1 = 3, distributor = 5
    private Long externalId;
}
