package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RequestProductDTO {
    private Long merchantId;
    private List<Long> lstPackingId;
    private int filterType;
}
