// Generated with g9.

package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.incentive.Incentive;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class IncentivePackingSaleDto implements Serializable {

    private Long id;
    private String uom;
    private Long valueType;
    private Long value;
    private Long isRequired;//0- ko bat buoc, 1- bat buoc
    private Long status;
    private LocalDateTime createDate;
    private String createUser;
    private LocalDateTime updateDate;
    private String updateUser;
    private Long incentiveLevelId;
    private Long packingProductId;
    private Long productId;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private IncentiveLevelDto incentiveLevel;
    private PackingProduct packingProduct;
    
    private Incentive incentive;
    
    
    private Long quantity;
    private Double amount;
    private Double price;
    
    
    /** Default constructor. */
    public IncentivePackingSaleDto() {
        super();
    }

    


}
