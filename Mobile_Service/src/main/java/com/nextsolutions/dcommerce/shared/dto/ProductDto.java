package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.ProductDescription;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;


@Data
@Entity
public class ProductDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    private Long manufacturerId;
    private String productCode;
    private String productName;
    private String description;
    private String urlImage;
    private Double reviewAvg;
    private Integer reviewCount;
    private Double price;
    private Integer quantity;
    //    private Integer typePromotion;
    private Double orgPrice;
    private Double marketPrice;
    private Double discountPercent;
    private Long packingProductId;
    private HashMap<String, ProductDescription> objLang;
    private String packingProductCode;
    @Transient
    private List<PackingProductDTO> packingProductList;

    public ProductDto() {
    }

    public ProductDto(Long id, Long manufacturerId, String productName, String urlImage, Double reviewAvg, Integer reviewCount, Double price, Integer quantity, Double orgPrice, Double marketPrice, Double discountPercent, Long packingProductId, String packingProductCode) {
        this.id = id;
        this.manufacturerId = manufacturerId;
        this.productName = productName;
        this.urlImage = urlImage;
        this.reviewAvg = reviewAvg;
        this.reviewCount = reviewCount;
        this.price = price;
        this.quantity = quantity;
//        this.typePromotion = typePromotion;
        this.orgPrice = orgPrice;
        this.marketPrice = marketPrice;
        this.discountPercent = discountPercent;
        this.packingProductId = packingProductId;
        this.packingProductCode = packingProductCode;
    }
}
