package com.nextsolutions.dcommerce.shared.dto.chat;

import lombok.Data;

@Data
public class ChatLoginResponseDto {
    private String status;
    private ChatLoginResponseDataDto data;
}
