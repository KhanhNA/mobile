package com.nextsolutions.dcommerce.shared.dto.product;

import com.nextsolutions.dcommerce.ui.model.response.CategoryResponseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductTableDto {

    private Long id;
    private String code;
    private String name;
    private String imageUrl;
    private String sku;
    private BigDecimal importPrice;
    private BigDecimal width;
    private BigDecimal height;
    private BigDecimal weight;
    private BigDecimal length;

    private CategoryResponseModel category;

}
