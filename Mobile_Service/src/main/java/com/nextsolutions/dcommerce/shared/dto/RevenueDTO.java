package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RevenueDTO {
    private Integer month;
    private Integer year;
    private Double amount;
}
