package com.nextsolutions.dcommerce.shared.dto;

import lombok.Value;

@Value
public class CategoryHierarchy {
    private Long id;
    private int level;
}
