package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.Distributor;
import com.nextsolutions.dcommerce.model.DistributorLink;
import com.nextsolutions.dcommerce.shared.dto.DistributorResource;
import com.nextsolutions.dcommerce.shared.dto.distributor.DistributorDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductLinksDistributorDto;
import com.nextsolutions.dcommerce.ui.model.distributor.*;
import com.nextsolutions.dcommerce.ui.model.product.GetProductLinksDistributorResponseModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DistributorMapper {
    Distributor toDistributor(DistributorResource distributorResource);

    PostDistributorResponseModel toPostDistributorResponseModel(DistributorDto source);

    DistributorDto toDistributorDto(PostDistributorRequestModel source);

    Distributor toDistributor(DistributorDto source);

    DistributorDto toDistributorDto(Distributor distributor);

    GetDistributorResponseModel toGetDistributorResponseModel(DistributorDto distributor);

    DistributorDto toDistributorDto(PutDistributorRequestModel requestModel);

    PutDistributorResponseModel toPutDistributorResponseModel(DistributorDto response);

    DistributorDto toDistributorDto(GetDistributorRequestModel requestModel);
}
