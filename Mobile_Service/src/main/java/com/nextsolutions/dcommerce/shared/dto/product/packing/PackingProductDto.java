package com.nextsolutions.dcommerce.shared.dto.product.packing;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PackingProductDto {
    private Long id;
    private String code;
    private String name;
    private String uom;
    private String imageUrl;
    private BigDecimal salePrice;
    private BigDecimal marketPrice;
    private Integer status;
}

