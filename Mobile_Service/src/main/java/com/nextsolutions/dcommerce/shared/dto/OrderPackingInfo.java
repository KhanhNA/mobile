package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderPackingInfo {
    private Long id;
    private String productCode;
    private String productName;
    private String packingProductCode;
    private String packingProductName;
    private Integer quantity;
    private BigDecimal price;
    private BigDecimal vat;
    private BigDecimal total;
}
