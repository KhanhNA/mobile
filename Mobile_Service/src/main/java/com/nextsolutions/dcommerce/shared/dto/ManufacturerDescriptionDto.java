package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

@Data
public class ManufacturerDescriptionDto {

    private Long id;

    private String updtId;

    private String description;

    private String name;

    private String title;

    private String manufacturersUrl;

    private Integer urlClicked;

    private Long manufacturerId;

    private String urlImage;

    private Long languageId;

    private String address;
}
