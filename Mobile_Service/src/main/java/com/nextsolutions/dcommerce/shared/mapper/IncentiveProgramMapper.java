package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.shared.dto.coupon.CouponIncentiveDTO;
import com.nextsolutions.dcommerce.shared.dto.incentive.IncentiveProgramDto;
import com.nextsolutions.dcommerce.ui.model.incentive.*;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {
        IncentiveProgramDescriptionMapper.class,
        IncentiveProgramDetailInformationMapper.class,
        IncentiveProgramGroupMapper.class,
})
public interface IncentiveProgramMapper {
    IncentiveProgramDto toIncentiveProgramDto(CreateIncentiveProgramRequest source);

    IncentiveProgramDto toIncentiveProgramDto(IncentiveProgram incentiveProgram);

    IncentiveProgramDto toIncentiveProgramDto(UpdateIncentiveProgramRequest requestModel);

    IncentiveProgram toIncentiveProgram(IncentiveProgramDto source);

    IncentiveProgramResponse toIncentiveProgramResponse(IncentiveProgramDto createdIncentiveProgramDto);

    GetIncentiveProgramResponse toGetIncentiveProgramResponse(IncentiveProgramDto source);

    CouponIncentiveDTO toCouponIncentiveDTO(CouponIncentive couponIncentive);

    IncentiveProgramDto toIncentiveProgramDto(PutIncentiveProgramMerchantGroup requestModel);
}
