package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoyaltyDescriptionDto {
    private String name;
    private String description;
    private Integer loyaltyType;
    private Long loyaltyId;
}
