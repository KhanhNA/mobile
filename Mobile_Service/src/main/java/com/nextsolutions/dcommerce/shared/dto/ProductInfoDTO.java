package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductInfoDTO implements Serializable {
    private Long id;
    private Long productId;
    private Long packingProductId;
    private String productName;
    private String packingProductCode;
    private String urlImage;
    private Float reviewAvg;
    private Integer reviewCount;
    private BigDecimal productPrice;
    private Double orgPrice;
    private Long quantity;
    private Long manufacturerId;
    private BigDecimal discountPercent;
    private String manufacturerName;
    private String sellerName;
    private String desProduct;
    private List<PackingProductInformationDTO> packingProductList = new ArrayList<>();

    public void setId(Number id) {
        this.id = id.longValue();
    }

    public void setProductId(Number productId) {
        this.productId = productId.longValue();
    }

    public void setPackingProductId(Number packingProductId) {
        this.packingProductId = packingProductId.longValue();
    }
    public void setOrgPrice(Number orgPrice) {
        this.orgPrice = orgPrice.doubleValue();
    }

    public void setQuantity(Number quantity) {
        this.quantity = quantity.longValue();
    }

    public void setManufacturerId(Number manufacturerId) {
        this.manufacturerId = manufacturerId.longValue();
    }

}
