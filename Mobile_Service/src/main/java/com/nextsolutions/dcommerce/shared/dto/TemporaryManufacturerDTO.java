package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.TemporaryManufacturerDescription;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemporaryManufacturerDTO {


    private Date createdDate;

    private Date modifiedDate;

    private String code;

    private String imageUrl;


    private Integer sortOrder;

    private Integer merchantId;

    private List<TemporaryManufacturerDescriptionDTO> descriptions;

    private String tel;


    private String email;


    private Long status;

    private Long manufacturerId;

    private String taxCode;
}
