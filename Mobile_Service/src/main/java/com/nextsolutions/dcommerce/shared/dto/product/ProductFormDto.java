package com.nextsolutions.dcommerce.shared.dto.product;

import com.nextsolutions.dcommerce.ui.model.response.CategoryResponseModel;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductFormDto {

    private Long id;

    @NotEmpty(message = "error.code.empty")
    @Length(message = "error.code.length")
    private String code;

    private String brand;

    private String color;

    private String origin;

    private String notes;

    private boolean returning;

    @NotEmpty(message = "error.name.empty")
    @Size(min = 1, max = 255, message = "error.name.size")
    private String name;

    @NotNull(message = "error.type.null")
    private Long type;

    @NotNull(message = "error.manufacturer.null")
    private Long manufacturerId;

    @NotEmpty(message = "error.sku.empty")
    @Length(message = "error.sku.length")
    private String sku;

    @Min(value = 0, message = "error.width.min")
    @Max(value = 999, message = "error.width.max")
    private BigDecimal width;

    @Min(value = 0, message = "error.height.min")
    @Max(value = 999, message = "error.height.max")
    private BigDecimal height;

    @Min(value = 0, message = "error.weight.min")
    @Max(value = 999, message = "error.weight.max")
    private BigDecimal weight;

    @Min(value = 0, message = "error.length.min")
    @Max(value = 999, message = "error.length.max")
    private BigDecimal length;

    private Long categoryId;

    @Valid
    private CategoryResponseModel category;

    @Valid
    private List<ProductImageDto> productImages;

    @Valid
    private List<ProductDescriptionDto> productDescriptions;

    @Valid
    private List<PackingProductDto> packingProducts;


}
