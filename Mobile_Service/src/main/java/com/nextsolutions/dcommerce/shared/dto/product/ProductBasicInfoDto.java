package com.nextsolutions.dcommerce.shared.dto.product;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class ProductBasicInfoDto {

    private Long id;
    private String code;
    private String name;
    private Long type;
    private String sku;

    private BigDecimal width;
    private BigDecimal height;
    private BigDecimal weight;
    private BigDecimal length;

    private Long manufacturerId;

    private List<Long> categoryIds;

    private List<ProductImageDto> productImages;

    private List<ProductDescriptionDto> productDescriptions;

    private Date creationDate;

    private Date lastModifiedDate;

    private boolean hot;

    private boolean ship;

}
