package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MerchantGroupResource {

    private Long id;

    private String code;

    private String name;

    private Integer isSelect;
}
