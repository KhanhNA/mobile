package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManufacturerResource {
    private Long id;
    private String code;
    private String name;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdDate;
    private String tel;
    private String email;
    private String address;
    private Long languageId;
    private Long status;
    private Long action;
    private String taxCode;
}
