package com.nextsolutions.dcommerce.shared.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.nextsolutions.dcommerce.model.Category;
import com.nextsolutions.dcommerce.model.CategoryDescription;

import lombok.Data;

@Data
@Entity
public class CategoryDto {
	@Id
	private Long id;
	private String code;
	private String title;
	private String name;
	private Integer sortOrder;
	private String image;
	private String description;
	private Boolean expandable;
	private String treeStruct;
	@Transient
	private Long level;
	@Transient
	private CategoryDto parent;
	@Transient
	private List<CategoryDescriptionDto> descriptions;

	public CategoryDto() {

	}

	public CategoryDto(Category category) {
		if (category != null) {
			this.id = category.getId();
			this.code = category.getCode();
			this.name = category.getName();
			this.sortOrder = category.getSortOrder();
			this.image = category.getImage();
			this.parent = new CategoryDto(category.getParent());
			this.descriptions = new ArrayList<CategoryDescriptionDto>();
			for (CategoryDescription categoryDescription : category.getDescriptions()) {
				this.descriptions.add(new CategoryDescriptionDto(categoryDescription));
			}
		}
	}
}
