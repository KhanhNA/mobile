package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductGeneralDto {
    private Long id;
    private String code;
    private String internationalName;
    private List<ProductDescriptionDto> descriptions;
}
