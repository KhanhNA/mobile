package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SimpleApiResource {
    private String code;
    private String message;
    private Object value;
}
