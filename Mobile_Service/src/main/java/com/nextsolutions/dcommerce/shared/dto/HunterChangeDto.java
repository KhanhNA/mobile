package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class HunterChangeDto {
    private Long hunterId;
    private String hunterCode;
    private String phoneNumber;
    private String username;
    private String fullName;
    private Integer status;
    private String email;
    private Integer gender;
    private String address;
    private String avatar;
    private String identityNumber;
    private String identityImgFront;
    private String identityImgBack;
    private String birthDate;
    private LocalDateTime createDate;
    private String bankAccountNo;

    private Long walletClientId;

    private Long parentMarchantId;

    private String contractNumber;

    private Integer cardType;

    private String fatherName;

    private String religion;

    private String identityCreateDate;

    private Double height;

    private String bloodType;

    private String firstName;

    private String significantFigure;

    private String lastName;


}
