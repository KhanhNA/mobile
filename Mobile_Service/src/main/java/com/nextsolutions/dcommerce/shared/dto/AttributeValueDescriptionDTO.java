package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.AttributeValue;
import lombok.Data;

import java.io.Serializable;

@Data
public class AttributeValueDescriptionDTO implements Serializable {
    private Long id;
    private AttributeValue attributeValue;
    private String name;
    private Integer isSelect;
}
