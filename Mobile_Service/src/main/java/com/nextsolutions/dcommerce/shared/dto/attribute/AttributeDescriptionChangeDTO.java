package com.nextsolutions.dcommerce.shared.dto.attribute;

import lombok.Data;

@Data
public class AttributeDescriptionChangeDTO {
    private Long id;
    private Long attributeId;
    private String name;
    private Integer languageId;
}
