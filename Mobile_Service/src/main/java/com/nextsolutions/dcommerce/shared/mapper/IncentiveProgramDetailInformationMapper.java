package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.IncentiveProgramDetailInformation;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramDetailInformationDto;
import com.nextsolutions.dcommerce.shared.dto.incentive.IncentiveProgramDto;
import com.nextsolutions.dcommerce.ui.model.incentive.IncentiveProgramDetailInformationResponse;
import com.nextsolutions.dcommerce.ui.model.incentive.PutIncentiveProgramDetailInformationRequest;
import com.nextsolutions.dcommerce.ui.model.incentive.PutIncentiveProgramFlashSale;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {
        IncentiveProgramFlashSaleDateMapper.class
})
public interface IncentiveProgramDetailInformationMapper {
    IncentiveProgramDetailInformationDto toIncentiveProgramDetailInformationDto(PutIncentiveProgramDetailInformationRequest source);

    IncentiveProgramDetailInformation toIncentiveProgramDetailInformation(IncentiveProgramDetailInformationDto source);

    IncentiveProgramDetailInformationDto toIncentiveProgramDetailInformationDto(IncentiveProgramDetailInformation source);

    IncentiveProgramDetailInformationResponse toIncentiveProgramDetailInformationResponse(IncentiveProgramDetailInformationDto source);

    IncentiveProgramDetailInformationDto toIncentiveProgramDetailInformationDto(PutIncentiveProgramFlashSale source);

    List<IncentiveProgramDetailInformationDto> toIncentiveProgramDetailInformationGroupDto(List<PutIncentiveProgramFlashSale> source);
}
