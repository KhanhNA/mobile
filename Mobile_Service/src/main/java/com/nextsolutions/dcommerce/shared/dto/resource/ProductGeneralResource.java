package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

import java.util.List;

@Data
public class ProductGeneralResource {
    private Long id;
    private Long categoryId;
    private String code;
    private String defaultName;
    private List<ProductDescriptionResource> productDescriptions;
}
