package com.nextsolutions.dcommerce.shared.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BaseResponse {
    private int code;
    private String message;
}
