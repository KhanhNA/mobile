package com.nextsolutions.dcommerce.shared.dto.resource;

import com.nextsolutions.dcommerce.shared.dto.product.ProductAttributeDto;
import lombok.Data;

import java.util.List;

@Data
public class ProductAttributeResource {
    private Long id;
    private List<ProductAttributeDto> productAttributes;
}
