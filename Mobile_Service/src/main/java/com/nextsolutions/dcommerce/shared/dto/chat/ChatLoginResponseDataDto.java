package com.nextsolutions.dcommerce.shared.dto.chat;

import lombok.Data;

@Data
public class ChatLoginResponseDataDto {
    private String userId;
    private String authToken;
}
