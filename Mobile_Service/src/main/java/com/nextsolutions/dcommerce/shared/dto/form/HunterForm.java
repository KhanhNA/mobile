package com.nextsolutions.dcommerce.shared.dto.form;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class HunterForm {
    private Long id;
    private String phoneNumber;
    private String username;
    private String fullName;
    private Integer status;
    private String email;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime birthDate;
    private Integer gender;
    private String address;
}
