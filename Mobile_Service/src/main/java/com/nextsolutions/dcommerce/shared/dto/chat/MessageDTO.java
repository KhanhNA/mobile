package com.nextsolutions.dcommerce.shared.dto.chat;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageDTO {
    String _id;
    String msg;
    String type;
}
