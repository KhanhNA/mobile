package com.nextsolutions.dcommerce.shared.dto.attribute;

import com.nextsolutions.dcommerce.shared.dto.resource.AttributeValueDescriptionResource;
import lombok.Data;

import java.util.List;

@Data
public class AttributeValueChangeDTO {
    private Long id;
    private Long attributeId;
    private Integer status;
    private String parentCode;
    private String code;
    private List<AttributeValueDescriptionResource> descriptions;
}
