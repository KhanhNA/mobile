package com.nextsolutions.dcommerce.shared.projection;

public interface PackingProductProjection {
    Long getPackingProductId();
    Double getPrice();
}
