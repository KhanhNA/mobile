package com.nextsolutions.dcommerce.shared.dto.chat;

import lombok.Data;

@Data
public class DiscussionDTO {
    private RoomDTO discussion;
}
