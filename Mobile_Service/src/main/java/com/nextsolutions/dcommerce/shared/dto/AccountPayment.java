package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountPayment {
    Long merchantId;
    String name;
}
