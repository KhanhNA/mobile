package com.nextsolutions.dcommerce.shared.dto.wallet;

public enum ClientType {
    L1(1), DISTRIBUTOR(2);

    private int value;

    ClientType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
