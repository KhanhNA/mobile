package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.Category;
import com.nextsolutions.dcommerce.model.CategoryDescription;

import lombok.Data;

import java.util.List;

@Data
public class CategoryDescriptionDto {
	private Long id;
	private String name;
	private String title;
	private String urlImage;
	private String description;
	private Long languageId;
	private Long categoryId;
	private boolean hasParent;

	public CategoryDescriptionDto() {
	}

	public CategoryDescriptionDto(CategoryDescription categoryDescription) {
		this.id = categoryDescription.getId();
		this.name = categoryDescription.getName();
		this.title = categoryDescription.getTitle();
		this.urlImage = categoryDescription.getUrlImage();
		this.description = categoryDescription.getDescription();
		this.languageId = categoryDescription.getLanguageId();
		this.categoryId = categoryDescription.getCategory().getId();
		List<Category> children = categoryDescription.getCategory().getChildren();
		this.hasParent = children == null || children.isEmpty();
	}
}
