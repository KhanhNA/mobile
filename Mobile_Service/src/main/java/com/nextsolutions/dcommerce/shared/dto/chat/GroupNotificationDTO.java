package com.nextsolutions.dcommerce.shared.dto.chat;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GroupNotificationDTO {
    private String prid;
    private String t_name;
    private String[] users;

}
