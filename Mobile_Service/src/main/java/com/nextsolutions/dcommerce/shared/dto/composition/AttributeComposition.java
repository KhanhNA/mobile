package com.nextsolutions.dcommerce.shared.dto.composition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttributeComposition {
    private Long attributeId;
    private String attributeCode;
    private String attributeName;
    private Integer attributeType;
    private Long attributeValueId;
    private String attributeValueCode;
    private String attributeValueName;
}
