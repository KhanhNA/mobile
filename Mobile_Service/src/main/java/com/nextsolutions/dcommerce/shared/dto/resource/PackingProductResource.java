package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PackingProductResource {
    private Long id;
    private Long productId;
    private Long packingTypeId;
    private String code;
    private String name;
    private String imageUrl;
    private Double originalPrice;
    private Double salePrice;
    private Double marketPrice;
    private String uom;
    private List<PromotionPriceResource> promotionPrices;

    public PackingProductResource(Long id, Long productId, Long packingTypeId, String code, String name, String imageUrl, Double originalPrice, Double salePrice, Double marketPrice, String uom) {
        this.id = id;
        this.productId = productId;
        this.packingTypeId = packingTypeId;
        this.code = code;
        this.name = name;
        this.imageUrl = imageUrl;
        this.originalPrice = originalPrice;
        this.salePrice = salePrice;
        this.marketPrice = marketPrice;
        this.uom = uom;
    }
}
