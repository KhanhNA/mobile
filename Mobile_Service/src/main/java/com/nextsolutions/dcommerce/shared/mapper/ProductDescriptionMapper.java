package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.ProductDescription;
import com.nextsolutions.dcommerce.shared.dto.product.ProductDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductDescriptionResource;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductDescriptionMapper {

    @Named("toProductDescriptionDto")
    ProductDescriptionDto toProductDescriptionDto(ProductDescription productDescription);

    @IterableMapping(qualifiedByName = "toProductDescriptionDto")
    List<ProductDescriptionDto> toProductDescriptionDtos(List<ProductDescription> productDescriptions);

    @Mapping(target = "product", ignore = true)
    ProductDescription toProductDescription(ProductDescriptionDto productDescriptionDto);

    ProductDescriptionResource toProductDescriptionResource(ProductDescription productDescription);
}
