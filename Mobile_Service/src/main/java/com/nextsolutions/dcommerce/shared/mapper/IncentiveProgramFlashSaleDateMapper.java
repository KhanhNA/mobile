package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.IncentiveProgramFlashSaleDate;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramFlashSaleDateDto;
import com.nextsolutions.dcommerce.ui.model.incentive.IncentiveProgramFlashSaleDateResponse;
import com.nextsolutions.dcommerce.ui.model.incentive.PutIncentiveProgramFlashSaleDate;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IncentiveProgramFlashSaleDateMapper {
    IncentiveProgramFlashSaleDateDto toIncentiveProgramFlashSaleDateDto(PutIncentiveProgramFlashSaleDate source);

    IncentiveProgramFlashSaleDate toIncentiveProgramFlashSaleDate(IncentiveProgramFlashSaleDateDto source);

    IncentiveProgramFlashSaleDateDto toIncentiveProgramFlashSaleDateDto(IncentiveProgramFlashSaleDate source);

    IncentiveProgramFlashSaleDateResponse toIncentiveProgramFlashSaleDateResponse(IncentiveProgramFlashSaleDateDto source);
}
