package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nextsolutions.dcommerce.utils.DeserializeDateOrderDTOHandler;
import com.nextsolutions.dcommerce.utils.SerializeDateHandler;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderPredictionDTO {
    private Long merchantId;

    private List<OrderPackingDTO> orderPackings;

    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonDeserialize(using = DeserializeDateOrderDTOHandler.class)
    private LocalDateTime orderDate;
}
