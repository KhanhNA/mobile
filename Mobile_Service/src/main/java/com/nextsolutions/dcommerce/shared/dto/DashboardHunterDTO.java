package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class DashboardHunterDTO {
    private Long totalRegister;
    private Long totalActive;
    private Long totalInactive;
    private Long totalMerchantInactiveRevenue;
    private Long totalOrder;
    private BigDecimal totalRevenue;
    private Long totalPending;
    private String updateAt;
}
