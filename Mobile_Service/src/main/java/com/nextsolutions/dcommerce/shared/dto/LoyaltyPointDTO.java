package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoyaltyPointDTO {
    private Long packingProductId;
    private Double minAmount;
    private Long minQuantity;
    private Long loyaltyCoin;
    private Long loyaltyPoint;
}
