package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PackingProductSearchResult {
    private Long packingId;
    private String packingCode;
    private String productImageUrl;
    private String productName;
    private Integer packingQuantity;
    private BigDecimal packingOriginalPrice;
    private BigDecimal packingSalePrice;
    private Boolean isExistsInOtherIncentivePrograms;
}
