package com.nextsolutions.dcommerce.shared.dto.wallet;

import com.nextsolutions.dcommerce.model.OrderTransferAmount;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TransferRequestBuilder {
    private Long merchantWalletId;
    private String description;
    private List<OrderTransferAmount> details;
}
