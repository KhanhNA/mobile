package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CallLoyaltyDTO {
    private Long packingProductId;
    private Integer loyaltyType;
    private Double minAmount;
    private Long minQuantity;
    private Long loyaltyCoin;
    private Long loyaltyPoint;
}
