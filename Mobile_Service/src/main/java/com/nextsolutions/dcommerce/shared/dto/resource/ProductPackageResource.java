package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

import java.util.List;

@Data
public class ProductPackageResource {
    private Long id;
    private List<PackingProductResource> packingProducts;
}
