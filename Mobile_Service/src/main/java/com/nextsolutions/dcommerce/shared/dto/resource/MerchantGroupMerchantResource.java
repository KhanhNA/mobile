package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MerchantGroupMerchantResource {
    private Long id;
    private Long merchantId;
    private Long merchantGroupId;
}
