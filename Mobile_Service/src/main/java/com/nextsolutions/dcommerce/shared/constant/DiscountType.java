package com.nextsolutions.dcommerce.shared.constant;

public enum DiscountType {
    DISCOUNT_PERCENT(1),
    DISCOUNT_PRICE(2),
    BONUS_PACKING_PRODUCT(3),
    CART_COUPON(4);

    private final int value;

    DiscountType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
