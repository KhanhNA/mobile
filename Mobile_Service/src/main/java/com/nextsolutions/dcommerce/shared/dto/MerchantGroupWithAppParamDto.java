package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.MerchantGroup;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.HashMap;

@Data
public class MerchantGroupWithAppParamDto {

    Page<MerchantGroup> merchantGroups;
    HashMap<String, String> locationBusinesses;
    HashMap<String, String> areas;

    HashMap<String, String> electricalInfrastructures;

    HashMap<String, String> displayDevices;

    HashMap<String, String> refrigerationEquipments;

    public MerchantGroupWithAppParamDto() {
//        this.merchantGroups = new ArrayList<>();
        this.locationBusinesses = new HashMap<>();
        this.areas = new HashMap<>();
        this.electricalInfrastructures = new HashMap<>();
        this.displayDevices = new HashMap<>();
        this.refrigerationEquipments = new HashMap<>();
    }
}
