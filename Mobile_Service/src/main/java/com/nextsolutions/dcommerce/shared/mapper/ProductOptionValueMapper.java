package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.ProductOptionValue;
import com.nextsolutions.dcommerce.shared.dto.ProductOptionValueDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {ProductOptionValueDescriptionMapper.class})
public interface ProductOptionValueMapper {
    ProductOptionValueDto toProductOptionValueDto(ProductOptionValue productOptionValue);
}
