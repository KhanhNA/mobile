package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nextsolutions.dcommerce.model.TemporaryManufacturer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemporaryManufacturerDescriptionDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private LocalDateTime dateCreate;

    private LocalDateTime dateModified;

    private String updtId;

    private String description;

    private String name;

    private String title;

    private LocalDateTime dateLastClick;

    private String manufacturersUrl;

    private Integer urlClicked;

    private Long languageId;

    private Long manufacturerId;

    private String urlImage;

    private Boolean isSynchronization;

    private String address;

    private Long descriptionId;


}
