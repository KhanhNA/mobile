package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class OverviewStatistics {
    private long totalOrders;
    private double totalOrderRate;

    private BigDecimal totalSales;
    private double totalSalesRate;

    private long totalActiveMerchants;
    private double totalActiveMerchantsRate;

    private int totalActiveMerchantsInMonth;
    private double totalActiveMerchantsInMonthRate;
}
