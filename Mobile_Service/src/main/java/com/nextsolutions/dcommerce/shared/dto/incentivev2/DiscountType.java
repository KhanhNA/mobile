package com.nextsolutions.dcommerce.shared.dto.incentivev2;

public enum DiscountType {

    DISCOUNT_PERCENT(1), DISCOUNT_PRICE(2), HAS_INCENTIVE_PACKING(3);
    int value;

    DiscountType(int v) {
        this.value = v;
    }

    public int value() {
        return value;
    }
}
