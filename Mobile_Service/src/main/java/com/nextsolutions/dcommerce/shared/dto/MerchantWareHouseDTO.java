package com.nextsolutions.dcommerce.shared.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MerchantWareHouseDTO {
    private String warehouseCode;
    private String address;
}
