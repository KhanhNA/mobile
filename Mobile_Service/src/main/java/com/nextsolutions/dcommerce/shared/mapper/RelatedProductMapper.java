package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.RelatedProduct;
import com.nextsolutions.dcommerce.shared.dto.product.ProductLinksRelatedProductDto;
import com.nextsolutions.dcommerce.ui.model.product.GetProductLinksRelatedProductResponseModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RelatedProductMapper {
    ProductLinksRelatedProductDto toProductLinksRelatedProductDto(RelatedProduct source);
    GetProductLinksRelatedProductResponseModel toGetProductLinksRelatedProductResponseModel(ProductLinksRelatedProductDto source);
}
