package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.IncentiveProgramGroup;
import com.nextsolutions.dcommerce.shared.dto.IncentiveProgramGroupDto;
import com.nextsolutions.dcommerce.ui.model.incentive.IncentiveProgramGroupResponse;
import com.nextsolutions.dcommerce.ui.model.incentive.PutIncentiveProgramGroupRequest;
import com.nextsolutions.dcommerce.ui.model.incentive.PutIncentiveProgramMerchantGroup;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {
        IncentiveProgramDetailInformationMapper.class,
})
public interface IncentiveProgramGroupMapper {
    IncentiveProgramGroupDto toIncentiveProgramGroupDto(PutIncentiveProgramGroupRequest source);

    IncentiveProgramGroup toIncentiveProgramGroup(IncentiveProgramGroupDto source);

    IncentiveProgramGroupDto toIncentiveProgramGroupDto(IncentiveProgramGroup source);

    IncentiveProgramGroupResponse toIncentiveProgramGroupResponse(IncentiveProgramGroupDto source);
}
