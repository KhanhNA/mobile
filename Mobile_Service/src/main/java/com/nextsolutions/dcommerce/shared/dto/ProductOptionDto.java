package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.util.List;

@Data
public class ProductOptionDto {
    private Long id;
    private String code;
    private List<ProductOptionDescriptionDto> descriptions;
}
