package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.time.LocalDateTime;


/**
 * 
 * @author ts-client01
 * Create at 2019-06-22 16:05
 */
@Data
public class PackingDescriptionDto {

    /**
     * 
     */
    public Long descriptionId;

    /**
     * 
     */
    public LocalDateTime dateCreated;

    /**
     * 
     */
    public LocalDateTime dateModified;

    /**
     * 
     */
    public String updtId;

    /**
     * 
     */
    public String description;

    /**
     * 
     */
    public String packingName;

    /**
     * 
     */
    public String packingTitle;

    /**
     * 
     */
    public String metaDescription;

    /**
     * 
     */
    public String metaKeywords;

    /**
     * 
     */
    public String metaTitle;

    /**
     * 
     */
    public String downloadLnk;

    /**
     * 
     */
    public String productHighlight;

    /**
     * 
     */
    public String sefUrl;

    /**
     * 
     */
    public Integer languageId;

    /**
     * 
     */
    public Long packingProductId;

    /**
     * 
     */
    public String urlImage;
}
