package com.nextsolutions.dcommerce.shared.dto;

import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPrediction {
    private Long user;
    private Integer num;

    private Long merchantId;
    private List<String> lstPrediction;

    private int filterType;
}
