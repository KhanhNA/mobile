package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDescriptionDto {
    private Long id;
    private String localizedName;
    private Long languageId;
    private String title;
    private Long productId;
    private String description;
    private String metaTagTitle;
    private String metaTagDescription;
    private String metaTagKeywords;
    private List<String> productTags;
}
