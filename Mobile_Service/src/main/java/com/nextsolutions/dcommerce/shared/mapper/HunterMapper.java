package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.HunterDto;
import com.nextsolutions.dcommerce.shared.dto.MerchantDto;
import com.nextsolutions.dcommerce.ui.model.request.CreateHunterRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.MerchantQueryRequestModel;
import com.nextsolutions.dcommerce.ui.model.request.UpdateHunterRequestModel;
import com.nextsolutions.dcommerce.ui.model.response.CreateHunterResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.HunterResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.MerchantResponseModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface HunterMapper {

    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "hunterCode", ignore = true)
    @Mapping(target = "hunterId", ignore = true)
    HunterDto toHunterDto(CreateHunterRequestModel hunterRequestModel);

    @Mapping(target = "avatar", source = "merchantImgUrl")
    @Mapping(target = "hunterCode", source = "merchantCode")
    @Mapping(target = "hunterId", source = "merchantId")
    @Mapping(target = "username", source = "userName")
    @Mapping(target = "phoneNumber", source = "mobilePhone")
    @Mapping(target = "birthDate", source = "dateBirth")
    @Mapping(target = "bankAccountNo", source = "defaultBankAccountNo")
    HunterDto toHunterDto(Merchant merchant);

    @Mapping(target = "id", source = "hunterId")
    @Mapping(target = "code", source = "hunterCode")
    CreateHunterResponseModel toCreateHunterResponseModel(HunterDto hunterDto);

    @Mapping(target = "merchantCode", source = "code")
    @Mapping(target = "birthday", source = "birthDate")
    @Mapping(target = "mobilePhone", source = "phoneNumber")
    @Mapping(target = "userName", source = "username")
    MerchantDto toHunterDto(MerchantQueryRequestModel merchantQueryRequestModel);

    @Mapping(target = "username", source = "userName")
    @Mapping(target = "phoneNumber", source = "mobilePhone")
    @Mapping(target = "id", source = "merchantId")
    @Mapping(target = "code", source = "merchantCode")
    MerchantResponseModel toMerchantResponseModel(MerchantDto merchantDto);

    @Mapping(target = "registerDate", source = "createDate")
    MerchantDto toMerchantDto(Merchant merchant);

    @Mapping(target = "registerDate", source = "createDate")
    @Mapping(target = "id", source = "hunterId")
    HunterResponseModel toHunterResponseModel(HunterDto hunterById);

    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "hunterCode", ignore = true)
    @Mapping(target = "hunterId", ignore = true)
    HunterDto toHunterDto(UpdateHunterRequestModel requestModel);
}
