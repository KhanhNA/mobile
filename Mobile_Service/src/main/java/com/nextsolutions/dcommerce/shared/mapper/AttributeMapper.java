package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.Attribute;
import com.nextsolutions.dcommerce.shared.dto.attribute.AttributeChangeDTO;
import com.nextsolutions.dcommerce.shared.dto.form.AttributeForm;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",
        uses = {AttributeDescriptionMapper.class})
public interface AttributeMapper {
    AttributeForm toAttributeForm(Attribute attribute);

    AttributeChangeDTO toAttributeChangeDTO(AttributeForm attributeForm);
}
