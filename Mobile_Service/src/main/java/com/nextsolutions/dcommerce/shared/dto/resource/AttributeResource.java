package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

import java.util.List;

@Data
public class AttributeResource {
    private Long id;
    private String code;
    private String name;
    private Integer type;
    private List<AttributeValueResource> values;
}
