package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.shared.dto.product.ProductImageDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductBasicInfoDto {

    private Long id;
    private String name;
    private String code;
    private String sku;

    private BigDecimal width;
    private BigDecimal height;
    private BigDecimal weight;
    private BigDecimal length;

    private Long manufacturerId;
    private Long productTypeId;
    private Long merchantId;
    private List<Long> categoryIds;
    private List<String> descriptions;

    private List<ProductImageDto> productImages;

}
