package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.KpiDescription;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Data
@NoArgsConstructor
public class KpiDto {

    private Long id;

    private String type;

    private String code;

    private String name;

    private Double plan;

    private Double bonus;

    private LocalDateTime fromDate;

    private LocalDateTime toDate;

    private LocalDateTime createDate;
    private String createUser;
    private LocalDateTime updateDate;
    private String updateUser;

    private HashMap<String, KpiDescription> descriptions;

    private List<KpiDescription> descriptionsList;
}
