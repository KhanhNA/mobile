package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

@Data
public class ProductDataResource {
    private Long id;
    private String sku;
    private String brand;
    private String origin;
    private String color;
    private String notes;
    private Boolean returning;

    private Double width;
    private Double height;
    private Double length;
    private String dimensionClass;
    private Double weight;
    private String weightClass;
}
