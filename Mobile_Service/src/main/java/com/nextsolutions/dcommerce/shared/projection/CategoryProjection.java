package com.nextsolutions.dcommerce.shared.projection;

public interface CategoryProjection {
    String getId();
    String getCode();
    String getName();
}
