package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.Banner;
import com.nextsolutions.dcommerce.ui.model.response.BannerFormResponseModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {
        BannerDescriptionMapper.class,
        BannerDetailsMapper.class,
        MerchantGroupMapper.class,
})
public interface BannerMapper {
    BannerFormResponseModel toBannerFormResponseModel(Banner banner);
}
