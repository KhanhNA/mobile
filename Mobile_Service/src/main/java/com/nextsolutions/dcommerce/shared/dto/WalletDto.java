package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * - Mỗi đại lý (merchant) sẽ có 1 ví (wallet)
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
@Getter
@Setter
public class WalletDto {

    /**
     * 
     */
    public Long walletId;

    /**
     * 
     */
    public Long merchantId;

    /**
     * 
     */
    public Double amount;
    public List<WalletHisDto> walletHisDtos;

    public String merchantCode;
}
