package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

@Data
public class MerchantGroupDescriptionResource {
    private Long id;
    private String name;
    private Integer languageId;
}
