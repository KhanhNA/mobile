package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class HisOrderTransfer {
    private Long id;
    private Long orderId;
    private Double amount;
    private LocalDateTime createDate;
}
