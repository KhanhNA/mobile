package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.utils.Utils;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Data
public class BaseDto {

    @Transient
    public String ORDER_BY;
    @Transient
    public Integer langId;
    @Transient
    public String _text;
    @Transient
    public String[] _displayCols;

    public HashMap<String, Object> getTextCondition(String alias) throws Exception {
        StringBuilder cond = new StringBuilder();
        HashMap<String, Object> params = new HashMap<>();
        LocalDateTime date;

        if (!Utils.isEmpty(alias) && !alias.endsWith(".")) {
            alias = alias.trim() + ".";
        }

        if (Utils.isEmpty(_text) || Utils.isEmpty(_displayCols)) {
            return null;
        }
        List<Field> fields = new ArrayList<>();
        Utils.getAllFields(fields, getClass());
        HashMap<String, Field> mapFields = new HashMap<>();
        for (Field f : fields) {
            f.setAccessible(true);
            mapFields.put(f.getName(), f);
        }
        Field field;
        Object value;
        Class<?> type;
        String or = "";
        for (String col : _displayCols) {
            field = mapFields.get(col);
            if (field == null) {
                continue;

            }
            type = field.getType();
            value = null;
            if (type.isAssignableFrom(LocalDateTime.class) || type.isAssignableFrom(LocalDate.class)
                    || type.isAssignableFrom(Date.class)) {
                date = Utils.tryToDate(this._text);
                if (date != null) {
                    value = date;
                    cond.append(or + " (" + alias + col + " >= :" + col + ") ");
                }
            } else if (type.isAssignableFrom(String.class)) {

                value = '%' + _text + '%';
                cond.append(or + " (" + alias + col + " like :" + col + ") ");
            } else if (type.isAssignableFrom(Number.class)) {
                value = '%' + _text + '%';
                cond.append(or + " (" + alias + col + " like :" + col + ") ");
            }
            if (value == null) {
                continue;
            }

            or = " or ";
            params.put(col, value);
        }
        params.put("_CONDITION_", cond.toString());
        return params;
    }

}
