package com.nextsolutions.dcommerce.shared.dto.product;

import com.nextsolutions.dcommerce.service.annotation.ColumnMapper;
import com.nextsolutions.dcommerce.shared.dto.CurrencyDTO;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class PackingProductDto {
    private Long id;

    @NotNull
    private Long productId;

    @NotNull
    private Long packingTypeId;

    private String code;

    private String name;

    private String imageUrl;

    private Double originalPrice;

    private Double discountOriginalPrice;

    private Double discountPercentOriginalPrice;

    private LocalDateTime discountFromDate;

    private LocalDateTime discountToDate;

    private BigDecimal salePrice;

    private Double promotionSalePrice;

    private Double promotionPercentSalePrice;

    private LocalDateTime promotionFromDate;

    private LocalDateTime promotionToDate;

    private Double marketPrice;

    private String uom;

    private BigDecimal width;
    private BigDecimal height;
    private BigDecimal weight;
    private BigDecimal length;
    private String lengthClass;
    private String weightClass;
    private Integer warningThreshold;
    private BigDecimal importPrice;
    private Double exchangeRate;
    private BigDecimal money;
    private CurrencyDTO currencyPurchase;
    private PackingPriceDto packingSaleOutPrice;
    private PackingPriceDto packingSaleInPrice;
    @Valid
    private List<PackingPriceDto> promotionPrices;
}
