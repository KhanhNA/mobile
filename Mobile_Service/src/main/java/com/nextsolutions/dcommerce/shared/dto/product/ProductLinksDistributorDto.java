package com.nextsolutions.dcommerce.shared.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductLinksDistributorDto {
    private Long id;
    private String code;
    private String name;
    private Long walletId;
}
