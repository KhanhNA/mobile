package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.PackingPrice;
import com.nextsolutions.dcommerce.shared.dto.product.PackingPriceDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PackingPriceMapper {

    PackingPriceDto toPackingPriceDto(PackingPrice packingPrice);

}
