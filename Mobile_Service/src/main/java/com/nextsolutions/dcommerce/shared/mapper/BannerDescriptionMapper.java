package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.BannerDescription;
import com.nextsolutions.dcommerce.shared.dto.resource.BannerDescriptionResource;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BannerDescriptionMapper {
    BannerDescriptionResource toBannerDescriptionResource(BannerDescription description);
}
