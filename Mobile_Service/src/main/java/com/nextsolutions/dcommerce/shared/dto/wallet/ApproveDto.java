package com.nextsolutions.dcommerce.shared.dto.wallet;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Getter
@Setter
public class ApproveDto {


    public String dateFormat = "yyyy-MM-dd";
    public String locale = "vi";
    public String approvedOnDate ;
//    public String activatedOnDate;


}
