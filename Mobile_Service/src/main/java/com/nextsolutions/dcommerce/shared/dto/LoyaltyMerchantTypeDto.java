package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

/**
 * 1 chương trình khuyến mại được áp dụng cho những loại merchant nào
 * @author ts-client01
 * Create at 2019-06-21 10:13
 */
@Data
public class LoyaltyMerchantTypeDto {

    /**
     * 
     */
    public Long id;

    /**
     * 
     */
    public Long loyaltyId;

    /**
     * 
     */
    public Long merchantTypeId;

    /**
     * 
     */
    public Long status;
}
