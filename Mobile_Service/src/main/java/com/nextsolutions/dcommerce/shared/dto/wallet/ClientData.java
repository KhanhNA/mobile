/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.nextsolutions.dcommerce.shared.dto.wallet;

import lombok.Data;
import org.joda.time.LocalDate;

@Data

public class ClientData {


    public Long id;
    public String accountNo;
    public String externalId;

    public EnumOptionData status;
//    public CodeValueData subStatus;

    public Boolean active;
//    public LocalDate activationDate;

    public String firstname;
    public String middlename;
    public String lastname;
    public String fullname;
    public String displayName;
    public String mobileNo;
	public String emailAddress;
    public LocalDate dateOfBirth;
//    public CodeValueData gender;
//    public CodeValueData clientType;
//    public CodeValueData clientClassification;
    public Boolean isStaff;

    public Long officeId;
    public String officeName;
    public Long transferToOfficeId;
    public String transferToOfficeName;

    public Long imageId;
    public Boolean imagePresent;
    public Long staffId;
    public String staffName;
//    public ClientTimelineData timeline;

    public Long savingsProductId;
    public String savingsProductName;

    public Long savingsAccountId;
    public EnumOptionData legalForm;

    // associations
//    public Collection<GroupGeneralData> groups;

    // template
//    public Collection<OfficeData> officeOptions;
//    public Collection<StaffData> staffOptions;
//    public Collection<CodeValueData> narrations;
//    public Collection<SavingsProductData> savingProductOptions;
//    public Collection<SavingsAccountData> savingAccountOptions;
//    public Collection<CodeValueData> genderOptions;
//    public Collection<CodeValueData> clientTypeOptions;
//    public Collection<CodeValueData> clientClassificationOptions;
//    public Collection<CodeValueData> clientNonPersonConstitutionOptions;
//    public Collection<CodeValueData> clientNonPersonMainBusinessLineOptions;
//    public List<EnumOptionData> clientLegalFormOptions;
//    public ClientFamilyMembersData familyMemberOptions;
//
//    public ClientNonPersonData clientNonPersonDetails;
//    
//    public Collection<AddressData> address;
//
//	public Boolean isAddressEnabled;

}
