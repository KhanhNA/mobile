package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;


/**
 * @author PhamBien
 */
@Getter
@Setter

public class MerchantAddressDto {
    private Long id;
    /**
     *
     */
    private Long merchantId;

    /**
     *
     */
    private int province;

    /**
     *
     */
    private int district;

    /**
     *
     */
    private int village;

    /**
     *
     */
    private String phoneNumber;
    /**
     *
     */
    private String fullName;
    /**
     *
     */
    private int isSelect;

    private String address;

}
