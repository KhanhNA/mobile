package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.ProductOptionValueDescription;
import com.nextsolutions.dcommerce.shared.dto.ProductOptionValueDescriptionDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductOptionValueDescriptionMapper {
    @Mapping(target = "productOptionValueId", source = "optionValue.id")
    @Named("toProductOptionValueDescriptionDto")
    ProductOptionValueDescriptionDto toProductOptionValueDescriptionDto(ProductOptionValueDescription description);

    @IterableMapping(qualifiedByName = "toProductOptionValueDescriptionDto")
    List<ProductOptionValueDescriptionDto> toProductOptionValueDescriptionsDto(List<ProductOptionValueDescription> descriptions);
}
