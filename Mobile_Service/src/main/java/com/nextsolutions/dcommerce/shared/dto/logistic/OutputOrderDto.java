package com.nextsolutions.dcommerce.shared.dto.logistic;

import java.io.Serializable;

public class OutputOrderDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -897620561635021452L;

	private String code;
	private String qrCode;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getQrCode() {
		return this.qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

}
