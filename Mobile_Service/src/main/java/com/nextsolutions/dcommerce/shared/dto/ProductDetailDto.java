package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class ProductDetailDto implements Serializable {
    @Id
    private Long id;
    private String productName;
    private String urlImage;
    private Double reviewAvg;
    private Integer reviewCount;
    private Long productPrice;
    private Integer productQuantity;
    //    private Integer typePromotion;
    private Long manufacturerId;
    private String manufacturerName;
    private String sellerName;
    //    private String incentiveDescription;
    private String desProduct;
    private Integer productShip;
    private String packingProductCode;
    @Transient
    private List<PackingProductDTO> packingProductList;
    @Transient
    private ArrayList<String> imageList;
}
