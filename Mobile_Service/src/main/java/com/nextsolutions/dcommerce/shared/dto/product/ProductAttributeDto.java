package com.nextsolutions.dcommerce.shared.dto.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductAttributeDto {
    private Long id;
    private Long productId;
    private Long productOptionId;
    private Long productOptionValueId;
}
