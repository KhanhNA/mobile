package com.nextsolutions.dcommerce.shared.dto.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DistributorForm {
    @NotNull(message = "Id is required field")
    private Long id;
    @NotBlank(message = "code is required field")
    private String code;
    @NotNull(message = "walletId is required field")
    private Long walletId;
}
