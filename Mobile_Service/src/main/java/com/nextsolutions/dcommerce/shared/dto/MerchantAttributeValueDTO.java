package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MerchantAttributeValueDTO {
    private Long id;
    private String name;
    private String code;
    private Boolean isSelect;

    private Long attId;
    private Long attValueId;
}
