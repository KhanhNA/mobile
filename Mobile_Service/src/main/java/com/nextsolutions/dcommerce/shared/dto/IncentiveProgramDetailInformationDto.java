package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class IncentiveProgramDetailInformationDto {
    private Long id;
    private Long incentiveProgramId;
    private Long packingId;
    private String packingCode;
    private String productName;
    private BigDecimal packingOriginalPrice;
    private BigDecimal packingSalePrice;
    private Integer discountType;
    private BigDecimal discountPrice;
    private BigDecimal discountPercent;
    private Integer limitedQuantity;
    private Integer sold;
    private Boolean isFlashSale;
    private Long bonusPackingId;
    private Integer requiredMinimumPurchaseQuantity;
    private String createUser;
    private LocalDateTime createDate;
    private String lastModifiedUser;
    private LocalDateTime lastModifiedDate;
    private Integer bonusPackingQuantity;
    private Integer limitedPackingQuantity;
    private PackingProductSearchResult packingProduct;
    private PackingProductSearchResult bonusPackingProduct;
    private List<IncentiveProgramFlashSaleDateDto> flashSaleDates;
}
