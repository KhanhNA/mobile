package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.MerchantGroup;
import com.nextsolutions.dcommerce.shared.dto.form.MerchantGroupForm;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MerchantGroupDescriptionMapper.class})
public interface MerchantGroupMapper {
    MerchantGroupResource toMerchantGroupResource(MerchantGroup merchantGroup);

    @Mapping(target = "attributes", ignore = true)
    MerchantGroupForm toMerchantGroupForm(MerchantGroup merchantGroup);
}
