package com.nextsolutions.dcommerce.shared.constant;

public enum IncentiveType {
    GROUP(1),
    PRODUCT(2),
    COUPON(3),
    FLASH_SALE(4);

    private final int value;

    IncentiveType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
