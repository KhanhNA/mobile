package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IncentiveAppliedDTO {
    private Long incentiveProgramId;
    private String name;
    private Integer incentiveType;
    private String imageUrl;
    private String description;
    private Long productPackingId;
    private Long manufacturerId;
    private Long categoryId;
    private Long productId;
    private Integer discountType;

}
