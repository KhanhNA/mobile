package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.loyalty.LoyaltyDescription;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;

@Data
public class LoyaltyDto {

    public static final Integer ON_WALLET=2;
    public static final Integer ON_ORDER=1;
    /**
     *
     */
    public Long id;

    /**
     *
     */
    public String code;

    /**
     *
     */
    public String name;

    /**
     *
     */
    public Integer status;

    /**
     * Loại khuyến mại: từ ZV01-ZV24
     */
    public Integer type;

    /**
     *
     */
    public String format;

    /**
     *
     */
    public LocalDateTime fromDate;

    /**
     *
     */
    public LocalDateTime toDate;

    /**
     *
     */
    public String description;
    public String imgUrl;
    private Boolean bigSale;

    /**
     *
     */
    public String nameText;

    /**
     *
     */
    public Integer isEdited;

    /**
     *
     */
    public OffsetDateTime createDate;

    /**
     *
     */
    public String createUser;

    /**
     *
     */
    public OffsetDateTime updateDate;

    /**
     *
     */
    public String updateUser;

    /**
     *
     */
    public Integer synAction;

    /**
     *
     */
    public String md5ValidCode;

    /**
     *
     */
    public OffsetDateTime openFromDate;

    /**
     *
     */
    public OffsetDateTime openToDate;

    /**
     *
     */
    public Integer quantiMonthNewOpen;

    /**
     *
     */
    public Integer firstbuyFlag;

    /**
     *
     */
    public Integer newcusFlag;

    /**
     *
     */
    public String firstbuyType;

    /**
     *
     */
    public Integer firstbuyNum;

    /**
     *
     */
    public Integer newcusNumCycle;

    /**
     *
     */
    public Integer ontop;

    /**
     *
     */
    public Integer haveRegulatedToStaff;

    /**
     *
     */
    public Integer haveRegulatedToCust;

    /**
     * Loại incentive: 1- trả trên đơn hàng, 2 - trả vào wallet
     */
    public Integer loyaltyType;
    public List<LoyaltyPackingSaleDto> loyaltyPackingSales;

    public List<LoyaltyLevelDto> loyaltyLevels;
    public List<LoyaltyMerchantTypeDto> merchantTypes;
    public HashMap<String, LoyaltyDescription> descriptions;
}
