package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

@Data
public class ProductOptionValueDescriptionDto {
    private Long id;
    private Long productOptionValueId;
    private String name;
    private Long languageId;
}
