package com.nextsolutions.dcommerce.shared.dto.resource;

import com.nextsolutions.dcommerce.model.PackingProduct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Transient;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecommendSearchResource {
    private Long id;
    private Long recommendId;
    private String name;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime  toDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime fromDate;
    private Integer type;
    private Long status;
    private Integer languageId;
    private String imageUrl;
    //nếu là gợi ý sản phẩm.
    private Long productId;
    private PackingProduct packingProduct;
}
