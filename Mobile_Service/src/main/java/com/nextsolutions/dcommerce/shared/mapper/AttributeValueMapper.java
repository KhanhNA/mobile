package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.AttributeValue;
import com.nextsolutions.dcommerce.shared.dto.attribute.AttributeValueChangeDTO;
import com.nextsolutions.dcommerce.shared.dto.form.AttributeValueForm;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {AttributeValueDescriptionMapper.class})
public interface AttributeValueMapper {
    @Mapping(target = "parentCode", source = "attribute.code")
    AttributeValueForm toAttributeValueForm(AttributeValue attributeValue);

    AttributeValueChangeDTO toAttributeValueForm(AttributeValueForm form);
}
