package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PackingProductFLashSaleDTO {

    private Long productId;
    private Long packingProductId;
    private String packingName;
    private Integer discountType;
    private BigDecimal discountPrice;
    private BigDecimal discountPercent;
    private BigDecimal price;
    private Double orgPrice;
    private String packingUrl;
    private Integer quantity;
    private Boolean zeroToNine;
    private Boolean nineToTwelve;
    private Boolean twelveToFifteen;
    private Boolean fifteenToEighteen;
    private Boolean eighteenToTwentyOne;
    private Boolean twentyOneToTwentyFour;
    private Integer sold;
    private Integer limitedQuantity;

    public void setProductId(Number productId) {
        this.productId = productId.longValue();
    }

    public void setPackingProductId(Number packingProductId) {
        this.packingProductId = packingProductId.longValue();
    }

    public void setDiscountType(Number discountType) {
        this.discountType = discountType.intValue();
    }
}
