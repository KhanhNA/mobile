package com.nextsolutions.dcommerce.shared.dto.logistic;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nextsolutions.dcommerce.utils.SerializeDateHandler;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class LogisticOrderDto implements Serializable {
	private static final long serialVersionUID = -4395487876927122079L;

	private Long orderId;
	private String orderCode;
	private String merchantCode;
	private String merchantName;
	private String fromStoreCode;
	private String phone;
	private BigDecimal amount;
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonFormat(pattern = SerializeDateHandler.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	private LocalDateTime orderDate;
	private List<LogistictOrderDetailDto> orderDetails;
}
