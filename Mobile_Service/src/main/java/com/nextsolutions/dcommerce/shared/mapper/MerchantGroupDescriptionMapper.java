package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.MerchantGroupDescription;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupDescriptionResource;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MerchantGroupDescriptionMapper {
    MerchantGroupDescriptionResource toMerchantGroupDescriptionResource(MerchantGroupDescription description);
}
