package com.nextsolutions.dcommerce.shared.dto.coupon;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class CouponIncentiveDTO {
    private Long id;

    private Long incentiveProgramId;

    private String code;

    private Integer type;

    private BigDecimal requiredMinimumAmount;

    private Long limitedQuantity;

    private BigDecimal discountPercent;

    private BigDecimal discountPrice;

    private BigDecimal limitedPrice;

    private String createUser;

    private LocalDateTime createDate;

    private String incentiveName;

    private String incentiveDescription;

    private String shortDescription;

    private LocalDateTime fromDate;

    private LocalDateTime toDate;
}
