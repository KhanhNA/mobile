package com.nextsolutions.dcommerce.shared.mapper;


import com.nextsolutions.dcommerce.model.Kpi;
import com.nextsolutions.dcommerce.shared.dto.KpiDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KpiMapper {

    @Mapping(target = "descriptionsList", ignore = true)
    KpiDto kpi2KpiDto(Kpi kpi);

    @Mapping(target = "descriptionsList", ignore = true)
    Kpi kpiDtoToKpi(KpiDto kpiDto);

    List<KpiDto> kpisToKpiDtos(List<Kpi> kpis);

}
