package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

@Data
public class AppUnitDto {
    private Long id;
    private String value;
}
