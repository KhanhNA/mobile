package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nextsolutions.dcommerce.utils.DeserializeDateHandler;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class MerchantUpdateStatusDTO {
    @NotNull
    private Long merchantId;

    @NotNull
    private Integer status;

    @JsonDeserialize(using = DeserializeDateHandler.class)
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime lockStartDate;

    @JsonDeserialize(using = DeserializeDateHandler.class)
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime lockEndDate;

    @NotNull
    private Long parentMarchantId;

    private String reason;
}
