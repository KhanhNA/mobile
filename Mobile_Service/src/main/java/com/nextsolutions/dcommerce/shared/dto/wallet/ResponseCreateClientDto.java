package com.nextsolutions.dcommerce.shared.dto.wallet;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Getter
@Setter
public class ResponseCreateClientDto {


    public Long officeId;
            public Long clientId;
            public Long resourceId;
            public Long savingsId;

    @Override
    public String toString() {
        return "ResponseCreateClientDto{" +
                "officeId=" + officeId +
                ", clientId=" + clientId +
                ", resourceId=" + resourceId +
                ", savingsId=" + savingsId +
                '}';
    }
}
