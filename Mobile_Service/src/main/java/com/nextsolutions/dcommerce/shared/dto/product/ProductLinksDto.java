package com.nextsolutions.dcommerce.shared.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductLinksDto {
    private Long id;
    private Long manufacturerId;
    private Long distributorId;
    private Long categoryId;
    private List<Long> relatedProductIds;

    private ProductLinksManufacturerDto manufacturer;
    private ProductLinksDistributorDto distributor;
    private ProductLinksCategoryDto category;
    private List<ProductLinksRelatedProductDto> relatedProducts;
}
