package com.nextsolutions.dcommerce.shared.dto.recommend;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Properties {
    private Integer rating;
}
