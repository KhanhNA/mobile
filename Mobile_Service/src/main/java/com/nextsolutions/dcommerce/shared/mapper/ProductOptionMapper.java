package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.ProductOption;
import com.nextsolutions.dcommerce.shared.dto.ProductOptionDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductOptionMapper {
    ProductOptionDto toProductOptionDto(ProductOption productOption);
}
