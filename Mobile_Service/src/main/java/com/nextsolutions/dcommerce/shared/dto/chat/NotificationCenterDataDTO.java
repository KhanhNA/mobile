package com.nextsolutions.dcommerce.shared.dto.chat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationCenterDataDTO {
    private Long id;
    private String type;
    private String title;
    private String description;
    private String actionTitle;
    private String imgUrl;
    private String actionClick;
    private String packageName;
    private String notificationDate;
}
