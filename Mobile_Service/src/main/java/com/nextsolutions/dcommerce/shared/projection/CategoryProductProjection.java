package com.nextsolutions.dcommerce.shared.projection;

public interface CategoryProductProjection {
    Long getId();
    String getCode();
    String getName();
    Long getLanguageId();
    Integer getLevel();
}
