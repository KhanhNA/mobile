package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import com.nextsolutions.dcommerce.shared.dto.product.PackingProductDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring", uses = {PackingPriceMapper.class})
public interface PackingProductMapper {

//    @Mapping(target = "promotionPrices", source = "packingPrices")
    @Mapping(target = "promotionToDate", source = "toDate")
    @Mapping(target = "promotionPercentSalePrice", source = "discountPercent")
    @Mapping(target = "promotionFromDate", source = "fromDate")
    @Mapping(target = "id", source = "packingProductId")
    @Mapping(target = "productId", source = "productId")
    @Mapping(target = "salePrice", source = "price")
    @Mapping(target = "originalPrice", source = "orgPrice")
    @Mapping(target = "imageUrl", source = "packingUrl")
    @Named("toPackingProductDto")
    PackingProductDto toPackingProductDto(PackingProduct packingProduct);

    @IterableMapping(qualifiedByName = "toPackingProductDto")
    List<PackingProductDto> toPackingProductDtos(List<PackingProduct> packingProducts);

    @Mapping(target = "toDate", source = "promotionToDate")
    @Mapping(target = "fromDate", source = "promotionFromDate")
    @Mapping(target = "discountPercent", source = "promotionPercentSalePrice")
    @Mapping(target = "packingProductId", source = "id")
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "price", source = "salePrice")
    @Mapping(target = "packingUrl", source = "imageUrl")
    @Mapping(target = "orgPrice", source = "originalPrice")
    @Named("toPackingProduct")
    PackingProduct toPackingProduct(PackingProductDto packingProductDto);

    @IterableMapping(qualifiedByName = "toPackingProduct")
    List<PackingProduct> toPackingProducts(List<PackingProductDto> packingProductDtos);

    @Mapping(target = "packingProductCode", source = "code")
    @Mapping(target = "duration", ignore = true)
    @Mapping(target = "productName", ignore = true)
    @Mapping(target = "quantity", ignore = true)
    @Mapping(target = "packingProductId", source = "packingProductId")
    @Mapping(source = "packingUrl", target = "packingUrl")
    PackingProductDTO packingProductToPackingProductDto(PackingProduct packingProduct);
}
