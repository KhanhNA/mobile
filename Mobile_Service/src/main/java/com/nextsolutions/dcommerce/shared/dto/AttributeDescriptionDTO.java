package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AttributeDescriptionDTO implements Serializable {
    private Long id;
    private Long attributeId;
    private String name;
    private Integer languageId;
    private Integer required;
    private List<AttributeValueDescriptionDTO> lstValue;
}
