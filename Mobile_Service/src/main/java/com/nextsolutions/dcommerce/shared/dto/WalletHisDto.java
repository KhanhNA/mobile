package com.nextsolutions.dcommerce.shared.dto;


import java.time.LocalDateTime;

/**
 * - khi thực hiện order thành công thì sẽ đẩy tiền chiết khấu sang ví
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
public class WalletHisDto{

    /**
     * 
     */
    public Long walletHisId;

    /**
     * 
     */
    public Long orderId;

    /**
     * 
     */
    public Double amount;

    /**
     * 
     */
    public LocalDateTime createDate;

    /**
     * 
     */
    public Long walletId;
}
