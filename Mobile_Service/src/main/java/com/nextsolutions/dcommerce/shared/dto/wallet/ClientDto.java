package com.nextsolutions.dcommerce.shared.dto.wallet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)

public class ClientDto {


    public Long officeId;
    public String externalId;
    public String dateFormat = "yyyy-MM-dd";
    public String locale = "vi";
    public Boolean active;
    public Long savingsProductId;

    public String firstname ;

    public String lastname ;


    public String activationDate ;
    public String submittedOnDate ;
    public String accountNo;

    public String toJson() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String dtoAsString = mapper.writeValueAsString(this);
        return dtoAsString;
    }


}
