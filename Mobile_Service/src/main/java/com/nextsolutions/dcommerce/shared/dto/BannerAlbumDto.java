package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.AlbumDetail;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class BannerAlbumDto extends BaseDto {
    public Long albumId;
    public String albumName;
    public LocalDateTime fromDate;
    public LocalDateTime toDate;
    public Integer status;
    public List<AlbumDetail> albumDetails;
    public String image;
    public String linkTarget;
}
