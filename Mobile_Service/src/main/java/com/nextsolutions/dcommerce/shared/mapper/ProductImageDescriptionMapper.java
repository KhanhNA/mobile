package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.ProductImageDescription;
import com.nextsolutions.dcommerce.shared.dto.product.ProductImageDescriptionDto;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductImageDescriptionMapper {

    @Mapping(target = "productImageId", source = "productImage.id")
    @Mapping(target = "name", source = "name")
    @Named("toProductImageDescriptionDto")
    ProductImageDescriptionDto toProductImageDescriptionDto(ProductImageDescription productImageDescription);

    @IterableMapping(qualifiedByName = "toProductImageDescriptionDto")
    List<ProductImageDescriptionDto> toProductImageDescriptionDtos(List<ProductImageDescription> productImageDescriptions);

    @Mapping(target = "updtId", ignore = true)
    @Mapping(target = "productImage", ignore = true)
    @Mapping(target = "dateModified", ignore = true)
    @Mapping(target = "dateCreated", ignore = true)
    @Mapping(target = "altTag", ignore = true)
    @Named("toProductImageDescription")
    ProductImageDescription toProductImageDescription(ProductImageDescriptionDto productImageDescriptionDto);

    @IterableMapping(qualifiedByName = "toProductImageDescription")
    List<ProductImageDescription> toProductImageDescriptions(List<ProductImageDescriptionDto> productImageDescriptionDtos);
}
