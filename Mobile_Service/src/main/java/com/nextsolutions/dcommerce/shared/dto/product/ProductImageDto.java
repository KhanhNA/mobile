package com.nextsolutions.dcommerce.shared.dto.product;

import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class ProductImageDto {
    private Long id;
    private String name;
    private String imageUrl;
    private String type;
    private Boolean crop;
    private boolean defaultImage;
    private Long productId;

    @Valid
    private List<ProductImageDescriptionDto> descriptions;
}
