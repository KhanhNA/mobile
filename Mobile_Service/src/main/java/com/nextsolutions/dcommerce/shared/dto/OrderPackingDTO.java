package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.incentive.IncentivePackingSaleResult;
import com.nextsolutions.dcommerce.model.incentive.IncentiveLevel;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.HashMap;

@Data
public class OrderPackingDTO {
    public static final int INCENTIVE = 1;
    public static final int SALE = 0;
    private Long orderPackingId;
    private String productName;
    private Integer quantity; //số lượng packing

    private Double orderQuantity;// số lượng mua
    private Long incentiveProgramId;
    private Double price;//gia
    private Double vat;
    private Long packingProductId;
    private Double amount = (double) 0;

    private Long productId;
    //private Double incentiveQuantity=(double)0;
    private Double incentiveAmount = (double) 0;

    private Double incentiveCoupon = (double) 0;

    private Integer remainQuantity =  0;
    private Double remainAmount = (double) 0;

    private Double incentiveTotal = (double) 0;
    ;
    private Integer isIncentive;
    private double orderPercent;//% dong gop vao don hang
    public String packingCode;
    public String packingName;
    public String packingUrl;
    public String packingProductCode;
    private int isDefault;
    public Double requireQuantity = 0d; //so luong dang du kien
    public String distributorCode;
    public Long distributorId;
    public Long distributorWalletId;
    public HashMap<String, IncentivePackingSaleResult> ictPackingResults;

    public void addIncentiveAmount(Double incenAmount) {
        if (incentiveAmount == null) {
            incentiveAmount = (double) 0;
        }
        incentiveAmount += incenAmount;
        if (amount - incenAmount > 0) {
            amount -= incenAmount;
        } else {
            amount = 0d;
        }

    }

    public void addIncentiveTotal(Double amount) {
        if (incentiveTotal == null) {
            incentiveTotal = (double) 0;
        }
        incentiveTotal += amount;
    }

    public void updateIctPackingResult(IncentiveLevel ictLevel, Long mercchantL1Id) {
        if (ictPackingResults == null) {
            ictPackingResults = new HashMap<>();
        }
        IncentivePackingSaleResult packingSaleResult = ictPackingResults.get(ictLevel.getIncentivePackingSaleId() + "-" + packingProductId);
        if (packingSaleResult == null) {
            packingSaleResult = new IncentivePackingSaleResult();
        }
        packingSaleResult.setPackingProductId(packingProductId);
        packingSaleResult.setIncentivePackingSaleId(ictLevel.getIncentivePackingSaleId());
        packingSaleResult.setIncentiveProgramId(ictLevel.getIncentiveProgramId());
        packingSaleResult.setTotalOrderAmount(amount);
        packingSaleResult.setTotalOrderQuantity(quantity);
        packingSaleResult.setMerchantL1Id(mercchantL1Id);
        ictPackingResults.put(ictLevel.getIncentivePackingSaleId() + "-" + packingProductId, packingSaleResult);

    }


}
