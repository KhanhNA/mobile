package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CurrentTimeFlashSaleDTO {
    private long duration;
    private String sqlWhere;
    private String milestone;
    private List<String> timeline = new ArrayList<>();
}
