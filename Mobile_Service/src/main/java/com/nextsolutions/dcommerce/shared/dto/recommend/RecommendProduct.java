package com.nextsolutions.dcommerce.shared.dto.recommend;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RecommendProduct {
    String event;
    String entityType;
    String entityId;
    String targetEntityType;
    String targetEntityId;
    String eventTime;
    Properties properties;
}
