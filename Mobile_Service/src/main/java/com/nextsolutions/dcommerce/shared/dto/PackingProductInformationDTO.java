package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
public interface PackingProductInformationDTO {
    Long getPackingProductId();

    String getPackingProductCode();

    String getPackingUrl();

    Long getQuantity();

    BigDecimal getPrice();

    BigDecimal getOrgPrice();

    Long getProductId();

    BigDecimal getDiscountPercent();

    BigDecimal getDiscountPrice();

    Double getMarketPrice();

    Integer getSold();

    Integer getLimitedQuantity();

    Integer getDiscountType();
}
