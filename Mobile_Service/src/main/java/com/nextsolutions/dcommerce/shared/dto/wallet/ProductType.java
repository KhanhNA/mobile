package com.nextsolutions.dcommerce.shared.dto.wallet;

public enum ProductType {
    WALLET(3),
    POINT(4),
    LOYALTY(5);

    int value;

    ProductType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
