package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

@Data
public class PackingTypeDto {
    private Long id;
    private String code;
    private Integer quantity;
}
