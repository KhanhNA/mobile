package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.ProductAttribute;
import com.nextsolutions.dcommerce.shared.dto.product.ProductAttributeDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductAttributeMapper {

    @Named("toProductAttributeDto")
    ProductAttributeDto toProductAttributeDto(ProductAttribute productAttribute);

    @IterableMapping(qualifiedByName = "toProductAttributeDto")
    List<ProductAttributeDto> toProductAttributesDto(List<ProductAttribute> productAttribute);
}
