package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IncentiveDescriptionDTO {
    private Long incentiveProgramId;
    private String name;
    private Integer incentiveType;
    private String imageUrl;
    private Long productPackingId;
    private Long manufacturerId;
    private Long categoryId;
    private Long productId;
    private String shortDescription;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private String description;

}
