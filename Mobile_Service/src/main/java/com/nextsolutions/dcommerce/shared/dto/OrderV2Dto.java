package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nextsolutions.dcommerce.model.order.OrderPackingV2;
import com.nextsolutions.dcommerce.shared.dto.incentivev2.OrderPackingV2DTO;
import com.nextsolutions.dcommerce.utils.DeserializeDateHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author PhamBien
 * Create at 2020-03-13 13:41
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class OrderV2Dto implements Serializable {
    public Long orderId;
    /**
     * Mã đơn hàng
     */
    public String orderNo;

    public Long merchantId;

    public String merchantFullName;

    public String merchantImgUrl;

    /**
     * isaveOrder = 0, no save order,1:save order
     */
    public Integer isSaveOrder;

    public Long merchantTypeId;

    public String merchantName;

    public String phone;

    private Long paymentMethodId;

    private Long shipMethodId;

    public Long shpCartId;

    public BigDecimal amount = new BigDecimal(0);

    public BigDecimal orgAmount = new BigDecimal(0);

    public BigDecimal incentiveCoupon = new BigDecimal(0);

    public BigDecimal vat;

    public BigDecimal incentiveAmount = new BigDecimal(0);

    public Long merchantAddressId;

    public String recvAddr;

    public Integer orderStatus;

    public Double remainAmount;

    public Integer langId;

    public Long parentMerchantId;

    public String paymentMethodName;
    @JsonDeserialize(using = DeserializeDateHandler.class)
    public LocalDateTime deliveryDate;

    public String deliveryDateStr;

    public LocalDateTime orderDate;


    public String merchantCode;

    public List<OrderPackingV2DTO> orderPackings;

    public List<OrderPackingV2> calculatePackings = new ArrayList<>();

    public List<OrderPackingV2> incentivePackings = new ArrayList<>();

    public List<Long> mapIncentives;

    public Integer deliveryStatus;

    public Integer paymentStatus;

    public String reason;

    public String merchantWareHouseCode;
    public String merchantWareHouseAddress;

    public String logisticCode;

    public String logisticQRCode;

    public Integer totalOrderProductQuantity; //Tong sp trong order


    public Integer orderType;


    //for ui item order
    public String packingUrl;
    public String productName;
    public Double orgPrice;
    public Double price;
    public Integer orderQuantity; //So luong sp dat
    //for ui item order template
    public String orderName;
    public Long orderTemplateId;
    public List<String> lstCouponCode;
    public String tokenMifos;

    public void addOrderIncentive(Long incentiveId) {
        if (this.mapIncentives == null) {
            this.mapIncentives = new ArrayList<>();
        }
        this.mapIncentives.add(incentiveId);
    }
}
