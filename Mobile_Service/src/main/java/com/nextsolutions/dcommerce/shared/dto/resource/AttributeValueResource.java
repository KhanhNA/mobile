package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

@Data
public class AttributeValueResource {
    private Long id;
    private String code;
    private String name;
    private Long languageId;
}
