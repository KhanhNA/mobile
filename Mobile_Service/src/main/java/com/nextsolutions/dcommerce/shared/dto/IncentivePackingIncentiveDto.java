// Generated with g9.

package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.incentive.IncentiveLevel;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class IncentivePackingIncentiveDto implements Serializable {

    private Long id;
    private Long incentiveLevelId;
    private Long packingProductId;
    private String uom;

    private Long isRequired;
    private Long status;
    private LocalDateTime createDate;
    private String createUser;
    private LocalDateTime updateDate;
    private String updateUser;
    private Long incentiveProgramId;
    private Long productId;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;

    private Double quantity;
    private Double amount;
    private Double price;

	private IncentiveLevel incentiveLevel;


    /** Default constructor. */
    public IncentivePackingIncentiveDto() {
        super();
    }

    

}
