package com.nextsolutions.dcommerce.shared.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationMerchantDTO {
    private Long id;
    private Long actionId;
    private Long merchantReceiverId;
    private Long senderId;
    private Integer typeId;
    private LocalDateTime createDate;
    private Integer isSeen;
    private String orderProductName;
    private Integer orderQuantity;
    private String orderPackingUrl;
    private String token;
    private String topic;
    private String click_action;
    private String merchantName;
    private String merchantNumberPhone;
    private String orderCode;
}
