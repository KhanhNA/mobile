package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.model.AttributeDescription;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttributeDto {
    private Long id;
    private String code;
    private String name;
    private Integer type;
    private Integer request;
    private Integer statusProcess;
    private String value;
    private Integer languageId;
}
