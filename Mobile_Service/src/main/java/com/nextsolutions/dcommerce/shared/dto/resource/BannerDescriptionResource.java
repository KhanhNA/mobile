package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

@Data
public class BannerDescriptionResource {
    private Long id;
    private String name;
    private Long languageId;
}
