package com.nextsolutions.dcommerce.shared.constant;

public enum PriceType {
    SALE_OUT(2),
    SALE_INT(1);

    private Integer value;

    PriceType(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return this.value;
    }
}
