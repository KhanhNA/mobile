package com.nextsolutions.dcommerce.shared.projection;

import java.math.BigDecimal;

public interface IncentivePackingProductProjection {
    Long getPackingId();
    String getPackingCode();
    String getProductName();
    BigDecimal getPackingOriginalPrice();
    BigDecimal getPackingSalePrice();
}
