package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
public class KpiMonthDto implements Serializable {
    @Id
    private Long id;
    private String kpiName;
    private LocalDateTime month;
    private Long kpiId;
    private Long merchantId;
    private Double plan;
    private Double done;
    private Double bonus;
}
