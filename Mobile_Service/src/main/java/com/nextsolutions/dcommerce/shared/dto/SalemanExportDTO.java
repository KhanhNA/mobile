package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalemanExportDTO {
    private String code;
    private String username;
    private String fullName;
    private String phoneNumber;
    private String address;
    private String registerDate;
    private Integer status;
    private String parent;
    private String idNumber;
    private String contractNumber;
    private String bankAccountNo;
    private String clientWalletId;
}
