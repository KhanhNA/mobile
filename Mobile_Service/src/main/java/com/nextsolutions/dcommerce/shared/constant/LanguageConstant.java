package com.nextsolutions.dcommerce.shared.constant;

public enum LanguageConstant {
    VIETNAMESE(1, "vi"), ENGLISH(2, "en"), BURMESE(3, "my");

    private long value;
    private String code;

    LanguageConstant(int value, String code) {
        this.value = value;
        this.code = code;
    }

    public long value() {
        return value;
    }

    public String code() {
        return code;
    }
}
