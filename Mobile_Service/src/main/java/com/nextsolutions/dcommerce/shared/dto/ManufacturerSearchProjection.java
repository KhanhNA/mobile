package com.nextsolutions.dcommerce.shared.dto;

public interface ManufacturerSearchProjection {
    Long getId();
    String getCode();
    String getName();
}
