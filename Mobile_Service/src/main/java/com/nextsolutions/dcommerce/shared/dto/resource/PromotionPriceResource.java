package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PromotionPriceResource {
    private Long id;
    private Long packingProductId;
    private Double price;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private Integer priceType;
    private String currencyCode;
}
