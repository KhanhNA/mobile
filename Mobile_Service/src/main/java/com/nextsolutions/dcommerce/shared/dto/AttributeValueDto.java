package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttributeValueDto {
    private Long id;
    private String parentCode;
    private String code;
    private String name;
    private Integer request;
    private Integer statusProcess;
    private String value;
}
