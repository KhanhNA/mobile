/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.nextsolutions.dcommerce.shared.dto.wallet;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Immutable data object representing a savings account.
 */
@Data
public class SavingsAccountData implements Serializable {

    public Long id;
    public String accountNo;

    public String externalId;
    public Long groupId;
    public String groupName;
    public Long clientId;
    public String clientName;
    public Long savingsProductId;
    public String savingsProductName;
    public Long fieldOfficerId;
    public String fieldOfficerName;
    public SavingsAccountStatusDto status;
//    public SavingsAccountStatusEnumData status;
//    public SavingsAccountSubStatusEnumData subStatus;
//    public SavingsAccountApplicationTimelineData timeline;
//    public CurrencyData currency;
//    public BigDecimal nominalAnnualInterestRate;
//    public EnumOptionData interestCompoundingPeriodType;
//    public EnumOptionData interestPostingPeriodType;
//    public EnumOptionData interestCalculationType;
//    public EnumOptionData interestCalculationDaysInYearType;
//    public BigDecimal minRequiredOpeningBalance;
    public Integer lockinPeriodFrequency;
//    public EnumOptionData lockinPeriodFrequencyType;
    public boolean withdrawalFeeForTransfers;
    public boolean allowOverdraft;
    public BigDecimal overdraftLimit;
    public BigDecimal minRequiredBalance;
    public boolean enforceMinRequiredBalance;
    public BigDecimal minBalanceForInterestCalculation;
    public BigDecimal onHoldFunds;
    public boolean withHoldTax;
//    public TaxGroupData taxGroup;
//    public LocalDate lastActiveTransactionDate;
    public boolean isDormancyTrackingActive;
    public Integer daysToInactive;
    public Integer daysToDormancy;
    public Integer daysToEscheat;
    public BigDecimal savingsAmountOnHold;

    // associations
//    public SavingsAccountSummaryData summary;
//    @SuppressWarnings("unused")
//    public Collection<SavingsAccountTransactionData> transactions;

//    public Collection<SavingsAccountChargeData> charges;

    // template
//    public Collection<SavingsProductData> productOptions;
//    public Collection<StaffData> fieldOfficerOptions;
//    public Collection<EnumOptionData> interestCompoundingPeriodTypeOptions;
//    public Collection<EnumOptionData> interestPostingPeriodTypeOptions;
//    public Collection<EnumOptionData> interestCalculationTypeOptions;
//    public Collection<EnumOptionData> interestCalculationDaysInYearTypeOptions;
//    public Collection<EnumOptionData> lockinPeriodFrequencyTypeOptions;
//    public Collection<EnumOptionData> withdrawalFeeTypeOptions;
//    public Collection<ChargeData> chargeOptions;

//    @SuppressWarnings("unused")
//    public SavingsAccountChargeData withdrawalFee;
//    @SuppressWarnings("unused")
//    public SavingsAccountChargeData annualFee;
    public BigDecimal nominalAnnualInterestRateOverdraft;
    public BigDecimal minOverdraftForInterestCalculation;

//    private List<DatatableData> datatables = null;

    //import field
    private Long productId;
    private String locale;
    private String dateFormat;
    private transient Integer rowIndex;
//    private LocalDate submittedOnDate;

    public SavingsAccountData() {
    }
}
