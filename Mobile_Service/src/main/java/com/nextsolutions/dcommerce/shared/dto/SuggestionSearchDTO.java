package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuggestionSearchDTO {
    private String productName;
    private Long packingProductId;
    private Long productId;
    private Integer quantity;
}
