// Generated with g9.

package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Data
public class LoyaltyLevelDto implements Serializable {

	public static final Long SALE=0L;
	public static final Long LOYALTY=1L;
	
    

    private Long id;

    private String groupText;

    private Long orderNumber;

    private int status;

    private LocalDateTime createDate;

    private String createUser;

    private LocalDateTime updateDate;

    private String updateUser;

    private Long minQuantity;
    

    private Long minAmount;
    
    

    private Long loyaltyId;
    

    private Double loyaltyAmount;

    private Double loyaltyAmountMax;
    

    private Double loyaltyPercent;
    

    private Double loyaltyPercentMax;

    private Integer isMultiple;
    private Double totalValue; //tong gia tri cua muc khuyen mai (dung de chon km nao tot nhat)

    private Set<LoyaltyPackingSaleDto> loyaltyPackingSale;

    private LoyaltyDto loyalty;



}
