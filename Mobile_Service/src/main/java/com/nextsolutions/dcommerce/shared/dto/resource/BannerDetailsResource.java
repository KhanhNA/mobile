package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

@Data
public class BannerDetailsResource {
    private Long id;
    private String linkTarget;
    private Long languageId;
    private String description;
    private String imageUrl;
}
