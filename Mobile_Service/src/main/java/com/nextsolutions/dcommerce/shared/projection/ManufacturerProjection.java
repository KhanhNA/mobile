package com.nextsolutions.dcommerce.shared.projection;

public interface ManufacturerProjection {
    String getId();
    String getCode();
    String getName();
}
