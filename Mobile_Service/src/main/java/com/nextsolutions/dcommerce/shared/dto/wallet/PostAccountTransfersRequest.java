package com.nextsolutions.dcommerce.shared.dto.wallet;

import io.swagger.annotations.ApiModelProperty;

public class PostAccountTransfersRequest {
    public PostAccountTransfersRequest() {
    }

    @ApiModelProperty(example = "1")
    public Long fromOfficeId;
    @ApiModelProperty(example = "1")
    public Long fromClientId;
    @ApiModelProperty(example = "2")
    public Long fromAccountType;
    @ApiModelProperty(example = "1")
    public Long fromAccountId;
    @ApiModelProperty(example = "1")
    public Long toOfficeId;
    @ApiModelProperty(example = "1")
    public Long toClientId;
    @ApiModelProperty(example = "2")
    public Long toAccountType;
    @ApiModelProperty(example = "2")
    public Long toAccountId;
    @ApiModelProperty(example = "yyyy-MM-dd")
    public String dateFormat = "yyyy-MM-dd";
    @ApiModelProperty(example = "vi")
    public String locale = "vi";
    @ApiModelProperty(example = "01 August 2011")
    public String transferDate;
    @ApiModelProperty(example = "112.45")
    public Double transferAmount;
    @ApiModelProperty(example = "A description of the transfer")
    public String transferDescription;
}
