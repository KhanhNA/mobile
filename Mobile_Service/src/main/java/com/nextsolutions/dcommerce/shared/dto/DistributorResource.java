package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
public class DistributorResource {

    private Long id;

    @NotBlank(message = "Code is required field")
    @Length(max = 10, message = "Code is limited at 10 characters")
    private String code;

    @NotBlank(message = "name is required field")
    @Length(max = 255, message = "name is limited at 255 characters")
    private String name;

    @NotBlank(message = "username is required field")
    @Length(max = 45, message = "username is limited at 45 characters")
    private String username;

    private Long walletId;

    @Length(max = 255, message = "Address is limited at 255 characters")
    private String address;

    @NotBlank(message = "phoneNumber is required field")
    @Length(max = 15, message = "phoneNumber is limited at 15 characters")
    private String phoneNumber;

    @NotBlank(message = "email is required field")
    @Length(max = 45, message = "email is limited at 45 characters")
    private String email;

    private Boolean enabled;

    @Length(max = 5000, message = "About us is limited at 5000 characters")
    private String aboutUs;

    private LocalDateTime createdTime;

    private LocalDateTime latestModified;

}
