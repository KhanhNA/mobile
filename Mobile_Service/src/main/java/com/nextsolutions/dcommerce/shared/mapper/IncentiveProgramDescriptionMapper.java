package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.IncentiveProgramDescription;
import com.nextsolutions.dcommerce.shared.dto.incentive.IncentiveProgramDescriptionDto;
import com.nextsolutions.dcommerce.ui.model.incentive.CreateIncentiveProgramDescriptionRequest;
import com.nextsolutions.dcommerce.ui.model.incentive.IncentiveProgramDescriptionResponse;
import com.nextsolutions.dcommerce.ui.model.incentive.UpdateIncentiveProgramDescriptionRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IncentiveProgramDescriptionMapper {
    IncentiveProgramDescriptionDto toIncentiveProgramDescriptionDto(CreateIncentiveProgramDescriptionRequest source);

    IncentiveProgramDescription toIncentiveProgramDescription(IncentiveProgramDescriptionDto source);

    IncentiveProgramDescriptionDto toIncentiveProgramDescriptionDto(IncentiveProgramDescription source);

    IncentiveProgramDescriptionResponse toIncentiveProgramDescriptionResponse(IncentiveProgramDescriptionDto source);

    IncentiveProgramDescriptionDto toIncentiveProgramDescriptionDto(UpdateIncentiveProgramDescriptionRequest source);
}
