package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

public interface PackingSupperSaleDTO {
    Long getProductId();

    String getPackingUrl();

    Long getPackingProductId();

    String getPackingProductCode();

    BigDecimal getPrice();

    BigDecimal getDiscountPercent();

    BigDecimal getDiscountPrice();

    Double getOrgPrice();

    Double getMarketPrice();

    Integer getQuantity();

    Integer getDuration();

    String getProductName();

    Integer getSold();

    Integer getLimitedQuantity();

    Integer getDiscountType();
}
