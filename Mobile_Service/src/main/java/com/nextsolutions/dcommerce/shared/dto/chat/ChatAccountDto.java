package com.nextsolutions.dcommerce.shared.dto.chat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;

/**
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChatAccountDto {

    public String username;
    public String pass;
    public String name;
    public String email;

    public String toJson() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsString(this);
    }
}
