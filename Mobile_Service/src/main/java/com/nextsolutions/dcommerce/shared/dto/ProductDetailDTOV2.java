package com.nextsolutions.dcommerce.shared.dto;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailDTOV2 implements Serializable {
    private Long productId;
    private String productName;
    private String urlImage;
    private Float reviewAvg;
    private Integer reviewCount;
    private BigDecimal productPrice;
    private Long quantity;
    private Long manufacturerId;
    private String manufacturerName;
    private String manufacturerImg;
    private String sellerName;
    private String desProduct;
    private List<PackingProductDTO> packingProductList = new ArrayList<>();
    private List<String> imageList = new ArrayList<>();
    // Danh sach KM order dang duoc ap dung
    private List<IncentiveAppliedDTO> mapIncentives = new ArrayList<>();
}
