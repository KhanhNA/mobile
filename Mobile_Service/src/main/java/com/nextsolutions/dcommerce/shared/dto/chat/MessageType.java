package com.nextsolutions.dcommerce.shared.dto.chat;

public enum MessageType {
    ORDER("ORD"), PROMOTION("PRM"),
    INVITATION("INV");
    String value;

    MessageType(String str) {
        this.value = str;
    }

    public String getValue() {
        return value;
    }
}
