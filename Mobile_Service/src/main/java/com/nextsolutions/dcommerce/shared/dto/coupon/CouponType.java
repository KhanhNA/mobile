package com.nextsolutions.dcommerce.shared.dto.coupon;

public enum CouponType {
    DISCOUNT_PERCENT(1), DISCOUNT_PRICE(2);
    private int value;

    CouponType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
