package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

@Data
public class ProductOptionDescriptionDto {
    private Long id;
    private String name;
    private Long languageId;
}
