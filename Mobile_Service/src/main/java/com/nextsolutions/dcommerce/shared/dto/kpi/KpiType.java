package com.nextsolutions.dcommerce.shared.dto.kpi;

public enum KpiType {

    BONUS_SALES(2), BONUS_ACQUIRE_L2(4);

    private Integer value;

    KpiType(int v) {
        this.value = v;
    }

    public Integer value() {
        return value;
    }
}
