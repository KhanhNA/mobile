package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.ProductImage;
import com.nextsolutions.dcommerce.shared.dto.product.ProductImageDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ProductImageDescriptionMapper.class})
public interface ProductImageMapper {

    @Mapping(target = "productId", source = "product.id")
    @Mapping(target = "imageUrl", source = "productImageUrl")
    @Mapping(target = "type", source = "imageType")
    @Mapping(target = "descriptions", source = "imageDescriptions")
    @Mapping(target = "crop", source = "imageCrop")
    @Mapping(target = "defaultImage", source = "defaultImage")
    @Named("toProductImageDto")
    ProductImageDto toProductImageDto(ProductImage productImage);

    @IterableMapping(qualifiedByName = "toProductImageDto")
    List<ProductImageDto> toProductImageDtos(List<ProductImage> productImages);

    @Mapping(target = "product", ignore = true)
    @Mapping(target = "productImageUrl", source = "imageUrl")
    @Mapping(target = "imageType", source = "type")
    @Mapping(target = "imageDescriptions", ignore = true)
    @Mapping(target = "imageCrop", source = "crop")
    @Named("toProductImage")
    ProductImage toProductImage(ProductImageDto productImageDto);

    @IterableMapping(qualifiedByName = "toProductImage")
    List<ProductImage> toProductImages(List<ProductImageDto> productImageDtos);

}
