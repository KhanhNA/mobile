package com.nextsolutions.dcommerce.shared.dto;

import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface OrderTemplateDetailDTO {
    Long getOrderId();

    String getOrderName();

    LocalDateTime getOrderDate();

    BigDecimal getAmount();

    Long getOrderPackingId();

    Long getPackingProductId();

    Integer getOrderQuantity();

    String getPackingUrl();

    BigDecimal getPrice();

    BigDecimal getOrgPrice();

    String getProductName();

    Long getOrderTemplateId();

}
