package com.nextsolutions.dcommerce.shared.dto.form;

import com.nextsolutions.dcommerce.shared.dto.resource.AttributeValueDescriptionResource;
import lombok.Data;

import java.util.List;

@Data
public class AttributeValueForm {
    private Long id;
    private Long attributeId;
    private String parentCode;
    private String code;
    private Integer request;
    private Integer statusProcess;
    private String value;
    private List<AttributeValueDescriptionResource> descriptions;
}
