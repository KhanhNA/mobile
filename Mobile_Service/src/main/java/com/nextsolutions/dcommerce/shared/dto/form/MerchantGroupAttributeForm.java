package com.nextsolutions.dcommerce.shared.dto.form;

import lombok.Data;

@Data
public class MerchantGroupAttributeForm {
    private Long id;
    private Long merchantGroupId;
    private Long merchantGroupAttributeId;
    private Long merchantGroupAttributeValueId;
}
