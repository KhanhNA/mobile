package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.*;
import com.nextsolutions.dcommerce.shared.dto.ProductDataDto;
import com.nextsolutions.dcommerce.shared.dto.ProductDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.ProductGeneralDto;
import com.nextsolutions.dcommerce.shared.dto.product.*;
import com.nextsolutions.dcommerce.shared.dto.product.packing.PackingProductDto;
import com.nextsolutions.dcommerce.ui.model.product.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductManagementMapper {
    @Mapping(target = "id", ignore = true)
    ProductGeneralDto toProductGeneralDto(PostProductGeneralRequestModel source);

    @Mapping(target = "internationalName", source = "name")
    @Mapping(target = "descriptions", source = "productDescriptions")
    ProductGeneralDto toProductGeneralDto(Product source);

    PostProductGeneralResponseModel toPostProductGeneralResponseModel(ProductGeneralDto source);

    @Mapping(target = "name", source = "internationalName")
    @Mapping(target = "productDescriptions", source = "descriptions")
    Product toProduct(ProductGeneralDto source);

    @Mapping(target = "id", ignore = true)
    ProductGeneralDto toProductGeneralDto(PutProductGeneralRequestModel source);

    PutProductGeneralResponseModel toPutProductGeneralResponseModel(ProductGeneralDto source);

    GetProductGeneralResponseModel toGetProductGeneralResponseModel(ProductGeneralDto source);

    @Mapping(target = "isSynchronization", ignore = true)
    @Mapping(target = "product", ignore = true)
    @Mapping(target = "metaDescription", source = "metaTagDescription")
    @Mapping(target = "metaKeywords", source = "metaTagKeywords")
    @Mapping(target = "metaTitle", source = "metaTagTitle")
    @Mapping(target = "productName", source = "localizedName")
    ProductDescription toProductDescription(ProductDescriptionDto source);

    @Mapping(target = "productTags", ignore = true)
    @Mapping(target = "metaTagTitle", source = "metaTitle")
    @Mapping(target = "metaTagKeywords", source = "metaKeywords")
    @Mapping(target = "metaTagDescription", source = "metaDescription")
    @Mapping(target = "localizedName", source = "productName")
    ProductDescriptionDto productDescriptionToProductDescriptionDto(ProductDescription source);

    @Mapping(target = "id", ignore = true)
    ProductDataDto toProductDataDto(PutProductDataRequestModel source);

    PutProductDataResponseModel toPutProductDataResponseModel(ProductDataDto source);

    ProductDataDto toProductDataDto(Product product);

    GetProductLinksResponseModel toGetProductLinksResponseModel(ProductLinksDto productLinksDto);

    ProductLinksDto toProductLinksDto(ProductLinks productLinks);

    PutProductLinksResponseModel toPutProductLinksResponseModel(ProductLinksDto productLinksDto);

    ProductLinksDto toProductLinksDto(PutProductLinksRequestModel requestModel);

    ProductLinks toProductLinks(ProductLinksDto productLinksDto);

    GetProductDataResponseModel toGetProductDataResponseModel(ProductDataDto productDataDto);

    ProductLinksManufacturerDto toProductLinksManufacturerDto(ManufacturerLink manufacturerLink);

    GetProductLinksManufacturerResponseModel toGetProductLinksManufacturerResponseModel(ProductLinksManufacturerDto source);

    ProductLinksDistributorDto toProductLinksDistributorDto(DistributorLink source);

    GetProductLinksDistributorResponseModel toGetProductLinksDistributorResponseModel(ProductLinksDistributorDto source);

    ProductLinksCategoryDto toProductLinksCategoryDto(CategoryLink source);

    GetProductLinksCategoryResponseModel toGetProductLinksCategoryResponseModel(ProductLinksCategoryDto source);

    ProductLinksRelatedProductDto toProductLinksRelatedProductDto(RelatedProduct source);

    GetProductLinksRelatedProductResponseModel toGetProductLinksRelatedProductResponseModel(ProductLinksRelatedProductDto source);

    ProductOptionsDto toProductOptionsDto(ProductOptions source);

    List<ProductOptionsDto> toProductOptionsDtos(List<ProductOptions> source);

    GetProductOptionsResponseModel toGetProductOptionsResponseModel(ProductOptionsDto source);

    ProductOptionsDto toProductOptionsDto(PutProductOptionRequestModel source);

    ProductOptions toProductOptions(ProductOptionsDto productOptionsDto);

    @Mapping(target = "salePrice", source = "price")
    @Mapping(target = "imageUrl", source = "packingUrl")
    @Mapping(target = "id", source = "packingProductId")
    PackingProductDto toPackingProductDto(PackingProduct packingProduct);

    GetPackingProductResponseModel toGetPackingProductResponseModel(PackingProductDto source);
}
