package com.nextsolutions.dcommerce.shared.dto.incentivev2;

import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import com.nextsolutions.dcommerce.shared.dto.PackingProductInformationDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductIncentiveInformationDTO implements Serializable {
    private Long id;
    private Long productId;
    private Long packingProductId;
    private String productName;
    private String packingProductCode;
    private String urlImage;
    private Float reviewAvg;
    private Integer reviewCount;
    private BigDecimal productPrice;
    private Double orgPrice;
    private Long quantity;
    private Long manufacturerId;
    private BigDecimal discountPercent;
    private String manufacturerName;
    private String sellerName;
    private String desProduct;
    private List<PackingProductDTO> packingProductList = new ArrayList<>();
}
