package com.nextsolutions.dcommerce.shared.dto.wallet;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Getter
@Setter
public class SavingsAccountStatusDto {


    public Long id = 1L;
    public String code = "1";
    public Boolean approved = true;
    public Boolean rejected = true;
    public Boolean active = true;
    public Boolean closed = true;

}
