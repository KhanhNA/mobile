package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.util.List;

@Data
public class ProductOptionValueDto {
    private Long id;
    private Long productOptionId;
    private String code;
    private List<ProductOptionValueDescriptionDto> descriptions;
}
