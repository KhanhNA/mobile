package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

@Data
public class ProductDescriptionResource {
    private Long id;
    private Long languageId;
    private String productName;
    private String description;
}
