package com.nextsolutions.dcommerce.shared.dto.attribute;

import lombok.Data;

import java.util.List;

@Data
public class AttributeChangeDTO {
    private Long id;
    private String code;
    private String name;
    private Integer type;
    private Integer status;
    private Integer request;
    private Integer statusProcess;
    private String value;
    private List<AttributeDescriptionChangeDTO> descriptions;
}

