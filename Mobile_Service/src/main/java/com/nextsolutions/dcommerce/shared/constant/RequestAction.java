package com.nextsolutions.dcommerce.shared.constant;

public enum RequestAction {
    CREATE, UPDATE
}
