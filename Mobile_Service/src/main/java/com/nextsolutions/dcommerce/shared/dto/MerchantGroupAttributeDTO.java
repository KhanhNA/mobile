package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MerchantGroupAttributeDTO {
    private List<Long> merchantGroupsId;
    private List<AttributeDescriptionDTO> attValues;
}
