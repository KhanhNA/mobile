package com.nextsolutions.dcommerce.shared.dto.wallet;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransferDetailsRequestBuilder {
    private Long distributorWalletId;
    private Double amount;
    private Integer productId;
}
