package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.util.List;
@Data
public class DataFlashSaleDTO {
    List<PackingProductFLashSaleDTO> lstPacking;
    long duration;

}
