package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HunterResource {
    private Long id;
    private String code;
    private String username;
    private String fullName;
    private String address;
    private String phoneNumber;
    private LocalDateTime registerDate;
    private Integer action;
    private Integer status;
}
