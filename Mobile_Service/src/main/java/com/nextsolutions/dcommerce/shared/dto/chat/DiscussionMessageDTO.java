package com.nextsolutions.dcommerce.shared.dto.chat;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DiscussionMessageDTO {
    private List<MessageDTO> messages;
    private Integer total;
    private Integer totalPage;

    public Integer getTotalPage() {
        return (total % 20) <= 0 ? 1 : (total / 20) + 1;
    }
}
