package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrouponRegistryStatusDTO {
    private Long id;
    private Double totalAmount;
    private Integer totalQuantity;
    private Double totalAmountNext;
    private Double totalQuantityNext;
    private Double incentiveAmount;
    private Double incentivePercent;
    private Double incentiveAmountNext;
    private Double incentivePercentNext;
    private Long packingProductId;

    private Long productId;
    private String productName;
    private Double price;
    private String url;
    private Double quantity;
}
