package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MerchantOnMapDTO {
    private Long merchantId;
    private String merchantImgUrl;
    private String address;
    private String mobilePhone;
    private Integer status;
    private String fullName;
    private String userName;
    public Double lat;
    public Double longitude;

}
