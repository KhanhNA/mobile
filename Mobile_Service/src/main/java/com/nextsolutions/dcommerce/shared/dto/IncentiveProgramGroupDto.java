package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.List;

@Data
public class IncentiveProgramGroupDto {
    private Long id;
    private Long incentiveProgramId;
    private Long categoryId;
    private String categoryCode;
    private String categoryName;
    private Long manufacturerId;
    private String manufacturerCode;
    private String manufacturerName;
    private Integer discountType;
    private BigDecimal discountPrice;
    private BigDecimal discountPercent;
    private BigDecimal defaultLimitedQuantity;
    private List<IncentiveProgramDetailInformationDto> incentiveProgramDetailInformationGroup;
    private List<PackingProductSearchResult> packingProductDetails;
}
