package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SalesmanDto {
    private Long salesmanId;
    private String salesmanCode;
    private String phoneNumber;
    private String username;
    private String fullName;
    private Integer status;
    private String email;
    private Integer gender;
    private String address;
    private String avatar;
    private String identityNumber;
    private String identityImgFront;
    private String identityImgBack;
    private LocalDateTime birthDate;
    private LocalDateTime createDate;
}
