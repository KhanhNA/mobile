package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;

@Data
public class CurrencyDTO {
    private Long id;

    private String code;

    private String name;

}
