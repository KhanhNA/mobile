package com.nextsolutions.dcommerce.shared.dto.product;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nextsolutions.dcommerce.service.annotation.ColumnMapper;
import com.nextsolutions.dcommerce.shared.dto.CurrencyDTO;
import com.nextsolutions.dcommerce.utils.CustomDateSerializer;
import com.nextsolutions.dcommerce.utils.DeserializeDateHandler;
import com.nextsolutions.dcommerce.utils.SerializeDateHandler;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class PackingPriceDto {
    @ColumnMapper(map = "packing_price_id")
    private Long id;
    private Long packingProductId;
    private BigDecimal price;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonDeserialize(using = DeserializeDateHandler.class)
    @JsonSerialize(using = SerializeDateHandler.class)
    private LocalDateTime fromDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonDeserialize(using = DeserializeDateHandler.class)
    private LocalDateTime toDate;
    private Integer priceType;
    private String currencyCode;
    private Long currencyId;
    private LocalDateTime createdTime;
    private LocalDateTime latestModifiedTime;
    private String code;
    private String imageUrl;
    private String name;
    private CurrencyDTO currency;
    private BigDecimal exchangeRate;
    private Long unitId;
    private Long status;
    private Long action;
    private String valueChange;
}
