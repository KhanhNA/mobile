package com.nextsolutions.dcommerce.shared.dto.incentive;

import lombok.Data;

@Data
public class IncentiveProgramDescriptionDto {
    private Long id;
    private Long incentiveProgramId;
    private String name;
    private String description;
    private String languageCode;
    private Long languageId;
    private String shortDescription;
    private String imageUrl;
}
