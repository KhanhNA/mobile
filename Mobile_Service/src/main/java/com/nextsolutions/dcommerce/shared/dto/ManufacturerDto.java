package com.nextsolutions.dcommerce.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ManufacturerDto {
    private Long id;
    private String code;
    private String name;
    private Date createdDate;
    private String tel;
    private String email;
    private String address;
    private Long languageId;
    private String taxCode;
    private String imageUrl;
    private List<ManufacturerDescriptionDto> descriptions;
}
