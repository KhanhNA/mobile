package com.nextsolutions.dcommerce.shared.dto;

import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MerchantDto {
    public Long merchantId;
    public String merchantCode;
    public String merchantName;

    //thông tin identity card
    private String fatherName;
    private String religion;
    private String bloodType;
    private String significantFigure; // đặc điểm nhận dạng
    private Double height; // đơn vị: cm

    public Long parentMarchantId;
    public Long merchantTypeId;
    public String userName;
    public String password;
    public String firstName;
    public String lastName;
    public String fullName;
    public String email;
    public Long defaultStoreId;
    public Double tax;
    public String merchantImgUrl;
    public String mobilePhone;
    public String activeCode;
    public LocalDateTime activeDate;
    private LocalDateTime deactiveDate;
    public LocalDateTime registerDate;
    public LocalDateTime createDate;
    public Integer activeStatus;
    public Integer status;

    private Boolean confirmKpi;

    public String address;
    public String tokenFirebase;
    public LocalDateTime birthday;

    public Long walletClientId;

    public Long defaultBankAccountId;

    public String defaultBankAccountNo;


    public Double lat;
    public Double longitude;
    public String area;
    public LocalDateTime dateCard;
    public String placeCard;
    public String nationality;
    public LocalDateTime dateBirth;


    public String province;
    public String district;
    public String village;
    public String identityImgFront;
    public String identityImgBack;
    public String contractImg;


    public Integer gender;
    public LocalDateTime identityCreateDate;
    public String identityAddressBy;
    public String identityNumber;

    public Integer businessModelId;
    public Integer acreageId;
    public Integer electricalInfrastructureId;
    public Integer displayDeviceId;
    public Integer refrigerationEquipmentId;
    public Integer action;
    private String discussionId;
    private LocalDateTime lockStartDate;
    private LocalDateTime lockEndDate;

    private Long request;
    private Long statusProcess;

    private List<AttributeDescriptionDTO> attValues;

    private List<MerchantGroupResource> groupValues;

    private List<AttrMapValueDTO> mapAttValues;

    private Boolean existAccountPost;

}
