package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.KpiSalesman;
import com.nextsolutions.dcommerce.shared.dto.KpiSalesmanDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface KpiSalesmanMapper {
    KpiSalesmanDTO toKpiSalesman(KpiSalesman kpiSalesman);
}
