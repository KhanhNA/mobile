package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.Product;
import com.nextsolutions.dcommerce.shared.dto.product.ProductFormDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductTableDto;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductAttributeResource;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductDataResource;
import com.nextsolutions.dcommerce.shared.dto.resource.ProductGeneralResource;
import com.nextsolutions.dcommerce.ui.model.response.PackingProductResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductBasicInfoResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductFormResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.ProductImagesResponseModel;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {
                CategoryMapper.class,
                ProductImageMapper.class,
                ManufacturerMapper.class,
                ProductDescriptionMapper.class,
                PackingProductMapper.class,
                ProductAttributeMapper.class
        }
)
public interface ProductMapper {

    @Mapping(target = "categoryId", source = "category.id")
    @Mapping(target = "type", source = "productType.id")
    @Mapping(target = "manufacturerId", source = "manufacturer.id")
    ProductFormDto toProductFormDto(Product product);

    @Mapping(target = "categoryId", source = "category.id")
    @Mapping(target = "productTypeId", source = "productType.id")
    @Mapping(target = "manufacturerId", source = "manufacturer.id")
    ProductFormResponseModel toProductFormResponse(Product product);

    @Mapping(target = "imageUrl", source = "imageDefaultUrl")
    @Mapping(target = "name", source = "productName")
    @Named("toProductTableDto")
    ProductTableDto toProductTableDto(Product product);

    @IterableMapping(qualifiedByName = "toProductTableDto")
    List<ProductTableDto> toProductTableDto(List<Product> products);

    ProductBasicInfoResponseModel productToProductBasicInfoResponse(Product product);

    @Mapping(target = "productId", source = "id")
    ProductImagesResponseModel toProductImagesResponse(Product product);

    @Mapping(target = "productId", source = "id")
    PackingProductResponseModel toPackingProductResponse(Product product);

    @Mapping(target = "defaultName", source = "name")
    ProductGeneralResource toProductGeneralResource(Product product);

    ProductDataResource toProductDataResource(Product product);

    ProductAttributeResource toProductAttributeResource(Product product);
}
