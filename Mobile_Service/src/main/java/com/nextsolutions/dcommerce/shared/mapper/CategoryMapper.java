package com.nextsolutions.dcommerce.shared.mapper;

import java.util.List;

import com.nextsolutions.dcommerce.model.CategoryLink;
import com.nextsolutions.dcommerce.shared.dto.product.ProductLinksCategoryDto;
import com.nextsolutions.dcommerce.ui.model.product.GetProductLinksCategoryResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.CategoryResponseModel;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import com.nextsolutions.dcommerce.model.Category;
import com.nextsolutions.dcommerce.shared.dto.CategoryDto;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

	@Named("categoryDto")
	CategoryDto toCategoryDto(Category category);

	@Named("categoryResponse")
	CategoryResponseModel toCategoryResponse(Category category);

	@IterableMapping(qualifiedByName = "categoryDto")
	List<CategoryDto> toCategories(List<Category> categories);

}
