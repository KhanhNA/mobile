package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.shared.dto.resource.AttributeValueDescriptionResource;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeValueResource;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AttributeValueDescriptionMapper {
    AttributeValueDescriptionResource toAttributeValueDescriptionResource(AttributeValueResource resource);
}
