package com.nextsolutions.dcommerce.shared.dto.wallet;

import io.swagger.annotations.ApiModelProperty;

public class PostAccountTransfersResponse {
    private PostAccountTransfersResponse() {
    }

    @ApiModelProperty(example = "1")
    public Integer savingsId;
    @ApiModelProperty(example = "1")
    public Integer resourceId;
}
