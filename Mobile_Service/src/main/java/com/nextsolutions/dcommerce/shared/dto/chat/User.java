package com.nextsolutions.dcommerce.shared.dto.chat;

import lombok.Data;

@Data
public class User {
    private Info user;

    public class Info {
        private String _id;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }
    }
}
