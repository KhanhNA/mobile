package com.nextsolutions.dcommerce.shared.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrouponRegistryDTO {
    private Long merchantId;
    private String merchantName;
    private Long registerQuantity;
}
