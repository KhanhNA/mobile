package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.Merchant;
import com.nextsolutions.dcommerce.shared.dto.SalesmanDto;
import com.nextsolutions.dcommerce.ui.model.salesman.CreateSalesmanRequestModel;
import com.nextsolutions.dcommerce.ui.model.salesman.CreateSalesmanResponseModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SalesmanMapper {

    @Mapping(target = "identityImgFront", ignore = true)
    @Mapping(target = "identityNumber", ignore = true)
    @Mapping(target = "identityImgBack", ignore = true)
    @Mapping(target = "avatar", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "salesmanCode", ignore = true)
    @Mapping(target = "salesmanId", ignore = true)
    SalesmanDto toSalesmanDto(CreateSalesmanRequestModel source);

    @Mapping(target = "id", source = "salesmanId")
    @Mapping(target = "code", source = "salesmanCode")
    CreateSalesmanResponseModel toCreateSalesmanResponseModel(SalesmanDto source);

    Merchant toMerchant(SalesmanDto salesmanDto);
}
