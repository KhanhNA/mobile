package com.nextsolutions.dcommerce.shared.dto.product;

import lombok.Data;

@Data
public class ProductImageDescriptionDto {
    private Long id;
    private Long productImageId;
    private String title;
    private String name;
    private String description;
    private Long languageId;
}
