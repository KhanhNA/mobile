package com.nextsolutions.dcommerce.shared.constant;

public enum AutoIncrementType {
    NPP("NPP"),NCC("NCC"),SALESMAN("SALESMAN")
    ,SKUs("SKUs"),PACK("PACK"),L1("L1"),
    L1EXCHANGE("L1_EXCHANGE"),KM("KM"),MCH("MCH"),ATTRIBUTE("ATTRIBUTE"), ATTRIBUTE_VALUE("ATTRIBUTE_VALUE"),
    MERCHANT_GROUPS("MERCHANT_GROUPS");
    String value;
    AutoIncrementType(String type) {
        this.value = type;
    }

    public String value() {
        return value;
    }
}
