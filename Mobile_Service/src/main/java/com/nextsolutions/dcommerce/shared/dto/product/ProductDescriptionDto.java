package com.nextsolutions.dcommerce.shared.dto.product;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDescriptionDto {

    private Long id;

    private Long productId;

    private String title;

    @NonNull
    @NotBlank(message = "error.descriptions.productName.empty")
    @Length(max = 255, message = "error.descriptions.productName.length")
    private String productName;

    private String description;

    @NonNull
    @NotNull
    private Long languageId;
}
