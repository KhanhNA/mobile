package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.Manufacturer;
import com.nextsolutions.dcommerce.model.ManufacturerLink;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDto;
import com.nextsolutions.dcommerce.shared.dto.product.ProductLinksManufacturerDto;
import com.nextsolutions.dcommerce.ui.model.product.GetProductLinksManufacturerResponseModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {
        ManufacturerDescriptionMapper.class
})
public interface ManufacturerMapper {
    ProductLinksManufacturerDto toProductLinksManufacturerDto(ManufacturerLink source);
    GetProductLinksManufacturerResponseModel toGetProductLinksManufacturerResponseModel(ProductLinksManufacturerDto source);
    @Mapping(target = "imageUrl", source = "image")
    ManufacturerDto toManufacturerDto(Manufacturer manufacturer);
}
