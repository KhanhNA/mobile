package com.nextsolutions.dcommerce.shared.dto.form;

import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupDescriptionResource;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class MerchantGroupForm {
    private Long id;
    private String code;
    private List<MerchantGroupDescriptionResource> descriptions;
    private Map<Long, MerchantGroupAttributeForm> attributes;
}
