// Generated with g9.

package com.nextsolutions.dcommerce.shared.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
@Data
public class IncentiveLevelDto implements Serializable {

	public static final Long SALE=0L;
	public static final Long INCENTIVE=1L;
	
    

    private Long id;

    private String groupText;

    private Long orderNumber;

    private int status;

    private LocalDateTime createDate;

    private String createUser;

    private LocalDateTime updateDate;

    private String updateUser;

    private Long minQuantity;
    

    private Long minAmount;
    
    

    private Long incentiveProgramId;
    

    private Double incentiveAmount;

    private Double incentiveAmountMax;
    

    private Double incentivePercent;
    

    private Double incentivePercentMax;

    private Integer isMultiple;
    private Double totalValue; //tong gia tri cua muc khuyen mai (dung de chon km nao tot nhat)
    

    private Set<IncentivePackingSaleDto> incentivePackingSale;
    private List<IncentivePackingIncentiveDto> incentivePackingIncentives;

    

    private IncentiveProgramDto incentiveProgram;



}
