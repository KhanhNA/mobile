package com.nextsolutions.dcommerce.shared.dto.resource;

import lombok.Data;

@Data
public class ProductLinksResource {
    private Long id;
    private Long productTypeId;
    private Long manufacturerId;
}
