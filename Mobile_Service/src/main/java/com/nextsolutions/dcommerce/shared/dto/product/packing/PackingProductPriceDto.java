package com.nextsolutions.dcommerce.shared.dto.product.packing;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PackingProductPriceDto {
    private Long id;
    private Long packingProductId;
    private BigDecimal price;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private Integer priceType;
    private String currencyCode;
    private LocalDateTime createdTime;
    private LocalDateTime latestModifiedTime;
}
