package com.nextsolutions.dcommerce.shared.mapper;

import com.nextsolutions.dcommerce.model.AttributeDescription;
import com.nextsolutions.dcommerce.shared.dto.attribute.AttributeDescriptionChangeDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeDescriptionResource;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AttributeDescriptionMapper {
    AttributeDescriptionResource toAttributeDescriptionResource(AttributeDescription source);
    AttributeDescriptionChangeDTO toAttributeDescriptionChangeDTO(AttributeDescription source);
}
