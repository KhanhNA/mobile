package com.nextsolutions.dcommerce.shared.dto.form;

import com.nextsolutions.dcommerce.shared.dto.resource.AttributeDescriptionResource;
import lombok.Data;

import java.util.List;

@Data
public class AttributeForm {
    private Long id;
    private String code;
    private Integer type;
    private Integer request;
    private Integer statusProcess;
    private String value;
    private List<AttributeDescriptionResource> descriptions;
}
