package com.nextsolutions.dcommerce.shared.mapper;

import java.util.List;

import com.nextsolutions.dcommerce.shared.dto.CategoryDescriptionDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.nextsolutions.dcommerce.model.CategoryDescription;

@Mapper(componentModel = "spring")
public interface CategoryDescriptionMapper {

	@Mapping(target = "hasParent", ignore = true)
	@Mapping(source = "category.id", target = "categoryId")
	@Named("categoryDescriptionDto")
    CategoryDescriptionDto toCategoryDescriptionDto(CategoryDescription category);

	@IterableMapping(qualifiedByName = "categoryDescriptionDto")
	List<CategoryDescriptionDto> toCategoryDescriptionsDto(List<CategoryDescription> categories);
}
