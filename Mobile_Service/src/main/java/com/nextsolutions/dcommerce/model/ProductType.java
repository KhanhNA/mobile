package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "product_type")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(insertable = false, name = "PRODUCT_TYPE_ID", nullable = false)
    private Long id;

    @Column(name = "PRD_TYPE_ADD_TO_CART")
    private Boolean prdTypeAddToCart;

    @Column(name = "DATE_CREATED")
    private Date createdDate;

    @Column(name = "DATE_MODIFIED")
    private Date modifiedDate;

    @Column(name = "UPDT_ID")
    private String updtId;

    @Column(name = "PRD_TYPE_CODE")
    private String code;

    @Column(name = "PRD_TYPE_NAME")
    private String name;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
