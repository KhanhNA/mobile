package com.nextsolutions.dcommerce.model;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class JsonEntityViewer {
	public interface Human {
		public interface Summary {
		}
		public interface CallOrder extends Summary {
		}
		public interface CommonList extends Summary{}
	}


	public static final Map<String, Class<? extends Human.Summary>> MapRequest = ImmutableMap.<String, Class<? extends Human.Summary>>builder()
			.put("/loyalty/packingForSale/list", Human.CallOrder.class)
			.put("/api/v1/common/list", Human.CommonList.class).build();

}
