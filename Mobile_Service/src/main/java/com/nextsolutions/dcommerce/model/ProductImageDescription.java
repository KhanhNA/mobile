package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@Entity
@Table(name = "product_image_description")
public class ProductImageDescription implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DESCRIPTION_ID")
    private Long id;

    @Column(name = "DATE_CREATED")
    private Date dateCreated;

    @Column(name = "DATE_MODIFIED")
    private Date dateModified;

    @Column(name = "UPDT_ID")
    private String updtId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "ALT_TAG")
    private String altTag;

    @Column(name = "LANGUAGE_ID", nullable = false)
    private Long languageId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "PRODUCT_IMAGE_ID")
    private ProductImage productImage;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
