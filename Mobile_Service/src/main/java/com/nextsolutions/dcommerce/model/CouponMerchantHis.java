package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author ts-client01
 * Create at 2019-06-21 09:39
 */
@Entity
@Table(name = "coupon_merchant_his")
@Data
public class CouponMerchantHis {


    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "merchant_id")
    private Long merchantId;

    @Column(name = "ORDER_ID")
    private Long orderId;

    @Column(name = "COUPON_ID")
    private Long couponId;


    @Column(name = "IS_USED")
    private Integer isUsed; //0- chua dung; 1- da dung; 2- huy

    @Column(name = "CREATE_DATE", insertable = false)
    private LocalDateTime createDate;

    @Column(name = "USED_DATE")
    private LocalDateTime usedDate;

    @Column(name = "status")
    private Integer status;
    @Column(name = "coupon_code")
    private String couponCode;

    public void setIsUsed(CouponMerchant.IsUsedType act) {
        this.isUsed = act.value;
    }
}
