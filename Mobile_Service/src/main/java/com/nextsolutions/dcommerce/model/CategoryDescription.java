package com.nextsolutions.dcommerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "category_description")
public class CategoryDescription {

	@Id
	@Column(name = "DESCRIPTION_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "URL_IMAGE")
	private String urlImage;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "LANGUAGE_ID")
	private Long languageId;

	@ManyToOne
	@JoinColumn(name = "CATEGORY_ID")
	private Category category;

	@Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
