// Generated with g9.

package com.nextsolutions.dcommerce.model.incentive;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Table(name="incentive_packing_sale_result")
@Entity
@Data
public class IncentivePackingSaleResult implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;



    @Column(name="INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;
    @Column(name="INCENTIVE_PACKING_SALE_ID")
    private Long incentivePackingSaleId;

    @Column(name="PACKING_PRODUCT_ID")
    private Long packingProductId;


    @Column(name = "MERCHANT_L1_ID")
    private Long merchantL1Id;

    @Column(name="TOTAL_ORDER_QUANTITY")
    private Integer totalOrderQuantity = 0;

    @Column(name="TOTAL_ORDER_AMOUNT")
    private Double totalOrderAmount = 0d;

    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;

    /** Default constructor. */
    public IncentivePackingSaleResult() {
        super();
    }

    

    

}
