package com.nextsolutions.dcommerce.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "kpi")
@Getter
@Setter
@NoArgsConstructor
public class Kpi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String type;
    @Column(nullable = false)
    private String code;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Double plan;
    @Column(nullable = false)
    private Double bonus;

    @Column(name = "from_date",nullable = false)
    private LocalDateTime fromDate;

    @Column(name = "to_date")
    private LocalDateTime toDate;

    @Column(name = "create_date")
    private LocalDateTime createDate;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "update_date")
    private LocalDateTime updateDate;
    @Column(name = "update_user")
    private String updateUser;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "kpi_id")
    private List<KpiDescription> descriptionsList;

}
