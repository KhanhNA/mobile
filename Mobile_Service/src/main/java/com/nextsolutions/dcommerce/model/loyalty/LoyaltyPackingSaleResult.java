// Generated with g9.

package com.nextsolutions.dcommerce.model.loyalty;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name="loyalty_packing_sale_result")
@Entity
@Data
public class LoyaltyPackingSaleResult implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @Column(name="LOYALTY_ID")
    private Long loyaltyId;
    @Column(name="LOYALTY_PACKING_SALE_ID")
    private Long loyaltyPackingSaleId;

    @Column(name="PACKING_PRODUCT_ID")
    private Long packingProductId;


    @Column(name = "MERCHANT_L1_ID")
    private Long merchantL1Id;

    @Column(name="TOTAL_ORDER_QUANTITY")
    private Double totalOrderQuantity = 0d;

    @Column(name="TOTAL_ORDER_AMOUNT")
    private Double totalOrderAmount = 0d;

    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;

    /** Default constructor. */
    public LoyaltyPackingSaleResult() {
        super();
    }

    

    

}
