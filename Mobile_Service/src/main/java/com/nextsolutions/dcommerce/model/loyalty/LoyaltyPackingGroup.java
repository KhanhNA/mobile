package com.nextsolutions.dcommerce.model.loyalty;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name="loyalty_packing_group")
@Entity
@Data
public class LoyaltyPackingGroup {
    /** Primary key. */
    protected static final String PK = "id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    @Column(name="CODE")
    private String code;
    @Column(name="NAME")
    private String name;
    @Column(name="STATUS")
    private Integer status;
    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;

    @Column(name="LOYALTY_ID")
    private Long loyaltyId;

    @Column(name="IS_ASSIGN")
    private Integer isAssign;

    @Column(name="FROM_DATE")
    private LocalDateTime fromDate;
    @Column(name="TO_DATE")
    private LocalDateTime toDate;
//    @ManyToOne(optional=false)
//    @JoinColumn(name="INCENTIVE_PROGRAM_ID", nullable=false)
//    private IncentiveProgram incentiveProgram;

}
