package com.nextsolutions.dcommerce.model;

import com.nextsolutions.dcommerce.shared.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


/**
 * @author PhamBien
 */
@Entity
@Table(name = "merchant_address")
@Getter
@Setter

public class MerchantAddress extends BaseDto {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     *
     */
    @Column(name = "merchant_id")
    private Long merchantId;

    /**
     *
     */
    @Column(name = "province")
    private String province;

    /**
     *
     */
    @Column(name = "district")
    private String district;

    /**
     *
     */
    @Column(name = "village")
    private String village;

    /**
     *
     */
    @Column(name = "phone_number")
    private String phoneNumber;
    /**
     *
     */
    @Column(name = "full_name")
    private String fullName;
    /**
     *
     */
    @Column(name = "is_select")
    private int isSelect;
    /**
     *
     */
    @Column(name = "address")
    private String address;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;
}
