package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;


/**
 * 
 * @author ts-client01
 * Create at 2019-06-22 16:15
 */
@Entity
@Table(name = "packing_description")
@Data

public class PackingDescription implements DescEntityBase {

    public static final String DESC_NAME = "PACKING";

    public PackingDescription() {
    }

    /**
     * 
     */
    @Id
    @Column(name = "DESCRIPTION_ID")
    private Long descriptionId;

    /**
     * 
     */
    @Column(name = "DATE_CREATED")
    private LocalDateTime dateCreated;

    /**
     * 
     */
    @Column(name = "DATE_MODIFIED")
    private LocalDateTime dateModified;

    /**
     * 
     */
    @Column(name = "UPDT_ID")
    private String updtId;

    /**
     * 
     */
    @Column(name = "DESCRIPTION")
    private String description;

    /**
     * 
     */
    @Column(name = "PACKING_NAME")
    private String packingName;

    /**
     * 
     */
    @Column(name = "PACKING_TITLE")
    private String packingTitle;

    /**
     * 
     */
    @Column(name = "META_DESCRIPTION")
    private String metaDescription;

    /**
     * 
     */
    @Column(name = "META_KEYWORDS")
    private String metaKeywords;

    /**
     * 
     */
    @Column(name = "META_TITLE")
    private String metaTitle;

    /**
     * 
     */
    @Column(name = "DOWNLOAD_LNK")
    private String downloadLnk;

    /**
     * 
     */
    @Column(name = "PRODUCT_HIGHLIGHT")
    private String productHighlight;

    /**
     * 
     */
    @Column(name = "SEF_URL")
    private String sefUrl;

    /**
     * 
     */
    @Column(name = "LANGUAGE_ID")
    private Integer languageId;

    /**
     * 
     */
    @Column(name = "OBJECT_ID")
    private Long objectId;

    /**
     * 
     */
    @Column(name = "URL_IMAGE")
    private String urlImage;


    @Override
    public String getName() {
        return DESC_NAME;
    }
}
