package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@Table(name = "product_image")
public class ProductImage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_IMAGE_ID")
    private Long id;

    @Column(name = "IMAGE_CROP")
    private Boolean imageCrop;

    @Column(name = "IMAGE_ORIGINAL_NAME")
    private String name;

    @Column(name = "IMAGE_TYPE")
    private String imageType;

    @Column(name = "PRODUCT_IMAGE")
    private String productImageUrl;

    @Column(name = "DEFAULT_IMAGE")
    private Boolean defaultImage;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "productImage", cascade = CascadeType.ALL)
    private List<ProductImageDescription> imageDescriptions;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
