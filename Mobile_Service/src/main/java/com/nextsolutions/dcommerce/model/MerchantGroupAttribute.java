package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

@Table(name = "merchant_group_attribute")
@Entity
@Data
public class MerchantGroupAttribute implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", insertable = false, nullable = false)
  private Long id;

  @Column(name = "merchant_group_id", nullable = false)
  private Long merchantGroupId;

  @Column(name = "attribute_id", nullable = false)
  private Long attributeId;

  @Column(name = "attribute_value_id", nullable = false)
  private Long attributeValueId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="merchant_group_id", insertable = false, updatable = false)
  private MerchantGroup merchantGroup;
}
