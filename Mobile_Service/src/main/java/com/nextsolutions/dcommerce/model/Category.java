package com.nextsolutions.dcommerce.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Table(name = "CATEGORY")
@NoArgsConstructor
public class Category {

    @Id
    @Column(name = "CATEGORY_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CODE", updatable = false)
    private String code;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SORT_ORDER")
    private Integer sortOrder;

    @Column(name = "URL_IMAGE")
    private String image;

    @Column(name = "VISIBLE")
    private Boolean visible;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = "parent_id")
    private Category parent;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "parent")
    private List<Category> children;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Product> products;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "category")
    private List<CategoryDescription> descriptions;

    public Category(Long categoryId) {
        this.id = categoryId;
    }

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;

    public Category(Long id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }
}
