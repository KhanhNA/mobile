// Generated with g9.

package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nextsolutions.dcommerce.shared.dto.AttributeDescriptionDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.HunterResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantCrudResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantResource;
import com.nextsolutions.dcommerce.utils.DeserializeDateHandler;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SqlResultSetMapping(
        name = "MerchantResource", classes = {
        @ConstructorResult(targetClass = MerchantResource.class,
                columns = {
                        @ColumnResult(name = "id", type = Long.class),
                        @ColumnResult(name = "code", type = String.class),
                        @ColumnResult(name = "username", type = String.class),
                        @ColumnResult(name = "fullName", type = String.class),
                        @ColumnResult(name = "address", type = String.class),
                        @ColumnResult(name = "phoneNumber", type = String.class),
                        @ColumnResult(name = "registerDate", type = LocalDateTime.class),
                }
        )
})

@SqlResultSetMapping(
        name = "HunterResource", classes = {
        @ConstructorResult(targetClass = HunterResource.class,
                columns = {
                        @ColumnResult(name = "id", type = Long.class),
                        @ColumnResult(name = "code", type = String.class),
                        @ColumnResult(name = "username", type = String.class),
                        @ColumnResult(name = "fullName", type = String.class),
                        @ColumnResult(name = "address", type = String.class),
                        @ColumnResult(name = "phoneNumber", type = String.class),
                        @ColumnResult(name = "registerDate", type = LocalDateTime.class),
                        @ColumnResult(name = "action", type = Integer.class),
                        @ColumnResult(name = "status", type = Integer.class),
                }
        )
})

@SqlResultSetMapping(
        name = "MerchantCrudResource", classes = {
        @ConstructorResult(targetClass = MerchantCrudResource.class,
                columns = {
                        @ColumnResult(name = "id", type = Long.class),
                        @ColumnResult(name = "code", type = String.class),
                        @ColumnResult(name = "username", type = String.class),
                        @ColumnResult(name = "fullName", type = String.class),
                        @ColumnResult(name = "address", type = String.class),
                        @ColumnResult(name = "phoneNumber", type = String.class),
                        @ColumnResult(name = "registerDate", type = LocalDateTime.class),
                        @ColumnResult(name = "walletId", type = Long.class),
                        @ColumnResult(name = "status", type = Integer.class),
                        @ColumnResult(name = "action", type = Integer.class),
                }
        )
})

@Table(name = "merchant")
@Getter
@Setter
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EqualsAndHashCode(of = "merchantId")
public class Merchant implements Serializable {

    /**
     * Primary key.
     */
    protected static final String PK = "merchantId";

    @Id
    @Column(name = "merchant_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long merchantId;

    @Column(name = "MERCHANT_UUID", length = 36, unique = true)
    private String uuid;

    @Column(name = "MERCHANT_CODE") //MERCHANT_CODE lấy luôn là số điện thoại của đại lý
    private String merchantCode;

    @Column(name = "MERCHANT_NAME")
    private String merchantName;

    @Column(name = "FATHER_NAME")
    private String fatherName;

    @Column(name = "HEIGHT")
    private Double height;

    @Column(name = "BLOOD_TYPE")
    private String bloodType;

    @Column(name = "RELIGION")
    private String religion;

    @Column(name = "SIGNIFICANT_FIGURE")
    private String significantFigure;

    @Column(name = "PARENT_MARCHANT_ID")
    private Long parentMarchantId;

    @Column(name = "MERCHANT_TYPE_ID")
    private Long merchantTypeId;

    @NotEmpty(message = "error.username.empty")
    @Column(name = "USER_NAME")
    private String userName; //MERCHANT_CODE lấy luôn là số điện thoại của đại lý

    // @Column(name = "PASSWORD")
    // private String password;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "DEFAULT_STORE_ID", precision = 19)
    private Long defaultStoreId;

    @Column(name = "TAX", precision = 20, scale = 2)
    private Double tax;

    @Column(name = "MERCHANT_IMG_URL")
    private String merchantImgUrl;

    @NotEmpty(message = "error.mobilephone.empty")
    @Column(name = "MOBILE_PHONE")
    private String mobilePhone; //MERCHANT_CODE lấy luôn là số điện thoại của đại lý

    @Column(name = "ACTIVE_CODE")
    private String activeCode;

    @Column(name = "ACTIVE_DATE", insertable = false)
    private LocalDateTime activeDate;

    @Column(name = "DEACTIVE_DATE")
    private LocalDateTime deactiveDate;

    @Column(name = "REACTIVE_DATE")
    private LocalDateTime reactiveDate;

//    @JsonDeserialize(using = DeserializeDateHandler.class)
//    @Column(name = "DATE_BIRTH")
//    private LocalDateTime birthday;

    @Column(name = "ACTIVE_STATUS")
    private Integer activeStatus;

    @Column(name = "STATUS")
    private Integer status; //0-Không hoạt động, 1-hoạt động

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "token_firebase")
    public String tokenFirebase;

    @Transient
    private Merchant parentMerchant;

    @Column(name = "wallet_Client_Id")
    private Long walletClientId;

    @Column(name = "default_bank_account_id")
    private Long defaultBankAccountId;

    @Column(name = "default_bank_account_no")
    private String defaultBankAccountNo;

    @Column(name = "LAT")
    public Double lat;

    @Column(name = "LONGITUDE")
    public Double longitude;

    @JsonDeserialize(using = DeserializeDateHandler.class)
    @Column(name = "date_birth")
    public LocalDateTime dateBirth;

    @Column(name = "province")
    public String province;

    @Column(name = "district")
    public String district;

    @Column(name = "village")
    public String village;

    @Column(name = "identity_img_front")
    public String identityImgFront;

    @Column(name = "identity_img_back")
    public String identityImgBack;

    @Column(name = "contract_img")
    public String contractImg;

    @Column(name = "gender")
    public Integer gender;

    @JsonDeserialize(using = DeserializeDateHandler.class)
    @Column(name = "identity_create_date")
    public LocalDateTime identityCreateDate;

    @Column(name = "identity_address_by")
    public String identityAddressBy;

    @Column(name = "business_model_id")
    public Integer businessModelId;

    @Column(name = "acreage_id")
    public Integer acreageId;

    @Column(name = "electrical_infrastructure_id")
    public Integer electricalInfrastructureId;

    @Column(name = "display_device_id")
    public Integer displayDeviceId;

    @Column(name = "refrigeration_equipment_id")
    public Integer refrigerationEquipmentId;

    @Column(name = "identity_number")
    public String identityNumber;

    @Column(name = "nationality")
    public String nationality;

    @Column(name = "action")
    private Integer action;

    @Column(name = "user_modified")
    private String userModified;

    @Transient
    private List<AttributeDescriptionDTO> attValues = new ArrayList<>();

    @Transient
    private List<MerchantGroupResource> groupValues = new ArrayList<>();

    @Column(name = "create_date", insertable = false, updatable = false)
    private LocalDateTime createDate;

    @Column(name = "discussion_id")
    private String discussionId;

    @Column(name = "email")
    private String email;

    @Column(name = "value_change")
    private String valueChange;

    @Column(name = "CONTRACT_NUMBER")
    private String contractNumber;

    @Column(name = "CARD_TYPE")
    private Integer cardType;

    @Column(name = "CONFIRM_KPI")
    private Boolean confirmKpi;

    @Column(name = "exist_account_post")
    private Boolean existAccountPost;
}
