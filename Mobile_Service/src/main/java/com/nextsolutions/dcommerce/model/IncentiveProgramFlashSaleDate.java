package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name = "incentive_program_flash_sale_date")
@Data
@Entity
public class IncentiveProgramFlashSaleDate implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "incentive_program_detail_information_id")
    private Long incentiveProgramDetailInformationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "incentive_program_detail_information_id", insertable = false, updatable = false)
    private IncentiveProgramDetailInformation incentiveProgramDetailInformation;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "zero_to_nine")
    private Boolean zeroToNine = Boolean.FALSE;

    @Column(name = "nine_to_twelve")
    private Boolean nineToTwelve = Boolean.FALSE;

    @Column(name = "twelve_to_fifteen")
    private Boolean twelveToFifteen = Boolean.FALSE;

    @Column(name = "fifteen_to_eighteen")
    private Boolean fifteenToEighteen = Boolean.FALSE;

    @Column(name = "eighteen_to_twenty_one")
    private Boolean eighteenToTwentyOne = Boolean.FALSE;

    @Column(name = "twenty_one_to_twenty_four")
    private Boolean twentyOneToTwentyFour = Boolean.FALSE;

}
