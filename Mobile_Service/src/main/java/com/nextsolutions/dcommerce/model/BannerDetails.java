package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

@Data
@Table(name = "banner_details")
@Entity
public class BannerDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "banner_id", nullable = false)
    private Long bannerId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "banner_id", insertable = false, updatable = false)
    private Banner banner;

    @Column(name = "link_target")
    private String linkTarget;

    @Column(name = "description")
    private String description;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "language_id")
    private Long languageId;

    @Column(name = "status")
    private Boolean status;
}
