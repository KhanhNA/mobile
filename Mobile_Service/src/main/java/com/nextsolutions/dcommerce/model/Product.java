package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nextsolutions.dcommerce.shared.dto.ProductDto;
import com.nextsolutions.dcommerce.shared.dto.product.PackingPriceDto;
import com.nextsolutions.dcommerce.ui.model.response.ProductPackingResponseModel;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


@SqlResultSetMapping(
        name = "ProductDtoMapping",
        classes = {
                @ConstructorResult(targetClass = ProductDto.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "productName", type = String.class),

                        }),
        }
)
@SqlResultSetMapping(
        name = "ProductDtoMappingAll",
        classes = {
                @ConstructorResult(targetClass = ProductDto.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "manufacturerId", type = Long.class),
                                @ColumnResult(name = "productName", type = String.class),
                                @ColumnResult(name = "urlImage", type = String.class),
                                @ColumnResult(name = "reviewAvg", type = Double.class),
                                @ColumnResult(name = "reviewCount", type = Integer.class),
                                @ColumnResult(name = "price", type = Double.class),
                                @ColumnResult(name = "quantity", type = Integer.class),
                                @ColumnResult(name = "orgPrice", type = Double.class),
                                @ColumnResult(name = "marketPrice", type = Double.class),
                                @ColumnResult(name = "discountPercent", type = Double.class),
                                @ColumnResult(name = "packingProductId", type = Long.class),
                                @ColumnResult(name = "packingProductCode", type = String.class)
                        }),
        }
)
@SqlResultSetMapping(
        name = "ProductDtoMappingAll1",
        entities = {@EntityResult(entityClass = ProductDto.class)}
)
@SqlResultSetMapping(
        name = "ProductPackingResponseModelMapping",
        classes = {
                @ConstructorResult(targetClass = ProductPackingResponseModel.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "imageUrl", type = String.class),
                                @ColumnResult(name = "name", type = String.class),
                                @ColumnResult(name = "code", type = String.class),
                                @ColumnResult(name = "sku", type = String.class),
                                @ColumnResult(name = "packingCode", type = String.class),
                                @ColumnResult(name = "packingName", type = String.class),
                                @ColumnResult(name = "originalPrice", type = BigDecimal.class),
                                @ColumnResult(name = "salePrice", type = BigDecimal.class),
                                @ColumnResult(name = "discountSalePrice", type = BigDecimal.class),
                                @ColumnResult(name = "discountPercent", type = Float.class),
                                @ColumnResult(name = "fromDate", type = LocalDateTime.class),
                                @ColumnResult(name = "toDate", type = LocalDateTime.class)
                        }),
        }
)
@Data
@Entity
@Table(name = "Product")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Product {

    @Id
    @Column(name = "PRODUCT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "PRODUCT_CODE")
    private String code;

    @Column(name = "PRODUCT_NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "SKU")
    private String sku;

    @Column(name = "PRODUCT_WEIGHT", precision = 2)
    private BigDecimal weight;

    @Column(name = "PRODUCT_WIDTH", precision = 2)
    private BigDecimal width;

    @Column(name = "PRODUCT_HEIGHT", precision = 2)
    private BigDecimal height;

    @Column(name = "PRODUCT_LENGTH", precision = 2)
    private BigDecimal length;

    @Column(name = "LENGTH_CLASS")
    private String lengthClass;

    @Column(name = "WEIGHT_CLASS")
    private String weightClass;

    @Column(name = "CALCULATED_WEIGHT", precision = 2)
    private BigDecimal calculatedWeight;

    @Column(name = "CALCULATED_WIDTH", precision = 2)
    private BigDecimal calculatedWidth;

    @Column(name = "CALCULATED_HEIGHT", precision = 2)
    private BigDecimal calculatedHeight;

    @Column(name = "CALCULATED_LENGTH", precision = 2)
    private BigDecimal calculatedLength;

    @Column(name = "WARNING_THRESHOLD")
    private Integer warningThreshold;

    @Column(name = "PRODUCT_LIFECYCLE")
    private Integer lifecycle;

    @Column(name = "UPDT_ID", length = 20)
    private String updtId;

    @Column(name = "AVAILABLE")
    private Boolean available;

    @Column(name = "COND")
    private Integer cond;

    @Column(name = "PREORDER")
    private Boolean preorder;

    @Column(name = "PRODUCT_FREE", nullable = true)
    private Boolean free;

    @Column(name = "QUANTITY_ORDERED")
    private Integer quantityOrdered;

    @Column(name = "REVIEW_AVG", precision = 2)
    private BigDecimal reviewAvg;

    @Column(name = "REVIEW_COUNT")
    private Integer reviewCount;

    @Column(name = "PRODUCT_SHIP")
    private Boolean ship;

    @Column(name = "PRODUCT_VIRTUAL")
    private Boolean virtual;

    @Column(name = "REF_SKU")
    private String refSku;

    @Column(name = "RENTAL_DURATION")
    private Integer rentalDuration;

    @Column(name = "RENTAL_PERIOD")
    private Integer rentalPeriod;

    @Column(name = "RENTAL_STATUS")
    private Integer rentalStatus;

    @Column(name = "SORT_ORDER")
    private Integer sortOrder;

    @Column(name = "IS_HOT")
    private Boolean hot;

    @Column(name = "IS_POPULAR")
    private Boolean popular;

    @Column(name = "DATE_AVAILABLE")
    private LocalDateTime dateAvailable;

    @Column(name = "DATE_CREATED")
    private LocalDateTime dateCreated;

    @Column(name = "DATE_MODIFIED")
    private LocalDateTime dateModified;

    @Column(name = "PRODUCT_IMAGE")
    private String imageDefaultUrl;

    @Column(name = "BRAND")
    private String brand;

    @Column(name = "COLOR")
    private String color;

    @Column(name = "ORIGIN")
    private String origin;

    @Column(name = "NOTES")
    private String notes;

    @Column(name = "PRODUCT_RETURNING")
    private Boolean returning;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "vat")
    private BigDecimal vat;

    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Category category;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<ProductImage> productImages;

    //    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<ProductDescription> productDescriptions;

    @Column(name = "MANUFACTURER_ID")
    private Long manufacturerId;

    //    @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne
    @JoinColumn(name = "MANUFACTURER_ID", insertable = false, updatable = false)
    private Manufacturer manufacturer;

    @Column(name = "PRODUCT_TYPE_ID")
    private Long productTypeId;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_TYPE_ID", insertable = false, updatable = false)
    private ProductType productType;

    @ManyToOne
    @JoinColumn(name = "DISTRIBUTOR_ID", insertable = false, updatable = false)
    private Distributor distributor;

    @Column(name = "DISTRIBUTOR_ID")
    private Long distributorId;

    @Column(name = "DISTRIBUTOR_CODE")
    private String distributorCode;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "PRODUCT_ID")
    private List<PackingProduct> packingProducts;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<ProductAttribute> productAttributes;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false)
    private Boolean isSynchronization;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "IMPORT_PRICE", precision = 10)
    private BigDecimal importPrice;

    @Transient
    private String productName; //

    public Product(Long id, String code, String name, String imageUrl, String sku, BigDecimal width, BigDecimal height, BigDecimal weight, BigDecimal length, Category category, BigDecimal importPrice, BigDecimal vat) {
        this.id = id;
        this.code = code;
        this.vat = vat;
        this.productName = name;
        this.imageDefaultUrl = imageUrl;
        this.sku = sku;
        this.width = width;
        this.height = height;
        this.weight = weight;
        this.length = length;
        this.category = category;
        this.importPrice = importPrice;
    }
}
