package com.nextsolutions.dcommerce.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * - Mỗi đại lý (merchant) sẽ có 1 ví (wallet)
 *
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
@Entity
@Table(name = "payment_method")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PaymentMethod {

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PAYMENT_METHOD_ID")
    private Long paymentMethodId;

    /**
     *
     */
    @Column(name = "PAYMENT_METHOD_NAME")
    private String paymentMethodName;


}
