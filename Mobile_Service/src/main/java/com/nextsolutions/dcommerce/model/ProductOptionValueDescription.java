package com.nextsolutions.dcommerce.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "product_option_value_description")
public class ProductOptionValueDescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DESCRIPTION_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_OPTION_VALUE_ID")
    private ProductOptionValue optionValue;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LANGUAGE_ID")
    private Long languageId;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
