package com.nextsolutions.dcommerce.model.firebase;

public enum NotificationAction {
    ALL(0),L2_ORDER(1), L1_ORDER_APPROVE(2), L1_ORDER_REJECT(3), INVITE(4), PROMOTION(5), ORDER_SUCCESS(6);
    private int value;

    NotificationAction(int type) {
        this.value = type;
    }

    public int getValue() {
        return value;
    }
}
