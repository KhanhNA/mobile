package com.nextsolutions.dcommerce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "merchant_images")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MerchantImages {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "merchant_id")
    private Long merchantId;
    @Column(name = "url")
    private String url;
    @Column(name = "type")
    private Integer type;



}
