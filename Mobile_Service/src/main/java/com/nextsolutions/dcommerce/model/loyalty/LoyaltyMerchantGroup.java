package com.nextsolutions.dcommerce.model.loyalty;

import com.nextsolutions.dcommerce.model.MerchantGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "loyalty_merchant_group")
@Getter
@Setter
@Entity
public class LoyaltyMerchantGroup {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "LOYALTY_ID")
    private Long loyaltyId;

    @ManyToOne
    @JoinColumn(name = "merchant_group_id", referencedColumnName = "id")
    private MerchantGroup merchantGroup;

    @Column(name = "STATUS")
    private Integer status;
}
