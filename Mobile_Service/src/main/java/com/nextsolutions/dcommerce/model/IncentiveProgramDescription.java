package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "incentive_program_description_v2")
@Data
@Entity
@ToString(exclude = {"incentiveProgram"})
@EqualsAndHashCode(exclude = {"incentiveProgram"})
public class IncentiveProgramDescription implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "incentive_program_id")
    private Long incentiveProgramId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "language_code")
    private String languageCode;

    @Column(name = "language_id")
    private Long languageId;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "image_url")
    private String imageUrl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "incentive_program_id", insertable = false, updatable = false)
    private IncentiveProgram incentiveProgram;
}
