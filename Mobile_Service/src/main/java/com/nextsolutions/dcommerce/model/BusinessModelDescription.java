package com.nextsolutions.dcommerce.model;

import com.nextsolutions.dcommerce.shared.dto.BaseDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "business_model_description")
@Data
@NoArgsConstructor()
public class BusinessModelDescription extends BaseDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "business_model_id")
    private Integer businessModelId;
    @Column(name = "name")
    private String name;
    @Column(name = "lang_id")
    private Integer langId;


}
