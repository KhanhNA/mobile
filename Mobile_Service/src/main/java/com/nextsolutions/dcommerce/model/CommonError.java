package com.nextsolutions.dcommerce.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CommonError {
    int code;
    String comment;
}
