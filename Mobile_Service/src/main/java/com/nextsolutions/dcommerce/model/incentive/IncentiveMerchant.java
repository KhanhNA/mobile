// Generated with g9.

package com.nextsolutions.dcommerce.model.incentive;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name="incentive_merchant")
@Entity
@Data
public class IncentiveMerchant implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    @Id
    @Column(name="ID", unique=true, nullable=false, precision=19)
    private Long id;
    @Column(name="MERCHANT_ID", precision=19)
    private Long merchantId;
    @Column(name="QUANTITY_MAX", precision=19)
    private Long quantityMax;
    @Column(name="QUANTITY_RECEIVED", precision=19)
    private Long quantityReceived;
    @Column(name="STATUS", precision=19)
    private Long status;
    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;
    @Column(name="QUANTITY_CUSTOMER", precision=19)
    private Long quantityCustomer;
    @Column(name="SYN_ACTION", precision=19)
    private Long synAction;
    @Column(name="FROM_DATE")
    private LocalDateTime fromDate;
    @Column(name="TO_DATE")
    private LocalDateTime toDate;
    @Column(name="IS_QUANTITY_MAX_EDIT", precision=19)
    private Long isQuantityMaxEdit;
    @Column(name="AMOUNT_MAX", precision=19)
    private Long amountMax;
    @Column(name="AMOUNT_RECEIVED", precision=19)
    private Long amountReceived;
    @Column(name="NUM_MAX", precision=19)
    private Long numMax;
    @Column(name="NUM_RECEIVED", precision=19)
    private Long numReceived;
    @Column(name="QUANTITY_RECEIVED_TOTAL", precision=19)
    private Long quantityReceivedTotal;
    @Column(name="AMOUNT_RECEIVED_TOTAL", precision=19)
    private Long amountReceivedTotal;
    @Column(name="NUM_RECEIVED_TOTAL", precision=19)
    private Long numReceivedTotal;
    @Column(name="NUM_ORDER_RECEIVED", precision=19)
    private Long numOrderReceived;
    @Column(name="NUM_ORDER_RECEIVED_TOTAL", precision=19)
    private Long numOrderReceivedTotal;
    @Column(name="VANSALE_ORDER", precision=19)
    private Long vansaleOrder;
    @ManyToOne(optional=false)
    @JoinColumn(name="INCENTIVE_PROGRAM_ID", nullable=false)
    private Incentive incentive;

    /** Default constructor. */
    public IncentiveMerchant() {
        super();
    }


}
