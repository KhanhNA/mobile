package com.nextsolutions.dcommerce.model;

public interface DescEntityBase {
    public Long getObjectId();
    public String getName();
}
