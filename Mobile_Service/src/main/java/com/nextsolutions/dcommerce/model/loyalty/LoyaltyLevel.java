package com.nextsolutions.dcommerce.model.loyalty;

import com.nextsolutions.dcommerce.model.PackingProduct;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Table(name="loyalty_level")
@Entity
@Data
public class LoyaltyLevel  implements Serializable {

    public static final Long SALE=0L;
    public static final Long LOYALTY=1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @Column(name="NAME")
    private String name;

    @Column(name="STATUS")
    private Integer status;
    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;
    @Column(name="MIN_QUANTITY")
    private Long minQuantity;

    @Column(name="MIN_AMOUNT")
    private Long minAmount;


    @Column(name="LOYALTY_ID")
    private Long loyaltyId;


    @Column(name="LOYALTY_POINT")
    private Double loyaltyPoint;

    @Column(name="LOYALTY_COIN")
    private Double loyaltyCoin;

    @Column(name="TOTAL_VALUE")
    private Double totalValue; //tong gia tri cua muc khuyen mai (dung de chon km nao tot nhat)

    @Column(name="LOYALTY_PACKING_SALE_ID")
    private Long loyaltyPackingSaleId;

    @ManyToOne
    @Transient
    @JoinColumn(name = "LOYALTY_PACKING_SALE_ID")
    private LoyaltyPackingSale loyaltyPackingSale;


//    @Column(name="PACKING_PRODUCT_ID")
//    @ManyToOne
//    @JoinColumn(name = "PACKING_PRODUCT_ID")
//    private PackingProduct packingProduct_loyaltyLvl;

    @Column(name="LOYALTY_PACKING_GROUP_ID")
    private Long loyaltyPackingGroupId;

    @Column(name="OBJ_TYPE")
    private Integer objType;

    @Column(name = "HAS_LOYALTY_PACKING")
    private Integer hasLoyaltyPacking;//Co sp khuyen mai ko: 1-co, 0-khong

    @Transient
    private List<LoyaltyPackingGroupPacking> loyaltyPackingGroupPackings;


    @ManyToOne
    @JoinColumn(name="Loyalty_ID")
    @Transient
    private Loyalty loyalty;


    /** Default constructor. */
    public LoyaltyLevel() {
        super();
    }

}
