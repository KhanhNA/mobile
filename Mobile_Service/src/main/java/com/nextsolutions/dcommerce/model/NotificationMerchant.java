package com.nextsolutions.dcommerce.model;

import com.nextsolutions.dcommerce.model.order.OrderPackingV2;
import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "notification_merchant")
@AllArgsConstructor
@NoArgsConstructor
public class NotificationMerchant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "action_id")
    private Long actionId;

    @Column(name = "merchant_receiver_id")
    private Long merchantReceiverId;

    @Column(name = "sender_id")
    private Long senderId;

    @Column(name = "type_id")
    private Integer typeId;

    @Column(name = "create_date", insertable = false, updatable = false)
    private LocalDateTime createDate;

    @Column(name = "is_seen")
    private Integer isSeen;

    @Column(name = "order_product_name")
    private String orderPackingName;

    @Column(name = "order_quantity")
    private Integer orderQuantity;

    @Column(name = "order_packing_url")
    private String orderPackingUrl;

    @Column(name = "merchant_name")
    private String merchantName;

    @Column(name = "merchant_number_phone")
    private String merchantNumberPhone;

    @Column(name = "order_code")
    private String orderCode;

    @Transient
    private String token;
    @Transient
    private String topic;
    @Transient
    private String click_action;
    @Transient
    private String title;
    @Transient
    private String message;


}
