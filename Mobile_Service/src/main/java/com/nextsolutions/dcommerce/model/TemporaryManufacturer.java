package com.nextsolutions.dcommerce.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Table(name = "temporary_manufacturer")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TemporaryManufacturer implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", insertable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DATE_CREATED", unique = true)
    private Date createdDate;

    @Column(name = "DATE_MODIFIED")
    private Date modifiedDate;

    @Column(name = "UPDT_ID")
    private String updtId;

    @Column(name = "CODE", unique = true)
    private String code;

    @Column(name = "MANUFACTURER_IMAGE")
    private String image;

    @Column(name = "SORT_ORDER")
    private Integer sortOrder;

    @Column(name = "MERCHANT_ID", nullable = false)
    private Integer merchantId;

    @Transient
    private List<TemporaryManufacturerDescription> temporaryManufacturerDescriptions;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;

    @Column(name = "TEL", nullable = false)
    private String tel;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "STATUS")
    private Long status;

    @Column(name = "MANUFACTURER_ID")
    private Long manufacturerId;

    @Column(name = "TIME_REQUEST")
    private Date timeRequest;

    @Column(name = "TAX_CODE")
    private String taxCode;
}
