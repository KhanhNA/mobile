package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-21 09:39
 */
@Entity
@Table(name = "merchant_type")
@Data
public class MerchantType {


    /**
     * 
     */
    @Id
    @Column(name = "ID")
    private Long id;

    /**
     * 
     */
    @Column(name = "CODE")
    private String code; //1-Dl cap 1;2-DL cap 2

    /**
     * 
     */
    @Column(name = "NAME")
    private String name;



}
