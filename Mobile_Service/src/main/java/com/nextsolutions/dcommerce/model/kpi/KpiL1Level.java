package com.nextsolutions.dcommerce.model.kpi;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "kpi_l1_level")
@Data
public class KpiL1Level {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "kpi_id")
    private Long kpiId;

    @Column(name = "target")
    private BigDecimal target;

    @Column(name = "bonus")
    private BigDecimal bonus;

}
