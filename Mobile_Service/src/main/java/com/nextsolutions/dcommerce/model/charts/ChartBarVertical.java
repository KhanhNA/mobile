package com.nextsolutions.dcommerce.model.charts;

import java.util.List;

public class ChartBarVertical {
	private String title;
	private List<ChartBarVerticalResult> results;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ChartBarVerticalResult> getResults() {
		return this.results;
	}

	public void setResults(List<ChartBarVerticalResult> results) {
		this.results = results;
	}

}
