// Generated with g9.

package com.nextsolutions.dcommerce.model.incentive;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Table(name="packing_incentive_program")
@Entity
@Data

public class PackingIncentiveProgram implements Serializable {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;


    @Column(name="packing_product_id")
    private Long packingProductId;


    @Column(name="incentive_program_id")
    private Long incentiveProgramId;

    @Column(name="incentive_program_type")
    private Integer incentiveProgramType; //1: KM theo sp, 2: KM theo don hnag, 3: groupon

    @Column(name="from_date")
    private LocalDateTime fromDate;

    @Column(name="to_date")
    private LocalDateTime toDate;

    @Column(name="sale_from_date")
    private LocalDateTime saleFromDate;

    @Column(name="sale_to_date")
    private LocalDateTime saleToDate;


    /** Default constructor. */
    public PackingIncentiveProgram() {
        super();
    }

    

    

}
