package com.nextsolutions.dcommerce.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "product_option")
public class ProductOption {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_OPTION_ID")
    private Long id;

    @Column(name = "PRODUCT_OPTION_CODE")
    private String code;

    @OneToMany(mappedBy = "option", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductOptionDescription> descriptions;

    @OneToMany(mappedBy = "option", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<ProductOptionValue> values;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
