package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * - Mỗi đại lý (merchant) sẽ có 1 ví (wallet)
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
@Entity
@Table(name = "merchant_wallet")
@Data
@EqualsAndHashCode(of = "ID")
public class MerchantWallet {


    /**
     * 
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long Id;

    /**
     * 
     */
    @Column(name = "MERCHANT_ID")
    private Long merchantId;

    @Column(name = "MERCHANT_CODE")
    private String merchantCode;

    @Column(name = "saving_account_id")
    private Long savingAccountId;

    @Column(name = "saving_account_code")
    private String savingAccountCode; //unique

    @Column(name = "saving_product_id")
    private Long savingProductId;


    /**
     * 
     */
    @Column(name = "AMOUNT")
    private Double amount;



    public void addIncentive(Double incentive){
        if(amount == null){
            amount = new Double(0);
        }
        amount += incentive;
    }


}
