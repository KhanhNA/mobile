package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="kpi_type")
@Data
public class KpiType {

    @Id
    private String name;
}
