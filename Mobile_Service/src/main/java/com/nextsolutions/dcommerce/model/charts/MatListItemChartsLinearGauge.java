package com.nextsolutions.dcommerce.model.charts;

import java.math.BigDecimal;

public class MatListItemChartsLinearGauge {
	private String avatarColor;
	private String avatarIcon;
	private String lineOne;
	private String lineTwo;
	private String scheme;
	private BigDecimal value;
	private BigDecimal previousValue;
	private BigDecimal min;
	private BigDecimal max;
	private BigDecimal units;

	public String getAvatarColor() {
		return this.avatarColor;
	}

	public void setAvatarColor(String matListAvatarColor) {
		this.avatarColor = matListAvatarColor;
	}

	public String getAvatarIcon() {
		return this.avatarIcon;
	}

	public void setAvatarIcon(String matListAvatarIcon) {
		this.avatarIcon = matListAvatarIcon;
	}

	public String getLineOne() {
		return this.lineOne;
	}

	public void setLineOne(String matLineOne) {
		this.lineOne = matLineOne;
	}

	public String getLineTwo() {
		return this.lineTwo;
	}

	public void setLineTwo(String matLineTwo) {
		this.lineTwo = matLineTwo;
	}

	public String getScheme() {
		return this.scheme;
	}

	public void setScheme(String chartsLinearGaugeScheme) {
		this.scheme = chartsLinearGaugeScheme;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal chartsLinearGaugeValue) {
		this.value = chartsLinearGaugeValue;
	}

	public BigDecimal getPreviousValue() {
		return this.previousValue;
	}

	public void setPreviousValue(BigDecimal chartsLinearGaugePreviousValue) {
		this.previousValue = chartsLinearGaugePreviousValue;
	}

	public BigDecimal getMin() {
		return this.min;
	}

	public void setMin(BigDecimal chartsLinearGaugeMin) {
		this.min = chartsLinearGaugeMin;
	}

	public BigDecimal getMax() {
		return this.max;
	}

	public void setMax(BigDecimal chartsLinearGaugeMax) {
		this.max = chartsLinearGaugeMax;
	}

	public BigDecimal getUnits() {
		return this.units;
	}

	public void setUnits(BigDecimal chartsLinearGaugeUnits) {
		this.units = chartsLinearGaugeUnits;
	}

}
