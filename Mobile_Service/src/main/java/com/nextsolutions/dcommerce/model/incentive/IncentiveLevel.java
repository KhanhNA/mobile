// Generated with g9.

package com.nextsolutions.dcommerce.model.incentive;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Table(name="incentive_level")
@Entity
@Data
public class IncentiveLevel implements Serializable {

	public static final Long SALE=0L;
	public static final Long INCENTIVE=1L;
	
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @Column(name="NAME")
    private String name;

    @Column(name="STATUS")
    private Integer status;
    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;
    @Column(name="MIN_QUANTITY")
    private Long minQuantity;
    
    @Column(name="MIN_AMOUNT")
    private Long minAmount;
    
    
    @Column(name="INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;
    
    
    @Column(name="INCENTIVE_AMOUNT")
    private Double incentiveAmount;
    @Column(name="INCENTIVE_AMOUNT_MAX")
    private Double incentiveAmountMax;
    
    @Column(name="INCENTIVE_PERCENT")
    private Double incentivePercent;
    
    @Column(name="INCENTIVE_PERCENT_MAX")
    private Double incentivePercentMax;
    
    
    @Column(name="IS_MULTIPLE")
    private Integer isMultiple;
    
    @Column(name="TOTAL_VALUE")
    private Double totalValue; //tong gia tri cua muc khuyen mai (dung de chon km nao tot nhat)

    @Column(name="INCENTIVE_PACKING_SALE_ID")
    private Long incentivePackingSaleId;


    @Column(name="PACKING_PRODUCT_ID")
    private Long packingProductId;

    @Column(name="INCENTIVE_PACKING_GROUP_ID")
    private Long ictPackingGroupId;

    @Column(name="OBJ_TYPE")
    private Integer objType;

    @Column(name = "HAS_INCENTIVE_PACKING")
    private Integer hasIctPacking;//Co sp khuyen mai ko: 1-co, 0-khong
    @Transient
    private List<IncentivePackingGroupPacking> ictGroupPackings;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "INCENTIVE_LEVEL_ID")
    private List<IncentivePackingIncentive> incentivePackingIncentives;

    @Transient
    @ManyToOne
    @JoinColumn(name = "INCENTIVE_PACKING_SALE_ID")
    private IncentivePackingSale incentivePackingSale;

    
//    @ManyToOne
//    @JoinColumn(name="INCENTIVE_PROGRAM_ID")
    @Transient
    private Incentive incentive;
    
    
    /** Default constructor. */
    public IncentiveLevel() {
        super();
    }

    

    

}
