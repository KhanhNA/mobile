package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Data
@Entity
@Table(name = "product_options")
public class ProductOptions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="product_id")
    private Long productId;

    @Column(name="attribute_id")
    private Long attributeId;

    @Column(name="attribute_value_id")
    private Long attributeValueId;

}
