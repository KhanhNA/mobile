package com.nextsolutions.dcommerce.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Language")
@Data
@NoArgsConstructor
public class Language{
    @Id
    @Column(name = "LANGUAGE_ID")
    private Long id;
    @Column(name = "CODE")
    private String code;
    @Column(name = "NAME")
    private String name;

    public Language(Long id) {
        this.id = id;
    }
}
