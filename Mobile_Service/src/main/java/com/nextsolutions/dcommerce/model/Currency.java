package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "currency")
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Currency implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CURRENCY_ID", insertable = false, nullable = false)
    private Long id;

    @Column(name = "CURRENCY_CODE")
    private String code;

    @Column(name = "CURRENCY_NAME")
    private String name;

    @Column(name = "CURRENCY_SUPPORTED")
    private Boolean supported;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
