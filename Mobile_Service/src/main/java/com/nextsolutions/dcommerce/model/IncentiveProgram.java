package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


@SqlResultSetMapping(
    name = "GetIncentiveProgram",
    classes = @ConstructorResult(
        targetClass = IncentiveProgram.class,
        columns = {
            @ColumnResult(name = "id", type = Long.class),
            @ColumnResult(name = "code", type = String.class),
            @ColumnResult(name = "name", type = String.class),
            @ColumnResult(name = "description", type = String.class),
            @ColumnResult(name = "type", type = Integer.class),
            @ColumnResult(name = "status", type = Integer.class),
            @ColumnResult(name = "fromDate", type = LocalDateTime.class),
            @ColumnResult(name = "toDate", type = LocalDateTime.class),
            @ColumnResult(name = "createdUser", type = String.class),
            @ColumnResult(name = "createdDate", type = LocalDateTime.class),
            @ColumnResult(name = "latestModifiedUser", type = String.class),
            @ColumnResult(name = "latestModifiedDate", type = LocalDateTime.class),
            @ColumnResult(name = "canActivate", type = Boolean.class),
        }
    )
)

/**
 * CT KM: - Description: nội dung ctkm
 */
@Entity
@Table(name = "incentive_program_v2")
@Data
@NoArgsConstructor
public class IncentiveProgram implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, nullable = false)
    private Long id;

    @Column(name = "CODE")
    private String code;

    /**
     * 1: giảm giá nhóm sản phẩm
     * 2: giảm giá theo sản phẩm
     * 3: coupon giỏ hàng
     */
    @Column(name = "TYPE")
    private Integer type;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "FROM_DATE")
    private LocalDateTime fromDate;

    @Column(name = "TO_DATE")
    private LocalDateTime toDate;

    @Column(name = "CREATED_USER")
    private String createdUser;

    @Column(name = "CREATED_DATE")
    private LocalDateTime createdDate;

    @Column(name = "LATEST_MODIFIED_USER")
    private String latestModifiedUser;

    @Column(name = "LATEST_MODIFIED_DATE")
    private LocalDateTime latestModifiedDate;

    @Column(name = "DISPLAY_AREA")
    private Integer displayArea;

    @Column(name = "APPLY_L2")
    private Boolean applyL2;

    @OneToMany(mappedBy = "incentiveProgram", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<IncentiveProgramDescription> descriptions;

    @Transient
    private IncentiveProgramDetailInformation incentiveProgramDetailInformation;

    @Transient
    private List<IncentiveProgramDetailInformation> incentiveProgramDetailInformationGroup;

    @OneToOne(mappedBy = "incentiveProgram", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private IncentiveProgramGroup incentiveProgramGroup;

    @Transient
    private String name;

    @Transient
    private String description;

    @Transient
    private Boolean canActivate;

    public IncentiveProgram(
            Long id,
            String code,
            String name,
            String description,
            Integer type,
            Integer status,
            LocalDateTime fromDate,
            LocalDateTime toDate,
            String createdUser,
            LocalDateTime createdDate,
            String latestModifiedUser,
            LocalDateTime latestModifiedDate,
            Boolean canActivate
    ) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.type = type;
        this.status = status;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.createdUser = createdUser;
        this.createdDate = createdDate;
        this.latestModifiedUser = latestModifiedUser;
        this.latestModifiedDate = latestModifiedDate;
        this.canActivate = canActivate;
    }
}
