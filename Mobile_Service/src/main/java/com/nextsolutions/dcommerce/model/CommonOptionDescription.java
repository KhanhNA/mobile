package com.nextsolutions.dcommerce.model;

import io.swagger.models.auth.In;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "common_option_description")
@NoArgsConstructor
public class CommonOptionDescription {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "common_option_id")
    private Integer commonOptionId;

    @Column(name = "name")
    private String name;

    @Column(name = "lang_id")
    private Integer langId;

    @Transient
    private Integer type;
    @Transient
    private String code;
}
