package com.nextsolutions.dcommerce.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {
    String firstName;
    Long id;
    String lastName;
    List<Role> roles;
    String username;
    String newConfirmPassword;
    String newPassword;
    String oldPassword;
    Boolean enabled;

    @Builder
    public UserEntity(String firstName, String lastName, List<Role> roles, String username, Boolean enabled) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.roles = roles;
        this.username = username;
        this.enabled = enabled;
    }
}
