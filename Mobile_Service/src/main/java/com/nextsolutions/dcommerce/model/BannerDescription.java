package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "banner_description")
public class BannerDescription implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "banner_id", nullable = false)
    private Long bannerId;

    @ManyToOne
    @JoinColumn(name = "banner_id", insertable = false, updatable = false)
    private Banner banner;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "language_id", nullable = false)
    private Long languageId;
}
