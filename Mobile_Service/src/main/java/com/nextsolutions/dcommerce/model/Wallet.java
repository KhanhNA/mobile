package com.nextsolutions.dcommerce.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * - Mỗi đại lý (merchant) sẽ có 1 ví (wallet)
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
@Entity
@Table(name = "wallet")
@Data
@EqualsAndHashCode(of = "walletId")
public class Wallet {


    /**
     * 
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "WALLET_ID")
    private Long walletId;

    /**
     * 
     */
    @Column(name = "MERCHANT_ID")
    private Long merchantId;

    @Column(name = "MERCHANT_CODE")
    private String merchantCode;

    @Column(name = "saving_account_id")
    private Long savingAccountId;

    @Column(name = "saving_account_code")
    private String savingAccountCode;

    @Column(name = "saving_product_id")
    private Long savingProductId;


    /**
     * 
     */
    @Column(name = "AMOUNT")
    private Double amount;



    public void addIncentive(Double incentive){
        if(amount == null){
            amount = new Double(0);
        }
        amount += incentive;
    }


}
