package com.nextsolutions.dcommerce.model.incentive;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * CT KM: - Description: nội dung ctkm
 *
 * @author ts-client01
 * Create at 2019-06-21 09:38
 */
@Entity
@Table(name = "incentive_program_description")
@Data
@EqualsAndHashCode(of = "id")

public class IncentiveProgramDescription {


    /**
     *
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @ManyToOne
    @Column(name = "INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;

    @Column(name = "LANG_ID")
    private Long langId;

    @Column(name = "LANG_CODE")
    private String langCode;

    /**
     *
     */
    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;


}
