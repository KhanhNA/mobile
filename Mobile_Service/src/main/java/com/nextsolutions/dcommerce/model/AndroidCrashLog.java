package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-27 15:57
 */
@Entity
@Table(name = "android_crash_log")
@Data

public class AndroidCrashLog {

    public AndroidCrashLog() {
    }

    /**
     * 
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    /**
     * 
     */
    @Column(name = "MERCHANT_ID")
    private Long merchantId;

    /**
     * 
     */
    @Column(name = "ERROR")
    private String error;

    /**
     * 
     */
    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
}
