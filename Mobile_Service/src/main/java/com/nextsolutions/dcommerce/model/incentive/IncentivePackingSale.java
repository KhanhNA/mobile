package com.nextsolutions.dcommerce.model.incentive;

import com.nextsolutions.dcommerce.model.PackingProduct;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Table(name = "incentive_packing_sale")
@Entity
@Data
public class IncentivePackingSale implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "INCENTIVE_LEVEL_ID")
    private Long incentiveLevelId;

    @Column(name = "PACKING_PRODUCT_ID", insertable=false, updatable=false)
    private Long packingProductId;

    @Column(name = "INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;

    @Column(name = "INCENTIVE_PACKING_GROUP_ID", insertable = false, updatable = false)
    private Long ictPackingGroupId;

    @Column(name = "OBJ_TYPE")
    private Integer objType;//1: packing_product;2: incentive_packing_group

    @Column(name = "STATUS")
    private Long status;

    @Column(name = "FROM_DATE")
    private LocalDateTime fromDate;

    @Column(name = "TO_DATE")
    private LocalDateTime toDate;

    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name = "CREATE_USER")
    private String createUser;
    @Column(name = "UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name = "UPDATE_USER")
    private String updateUser;

    @Transient
    public String getCode() {
        if (objType == 1) {
            return this.packingProduct == null ? null : this.packingProduct.getCode();
        } else {
            return this.ictPackingGroup == null ? null : this.ictPackingGroup.getCode();
        }

    }

    //    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Transient
    public String getName() {
        if (objType == 1) {
            return this.packingProduct == null ? null : this.packingProduct.getName();
        } else {
            return this.ictPackingGroup == null ? null : this.ictPackingGroup.getName();
        }
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "INCENTIVE_PACKING_SALE_ID")
    private Set<IncentiveLevel> incentiveLevel;
    //
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "PACKING_PRODUCT_ID")
    private PackingProduct packingProduct;

    @ManyToOne
    @JoinColumn(name = "INCENTIVE_PACKING_GROUP_ID")
    private IncentivePackingGroup ictPackingGroup;
    //    @ManyToOne
//    @JoinColumn(name="INCENTIVE_PROGRAM_ID")
    @Transient
    private Incentive incentive;


    /**
     * Default constructor.
     */
    public IncentivePackingSale() {
        super();
    }


}
