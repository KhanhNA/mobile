package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "kpi_description")
@Data
@NoArgsConstructor
public class KpiDescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "lang_id",nullable = false)
    private Integer langId;

    @Column(name = "lang_code",nullable = false)
    private String langCode;

    @Column(name = "kpi_id", nullable = false)
    private Long kpiId;
}
