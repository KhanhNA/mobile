package com.nextsolutions.dcommerce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "order_transfer_amount")
@Data
@NoArgsConstructor()
@AllArgsConstructor
@Builder
public class OrderTransferAmount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "status")
    private Integer status;

    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "distributor_wallet_id")
    private Long distributorWalletId;

}
