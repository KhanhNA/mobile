package com.nextsolutions.dcommerce.model;

import com.nextsolutions.dcommerce.shared.dto.resource.RecommendSearchResource;
import io.swagger.models.auth.In;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "recommend_search")
public class RecommendSearch implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "recommend_id", nullable = false)
    private Long recommendId;

    @Column(name = "from_date")
    private LocalDateTime fromDate;

    @Column(name = "to_date")
    private LocalDateTime toDate;

    @Column(name = "type")
    private Integer type;

    @Column(name = "status")
    private Integer status;

}
