package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-21 09:39
 */
@Entity
@Table(name = "groupon_register")
@Data
public class GrouponRegister {


    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;


    /**
     * 
     */
    @Column(name = "MERCHANT_L1_ID")
    private Long merchantL1Id;

    @Column(name = "INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;

    @Column(name = "PACKING_PRODUCT_ID")
    private Long packingProductId;

    @Column(name = "TOTAL_QUANTITY")
    private Double totalQuantity;

    @Column(name = "TOTAL_AMOUNT")
    private Double totalAmount;

    @Column(name = "TOTAL_INCENTIVE")
    private Double totalIncentive;


    @Column(name = "INCENTIVE_AMOUNT")
    private Double incentiveAmount;

    @Column(name = "INCENTIVE_PERCENT")
    private Double incentivePercent;

    @Column(name = "TOTAL_QUANTITY_NEXT")
    private Double totalQuantityNext;

    @Column(name = "TOTAL_AMOUNT_NEXT")
    private Double totalAmountNext;

    @Column(name = "INCENTIVE_AMOUNT_NEXT")
    private Double incentiveAmountNext;

    @Column(name = "INCENTIVE_PERCENT_NEXT")
    private Double incentivePercentNext;

    @Column(name = "INCENTIVE_LEVEL_ID")
    private Long incentiveLevelId;

    @Column(name = "INCENTIVE_LEVEL_ID_NEXT")
    private Long incentiveLevelIdNext;

    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;

    @Transient
    private String promotionName;
}
