package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "recommend_search_group")
public class RecommendSearchGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "recommend_search_id")
    private Long recommendSearchId;

    @Column(name = "merchant_group_id")
    private Long merchantGroupId;

}
