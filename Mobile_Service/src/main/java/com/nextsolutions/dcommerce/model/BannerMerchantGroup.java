package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "banner_merchant_group")
public class BannerMerchantGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "banner_id", nullable = false)
    private Long bannerId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "banner_id", insertable = false, updatable = false)
    private Banner banner;

    @Column(name = "merchant_group_id", nullable = false)
    private Long merchantGroupId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_group_id", insertable = false, updatable = false)
    private MerchantGroup merchantGroup;
}
