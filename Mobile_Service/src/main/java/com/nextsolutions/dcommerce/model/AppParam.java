package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-21 09:39
 */
@Entity
@Table(name = "App_Param")
@Data
public class AppParam {


    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "APP_PARAM_GROUP_ID")
    private Long appParamGroupId;

    /**
     * 
     */
    @Column(name = "CODE")
    private String code; //1-Dl cap 1;2-DL cap 2

    /**
     * 
     */
    @Column(name = "NAME")
    private String name;

    @Column(name = "GROUP_CODE")
    private String groupCode;
    @Column(name = "Type")
    private String type;
    @Column(name = "Description")
    private String description;
    @Column(name = "Status")
    private Integer staus;
    @Column(name = "ORD")
    private Integer ord;



}
