package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@Table(name = "temporary_manufacturer_description")
public class TemporaryManufacturerDescription implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(insertable = false, name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DATE_CREATED")
    private LocalDateTime dateCreate;

    @Column(name = "DATE_MODIFIED")
    private LocalDateTime dateModified;

    @Column(name = "UPDT_ID")
    private String updtId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "DATE_LAST_CLICK")
    private LocalDateTime dateLastClick;

    @Column(name = "MANUFACTURERS_URL")
    private String manufacturersUrl;

    @Column(name = "URL_CLICKED")
    private Integer urlClicked;

    @Column(name = "LANGUAGE_ID", nullable = false)
    private Long languageId;

    @Column(name = "MANUFACTURER_ID", nullable = false)
    private Long manufacturerId;

    @Column(name = "URL_IMAGE")
    private String urlImage;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "DESCRIPTION_ID")
    private Long descriptionId;

    @Column(name = "TIME_REQUEST")
    private Date timeRequest;
}
