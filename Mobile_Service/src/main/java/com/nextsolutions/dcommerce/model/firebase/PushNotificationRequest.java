package com.nextsolutions.dcommerce.model.firebase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.nextsolutions.dcommerce.model.NotificationMerchant;
import com.nextsolutions.dcommerce.model.order.OrderPackingV2;
import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PushNotificationRequest {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String title;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String topic;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String token;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String senderId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String senderName;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer typeId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long actionId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String customData;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long merchantReceiverId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String click_action;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String orderPackingName;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String orderQuantity;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String orderPackingUrl;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String merchantNumberPhone;

    private String sound = "notification_bell";


}
