package com.nextsolutions.dcommerce.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "merchant_adhere")
@Getter
@Setter
@Entity
public class MerchantAdhere {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "album_id")
    private Long albumId;

    @ManyToOne
    @JoinColumn(name = "merchant_group_id", referencedColumnName = "id")
    private MerchantGroup merchantGroup;

    @Column(name = "merchant_code")
    private String merchantCode;
}
