package com.nextsolutions.dcommerce.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "app_unit")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppUnit {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "value")
    private String value;
}
