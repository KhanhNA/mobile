package com.nextsolutions.dcommerce.model.loyalty;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "loyalty")
@Data
@EqualsAndHashCode(of = "id")
public class Loyalty {
    public static int STATUS_DRAFT = 0;
    public static int STATUS_ACT = 1;
    public static int STATUS_DEACT = 2;

    public static int TYPE_PACKING = 1;
    public static int TYPE_ORDER = 2;
    /**
     *
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     *
     */
    @Column(name = "CODE")
    private String code;

    /**
     *
     */
    @Column(name = "NAME")
    private String name;

    /**
     *
     */
    @Column(name = "STATUS")
    private Integer status;

    /**
     * Loại khuyến mại: 1: KM theo sp, 2: KM theo don hnag, 3: groupon
     */
    @Column(name = "TYPE")
    private Integer type;

    /**
     *
     */
    @Column(name = "FORMAT")
    private String format;

    /**
     *
     */
    @Column(name = "FROM_DATE")
    private LocalDateTime fromDate;

    /**
     *
     */
    @Column(name = "TO_DATE")
    private LocalDateTime toDate;

    /**
     *
     */
    @Column(name = "DESCRIPTION")
    private String description;

    /**
     *
     */
    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;

    /**
     *
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     *
     */
    @Column(name = "UPDATE_DATE")
    private LocalDateTime updateDate;

    /**
     *
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;


    /**
     * Loại loyalty: 1- trả trên đơn hàng, 2 - trả vào wallet
     */
    @Column(name = "LOYALTY_TYPE")
    private Integer loyaltyType;

    //    @OneToMany(mappedBy="loyalty")
    @Transient
    List<LoyaltyPackingSale> loyaltyPackingSales;

    //
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOYALTY_ID", referencedColumnName = "id", insertable = false, updatable = false)
    List<LoyaltyDescription> descriptions;


    //    @OneToMany(fetch = FetchType.LAZY,mappedBy="loyalty")
    @Transient
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "PACKING_PRODUCT_ID")
    List<LoyaltyLevel> loyaltyLevels;

    //    @OneToMany(fetch = FetchType.LAZY,mappedBy="loyalty")
    @Transient
    List<LoyaltyMerchantType> merchantTypes;


}
