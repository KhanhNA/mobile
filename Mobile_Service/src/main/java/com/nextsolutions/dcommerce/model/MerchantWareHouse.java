package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "merchant_warehouse")
@Data
@NoArgsConstructor()
public class MerchantWareHouse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "merchant_id")
    private Long merchantId;

    @NotEmpty(message = "StoreCode is not null")
    @Column(name = "store_code")
    private String storeCode;

    @NotEmpty(message = "storeName is not null")
    @Column(name = "store_name")
    private String storeName;

    @NotEmpty(message = "storeAddress is not null")
    @Column(name = "store_address")
    private String storeAddress;

    @Column(name = "city")
    private String city;

    @Column(name = "phone")
    private String phone;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "longitude")
    private Double longitude;


}
