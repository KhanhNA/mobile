package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nextsolutions.dcommerce.shared.dto.composition.AttributeComposition;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@SqlResultSetMapping(
  name = "AttributeComposition",
  classes = @ConstructorResult(
    targetClass = AttributeComposition.class,
    columns = {
      @ColumnResult(name = "attributeId", type = Long.class),
      @ColumnResult(name = "attributeCode", type = String.class),
      @ColumnResult(name = "attributeName", type = String.class),
      @ColumnResult(name = "attributeType", type = Integer.class),
      @ColumnResult(name = "attributeValueId", type = Long.class),
      @ColumnResult(name = "attributeValueCode", type = String.class),
      @ColumnResult(name = "attributeValueName", type = String.class)
    }
  )
)

@Table(name = "attribute")
@Data
@Entity
public class Attribute implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "type", nullable = false)
    private Integer type;

    @Column(name = "status")
    private Integer status;

    @Column(name = "request")
    private Integer request;

    @Column(name = "status_process")
    private Integer statusProcess;

    @Column(name = "value")
    private String value;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "attribute", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AttributeDescription> descriptions;

}
