package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name="coupon_incentive")
public class CouponIncentive implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "incentive_program_id", nullable = false)
    private Long incentiveProgramId;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "type", nullable = false)
    private Integer type;

    @Column(name = "required_minimum_amount", nullable = false)
    private BigDecimal requiredMinimumAmount;

    @Column(name = "limited_quantity")
    private Long limitedQuantity;

    @Column(name = "discount_percent")
    private BigDecimal discountPercent;

    @Column(name = "discount_price")
    private BigDecimal discountPrice;

    @Column(name = "limited_price")
    private BigDecimal limitedPrice;

    @Column(name = "create_user")
    private String createUser;

    @Column(name = "create_date")
    private LocalDateTime createDate;

}
