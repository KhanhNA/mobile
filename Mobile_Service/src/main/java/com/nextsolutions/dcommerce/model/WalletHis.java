package com.nextsolutions.dcommerce.model;

import com.nextsolutions.dcommerce.shared.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;


/**
 * - khi thực hiện order thành công thì sẽ đẩy tiền chiết khấu sang ví
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
@Entity
@Table(name = "wallet_his")
@Data
@EqualsAndHashCode(of = "walletHisId")

public class WalletHis  extends BaseDto {

    /**
     * 
     */
    @Id
    @Column(name = "WALLET_HIS_ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long walletHisId;

    /**
     * 
     */
    @Column(name = "ORDER_ID")
    private Long orderId;

    /**
     * 
     */
    @Column(name = "AMOUNT")
    private Double amount;

    /**
     * 
     */
    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;

    /**
     * 
     */
    @Column(name = "WALLET_ID")
    private Long walletId;

    public WalletHis() {
    }

    public WalletHis(Long orderId, Double amount, LocalDateTime createDate, Long walletId) {
        this.orderId = orderId;
        this.amount = amount;
        this.createDate = createDate;
        this.walletId = walletId;
    }
}
