package com.nextsolutions.dcommerce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


@Data
@Entity
@Table(name = "order_template")
@NoArgsConstructor()
@AllArgsConstructor
@Builder
public class OrderTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "merchant_id")
    private Long merchantId;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "packing_product_id")
    private Long packingProductId;

    @Column(name = "order_quantity")
    private Double orderQuantity;

    @Column(name = "payment_method_id")
    private Long paymentMethodId;

    @Column(name = "ship_method_id")
    private Long shipMethodId;

    @Column(name = "name")
    private String name;

}
