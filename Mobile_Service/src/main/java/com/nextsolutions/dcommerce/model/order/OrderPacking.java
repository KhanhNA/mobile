package com.nextsolutions.dcommerce.model.order;

import com.nextsolutions.dcommerce.shared.dto.OrderPackingInfo;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


@SqlResultSetMapping(
        name = "orderPackingInfoMapping",
        classes = {
                @ConstructorResult(
                        targetClass = OrderPackingInfo.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "productCode", type = String.class),
                                @ColumnResult(name = "productName", type = String.class),
                                @ColumnResult(name = "packingProductCode", type = String.class),
                                @ColumnResult(name = "packingProductName", type = String.class),
                                @ColumnResult(name = "quantity", type = Integer.class),
                                @ColumnResult(name = "price", type = BigDecimal.class),
                                @ColumnResult(name = "vat", type = BigDecimal.class),
                                @ColumnResult(name = "total", type = BigDecimal.class),
                        }),
        }
)

/**
 * The persistent class for the order_packing database table.
 */
@Entity
@Data
@Table(name = "order_packing")
public class OrderPacking implements Serializable {
    private static final long serialVersionUID = 1L;
    private Double amount;
    @Id
    @Column(name = "ORDER_PACKING_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderPackingId;

    @Column(name = "INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;

    @Column(name = "ONETIME_CHARGE")
    private Double onetimeCharge;

    @Column(name = "ORDER_ID")
    private Long orderId;

    @Column(name = "PACKING_PRODUCT_ID")
    private Long packingProductId;

    private Double price;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "PRODUCT_SKU")
    private String productSku;


    @Column(name = "INCENTIVE_AMOUNT")
    private Double incentiveAmount;

    @Column(name = "INCENTIVE_TOTAL")
    private Double incentiveTotal;

    @Column(name = "IS_INCENTIVE")
    private Integer isIncentive;

    private Double quantity;

    private Double vat;

    @Column(name = "INCENTIVE_COUPON")
    private Double incentiveCoupon;
    @Column(name = "IS_DEFAULT")
    private int isDefault;

    public OrderPacking() {
    }


}
