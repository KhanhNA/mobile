package com.nextsolutions.dcommerce.model.order;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Getter
@Setter
@Table(name = "order_packing_v2")
public class OrderPackingV2 implements Serializable {
    private static final long serialVersionUID = 1L;

    private BigDecimal amount;
    @Id
    @Column(name = "ORDER_PACKING_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderPackingId;

    @Column(name = "INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;

    @Column(name = "INCENTIVE_PROGRAM_DETAIL_ID")
    private Long incentiveProgramDetailId;

//    @Column(name = "ONETIME_CHARGE")
//    private Double onetimeCharge;

    @Column(name = "ORDER_ID")
    private Long orderId;

    @Column(name = "PACKING_PRODUCT_ID")
    private Long packingProductId;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "price_sale")
    private BigDecimal priceSale;

    @Column(name = "origin_price")
    private BigDecimal orgPrice;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "PRODUCT_SKU")
    private String productSku;


    @Column(name = "INCENTIVE_AMOUNT")
    private BigDecimal incentiveAmount = new BigDecimal(0);

    @Column(name = "INCENTIVE_TOTAL")
    private Double incentiveTotal;

    @Column(name = "IS_INCENTIVE")
    private Integer isIncentive;

    @Column(name = "order_quantity")
    private Integer orderQuantity;

    @Column(name = "bonus_quantity")
    private Integer bonusQuantity;

    private Double vat;

    @Column(name = "INCENTIVE_COUPON")
    private Double incentiveCoupon;

    @Column(name = "IS_DEFAULT")
    private int isDefault;

    @Transient
    private Integer quantity;

    @Transient
    private String packingUrl;
    @Transient
    private String packingProductCode;
    @Transient
    private Long distributorWalletId;
    @Transient
    private Integer remainIncentive;

}
