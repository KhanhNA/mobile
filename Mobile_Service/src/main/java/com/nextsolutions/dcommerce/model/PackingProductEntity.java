package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingGroupPacking;
import com.nextsolutions.dcommerce.model.incentive.IncentivePackingSale;
import com.nextsolutions.dcommerce.model.loyalty.LoyaltyPackingSale;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "packing_product")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PackingProductEntity implements Serializable {

    protected static final String PK = "packingProductId";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PACKING_PRODUCT_ID")
    private Long packingProductId;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", insertable = false, updatable = false)
    private ProductEntity product;

    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME")
    private String name;

    @Column(name = "VAT")
    private Float vat;

    @Column(name = "ORG_PRICE", precision = 10)
    private Double orgPrice;

    @Column(name = "DISCOUNT_ORIGINAL_PRICE")
    private Double discountOriginalPrice;

    @Column(name = "DISCOUNT_PERCENT_ORIGINAL_PRICE")
    private Double discountPercentOriginalPrice;

    @Column(name = "DISCOUNT_FROM_DATE")
    private LocalDateTime discountFromDate;

    @Column(name = "DISCOUNT_TO_DATE")
    private LocalDateTime discountToDate;

    @Column(name = "PRICE", precision = 10)
    private Double price;

    @Column(name = "PROMOTION_SALE_PRICE")
    private Double promotionSalePrice;

    @Column(name = "DISCOUNT_PERCENT", precision = 10)
    private Double discountPercent;

    @Column(name = "FROM_DATE")
    private LocalDateTime fromDate;

    @Column(name = "TO_DATE")
    private LocalDateTime toDate;

    @Column(name = "MARKET_PRICE", precision = 10)
    private Double marketPrice;

    @Column(name = "PACKING_URL")
    private String packingUrl;

    @Column(name = "UOM")
    private String uom;

    @Column(name = "PACKING_TYPE_ID")
    private Long packingTypeId;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "PACKING_TYPE_ID", insertable = false, updatable = false)
    private PackingTypeEntity packingType;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "DISTRIBUTOR_ID")
    private Long distributorId;

    @Column(name = "DISTRIBUTOR_WALLET_ID")
    private Long distributorWalletId;

    @Column(name = "DISTRIBUTOR_CODE")
    private String distributorCode;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "packingProduct")
    private List<PackingPrice> packingPrices;

    @Transient
    private Double priceSale;

    @Transient
    private String productName;

    @Transient
    private Double quantity;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
