package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-21 09:39
 */
@Entity
@Table(name = "App_Param_Group")
@Data
public class AppParamGroup {


    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    /**
     * 
     */
    @Column(name = "CODE")
    private String code; //1-Dl cap 1;2-DL cap 2

    /**
     * 
     */
    @Column(name = "NAME")
    private String name;

    @Column(name = "Description")
    private String description;
    @Column(name = "Status")
    private Integer staus;



}
