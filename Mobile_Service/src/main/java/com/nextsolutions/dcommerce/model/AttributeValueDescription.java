package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Entity
@Data
@Table(name = "attribute_value_description")
public class AttributeValueDescription implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", insertable = false, nullable = false)
  private Long id;

  @Column(name = "attribute_value_id", nullable = false)
  private Long attributeValueId;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "attribute_value_id", insertable = false, updatable = false)
  private AttributeValue attributeValue;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "language_id", nullable = false)
  private Integer languageId;
}
