package com.nextsolutions.dcommerce.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * - Mỗi đại lý (merchant) sẽ có 1 ví (wallet)
 *
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
@Entity
@Table(name = "ship_method")
@Data
public class ShipMethod {


    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SHIP_METHOD_Id")
    private Long shipMethodId;

    /**
     *
     */
    @Column(name = "SHIP_METHOD_NAME")
    private String shipMethodName;


}
