package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Table(name = "incentive_program_group")
@Data
@Entity
@ToString(exclude = {"incentiveProgram", "incentiveProgramDetailInformationGroup"})
@EqualsAndHashCode(exclude = {"incentiveProgram", "incentiveProgramDetailInformationGroup"})
public class IncentiveProgramGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "incentive_program_id", nullable = false)
    private Long incentiveProgramId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "incentive_program_id", insertable = false, updatable = false)
    private IncentiveProgram incentiveProgram;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "manufacturer_id")
    private Long manufacturerId;

    @Column(name = "discount_type")
    private Integer discountType;

    @Column(name = "discount_price")
    private BigDecimal discountPrice;

    @Column(name = "discount_percent")
    private BigDecimal discountPercent;

    @Column(name = "default_limited_quantity")
    private BigDecimal defaultLimitedQuantity;

    @OneToMany(mappedBy = "incentiveProgramGroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<IncentiveProgramDetailInformation> incentiveProgramDetailInformationGroup;
}
