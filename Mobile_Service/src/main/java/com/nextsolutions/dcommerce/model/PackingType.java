package com.nextsolutions.dcommerce.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "packing_type")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PackingType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PACKING_TYPE_ID")
    private Long packingTypeId;

    @Column(name = "CODE", unique = true)
    private String code;

    @Column(name = "QUANTITY")
    private Integer quantity;

    @Transient
    private Set<PackingProduct> packingProduct;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
