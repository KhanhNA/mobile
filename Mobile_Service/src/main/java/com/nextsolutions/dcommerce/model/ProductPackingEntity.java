package com.nextsolutions.dcommerce.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "packing_product")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductPackingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PACKING_PRODUCT_ID")
    private Long packingProductId;

    @Column(name = "CODE")
    private String code;
}
