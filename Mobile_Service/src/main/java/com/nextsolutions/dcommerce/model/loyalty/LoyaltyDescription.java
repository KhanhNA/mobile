package com.nextsolutions.dcommerce.model.loyalty;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "loyalty_description")
@Data
@EqualsAndHashCode(of = "id")
public class LoyaltyDescription {
    /**
     *
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //    @ManyToOne
    @Column(name = "LOYALTY_ID")
    private Long loyaltyId;

    @Column(name = "LANG_ID")
    private Long langId;

    @Column(name = "LANG_CODE")
    private String langCode;

    /**
     *
     */
    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;
}
