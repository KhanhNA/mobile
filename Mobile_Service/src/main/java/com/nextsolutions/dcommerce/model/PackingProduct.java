package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nextsolutions.dcommerce.shared.dto.PackingProductDTO;
import com.nextsolutions.dcommerce.shared.dto.PackingProductSearchResult;
import com.nextsolutions.dcommerce.shared.dto.resource.PackingProductResource;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


@SqlResultSetMapping(
        name = "PackingProductDTOMappingAll",
        classes = {
                @ConstructorResult(targetClass = PackingProductDTO.class,
                        columns = {
                                @ColumnResult(name = "productId", type = Long.class),
                                @ColumnResult(name = "packingUrl", type = String.class),
                                @ColumnResult(name = "packingProductId", type = Long.class),
                                @ColumnResult(name = "price", type = Double.class),
                                @ColumnResult(name = "discountPercent", type = Double.class),
                                @ColumnResult(name = "orgPrice", type = Long.class),
                                @ColumnResult(name = "marketPrice", type = Long.class),
                                @ColumnResult(name = "quantity", type = Long.class),
                                @ColumnResult(name = "packingProductCode", type = String.class)
                        }),
        }
)

@SqlResultSetMapping(
        name = "PackingProductResource",
        classes = {
                @ConstructorResult(targetClass = PackingProductResource.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "productId", type = Long.class),
                                @ColumnResult(name = "packingTypeId", type = Long.class),
                                @ColumnResult(name = "code", type = String.class),
                                @ColumnResult(name = "name", type = String.class),
                                @ColumnResult(name = "imageUrl", type = String.class),
                                @ColumnResult(name = "originalPrice", type = Double.class),
                                @ColumnResult(name = "salePrice", type = Double.class),
                                @ColumnResult(name = "marketPrice", type = Double.class),
                                @ColumnResult(name = "uom", type = String.class)
                        }),
        }
)

@SqlResultSetMapping(
        name = "PackingProductSearchResult",
        classes = {
                @ConstructorResult(targetClass = PackingProductSearchResult.class,
                        columns = {
                                @ColumnResult(name = "packingId", type = Long.class),
                                @ColumnResult(name = "packingCode", type = String.class),
                                @ColumnResult(name = "productImageUrl", type = String.class),
                                @ColumnResult(name = "productName", type = String.class),
                                @ColumnResult(name = "packingQuantity", type = Integer.class),
                                @ColumnResult(name = "packingOriginalPrice", type = BigDecimal.class),
                                @ColumnResult(name = "packingSalePrice", type = BigDecimal.class),
                                @ColumnResult(name = "isExistsInOtherIncentivePrograms", type = Boolean.class),
                        }),
        }
)
@Entity
@Table(name = "packing_product")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PackingProduct implements Serializable {

    protected static final String PK = "packingProductId";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PACKING_PRODUCT_ID")
    private Long packingProductId;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", insertable = false, updatable = false)
    private ProductEntity product;

    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME")
    private String name;

    @Column(name = "VAT")
    private Float vat;

    @Column(name = "ORG_PRICE", precision = 10)
    private Double orgPrice;

    @Column(name = "DISCOUNT_ORIGINAL_PRICE")
    private Double discountOriginalPrice;

    @Column(name = "DISCOUNT_PERCENT_ORIGINAL_PRICE")
    private Double discountPercentOriginalPrice;

    @Column(name = "DISCOUNT_FROM_DATE")
    private LocalDateTime discountFromDate;

    @Column(name = "DISCOUNT_TO_DATE")
    private LocalDateTime discountToDate;

    @Column(name = "PRICE", precision = 10)
    private BigDecimal price;

    @Column(name = "PROMOTION_SALE_PRICE")
    private Double promotionSalePrice;

    @Column(name = "DISCOUNT_PERCENT", precision = 10)
    private Double discountPercent;

    @Column(name = "FROM_DATE")
    private LocalDateTime fromDate;

    @Column(name = "TO_DATE")
    private LocalDateTime toDate;

    @Column(name = "MARKET_PRICE", precision = 10)
    private Double marketPrice;

    @Column(name = "PACKING_URL")
    private String packingUrl;

    @Column(name = "UOM")
    private String uom;

//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    @OneToMany(cascade = CascadeType.ALL)
//    @EqualsAndHashCode.Exclude
//    @ToString.Exclude
//    @JoinColumn(name = "PACKING_PRODUCT_ID")
//    private Set<IncentivePackingSale> incentivePackingSales;

    @Column(name = "PACKING_TYPE_ID")
    private Long packingTypeId;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "PACKING_TYPE_ID", insertable = false, updatable = false)
    private PackingTypeEntity packingType;

    @Column(name = "STATUS")
    private Integer status;

//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    @EqualsAndHashCode.Exclude
//    @ToString.Exclude
//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "PACKING_PRODUCT_ID")
//    private Set<LoyaltyPackingSale> loyaltyPackingSales;

//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    @EqualsAndHashCode.Exclude
//    @ToString.Exclude
//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "PACKING_PRODUCT_ID")
//    private Set<IncentivePackingGroupPacking> incentivePackingGroupPackings;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "packingProduct")
    private List<PackingPrice> packingPrices;

    @ManyToOne
    @JoinColumn(name = "DISTRIBUTOR_ID", insertable = false, updatable = false)
    private DistributorLink distributor;

    @Column(name = "DISTRIBUTOR_ID")
    private Long distributorId;

    @Column(name = "DISTRIBUTOR_CODE")
    private String distributorCode;

    @Column(name = "DISTRIBUTOR_WALLET_ID")
    private Long distributorWalletId;

    @Column(name = "WEIGHT", precision = 2)
    private BigDecimal weight;

    @Column(name = "WIDTH", precision = 2)
    private BigDecimal width;

    @Column(name = "HEIGHT", precision = 2)
    private BigDecimal height;

    @Column(name = "LENGTH", precision = 2)
    private BigDecimal length;

    @Column(name = "LENGTH_CLASS")
    private String lengthClass;

    @Column(name = "WEIGHT_CLASS")
    private String weightClass;

    @Column(name = "WARNING_THRESHOLD")
    private Integer waningThreshold;

    @Column(name = "PRODUCT_LIFECYCLE")
    private Integer lifecycle;

    @Column(name = "CALCULATED_WEIGHT", precision = 2)
    private BigDecimal calculatedWeight;

    @Column(name = "CALCULATED_WIDTH", precision = 2)
    private BigDecimal calculatedWidth;

    @Column(name = "CALCULATED_HEIGHT", precision = 2)
    private BigDecimal calculatedHeight;

    @Column(name = "CALCULATED_LENGTH", precision = 2)
    private BigDecimal calculatedLength;

    @Transient
    private Double priceSale;

    @Transient
    private String productName;

    @Transient
    public String packingName;

    @Transient
    private Double quantity;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false)
    private Boolean isSynchronization;

    public PackingProduct(
            Long id,
            String code,
            String name,
            String uom,
            String imageUrl,
            BigDecimal salePrice,
            Double marketPrice,
            Integer status
    ) {
        this.packingTypeId = id;
        this.code = code;
        this.name = name;
        this.uom = uom;
        this.packingUrl = imageUrl;
        this.price = salePrice;
        this.marketPrice = marketPrice;
        this.status = status;
    }
}
