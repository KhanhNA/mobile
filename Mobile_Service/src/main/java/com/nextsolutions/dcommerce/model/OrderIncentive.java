package com.nextsolutions.dcommerce.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "order_incentive")
@NoArgsConstructor
public class OrderIncentive {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "order_id")
    private Long orderId;
    @Column(name = "order_no")
    private String orderNo;
    @Column(name = "incentive_program_id")
    private Long incentiveProgramId;

    public OrderIncentive(Long orderId, String orderNo, Long incentiveProgramId) {
        this.orderId = orderId;
        this.orderNo = orderNo;
        this.incentiveProgramId = incentiveProgramId;
    }
}
