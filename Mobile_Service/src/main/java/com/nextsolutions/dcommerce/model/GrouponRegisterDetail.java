package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-21 09:39
 */
@Entity
@Table(name = "groupon_register_detail")
@Data
public class GrouponRegisterDetail {


    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;

    @Column(name = "MERCHANT_L1_ID")
    private Long merchantL1Id;

    @Column(name = "merchant_id")
    private Long merchantId;

    @Column(name = "groupon_register_id")
    private Long grouponRegisterId;

    @Column(name = "PACKING_PRODUCT_ID")
    private Long packingProductId;


    @Column(name = "register_quantity")
    private Integer registerQuantity;

    @Column(name = "register_amount")
    private Double registerAmount;

    @Column(name="last_register_date")
    private LocalDateTime lastRegisterDate;

    @Column(name="status")
    private Integer status;


    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;

}
