package com.nextsolutions.dcommerce.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "kpi_month")
@Getter
@Setter
public class KpiMonth {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "MONTH")
    private LocalDateTime month;

    @Column(name = "KPI_ID")
    private Long kpiId;

    @Column(name = "MERCHANT_ID")
    private Long merchantId;

    @Column(name = "PLAN")
    private Double plan;

    @Column(name = "DONE")
    private Double done;

    @Column(name = "BONUS")
    private Double bonus;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;

    @Column(name = "UPDATE_USER")
    private String updateUser;

    @Column(name = "UPDATE_DATE")
    private LocalDateTime updateDate;

    public KpiMonth() {
    }

    public KpiMonth(Long id, LocalDateTime month, Long kpiId, Long merchantId, Double plan, Double done, Double bonus, String createUser, LocalDateTime createDate, String updateUser, LocalDateTime updateDate) {
        this.id = id;
        this.month = month;
        this.kpiId = kpiId;
        this.merchantId = merchantId;
        this.plan = plan;
        this.done = done;
        this.bonus = bonus;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
    }
}

