package com.nextsolutions.dcommerce.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "packing_type")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PackingTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PACKING_TYPE_ID")
    private Long packingTypeId;

    @Column(name = "CODE")
    private String code;
}
