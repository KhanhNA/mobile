// Generated with g9.

package com.nextsolutions.dcommerce.model.loyalty;

import com.nextsolutions.dcommerce.model.PackingProduct;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name="loyalty_packing_group_packing")
@Entity
@Data
public class LoyaltyPackingGroupPacking implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @Column(name="LOYALTY_PACKING_GROUP_ID")
    private Long loyaltyPackingGroupId;

//    @Column(name="PACKING_PRODUCT_ID")
//    private Long packingProductId;


    @ManyToOne
    @JoinColumn(name = "PACKING_PRODUCT_ID")
    private PackingProduct packingProduct;



    @Column(name="MANDATORY")
    private Integer mandatory;

    @Column(name="MIN_QUANTITY")
    private Double minQuantity;

    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;

    /** Default constructor. */
    public LoyaltyPackingGroupPacking() {
        super();
    }


}
