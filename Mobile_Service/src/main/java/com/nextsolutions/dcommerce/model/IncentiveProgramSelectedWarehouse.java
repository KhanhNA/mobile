package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "incentive_program_selected_warehouse")
@Entity
@Data
@NoArgsConstructor
public class IncentiveProgramSelectedWarehouse implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "incentive_program_id")
    private Long incentiveProgramId;

    @Column(name = "warehouse_code")
    private String warehouseCode;

    public IncentiveProgramSelectedWarehouse(Long incentiveProgramId, String warehouseCode) {
        this.incentiveProgramId = incentiveProgramId;
        this.warehouseCode = warehouseCode;
    }
}
