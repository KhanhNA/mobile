package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-21 09:39
 */
@Entity
@Table(name = "Coupon")
@Data
public class Coupon {

    public enum Type {
        ALL(1),
        L1(2);

        public Integer value;
        Type(Integer value) {
            this.value = value;
        }

    }
    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;


    /**
     * 
     */
    @Column(name = "CODE")
    private String code;

    @Column(name = "COUPON_TYPE")
    private Integer couponType;//1- public, 2- byL1


    @Column(name = "TIME_TYPE")
    private Integer timeType;//1- oneTime, 2- manyTimes

    /**
     * 
     */
    @Column(name = "FROM_DATE")
    private LocalDateTime fromDate;

    @Column(name = "TO_DATE")
    private LocalDateTime toDate;
    @Column(name = "Status")
    private Integer status;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "PERCENT")
    private Double percent;



    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;
}
