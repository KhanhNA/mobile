package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "incentive_program_detail_information")
public class IncentiveProgramDetailInformation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "incentive_program_id")
    private Long incentiveProgramId;

    @Column(name = "incentive_program_group_id")
    private Long incentiveProgramGroupId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "incentive_program_group_id", insertable = false, updatable = false)
    private IncentiveProgramGroup incentiveProgramGroup;

    @Column(name = "packing_id")
    private Long packingId;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "manufacturer_id")
    private Long manufacturerId;

    @Column(name = "discount_type")
    private Integer discountType;

    @Column(name = "discount_price")
    private BigDecimal discountPrice;

    @Column(name = "discount_percent")
    private BigDecimal discountPercent;

    @Column(name = "limited_quantity")
    private Integer limitedQuantity;

    @Column(name = "is_flash_sale")
    private Boolean isFlashSale;

    @Column(name = "bonus_packing_id")
    private Long bonusPackingId;

    @Column(name = "required_minimum_purchase_quantity")
    private Integer requiredMinimumPurchaseQuantity;

    @Column(name = "bonus_packing_quantity")
    private Integer bonusPackingQuantity;

    @Column(name = "limited_packing_quantity")
    private Integer limitedPackingQuantity;

    @Column(name = "sold")
    private Integer sold;

    @Column(name = "CREATED_USER")
    private String createdUser;

    @Column(name = "CREATED_DATE")
    private LocalDateTime createdDate;

    @Column(name = "LATEST_MODIFIED_USER")
    private String latestModifiedUser;

    @Column(name = "LATEST_MODIFIED_DATE")
    private LocalDateTime latestModifiedDate;

    @OneToMany(mappedBy = "incentiveProgramDetailInformation")
    private List<IncentiveProgramFlashSaleDate> flashSaleDates;
}
