package com.nextsolutions.dcommerce.model.incentive;

import com.nextsolutions.dcommerce.model.MerchantGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "incentive_merchant_group")
@Getter
@Setter
@Entity
public class IncentiveMerchantGroup {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "INCENTIVE_PROGRAM_ID")
    private Long ictProgramId;

    @ManyToOne
    @JoinColumn(name = "merchant_group_id", referencedColumnName = "id")
    private MerchantGroup merchantGroup;

    @Column(name = "STATUS")
    private Integer status;
}
