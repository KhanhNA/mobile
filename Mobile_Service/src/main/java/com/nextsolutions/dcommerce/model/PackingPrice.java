package com.nextsolutions.dcommerce.model;

import com.nextsolutions.dcommerce.shared.dto.resource.PromotionPriceResource;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@SqlResultSetMapping(
        name = "PromotionPriceResource",
        classes = {
                @ConstructorResult(targetClass = PromotionPriceResource.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "packingProductId", type = Long.class),
                                @ColumnResult(name = "price", type = Double.class),
                                @ColumnResult(name = "fromDate", type = LocalDateTime.class),
                                @ColumnResult(name = "toDate", type = LocalDateTime.class),
                                @ColumnResult(name = "priceType", type = Integer.class),
                                @ColumnResult(name = "currencyCode", type = String.class),
                        }),
        }
)

@Getter
@Setter
@Entity
@Table(name = "packing_price")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PackingPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "packing_price_id")
    private Long id;

    @Column(name = "packing_product_id")
    private Long packingProductId;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "from_date")
    private LocalDateTime fromDate;

    @Column(name = "to_date")
    private LocalDateTime toDate;

    @Column(name = "price_type") // 1: IN | 2: OUT | 3: PROMOTION
    private Integer priceType;

    @ManyToOne
    @JoinColumn(name="currency_id", insertable = false, updatable = false)
    private Currency currency;

    @Column(name = "currency_code")
    private String currencyCode;

    @Column(name = "currency_id")
    private Long currencyId;

    @Column(name = "IS_SYNCHRONIZATION")
    private Boolean isSynchronization;

    @ManyToOne
    @JoinColumn(name="packing_product_id", insertable = false, updatable = false)
    private ProductPackingEntity packingProduct;

    @Column(name="created_time", insertable = false, updatable = false)
    private LocalDateTime createdTime;

    @Column(name="latest_modified_time", insertable = false, updatable = false)
    private LocalDateTime latestModifiedTime;

    @Column(name = "exchange_rate")
    private BigDecimal exchangeRate;

    @Column(name = "unit_id")
    private Long unitId;

    @Column(name = "status")
    private Long status;

    @Column(name = "action")
    private Long action;

    @Column(name = "value_change")
    private String valueChange;
}
