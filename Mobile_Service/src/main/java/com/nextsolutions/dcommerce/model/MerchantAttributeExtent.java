package com.nextsolutions.dcommerce.model;

import lombok.*;

import javax.persistence.*;

@Table(name = "merchant_attribute_extent")
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MerchantAttributeExtent {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "merchant_id")
    private Long merchantId;

    @Column(name = "att_id")
    private Long attId;

    @Column(name = "att_value_id")
    private Long attValueId;
}
