package com.nextsolutions.dcommerce.model.charts;

import java.util.List;

public class ChartBarVerticalResult {
	private String name;
	private List<ChartBarVerticalResultSerie> series;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ChartBarVerticalResultSerie> getSeries() {
		return this.series;
	}

	public void setSeries(List<ChartBarVerticalResultSerie> series) {
		this.series = series;
	}

}
