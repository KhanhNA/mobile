package com.nextsolutions.dcommerce.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Table(name = "merchant_active_code")
@Entity
public class MerchantActiveCode implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "parent_merchant_code")
    private String parentMerchantCode;

    @Column(name = "invite_mobile_phone")
    private String inviteMobilePhone;

    @Column(name = "create_date", insertable = false)
    private Date createDate;

    @Column(name = "active_code")
    private String activeCode;
}
