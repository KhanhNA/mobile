package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "incentive_program_merchant_group")
@Data
@NoArgsConstructor
public class IncentiveProgramMerchantGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "incentive_program_id")
    private Long incentiveProgramId;

    @Column(name = "merchant_group_id")
    private Long merchantGroupId;

    public IncentiveProgramMerchantGroup(Long incentiveProgramId, Long merchantGroupId) {
        this.incentiveProgramId = incentiveProgramId;
        this.merchantGroupId = merchantGroupId;
    }
}
