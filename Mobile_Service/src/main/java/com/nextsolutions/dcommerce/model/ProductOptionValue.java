package com.nextsolutions.dcommerce.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "product_option_value")
public class ProductOptionValue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_OPTION_VALUE_ID")
    private Long id;

    @Column(name = "PRODUCT_OPTION_VALUE_CODE")
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_OPTION_ID", insertable = false, updatable = false)
    private ProductOption option;

    @Column(name = "PRODUCT_OPTION_ID")
    private Long productOptionId;

    @OneToMany(mappedBy = "optionValue", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ProductOptionValueDescription> descriptions;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
