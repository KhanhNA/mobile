package com.nextsolutions.dcommerce.model.loyalty;

import com.nextsolutions.dcommerce.model.PackingProduct;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Table(name="loyalty_packing_sale")
@Entity
@Data
public class LoyaltyPackingSale implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="LOYALTY_LEVEL_ID", insertable = false, updatable = false)
    private Long loyaltyLevelId;

    @Column(name="PACKING_PRODUCT_ID",  insertable = false, updatable = false)
    private Long packingProductId;

    @Column(name="LOYALTY_ID")
    private Long loyaltyId;

    @Column(name="LOYALTY_PACKING_GROUP_ID", insertable = false, updatable = false)
    private Long loyaltyPackingGroupId;

    @Column(name="OBJ_TYPE")
    private Integer objType;//1: packing_product;2: loyalty_packing_group

    @Column(name="STATUS")
    private Long status;

    @Column(name="FROM_DATE")
    private LocalDateTime fromDate;

    @Column(name="TO_DATE")
    private LocalDateTime toDate;

    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;

    @Transient
    public String getCode() {
        if(objType == 1){
            return this.packingProduct == null? null: this.packingProduct.getCode();
        }else{
            return this.loyaltyPackingGroup == null ? null: this.loyaltyPackingGroup.getCode();
        }

    }

//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Transient
    public String getName() {
        if(objType == 1){
            return this.packingProduct == null? null: this.packingProduct.getName();
        }else{
            return this.loyaltyPackingGroup == null ? null: this.loyaltyPackingGroup.getName();
        }
    }

    //    @ManyToOne
//    @JoinColumn(name="LOYALTY_LEVEL_ID")
//    private LoyaltyLevel loyaltyLevel;
//
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name="PACKING_PRODUCT_ID")
    private PackingProduct packingProduct;

    @ManyToOne
    @JoinColumn(name="LOYALTY_PACKING_GROUP_ID")
//    @Transient
    private LoyaltyPackingGroup loyaltyPackingGroup;
//    @ManyToOne
//    @JoinColumn(name="loyalty_PROGRAM_ID")
    @Transient
    private Loyalty loyalty;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "LOYALTY_PACKING_SALE_ID")
    private Set<LoyaltyLevel> loyaltyLevelSet;



    /** Default constructor. */
    public LoyaltyPackingSale() {
        super();
    }

    


}
