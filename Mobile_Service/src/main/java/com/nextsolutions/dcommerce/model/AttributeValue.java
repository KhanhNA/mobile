package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nextsolutions.dcommerce.shared.dto.resource.AttributeResource;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@SqlResultSetMapping(
  name = "AttributeValueResource",
  classes = @ConstructorResult(
    targetClass = AttributeResource.class,
    columns = {
      @ColumnResult(name = "id"),
      @ColumnResult(name = "code"),
      @ColumnResult(name = "name"),
    }
  )
)

@Data
@Entity
@Table(name = "attribute_value")
public class AttributeValue implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", insertable = false, nullable = false)
  private Long id;

  @Column(name = "attribute_id", nullable = false)
  private Long attributeId;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "attribute_id", insertable = false, updatable = false)
  private Attribute attribute;

  @Column(name = "code", nullable = false)
  private String code;

  @Column(name = "status")
  private Integer status;

  @Column(name = "request")
  private Integer request;

  @Column(name = "status_process")
  private Integer statusProcess;

  @Column(name = "value")
  private String value;

  @JsonIgnore
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @OneToMany(mappedBy = "attributeValue", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private List<AttributeValueDescription> descriptions;
}
