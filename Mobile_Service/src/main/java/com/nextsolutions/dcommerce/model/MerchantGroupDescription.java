package com.nextsolutions.dcommerce.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Entity
@Table(name = "merchant_group_description")
@Data
public class MerchantGroupDescription implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "merchant_group_id", nullable = false)
    private Long merchantGroupId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_group_id", insertable = false, updatable = false)
    private MerchantGroup merchantGroup;

    @Column(name = "language_id", nullable = false)
    private Integer languageId;
}
