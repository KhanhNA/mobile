package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "product_option_description")
public class ProductOptionDescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DESCRIPTION_ID")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_OPTION_ID")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private ProductOption option;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LANGUAGE_ID")
    private Long languageId;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false, updatable = false)
    private Boolean isSynchronization;
}
