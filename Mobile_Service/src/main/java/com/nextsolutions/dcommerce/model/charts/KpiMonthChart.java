package com.nextsolutions.dcommerce.model.charts;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class KpiMonthChart implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5268204079045281276L;

	@Id
	private Long id;

	private LocalDateTime month;

	private Long merchantId;
	private String merchantName;

	private Long kpiId;
	private String kpiCode;

	private BigDecimal plan;

	private BigDecimal done;

	private BigDecimal bonus;

	public LocalDateTime getMonth() {
		return this.month;
	}

	public void setMonth(LocalDateTime month) {
		this.month = month;
	}

	public Long getMerchantId() {
		return this.merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return this.merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Long getKpiId() {
		return this.kpiId;
	}

	public void setKpiId(Long kpiId) {
		this.kpiId = kpiId;
	}

	public String getKpiCode() {
		return this.kpiCode;
	}

	public void setKpiCode(String kpiCode) {
		this.kpiCode = kpiCode;
	}

	public BigDecimal getPlan() {
		return this.plan;
	}

	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}

	public BigDecimal getDone() {
		return this.done;
	}

	public void setDone(BigDecimal done) {
		this.done = done;
	}

	public BigDecimal getBonus() {
		return this.bonus;
	}

	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}

}
