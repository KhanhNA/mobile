package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nextsolutions.dcommerce.shared.dto.MerchantGroupDTO;
import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupResource;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@SqlResultSetMapping(
        name = "MerchantGroupResource",
        classes = {
                @ConstructorResult(targetClass = MerchantGroupResource.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "code", type = String.class),
                                @ColumnResult(name = "name", type = String.class),
                                @ColumnResult(name = "isSelect", type = Integer.class),
                        }
                ),
        }
)
@SqlResultSetMapping(
        name = "MerchantGroupList",
        classes = {
                @ConstructorResult(targetClass = MerchantGroupDTO.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "code", type = String.class),
                                @ColumnResult(name = "name", type = String.class),
                        }
                ),
        }
)

@Entity
@Table(name = "merchant_group")
@Data
public class MerchantGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE")
    private String code; //1-Dl cap 1;2-DL cap 2

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "merchantGroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MerchantGroupDescription> descriptions;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "merchantGroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MerchantGroupAttribute> attributes;

    @Column(name = "NAME")
    private String name;

    @Column(name = "FROM_DATE")
    private LocalDateTime fromDate;

    @Column(name = "TO_DATE")
    private LocalDateTime toDate;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "Location_Business")
    private String locationBusiness;

    @Column(name = "area")
    private String area;

    @Column(name = "Electrical_Infrastructure")
    private String electricalInfrastructure;

    @Column(name = "Display_Device")
    private String displayDevice;

    @Column(name = "Refrigeration_Equipment")
    private String refrigerationEquipment;

}
