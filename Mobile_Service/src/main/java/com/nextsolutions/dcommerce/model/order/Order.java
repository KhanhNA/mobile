package com.nextsolutions.dcommerce.model.order;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nextsolutions.dcommerce.model.MerchantAddress;
import com.nextsolutions.dcommerce.model.PaymentMethod;
import com.nextsolutions.dcommerce.model.ShipMethod;
import com.nextsolutions.dcommerce.shared.dto.OrderDto;
import com.nextsolutions.dcommerce.ui.model.response.OrderDetailsResponseModel;
import com.nextsolutions.dcommerce.ui.model.response.OrderResponseModel;
import com.nextsolutions.dcommerce.utils.DeserializeDateHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@SqlResultSetMapping(
        name = "OrderDtoMapping",
        classes = {
                @ConstructorResult(targetClass = OrderDto.class,
                        columns = {
                                @ColumnResult(name = "orderId", type = Long.class),
                                @ColumnResult(name = "orderStatus", type = Integer.class),
                                @ColumnResult(name = "paymentStatus", type = Integer.class),
                                @ColumnResult(name = "deliveryStatus", type = Integer.class)
                        }),
        }
)

@SqlResultSetMapping(
        name = "OrderResponseMapping",
        classes = {
                @ConstructorResult(
                        targetClass = OrderResponseModel.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "merchantId", type = Long.class),
                                @ColumnResult(name = "merchantName", type = String.class),
                                @ColumnResult(name = "status", type = Integer.class),
                                @ColumnResult(name = "amount", type = BigDecimal.class),
                                @ColumnResult(name = "dateAdded", type = LocalDateTime.class),
                                @ColumnResult(name = "dateModified", type = LocalDateTime.class),
                                @ColumnResult(name = "logisticsCode", type = String.class),
                                @ColumnResult(name = "reason", type = String.class)
                        }),
        }
)

@SqlResultSetMapping(
        name = "OrderDetailsResponseMapping",
        classes = {
                @ConstructorResult(
                        targetClass = OrderDetailsResponseModel.class,
                        columns = {
                                @ColumnResult(name = "store", type = String.class),
                                @ColumnResult(name = "orderDate", type = LocalDateTime.class),
                                @ColumnResult(name = "paymentMethod", type = String.class),
                                @ColumnResult(name = "shippingMethod", type = String.class),
                                @ColumnResult(name = "merchantName", type = String.class),
                                @ColumnResult(name = "merchantGroup", type = String.class),
                                @ColumnResult(name = "email", type = String.class),
                                @ColumnResult(name = "phoneNumber", type = String.class),
                                @ColumnResult(name = "orderId", type = Long.class),
                                @ColumnResult(name = "logisticsCode", type = String.class),
                        }),
        }
)

/**
 * Đơn hàng: - merchant: đại lý nào đặt đơn hàng này - payment_method: thanh toán bằng phương thức nào - recv_store_id: khi order merchant sẽ chọn kho nào nhận đơn hàng này
 *
 * @author ts-client01
 * Create at 2019-06-20 12:19
 */
@Entity
@Table(name = "orders")
@Data
@EqualsAndHashCode(of = "orderId")
public class Order implements Serializable {


    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ID")
    private Long orderId;

    /**
     * Mã đơn hàng
     */
    @Column(name = "ORDER_NO")
    private String orderNo;

    /**
     *
     */
    @Column(name = "MERCHANT_ID")
    private Long merchantId;

    @Column(name = "MERCHANT_CODE")
    private Long merchantCode;
    /**
     *
     */
    @Column(name = "MERCHANT_NAME")
    private String merchantName;
    @Column(name = "PAYMENT_METHOD_ID")
    private Long paymentMethodId;
    @Column(name = "SHIP_METHOD_ID")
    private Long shipMethodId;

    /**
     *
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PAYMENT_METHOD_ID", updatable = false, insertable = false)
    private PaymentMethod paymentMethod;

    /**
     *
     */
    @Column(name = "ORDER_INVOICE_ID")
    private Long orderInvoiceId;

    /**
     *
     */
    @ManyToOne
    @JoinColumn(name = "SHIP_METHOD_ID", updatable = false, insertable = false)
    private ShipMethod shipMethod;

    /**
     *
     */
    @Column(name = "SHIP_CHARGE")
    private Double shipCharge;

    /**
     *
     */
    @Column(name = "SHP_CART_ID")
    private Long shpCartId;

    /**
     *
     */
    @Column(name = "RECV_STORE_ID")
    private Long recvStoreId;

    /**
     *
     */
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "ORG_AMOUNT")
    private BigDecimal orgAmount;

    /**
     *
     */
    @Column(name = "VAT")
    private BigDecimal vat;

    /**
     *
     */
    @Column(name = "INCENTIVE_AMOUNT")
    private BigDecimal incentiveAmount;

    /**
     *
     */

    private Double quantity;

    /**
     *
     */
    @Column(name = "RECV_LAT")
    private Double recvLat;

    /**
     *
     */
    @Column(name = "RECV_LONG")
    private Double recvLong;

    /**
     *
     */
    @Column(name = "RECV_ADDR")
    private String recvAddr;

    @Column(name = "MERCHANT_ADDRESS_ID")
    private Long merchantAddressId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MERCHANT_ADDRESS_ID", updatable = false, insertable = false)
    private MerchantAddress merchantAddress;

    @Column(name = "ORDER_STATUS")
    private Integer orderStatus;

    @Column(name = "ORDER_TYPE")
    private Integer orderType;

    @Column(name = "PARENT_MERCHANT_ID")
    private Long parentMerchantId;
    /**
     * Đã thanh toán chưa
     */
    @Column(name = "PAYMENT_STATUS")
    private Integer paymentStatus;

    /**
     * Đơn hàng đã giao cho khách hàng
     */
    @Column(name = "DELIVERY_STATUS")
    private Integer deliveryStatus;
    @Column(name = "MERCHANT_IMG_URL")
    private String merchantImgUrl;

    /**
     * Ngày thanh toán
     */
    @JsonDeserialize(using = DeserializeDateHandler.class)
    @Column(name = "PAYMENT_DATE")
    private LocalDateTime paymentDate;

    @JsonDeserialize(using = DeserializeDateHandler.class)
    @Column(name = "ORDER_DATE")
    private LocalDateTime orderDate;

    @JsonDeserialize(using = DeserializeDateHandler.class)
    @Column(name = "DATE_MODIFIED", insertable = false, updatable = false)
    private LocalDateTime dateModified;

    /**
     * Ngày giao hàng
     */
    @JsonDeserialize(using = DeserializeDateHandler.class)
    @Column(name = "DELIVERY_DATE")
    private LocalDateTime deliveryDate;

    private Double tax;

    @Column(name = "LOGISTIC_CODE")
    private String logisticCode;

    @Column(name = "LOGISTIC_QR_CODE")
    private String logisticQRCode;

    @Column(name = "COUPON_CODE")
    private String couponCode;

    @Column(name = "INCENTIVE_COUPON")
    private BigDecimal incentiveCoupon;

    @Column(name = "REASON")
    private String reason;

    @Column(name = "MERCHANT_WAREHOUSE_CODE")
    private String merchantWareHouseCode;

    @Column(name = "MERCHANT_WAREHOUSE_ADDRESS")
    private String merchantWareHouseAddress;

    @Column(name = "order_success_date")
    private LocalDateTime orderSuccessDate;


}
