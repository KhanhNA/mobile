package com.nextsolutions.dcommerce.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "album_detail")
@Data
@NoArgsConstructor()
public class AlbumDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "al_detail_id")
    private Long alDetailId;

    @Column(name = "album_id")
    private Long albumId;

    @Column(name = "image")
    private String image;

    @Column(name = "link_target")
    private String linkTarget;

    @Column(name = "description")
    private String description;
}
