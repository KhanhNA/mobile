package com.nextsolutions.dcommerce.model.charts;

import java.util.List;

public class MatCardChartsLinearGauge {
	private String title;
	private List<MatListItemChartsLinearGauge> items;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<MatListItemChartsLinearGauge> getItems() {
		return this.items;
	}

	public void setItems(List<MatListItemChartsLinearGauge> items) {
		this.items = items;
	}

}
