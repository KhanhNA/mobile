package com.nextsolutions.dcommerce.model;

import com.nextsolutions.dcommerce.shared.dto.resource.BannerResource;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@SqlResultSetMapping(
    name = "BannerResourceMapping",
    classes = {
        @ConstructorResult(targetClass = BannerResource.class,
            columns = {
                @ColumnResult(name = "id", type = Long.class),
                @ColumnResult(name = "code", type = String.class),
                @ColumnResult(name = "name", type = String.class),
                @ColumnResult(name = "fromDate", type = LocalDateTime.class),
                @ColumnResult(name = "toDate", type = LocalDateTime.class),
                @ColumnResult(name = "languageId", type = Integer.class),
            }
        ),
    }
)

@Data
@Entity
@Table(name = "banner")
public class Banner implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "from_date", nullable = false)
    private LocalDateTime fromDate;

    @Column(name = "to_date")
    private LocalDateTime toDate;

    @Column(name = "status")
    private Integer status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "banner", cascade = CascadeType.ALL)
    private List<BannerDescription> descriptions; // 3

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "banner", cascade = CascadeType.ALL)
    private List<BannerDetails> details;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "banner", cascade = CascadeType.ALL)
    private List<BannerMerchantGroup> merchantGroups;
}
