package com.nextsolutions.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.nextsolutions.dcommerce.shared.dto.ManufacturerDescriptionDto;
import com.nextsolutions.dcommerce.shared.dto.resource.ManufacturerResource;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@SqlResultSetMapping(
        name = "ManufacturerDtoMapping",
        classes = {
                @ConstructorResult(targetClass = ManufacturerResource.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "code", type = String.class),
                                @ColumnResult(name = "name", type = String.class),
                                @ColumnResult(name = "createdDate", type = LocalDateTime.class),
                                @ColumnResult(name = "tel", type = String.class),
                                @ColumnResult(name = "email", type = String.class),
                                @ColumnResult(name = "address", type = String.class),
                                @ColumnResult(name = "languageId", type = Long.class),
                                @ColumnResult(name = "status", type = Long.class),
                                @ColumnResult(name = "action", type = Long.class),
                                @ColumnResult(name = "taxCode", type = String.class),
                        }
                ),
        }
)

@Data
@Table(name = "manufacturer")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Manufacturer implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "MANUFACTURER_ID", insertable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DATE_CREATED", unique = true)
    private Date createdDate;

    @Column(name = "DATE_MODIFIED")
    private Date modifiedDate;

    @Column(name = "UPDT_ID")
    private String updtId;

    @Column(name = "CODE", unique = true)
    private String code;

    @Column(name = "MANUFACTURER_IMAGE")
    private String image;

    @Column(name = "SORT_ORDER")
    private Integer sortOrder;

    @Column(name = "MERCHANT_ID", nullable = false)
    private Integer merchantId;

    @JsonView(JsonEntityViewer.Human.CommonList.class)
    @OneToMany(mappedBy = "manufacturer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ManufacturerDescription> descriptions;

    @Column(name = "IS_SYNCHRONIZATION", insertable = false)
    private Boolean isSynchronization;

    @Column(name = "TEL", nullable = false)
    private String tel;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "STATUS")
    private Long status;

    @Column(name = "ACTION")
    private Long action;

    @Column(name = "TAX_CODE")
    private String taxCode;

    @Column(name = "is_active", insertable = false)
    private Integer isActive;

    @Transient
    private String name;

    @Transient
    private List<ManufacturerDescriptionDto> list;

    public Manufacturer(Long id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }
}
