package com.nextsolutions.dcommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-21 09:39
 */
@Entity
@Table(name = "coupon_merchant")
@Data
public class CouponMerchant {

    public void setIsUsed(IsUsedType aNew) {
        this.isUsed = aNew.value;
    }

    public enum IsUsedType {
        NEW(0),
        ACT(1),
        DEACT(2);

        Integer value;
        IsUsedType(Integer value) {
            this.value = value;
        }

    }
    public static Integer IS_USED_NEW = 0; //chua su dung
    public static Integer IS_USED_ACT = 1; //DA su dung
    public static Integer IS_USED_DEACT = 2; //HUY
    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "MERCHANT_ID")
    private Merchant merchant;

    @Column(name = "COUPON_ID")
    private Long couponId;


    @Column(name = "IS_USED")
    private Integer isUsed; //0- chua dung; 1- da dung; 2- huy


    @Column(name = "USED_DATE")
    private LocalDateTime usedDate;

    @Column(name = "status")
    private Integer status;

    @Column(name="CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name="CREATE_USER")
    private String createUser;
    @Column(name="UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name="UPDATE_USER")
    private String updateUser;

}
