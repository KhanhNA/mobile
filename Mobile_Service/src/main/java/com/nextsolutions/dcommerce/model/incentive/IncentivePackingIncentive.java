// Generated with g9.

package com.nextsolutions.dcommerce.model.incentive;

import com.nextsolutions.dcommerce.model.PackingProduct;
import com.nextsolutions.dcommerce.model.PackingProductEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name = "incentive_packing_incentive")
@Entity
@Data
public class IncentivePackingIncentive implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "INCENTIVE_LEVEL_ID")
    private Long incentiveLevelId;

    @Column(name = "INCENTIVE_PROGRAM_ID")
    private Long incentiveProgramId;

    @ManyToOne
    @JoinColumn(name = "PACKING_PRODUCT_ID")
    private PackingProductEntity packingProduct;

    @Column(name = "STATUS")
    private Long status;
    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;
    @Column(name = "CREATE_USER")
    private String createUser;
    @Column(name = "UPDATE_DATE")
    private LocalDateTime updateDate;
    @Column(name = "UPDATE_USER")
    private String updateUser;


    @Column(name = "QUANTITY")
    private Double quantity;
    @Column(name = "MAX_QUANTITY")
    private Double maxQuantity;

    @Transient
    @ManyToOne
    @JoinColumn(name = "INCENTIVE_LEVEL_ID")
    private IncentiveLevel incentiveLevel;

    /**
     * Default constructor.
     */
    public IncentivePackingIncentive() {
        super();
    }


}
