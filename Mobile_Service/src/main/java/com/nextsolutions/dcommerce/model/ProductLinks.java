package com.nextsolutions.dcommerce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductLinks {
    @Id
    @Column(name = "PRODUCT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "MANUFACTURER_ID")
    private Long manufacturerId;

    @ManyToOne
    @JoinColumn(name = "MANUFACTURER_ID", insertable = false, updatable = false)
    private ManufacturerLink manufacturer;

    @Column(name = "DISTRIBUTOR_ID")
    private Long distributorId;

    @ManyToOne
    @JoinColumn(name = "DISTRIBUTOR_ID", insertable = false, updatable = false)
    private DistributorLink distributor;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID", insertable = false, updatable = false)
    private CategoryLink category;

    @Transient
    private List<Long> relatedProductIds;

    @Transient
    private List<RelatedProduct> relatedProducts;

}
