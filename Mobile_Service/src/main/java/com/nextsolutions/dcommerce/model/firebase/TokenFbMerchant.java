package com.nextsolutions.dcommerce.model.firebase;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "token_fb_merchant")
@IdClass(TokenFbId.class)
public class TokenFbMerchant {
    public TokenFbMerchant() {
    }

    public TokenFbMerchant(Long merchantId, String token) {
        this.merchantId = merchantId;
        this.token = token;
    }

    @Id
    private Long merchantId;
    @Id
    private String token;

    @Column(name = "imei")
    private String imei;

    @Column(name = "create_date", insertable = false, updatable = false)
    private Date createDate;
    @Transient
    private Boolean isDelete;
}
