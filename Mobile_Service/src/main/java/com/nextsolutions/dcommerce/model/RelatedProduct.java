package com.nextsolutions.dcommerce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "related_product")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RelatedProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "RELATED_PRODUCT_ID")
    private Long relatedProductId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RELATED_PRODUCT_ID", insertable = false, updatable = false)
    private Product product;

    @Transient
    private String code;

    @Transient
    private String name;

    public RelatedProduct(Long id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }
}
