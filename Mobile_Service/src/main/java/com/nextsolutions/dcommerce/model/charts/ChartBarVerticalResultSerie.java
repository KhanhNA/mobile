package com.nextsolutions.dcommerce.model.charts;

import java.math.BigDecimal;

public class ChartBarVerticalResultSerie {
	private String name;
	private BigDecimal value;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
