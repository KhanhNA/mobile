package com.nextsolutions.dcommerce.model.incentive;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * CT KM: - Description: nội dung ctkm
 *
 * @author ts-client01
 * Create at 2019-06-21 09:38
 */
@Entity
@Table(name = "incentive_program")
@Data
@EqualsAndHashCode(of = "id")
public class Incentive {

    public static int STATUS_DRAFT = 0;
    public static int STATUS_ACT = 1;
    public static int STATUS_DEACT = 2;

    public static int TYPE_PACKING = 1;
    public static int TYPE_ORDER = 2;
    public static int TYPE_GROUPON = 3;
    public static int TYPE_ORDER_VALUE = 4;
    /**
     *
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     *
     */
    @Column(name = "CODE")
    private String code;

    /**
     *
     */
    @Column(name = "NAME")
    private String name;

    /**
     *
     */
    @Column(name = "STATUS")
    private Integer status;

    /**
     * Loại khuyến mại: 1: KM theo sp, 2: KM theo don hang, 3: groupon, 4: theo gia tri don hang
     */
    @Column(name = "TYPE")
    private Integer type;

    /**
     *
     */
    @Column(name = "FORMAT")
    private String format;

    /**
     *
     */
    @Column(name = "FROM_DATE")
    private LocalDateTime fromDate;

    /**
     *
     */
    @Column(name = "TO_DATE")
    private LocalDateTime toDate;

    /**
     *
     */
    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IMG_URL")
    private String imgUrl;
    @Column(name = "BIG_SALE")
    private Boolean bigSale;


    /**
     *
     */
    @Column(name = "CREATE_DATE")
    private LocalDateTime createDate;

    /**
     *
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     *
     */
    @Column(name = "UPDATE_DATE")
    private LocalDateTime updateDate;

    /**
     *
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;


    /**
     * Loại incentive: 1- trả trên đơn hàng, 2 - trả vào wallet
     */
    @Column(name = "INCENTIVE_TYPE")
    private Integer incentiveType;

    //    @OneToMany(mappedBy="incentiveProgram")
    @Transient
    List<IncentivePackingSale> incentivePackingSales;

    //
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "INCENTIVE_PROGRAM_ID", referencedColumnName = "id", insertable = false, updatable = false)
    List<IncentiveProgramDescription> descriptions;


    //    @OneToMany(fetch = FetchType.LAZY,mappedBy="incentiveProgram")
    @Transient
    List<IncentiveLevel> incentiveLevels;

    //    @OneToMany(fetch = FetchType.LAZY,mappedBy="incentiveProgram")
    @Transient
    List<IncentiveMerchantType> merchantTypes;


}
