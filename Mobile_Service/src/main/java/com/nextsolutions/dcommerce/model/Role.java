package com.nextsolutions.dcommerce.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    private Long id;
    private String clientId;
    private String roleName;
    public Role(Long id){
        this.id = id;
    }
}
