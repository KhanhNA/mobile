package com.nextsolutions.dcommerce.model.loyalty;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * 1 chương trình khuyến mại được áp dụng cho những loại merchant nào
 * @author ts-client01
 * Create at 2019-06-21 09:38
 */
@Entity
@Table(name = "loyalty_merchant_type")
@Data
@EqualsAndHashCode(of = "id")

public class LoyaltyMerchantType {


    /**
     * 
     */
    @Id

    @Column(name = "ID")
    private Long id;

    /**
     * 
     */
    @Column(name = "LOYALTY_ID", insertable = false, updatable =  false)
    private Long loyaltyId;

    /**
     * 
     */
    @Column(name = "MERCHANT_TYPE_ID")
    private Long merchantTypeId;

    /**
     * 
     */
    @Column(name = "STATUS")
    private Long status;

//    @ManyToOne
//    @JoinColumn(name="LOYALTY_ID")
    @Transient
    private Loyalty loyalty;
}
