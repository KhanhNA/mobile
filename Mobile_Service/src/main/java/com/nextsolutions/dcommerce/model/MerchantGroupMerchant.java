package com.nextsolutions.dcommerce.model;

import com.nextsolutions.dcommerce.shared.dto.resource.MerchantGroupMerchantResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@SqlResultSetMapping(
    name = "MerchantGroupMerchantResource",
    classes = {
        @ConstructorResult(targetClass = MerchantGroupMerchantResource.class,
            columns = {
                @ColumnResult(name = "id", type = Long.class),
                @ColumnResult(name = "merchantId", type = Long.class),
                @ColumnResult(name = "merchantGroupId", type = Long.class),
            }
        ),
    }
)


@Entity
@Table(name = "merchant_group_merchant")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MerchantGroupMerchant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "merchant_group_id")
    private Long merchantGroupId;

    @Column(name = "MERCHANT_ID")
    private Long merchantId;

    @ManyToOne
    @JoinColumn(name = "MERCHANT_ID", insertable = false, updatable = false)
    private Merchant merchant;

}
