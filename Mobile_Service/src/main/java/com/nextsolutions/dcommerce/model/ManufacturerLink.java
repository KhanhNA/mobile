package com.nextsolutions.dcommerce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Table(name = "manufacturer")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ManufacturerLink {
    @Id
    @Column(name = "MANUFACTURER_ID", insertable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Transient
    private String name;
}
