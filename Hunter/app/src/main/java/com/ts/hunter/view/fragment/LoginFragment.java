package com.ts.hunter.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.button.MaterialButton;
import com.ts.hunter.R;
import com.ts.hunter.utils.ApiResponse;
import com.ts.hunter.utils.NetworkUtils;
import com.ts.hunter.utils.ToastUtils;
import com.ts.hunter.view.activity.MainActivity;
import com.ts.hunter.viewmodel.LoginVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;

public class LoginFragment extends BaseFragment implements ActionsListener {
    private LoginVM loginVM;
    MaterialButton btnLogin;
    ProgressBar progressBar;

    public static LoginFragment newInstance() {
        LoginFragment myFragment = new LoginFragment();
        return myFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        loginVM = (LoginVM) viewModel;
        loginVM.loginResponse().observe(this, this::consumeResponse);
        btnLogin = view.findViewById(R.id.btnLogin);
        loginVM.getModelE().setUserName("madmin");
        loginVM.getModelE().setPassWord("abc@123");
        progressBar = view.findViewById(R.id.progressBar);
//        btnLogin.setOnClickListener(v -> {
//            Intent intent = new Intent(getActivity(), MainActivity.class);
//            startActivity(intent);
//        });

        return view;
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        if (R.id.btnLogin == v.getId()) {
            if (isValid()) {
                if (!NetworkUtils.isNetworkConnected(getActivity())) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                } else {
                    btnLogin.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    loginVM.requestLogin();
                }
            }
        }
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {
            case LOADING:
                progressBar.setVisibility(View.VISIBLE);
                btnLogin.setVisibility(View.GONE);
                break;

            case SUCCESS:
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                break;

            case ERROR:
                progressBar.setVisibility(View.GONE);
                btnLogin.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), R.string.invalidUserOrPass, Toast.LENGTH_SHORT).show();
                break;
            case NOT_CONNECT:
                Toast.makeText(getActivity(), R.string.not_connect_server, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                btnLogin.setVisibility(View.VISIBLE);
                break;

            default:
                break;
        }
    }

    private boolean isValid() {
        if (loginVM.getModelE().getUserName().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_username));
            return false;
        } else if (loginVM.getModelE().getPassWord().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_password));
            return false;
        }

        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
