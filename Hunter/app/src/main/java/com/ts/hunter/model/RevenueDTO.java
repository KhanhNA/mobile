package com.ts.hunter.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RevenueDTO extends BaseModel {
    private Integer month;
    private Integer year;
    private Double amount;
}