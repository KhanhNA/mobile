package com.ts.hunter.view.activity;

import android.os.Bundle;

import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ts.hunter.R;
import com.ts.hunter.view.adapter.TabAdapter;
import com.ts.hunter.view.fragment.LoginFragment;
import com.ts.hunter.view.fragment.RegisterFragment;
import com.ts.hunter.viewmodel.LoginVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class LoginActivity extends BaseActivity {
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.viewPager);

        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), getResources().getString(R.string.login));
        adapter.addFragment(new RegisterFragment(), getResources().getString(R.string.introduction));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_login;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
