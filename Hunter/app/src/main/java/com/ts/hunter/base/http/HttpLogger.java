package com.ts.hunter.base.http;


import android.util.Log;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * @author：PhamBien
 */
public class HttpLogger implements HttpLoggingInterceptor.Logger {
    private static String TAG = HttpLogger.class.getSimpleName();

    private StringBuilder mMessage = new StringBuilder();

    @Override
    public void log(String message) {
        if (message.startsWith("--> POST")) {
            mMessage.setLength(0);
        }
        if ((message.startsWith("{") && message.endsWith("}"))
                || (message.startsWith("[") && message.endsWith("]"))) {
            Log.i(TAG, message);
        }
        mMessage.append(message.concat("\n"));
        if (message.startsWith("<-- END HTTP")) {
            Log.i(TAG, mMessage.toString());
        }
    }
}