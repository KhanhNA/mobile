package com.ts.hunter.view.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.material.button.MaterialButton;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.hunter.R;
import com.ts.hunter.databinding.FragmentSelectWarehousesBinding;
import com.ts.hunter.view.adapter.BaseAdapterV2;
import com.ts.hunter.viewmodel.CreateL1VM;
import com.tsolution.base.listener.AdapterListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

@SuppressLint("ValidFragment")
public class SelectWareHouseFragment extends DialogFragment implements AdapterListener {
    private XRecyclerView rcStore;
    private BaseAdapterV2 wareHouseAdapter;
    private SeekBar seekBar;
    private CreateL1VM createL1VM;

    private TextView txtEmptyWarehouse;
    private TextView txtSeekBar;
    private ImageView btnDismiss;

    private RequestQueue queue;

    public SelectWareHouseFragment(CreateL1VM createL1VM) {
        this.createL1VM = createL1VM;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentSelectWarehousesBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_warehouses, container, false);
        binding.setViewModel(createL1VM);
        innitView(binding.getRoot());

        return binding.getRoot();
    }

    private void innitView(View v) {
        queue = Volley.newRequestQueue(getContext());
        btnDismiss = v.findViewById(R.id.btnDismiss);
        MaterialButton btnNextStep = v.findViewById(R.id.btnNextStep);
        txtEmptyWarehouse = v.findViewById(R.id.txtEmptyWarehouse);
        txtSeekBar = v.findViewById(R.id.txtSeekBar);
        rcStore = v.findViewById(R.id.rcStore);
        wareHouseAdapter = new BaseAdapterV2(R.layout.item_store, createL1VM.getListWareHouse(), this);
        wareHouseAdapter.setConfigXRecycler(rcStore, 1);

        rcStore.getDefaultRefreshHeaderView().setState(2);
        createL1VM.getListWareHouse().getBaseModelsE().clear();
        createL1VM.getWareHouses(1, queue, 300);


        rcStore.setAdapter(wareHouseAdapter);
        rcStore.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                wareHouseAdapter.notifyDataSetChanged();
                createL1VM.setCurrentPage(1);
                createL1VM.getListWareHouse().getBaseModelsE().clear();
                createL1VM.getWareHouses(1, queue, seekBar.getProgress());

            }

            @Override
            public void onLoadMore() {
                createL1VM.getMoreWareHouse(queue, seekBar.getProgress());
            }
        });

        seekBar = v.findViewById(R.id.seek_bar);
        seekBar.setMax(3000);
        seekBar.setProgress(300);
        txtSeekBar.setText(300 + getContext().getResources().getString(R.string.kilometer));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtSeekBar.setText(seekBar.getProgress() + getContext().getResources().getString(R.string.kilometer));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                wareHouseAdapter.notifyDataSetChanged();
                rcStore.getDefaultRefreshHeaderView().setState(2);
                txtSeekBar.setText(seekBar.getProgress() + getContext().getResources().getString(R.string.kilometer));
                createL1VM.getListWareHouse().getBaseModelsE().clear();
                createL1VM.getWareHouses(1, queue, seekBar.getProgress());

            }
        });

        btnNextStep.setOnClickListener(view -> {
            this.dismiss();
            createL1VM.selectedStore();
        });
        txtEmptyWarehouse.setOnClickListener(v1 -> {
            this.dismiss();
            EventBus.getDefault().post(1);
        });

        btnDismiss.setOnClickListener(v1-> this.dismiss());

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String code) {
        if (code != null) {
            switch (code) {
                //getSuccess
                case "getSuccess":
                    rcStore.refreshComplete();
                    rcStore.loadMoreComplete();
                    wareHouseAdapter.notifyDataSetChanged();
                    txtEmptyWarehouse.setVisibility(View.GONE);
                    break;
                case "getFail":
                    rcStore.refreshComplete();
                    rcStore.loadMoreComplete();
                    break;
                //noMore to load
                case "noMore":
                    txtEmptyWarehouse.setVisibility(View.GONE);
                    rcStore.setNoMore(true);
                    break;

            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onItemClick(View view, Object o) {

    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }
}
