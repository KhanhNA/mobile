package com.ts.hunter.view.fragment;

import com.ts.hunter.R;
import com.ts.hunter.viewmodel.LoginVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class RegisterFragment extends BaseFragment {
    public static RegisterFragment newInstance() {
        RegisterFragment myFragment = new RegisterFragment();
        return myFragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_register;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
