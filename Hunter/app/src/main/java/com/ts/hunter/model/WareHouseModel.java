package com.ts.hunter.model;

import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WareHouseModel extends BaseModel {
    private String code;
    private String name;
    private String domainName;
    private Date inBusinessSince;
    private Date outBusinessSince;
    private String email;
    private String logo;
    private Integer floorNo;
    private String address;
    private String city;
    private String phone;
    private String postalCode;
    private Boolean dc;
    private Boolean status;
    private Double lat;
    private Double lng;
}
