package com.ts.hunter.model;

import com.google.gson.annotations.SerializedName;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantWareHouseModel extends BaseModel {
    private Long id;
    private Long merchantId;
    private String storeCode;
    private String storeName;
    private String storeAddress;
    private String city;
    private String phone;
    private String postalCode;
    private Double lat;

    @SerializedName("longitude")
    private Double lng;

}
