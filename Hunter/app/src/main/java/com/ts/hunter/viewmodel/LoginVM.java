package com.ts.hunter.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.base.http.HttpHelper;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.model.UserInfo;
import com.ts.hunter.utils.ApiResponse;
import com.ts.hunter.utils.Constants;
import com.ts.hunter.utils.ToastUtils;
import com.ts.hunter.utils.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.RetrofitClient;
import com.tsolution.base.ServiceResponse;
import com.tsolution.base.exceptionHandle.AppException;

import java.io.IOException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Response;

public class LoginVM extends BaseViewModel<UserInfo> {
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    private UserInfo userInfo;

    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }

    public LoginVM(@NonNull Application application) {
        super(application);
        userInfo = new UserInfo();
        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
        userInfo.setIsSave(sharedPreferences.getBoolean("chkSave", false));
        userInfo.setUserName(sharedPreferences.getString(Constants.USER_NAME, ""));
        userInfo.setPassWord(sharedPreferences.getString(Constants.MK, ""));
        model.set(userInfo);
    }

    public void requestLogin() {
        if (model.get() != null) {
            UserInfo userInfo = model.get();
            HttpHelper.requestLogin(appException, userInfo.getUserName(), userInfo.getPassWord(), this::loginSuccess);
        }

    }

    private void loginSuccess(Call call, Response response, Object o, Throwable throwable) {
        if (o != null && !(o instanceof IOException)) {
            Gson gson = new Gson();
            String json = gson.toJson(getModelE());

            SharedPreferences.Editor editor = AppController.getInstance().getEditor();
            editor.putString("UserInfo", json);


            editor.putString(Constants.USER_NAME, getModelE().getUserName());
            if (userInfo.getIsSave() != null && userInfo.getIsSave()) {
                editor.putString(Constants.MK, getModelE().getPassWord());
                editor.putBoolean("chkSave", true);
            } else {
                editor.putBoolean("chkSave", false);
                editor.putString(Constants.MK, "");
            }
            editor.commit();
            getUserInfo();

        } else if (o instanceof IOException) {
            responseLiveData.postValue(ApiResponse.notConnect(new Throwable()));
            RetrofitClient.TOKEN = "";
            HttpHelper.TOKEN_DCOM = "";
        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
//            RetrofitClientV2.TOKEN = "";
            RetrofitClient.TOKEN = "";
            HttpHelper.TOKEN_DCOM = "";
        }

    }


    private void getUserInfo() {
        callApi(HttpHelper.getInstance().getApi().getMerchantByUserName(model.get().getUserName())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::getUserInfoSuccess, this::handleError));
    }

    private void handleError(Throwable throwable) {
        responseLiveData.postValue(ApiResponse.error(new Throwable()));
        throwable.printStackTrace();
    }


    private void getUserInfoSuccess(MerchantModel result) {
//        if (result != null) {
//            responseLiveData.postValue(ApiResponse.success(result));
//            AppController.getInstance().putCatche(AppController.MERCHANT, result);
//        } else {
//            responseLiveData.postValue(ApiResponse.error(new Throwable()));
//        }
        if (result != null) {
            if (result.getStatus() != 1 || result.getMerchantTypeId() != 0) {
                ToastUtils.showToast(R.string.not_permission);
                responseLiveData.postValue(ApiResponse.error(new Throwable()));
            } else if (result.getMerchantTypeId() != null) {
                responseLiveData.postValue(ApiResponse.success(result));
                AppController.getInstance().putCatche(AppController.MERCHANT, result);
            }
        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
        }
    }
}

