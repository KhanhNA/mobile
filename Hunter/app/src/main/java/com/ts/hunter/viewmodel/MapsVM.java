package com.ts.hunter.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.ts.hunter.base.AppController;
import com.ts.hunter.base.http.HttpHelper;
import com.ts.hunter.base.http.rx.RxSchedulers;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.service.network.rx.RxSubscriber;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.ServiceResponse;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.List;

public class MapsVM extends BaseViewModel {

    private int totalPage = 0;
    private int currentPage = 0;


    public MapsVM(@NonNull Application application) {
        super(application);
    }


    public void getMerchants(int page) {
        //set MerchantDto
        MerchantModel merchantModel = new MerchantModel();
        merchantModel.setParentMarchantId(AppController.getMerchantId());
        merchantModel.setStatus(1);
        callApi(HttpHelper.getInstance().getApi().getDataOnMap(AppController.getMerchantId(), 0, 1, page, Integer.MAX_VALUE)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<MerchantModel>>() {
                    @Override
                    public void onSuccess(ServiceResponse<MerchantModel> response) {
                        getMerchantsSuccess(response);
                    }
                }));
    }

    private void getMerchantsSuccess(ServiceResponse<MerchantModel> response) {
        if (response != null && response.getArrData() != null) {
            List<MerchantModel> merchantModels = response.getArrData();
            totalPage = response.getTotalPages();
            addNewPage(merchantModels, totalPage);
            try {
                view.action("getMerchantSuccess", null, this, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }
}
