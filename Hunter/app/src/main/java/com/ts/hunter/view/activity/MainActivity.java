package com.ts.hunter.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.utils.Constants;
import com.ts.hunter.view.fragment.CreateL1Fragment;
import com.ts.hunter.view.fragment.HomeFragment;
import com.ts.hunter.view.fragment.MapsFragment;
import com.ts.hunter.view.fragment.UpdateStatusFragment;
import com.ts.hunter.viewmodel.MainVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;

public class MainActivity extends BaseActivity {
    boolean doubleBackToExitPressedOnce = false;
    MaterialToolbar toolbar;
    DrawerLayout drawerLayout;
    FragmentManager fragmentManager;
    NavigationView navigationView;
    int lastSelected;
    HomeFragment home;
    ImageView imgMerchant;
    TextView txtFullName;
    MainVM mainVM;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainVM = (MainVM) viewModel;
        mainVM.setModelE(AppController.getCurrentMerchant());


        initView();

    }


    private void initView() {

        fragmentManager = getSupportFragmentManager();
        toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.navigation);
        View headerLayout =
                navigationView.inflateHeaderView(R.layout.navigationview_header);
        txtFullName = headerLayout.findViewById(R.id.txtFullName);
        imgMerchant = headerLayout.findViewById(R.id.imgMerchant);
        txtFullName.setText(mainVM.getModelE().getFullName());
        Glide.with(MainActivity.this)
                .load(AppController.BASE_IMAGE + mainVM.getModelE().getMerchantImgUrl())
                .error(R.drawable.ic_stub)
                .placeholder(R.drawable.ic_stub)
                .transition(new DrawableTransitionOptions().crossFade())
                .into(imgMerchant);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.mango);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setTitle(getResources().getString(R.string.general));
        drawerLayout = findViewById(R.id.drawer_layout);

        lastSelected = R.id.navHome;
        home = new HomeFragment();
        openFragment(home);


        NavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(true);
                    // close drawer when item is tapped
                    drawerLayout.closeDrawers();
                    if(lastSelected != menuItem.getItemId())
                    {
                        lastSelected = menuItem.getItemId();
                        switch (menuItem.getItemId()) {
                            case R.id.navHome:
                                toolbar.setTitle(getResources().getString(R.string.general));
                                home = new HomeFragment();
                                openFragment(home);
                                break;
                            case R.id.navCreateL1:
                                toolbar.setTitle(getResources().getString(R.string.create_l1));
                                CreateL1Fragment createL1Fragment = new CreateL1Fragment();
                                openFragment(createL1Fragment);
                                break;
                            case R.id.navUpdateStatus:
                                toolbar.setTitle(getResources().getString(R.string.update_status));
                                UpdateStatusFragment updateStatusFragment = new UpdateStatusFragment();
                                openFragment(updateStatusFragment);
                                break;
                            case R.id.navMaps:
                                toolbar.setTitle(getResources().getString(R.string.maps));
                                MapsFragment maps = new MapsFragment();
                                openFragment(maps);
                                break;
                            case R.id.navLogout:
                                showDialogLogout();
                                break;
                        }
                    }

                    return true;
                });
    }

    private void showDialogLogout() {
        new AlertDialog.Builder(getBaseActivity()).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.message_logout)
                .setPositiveButton(R.string.OK, (dialog, which) -> {
                    // Delete token firebase
                    AppController.getInstance().getSharePre().edit().putString(Constants.USER_NAME, "").apply();
                    AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
                    // Delete cache merchant
                    AppController.getInstance().clearCache();
                    // Intent loginActivity
                    Intent intent = new Intent(getBaseActivity(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                })
                .setNegativeButton(R.string.CANCEL, null)
                .show();
    }

    private void openFragment(Fragment fragment){
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content_main, fragment);
        transaction.commit();
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            EventBus.getDefault().unregister(this);
            super.onBackPressed();
            return;
        }
        if (lastSelected != R.id.navHome) {
            lastSelected = R.id.navHome;
            toolbar.setTitle(getResources().getString(R.string.general));
            home = new HomeFragment();
            openFragment(home);
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.message_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MainVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
