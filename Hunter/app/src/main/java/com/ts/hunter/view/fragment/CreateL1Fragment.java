package com.ts.hunter.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.tabs.TabLayout;
import com.ts.hunter.BR;
import com.ts.hunter.R;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.view.adapter.TabAdapter;
import com.ts.hunter.viewmodel.CreateL1VM;
import com.ts.hunter.widget.NonSwipeViewPager;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class CreateL1Fragment extends BaseFragment {
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private NonSwipeViewPager viewPager;

    Toolbar toolbar;
    private CreateL1VM createL1VM;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        viewModel = ViewModelProviders.of(getBaseActivity()).get(CreateL1VM.class);
        binding.setVariable(BR.viewModel, viewModel);
        createL1VM = (CreateL1VM) viewModel;
        createL1VM.resetData();

        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.merchant_info);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        Intent intent = getActivity().getIntent();
        if (intent != null) {
            if (intent.hasExtra("merchantUserName")) {
                createL1VM.getIsCreate().set(false);
                String merchantUserName = intent.getStringExtra("merchantUserName");
                new Handler().postDelayed(() -> createL1VM.getUserInfo(merchantUserName), 200);

            }
        }

        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.viewPager);

        adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragment(new BasicInfoFragment(), getContext().getResources().getString(R.string.basic_infor));
        adapter.addFragment(new MatrixPropertiesFragment(), getContext().getResources().getString(R.string.matrix_properties));
        adapter.addFragment(new ContractFragment(), getContext().getResources().getString(R.string.contract));
        adapter.addFragment(new WareHouseFragment(), getContext().getResources().getString(R.string.warehouse));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener((v, event) -> !createL1VM.getSwipeAble().get());
        }

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Integer pageSelected) {
        if (pageSelected != null) {
            switch (pageSelected) {
                case 0:
                    viewPager.setCurrentItem(0);
                    break;
                case 1:
                    viewPager.setCurrentItem(1);
                    break;
                case 2:
                    viewPager.setCurrentItem(2);
                    break;
                case 3:
                    viewPager.setCurrentItem(3);
                    break;
            }
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_l1;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateL1VM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
