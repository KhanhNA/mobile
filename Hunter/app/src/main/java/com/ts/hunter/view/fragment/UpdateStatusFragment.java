package com.ts.hunter.view.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;

import com.google.android.material.snackbar.Snackbar;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.utils.ToastUtils;
import com.ts.hunter.view.adapter.BaseAdapterV2;
import com.ts.hunter.view.adapter.UpdateStatusAdapter;
import com.ts.hunter.viewmodel.UpdateStatusVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.Calendar;
import java.util.Date;

public class UpdateStatusFragment extends BaseFragment {
    private TextView txtFromDate, txtToDate;
    private XRecyclerView rcMerchant;
    private BaseAdapterV2 merchantAdapter;
    private SearchView searchView;
    private UpdateStatusVM updateStatusVM;
    private Spinner spStatus;
    private String preKey = "";
    private Handler handler;
    private ImageView btnBack;
    private TextView txtTitle;
    private Date fromDate, toDate;
    private Integer nonRevenue = 0;
    private DeActiveDialogFragment dialogDeActive;
    private Snackbar snackbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        updateStatusVM = (UpdateStatusVM) viewModel;

        innitView(view);

        return view;
    }

    private void innitView(View v) {
        MerchantModel merchantModel = new MerchantModel();
        merchantModel.setParentMarchantId(AppController.getMerchantId());
        merchantModel.setFullName("");
        handler = new Handler();

        txtFromDate = v.findViewById(R.id.txtFromDate);
        txtToDate = v.findViewById(R.id.txtToDate);
        spStatus = v.findViewById(R.id.spStatus);
        rcMerchant = v.findViewById(R.id.rcMerchant);
        txtTitle = v.findViewById(R.id.txtTitle);
        btnBack = v.findViewById(R.id.btnBack);

        snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.updating, Snackbar.LENGTH_INDEFINITE);

        //set listener
        txtFromDate.setOnClickListener(view -> selectDate(view, merchantModel, fromDate, toDate));
        txtToDate.setOnClickListener(view -> selectDate(view, merchantModel, fromDate, toDate));

        btnBack.setOnClickListener(view-> getActivity().onBackPressed());

        merchantAdapter = new UpdateStatusAdapter(R.layout.item_merchant, updateStatusVM, this);
        merchantAdapter.setConfigXRecycler(rcMerchant, 1);
        rcMerchant.setAdapter(merchantAdapter);
        rcMerchant.getDefaultRefreshHeaderView().setState(2);
        rcMerchant.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                merchantAdapter.notifyDataSetChanged();
                updateStatusVM.onRefresh(merchantModel, fromDate, toDate, nonRevenue);
            }

            @Override
            public void onLoadMore() {
                updateStatusVM.getMoreMerchant(merchantModel, fromDate, toDate, nonRevenue);
            }
        });

        searchView = v.findViewById(R.id.searchBarSearchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.equalsIgnoreCase(preKey)){
                    merchantAdapter.notifyDataSetChanged();
                    rcMerchant.getDefaultRefreshHeaderView().setState(2);
                    preKey = query;
                    merchantModel.setFullName(preKey);
                    updateStatusVM.onRefresh(merchantModel, fromDate, toDate, nonRevenue);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //search here
                if (!preKey.equalsIgnoreCase(newText)) {
                    handler.removeCallbacksAndMessages(null);

                    handler.postDelayed(() -> {
                        merchantAdapter.notifyDataSetChanged();
                        rcMerchant.getDefaultRefreshHeaderView().setState(2);
                        preKey = newText;
                        merchantModel.setFullName(preKey);
                        updateStatusVM.onRefresh(merchantModel, fromDate, toDate, nonRevenue);
                    }, 350);

                }
                return false;
            }
        });

        //init position
        final int[] lastPosition = {0};
        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (position != lastPosition[0]) {
                    lastPosition[0] = position;
                    switch (position) {
                        case 1:
                            merchantModel.setStatus(1);
                            txtTitle.setText(R.string.number_of_active);
                            break;
                        case 2:
                            merchantModel.setStatus(2);
                            txtTitle.setText(R.string.number_of_retired);
                            break;
                        case 3:
                            merchantModel.setStatus(0);
                            txtTitle.setText(R.string.number_of_registered);
                            break;
                        default:
                            merchantModel.setStatus(null);
                            break;
                    }
                    merchantAdapter.notifyDataSetChanged();
                    rcMerchant.getDefaultRefreshHeaderView().setState(2);
                    updateStatusVM.onRefresh(merchantModel, fromDate, toDate, nonRevenue);
                }
        }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        searchView.onActionViewExpanded();
        searchView.clearFocus();

        getData(merchantModel);

    }

    private void selectDate(View v, MerchantModel merchantModel, Date fromDate, Date toDate) {
        if (getContext() != null) {
            Calendar c = Calendar.getInstance();
            switch (v.getId()) {
                case R.id.txtFromDate:
                    c.setTime(fromDate);
                    break;
                case R.id.txtToDate:
                    c.setTime(toDate);
                    break;
            }
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
                // TODO: 12/8/2019 set date for ViewModel
                Calendar calendar = Calendar.getInstance();
                calendar.set(year1, month1, dayOfMonth);
                String date = AppController.formatDate.format(calendar.getTime());
                switch (v.getId()) {
                    case R.id.txtFromDate:
                        merchantAdapter.notifyDataSetChanged();
                        rcMerchant.getDefaultRefreshHeaderView().setState(2);
                        ((TextView) v).setText(date);
                        fromDate.setTime(calendar.getTime().getTime());
                        updateStatusVM.onRefresh(merchantModel, fromDate, toDate, nonRevenue);
                        break;
                    case R.id.txtToDate:
                        merchantAdapter.notifyDataSetChanged();
                        rcMerchant.getDefaultRefreshHeaderView().setState(2);
                        ((TextView) v).setText(date);
                        toDate.setTime(calendar.getTime().getTime());
                        updateStatusVM.onRefresh(merchantModel, fromDate, toDate, nonRevenue);

                        break;
                }

            }, year, month, day);
            if (v.getId() == R.id.txtToDate) {
                datePickerDialog.getDatePicker().setMinDate(fromDate.getTime());
            }
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

    }

    private void getData(MerchantModel merchantModel) {
        //get intent neu co
        Intent intent = getBaseActivity().getIntent();
        if (intent != null) {
            if (intent.hasExtra("merchantStatus")) {
                Integer merchantStatus = (Integer) intent.getExtras().getSerializable("merchantStatus");
                fromDate = (Date) intent.getExtras().getSerializable("fromDate");
                toDate = (Date) intent.getExtras().getSerializable("toDate");
                nonRevenue = (Integer) intent.getExtras().getSerializable("nonRevenue");
                if(nonRevenue != null && nonRevenue == 1){
                    txtTitle.setText(R.string.number_of_inactive);
                }
                txtFromDate.setText(AppController.formatDate.format(fromDate.getTime()));
                txtToDate.setText(AppController.formatDate.format(toDate.getTime()));
                updateStatusVM.setIsDashBoard(true);
                merchantModel.setStatus(merchantStatus);
                if(merchantStatus != null){
                    //when set selection spinner win be load data
                    switch (merchantStatus){
                        case 0:
                            spStatus.setSelection(3);
                            break;
                        case 1:
                            spStatus.setSelection(1);
                            break;
                        case 2:
                            spStatus.setSelection(2);
                            break;
                        default:
                            spStatus.setSelection(0);
                    }
                }else {
                    updateStatusVM.getMerchants(merchantModel, fromDate, toDate, nonRevenue, 0);
                }
            }else {
                updateStatusVM.getMerchants(merchantModel, fromDate, toDate, nonRevenue, 0);
            }
        }else {
            updateStatusVM.getMerchants(merchantModel, fromDate, toDate, nonRevenue, 0);
        }

    }

    @Override
    public void onItemClick(View v, Object o) {
        switch (v.getId()) {
            case R.id.itemContact:
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra("FRAGMENT", MerchantDetailFragment.class);
                intent.putExtra("index", ((MerchantModel) o).index);
                intent.putExtra("merchantUserName", ((MerchantModel) o).getUserName());
                startActivity(intent);

                break;
            case R.id.activeMerchant:
            case R.id.deactivateMerchant:
                updateStatusVM.indexChange = ((MerchantModel) o).index - 1;
                if(((MerchantModel) o).getStatus() == 1){
                    showDeActivateDialog((MerchantModel) o, snackbar);
                }else{
                    showActiveDialog((MerchantModel)o);
                }
                break;
        }

    }

    private void showActiveDialog(MerchantModel o) {
        new AlertDialog.Builder(getActivity()).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.message_confirm_activate)
                .setPositiveButton(R.string.OK, (dialog, which) -> {
                    snackbar.show();
                    o.setLockStartDate(AppController.formatDate_v2.format(Calendar.getInstance().getTime().getTime()));
                    updateStatusVM.updateStatus(o);
                })
                .setNegativeButton(R.string.CANCEL, null)
                .show();
    }

    private void showDeActivateDialog(MerchantModel model, Snackbar snackbar) {
        dialogDeActive = new DeActiveDialogFragment(updateStatusVM, model, snackbar);
        dialogDeActive.show(getBaseActivity().getFragmentManager(), dialogDeActive.getTag());
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "getMerchantSuccess":
                rcMerchant.refreshComplete();
                rcMerchant.loadMoreComplete();
                merchantAdapter.notifyDataSetChanged();
                break;
            case "noMore":
                rcMerchant.setNoMore(true);
                break;
            case "updateStatusSuccess":
                rcMerchant.notifyItemChanged(updateStatusVM.indexChange);
                snackbar.dismiss();
                break;
            case "updateStatusFail":
                snackbar.dismiss();
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_update_status;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return UpdateStatusVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
