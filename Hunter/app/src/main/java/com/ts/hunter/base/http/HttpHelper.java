package com.ts.hunter.base.http;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ts.hunter.base.AppController;
import com.ts.hunter.service.SOService;
import com.tsolution.base.listener.ResponseResult;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ts.hunter.utils.TsUtils.checkNotNull;


/**
 * @author：PhamBien
 */
public class HttpHelper {

    private static volatile HttpHelper mHttpHelper = null;

    private static OkHttpClient mOkHttpClient;

    private static Retrofit mRetrofit;

    private static OkHttpClient.Builder mBuilder;

    private static String BASE_URL;
    public static Long langIdSelected = 1L;
    public static String TOKEN_DCOM = "";

    private HttpHelper() {
    }

    public static HttpHelper getInstance() {
        if (mHttpHelper == null) {
            synchronized (HttpHelper.class) {
                if (mHttpHelper == null) {
                    mHttpHelper = new HttpHelper();
                }
            }
        }
        return mHttpHelper;
    }

    public static void init(Context context, String baseUrl) {
        new HttpHelper.Builder(context)
                .initOkHttp()
                .createRetrofit(baseUrl)
                .build();
    }


    public static class Builder {
        private OkHttpClient mOkHttpClient;

        private OkHttpClient.Builder mBuilder;

        private Retrofit mRetrofit;

        private Context mContext;

        public Builder(Context context) {
            this.mContext = context;
            BASE_URL = AppController.BASE_URL;

        }

        /**
         * create  OKHttpClient
         *
         * @return Builder
         */
        public Builder initOkHttp() {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLogger());
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            if (mBuilder == null) {
                synchronized (HttpHelper.class) {
                    if (mBuilder == null) {
                        Cache cache = new Cache(new File(mContext.getCacheDir(), "HttpCache"), 1024 * 1024 * 10);
                        mBuilder = new OkHttpClient.Builder()
                                .cache(cache)
                                .addInterceptor(interceptor)
                                .connectTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .addInterceptor(chain -> {
                                    Request request = chain.request();
                                    Request.Builder newRequest = request.newBuilder().addHeader("Authorization", TOKEN_DCOM)
                                            .addHeader("Content-Type", "application/json")
                                            .addHeader("Accept-Language", "" + langIdSelected);
                                    return chain.proceed(newRequest.build());
                                });
                        ;
                    }
                }
            }

            return this;
        }

        /**
         * @param mInterceptor Interceptor
         * @return Builder
         */
        public Builder addInterceptor(Interceptor mInterceptor) {
            checkNotNull(mInterceptor);
            this.mBuilder.addNetworkInterceptor(mInterceptor);
            return this;
        }

        /**
         * create retrofit
         *
         * @param baseUrl baseUrl
         * @return Builder
         */
        public Builder createRetrofit(String baseUrl) {
            checkNotNull(baseUrl);
            GsonBuilder gsonBuilder = new GsonBuilder();
            //gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
            gsonBuilder.setDateFormat(AppController.DATE_PATTERN_GSON);
            Retrofit.Builder builder = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .baseUrl(baseUrl);
            BASE_URL = baseUrl;
            this.mOkHttpClient = mBuilder.build();
            this.mRetrofit = builder.client(mOkHttpClient).build();
            return this;
        }

        public void build() {
            HttpHelper.getInstance().build(this);
        }

    }

    private void build(Builder builder) {
        checkNotNull(builder);
        checkNotNull(builder.mBuilder);
        checkNotNull(builder.mOkHttpClient);
        checkNotNull(builder.mRetrofit);
        mBuilder = builder.mBuilder;
        mOkHttpClient = builder.mOkHttpClient;
        mRetrofit = builder.mRetrofit;
    }

    public <T> T create(Class<T> clz) {
        checkNotNull(clz);
        checkNotNull(mRetrofit);
        return mRetrofit.create(clz);
    }

    public SOService getApi() {
        checkNotNull(SOService.class);
        checkNotNull(mRetrofit);
        return mRetrofit.create(SOService.class);
    }

    public static void requestLogin(MutableLiveData<Throwable> appException, String userName, String password, ResponseResult result) {
        String x = Credentials.basic(AppController.CLIENT_ID, AppController.CLIENT_SECRET);

        FormBody.Builder builder = new FormBody.Builder();
        builder.add("grant_type", "password");
        builder.add("username", userName.trim());
        builder.add("password", password.trim());

        final Request request = new Request.Builder()
                .url(AppController.BASE_LOGIN_URL + "/oauth/token").addHeader("origin", "abc")
                .addHeader("Authorization", x)
                .addHeader("Accept", "application/json, text/plain, */*")
                .addHeader("Content-type", "application/x-www-form-urlencoded")
                .addHeader("Accept-Language", "" + langIdSelected)
                .post(builder.build())
                .build();

        OkHttpClient.Builder builder1 = new OkHttpClient.Builder();

        builder1.build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                result.onResponse(null, null, e, null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String token = response.body().string();
                    Type type = new TypeToken<Map<String, String>>() {
                    }.getType();
                    Gson gson = new Gson();
                    Map<String, String> myMap = gson.fromJson(token, type);
                    TOKEN_DCOM = myMap.get("token_type") + " " + myMap.get("access_token");
                    new HttpHelper.Builder(AppController.getInstance())
                            .initOkHttp()
                            .createRetrofit(BASE_URL)
                            .build();
                    result.onResponse(null, null, token, null);
                } else {
                    result.onResponse(null, null, null, null);
                }

            }
        });
    }

}
