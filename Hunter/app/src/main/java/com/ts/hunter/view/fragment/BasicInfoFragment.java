package com.ts.hunter.view.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.viewmodel.CreateL1VM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.api.widget.Widget;

import java.util.ArrayList;
import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

public class BasicInfoFragment extends BaseFragment {
    private ImageView frontImage, backImage;
    private Spinner nationality;
    private RadioGroup rdGender;
    //list img
    private AlbumFile[] mAlbumFiles;

    private CreateL1VM createL1VM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        createL1VM = ViewModelProviders.of(getBaseActivity()).get(CreateL1VM.class);
        binding.setVariable(com.ts.hunter.BR.viewModel, createL1VM);
        mAlbumFiles = new AlbumFile[2];
        createL1VM.setView(this:: processFromVM);
        assert view != null;
        initView(view);

        return view;
    }

    private void initView(View view) {
        frontImage = view.findViewById(R.id.frontImage);
        backImage = view.findViewById(R.id.backImage);
        TextView txtBirthDay = view.findViewById(R.id.txtBirthday);
        TextView txtCreateDate = view.findViewById(R.id.createDate);
        MaterialButton btnCreate = view.findViewById(R.id.btnNextStep);
        nationality = view.findViewById(R.id.nationality);
        rdGender = view.findViewById(R.id.gender);


        if(!createL1VM.getIsCreate().get()){
            btnCreate.setText(getResources().getString(R.string.update_info));
        }

        btnCreate.setOnClickListener(v -> {
            int gender;
            String national = nationality.getSelectedItem().toString();
            switch (rdGender.getCheckedRadioButtonId()){
                case R.id.first:
                    gender = 1;
                    break;
                case R.id.second:
                    gender = -1;
                    break;
                default:
                    gender = 0;

            }
            createL1VM.getModel().get().setNationality(national);
            createL1VM.getModel().get().setGender(gender);

            createL1VM.createMerchantL1(mAlbumFiles);
        });

        txtBirthDay.setOnClickListener(this::selectDate);
        txtCreateDate.setOnClickListener(this::selectDate);

        frontImage.setOnClickListener(v -> {
            if(!createL1VM.getIsCreate().get()){
                previewImage(mAlbumFiles[0]);
            }else {
                selectImage((ImageView) v, 0);
            }
        });
        backImage.setOnClickListener(v -> {
            if(!createL1VM.getIsCreate().get()){
                previewImage(mAlbumFiles[1]);
            }else {
                selectImage((ImageView) v, 1);
            }
        });

    }

    private void previewImage(AlbumFile albumFile) {
        if (albumFile == null) {
            Toast.makeText(getActivity(), R.string.no_selected, Toast.LENGTH_LONG).show();
        } else {
            ArrayList<AlbumFile> temp = new ArrayList<>();
            temp.add(albumFile);
            Album.galleryAlbum(this)
                    .checkable(false)
                    .checkedList(temp)
                    .currentPosition(0)
                    .widget(
                            Widget.newDarkBuilder(getActivity())
                                    .title(R.string.album_title)
                                    .build()
                    )
                    .start();
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if(action.equals("getUserSuccess")){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(createL1VM.getModel().get().getFullName());
            if(createL1VM.getModel().get().identityImgFront != null && createL1VM.getModel().get().identityImgFront != null){
                AlbumFile front = new AlbumFile();
                front.setPath(AppController.BASE_IMAGE + createL1VM.getModel().get().identityImgFront);
                AlbumFile back = new AlbumFile();
                back.setPath(AppController.BASE_IMAGE + createL1VM.getModel().get().identityImgBack);
                mAlbumFiles[0] = front;
                mAlbumFiles[1] = back;
                createL1VM.getSwipeAble().set(true);
            }

        }
    }

    private void selectDate(View v) {
        if (getContext() != null) {
            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year1, month1, dayOfMonth);
                String date = AppController.formatDate.format(calendar.getTime());
                switch (v.getId()){
                    case  R.id.txtBirthday:
                        ((TextView) v).setText(date);
                        createL1VM.getModel().get().setBirthday(calendar.getTime());
                        break;
                    case  R.id.createDate:
                        ((TextView) v).setText(date);
                        createL1VM.getModel().get().setIdentityCreateDate(calendar.getTime());
                        break;
                }




            }, year, month, day);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

    }


    private void selectImage(ImageView imageView, int index) {
        Album.image(this)
                .multipleChoice()
                .camera(true)
                .columnCount(2)
                .selectCount(2)
                .widget(
                        Widget.newDarkBuilder(getActivity())
                                .title(R.string.album_title)
                                .build())
                .onResult(result -> {
                    switch (result.size()) {
                        case 1:
                            mAlbumFiles[index] = result.get(0);
                            setImage(imageView, result.get(0));
                            break;
                        case 2:
                            mAlbumFiles[0] = result.get(0);
                            mAlbumFiles[1] = result.get(1);
                            setImage(frontImage, result.get(0));
                            setImage(backImage, result.get(1));
                            break;
                    }
                })
                .onCancel(result -> Toast.makeText(getActivity(), R.string.canceled, Toast.LENGTH_SHORT).show())
                .start();
    }

    private void setImage(ImageView image, AlbumFile albumFile) {
        Album.getAlbumConfig().
                getAlbumLoader().
                load(image, albumFile);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_basic_infor;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateL1VM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


}
