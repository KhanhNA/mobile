package com.ts.hunter.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.material.button.MaterialButton;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.hunter.R;
import com.ts.hunter.view.adapter.BaseAdapterV2;
import com.ts.hunter.viewmodel.CreateL1VM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class WareHouseFragment extends BaseFragment {
    private boolean isLoad = true;
    private XRecyclerView rcStore;
    private BaseAdapterV2 wareHouseAdapter;
    CreateL1VM createL1VM;

    private TextView txtEmptyWarehouse, txtCreateHouse;


    @Override
    public void onResume() {
        super.onResume();
        if(isLoad){
            isLoad = false;
            createL1VM.getMerchantWarehouse();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        createL1VM = ViewModelProviders.of(getBaseActivity()).get(CreateL1VM.class);
        binding.setVariable(com.ts.hunter.BR.viewModel, createL1VM);

        innitView(view);

        return view;
    }

    private void innitView(View v) {
        MaterialButton btnNextStep = v.findViewById(R.id.btnNextStep);
        txtEmptyWarehouse = v.findViewById(R.id.txtEmptyWarehouse);
        txtCreateHouse = v.findViewById(R.id.txtCreateHouse);
        rcStore = v.findViewById(R.id.rcStore);
        wareHouseAdapter = new BaseAdapterV2(R.layout.item_store, createL1VM.getListMerchantWareHouse(), this);
        wareHouseAdapter.setConfigXRecycler(rcStore, 1);
        rcStore.setAdapter(wareHouseAdapter);
        rcStore.setPullRefreshEnabled(false);
        rcStore.setLoadingMoreEnabled(false);


        txtCreateHouse.setOnClickListener(v1 -> showDialogSelectWareHouse());

        txtEmptyWarehouse.setOnClickListener(v1->showDialogSelectWareHouse());

        btnNextStep.setOnClickListener(view -> createL1VM.updateStores());

    }


    private void showDialogSelectWareHouse(){
        SelectWareHouseFragment selectWareHouse = new SelectWareHouseFragment(createL1VM);
        selectWareHouse.show(getBaseActivity().getSupportFragmentManager(),selectWareHouse.getTag() );
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String code) {
        if (code != null) {
            switch (code) {
                //getSuccess
                case "selectedStore":
                    wareHouseAdapter.notifyDataSetChanged();
                    break;

            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_warehouses;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateL1VM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
