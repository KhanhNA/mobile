package com.ts.hunter.view.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.databinding.DialogDeactivateBinding;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.viewmodel.MerchantDetailVM;
import com.ts.hunter.viewmodel.UpdateStatusVM;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ViewFunction;

import java.util.Calendar;
import java.util.Date;

@SuppressLint("ValidFragment")
public class DeActiveDialogFragment extends DialogFragment{
    Snackbar snackbar;
    MaterialButton btnSave;
    ImageView btnDismiss;
    TextView txtFromDate, txtToDate, txtReason;

    UpdateStatusVM updateStatusVM;
    MerchantModel merchantModel;
    MerchantDetailVM merchantDetailVM;

    private Date fromDate;
    private Date toDate;
    public DeActiveDialogFragment(UpdateStatusVM updateStatusVM, MerchantModel merchantModel, Snackbar snackbar){
        this.updateStatusVM = updateStatusVM;
        this.merchantModel = merchantModel;
        this.snackbar = snackbar;
    }
    public DeActiveDialogFragment(MerchantDetailVM merchantDetailVM, MerchantModel merchantModel, Snackbar snackbar){
        this.merchantDetailVM = merchantDetailVM;
        this.merchantModel = merchantModel;
        this.snackbar = snackbar;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogDeactivateBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_deactivate, container, false);
        binding.setViewModel(updateStatusVM);
        initView(binding.getRoot());

        return binding.getRoot();
    }

    private void initView(View view) {
        btnSave = view.findViewById(R.id.btnSave);
        btnDismiss = view.findViewById(R.id.btnDismiss);
        txtFromDate = view.findViewById(R.id.txtFromDate);
        txtToDate = view.findViewById(R.id.txtToDate);
        txtReason = view.findViewById(R.id.txtReason);

        Calendar cal = Calendar.getInstance();
        fromDate = cal.getTime();
        // get start of next month
        cal.add(Calendar.MONTH, 1);
        toDate = cal.getTime();
        txtFromDate.setText(AppController.formatDate.format(fromDate.getTime()));
        txtToDate.setText(AppController.formatDate.format(toDate.getTime()));

        txtFromDate.setOnClickListener(v->selectDate(v, fromDate, toDate));
        txtToDate.setOnClickListener(v->selectDate(v, fromDate, toDate));
        btnDismiss.setOnClickListener(v-> this.dismiss());
        btnSave.setOnClickListener(v-> {
            if(updateStatusVM != null) {
                if (updateStatusVM.isValidDeActive(txtReason.getText().toString())) {
                    merchantModel.setLockStartDate(AppController.formatDate_v2.format(fromDate.getTime()));
                    merchantModel.setLockEndDate(AppController.formatDate_v2.format(fromDate.getTime()));
                    merchantModel.setReason(txtReason.getText().toString());
                    showDialogConfirm();
                }
            }else if(merchantDetailVM != null){
                if (merchantDetailVM.isValidDeActive(txtReason.getText().toString())) {
                    merchantModel.setLockStartDate(AppController.formatDate_v2.format(fromDate.getTime()));
                    merchantModel.setLockEndDate(AppController.formatDate_v2.format(fromDate.getTime()));
                    merchantModel.setReason(txtReason.getText().toString());
                    showDialogConfirm();
                }
            }
        });
    }

    private void selectDate(View v, Date fromDate, Date toDate) {
        if (getActivity() != null) {
            Calendar c = Calendar.getInstance();
            switch (v.getId()) {
                case R.id.txtFromDate:
                    c.setTime(fromDate);
                    break;
                case R.id.txtToDate:
                    c.setTime(toDate);
                    break;
            }
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), (view, year1, month1, dayOfMonth) -> {
                // TODO: 12/8/2019 set date for ViewModel
                Calendar calendar = Calendar.getInstance();
                calendar.set(year1, month1, dayOfMonth);
                String date = AppController.formatDate.format(calendar.getTime());
                switch (v.getId()) {
                    case R.id.txtFromDate:
                        ((TextView) v).setText(date);
                        fromDate.setTime(calendar.getTime().getTime());
                        break;
                    case R.id.txtToDate:
                        toDate.setTime(calendar.getTime().getTime());
                        ((TextView) v).setText(date);
                        break;
                }

            }, year, month, day);
            if (v.getId() == R.id.txtToDate) {
                datePickerDialog.getDatePicker().setMinDate(fromDate.getTime());
            }
            datePickerDialog.show();
        }

    }

    private void showDialogConfirm() {
        new AlertDialog.Builder(getActivity()).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.message_confirm_activate)
                .setPositiveButton(R.string.OK, (dialog, which) -> {
                    dismiss();
                    snackbar.show();
                    merchantModel.setLockStartDate(AppController.formatDate_v2.format(fromDate.getTime()));
                    merchantModel.setLockStartDate(AppController.formatDate_v2.format(toDate.getTime()));
                    if(updateStatusVM != null){
                        updateStatusVM.updateStatus(merchantModel);
                    }else if (merchantDetailVM != null) {
                        merchantDetailVM.updateStatus(merchantModel);
                    }
                })
                .setNegativeButton(R.string.CANCEL, null)
                .show();
    }

}


