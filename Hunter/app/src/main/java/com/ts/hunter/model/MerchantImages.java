package com.ts.hunter.model;

import androidx.annotation.Nullable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class MerchantImages {
    private Long id;
    private Long merchantId;
    private String url;
    private Integer type;
    private String contractNumber;
    private boolean isExist;

    public MerchantImages(String url) {
        this.url = url;
    }

    public MerchantImages setExits(boolean isExist){
        this.isExist = isExist;
        return this;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        return ((MerchantImages) obj).id.equals(this.id);
    }
}
