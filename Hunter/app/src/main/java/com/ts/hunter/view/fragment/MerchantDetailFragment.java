package com.ts.hunter.view.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.model.RevenueDTO;
import com.ts.hunter.utils.DayAxisValueFormatter;
import com.ts.hunter.utils.MyValueFormatter;
import com.ts.hunter.utils.XYMarkerView;
import com.ts.hunter.viewmodel.MerchantDetailVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantDetailFragment extends BaseFragment {
    private Snackbar snackbar;
    private BarChart barChart;
    private MerchantDetailVM merchantDetailVM;

    private TextView txtEmpty;
    private String merchantUserName;

    private Date toDate;
    private Date fromDate;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        merchantDetailVM = (MerchantDetailVM) viewModel;
        initView(view);
        setupToolBar(view);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            if (intent.hasExtra("merchantUserName")) {
                merchantUserName = intent.getStringExtra("merchantUserName");
                new Handler().postDelayed(() -> merchantDetailVM.getUserInfo(merchantUserName), 200);
            }
        }
        return view;
    }

    private final RectF onValueSelectedRectF = new RectF();

    private void initView(View view) {
        TextView txtPhoneNumber = view.findViewById(R.id.txtPhoneNumber);
        TextView txtFromDate = view.findViewById(R.id.txtFromDate);
        TextView txtToDate = view.findViewById(R.id.txtToDate);
        barChart = view.findViewById(R.id.barChart);
        LinearLayout btnUserInfo = view.findViewById(R.id.btnUserInfo);
        txtEmpty = view.findViewById(R.id.txtEmpty);
        MaterialButton activeMerchant = view.findViewById(R.id.activeMerchant);
        MaterialButton deactivateMerchant = view.findViewById(R.id.deactivateMerchant);


        toDate = Calendar.getInstance().getTime();
        txtToDate.setText(AppController.formatDate.format(toDate.getTime()));
        // get start of the month
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        fromDate = cal.getTime();
        txtFromDate.setText(AppController.formatDate.format(fromDate.getTime()));


        snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.updating, Snackbar.LENGTH_INDEFINITE);
        //---------------------set listener------------------------
        btnUserInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CommonActivity.class);
            intent.putExtra("FRAGMENT", CreateL1Fragment.class);
            intent.putExtra("merchantUserName", merchantUserName);
            startActivity(intent);
        });
        txtPhoneNumber.setOnClickListener(v -> onCall());

        //set listener
        txtFromDate.setOnClickListener(v -> selectDate(v, fromDate, toDate));
        txtToDate.setOnClickListener(v -> selectDate(v, fromDate, toDate));

        activeMerchant.setOnClickListener(v->showActiveDialog(merchantDetailVM.getModelE()));
        deactivateMerchant.setOnClickListener(v->showDeActivateDialog(merchantDetailVM.getModelE(), snackbar));



        //-------------------- init bar chart -----------------
        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e == null)
                    return;
                barChart.getBarBounds((BarEntry) e, onValueSelectedRectF);
                MPPointF position = barChart.getPosition(e, YAxis.AxisDependency.LEFT);
                MPPointF.recycleInstance(position);

            }

            @Override
            public void onNothingSelected() {

            }
        });


        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);

        barChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);

        //định dạng giá trị chiều ngang
        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        //định dạng giá trị theo chiều dọc
        ValueFormatter custom = new MyValueFormatter("");
        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)


        XYMarkerView mv = new XYMarkerView(getBaseActivity(), xAxisFormatter);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart


        barChart.animateXY(1500, 1500);

    }
    private void setData(SparseArray<RevenueDTO> listData) {
        float start = 1f;
        ArrayList<BarEntry> values = new ArrayList<>();
        //dump data
        if (listData != null && listData.size() != 0) {
            txtEmpty.setVisibility(View.GONE);
            for (int i = (int) start; i < start + 12; i++) {
                RevenueDTO revenueDTO = listData.get(i);
                if (revenueDTO != null) {
                    values.add(new BarEntry(i, revenueDTO.getAmount().floatValue()));
                } else {
                    values.add(new BarEntry(i, 0));
                }
            }
        }else {
            barChart.setVisibility(View.GONE);
        }

        BarDataSet set1;
        if (barChart.getData() != null &&
                barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            Calendar calendar = Calendar.getInstance();
            set1 = new BarDataSet(values, getString(R.string.total_revenue_of_year) + " " + calendar.get(Calendar.YEAR));
            set1.setDrawIcons(false);

            int startColor = ContextCompat.getColor(getBaseActivity(), R.color.da_cam);
            int endColor = ContextCompat.getColor(getBaseActivity(), R.color.da_cam_dark);
            set1.setGradientColor(startColor, endColor);
            set1.setDrawValues(false);
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);
            barChart.setData(data);
        }
    }

    private void onCall() {
        new AlertDialog.Builder(getBaseActivity()).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.make_phone_call)
                .setPositiveButton(R.string.OK, (dialog, which) -> {
                    int permissionCheck = ContextCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.CALL_PHONE);
                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(
                                getBaseActivity(),
                                new String[]{Manifest.permission.CALL_PHONE},
                                123);
                    } else {
                        startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + merchantDetailVM.getModelE().getMobilePhone())));
                    }
                })
                .setNegativeButton(R.string.CANCEL, null)
                .show();
    }

    private void setupToolBar(View v){
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        assert appCompatActivity != null;
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    private void selectDate(View v, Date fromDate, Date toDate) {
        if (getContext() != null) {
            Calendar c = Calendar.getInstance();
            switch (v.getId()) {
                case R.id.txtFromDate:
                    c.setTime(fromDate);
                    break;
                case R.id.txtToDate:
                    c.setTime(toDate);
                    break;
            }
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
                // TODO: 12/8/2019 set date for ViewModel
                Calendar calendar = Calendar.getInstance();
                calendar.set(year1, month1, dayOfMonth);
                String date = AppController.formatDate.format(calendar.getTime());
                switch (v.getId()) {
                    case R.id.txtFromDate:
                        ((TextView) v).setText(date);
                        //getData
                        fromDate.setTime(calendar.getTime().getTime());
                        merchantDetailVM.getMerchantDashBoard(merchantDetailVM.getModelE().getMerchantId(), AppController.formatDate_v2.format(fromDate.getTime()),
                                AppController.formatDate_v2.format(toDate.getTime()));
                        break;
                    case R.id.txtToDate:
                        //getData
                        toDate.setTime(calendar.getTime().getTime());
                        merchantDetailVM.getMerchantDashBoard(merchantDetailVM.getModelE().getMerchantId(), AppController.formatDate_v2.format(fromDate.getTime()),
                                AppController.formatDate_v2.format(toDate.getTime()));
                        ((TextView) v).setText(date);
                        break;
                }

            }, year, month, day);
            if (v.getId() == R.id.txtToDate) {
                datePickerDialog.getDatePicker().setMinDate(fromDate.getTime());
            }
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action){
            case "getUserSuccess":
                Calendar cal = Calendar.getInstance();
                merchantDetailVM.getRevenueMonth(merchantDetailVM.getModelE().getMerchantId(),cal.get(Calendar.YEAR), 1);
                merchantDetailVM.getMerchantDashBoard(merchantDetailVM.getModelE().getMerchantId(),
                        AppController.formatDate_v2.format(fromDate.getTime()),
                        AppController.formatDate_v2.format(toDate.getTime()));
                break;
            case "getRevenueMonth":
                List<RevenueDTO> listData = viewModel.getBaseModelsE();
                SparseArray<RevenueDTO> hashData = new SparseArray<>();
                if (listData != null) {
                    for (RevenueDTO revenueDTO : listData) {
                        hashData.put(revenueDTO.getMonth(), revenueDTO);
                    }
                }
                setData(hashData);
                break;
            case "updateStatusSuccess":
            case "updateStatusFail":
                snackbar.dismiss();
                break;
        }
    }



    private void showActiveDialog(MerchantModel o) {
        new AlertDialog.Builder(Objects.requireNonNull(getActivity())).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.message_confirm_deactivate)
                .setPositiveButton(R.string.OK, (dialog, which) -> {
                    snackbar.show();
                    merchantDetailVM.updateStatus(o);
                })
                .setNegativeButton(R.string.CANCEL, null)
                .show();
    }

    private void showDeActivateDialog(MerchantModel model, Snackbar snackbar) {
        DeActiveDialogFragment dialogDeActive = new DeActiveDialogFragment(merchantDetailVM, model, snackbar);
        dialogDeActive.show(getBaseActivity().getFragmentManager(), dialogDeActive.getTag());
    }


    @SuppressLint("ShowToast")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 123) {
            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                onCall();
            } else {
                Toast.makeText(getContext(), R.string.permission_failed, Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public int getLayoutRes() {
        return R.layout.fragment_merchant_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MerchantDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
