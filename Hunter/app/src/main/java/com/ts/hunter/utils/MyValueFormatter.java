package com.ts.hunter.utils;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.ts.hunter.base.AppController;

public class MyValueFormatter extends ValueFormatter {
    private String suffix;

    public MyValueFormatter(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String getFormattedValue(float value) {
        return AppController.formatNumber(value) + " " + suffix;
    }

}