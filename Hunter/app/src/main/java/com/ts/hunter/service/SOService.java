package com.ts.hunter.service;

import com.ts.hunter.model.BaseResponse;
import com.ts.hunter.model.DashBoardDTO;
import com.ts.hunter.model.MerchantImages;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.model.MerchantWareHouseModel;
import com.ts.hunter.model.RevenueDTO;
import com.tsolution.base.ServiceResponse;

import java.util.List;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface SOService {
    @GET("merchant/user")
    Flowable<MerchantModel> getMerchantByUserName(@Query("userName") String userName);

    @Multipart
    @POST("merchant/create/info")
    Flowable<MerchantModel> createMerchantL1(@Part("merchant") RequestBody body, @Part MultipartBody.Part[] files);

    @POST("merchants/update-status")
    Flowable<BaseResponse> updateStatus(@Body MerchantModel merchantModel);

    @POST("merchants/update")
    Flowable<MerchantModel> updateMerchant(@Body MerchantModel body);

    @POST("merchant/save/store")
    Flowable<BaseResponse> updateStores(@Body List<MerchantWareHouseModel> body, @Query("merchantId") Long merchantId);

    @GET("hunter-dashboard")
    Flowable<DashBoardDTO> getDashBoard(@Query("parentMerchantId") Long parentMerchantId,
                                        @Query("fromDate") String fromDate,
                                        @Query("toDate") String toDate);

    @GET("merchant-dashboard")
    Flowable<DashBoardDTO> getMerchantDashBoard(@Query("merchantId") Long merchantId,
                                                @Query("fromDate") String fromDate,
                                                @Query("toDate") String toDate);

    @GET("merchant-revenue")
    Flowable<List<RevenueDTO>> getRevenueMonth(@Query("merchantId") Long parentMerchantId,
                                               @Query("year") Integer year,
                                               @Query("type") int type);

    @Multipart
    @POST("merchant/image")
    Flowable<MerchantModel> updateMerchantImg(@Query("merchantId") Long merchantId,
                                              @Query("type") Integer type,
                                              @Query("contractNumber") String contractNumber,
                                              @Query("idsDelete") Long[] ids,
                                              @Part MultipartBody.Part[] files);
    @POST("merchant/image")
    Flowable<MerchantModel> updateMerchantImg(@Query("merchantId") Long merchantId,
                                              @Query("type") Integer type,
                                              @Query("contractNumber") String contractNumber,
                                              @Query("idsDelete") Long[] ids);

    @POST("common/list")
    Flowable<ServiceResponse<MerchantModel>> getMerchants(@Body MerchantModel merchantModel, @Query("entityName") String entityName,
                                                          @Query("page") int page, @Query("pageSize") int pageSize);

    @GET("merchants/data-map")
    Flowable<ServiceResponse<MerchantModel>> getDataOnMap(@Query("parentMerchantId") Long parentId, @Query("action") Integer action, @Query("status") Integer status,
                                                          @Query("page") int page, @Query("pageSize") int pageSize);

    @GET("merchants/search")
    Flowable<ServiceResponse<MerchantModel>> searchMerchants(@Query("keyword") String keyword,
                                                             @Query("parentMerchantId") Long parentParentId,
                                                             @Query("status") Integer status,
                                                             @Query("fromDate") String fromDate,
                                                             @Query("toDate") String toDate,
                                                             @Query("noRevenue") int noRevenue,
                                                             @Query("page") int page);

    @POST("common/list")
    Flowable<ServiceResponse<MerchantImages>> getMerchantImages(@Body MerchantImages merchantModel, @Query("entityName") String entityName);

    @POST("common/list")
    Flowable<ServiceResponse<MerchantWareHouseModel>> getMerchantWareHouse(@Body MerchantWareHouseModel merchantModel, @Query("entityName") String entityName);


}
