// Generated with g9.

package com.ts.hunter.model;

import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.tsolution.base.BaseModel;
import com.yanzhenjie.album.Album;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MerchantModel extends BaseModel implements Serializable {

    private Long merchantId;
    private String merchantCode;
    private String merchantName;
    private Long parentMarchantId;
    private Long merchantTypeId;
    private String userName; //MERCHANT_CODE lấy luôn là số điện thoại của đại lý
    private String password;
    private String fullName;
    private Long defaultStoreId;
    private Double tax;
    private String merchantImgUrl;
    private String mobilePhone; //MERCHANT_CODE lấy luôn là số điện thoại của đại lý
    private String activeCode;
    private Date activeDate;
    private Date deactiveDate;
    private Date registerDate;
    private Date birthday;
    private Integer activeStatus;
    private Integer status; //0-chờ duyệt, 1-hoạt động 2-không hoạt động
    private int action; //0-không action, 1- yêu cầu active, 2- yêu cầu hủy active, 3- yêu cầu tạo.
    private String address;
    private String reason;
    private String lockStartDate;
    private String lockEndDate;

    public Double lat;
    public Double longitude;
    public Date dateBirth;
    public String province;
    public String district;
    public String village;

    public Integer gender;// 0:other, 1:nam , -1:nữ
    public Date identityCreateDate;
    public String identityAddressBy;
    public Integer businessModelId;
    public Integer acreageId;
    public Integer electricalInfrastructureId;
    public Integer displayDeviceId;
    public Integer refrigerationEquipmentId;
    public String identityNumber;
    public String nationality;

    public String identityImgFront;
    public String identityImgBack;

    //attr - group
    private List<MerchantGroup> groupValues;
    private List<MerchantAttr> attValues;
    private List<AttrMapValue> mapAttValues;



    public String getLongitudeStr(){
        return this.longitude == null ? "" : longitude + "";
    }
    public void setLongitudeStr(String str){
        this.longitude = Double.parseDouble(str);
    }

    public String getLatStr(){
        return this.lat == null ? "" : lat + "";
    }
    public void setLatStr(String str){
        this.lat = Double.parseDouble(str);
    }

    public String getBirthdayStr() {
        return dateBirth == null ? "" : AppController.formatDate.format(this.dateBirth);
    }


    public String getIdentityCreateDateStr(){
        return identityCreateDate == null ? "" : AppController.formatDate.format(this.identityCreateDate);
    }



    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .apply(new RequestOptions().override(500, 500))
                .error(R.drawable.ic_stub)
                .placeholder(R.drawable.ic_stub)
                .transition(new DrawableTransitionOptions().crossFade())
                .into(view);
    }

    @BindingAdapter("nationality")
    public static void setNational(Spinner spinner, String nationality) {
       String[] nationals = spinner.getContext().getResources().getStringArray(R.array.country);
       for(int i = 0; i < nationals.length; i++){
           if(nationals[i].equals(nationality)){
               spinner.setSelection(i);
           }
       }
    }
    @BindingAdapter("gender")
    public static void setGender(RadioGroup view, Integer gender) {
        if(gender != null){
            switch (gender){
                case 0:
                    view.check(view.getChildAt(2).getId());
                    break;
                case -1:
                    view.check(view.getChildAt(1).getId());
                    break;
                case 1:
                    view.check(view.getChildAt(0).getId());
                    break;
            }

        }
    }

}
