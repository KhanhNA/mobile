package com.ts.hunter.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.model.MerchantModel;

import de.hdodenhof.circleimageview.CircleImageView;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private TextView txtName;
    private TextView txtPhone;
    private TextView txtAddress;
    private CircleImageView contactImg;
    private ImageView imgStatus;
    private Context context;


    public CustomInfoWindowAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    private void render(Marker marker) {
        MerchantModel merchantModel = (MerchantModel) marker.getTag();
        if(merchantModel != null){
            txtPhone.setText(merchantModel.getMobilePhone());
            txtAddress.setText(merchantModel.getAddress());
            txtName.setText(merchantModel.getFullName());
            imgStatus.setVisibility(merchantModel.getStatus() == 0 ? View.GONE : View.VISIBLE);
            Glide.with(context).load(AppController.BASE_IMAGE +  merchantModel.getMerchantImgUrl())
                    .centerCrop()
                    .apply(new RequestOptions().override(200, 200))
                    .placeholder(R.drawable.ic_stub)
                    .error(R.drawable.ic_stub)
                    .into(contactImg);
        }
    }

    private void init(View view) {
        txtAddress = view.findViewById(R.id.txtAddress);
        txtPhone = view.findViewById(R.id.txtPhoneNumber);
        txtName = view.findViewById(R.id.txtName);
        imgStatus = view.findViewById(R.id.imgStatus);
        Drawable iconAddress = context.getResources().getDrawable(R.drawable.location);
        txtAddress.setCompoundDrawablesWithIntrinsicBounds(iconAddress, null, null, null);

        Drawable iconPhone = context.getResources().getDrawable(R.drawable.mobile_phone);
        txtPhone.setCompoundDrawablesWithIntrinsicBounds(iconPhone, null, null, null);

        contactImg = view.findViewById(R.id.contact_img);
    }

    @Override
    public View getInfoContents(final Marker marker) {
        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.map_window_properties, null);
        init(view);
        render(marker);
        return view;
    }

}