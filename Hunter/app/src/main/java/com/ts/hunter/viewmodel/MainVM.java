package com.ts.hunter.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.ts.hunter.model.MerchantModel;
import com.tsolution.base.BaseViewModel;

public class MainVM extends BaseViewModel<MerchantModel> {
    public MainVM(@NonNull Application application) {
        super(application);
    }
}
