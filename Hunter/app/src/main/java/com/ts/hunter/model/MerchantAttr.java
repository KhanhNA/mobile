package com.ts.hunter.model;

import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantAttr extends BaseModel {
    Long id;
    String code;
    String name;
    Integer required;
    List<MerchantAttrValue> lstValue;

    public void setChecked(Long valueId){
        for(MerchantAttrValue value : lstValue){
            if(value.getId().equals(valueId)){
                value.setIsSelect(1);
            }else {
                value.setIsSelect(0);
            }
        }
    }
}
