package com.ts.hunter.model;

import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantAttrValue extends BaseModel {
    Long id;
    String code;
    String name;
    Integer isSelect;
}
