package com.ts.hunter.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.base.http.HttpHelper;
import com.ts.hunter.base.http.rx.RxSchedulers;
import com.ts.hunter.model.BaseResponse;
import com.ts.hunter.model.DashBoardDTO;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.model.RevenueDTO;
import com.ts.hunter.service.network.rx.RxSubscriber;
import com.ts.hunter.utils.StringUtils;
import com.ts.hunter.utils.ToastUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantDetailVM extends BaseViewModel<MerchantModel> {
    ObservableBoolean loadingVisible = new ObservableBoolean();
    private ObservableField<DashBoardDTO> dashBoard = new ObservableField<>();
    private BaseViewModel revenueMonths;
    public MerchantDetailVM(@NonNull Application application) {
        super(application);
        dashBoard.set(new DashBoardDTO());
        revenueMonths = new BaseViewModel(application);
    }

    public void getUserInfo(String userName) {
        loadingVisible.set(true);
        callApi(HttpHelper.getInstance().getApi().getMerchantByUserName(userName)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<MerchantModel>() {
                    @Override
                    public void onSuccess(MerchantModel merchantModel) {
                        loadingVisible.set(false);
                        model.set(merchantModel);
                        try {
                            view.action("getUserSuccess", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when load user info");
                    }
                }));
    }
    public void getMerchantDashBoard(Long merchantId, String fromDate, String toDate) {
        loadingVisible.set(true);
        callApi(HttpHelper.getInstance().getApi().getMerchantDashBoard(merchantId, fromDate, toDate)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<DashBoardDTO>() {
                    @Override
                    public void onSuccess(DashBoardDTO dashBoardDTO) {
                        loadingVisible.set(false);
                        dashBoard.set(dashBoardDTO);
                        try {
                            view.action("getDashBoardSuccess", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when load user info");
                    }
                }));
    }

    /**
     * @param
     * type 0: revenue of all merchant with parent is this hunter
     * type 1: revenue of merchantId
     * @param year revenue month of year
     */
    public void getRevenueMonth(Long merchantId ,Integer year, int type){
        loadingVisible.set(true);
        callApi(HttpHelper.getInstance().getApi().getRevenueMonth(merchantId, year, type)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<RevenueDTO>>() {
                    @Override
                    public void onSuccess(List<RevenueDTO> response) {
                        revenueMonths.setData(response);
                        try {
                            view.action("getRevenueMonth", null, revenueMonths, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                        loadingVisible.set(false);
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when load dashboard info");
                    }
                }));
    }

    public void updateStatus(MerchantModel merchantModel){
        MerchantModel temp = new MerchantModel();
        temp.setMerchantId(merchantModel.getMerchantId());
        temp.setParentMarchantId(merchantModel.getParentMarchantId());
        // status want to update
        temp.setStatus(merchantModel.getStatus() == 1 ? 2 : 1);
        temp.setReason(merchantModel.getReason());
        temp.setLockStartDate(merchantModel.getLockStartDate());
        temp.setLockEndDate(merchantModel.getLockEndDate());

        callApi(HttpHelper.getInstance().getApi().updateStatus(temp)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<BaseResponse>() {
                    @Override
                    public void onSuccess(BaseResponse response) {
                        loadingVisible.set(false);
                        MerchantModel result = getModelE();
                        result.setStatus(temp.getStatus());
                        //update action
                        result.setAction(temp.getStatus() == 1 ? 1 : 2);
                        result.setReason(temp.getReason());
                        result.setLockStartDate(temp.getLockStartDate());
                        result.setLockEndDate(temp.getLockEndDate());
                        model.notifyChange();
                        try {
                            view.action("updateStatusSuccess", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        try {
                            view.action("updateStatusFail", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when update");
                    }
                }));
    }

    public boolean isValidDeActive(String reason) {
        boolean isValid = true;
        if (StringUtils.isNullOrEmpty(reason)) {
            isValid = false;
            addError("reason", R.string.INVALID_FIELD, true);
        } else {
            clearErro("reason");
        }

        return isValid;
    }

    //text action pending
    public String getTitleAction(int action) {
        switch (action) {
            case 1:
                return AppController.getInstance().getResources().getString(R.string.pending_activate);
            case 2:
                return AppController.getInstance().getResources().getString(R.string.pending_deactivate);
        }
        return AppController.getInstance().getResources().getString(R.string.pending);
    }

}
