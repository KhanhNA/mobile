package com.ts.hunter.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.ts.hunter.BR;
import com.ts.hunter.R;
import com.ts.hunter.model.MerchantImages;
import com.ts.hunter.view.adapter.ImagesAdapter;
import com.ts.hunter.viewmodel.CreateL1VM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.api.widget.Widget;
import com.yanzhenjie.album.widget.divider.Api21ItemDivider;
import com.yanzhenjie.album.widget.divider.Divider;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ContractFragment extends BaseFragment {
    private boolean isLoad = true;
    //list img
    private ArrayList<AlbumFile> mAlbumFiles;
    private ImagesAdapter imgAdapter;

    private TextView hintImg;
    private CreateL1VM createL1VM;
    @Override
    public void onResume() {
        super.onResume();
        if(isLoad){
            isLoad = false;
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  super.onCreateView(inflater, container, savedInstanceState);

        createL1VM = ViewModelProviders.of(getBaseActivity()).get(CreateL1VM.class);
        binding.setVariable(BR.viewModel, createL1VM);

        assert view != null;
        initView(view);

        createL1VM.getImageContract().observe(this, list-> {
            if(list.size() != 0){
                hintImg.setVisibility(View.GONE);
            }else {
                hintImg.setVisibility(View.VISIBLE);
            }
            imgAdapter.notifyDataSetChanged(list, null);
            if(mAlbumFiles != null){
                mAlbumFiles = null;
            }
        });

        return view;
    }


    private void initView(View view) {
        TextView txtCreateImage = view.findViewById(R.id.txtCreateImg);
        hintImg = view.findViewById(R.id.hintImg);
        MaterialButton btnNextStep = view.findViewById(R.id.btnNextStep);

        //init images view
        RecyclerView rcImages = view.findViewById(R.id.rcImages);
        Divider divider = new Api21ItemDivider(Color.TRANSPARENT, 10, 10);
        rcImages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rcImages.addItemDecoration(divider);
        imgAdapter = new ImagesAdapter(getActivity(), (v, position) -> {
            if(v.getId() == R.id.clearImage){
                MerchantImages temp = imgAdapter.getMerchantImages().get(position);
                if(temp.isExist()){
                    showDialog(temp);
                }else{
                    Toast.makeText(getActivity(), "delete albumFile", Toast.LENGTH_SHORT).show();
                    imgAdapter.getMerchantImages().remove(position);
                    imgAdapter.notifyDataSetChanged();
                }
            }else {
                previewImage(position);
            }
        });
        rcImages.setAdapter(imgAdapter);

        txtCreateImage.setOnClickListener(v-> selectImage());
        btnNextStep.setOnClickListener(v-> createL1VM.updateContractImg(mAlbumFiles));



    }

    private void showDialog(MerchantImages merchantImage) {
        new AlertDialog.Builder(getBaseActivity()).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.confirm_delete_image)
                .setPositiveButton(R.string.OK, (dialog, which) -> {
                    List<MerchantImages> temp = createL1VM.getImageContract().getValue();
                    temp.remove(merchantImage);
                    createL1VM.getImageContract().setValue(temp);
                    createL1VM.getIdsDelete().add(merchantImage.getId());

                })
                .setNegativeButton(R.string.CANCEL, null)
                .show();
    }

    private void previewImage(int position) {
        if (imgAdapter.getAlbumFiles() == null || imgAdapter.getAlbumFiles().size() == 0) {
            Toast.makeText(getActivity(), R.string.no_selected, Toast.LENGTH_SHORT).show();
        }  else {
            Album.galleryAlbum(this)
                    .checkable(false)
                    .checkedList(imgAdapter.getAlbumFiles())
                    .currentPosition(position)
                    .widget(
                            Widget.newDarkBuilder(getActivity())
                                    .title(R.string.album_title)
                                    .build()
                    )
                    .start();
        }
    }

    private void selectImage() {
        Album.image(this)
                .multipleChoice()
                .camera(true)
                .columnCount(2)
                .selectCount(10)
                .checkedList(mAlbumFiles)
                .widget(
                        Widget.newDarkBuilder(getContext())
                                .title(R.string.album_title)
                                .build()
                )
                .onResult(result -> {
                    if(result.size() != 0){
                        hintImg.setVisibility(View.GONE);
                    }else {
                        hintImg.setVisibility(View.VISIBLE);
                    }
                    mAlbumFiles = result;
                    imgAdapter.notifyDataSetChanged(createL1VM.getImageContract().getValue(), createL1VM.convertData(mAlbumFiles));
                })
                .onCancel(result -> Toast.makeText(getActivity(), R.string.canceled, Toast.LENGTH_LONG).show())
                .start();
    }





    @Override
    public int getLayoutRes() {
        return R.layout.fragment_contract;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateL1VM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


}
