package com.ts.hunter.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputEditText;
import com.ts.hunter.BR;
import com.ts.hunter.R;
import com.ts.hunter.model.AttrMapValue;
import com.ts.hunter.model.MerchantAttr;
import com.ts.hunter.model.MerchantGroup;
import com.ts.hunter.model.MerchantImages;
import com.ts.hunter.utils.StringUtils;
import com.ts.hunter.utils.ToastUtils;
import com.ts.hunter.utils.TsUtils;
import com.ts.hunter.view.activity.MapsActivity;
import com.ts.hunter.view.adapter.ImagesAdapter;
import com.ts.hunter.view.adapter.MerchantGroupAdapter;
import com.ts.hunter.viewmodel.CreateL1VM;
import com.ts.hunter.widget.ItemGroupMerchantAttr;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.api.widget.Widget;
import com.yanzhenjie.album.widget.divider.Api21ItemDivider;
import com.yanzhenjie.album.widget.divider.Divider;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MatrixPropertiesFragment extends BaseFragment {
    private boolean isLoad = true;
    private static final int SELECT_LOCATION = 1412;
    private TextInputEditText txtLong, txtLat, txtProvince, txtDistrict, txtAddress;
    private AutoCompleteTextView txtMerchantGroup;
    private MerchantGroupAdapter merchantGroupAdapter;
    //list img
    private ArrayList<AlbumFile> mAlbumFiles;
    private ImagesAdapter imgAdapter;

    private TextView hintImg;
    private CreateL1VM createL1VM;

    private ChipGroup chipGroupMerchant;
    private LinearLayout merchant_group_attr_container;

    private List<ItemGroupMerchantAttr> listMerchantAttrs;

    private boolean isValid;

    @Override
    public void onResume() {
        super.onResume();
        if (isLoad) {
            isLoad = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        createL1VM = ViewModelProviders.of(getBaseActivity()).get(CreateL1VM.class);
        binding.setVariable(BR.viewModel, createL1VM);

        assert view != null;
        initView(view);

        createL1VM.getImageStore().observe(this, list -> {
            if (list.size() != 0) {
                hintImg.setVisibility(View.GONE);
            } else {
                hintImg.setVisibility(View.VISIBLE);
            }
            imgAdapter.notifyDataSetChanged(list, null);
            if (mAlbumFiles != null) {
                mAlbumFiles = null;
            }
        });


        createL1VM.getMerchantGroup().observe(this, list -> {
            if (list.size() != 0) {
                hintImg.setVisibility(View.GONE);
                showMerchantGroup(list);
                merchantGroupAdapter.notifyDataSetChanged(list);
            }
        });

        createL1VM.getMerchantAttr().observe(this, list -> {
            if (list.size() != 0) {
                showAttrValue(list);
            }
        });

        return view;
    }

    private void showMerchantGroup(List<MerchantGroup> list) {
        for (MerchantGroup group : list) {
            if(group.getIsSelect() != null && group.getIsSelect() == 1){
                Chip chip = new Chip(getBaseActivity());
                chip.setTag(group);
                chip.setText(group.getName());
                chip.setCloseIconVisible(true);
                chip.setCheckable(false);

                if (createL1VM.getSelectedGroup().get(group.getId()) == null) {
                    createL1VM.getSelectedGroup().put(group.getId(), group);
                    chipGroupMerchant.addView(chip);
                }

                chip.setOnCloseIconClickListener(v -> {
                    chipGroupMerchant.removeView(v);
                    createL1VM.getSelectedGroup().remove(((MerchantGroup) v.getTag()).getId());
                });
            }
        }
    }

    private void showAttrValue(List<MerchantAttr> list) {
        listMerchantAttrs.clear();
        if (!TsUtils.isNullOrEmpty(list)) {
            merchant_group_attr_container.setVisibility(View.VISIBLE);
            merchant_group_attr_container.removeAllViews();
            for (MerchantAttr attr : list) {
                if (attr.getLstValue() != null && attr.getRequired() == 1) {
                    ItemGroupMerchantAttr itemAttr = new ItemGroupMerchantAttr(getContext(), attr);
                    listMerchantAttrs.add(itemAttr);
                    merchant_group_attr_container.addView(itemAttr);
                }
            }
            TextView expander = new TextView(getContext());
            expander.setPadding(10, 10, 10, 10);
            expander.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            Drawable icon = getBaseActivity().getResources().getDrawable(R.drawable.ic_expand_more_black_24dp);
            expander.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
            expander.setText(R.string.expander);
            expander.setOnClickListener(v->showAttrNotRequite(list));
            merchant_group_attr_container.addView(expander);
        }
    }

    private void showAttrNotRequite(List<MerchantAttr> list){
        listMerchantAttrs.clear();
        if (!TsUtils.isNullOrEmpty(list)) {
            merchant_group_attr_container.setVisibility(View.VISIBLE);
            merchant_group_attr_container.removeAllViews();
            for (MerchantAttr attr : list) {
                if (attr.getLstValue() != null) {
                    ItemGroupMerchantAttr itemAttr = new ItemGroupMerchantAttr(getContext(), attr);
                    listMerchantAttrs.add(itemAttr);
                    merchant_group_attr_container.addView(itemAttr);
                }
            }
            TextView collapse = new TextView(getContext());
            collapse.setPadding(20, 20, 20, 20);
            collapse.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            Drawable icon = getBaseActivity().getResources().getDrawable(R.drawable.ic_expand_less_black_24dp);
            collapse.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
            collapse.setText(R.string.collapse);
            collapse.setOnClickListener(v->showAttrValue(list));
            merchant_group_attr_container.addView(collapse);
        }
    }


    private void initView(View view) {
        listMerchantAttrs = new ArrayList<>();
        txtLong = view.findViewById(R.id.txtLong);
        txtLat = view.findViewById(R.id.txtLat);
        TextView txtCreateImage = view.findViewById(R.id.txtCreateImg);
        hintImg = view.findViewById(R.id.hintImg);
        MaterialButton btnNextStep = view.findViewById(R.id.btnNextStep);
        txtDistrict = view.findViewById(R.id.txtDistrict);
        txtProvince = view.findViewById(R.id.txtProvince);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtMerchantGroup = view.findViewById(R.id.txtMerchantGroup);
        chipGroupMerchant = view.findViewById(R.id.chipGroupMerchant);

        merchant_group_attr_container = view.findViewById(R.id.merchant_group_attr_container);

        //init images view
        RecyclerView rcImages = view.findViewById(R.id.rcImages);
        Divider divider = new Api21ItemDivider(Color.TRANSPARENT, 10, 10);
        rcImages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rcImages.addItemDecoration(divider);
        imgAdapter = new ImagesAdapter(getActivity(), (v, position) -> {
            if (v.getId() == R.id.clearImage) {
                MerchantImages temp = imgAdapter.getMerchantImages().get(position);
                if (temp.isExist()) {
                    showDialog(temp);
                } else {
                    Toast.makeText(getActivity(), "delete albumFile", Toast.LENGTH_SHORT).show();
                    imgAdapter.getMerchantImages().remove(position);
                    imgAdapter.notifyDataSetChanged();
                }
            } else {
                previewImage(position);
            }
        });
        rcImages.setAdapter(imgAdapter);

        //init merchant group
        merchantGroupAdapter = new MerchantGroupAdapter(getBaseActivity(), R.layout.layout_item_merchant_group,
                (ArrayList<MerchantGroup>) createL1VM.getMerchantGroup().getValue());
        txtMerchantGroup.setThreshold(1);
        txtMerchantGroup.setAdapter(merchantGroupAdapter);
        txtMerchantGroup.setTextColor(getResources().getColor(R.color.gray2));

        txtMerchantGroup.setOnItemClickListener((adapterView, view1, i, l) -> {
            txtMerchantGroup.setText("");
            MerchantGroup group = (MerchantGroup) adapterView.getItemAtPosition(i);

            Chip chip = new Chip(getBaseActivity());
            chip.setTag(group);
            chip.setText(group.getName());
            chip.setCloseIconVisible(true);
            chip.setCheckable(false);

            if (createL1VM.getSelectedGroup().get(group.getId()) == null) {
                createL1VM.getSelectedGroup().put(group.getId(), group);
                chipGroupMerchant.addView(chip);
            } else {
                ToastUtils.showToast(R.string.group_is_selected);
            }

            chip.setOnCloseIconClickListener(v -> {
                chipGroupMerchant.removeView(v);
                createL1VM.getSelectedGroup().remove(((MerchantGroup) v.getTag()).getId());
            });
        });

        txtCreateImage.setOnClickListener(v -> selectImage());
        hintImg.setOnClickListener(v -> selectImage());

        txtLat.setOnClickListener(v -> openMaps());
        txtLong.setOnClickListener(v -> openMaps());

        btnNextStep.setOnClickListener(v -> {
            createL1VM.getModelE().setMapAttValues(getValueAttr());
            if(isValid){
                createL1VM.updateMerchant(mAlbumFiles);
            }
        });

    }

    private List<AttrMapValue> getValueAttr() {
        isValid = true;
        List<AttrMapValue> temp = new ArrayList<>();
        for (ItemGroupMerchantAttr item : listMerchantAttrs) {
            if(item.getMerchantAttr().getRequired() == 1){
                if(item.getSelectedItem() != null){
                    AttrMapValue itemValue = new AttrMapValue();
                    itemValue.setAttId(item.getMerchantAttr().getId());
                    itemValue.setAttValueId(item.getSelectedItem().getId());
                    temp.add(itemValue);
                    item.getTxtAttrName().setError(null);
                }else {
                    isValid = false;
                    item.getTxtAttrName().setError(getResources().getString(R.string.INVALID_FIELD));
                }

            }else {
                if (item.getSelectedItem() != null) {
                    AttrMapValue itemValue = new AttrMapValue();
                    itemValue.setAttId(item.getMerchantAttr().getId());
                    itemValue.setAttValueId(item.getSelectedItem().getId());
                    temp.add(itemValue);
                }
            }

        }
        return temp;
    }

    private void showDialog(MerchantImages merchantImage) {
        new AlertDialog.Builder(getBaseActivity()).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.confirm_delete_image)
                .setPositiveButton(R.string.OK, (dialog, which) -> {
                    List<MerchantImages> temp = createL1VM.getImageStore().getValue();
                    if (temp != null) {
                        temp.remove(merchantImage);
                    }
                    createL1VM.getImageStore().setValue(temp);
                    createL1VM.getIdsDelete().add(merchantImage.getId());

                })
                .setNegativeButton(R.string.CANCEL, null)
                .show();
    }

    private void openMaps() {
        Intent intent = new Intent(getActivity(), MapsActivity.class);
        startActivityForResult(intent, SELECT_LOCATION);
    }

    private void previewImage(int position) {
        if (imgAdapter.getAlbumFiles() == null || imgAdapter.getAlbumFiles().size() == 0) {
            Toast.makeText(getActivity(), R.string.no_selected, Toast.LENGTH_SHORT).show();
        } else {
            Album.galleryAlbum(this)
                    .checkable(false)
                    .checkedList(imgAdapter.getAlbumFiles())
                    .currentPosition(position)
                    .widget(
                            Widget.newDarkBuilder(getActivity())
                                    .title(R.string.album_title)
                                    .build()
                    )
                    .start();
        }
    }


    private void selectImage() {
        Album.image(this)
                .multipleChoice()
                .camera(true)
                .columnCount(2)
                .selectCount(10)
                .checkedList(mAlbumFiles)
                .widget(
                        Widget.newDarkBuilder(getContext())
                                .title(R.string.album_title)
                                .build()
                )
                .onResult(result -> {
                    if (result.size() != 0) {
                        hintImg.setVisibility(View.GONE);
                    } else {
                        hintImg.setVisibility(View.VISIBLE);
                    }
                    mAlbumFiles = result;
                    imgAdapter.notifyDataSetChanged(createL1VM.getImageStore().getValue(), createL1VM.convertData(mAlbumFiles));
                })
                .onCancel(result -> Toast.makeText(getActivity(), R.string.canceled, Toast.LENGTH_LONG).show())
                .start();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_LOCATION && resultCode == Activity.RESULT_OK) {
            assert data != null;

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(data.getDoubleExtra("lat", 0),
                        data.getDoubleExtra("long", 0), 1);
                String address = addresses.get(0).getAddressLine(0);
                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                if (!StringUtils.isNullOrEmpty(address)) {
                    txtAddress.setText(address);
                }
                if (!StringUtils.isNullOrEmpty(addresses.get(0).getAdminArea())) {
                    txtProvince.setText(addresses.get(0).getAdminArea());
                }
                if (!StringUtils.isNullOrEmpty(addresses.get(0).getAdminArea())) {
                    txtDistrict.setText(addresses.get(0).getSubAdminArea());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            txtLong.setText(data.getDoubleExtra("long", 0) + "");
            txtLat.setText(data.getDoubleExtra("lat", 0) + "");
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_matrix_properties;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateL1VM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
