package com.ts.hunter.viewmodel;

import android.app.Application;
import android.content.res.Resources;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.base.http.HttpHelper;
import com.ts.hunter.base.http.rx.RxSchedulers;
import com.ts.hunter.model.BaseResponse;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.service.network.rx.RxSubscriber;
import com.ts.hunter.utils.StringUtils;
import com.ts.hunter.utils.ToastUtils;
import com.ts.hunter.utils.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.ServiceResponse;
import com.tsolution.base.exceptionHandle.AppException;
import com.yanzhenjie.album.AlbumFile;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateStatusVM extends BaseViewModel {
    Resources resources;

    private int totalPage = 0;
    private int currentPage = 0;
    public int indexChange;
    private Boolean isDashBoard;
    private ObservableBoolean loadingVisible = new ObservableBoolean();

    public UpdateStatusVM(@NonNull Application application) {
        super(application);
        resources = application.getResources();
    }

    public void getMerchants(MerchantModel merchantModel, Date fromDate, Date toDate, int noRevenue, int page) {
        String startDate = null, endDate = null;
        if(fromDate != null){
            startDate = AppController.formatDate_v2.format(fromDate);
        }
        if(fromDate != null){
            endDate = AppController.formatDate_v2.format(toDate);
        }

        if(merchantModel == null){
            merchantModel = new MerchantModel();
            merchantModel.setParentMarchantId(AppController.getMerchantId());
        }

        callApi(HttpHelper.getInstance().getApi().searchMerchants(merchantModel.getFullName(),merchantModel.getParentMarchantId(), merchantModel.getStatus(), startDate, endDate, noRevenue, page)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<MerchantModel>>() {
                    @Override
                    public void onSuccess(ServiceResponse<MerchantModel> response) {
                        getMerchantsSuccess(response);
                    }
                }));
    }

    private void getMerchantsSuccess(ServiceResponse<MerchantModel> response) {
        if (response != null && response.getArrData() != null) {
            List<MerchantModel> merchantModels = response.getArrData();
            totalPage = response.getTotalPages();
            addNewPage(merchantModels, totalPage);
            try {
                view.action("getMerchantSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }


    public void updateStatus(MerchantModel model){
        MerchantModel temp = new MerchantModel();
        temp.setMerchantId(model.getMerchantId());
        temp.setParentMarchantId(model.getParentMarchantId());
        // status want to update
        temp.setStatus(model.getStatus() == 1 ? 2 : 1);
        temp.setReason(model.getReason());
        temp.setLockStartDate(model.getLockStartDate());
        temp.setLockEndDate(model.getLockEndDate());

        callApi(HttpHelper.getInstance().getApi().updateStatus(temp)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<BaseResponse>() {
                    @Override
                    public void onSuccess(BaseResponse response) {
                        loadingVisible.set(false);
                        MerchantModel result = ((MerchantModel)getBaseModelsE().get(model.index - 1));
                        result.setStatus(temp.getStatus());
                        //update action
                        result.setAction(temp.getStatus() == 1 ? 1 : 2);
                        result.setReason(temp.getReason());
                        result.setLockStartDate(temp.getLockStartDate());
                        result.setLockEndDate(temp.getLockEndDate());
                        try {
                            view.action("updateStatusSuccess", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        try {
                            view.action("updateStatusFail", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when update");
                    }
                }));
    }


    //invoke
    public void onRefresh(MerchantModel merchantModel, Date fromDate, Date toDate, int noRevenue) {
        currentPage = 0;
        getMerchants(merchantModel, fromDate, toDate, noRevenue, 0);
        getBaseModelsE().clear();
    }


    public void getMoreMerchant(MerchantModel merchantModel , Date fromDate, Date toDate, int noRevenue) {
        if (totalPage > 0 && currentPage < totalPage) {
            currentPage ++;
            getMerchants(merchantModel, fromDate, toDate, noRevenue, currentPage);
        } else {
            try {
                view.action("noMore", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
    }

    public boolean isValidDeActive(String reason) {
        boolean isValid = true;
        if (StringUtils.isNullOrEmpty(reason)) {
            isValid = false;
            addError("reason", R.string.INVALID_FIELD, true);
        } else {
            clearErro("reason");
        }

        return isValid;
    }

    //text action pending
    public String getTitleAction(int action) {
        switch (action) {
            case 1:
                return resources.getString(R.string.pending_activate);
            case 2:
                return resources.getString(R.string.pending_deactivate);
        }
        return resources.getString(R.string.pending);
    }
}
