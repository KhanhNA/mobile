package com.ts.hunter.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

import com.ts.hunter.R;

public class NonSwipeViewPager extends ViewPager {
    private boolean swipeAble;
    public NonSwipeViewPager(Context context) {
        super(context);
    }
    public NonSwipeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NonSwipeViewPager);
        try {
            swipeAble = a.getBoolean(R.styleable.NonSwipeViewPager_swipeAble, true);
        } finally {
            a.recycle();
        }
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return swipeAble && super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return swipeAble && super.onTouchEvent(event);
    }

    public void setSwipeable(Boolean disable){
        //When disable = true not work the scroll and when disble = false work the scroll
        this.swipeAble = disable;
    }
}
