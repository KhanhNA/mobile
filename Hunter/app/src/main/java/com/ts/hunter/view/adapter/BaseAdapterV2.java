package com.ts.hunter.view.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.hunter.BR;
import com.ts.hunter.R;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

import lombok.Getter;

@Getter
public class BaseAdapterV2 extends RecyclerView.Adapter<BaseAdapterV2.ViewHolder> implements AdapterListener {
    private AdapterListener adapterListener;
    private ObservableField<List> datas;
    private int layoutItem;
    private XRecyclerView mBaseRecyclerView;
    private BaseViewModel viewModel;

    public BaseAdapterV2(@LayoutRes int item, BaseViewModel viewModel, AdapterListener listener) {
        this.layoutItem = item;
        this.datas = viewModel.baseModels;
        this.adapterListener = listener;
    }

    public void update(List<Object> lst) {
        this.datas.set(lst);
    }


    public void setConfigXRecycler(XRecyclerView xRecycler, int spanCount) {
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(spanCount,
                StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        xRecycler.setLayoutManager(layoutManager);

        xRecycler.setRefreshProgressStyle(ProgressStyle.Pacman);
        xRecycler.setLoadingMoreProgressStyle(ProgressStyle.BallRotate);
        xRecycler.setArrowImageView(R.drawable.mango);

        this.mBaseRecyclerView = xRecycler;
    }

    public void setConfigXRecyclerHorizontal(XRecyclerView xRecycler, int spanCount) {
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(spanCount,
                StaggeredGridLayoutManager.HORIZONTAL);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        xRecycler.setLayoutManager(layoutManager);

        xRecycler.setRefreshProgressStyle(ProgressStyle.Pacman);
        xRecycler.setLoadingMoreProgressStyle(ProgressStyle.BallRotate);
        xRecycler.setArrowImageView(R.drawable.mango);

        this.mBaseRecyclerView = xRecycler;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), this.layoutItem, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        BaseModel bm = (BaseModel) ((List) this.datas.get()).get(position);
        bm.index = position + 1;
        viewHolder.bind(bm);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);

    }

    @Override
    public int getItemCount() {
        return this.datas.get() == null ? 0 : ((List) this.datas.get()).size();
    }

    public void onItemClick(View v, Object o) {
        if (adapterListener != null) {
            adapterListener.onItemClick(v, o);
        }
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding itemProductBinding;

        public ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.itemProductBinding = view;
            view.getRoot().setOnLongClickListener(v -> {
                try {
                    if (mBaseRecyclerView != null) {
                        Log.d("test_ppp", "P: " + (getAdapterPosition() - 1));
                        adapterListener.onItemLongClick(v, datas.get().get(getAdapterPosition() - 1));
                    } else {
                        Log.d("test_ppp", "P: " + getAdapterPosition());
                        adapterListener.onItemLongClick(v, datas.get().get(getAdapterPosition()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            });
            view.getRoot().setOnClickListener(v -> {
                try {
                    if (mBaseRecyclerView != null) {
                        Log.d("test_ppp", "P: " + (getAdapterPosition() - 1));
                        adapterListener.onItemClick(v, datas.get().get(getAdapterPosition() - 1));
                    } else {
                        Log.d("test_ppp", "P: " + getAdapterPosition());
                        adapterListener.onItemClick(v, datas.get().get(getAdapterPosition()));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
        }

        public void bind(Object obj) {
            itemProductBinding.setVariable(BR.viewHolder, obj);
//            itemProductBinding.setVariable(BR.viewModel, viewModel);
            itemProductBinding.setVariable(BR.listenerAdapter, adapterListener);
            itemProductBinding.executePendingBindings();
        }

        public void bindWithVM(BaseViewModel viewModel) {
            itemProductBinding.setVariable(BR.viewModel, viewModel);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
