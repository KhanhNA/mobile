package com.ts.hunter.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DashBoardDTO extends BaseModel {
    private long totalRegister;
    private long totalPending;
    private long totalActive;
    private long totalInactive;
    private long totalMerchantInactiveRevenue;
    private long totalOrder;
    private double totalRevenue;

}