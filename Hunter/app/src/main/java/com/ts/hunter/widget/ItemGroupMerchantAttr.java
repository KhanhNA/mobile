package com.ts.hunter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.radiobutton.MaterialRadioButton;
import com.ts.hunter.R;
import com.ts.hunter.model.MerchantAttr;
import com.ts.hunter.model.MerchantAttrValue;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemGroupMerchantAttr extends LinearLayout {

    MerchantAttrValue selectedItem;
    RadioGroup radioGroup;
    MerchantAttr merchantAttr;
    TextView txtAttrName;

    private int selectedValue = -1;

    public ItemGroupMerchantAttr(Context context, MerchantAttr merchantAttr) {
        super(context);
        this.merchantAttr = merchantAttr;
        init(context);
    }
    public ItemGroupMerchantAttr(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ItemGroupMerchantAttr(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init(Context context) {
        final View rootView =
                LayoutInflater.from(context).inflate(R.layout.layout_item_attr, this, true);
        txtAttrName = rootView.findViewById(R.id.txtAttrName);
        txtAttrName.setText(merchantAttr.getName());
        radioGroup = rootView.findViewById(R.id.rdg_attr_container);


        for(MerchantAttrValue attrValue : merchantAttr.getLstValue()){
            MaterialRadioButton radioButton = new MaterialRadioButton(context);
            radioButton.setPadding(0, 0, 20, 0);
            radioButton.setTag(attrValue);
            if(attrValue.getName() != null){
                radioButton.setText(attrValue.getName());
            }
            radioGroup.addView(radioButton);
            if(attrValue.getIsSelect() == 1){
                radioButton.setChecked(true);
                selectedItem = attrValue;
            }
        }

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton radioButton = radioGroup.findViewById(checkedId);
            if(radioButton != null){
                int idx = radioGroup.indexOfChild(radioButton);

                selectedItem = (MerchantAttrValue) radioButton.getTag();
                merchantAttr.getLstValue().get(idx).setIsSelect(1);
                if(selectedValue != -1){
                    merchantAttr.getLstValue().get(selectedValue).setIsSelect(0);
                }

                selectedValue = idx;
            }
        });

    }
}
