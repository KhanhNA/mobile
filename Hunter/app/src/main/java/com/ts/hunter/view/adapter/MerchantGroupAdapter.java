package com.ts.hunter.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.ts.hunter.R;
import com.ts.hunter.model.MerchantGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

public class MerchantGroupAdapter extends ArrayAdapter<MerchantGroup> {
    private ArrayList<MerchantGroup> items;
    private ArrayList<MerchantGroup> itemsAll;
    private ArrayList<MerchantGroup> suggestions;
    private int viewResourceId;

    public MerchantGroupAdapter(Context context, int viewResourceId, ArrayList<MerchantGroup> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<MerchantGroup>) items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        MerchantGroup customer = suggestions.get(position);
        if (customer != null) {
            TextView code = v.findViewById(R.id.txtCode);
            if (code != null) {
                code.setText(customer.getCode());
            }
            TextView name = v.findViewById(R.id.txtName);
            if (name != null) {
                name.setText(customer.getName());
            }
        }
        return v;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    private Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((MerchantGroup)(resultValue)).getName();
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (MerchantGroup customer : itemsAll) {
                    if(customer.getName().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            customer.getCode().toLowerCase().contains(constraint.toString().toLowerCase())){
                        suggestions.add(customer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<MerchantGroup> filteredList = (ArrayList<MerchantGroup>) results.values;
            if(results.count > 0) {
                clear();
                for (MerchantGroup c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

    public void notifyDataSetChanged(List<MerchantGroup> list){
        this.items = (ArrayList<MerchantGroup>) list;
        this.itemsAll  = new ArrayList<>(items);
        this.notifyDataSetChanged();
    }

}
