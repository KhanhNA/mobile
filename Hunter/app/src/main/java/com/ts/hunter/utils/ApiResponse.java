package com.ts.hunter.utils;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

import static com.ts.hunter.utils.Status.ERROR;
import static com.ts.hunter.utils.Status.LOADING;
import static com.ts.hunter.utils.Status.NOT_CONNECT;
import static com.ts.hunter.utils.Status.SUCCESS;


/**
 * Created by PhamBien.
 */

public class ApiResponse {
    public final Status status;
    @Nullable
    public final Throwable error;
    Object object;

    private ApiResponse(Status status, Object o, @Nullable Throwable error) {
        this.status = status;
        this.error = error;
        this.object = o;
    }

    public static ApiResponse loading() {
        return new ApiResponse(LOADING, null, null);
    }

    public static ApiResponse success(Object o) {
        return new ApiResponse(SUCCESS, o, null);
    }

    public static ApiResponse error(@NonNull Throwable error) {
        return new ApiResponse(ERROR, null, error);
    }
    public static ApiResponse notConnect(@NonNull Throwable error) {
        return new ApiResponse(NOT_CONNECT, null, error);
    }

}
