/*
 * Copyright 2016 Yan Zhenjie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ts.hunter.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ts.hunter.R;
import com.ts.hunter.model.MerchantImages;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.impl.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mInflater;
    private OnItemClickListener mItemClickListener;

    private List<MerchantImages> merchantImages;

    public ImagesAdapter(Context context, OnItemClickListener itemClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mItemClickListener = itemClickListener;
    }

    public ArrayList<AlbumFile> getAlbumFiles() {
        ArrayList<AlbumFile> albumFiles = new ArrayList<>();
        for (MerchantImages merchantImage : merchantImages) {
            AlbumFile albumFile = new AlbumFile();
            albumFile.setPath(merchantImage.getUrl());
            albumFile.setChecked(true);
            albumFiles.add(albumFile);
        }
        return albumFiles;
    }

    public void notifyDataSetChanged(List<MerchantImages> originList, List<MerchantImages> files) {

        this.merchantImages = new ArrayList<>();
        if(originList != null){
            this.merchantImages.addAll(originList);
        }
        if (files != null) {
            this.merchantImages.addAll(files);
        }
        super.notifyDataSetChanged();
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageViewHolder(mInflater.inflate(R.layout.item_content_image, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ImageViewHolder) holder).setData(merchantImages.get(position));

    }

    @Override
    public int getItemCount() {
        return merchantImages == null ? 0 : merchantImages.size();
    }

    private static class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final OnItemClickListener mItemClickListener;
        private ImageView mIvImage;

        ImageViewHolder(View itemView, OnItemClickListener itemClickListener) {
            super(itemView);
            this.mItemClickListener = itemClickListener;
            this.mIvImage = itemView.findViewById(R.id.iv_album_content_image);
            ImageView clear = itemView.findViewById(R.id.clearImage);

            clear.setOnClickListener(this);
            itemView.setOnClickListener(this);

        }

        void setData(MerchantImages albumFile) {
            Album.getAlbumConfig().
                    getAlbumLoader().
                    load(mIvImage, albumFile.getUrl());
        }


        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }


}
