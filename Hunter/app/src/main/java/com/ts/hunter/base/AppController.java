package com.ts.hunter.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import com.ts.hunter.base.http.HttpHelper;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.utils.MediaLoader;
import com.ts.hunter.utils.StringUtils;
import com.tsolution.base.RetrofitClient;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

public class AppController extends MyApplication {
    public static final String API_GOOGLE_MAP = "AIzaSyDLtkodzb8ivLLg50q19eObdg4zs9MT04I";
    private static AppController mInstance;
    public static final String DOMAIN_NAME = "http://mstore.nextsolutions.com.vn";
    public static final String BASE_URL = DOMAIN_NAME + ":8234/api/v1/";


    public static final String BASE_LOGIN_URL = "http://sso.nextsolutions.com.vn";
    public static final String MERCHANT = "MERCHANT";
    public static final String BASE_IMAGE = BASE_URL + "files/";


    public static final String CLIENT_ID = "Dcommerce";
    public static final String CLIENT_SECRET = "A31b4c24l3kj35d4AKJQ";
    public static final String APP_INFO = "A31b4c24l3kj35d4AKJQ";
    public static final String DATE_PATTERN = "dd/MM/yyyy";
    public static final String DATE_PATTERN_V2 = "yyyy-MM-dd";
    public static final String DATE_PATTERN_GSON = "yyyy-MM-dd'T'HH:mm:ss";

    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDate = new SimpleDateFormat(AppController.DATE_PATTERN);
    public static final SimpleDateFormat formatDate_v2 = new SimpleDateFormat(AppController.DATE_PATTERN_V2);
    public static final SimpleDateFormat formatDateGson = new SimpleDateFormat(AppController.DATE_PATTERN_GSON);

    private SharedPreferences sharedPreferences;
    private DecimalFormat formatter;
    HashMap<String, Object> clientCache;
    public static HashMap<String, Object> authens = new HashMap<>();

    public String current_user_firebase_id = "";

    static {
        RetrofitClient.BASE_URL = BASE_URL;
        RetrofitClient.BASE_URL_OAUTH = BASE_LOGIN_URL;
        RetrofitClient.clientId = CLIENT_ID;
        RetrofitClient.clientSecret = CLIENT_SECRET;
        RetrofitClient.DATE_FORMAT = DATE_PATTERN_GSON;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        /* Glide */
        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .setLocale(Locale.getDefault())
                .build()
        );

        mInstance = this;
        sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        clientCache = new HashMap<>();
        new HttpHelper.Builder(AppController.getInstance())
                .initOkHttp()
                .createRetrofit(AppController.BASE_URL)
                .build();


    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public SharedPreferences getSharePre() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences.edit();
    }

    /**
     * save object vao file
     *
     * @author: PhamBien
     * @return: void
     * @throws:
     */
    public void saveObject(String fileName, Object object) {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), fileName);
            if (!file.exists()) {
                file.mkdirs();
            }
            FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch (Exception e) {
            Log.e("Error: ", "Loi write file: " + e.getMessage());
        }
    }


    /**
     * Kiem tra file ton tai hay khong
     *
     * @author PhamBien
     */
    public static boolean isExistFile(String fileName) {
        try {
            if (!StringUtils.isNullOrEmpty(fileName)) {
                String[] s = AppController.getInstance()
                        .fileList();
                for (String s1 : s) {
                    if (fileName.equals(s1)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            Log.w("File", "" + e.getMessage());
        }
        return false;
    }

    /**
     * Doc file
     *
     * @author: PhamBien
     * @return: void
     * @throws:
     */
    public static Object readObject(String fileName) {
        Object object = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            if (isExistFile(fileName)) {
                fis = AppController.getInstance()
                        .openFileInput(fileName);
                if (fis != null) {// ton tai file
                    ois = new ObjectInputStream(fis);
                    object = ois.readObject();

                }
            }
        } catch (Exception e) {
            Log.e("", "" + e);
            object = null;
            Log.w("", "" + e);
        } finally {
            try {
                if (ois != null) {
                    ois.close();
                }
            } catch (Exception e) {
                Log.e("", "" + e);
            }
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {
                Log.e("", "" + e);
            }
        }
        return object;
    }

    public String formatCurrency(Double money) {
        if (money == null) {
            return "0 đ";
        }
        if (formatter == null) {
            formatter = new DecimalFormat("###,###,###");
        }

        return formatter.format(money) + " đ";
    }

    public void putCatche(String key, Object obj) {
        if (clientCache == null) {
            clientCache = new HashMap<>();
        }
        clientCache.put(key, obj);
    }

    public Object getFromCache(String key) {
        if (clientCache == null) {
            return null;
        }
        return clientCache.get(key);
    }

    public static Long getMerchantId() {
        return getCurrentMerchant() != null ? getCurrentMerchant().getMerchantId() : null;
    }

    public static MerchantModel getCurrentMerchant() {
        return (MerchantModel) AppController.getInstance().getFromCache(AppController.MERCHANT);
    }

    @SuppressLint("DefaultLocale")
    public static String formatNumber(double number) {
//        if (number >= 1000000000) {
//            return String.format("%.1fT", number / 1000000000.0);
//        }
        if (number >= 1000000000) {
            return String.format("%.1fB", number / 1000000000.0);
        }

        if (number >= 1000000) {
            return String.format("%.1fM", number / 1000000.0);
        }

        if (number >= 1000) {
            return String.format("%.1fK", number / 1000.0);
        }
        return String.valueOf(number);
    }

    public void clearCache() {
        clientCache = new HashMap<>();
    }
}
