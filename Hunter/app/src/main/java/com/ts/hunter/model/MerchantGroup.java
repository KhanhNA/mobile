package com.ts.hunter.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantGroup extends BaseModel {
    private Long id;
    private String code;
    private String name;
    private Integer isSelect;

}
