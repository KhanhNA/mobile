package com.ts.hunter.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttrMapValue {
    private Long attId;
    private Long attValueId;
}
