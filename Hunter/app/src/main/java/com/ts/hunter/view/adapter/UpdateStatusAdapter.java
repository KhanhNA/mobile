package com.ts.hunter.view.adapter;

import androidx.annotation.NonNull;

import com.ts.hunter.viewmodel.UpdateStatusVM;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

public class UpdateStatusAdapter extends BaseAdapterV2 {
    UpdateStatusVM viewModel;
    public UpdateStatusAdapter(int item, UpdateStatusVM viewModel, AdapterListener listener) {
        super(item, viewModel, listener);
        this.viewModel = viewModel;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        viewHolder.bindWithVM(viewModel);
    }
}
