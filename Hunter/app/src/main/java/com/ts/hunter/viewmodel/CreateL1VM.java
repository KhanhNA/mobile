package com.ts.hunter.viewmodel;

import android.app.Application;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import android.util.LongSparseArray;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.base.http.HttpHelper;
import com.ts.hunter.base.http.rx.RxSchedulers;
import com.ts.hunter.model.BaseResponse;
import com.ts.hunter.model.MerchantAttr;
import com.ts.hunter.model.MerchantGroup;
import com.ts.hunter.model.MerchantImages;
import com.ts.hunter.model.MerchantModel;
import com.ts.hunter.model.MerchantWareHouseModel;
import com.ts.hunter.model.WareHouseModel;
import com.ts.hunter.service.network.rx.RxSubscriber;
import com.ts.hunter.utils.StringUtils;
import com.ts.hunter.utils.ToastUtils;
import com.ts.hunter.utils.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.ServiceResponse;
import com.tsolution.base.exceptionHandle.AppException;
import com.yanzhenjie.album.AlbumFile;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;

import lombok.Getter;
import lombok.Setter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@Getter
@Setter
public class CreateL1VM extends BaseViewModel<MerchantModel> {
    Resources resources;
    private int totalPage = 0;
    private int currentPage = 1;
    private ObservableField<Boolean> isCreate = new ObservableField<>(true);
    private ObservableField<Boolean> swipeAble = new ObservableField<>(false);

    private ObservableBoolean loadingVisible = new ObservableBoolean();


    //---------list merchant group attr ---------------
    private MutableLiveData<List<MerchantGroup>> merchantGroup = new MutableLiveData<>();
    private MutableLiveData<List<MerchantAttr>> merchantAttr = new MutableLiveData<>();
    private LongSparseArray<MerchantGroup> selectedGroup;

    private ObservableField<String> numberContract;
    private ObservableField<Integer> recordCount = new ObservableField<>();

    private MutableLiveData<List<MerchantImages>> imageStore;
    private MutableLiveData<List<MerchantImages>> imageContract;

    //list merchantWarehouse
    private BaseViewModel listWareHouse;
    private BaseViewModel listMerchantWareHouse;
    private HashMap<String, WareHouseModel> hashMapMerchantWareHouse;

    //list id image delete
    private List<Long> idsDelete;

    public CreateL1VM(@NonNull Application application) {
        super(application);
        model.set(new MerchantModel());
        merchantGroup.setValue(new ArrayList<>());
        merchantAttr.setValue(new ArrayList<>());
        resources = application.getResources();
        listWareHouse = new BaseViewModel(application);
        listMerchantWareHouse = new BaseViewModel(application);
        selectedGroup = new LongSparseArray<>();
        idsDelete = new ArrayList<>();
        imageStore = new MutableLiveData<>();
        imageContract = new MutableLiveData<>();
        numberContract = new ObservableField<>();
    }

    public void resetData() {
        model.set(new MerchantModel());
        merchantGroup.setValue(new ArrayList<>());
        merchantAttr.setValue(new ArrayList<>());
        selectedGroup = new LongSparseArray<>();
        idsDelete = new ArrayList<>();
        imageStore = new MutableLiveData<>();
        imageContract = new MutableLiveData<>();
        numberContract = new ObservableField<>();
    }

    public void createMerchantL1(AlbumFile[] filesPath) {
        if (isValidBasicInfo(filesPath)) {
            loadingVisible.set(true);
            if (isCreate.get()) {
                MultipartBody.Part[] parts = new MultipartBody.Part[filesPath.length];
                for (int i = 0; i < filesPath.length; i++) {
                    File file = new File(filesPath[i].getPath());
                    RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                    MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
                    parts[i] = part;
                }

                model.get().setUserName(model.get().getMobilePhone());
                model.get().setParentMarchantId(AppController.getMerchantId());
                Gson gson = new Gson();

                String json = gson.toJson(model.get());
                RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);

                callApi(HttpHelper.getInstance().getApi().createMerchantL1(bodyJson, parts)
                        .compose(RxSchedulers.io_main())
                        .subscribeWith(new RxSubscriber<MerchantModel>() {
                            @Override
                            public void onSuccess(MerchantModel merchantModel) {
                                Log.e("upload", "success");
                                model.set(merchantModel);
                                loadingVisible.set(false);
                                getUserInfo(merchantModel.getUserName());
                                EventBus.getDefault().post(1);
                                swipeAble.set(true);
                            }

                            @Override
                            public void onFailure(String msg, int code) {
                                Log.e("upload", "false");
                                loadingVisible.set(false);
                                if (msg.contains("Identity")) {
                                    addError("identityCard", R.string.INVALID_FIELD, true);
                                }
                            }
                        }));
            } else {
                callApi(HttpHelper.getInstance().getApi().updateMerchant(model.get())
                        .compose(RxSchedulers.io_main())
                        .subscribeWith(new RxSubscriber<MerchantModel>() {
                            @Override
                            public void onSuccess(MerchantModel response) {
                                Log.e("upload", "success");
                                model.set(response);
                                loadingVisible.set(false);
                                EventBus.getDefault().post(1);
                                swipeAble.set(true);
                            }

                            @Override
                            public void onFailure(String msg, int code) {
                                Log.e("upload", "false");
                                loadingVisible.set(false);
                                if (msg.contains("Identity")) {
                                    addError("identityCard", R.string.INVALID_FIELD, true);
                                }
                            }
                        }));
            }
        } else {
            ToastUtils.showToast(R.string.enter_requite_field);
        }

    }

    private boolean isValidBasicInfo(AlbumFile[] filesPath) {
        MerchantModel temp = model.get();
        boolean isValid = true;
        if (temp != null) {
            if (StringUtils.isNullOrEmpty(temp.getFullName())) {
                isValid = false;
                addError("fullName", R.string.INVALID_FIELD, true);
            } else {
                if (temp.getFullName().length() < 6) {
                    addError("fullName", R.string.FULL_NAME_MIN_LENGTH, true);
                } else {
                    clearErro("fullName");
                }

            }

            if (StringUtils.isNullOrEmpty(temp.getMobilePhone())) {
                isValid = false;
                addError("phoneNumber", R.string.INVALID_FIELD, true);
            } else {
                clearErro("phoneNumber");
            }

            if (temp.getBirthday() == null) {
                isValid = false;
                addError("birthDay", R.string.INVALID_FIELD, true);
            } else {
                clearErro("birthDay");
            }

            if (StringUtils.isNullOrEmpty(temp.getIdentityNumber())) {
                isValid = false;
                addError("identityCard", R.string.INVALID_FIELD, true);
            } else {
                clearErro("identityCard");
            }

//            if (temp.getIdentityCreateDate() == null) {
//                isValid = false;
//                addError("issuedDate", R.string.INVALID_FIELD, true);
//            } else {
//                clearErro("issuedDate");
//            }

            if (StringUtils.isNullOrEmpty(temp.getIdentityAddressBy())) {
                isValid = false;
                addError("issuedBy", R.string.INVALID_FIELD, true);
            } else {
                clearErro("issuedBy");
            }

            if (filesPath[0] == null) {
                isValid = false;
                addError("frontImg", R.string.INVALID_FIELD, true);
            } else {
                clearErro("frontImg");
            }

            if (filesPath[1] == null) {
                isValid = false;
                addError("backImg", R.string.INVALID_FIELD, true);
            } else {
                clearErro("backImg");
            }

        }
        return isValid;
    }

    private <C> List<C> asList(LongSparseArray<C> sparseArray) {
        if (sparseArray == null) return null;
        List<C> arrayList = new ArrayList<>(sparseArray.size());
        for (int i = 0; i < sparseArray.size(); i++)
            arrayList.add(sparseArray.valueAt(i));
        return arrayList;
    }

    public void getWareHouses(int page, RequestQueue queue, Integer distance) {
        final String url = AppController.DOMAIN_NAME + ":8888/stores/near-here";
        Uri.Builder builder = Uri.parse(url).buildUpon();
        builder.appendQueryParameter("currentLat", Objects.requireNonNull(model.get()).getLatStr());
        builder.appendQueryParameter("currentLng", Objects.requireNonNull(model.get()).getLongitudeStr());
        builder.appendQueryParameter("maxDistance", distance + "");
        builder.appendQueryParameter("pageNumber", page + "");
        builder.appendQueryParameter("pageSize", 20 + "");
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, builder.build().toString(), null,
                response -> {
                    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.sss'Z'").create();
                    try {
                        List<WareHouseModel> wareHouses = new ArrayList<>();
                        JSONArray jr = new JSONArray(response.getString("content"));
                        totalPage = response.getInt("totalPages");
                        if (jr.length() > 0) {
                            for (int i = 0; i < jr.length(); i++) {
                                wareHouses.add(gson.fromJson(jr.get(i).toString(), WareHouseModel.class));
                            }
                        }
                        getMerchantWareHouseSuccess(wareHouses);
                        listWareHouse.addNewPage(wareHouses, totalPage);
                        EventBus.getDefault().post("getSuccess");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                error -> {
                    EventBus.getDefault().post("getFail");
                    Log.e("Error.Response", "" + error.getMessage());
                }
        ) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", HttpHelper.TOKEN_DCOM);
                return params;
            }

        };
        queue.add(getRequest);

    }

    public void getMerchantWarehouse() {
        loadingVisible.set(true);
        recordCount.set(0);
        hashMapMerchantWareHouse = new HashMap<>();
        MerchantWareHouseModel merchantWarehouse = new MerchantWareHouseModel();
        merchantWarehouse.setMerchantId(Objects.requireNonNull(model.get()).getMerchantId());
        callApi(HttpHelper.getInstance().getApi().getMerchantWareHouse(merchantWarehouse, "MerchantWareHouse")
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<MerchantWareHouseModel>>() {
                    @Override
                    public void onSuccess(ServiceResponse<MerchantWareHouseModel> response) {
                        loadingVisible.set(false);
                        if (response.getArrData() != null && response.getArrData().size() != 0) {
                            //convert Warehouse 8888 ->8234
                            recordCount.set(response.getArrData().size());
                            List<WareHouseModel> responseData = new ArrayList<>();
                            for (MerchantWareHouseModel wareHouse : response.getArrData()) {
                                WareHouseModel model = new WareHouseModel();
                                model.setAddress(wareHouse.getStoreAddress());
                                model.setPostalCode(wareHouse.getPostalCode());
                                model.setPhone(wareHouse.getPhone());
                                model.setCode(wareHouse.getStoreCode());
                                model.setName(wareHouse.getStoreName());
                                model.checked = true;
                                hashMapMerchantWareHouse.put(model.getCode(), model);
                                responseData.add(model);
                            }
                            listMerchantWareHouse.setData(responseData);
                            EventBus.getDefault().post("selectedStore");

                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when load user info");
                    }
                }));
    }

    public void selectedStore() {
        List<WareHouseModel> wareHouseList = listWareHouse.getBaseModelsE();
        listMerchantWareHouse.getBaseModelsE();
        for (WareHouseModel warehouse : wareHouseList) {
            if (hashMapMerchantWareHouse.get(warehouse.getCode()) != null) {
                hashMapMerchantWareHouse.get(warehouse.getCode()).checked = warehouse.checked;
            } else if (warehouse.checked != null && warehouse.checked) {
                hashMapMerchantWareHouse.put(warehouse.getCode(), warehouse);
            }
        }
        recordCount.set(hashMapMerchantWareHouse.size());
        listMerchantWareHouse.setData(new ArrayList<>(hashMapMerchantWareHouse.values()));
        EventBus.getDefault().post("selectedStore");
    }


    public void updateStores() {
        List<WareHouseModel> listWareHouseModel = listMerchantWareHouse.getBaseModelsE();
        List<MerchantWareHouseModel> temp = new ArrayList<>();
        for (WareHouseModel wareHouse : listWareHouseModel) {
            if (wareHouse.checked != null && wareHouse.checked) {
                MerchantWareHouseModel model = new MerchantWareHouseModel();
                model.setStoreAddress(wareHouse.getAddress());
                model.setPostalCode(wareHouse.getPostalCode());
                model.setPhone(wareHouse.getPhone());
                model.setStoreCode(wareHouse.getCode());
                model.setStoreName(wareHouse.getName());
                temp.add(model);
            }
        }
        if (temp.size() == 0) {
            ToastUtils.showToast(R.string.select_one_store);
        } else {
            loadingVisible.set(true);
            callApi(HttpHelper.getInstance().getApi().updateStores(temp, Objects.requireNonNull(model.get()).getMerchantId())
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<BaseResponse>() {
                        @Override
                        public void onSuccess(BaseResponse merchantModel) {
                            loadingVisible.set(false);
                            ToastUtils.showToast("Success");
                        }

                        @Override
                        public void onFailure(String msg, int code) {
                            loadingVisible.set(false);
                            ToastUtils.showToast("Opp! error when update store");
                        }
                    }));
        }


    }

    private void getMerchantWareHouseSuccess(List<WareHouseModel> merchantWareHouses) {
        if (!hashMapMerchantWareHouse.isEmpty() && merchantWareHouses != null) {
            for (WareHouseModel wareHouse : merchantWareHouses) {
                WareHouseModel temp = hashMapMerchantWareHouse.get(wareHouse.getCode());
                if (temp != null && temp.checked != null && temp.checked) {
                    wareHouse.checked = true;
                }
            }
        }
    }

    public void getMoreWareHouse(RequestQueue queue, Integer distance) {
        if (totalPage > 1 && currentPage <= totalPage) {
            currentPage++;
            getWareHouses(currentPage, queue, distance);
        } else {
            EventBus.getDefault().post("noMore");
        }
    }

    public void updateContractImg(ArrayList<AlbumFile> filesPath) {
        if (isValidContract(filesPath)) {
            loadingVisible.set(true);
            MultipartBody.Part[] parts = null;
            if (filesPath != null) {
                parts = new MultipartBody.Part[filesPath.size()];
                for (int i = 0; i < filesPath.size(); i++) {
                    File file = new File(filesPath.get(i).getPath());
                    RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                    MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
                    parts[i] = part;
                }
            }
            String number = numberContract.get();
            if (parts == null) {
                callApi(HttpHelper.getInstance().getApi().updateMerchantImg(Objects.requireNonNull(model.get()).getMerchantId(), 1,
                        number, idsDelete.toArray(new Long[0]))
                        .compose(RxSchedulers.io_main())
                        .subscribeWith(new RxSubscriber<MerchantModel>() {
                            @Override
                            public void onSuccess(MerchantModel merchantModel) {
                                uploadSuccess(null);
                            }

                            @Override
                            public void onFailure(String msg, int code) {
                                Log.e("upload", "false");
                                loadingVisible.set(false);
                                ToastUtils.showToast("Opp! error when upload image");
                            }
                        }));
            } else {
                callApi(HttpHelper.getInstance().getApi().updateMerchantImg(Objects.requireNonNull(model.get()).getMerchantId(), 1,
                        number, idsDelete.toArray(new Long[0]), parts)
                        .compose(RxSchedulers.io_main())
                        .subscribeWith(new RxSubscriber<MerchantModel>() {
                            @Override
                            public void onSuccess(MerchantModel merchantModel) {
                                uploadSuccess(filesPath);
                            }

                            @Override
                            public void onFailure(String msg, int code) {
                                Log.e("upload", "false");
                                loadingVisible.set(false);
                                ToastUtils.showToast("Opp! error when upload image");
                            }
                        }));
            }
        } else {
            ToastUtils.showToast(R.string.enter_requite_field);
        }
    }

    private void uploadSuccess(ArrayList<AlbumFile> filesPath) {
        Log.e("upload", "success");
        idsDelete.clear();
        if (imageContract.getValue() != null && filesPath != null) {
            List<MerchantImages> temp = imageContract.getValue();
            temp.addAll(convertData(filesPath));
            imageContract.setValue(temp);
        }
        loadingVisible.set(false);
        EventBus.getDefault().post(3);
    }

    private boolean isValidContract(List<AlbumFile> files) {
        String temp = numberContract.get();
        boolean isValid = true;
        if (TsUtils.isNullOrEmpty(imageContract.getValue()) && TsUtils.isNullOrEmpty(files)) {
            isValid = false;
            addError("imgContract", R.string.INVALID_FIELD, true);
        } else {
            clearErro("imgContract");
        }

        if (StringUtils.isNullOrEmpty(temp)) {
            isValid = false;
            addError("contractNumber", R.string.INVALID_FIELD, true);
        } else {
            clearErro("contractNumber");
        }
        return isValid;
    }

    public void updateMerchant(ArrayList<AlbumFile> filesPath) {

        if (isValidMatrixProperties()) {
            loadingVisible.set(true);
            if (filesPath == null && idsDelete.size() == 0) {
                updateInfo();
            } else {
                MultipartBody.Part[] parts = null;
                if (filesPath != null) {
                    parts = new MultipartBody.Part[filesPath.size()];
                    for (int i = 0; i < filesPath.size(); i++) {
                        File file = new File(filesPath.get(i).getPath());
                        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                        MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
                        parts[i] = part;
                    }
                }
                if (parts == null) {
                    callApi(HttpHelper.getInstance().getApi().updateMerchantImg(Objects.requireNonNull(model.get()).getMerchantId(),
                            0,
                            null,
                            idsDelete.toArray(new Long[0]))
                            .compose(RxSchedulers.io_main())
                            .subscribeWith(new RxSubscriber<MerchantModel>() {
                                @Override
                                public void onSuccess(MerchantModel merchantModel) {
                                    updateMerchantSuccess(null);
                                }

                                @Override
                                public void onFailure(String msg, int code) {
                                    uploadFail();
                                }
                            }));
                } else {
                    callApi(HttpHelper.getInstance().getApi().updateMerchantImg(Objects.requireNonNull(model.get()).getMerchantId(),
                            0,
                            null,
                            idsDelete.toArray(new Long[0]), parts)
                            .compose(RxSchedulers.io_main())
                            .subscribeWith(new RxSubscriber<MerchantModel>() {
                                @Override
                                public void onSuccess(MerchantModel merchantModel) {
                                    updateMerchantSuccess(filesPath);
                                }

                                @Override
                                public void onFailure(String msg, int code) {
                                    uploadFail();
                                }
                            }));
                }
            }
        } else {
            ToastUtils.showToast(R.string.enter_requite_field);
        }
    }

    private void updateMerchantSuccess(ArrayList<AlbumFile> filesPath) {
        Log.e("upload", "success");
        idsDelete.clear();
        if (imageStore.getValue() != null && filesPath != null) {
            List<MerchantImages> temp = imageStore.getValue();
            temp.addAll(convertData(filesPath));
            imageStore.setValue(temp);
        }
        updateInfo();
    }

    private void uploadFail() {
        Log.e("upload", "false");
        loadingVisible.set(false);
        ToastUtils.showToast("Opp! error when upload image");
    }


    private boolean isValidMatrixProperties() {
        MerchantModel temp = model.get();
        boolean isValid = true;
        if (temp != null) {
            if (StringUtils.isNullOrEmpty(temp.province)) {
                isValid = false;
                addError("province", R.string.INVALID_FIELD, true);
            } else {
                clearErro("province");
            }

            if (StringUtils.isNullOrEmpty(temp.district)) {
                isValid = false;
                addError("district", R.string.INVALID_FIELD, true);
            } else {
                clearErro("district");
            }

            if (temp.longitude == null) {
                isValid = false;
                addError("longitude", R.string.INVALID_FIELD, true);
            } else {
                clearErro("longitude");
            }

            if (temp.lat == null) {
                isValid = false;
                addError("latitude", R.string.INVALID_FIELD, true);
            } else {
                clearErro("latitude");
            }

            if (selectedGroup.size() == 0) {
                isValid = false;
                addError("selectedGroup", R.string.INVALID_FIELD, true);
            } else {
                clearErro("selectedGroup");
            }

        }
        return isValid;
    }

    private void updateInfo() {
        Objects.requireNonNull(model.get()).setGroupValues(asList(selectedGroup));
        callApi(HttpHelper.getInstance().getApi().updateMerchant(model.get())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<MerchantModel>() {
                    @Override
                    public void onSuccess(MerchantModel merchantModel) {
                        Log.e("get", "success");
                        loadingVisible.set(false);
                        try {
                            view.action("updateSuccess", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(2);
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        Log.e("get", "false");
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when update");
                    }
                }));
    }

    public void getUserInfo(String userName) {
        loadingVisible.set(true);
        callApi(HttpHelper.getInstance().getApi().getMerchantByUserName(userName)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<MerchantModel>() {
                    @Override
                    public void onSuccess(MerchantModel merchantModel) {
                        model.set(merchantModel);
                        if (!TsUtils.isNullOrEmpty(merchantModel.getAttValues())) {
                            merchantAttr.postValue(merchantModel.getAttValues());
                        }
                        if (!TsUtils.isNullOrEmpty(merchantModel.getGroupValues())) {
                            merchantGroup.postValue(merchantModel.getGroupValues());
                        }
                        getMerchantImages(0);
                        getMerchantImages(1);
                        try {
                            view.action("getUserSuccess", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                        loadingVisible.set(false);
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when load user info");
                    }
                }));
    }

    private void getMerchantImages(int type) {
        MerchantImages merchantImages = new MerchantImages(null);
        merchantImages.setMerchantId(Objects.requireNonNull(model.get()).getMerchantId());
        merchantImages.setType(type);
        callApi(HttpHelper.getInstance().getApi().getMerchantImages(merchantImages, "MerchantImages")
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<MerchantImages>>() {
                    @Override
                    public void onSuccess(ServiceResponse<MerchantImages> response) {
                        if (response.getArrData() != null && response.getArrData().size() != 0) {

                            for (MerchantImages merchantImage : response.getArrData()) {
                                merchantImage.setExits(true).setUrl(AppController.BASE_IMAGE + merchantImage.getUrl());
                            }

                            if (type == 0) {
                                imageStore.postValue(response.getArrData());
                            } else {
                                imageContract.postValue(response.getArrData());
                                numberContract.set(response.getArrData().get(0).getContractNumber());
                            }
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        ToastUtils.showToast("Opp! error when load user info");
                    }
                }));
    }


    public List<MerchantImages> convertData(ArrayList<AlbumFile> mAlbumFiles) {
        List<MerchantImages> merchantImages = new ArrayList<>();
        for (AlbumFile albumFile : mAlbumFiles) {
            merchantImages.add(new MerchantImages(albumFile.getPath()));
        }
        return merchantImages;
    }


}
