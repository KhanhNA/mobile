package com.ts.hunter.view.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.ts.hunter.R;
import com.ts.hunter.base.AppController;
import com.ts.hunter.model.RevenueDTO;
import com.ts.hunter.utils.DayAxisValueFormatter;
import com.ts.hunter.utils.MyValueFormatter;
import com.ts.hunter.utils.XYMarkerView;
import com.ts.hunter.viewmodel.HomeVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends BaseFragment {
    private TextView txtEmpty, txtLabelBarChar;
    private BarChart barChart;
    private HomeVM homeVM;
    private Date toDate;
    private Date fromDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        homeVM = (HomeVM) viewModel;
        initView(view);


        return view;
    }

    private final RectF onValueSelectedRectF = new RectF();


    private void selectDate(View v, Date fromDate, Date toDate) {
        if (getContext() != null) {
            Calendar c = Calendar.getInstance();
            switch (v.getId()) {
                case R.id.txtFromDate:
                    c.setTime(fromDate);
                    break;
                case R.id.txtToDate:
                    c.setTime(toDate);
                    break;
            }
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year1, month1, dayOfMonth);
                String date = AppController.formatDate.format(calendar.getTime());
                switch (v.getId()) {
                    case R.id.txtFromDate:
                        ((TextView) v).setText(date);
                        //getData
                        fromDate.setTime(calendar.getTime().getTime());
                        homeVM.getDashBoard(AppController.getMerchantId(), AppController.formatDate_v2.format(fromDate.getTime()),
                                AppController.formatDate_v2.format(toDate.getTime()));
                        break;
                    case R.id.txtToDate:
                        //getData
                        toDate.setTime(calendar.getTime().getTime());
                        homeVM.getDashBoard(AppController.getMerchantId(), AppController.formatDate_v2.format(fromDate.getTime()),
                                AppController.formatDate_v2.format(toDate.getTime()));
                        ((TextView) v).setText(date);
                        break;
                }

            }, year, month, day);
            if (v.getId() == R.id.txtToDate) {
                datePickerDialog.getDatePicker().setMinDate(fromDate.getTime());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }else if(v.getId() == R.id.txtFromDate){
                datePickerDialog.getDatePicker().setMaxDate(toDate.getTime());
            }

            datePickerDialog.show();
        }

    }


    private void initView(View v) {
        // -------------------- init action ------------------
        barChart = v.findViewById(R.id.barChart);
        TextView txtFromDate = v.findViewById(R.id.txtFromDate);
        TextView txtToDate = v.findViewById(R.id.txtToDate);
        txtEmpty = v.findViewById(R.id.txtEmpty);
        txtLabelBarChar = v.findViewById(R.id.txtLabelBarChar);
        LinearLayout btnGetActives = v.findViewById(R.id.btnGetActives);
        LinearLayout btnGetRegisters = v.findViewById(R.id.btnGetRegisters);
        LinearLayout btnGetNonRevenues = v.findViewById(R.id.btnGetNonRevenues);
        LinearLayout btnGetInactive = v.findViewById(R.id.btnGetInactives);

        toDate = Calendar.getInstance().getTime();
        txtToDate.setText(AppController.formatDate.format(toDate.getTime()));
        // get start of the month
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        fromDate = cal.getTime();
        txtFromDate.setText(AppController.formatDate.format(fromDate.getTime()));

        //set listener
        txtFromDate.setOnClickListener(view -> selectDate(view, fromDate, toDate));
        txtToDate.setOnClickListener(view -> selectDate(view, fromDate, toDate));

        btnGetActives.setOnClickListener(view->openDashBoardDetail(1, 0, fromDate, toDate));
        btnGetRegisters.setOnClickListener(view->openDashBoardDetail(0, 0, fromDate, toDate));
        btnGetInactive.setOnClickListener(view->openDashBoardDetail(2, 0, fromDate, toDate));
        btnGetNonRevenues.setOnClickListener(view->openDashBoardDetail(1, 1, fromDate, toDate));


        //-------------------- init bar chart -----------------
        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e == null)
                    return;
                barChart.getBarBounds((BarEntry) e, onValueSelectedRectF);
                MPPointF position = barChart.getPosition(e, YAxis.AxisDependency.LEFT);
                MPPointF.recycleInstance(position);

            }

            @Override
            public void onNothingSelected() {

            }
        });


        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);

        barChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);

        //định dạng giá trị chiều ngang
        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        //định dạng giá trị theo chiều dọc
        ValueFormatter custom = new MyValueFormatter("");
        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)


        XYMarkerView mv = new XYMarkerView(getBaseActivity(), xAxisFormatter);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart


        //getData
        homeVM.getDashBoard(AppController.getMerchantId(), AppController.formatDate_v2.format(fromDate.getTime()),
                AppController.formatDate_v2.format(toDate.getTime()));

        homeVM.getRevenueMonth(AppController.getMerchantId(), cal.get(Calendar.YEAR), 0);

        barChart.animateXY(1500, 1500);
    }

    private void openDashBoardDetail(Integer status, Integer nonRevenue, Date fromDate, Date toDate) {
        Intent intent = new Intent(getActivity(), CommonActivity.class);
        intent.putExtra("FRAGMENT", UpdateStatusFragment.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("fromDate", fromDate);
        bundle.putSerializable("toDate", toDate);
        bundle.putSerializable("merchantStatus", status);
        bundle.putSerializable("nonRevenue", nonRevenue);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @SuppressLint("SetTextI18n")
    private void setData(HashMap<Integer, RevenueDTO> listData) {
        float start = 1f;
        ArrayList<BarEntry> values = new ArrayList<>();
        //dump data
        if (listData != null && listData.size() != 0) {
            txtEmpty.setVisibility(View.GONE);
            for (int i = (int) start; i < start + 12; i++) {
                RevenueDTO revenueDTO = listData.get(i);
                if (revenueDTO != null) {
                    values.add(new BarEntry(i, revenueDTO.getAmount().floatValue()));
                } else {
                    values.add(new BarEntry(i, 0));
                }
            }
        }else {
            barChart.setVisibility(View.GONE);
        }


        Legend l = barChart.getLegend();

        l.setEnabled(false);

        BarDataSet set1;
        if (barChart.getData() != null &&
                barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            Calendar calendar = Calendar.getInstance();

            set1 = new BarDataSet(values, getString(R.string.total_revenue_of_year) + " " + calendar.get(Calendar.YEAR));
            txtLabelBarChar.setText(getString(R.string.total_revenue_of_year) + " " + calendar.get(Calendar.YEAR));
            set1.setDrawIcons(false);

            int startColor = ContextCompat.getColor(getBaseActivity(), R.color.da_cam);
            int endColor = ContextCompat.getColor(getBaseActivity(), R.color.da_cam_dark);
            set1.setGradientColor(startColor, endColor);
            set1.setDrawValues(false);
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);
            barChart.setData(data);
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if ("getRevenueMonth".equals(action)) {
            List<RevenueDTO> listData = (List<RevenueDTO>) viewModel.baseModels.get();
            HashMap<Integer, RevenueDTO> hashData = new HashMap<>();
            if (listData != null) {
                for (RevenueDTO revenueDTO : listData) {
                    hashData.put(revenueDTO.getMonth(), revenueDTO);
                }
            }
            setData(hashData);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_sync) {
            homeVM.getDashBoard(AppController.getMerchantId(), AppController.formatDate_v2.format(fromDate.getTime()),
                    AppController.formatDate_v2.format(toDate.getTime()));
            homeVM.getRevenueMonth(AppController.getMerchantId(), Calendar.getInstance().get(Calendar.YEAR), 0);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return HomeVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
