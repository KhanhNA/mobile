package com.ts.hunter.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.ts.hunter.base.http.HttpHelper;
import com.ts.hunter.base.http.rx.RxSchedulers;
import com.ts.hunter.model.DashBoardDTO;
import com.ts.hunter.model.RevenueDTO;
import com.ts.hunter.service.network.rx.RxSubscriber;
import com.ts.hunter.utils.ToastUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HomeVM extends BaseViewModel<DashBoardDTO> {

    ObservableBoolean loadingVisible = new ObservableBoolean();
    private BaseViewModel revenueMonths;


    public HomeVM(@NonNull Application application) {
        super(application);
        revenueMonths = new BaseViewModel(application);
    }

    public void getDashBoard(Long parentMerchantId, String fromDate, String toDate){
        loadingVisible.set(true);
        callApi(HttpHelper.getInstance().getApi().getDashBoard(parentMerchantId, fromDate, toDate)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<DashBoardDTO>() {
                    @Override
                    public void onSuccess(DashBoardDTO dashBoard) {
                        model.set(dashBoard);
                        try {
                            view.action("getDashBoardSuccess", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                        loadingVisible.set(false);
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when load dashboard info");
                    }
                }));
    }


    /**
     * @param
     * type 0: revenue of all merchant with parent is this hunter
     * type 1: revenue of merchantId
     * @param year revenue month of year
     */
    public void getRevenueMonth(Long merchantId ,Integer year, int type){
        loadingVisible.set(true);
        callApi(HttpHelper.getInstance().getApi().getRevenueMonth(merchantId, year, type)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<RevenueDTO>>() {
                    @Override
                    public void onSuccess(List<RevenueDTO> response) {
                        revenueMonths.setData(response);
                        try {
                            view.action("getRevenueMonth", null, revenueMonths, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                        loadingVisible.set(false);
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        loadingVisible.set(false);
                        ToastUtils.showToast("Opp! error when load dashboard info");
                    }
                }));
    }

}
