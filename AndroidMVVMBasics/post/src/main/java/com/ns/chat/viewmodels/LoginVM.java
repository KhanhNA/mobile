package com.ns.chat.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.ns.chat.BR;
import com.ns.chat.model.User;
import com.tsolution.base.BaseViewModel;

public class LoginVM extends BaseViewModel {
    private User user;


    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";


    public String toastMessage = null;

    public LoginVM(@NonNull Application application) {
        super(application);
        user = new User("", "");
    }


    public String getToastMessage() {
        return toastMessage;
    }


    private void setToastMessage(String toastMessage) {

        this.toastMessage = toastMessage;
//        notifyPropertyChanged(com.ns.chat.BR.toastMessage);
    }

//    public LoginVM() {
//        user = new User("", "");
//    }

    public void afterEmailTextChanged(CharSequence s) {
        user.setEmail(s.toString());
    }

    public void afterPasswordTextChanged(CharSequence s) {
        user.setPassword(s.toString());
    }

    public void onLoginClicked() {
        if (user.isInputDataValid())
            setToastMessage(successMessage);
        else
            setToastMessage(errorMessage);
    }
}