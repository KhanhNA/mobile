/*
 * Copyright 2017 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ns.chat.model;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.ns.chat.R;
import com.ns.chat.application.GlideApp;
import com.ns.chat.enums.ItemType;
import com.ns.chat.listener.OnObjectChangedListener;
import com.ns.chat.utils.FormatterUtil;
import com.ns.chat.utils.ImageUtil;
import com.ns.chat.views.components.CircularImageView;
import com.ns.chat.views.manager.PostManager;
import com.ns.chat.views.manager.ProfileManager;
import com.tsolution.base.BaseModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

import static android.view.View.VISIBLE;

/**
 * Created by Kristina on 10/28/16.
 */
@Getter
@Setter
public class Post extends BaseModel implements Serializable, LazyLoading {

    private String id;
    private String title;
    private String description;
    private long createdDate;
    private String imagePath;
    private String imageTitle;
    private String authorId;
    private long commentsCount;
    private long likesCount;
    private long watchersCount;
    private boolean hasComplain;
    private ItemType itemType;
    private boolean isCurentUserLike;
    private String formatTile;
    private ArrayList<String> imagesPostFiles;

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("title", title);
        result.put("description", description);
        result.put("createdDate", createdDate);
        result.put("imagePath", imagePath);
        result.put("imageTitle", imageTitle);
        result.put("authorId", authorId);
        result.put("commentsCount", commentsCount);
        result.put("likesCount", likesCount);
        result.put("watchersCount", watchersCount);
        result.put("hasComplain", hasComplain);
        result.put("createdDateText", FormatterUtil.getFirebaseDateFormat().format(new Date(createdDate)));
        result.put("formatTile", formatTile);
        result.put("imagesPostFiles", imagesPostFiles);
        return result;
    }



    public Post() {
        this.createdDate = new Date().getTime();
        itemType = ItemType.ITEM;
    }

    public Post(ItemType itemType) {
        this.itemType = itemType;
        setId(itemType.toString());
    }


    @Override
    public ItemType getItemType() {
        return itemType;
    }

    @Override
    public void setItemType(ItemType itemType) {

    }

    @BindingAdapter("imageUrlPost")
    public static void loadImage(ImageView view, String titleImage) {
        if(titleImage != null && !titleImage.equals("")){
            PostManager postManager = new PostManager(view.getContext());
            postManager.loadImageMediumSize(GlideApp.with(view.getContext()), titleImage, view);
        }

    }

    @BindingAdapter("titleFormat")
    public static void titleFormat(TextView textView, String formatTile){
        if(formatTile != null){
            String[] format = formatTile.split("\\|");
            if(!format[0].equals("")){
                textView.setTextColor(Color.parseColor(format[0]));
            }
            if(!format[1].equals("")){
                Typeface type = Typeface.createFromAsset(textView.getContext().getAssets(),"fonts/" + format[1]);
                textView.setTypeface(type);
            }
        }else {
            textView.setTextColor(Color.parseColor("#2d2d2d"));
            textView.setTypeface(Typeface.SANS_SERIF);
        }
    }

    //CODE THEO KIỂU NÔNG DÂN
    @BindingAdapter("imagesPost")
    public static void viewImages(LinearLayout layout, ArrayList<String> attrs) {
        Context context = layout.getContext();
        PostManager postManager = new PostManager(context);
        TextView moreImage;
        ImageView[] imgs = new ImageView[4];
        FrameLayout lastImg;
        int[] imgsId = {R.id.image1, R.id.image2, R.id.image3, R.id.image4};

        final View rootView =
                LayoutInflater.from(context).inflate(R.layout.view_post_images, layout, true);

        moreImage = rootView.findViewById(R.id.moreImage);
        lastImg = rootView.findViewById(R.id.lastImg);
        //Đoạn code tay to
        if(attrs != null && attrs.size() > 0){
            if(attrs.size() > 4){
                moreImage.setText(attrs.size() - 4 + "+");
                moreImage.setVisibility(VISIBLE);
                lastImg.setVisibility(VISIBLE);
                for(int i = 0; i < 4; i++){
                    imgs[i] = rootView.findViewById(imgsId[i]);
                    imgs[i].setVisibility(VISIBLE);
                    if(attrs.get(i) != null && !attrs.get(i).equals("")){
                        postManager.loadImageMediumSize(GlideApp.with(context), attrs.get(i), imgs[i]);
                    }
                }
            }else  if(attrs.size() == 2){
                //chỗ này to khỏi nói rồi
                moreImage.setVisibility(View.GONE);
                lastImg.setVisibility(View.GONE);
                imgs[0] = rootView.findViewById(R.id.image1);
                imgs[0].setVisibility(VISIBLE);
                imgs[1] = rootView.findViewById(R.id.image2);
                imgs[1].setVisibility(VISIBLE);
                //to to to to to....
                imgs[2] = rootView.findViewById(R.id.image3);
                imgs[2].setVisibility(View.GONE);
                imgs[3] = rootView.findViewById(R.id.image4);
                imgs[3].setVisibility(View.GONE);
                if(attrs.get(0) != null && !attrs.get(0).equals("")){
                    postManager.loadImageMediumSize(GlideApp.with(context), attrs.get(0), imgs[0]);
                }
                if(attrs.get(1) != null && !attrs.get(1).equals("")){
                    postManager.loadImageMediumSize(GlideApp.with(context), attrs.get(1), imgs[1]);
                }
            }
            else{
                lastImg.setVisibility(View.GONE);
                moreImage.setVisibility(View.GONE);
                for(int i = 0; i < 4; i++){
                    if(i < attrs.size()) {
                        imgs[i] = rootView.findViewById(imgsId[i]);
                        imgs[i].setVisibility(VISIBLE);
                        if (attrs.get(i) != null && !attrs.get(i).equals("")) {
                            postManager.loadImageMediumSize(GlideApp.with(context), attrs.get(i), imgs[i]);
                        }
                    }else {
                        imgs[i] = rootView.findViewById(imgsId[i]);
                        imgs[i].setVisibility(View.GONE);
                    }
                }
            }
        }else {
            lastImg.setVisibility(View.GONE);
            moreImage.setVisibility(View.GONE);
            for(int i = 0; i < 4; i++){
                imgs[i] = rootView.findViewById(imgsId[i]);
                imgs[i].setVisibility(View.GONE);
            }
        }

    }

    @BindingAdapter("imageAuthor")
    public static void loadAuthorImg(LinearLayout view, String authorId){
        ProfileManager profileManager = ProfileManager.getInstance(view.getContext().getApplicationContext());
        profileManager.getProfileSingleValue(authorId, new OnObjectChangedListener<Profile>() {
            @Override
            public void onObjectChanged(Profile obj) {
                if (obj != null && obj.getImage() != null) {
                    CircularImageView authorImg = view.findViewById(R.id.authorImageView);
                    if(authorImg != null){
                        ImageUtil.loadImage(GlideApp.with(view.getContext()), obj.getImage(), authorImg);
                    }
                    TextView authorName = view.findViewById(R.id.authorTextView);
                    if(authorName != null) {
                        authorName.setText(obj.getName());
                    }
                }
            }

            @Override
            public void onError(String errorText) {

            }
        });
    }

    @BindingAdapter({"time"})
    public static void setDateText(TextView view, long time) {
        view.setText(FormatterUtil.getRelativeTimeSpanStringShort(view.getContext(), time));
    }

}
