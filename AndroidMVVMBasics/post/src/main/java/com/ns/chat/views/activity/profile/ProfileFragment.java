package com.ns.chat.views.activity.profile;


/*
 * Copyright 2018 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ns.chat.R;
import com.ns.chat.application.GlideApp;
import com.ns.chat.enums.FollowState;
import com.ns.chat.model.Post;
import com.ns.chat.model.Profile;
import com.ns.chat.utils.ImageUtil;
import com.ns.chat.utils.LogUtil;
import com.ns.chat.viewmodels.ProfileVM;
import com.ns.chat.views.activity.FriendPostsFragment;
import com.ns.chat.views.activity.MainActivity;
import com.ns.chat.views.activity.postDetails.PostDetailsActivityV2;
import com.ns.chat.views.adapter.BaseAdapterV2;
import com.ns.chat.views.components.FollowButton;
import com.ns.chat.views.controllers.LikeController;
import com.ns.chat.views.dialogs.UnfollowConfirmationDialog;
import com.ns.chat.views.manager.FollowManager;
import com.ns.chat.views.manager.ProfileManager;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;


public class ProfileFragment extends BaseFragment implements ProfileView {//<ProfileView, ProfilePresenter> implements ProfileView, GoogleApiClient.OnConnectionFailedListener, UnfollowConfirmationDialog.Callback {
    private static final String TAG = ProfileFragment.class.getSimpleName();
    private ProfileVM profileVM;
    public static final int CREATE_POST_FROM_PROFILE_REQUEST = 22;
    public static final String USER_ID_EXTRA_KEY = "ProfileActivity.USER_ID_EXTRA_KEY";

    // UI references.
    private TextView nameEditText;
    private ImageView imageView;

    private ProgressBar progressBar;
    private TextView postsCounterTextView;
    private ProgressBar postsProgressBar;

    private FirebaseAuth mAuth;
    private GoogleApiClient mGoogleApiClient;
    private String currentUserId;
    private String userID;

//    private PostsByUserAdapter postsAdapter;
    private SwipeRefreshLayout swipeContainer;
    private TextView likesCountersTextView;
    private TextView followersCounterTextView;
    private TextView followingsCounterTextView;
    private FollowButton followButton;
    private TextView editProfile;
    private TextView btnCreatePost;

    Toolbar toolbar;
    private static ProfileFragment instance;
    public static ProfileFragment getInstance(){
        if (instance == null) {
            instance = new ProfileFragment();
        }

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        profileVM = (ProfileVM)viewModel;

//        View view = inflater.inflate(R.layout.activity_profile, container, false);
        viewModel.setView(this);
        BaseAdapterV2 baseAdapter = new BaseAdapterV2(R.layout.post_item_list_view, viewModel, this::onItemClick, getActivity());
        recyclerView.setAdapter(baseAdapter);


        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);

        if (getArguments() != null) {
            userID = getArguments().getString(USER_ID_EXTRA_KEY);
        }else {
            userID = getActivity().getIntent().getStringExtra(ProfileFragment.USER_ID_EXTRA_KEY);
        }
        if (userID == null && getActivity().getIntent().getStringExtra(ProfileFragment.USER_ID_EXTRA_KEY) != null) {
            userID = getActivity().getIntent().getStringExtra(ProfileFragment.USER_ID_EXTRA_KEY);
        }
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            currentUserId = firebaseUser.getUid();
        }

        // Set up the login form.
        progressBar = view.findViewById(R.id.progressBar);
        imageView = view.findViewById(R.id.imageView);
        nameEditText = view.findViewById(R.id.nameEditText);
        postsCounterTextView = view.findViewById(R.id.postsCounterTextView);
        likesCountersTextView = view.findViewById(R.id.likesCountersTextView);
        followersCounterTextView = view.findViewById(R.id.followersCounterTextView);
        followingsCounterTextView = view.findViewById(R.id.followingsCounterTextView);
        postsProgressBar = view.findViewById(R.id.postsProgressBar);
        followButton = view.findViewById(R.id.followButton);
        swipeContainer = view.findViewById(R.id.swipeContainer);

//        editProfile = view.findViewById(R.id.btnEditProfile);
//        editProfile.setVisibility(View.VISIBLE);
//        btnCreatePost = view.findViewById(R.id.btnCreatePost);
//        btnCreatePost.setVisibility(View.VISIBLE);


        profileVM.checkFollowState(userID);
        profileVM.loadPosts(userID);
        getActivity().supportPostponeEnterTransition();
        initListeners();
        return view;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        profileVM.loadProfile(getActivity(), userID);
        profileVM.getFollowersCount(userID);
        profileVM.getFollowingsCount(userID);

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        FollowManager.getInstance(getActivity()).closeListeners(getActivity());
        ProfileManager.getInstance(getActivity()).closeListeners(getActivity());

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }



    private void initListeners() {
        followButton.setOnClickListener(v -> {
            profileVM.onFollowButtonClick(getActivity(), followButton.getState(), userID);
        });

        followingsCounterTextView.setOnClickListener(v -> {
            startUsersListActivity(UsersListType.FOLLOWINGS);
        });

        followersCounterTextView.setOnClickListener(v -> {
            startUsersListActivity(UsersListType.FOLLOWERS);
        });

        swipeContainer.setOnRefreshListener(this::onRefreshAction);

//        editProfile.setOnClickListener(v -> profileVM.onEditProfileClick());

//        btnCreatePost.setOnClickListener(v -> profileVM.onCreatePostClick());


    }

    private void onRefreshAction() {
        profileVM.loadPosts(userID);
    }

    private void startUsersListActivity(int usersListType) {
//        Intent intent = new Intent(getActivity(), UsersListActivity.class);
//        intent.putExtra(UsersListActivity.USER_ID_EXTRA_KEY, userID);
//        intent.putExtra(UsersListActivity.USER_LIST_TYPE, usersListType);
//        startActivity(intent);
    }

//    private void loadPostsList(View view) {
//        if (recyclerView == null) {
//
//            recyclerView = view.findViewById(R.id.recycler_view);
//            postsAdapter = new PostsByUserAdapter(getBaseActivity(), userID);
//            postsAdapter.setCallBack(new PostsByUserAdapter.CallBack() {
//                @Override
//                public void onItemClick(final Post post, final View view) {
//                    presenter.onPostClick(post, view);
//                }
//
//                @Override
//                public void onPostsListChanged(int postsCount) {
//                    presenter.onPostListChanged(postsCount);
//                }
//
//                @Override
//                public void onPostLoadingCanceled() {
//                    hideLoadingPostsProgress();
//                }
//            });
//
//            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//            ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
//            recyclerView.setAdapter(postsAdapter);
//            postsAdapter.loadPosts();
//        }
//    }

    @SuppressLint("RestrictedApi")
    @Override
    public void openPostDetailsActivity(Post post, View v) {
//        Intent intent = new Intent(getActivity(), PostDetailsActivity.class);
//        intent.putExtra(PostDetailsActivity.POST_ID_EXTRA_KEY, post.getId());
//        intent.putExtra(PostDetailsActivity.AUTHOR_ANIMATION_NEEDED_EXTRA_KEY, true);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//
//            View imageView = v.findViewById(R.id.postImageView);
//
//            ActivityOptions options = ActivityOptions.
//                    makeSceneTransitionAnimation(getActivity(),
//                            new android.util.Pair<>(imageView, getString(R.string.post_image_transition_name))
//                    );
//            startActivityForResult(intent, PostDetailsActivity.UPDATE_POST_REQUEST, options.toBundle());
//        } else {
//            startActivityForResult(intent, PostDetailsActivity.UPDATE_POST_REQUEST);
//        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        int id = v.getId();
        if (id == R.id.postImageView || id == R.id.commentsCounterContainer) {
            openPostDetail(((Post) o).getId(), ((Post) o).index - 1, v);
        } else if (id == R.id.likesContainer) {//                View viewItem = recyclerView.getLayoutManager().findViewByPosition(((Post) o).index - 1);
            ImageView likesImageView = v.findViewById(R.id.likesImageView);
            TextView countLike = v.findViewById(R.id.likeCounterTextView);
            onLikeClick(likesImageView, countLike, (Post) o);
        }
    }
    private void onLikeClick(View v, TextView likeCount, Post o) {
        LikeController likeController = new LikeController(getContext(), o, likeCount, (ImageView) v, true);
        likeController.setLiked(o.isCurentUserLike());
        likeController.handleLikeClickAction(getActivity(), o);
        likeController.setLike(o);
    }



    private void openPostDetail(String postId, int position, View view) {
        Intent intent = new Intent(getActivity(), PostDetailsActivityV2.class);
        intent.putExtra(PostDetailsActivityV2.POST_ID_EXTRA_KEY, postId);
        intent.putExtra("position", position);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            View authorImageView = recyclerView.getLayoutManager().findViewByPosition(position).findViewById(R.id.authorImageView);
            ActivityOptions options = ActivityOptions.
                    makeSceneTransitionAnimation(getActivity(),
                            new android.util.Pair<>(view, getString(R.string.post_image_transition_name)),
                            new android.util.Pair<>(authorImageView, getString(R.string.post_author_image_transition_name))
                    );
            startActivityForResult(intent, PostDetailsActivityV2.UPDATE_POST_REQUEST, options.toBundle());
        } else {
            startActivityForResult(intent, PostDetailsActivityV2.UPDATE_POST_REQUEST);
        }
    }

    private void scheduleStartPostponedTransition(final ImageView imageView) {
        imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                getActivity().supportStartPostponedEnterTransition();
                return true;
            }
        });
    }

    private void startMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void startEditProfileActivity() {
//        Intent intent = new Intent(getActivity(), EditProfileActivity.class);
//        startActivity(intent);
    }

//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//        LogUtil.logDebug(TAG, "onConnectionFailed:" + connectionResult);
//    }

    @Override
    public void openCreatePostActivity() {
//        Intent intent = new Intent(getActivity(), CreatePostActivity.class);
//        startActivityForResult(intent, CreatePostActivity.CREATE_NEW_POST_REQUEST);
    }

    @Override
    public void setProfileName(String username) {
        nameEditText.setText(username);
    }

    @Override
    public void setProfilePhoto(String photoUrl) {
        ImageUtil.loadImage(GlideApp.with(this), photoUrl, imageView, new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                scheduleStartPostponedTransition(imageView);
                progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                scheduleStartPostponedTransition(imageView);
                progressBar.setVisibility(View.GONE);
                return false;
            }
        });
    }

    @Override
    public void setDefaultProfilePhoto() {
        progressBar.setVisibility(View.GONE);
        imageView.setImageResource(R.drawable.ic_stub);
    }

    @Override
    public void updateLikesCounter(Spannable text) {
        likesCountersTextView.setText(text);
    }

    @Override
    public void hideLoadingPostsProgress() {
        swipeContainer.setRefreshing(false);
        if (postsProgressBar.getVisibility() != View.GONE) {
            postsProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showLikeCounter(boolean show) {
        likesCountersTextView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updatePostsCounter(Spannable text) {
        postsCounterTextView.setText(text);
    }

    @Override
    public void showPostCounter(boolean show) {
        postsCounterTextView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPostRemoved() {
//        postsAdapter.removeSelectedPost();
    }

    @Override
    public void onPostUpdated() {
//        postsAdapter.updateSelectedPost();
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showUnfollowConfirmation(@NonNull Profile profile) {
        UnfollowConfirmationDialog unfollowConfirmationDialog = new UnfollowConfirmationDialog();
        Bundle args = new Bundle();
        args.putSerializable(UnfollowConfirmationDialog.PROFILE, profile);
        unfollowConfirmationDialog.setArguments(args);
        unfollowConfirmationDialog.show(getActivity().getFragmentManager(), UnfollowConfirmationDialog.TAG);
    }

    @Override
    public void updateFollowButtonState(FollowState followState) {
        followButton.setState(followState);
    }

    @Override
    public void updateFollowersCount(int count) {
        followersCounterTextView.setVisibility(View.VISIBLE);
        String followersLabel = getResources().getQuantityString(R.plurals.followers_counter_format, count, count);
        followersCounterTextView.setText(profileVM.buildCounterSpannable(followersLabel, count));
    }

    @Override
    public void updateFollowingsCount(int count) {
        followingsCounterTextView.setVisibility(View.VISIBLE);
        String followingsLabel = getResources().getQuantityString(R.plurals.followings_counter_format, count, count);
        followingsCounterTextView.setText(profileVM.buildCounterSpannable(followingsLabel, count));
    }

    @Override
    public void setFollowStateChangeResultOk() {
//        getActivity().setResult(UsersListActivity.UPDATE_FOLLOWING_STATE_RESULT_OK);
    }

//    @Override
//    public void onUnfollowButtonClicked() {
//        profileVM.unfollowUser(userID);
//    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_profile;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ProfileVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.recycler_view;
    }

    @Override
    public void process(String s, View view, BaseViewModel baseViewModel, Throwable throwable) {

    }
}