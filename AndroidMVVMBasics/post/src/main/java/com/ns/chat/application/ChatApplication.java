package com.ns.chat.application;

import android.app.Application;
import android.content.Context;


import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.ns.chat.interactors.PostInteractor;
import com.ns.chat.utils.MediaLoader;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;

import java.util.Locale;

//import com.squareup.picasso.OkHttpDownloader;

/**
 * Created by AkshayeJH on 01/07/17.
 */

public class ChatApplication extends Application {
    private Application mApplicaton;
    private DatabaseReference mUserDatabase;
    private static FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener authListener;
    private static boolean connected = false;


    public ChatApplication(){
        mApplicaton = this;
    }

    public ChatApplication(Application mApplicaton){
        this.mApplicaton = mApplicaton;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        ApplicationHelper.initDatabaseHelper(mApplicaton);
        PostInteractor.getInstance(mApplicaton).subscribeToNewPosts();

        /* Glide */
        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .setLocale(Locale.getDefault())
                .build()
        );

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            connected = true;
            mUserDatabase = ChatApplication.getReference().child(DatabaseHelper.PROFILES_DB_KEY).child(mAuth.getCurrentUser().getUid());

            mUserDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot != null) {

                        mUserDatabase.child("online").onDisconnect().setValue(ServerValue.TIMESTAMP);

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }

    public static DatabaseReference getReference() {
        return FirebaseDatabase.getInstance().getReference().child("lapit");
    }

    public static String getCurrentUserId() {
        return FirebaseAuth.getInstance().getUid();
    }

    public static void signIn(Application application, OnCompleteListener<AuthResult> result) {
        if (getCurrentUserId() != null) {
            connected = true;
            ApplicationHelper.initDatabaseHelper(application);
            return;
        }
        mAuth = FirebaseAuth.getInstance();
        String error;

        int i = 0;


            if (i == 0) {
                ApplicationHelper.initDatabaseHelper(application);
                mAuth.signInWithEmailAndPassword("thonv2@gmail.com", "123456")
                        .addOnCompleteListener(task -> {
                            ChatApplication.connected = true;
                            System.out.println("connectedxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                            if (result != null) {

                                result.onComplete(task);
                            }
                        });
            }

//        while (!ChatApplication.connected) {
//
//            try {
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        // Actions to do after 10 seconds
//                    }
//                }, 1000);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

    }
}
