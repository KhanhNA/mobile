/*
 * Copyright 2018 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ns.chat.viewmodels;

import android.app.Application;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.ns.chat.R;
import com.ns.chat.listener.OnDataChangedListenerV2;
import com.ns.chat.listener.OnObjectChangedListenerSimple;
import com.ns.chat.listener.OnPostChangedListener;
import com.ns.chat.listener.OnPostListChangedListener;
import com.ns.chat.model.FollowingPost;
import com.ns.chat.model.Post;
import com.ns.chat.model.PostListResult;
import com.ns.chat.model.Profile;
import com.ns.chat.utils.PreferencesUtil;
import com.ns.chat.views.controllers.LikeController;
import com.ns.chat.views.manager.PostManager;

import com.ns.chat.views.manager.ProfileManager;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;


/**
 * Created by Alexey on 03.05.18.
 */
@Getter
@Setter
public class FriendPostsVM extends BaseVM {

    private boolean isMoreDataAvailable = true;
    private long lastLoadedItemCreatedDate;

    private PostManager postManager;
    public ObservableField<Profile> mProfile = new ObservableField<>();
    public ProfileManager profileManager;
    public ObservableField<Boolean> isLoading = new ObservableField<>();

    public FriendPostsVM(@NonNull Application application) {
        super(application);
        profileManager = ProfileManager.getInstance(context.getApplicationContext());
        postManager = PostManager.getInstance(application.getApplicationContext());
        profileManager = ProfileManager.getInstance(application.getApplicationContext());
    }

    public void incrementWatchersCount(String postId) {
        postManager.incrementWatchersCount(postId);
    }

    public void getProfile(Context context){
        profileManager.getProfileValue(context, getCurrentUserId(), new OnObjectChangedListenerSimple<Profile>() {
            @Override
            public void onObjectChanged(Profile obj) {
                mProfile.set(obj);
            }
        });
    }



    public void loadPosts(long date) {
        if (checkInternetConnection()) {
            isLoading.set(true);
            postManager.getPostsList(new OnPostListChangedListener<Post>() {
                @Override
                public void onListChanged(PostListResult result) {
                    lastLoadedItemCreatedDate = result.getLastItemCreatedDate();
                    isMoreDataAvailable = result.isMoreDataAvailable();
                    List<Post> list = result.getPosts();
                    if (date == 0) {
                        isLoading.set(false);
                    }
                    if (!list.isEmpty()) {
                        setData(list, false);
                        try {
                            view.action("addPost", null , null, null);
                        } catch (AppException e) {e.printStackTrace();

                        }
                        if (!PreferencesUtil.isPostWasLoadedAtLeastOnce(context)) {
                            PreferencesUtil.setPostWasLoadedAtLeastOnce(context, true);
                        }
                        isLoading.set(false);
                    } else {
                        isLoading.set(false);
                    }
                }

                @Override
                public void onCanceled(String message) {
                    isLoading.set(false);
                }
            }, date);
        } else {
            isLoading.set(false);
        }
    }

    public void postChange(Post post){
        postManager.getSinglePostValue(post.getId(), new OnPostChangedListener() {
            @Override
            public void onObjectChanged(Post obj) {
                getBaseModelsE().set(post.index, obj);
            }
            @Override
            public void onError(String errorText) {

            }
        });
    }




    public void onRefresh() {
        setData(new ArrayList<>());
        loadPosts(0);
    }


    public PostManager getPostManager() {
        return postManager;
    }
}
