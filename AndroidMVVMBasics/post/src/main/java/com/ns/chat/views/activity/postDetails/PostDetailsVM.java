/*
 * Copyright 2018 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ns.chat.views.activity.postDetails;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.ObservableField;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ns.chat.R;
import com.ns.chat.listener.OnObjectChangedListenerSimple;
import com.ns.chat.listener.OnPostChangedListener;
import com.ns.chat.model.Post;
import com.ns.chat.model.Profile;
import com.ns.chat.utils.FormatterUtil;
import com.ns.chat.viewmodels.BaseVM;
import com.ns.chat.views.manager.CommentManager;
import com.ns.chat.views.manager.PostManager;
import com.ns.chat.views.manager.ProfileManager;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;


/**
 * Created by Alexey on 03.05.18.
 */

public class PostDetailsVM extends BaseVM {// <PostDetailsView> {
    private static final int TIME_OUT_LOADING_COMMENTS = 30000;

    private PostManager postManager;
    private ProfileManager profileManager;
    private CommentManager commentManager;
    public ObservableField<Post> post = new ObservableField<>();
    public ObservableField<Profile> mProfile = new ObservableField<>();
//    private String commentText = "";
    public ObservableField<String> commentText = new ObservableField<>();
    public ObservableField<String> commentsCountText = new ObservableField<>();


    private boolean isPostExist;
    private boolean postRemovingProcess = false;

    private boolean attemptToLoadComments = false;

    public PostDetailsVM(@NonNull Application application) {
        super(application);
        context = application.getApplicationContext();
        postManager = PostManager.getInstance(context.getApplicationContext());
        profileManager = ProfileManager.getInstance(context.getApplicationContext());
        commentManager = CommentManager.getInstance(context.getApplicationContext());
    }


//    PostDetailsVM(Activity activity) {
//        super(activity);
//
//        postManager = PostManager.getInstance(context.getApplicationContext());
//        profileManager = ProfileManager.getInstance(context.getApplicationContext());
//        commentManager = CommentManager.getInstance(context.getApplicationContext());
//    }

    public void loadPost(String postId, Integer position) {
        postManager.getPost(context, postId, new OnPostChangedListener() {
            @Override
            public void onObjectChanged(Post obj) {
//                ifViewAttached(view -> {
                    if (obj != null) {
                        obj.index = position;
                        post.set(obj);
                        isPostExist = true;
                        ((PostDetailsView)view).initLikeController(post.get());
                        fillInUI(post.get());
                        initLikeButtonState();
//                        updateOptionMenuVisibility();
                    } else if (!postRemovingProcess) {
                        isPostExist = false;
                        ((PostDetailsView)view).onPostRemoved();
//                        ((PostDetailsView)view).showNotCancelableWarningDialog(context.getString(R.string.error_post_was_removed));
                    }
//                });
            }

            @Override
            public void onError(String errorText) {
//                ifViewAttached(view -> {
//                    view.showNotCancelableWarningDialog(errorText);
//                });
            }
        });
    }

    private void fillInUI(@NonNull Post post) {
//        ifViewAttached(view -> {

        ((PostDetailsView)view).loadPostDetailImage(post.getImagesPostFiles());
            loadAuthorProfile();
//        });
    }

    public void initLikeButtonState() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null && post.get() != null) {
            postManager.hasCurrentUserLike(context, post.get().getId(), firebaseUser.getUid(), exist -> {
                post.get().setCurentUserLike(exist);
                post.notifyChange();
            });
        }
    }

    private void loadAuthorProfile() {
        if (post.get() != null && post.get().getAuthorId() != null) {
            profileManager.getProfileSingleValue(post.get().getAuthorId(), new OnObjectChangedListenerSimple<Profile>() {
                @Override
                public void onObjectChanged(Profile profile) {
                        mProfile.set(profile);
                }
            });
        }
    }

    public void onAuthorClick(View authorView) {
        if (post != null) {
            ((PostDetailsView)view).openProfileActivity(post.get().getAuthorId(), authorView);
        }
    }


    public void onSendButtonClick(ArrayList<Uri> uris) {
        if (checkInternetConnection() && checkAuthorization()) {
            sendComment(uris);
        }
    }

    public boolean hasAccessToModifyPost() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return currentUser != null && post != null && post.get().getAuthorId().equals(currentUser.getUid());
    }

    public boolean hasAccessToEditComment(String commentAuthorId) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return currentUser != null && commentAuthorId.equals(currentUser.getUid());
    }

    public void updateComment(String newText, String commentId) {
        if (post != null) {
            commentManager.updateComment(commentId, newText, post.get().getId(), success -> {
//                ifViewAttached(view -> {
//                    view.hideProgress();
//                ((PostDetailsView)view).showSnackBar(R.string.message_comment_was_edited);
//                });
            });
        }
    }

    private void openComplainDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.add_complain)
                .setMessage(R.string.complain_text)
                .setNegativeButton(R.string.button_title_cancel, null)
                .setPositiveButton(R.string.add_complain, (dialogInterface, i) -> addComplain());

        builder.create().show();
    }

    private void addComplain() {
        postManager.addComplain(post.get());
//        ifViewAttached(view -> {
        ((PostDetailsView)view).showComplainMenuAction(false);
//            view.showSnackBar(R.string.complain_sent);
//        });
    }

    public void doComplainAction() {
        if (checkAuthorization()) {
            openComplainDialog();
        }
    }

    public void attemptToRemovePost() {
        if (hasAccessToModifyPost() && checkInternetConnection()) {
            if (!postRemovingProcess) {
                openConfirmDeletingDialog();
            }
        }
    }

    private void openConfirmDeletingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.confirm_deletion_post)
                .setNegativeButton(R.string.button_title_cancel, null)
                .setPositiveButton(R.string.button_ok, (dialogInterface, i) -> removePost());

        builder.create().show();
    }

    private void removePost() {
        postRemovingProcess = true;
//        ifViewAttached(view -> view.showProgress(R.string.removing));
        postManager.removePost(post.get(), success -> {//ifViewAttached(view -> {
            if (success) {
                ((PostDetailsView)view).onPostRemoved();
//                ((PostDetailsView)view).finish();
            } else {
                postRemovingProcess = false;
//                view.showSnackBar(R.string.error_fail_remove_post);
            }

//            view.hideProgress();
        });


    }

    private void sendComment(ArrayList<Uri> uris) {
        if (post == null) {
            return;
        }

//        ifViewAttached(view -> {
            String comment = commentText.get();

            if (comment.length() > 0 && isPostExist) {
                createOrUpdateComment(uris, comment);
//                ((PostDetailsView)view).clearCommentField();
                commentText.set("");
            }
//        });
    }

    private void createOrUpdateComment(ArrayList<Uri> uris,String commentText) {
        commentManager.createOrUpdateComment(uris, commentText, post.get().getId(), success -> {
//            ifViewAttached(view -> {
                if (success) {
                    if (post != null && post.get().getCommentsCount() > 0) {
                        ((PostDetailsView)view).scrollToFirstComment();
                    }
                }
//            });
        });
    }

    public void removeComment(String commentId) {
//        ifViewAttached(BaseView::showProgress);
        commentManager.removeComment(commentId, post.get().getId(), success -> {
//            ifViewAttached(view -> {
//                view.hideProgress();
//                view.showSnackBar(R.string.message_comment_was_removed);
//            });
        });
    }

    public void editPostAction() {
        if (hasAccessToModifyPost() && checkInternetConnection()) {
//            ifViewAttached(view ->
            ((PostDetailsView)view).openEditPostActivity(post.get());
        }
    }

    public void updateOptionMenuVisibility() {
//        ifViewAttached(view -> {
            if (post != null) {
                ((PostDetailsView)view).showEditMenuAction(hasAccessToModifyPost());
                ((PostDetailsView)view).showDeleteMenuAction(hasAccessToModifyPost());
                ((PostDetailsView)view).showComplainMenuAction(!post.get().isHasComplain());
            }
//        });
    }

    public boolean isPostExist() {
        return isPostExist;
    }


    public void getCommentsList(Context activityContext, String postId) {
        attemptToLoadComments = true;
        runHidingCommentProgressByTimeOut();

        commentManager.getCommentsList(activityContext, postId, list -> {
            attemptToLoadComments = false;
//            ifViewAttached(view -> {
            setData(list);
            ((PostDetailsView)view).onCommentsListChanged(list);
            ((PostDetailsView)view).showCommentProgress(false);
            ((PostDetailsView)view).showCommentsRecyclerView(true);
            ((PostDetailsView)view).showCommentsWarning(false);
//            });
        });
    }

    private void runHidingCommentProgressByTimeOut() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (attemptToLoadComments) {
//                ifViewAttached(view -> {
                ((PostDetailsView)view).showCommentProgress(false);
                ((PostDetailsView)view).showCommentsWarning(true);
//                });
            }
        }, TIME_OUT_LOADING_COMMENTS);
    }

    public void updateCommentsVisibility(long commentsCount) {
//        ifViewAttached(view -> {
            if (commentsCount == 0) {
                ((PostDetailsView)view).showCommentsLabel(false);
                ((PostDetailsView)view).showCommentProgress(false);
            } else {
                ((PostDetailsView)view).showCommentsLabel(true);
            }
//        });
    }

    public ObservableField<String> getCommentText() {
        return commentText;
    }

    public void setCommentText(ObservableField<String> commentText) {
        this.commentText = commentText;
    }
}
