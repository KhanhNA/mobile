/*
 * Copyright 2018 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ns.chat.views.activity;

import android.app.ActivityOptions;
import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ns.chat.R;
import com.ns.chat.application.ChatApplication;
import com.ns.chat.model.Post;
import com.ns.chat.viewmodels.FriendPostsVM;
import com.ns.chat.views.activity.postDetails.PostDetailsActivityV2;
import com.ns.chat.views.activity.profile.ProfileFragment;
import com.ns.chat.views.adapter.BaseAdapterV2;
import com.ns.chat.views.controllers.LikeController;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.ViewActionsListener;

//import com.ns.chat.views.adapter.FollowPostsAdapter;

public class FriendPostsFragment extends BaseFragment implements ViewActionsListener {
    private static final int CREATE_POST_FROM_PROFILE_REQUEST = 22; //BaseFragment<FriendPostsView, FriendPostsPresenter> implements FriendPostsView {

    private static FriendPostsFragment instance;
//    private FollowPostsAdapter postsAdapter;
//    private RecyclerView recyclerView;

    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeContainer;
    private TextView message_following_posts_empty;
    private TextView createPost;
    private ImageView authorImageView;
    FriendPostsVM friendPostsVM;


    BaseAdapterV2 baseAdapter;

    public static FriendPostsFragment getInstance() {
        if (instance == null) {
            instance = new FriendPostsFragment();
        }

        return instance;
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }

    @Override
    public void init(@Nullable Bundle savedInstanceState, @LayoutRes int layoutId,
                     Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Throwable {
        viewModel = clazz.getDeclaredConstructor(Application.class).newInstance(getActivity().getApplication());//ViewModelProviders.of(getActivity()).get(clazz);
        View view = binding.getRoot();
        binding.setVariable(com.ns.chat.BR.viewModel, viewModel);
        binding.setVariable(com.ns.chat.BR.listener, this);
        viewModel.setView(this::processFromVM);
        if (recyclerViewId != 0) {
            recyclerView = view.findViewById(recyclerViewId);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String userUID = getArguments().getString(ProfileFragment.USER_ID_EXTRA_KEY);
        ChatApplication.signIn(this.getActivity().getApplication(), null);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        friendPostsVM = (FriendPostsVM) viewModel;
        friendPostsVM.getProfile(getContext());
        baseAdapter = new BaseAdapterV2(R.layout.post_item_list_view, viewModel, this::onItemClick, getActivity());
        recyclerView.setAdapter(baseAdapter);
//        View view = inflater.inflate(R.layout.fragment_friend_posts, container, false);

//        setContentView(R.layout.activity_follow_posts);
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        actionBar = getSupportActionBar();
//
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//        }

        initContentView(view, userUID);
//        postsAdapter.setList(new ArrayList<>());
        friendPostsVM.loadPosts(friendPostsVM.getLastLoadedItemCreatedDate());
        return view;
    }


    @Override
    public void onItemClick(View v, Object o) {
        int id = v.getId();
        if (id == R.id.postImageView || id == R.id.commentsCounterContainer) {
            openPostDetail(((Post) o).getId(), ((Post) o).index - 1, v);
        } else if (id == R.id.likesContainer) {//                View viewItem = recyclerView.getLayoutManager().findViewByPosition(((Post) o).index - 1);
            ImageView likesImageView = v.findViewById(R.id.likesImageView);
            TextView countLike = v.findViewById(R.id.likeCounterTextView);
            onLikeClick(likesImageView, countLike, (Post) o);
        } else if (id == R.id.authorImageView) {
            openAuthorProfile(((Post) o).getAuthorId(), v);
        }
    }

    private void openAuthorProfile(String userId, View view) {
        Intent intent = new Intent(getActivity(), CommonActivity.class);
        intent.putExtra("FRAGMENT", ProfileFragment.class);
        intent.putExtra(ProfileFragment.USER_ID_EXTRA_KEY, userId);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.
                    makeSceneTransitionAnimation(getActivity(),
                            new android.util.Pair<>(view, getString(R.string.post_author_image_transition_name)));
            startActivityForResult(intent, FriendPostsFragment.CREATE_POST_FROM_PROFILE_REQUEST, options.toBundle());
        } else {
            startActivityForResult(intent, FriendPostsFragment.CREATE_POST_FROM_PROFILE_REQUEST);
        }
    }

    private void onLikeClick(View v, TextView likeCount, Post o) {
        LikeController likeController = new LikeController(getContext(), o, likeCount, (ImageView) v, true);
        likeController.setLiked(o.isCurentUserLike());
        likeController.handleLikeClickAction(getActivity(), o);
        likeController.setLike(o);
    }


    private void openPostDetail(String postId, int position, View view) {
        Intent intent = new Intent(getActivity(), PostDetailsActivityV2.class);
        intent.putExtra(PostDetailsActivityV2.POST_ID_EXTRA_KEY, postId);

        intent.putExtra("position", position);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            View authorImageView = recyclerView.getLayoutManager().findViewByPosition(position).findViewById(R.id.authorImageView);
            ActivityOptions options = ActivityOptions.
                    makeSceneTransitionAnimation(getActivity()
//                            , new android.util.Pair<>(view, getString(R.string.post_image_transition_name)),
//                            new android.util.Pair<>(authorImageView, getString(R.string.post_author_image_transition_name))
                    );
            startActivityForResult(intent, PostDetailsActivityV2.UPDATE_POST_REQUEST);

            //, options.toBundle()


        } else {
            startActivityForResult(intent, PostDetailsActivityV2.UPDATE_POST_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(resultCode == PostDetailsActivityV2.UPDATE_POST_REQUEST){
//            Post postIntent = (Post) data.getSerializableExtra("postSelected");
//            if(postIntent != null) {
//                Log.e("xxx",postIntent.isCurentUserLike() + "");
////                friendPostsVM.postChange(postIntent);
////                viewModel.getModels().notifyChange();
//            }
//        }
    }


//    @Override
//    public void onFollowingPostsLoaded(List<FollowingPost> list) {
//
//
//    }

    private void initContentView(View view, String userUID) {
//        if (recyclerView == null) {
        progressBar = view.findViewById(R.id.progressBar);

        createPost = view.findViewById(R.id.createPost);
        createPost.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CreatePostActivity.class);
            startActivityForResult(intent, CreatePostActivity.CREATE_NEW_POST_REQUEST);
        });

        authorImageView = view.findViewById(R.id.authorImageView);
        if (userUID != null) {
            authorImageView.setOnClickListener(v -> {
                openAuthorProfile(userUID, authorImageView);
            });
        }


        message_following_posts_empty = view.findViewById(R.id.message_following_posts_empty);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(() -> friendPostsVM.onRefresh());
        initPostListRecyclerView(view);
//        }
    }

    private void initPostListRecyclerView(View view) {
//        recyclerView = view.findViewById(R.id.recyclerView);
//        postsAdapter = new FollowPostsAdapter((BaseActivity) getActivity());
//        postsAdapter.setCallBack(new FollowPostsAdapter.CallBack() {
//            @Override
//            public void onItemClick(Post followingPost, View view) {
//                friendPostsVM.onPostClicked(followingPost.getId(), view);
//            }
//
//            @Override
//            public void onAuthorClick(int position, View view) {
//                String postId = postsAdapter.getItemByPosition(position).getId();
//                friendPostsVM.onAuthorClick(postId, view);
//            }
//        });
//        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
//        recyclerView.setAdapter(postsAdapter);
    }

//    @SuppressLint("RestrictedApi")
//    @Override
//    public void openPostDetailsActivity(String postId, View v) {
//        Intent intent = new Intent((BaseActivity) getActivity(), PostDetailsActivityV2.class);
//        intent.putExtra(PostDetailsActivity.POST_ID_EXTRA_KEY, postId);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//
//            View imageView = v.findViewById(R.id.postImageView);
//            View authorImageView = v.findViewById(R.id.authorImageView);
//
//            ActivityOptions options = ActivityOptions.
//                    makeSceneTransitionAnimation((BaseActivity) getActivity(),
//                            new android.util.Pair<>(imageView, getString(R.string.post_image_transition_name)),
//                            new android.util.Pair<>(authorImageView, getString(R.string.post_author_image_transition_name))
//                    );
//            startActivityForResult(intent, PostDetailsActivity.UPDATE_POST_REQUEST, options.toBundle());
//        } else {
//            startActivityForResult(intent, PostDetailsActivity.UPDATE_POST_REQUEST);
//        }
//    }

//    @SuppressLint("RestrictedApi")
//    @Override
//    public void openProfileActivity(String userId, View view) {
//        Intent intent = new Intent((BaseActivity) getActivity(), ProfileActivity.class);
//        intent.putExtra(ProfileActivity.USER_ID_EXTRA_KEY, userId);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && view != null) {
//
//            View authorImageView = view.findViewById(R.id.authorImageView);
//
//            ActivityOptions options = ActivityOptions.
//                    makeSceneTransitionAnimation((BaseActivity) getActivity(),
//                            new android.util.Pair<>(authorImageView, getString(R.string.post_author_image_transition_name)));
//            startActivityForResult(intent, ProfileActivity.CREATE_POST_FROM_PROFILE_REQUEST, options.toBundle());
//        } else {
//            startActivityForResult(intent, ProfileActivity.CREATE_POST_FROM_PROFILE_REQUEST);
//        }
//    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_friend_posts;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return FriendPostsVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.recyclerView;
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
//            case "addPost":
//                List<Post> list = (List<Post>) friendPostsVM.baseModels.get();
//                //loop for check post is liked by current user
//                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//                if (firebaseUser != null) {
//                    for (int i = 0 ; i < list.size(); i++) {
//                        Post temp = list.get(i);
//                        temp.index = i + 1;
//                        friendPostsVM.getPostManager().hasCurrentUserLike(getActivity(), temp.getId(), firebaseUser.getUid(), new OnObjectExistListener<Like>() {
//                            @Override
//                            public void onDataChanged(boolean exist) {
//                                temp.setCurentUserLike(exist);
//                                recyclerView.getAdapter().notifyItemChanged(temp.index - 1);
//                            }
//                        });
//                    }
//
//                }
//                recyclerView.getAdapter().notifyDataSetChanged();
//                break;
            case "addPost":
                recyclerView.getAdapter().notifyDataSetChanged();
                break;
        }
    }

}
