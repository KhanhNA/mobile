package com.ns.chat.views.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ns.chat.R;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.holder.GenericViewHolder;
import com.tsolution.base.listener.AdapterActionsListener;
import com.tsolution.base.listener.ILoadMore;
import com.tsolution.base.listener.OnBottomReachedListener;
import com.tsolution.base.listener.OwnerView;

import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BaseAdapterV2 extends RecyclerView.Adapter<GenericViewHolder> implements AdapterActionsListener {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    final int visibleThreshold = 5;
    BaseViewModel viewModel;
    OwnerView listener;
    private int layoutId;
    private Activity activity;
    private ObservableField<List> datas;
    private OnBottomReachedListener onBottomReachedListener;
    private Integer lastItemRefresh = 0;

    public BaseAdapterV2(@LayoutRes int loId, BaseViewModel vm, Activity a) {
        this.layoutId = loId;
        this.viewModel = vm;
        this.activity = a;
        this.datas = this.viewModel.baseModels;
    }

    public BaseAdapterV2(@LayoutRes int loId, ObservableField data, Activity a) {
        this.layoutId = loId;
        this.datas = data;
        this.activity = a;
        this.viewModel = new BaseViewModel(a.getApplication());
        this.viewModel.baseModels = data;
    }

    public BaseAdapterV2(@LayoutRes int layoutId, BaseViewModel viewModel, OwnerView listener, Activity a) {
        this.viewModel = viewModel;
        this.listener = listener;
        this.layoutId = layoutId;
        this.activity = a;
        this.datas = viewModel.baseModels;
    }

    public BaseAdapterV2(@LayoutRes int layoutId, ObservableField data, OwnerView listener, Activity a) {
        this.viewModel = this.viewModel;
        this.listener = listener;
        this.layoutId = layoutId;
        this.activity = a;
        this.datas = data;
        this.viewModel = new BaseViewModel(a.getApplication());
        this.viewModel.baseModels = data;
    }

    public void replaceData(List<BaseModel> tasks) {
        Integer pos = this.viewModel.getLastPos();
        List<BaseModel> lst = (List)this.viewModel.baseModels.get();
        if (lst != null && lst.size() > pos && lst.get(pos) == null) {
            lst.remove(pos);
        }

        switch(this.viewModel.getUpdateAdappType()) {
            case 0:
                this.lastItemRefresh = this.viewModel.getTotalCount() - 1;
                this.notifyDataSetChanged();
                break;
            case 1:
                if (this.lastItemRefresh == 0) {
                    if (lst != null) {
                        this.lastItemRefresh = lst.size() - 1;
                    }

                    this.notifyDataSetChanged();
                } else {
                    Integer count = this.viewModel.getFetchSize();
                    if (count + this.lastItemRefresh >= lst.size()) {
                        count = lst.size() - this.lastItemRefresh - 3;
                    }

                    this.notifyItemRangeInserted(this.lastItemRefresh + 2, count);
                    this.notifyItemChanged(this.lastItemRefresh + 1);
                    this.lastItemRefresh = lst.size() - 1;
                }
        }

        this.viewModel.setLoadingInfo(false);
    }

    @NonNull
    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = LayoutInflater.from(this.activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        } else {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), this.layoutId, parent, false);
            return new GenericViewHolder(binding);
        }
    }

    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
        if (holder instanceof BaseAdapterV2.LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        } else if (this.datas.get() != null && this.datas.get().size() != 0 && position < this.datas.get().size()) {
            BaseModel bm = (BaseModel) this.datas.get().get(position);
            bm.index = position + 1;
            holder.setBinding(bm, this.viewModel, this);
        }
    }

    public void registerScroll(RecyclerView recyclerView, final ILoadMore loadMore) {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                try {
                    super.onScrolled(recyclerView, dx, dy);
                    Integer totalItemCount = linearLayoutManager.getItemCount();
                    Integer lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    Boolean isLoading = BaseAdapterV2.this.viewModel.getLoadingInfo();
                    if (!isLoading && totalItemCount <= lastVisibleItem + 5) {
                        BaseAdapterV2.this.loadData(loadMore);
                    }
                } catch (Throwable var7) {
                    var7.printStackTrace();
                }

            }
        });
    }

    public void registerScroll(RecyclerView recyclerView, NestedScrollView scroll, final ILoadMore loadMore) {
        final LinearLayoutManager mLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
        scroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null && scrollY >= v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight() && scrollY > oldScrollY) {
                    Integer visibleItemCount = mLayoutManager.getChildCount();
                    Integer totalItemCount = mLayoutManager.getItemCount();
                    Integer pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!BaseAdapterV2.this.viewModel.getLoadingInfo() && visibleItemCount + pastVisiblesItems >= totalItemCount) {
                        BaseAdapterV2.this.loadData(loadMore);
                    }
                }

            }
        });
    }

    private void loadData(ILoadMore loadMore) {
        this.viewModel.setLoadingInfo(true);
        Integer fetchSize = this.viewModel.getFetchSize();
        List lstData = (List)this.viewModel.baseModels.get();
        if (lstData != null && lstData.size() > 0 && this.lastItemRefresh + fetchSize < lstData.size()) {
            this.viewModel.add1Fetch();
            this.replaceData(null);
        } else {
            int pos = this.datas.get().size();
            this.datas.get().add(null);
            this.viewModel.setLastPos(pos);
            if (loadMore != null) {
                loadMore.onLoadMore();
            }

        }
    }

    public int getItemCount() {
        return this.datas.get() == null ? 0 : ((List)this.datas.get()).size();
    }

    public void adapterAction(View view, BaseModel baseModel) {
        if (this.listener != null) {
            this.listener.onClicked(view, baseModel);
        }

    }

    public final void onAdapterClicked(View view, BaseModel bm) {
        AdapterActionsListener.super.onAdapterClicked(view, bm);
    }

    public int getItemViewType(int position) {
        if (this.datas.get() != null && this.datas.get().size() != 0 && position < ((List)this.datas.get()).size()) {
            return this.datas.get().get(position) == null ? 1 : 0;
        } else {
            return 0;
        }
    }

    public long getItemId(int position) {
        return (long)position;
    }

    public Activity getActivity() {
        return this.activity;
    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    public int getLayoutRes() {
        return this.layoutId;
    }

    public Class<? extends BaseViewModel> getVMClass() {
        return this.viewModel.getClass();
    }

    public int getRecycleResId() {
        return 0;
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }

    public BaseViewModel getVM() {
        return this.viewModel;
    }

    class LoadingViewHolder extends GenericViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            this.progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}