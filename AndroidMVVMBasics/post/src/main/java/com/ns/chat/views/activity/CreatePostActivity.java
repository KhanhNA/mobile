package com.ns.chat.views.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.snackbar.Snackbar;
import com.ns.chat.R;
import com.ns.chat.application.GlideApp;
import com.ns.chat.utils.ImageUtil;
import com.ns.chat.utils.ValidationUtil;
import com.ns.chat.viewmodels.BaseVM;
import com.ns.chat.viewmodels.CreatePostVM;
import com.ns.chat.views.adapter.FontsAdapter;
import com.ns.chat.views.adapter.ImagesAdapter;
import com.ns.chat.views.components.ColorPaletteView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.yanzhenjie.album.Action;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.api.widget.Widget;
import com.yanzhenjie.album.impl.OnItemClickListener;
import com.yanzhenjie.album.widget.divider.Api21ItemDivider;
import com.yanzhenjie.album.widget.divider.Divider;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreatePostActivity extends BaseActivity implements View.OnClickListener {
    public static final int CREATE_NEW_POST_REQUEST = 11;
    protected static final int MAX_FILE_SIZE_IN_BYTES = 10485760;   //10 Mb

    ImageView imageView;
    Uri imageUri;
    ProgressBar progressBar;
    private Toolbar mToolbar;
    CreatePostVM createPostVM;

    ImageView pickerImage;
    ImageView pickerColor;
    RecyclerView rcImages;
    RecyclerView rcFonts;
    ImagesAdapter imgAdapter;
    ColorPaletteView cpvFontTextColor;

    //list img
    private ArrayList<AlbumFile> mAlbumFiles;
    //list fonts
    private List<String> fonts;
    String titleColor = "#2d2d2d";
    final String defaultColor = "#2d2d2d";
    ScrollView scrollView;
    EditText titleEditText;

    FontsAdapter fontsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        createPostVM = (CreatePostVM) viewModel;
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView();

    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        scrollView = findViewById(R.id.scrollView);

        titleEditText = findViewById(R.id.titleEditText);
        titleEditText.requestFocus();

        cpvFontTextColor = findViewById(R.id.cpv_font_text_color);
        cpvFontTextColor.setOnColorChangeListener(color -> {
            titleEditText.setTextColor(Color.parseColor(color));
            titleColor = color;
            fontsAdapter.selectedColor = color;
            if (fontsAdapter.selected != -1) {
                fontsAdapter.notifyItemChanged(fontsAdapter.selected);
            }
        });

        //init images view
        rcImages = findViewById(R.id.rcImages);
        Divider divider = new Api21ItemDivider(Color.TRANSPARENT, 10, 10);
        rcImages.setLayoutManager(new GridLayoutManager(this, 3));
        rcImages.addItemDecoration(divider);
        imgAdapter = new ImagesAdapter(this, (view, position) -> previewImage(position));
        rcImages.setAdapter(imgAdapter);


        //pick images
        pickerImage = findViewById(R.id.pickerImage);
        pickerImage.setOnClickListener(this);

        //pick color
        pickerColor = findViewById(R.id.pickerColor);
        pickerColor.setOnClickListener(this);

        imageView = findViewById(R.id.imageView);
        progressBar = findViewById(R.id.progressBar);

        //init fonts
        try {
            // to reach asset
            AssetManager assetManager = getAssets();
            fonts = Arrays.asList(assetManager.list("fonts"));
        } catch (IOException e) {
            // you can print error or log.
        }

        fontsAdapter = new FontsAdapter(this, fonts, (view, position) -> {
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/" + fonts.get(position));
            titleEditText.setTypeface(type);
            fontsAdapter.notifyDataSetChanged();

        });
        rcFonts = findViewById(R.id.rcFonts);
        rcFonts.setAdapter(fontsAdapter);
        rcFonts.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }

    private void selectImage() {
        Album.image(this)
                .multipleChoice()
                .camera(true)
                .columnCount(2)
                .selectCount(10)
                .checkedList(mAlbumFiles)
                .widget(
                        Widget.newDarkBuilder(this)
                                .title(mToolbar.getTitle().toString())
                                .build()
                )
                .onResult(new Action<ArrayList<AlbumFile>>() {
                    @Override
                    public void onAction(@NonNull ArrayList<AlbumFile> result) {
                        mAlbumFiles = result;
                        imgAdapter.notifyDataSetChanged(mAlbumFiles);
                    }
                })
                .onCancel(new Action<String>() {
                    @Override
                    public void onAction(@NonNull String result) {
                        Toast.makeText(CreatePostActivity.this, R.string.canceled, Toast.LENGTH_LONG).show();
                    }
                })
                .start();
    }

    private void previewImage(int position) {
        if (mAlbumFiles == null || mAlbumFiles.size() == 0) {
            Toast.makeText(this, R.string.no_selected, Toast.LENGTH_LONG).show();
        } else {
            Album.galleryAlbum(this)
                    .checkable(true)
                    .checkedList(mAlbumFiles)
                    .currentPosition(position)
                    .widget(
                            Widget.newDarkBuilder(this)
                                    .title(mToolbar.getTitle().toString())
                                    .build()
                    )
                    .onResult(new Action<ArrayList<AlbumFile>>() {
                        @Override
                        public void onAction(@NonNull ArrayList<AlbumFile> result) {
                            mAlbumFiles = result;
                            imgAdapter.notifyDataSetChanged(mAlbumFiles);
                        }
                    })
                    .start();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.imageView) {
            if (CropImage.isExplicitCameraPermissionRequired(CreatePostActivity.this)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                }
            } else {
                CropImage.startPickImageActivity(CreatePostActivity.this);
            }
        } else if (id == R.id.pickerImage) {
            selectImage();
        } else if (id == R.id.pickerColor) {
            cpvFontTextColor.setVisibility(cpvFontTextColor.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        }
    }

    public void showSnackBar(@StringRes int messageId) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                messageId, Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // handle result of pick image chooser
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//            if (isImageFileValid(imageUri)) {
//                this.imageUri = imageUri;
//            }
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
//                }
//            } else {
//                // no permissions required or already grunted
//                loadImageToImageView(imageUri);
//            }
//        }
//    }

    public void savePost(Uri uri, String format) {
        if (BaseVM.hasInternetConnection(this)) {
            createPostVM.post.get().setFormatTile(format);
            showSnackBar(R.string.message_creating_post);
            if (uri != null) {
                createPostVM.postManager.createOrUpdatePostWithImage(uri, this::onCreated, createPostVM.post.get());
            } else {
                createPostVM.postManager.createOrUpdatePost(createPostVM.post.get());
            }
            hideKeyBoard();
        } else {
            Toast.makeText(this, R.string.internet_connection_failed, Toast.LENGTH_SHORT).show();
        }
    }

    public void savePost(ArrayList<Uri> uri, String format) {
        if (BaseVM.hasInternetConnection(this)) {
            if (format != null) {
                createPostVM.post.get().setFormatTile(format);
            }
            showSnackBar(R.string.message_creating_post);
            if (uri != null) {
                createPostVM.postManager.createOrUpdatePostWithImages(uri, this::onCreated, createPostVM.post.get());
            } else {
                createPostVM.postManager.createOrUpdatePost(createPostVM.post.get());
            }
            hideKeyBoard();
        } else {
            Toast.makeText(this, R.string.internet_connection_failed, Toast.LENGTH_SHORT).show();
        }
    }

    private void onCreated(boolean b) {
        if (b) {
            setResult(RESULT_OK);
            this.finish();
        } else {
            Toast.makeText(this, "False", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isImageFileValid(Uri imageUri) {
        int message = R.string.error_general;
        boolean result = false;
        if (imageUri != null) {
            if (ValidationUtil.isImage(imageUri, this)) {
                File imageFile = new File(imageUri.getPath());
                if (imageFile.length() > MAX_FILE_SIZE_IN_BYTES) {
                    message = R.string.error_bigger_file;
                } else {
                    result = true;
                }
            } else {
                message = R.string.error_incorrect_file_type;
            }
        }
        if (!result) {
            int finalMessage = message;
            progressBar.setVisibility(View.GONE);
            showSnackBar(finalMessage);

        }
        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                showSnackBar(R.string.permissions_not_granted);
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (imageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadImageToImageView(imageUri);
            } else {
                showSnackBar(R.string.permissions_not_granted);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }
//        switch (requestCode) {
//            case Constants.TYPE_MULTI_CAPTURE:
//                handleResponseIntent(intent);
//                break;
//            case Constants.TYPE_MULTI_PICKER:
//                handleResponseIntent(intent);
//                break;
//        }
    }


    public void loadImageToImageView(Uri imageUri) {
        if (imageUri == null) {
            return;
        }
        this.imageUri = imageUri;
        ImageUtil.loadLocalImage(GlideApp.with(this), imageUri, imageView, new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        });
    }

    public void requestImageViewFocus() {
        imageView.requestFocus();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.base_create_post_activity_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreatePostVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (itemId == R.id.post) {
            if (createPostVM.validatePost()) {
//                    savePost(imageUri, titleColor + "|" + fontsAdapter.fonts.get(fontsAdapter.selected));
                ArrayList<Uri> uris = new ArrayList<>();
                for (AlbumFile albumFile : mAlbumFiles) {
                    uris.add(Uri.fromFile(new File(albumFile.getPath())));
                }
                savePost(uris, titleColor + "|" + fontsAdapter.fonts.get(fontsAdapter.selected));

            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_post_menu, menu);
        return true;
    }


}
