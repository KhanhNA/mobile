package com.ns.chat.viewmodels;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.google.firebase.auth.FirebaseAuth;
import com.ns.chat.R;
import com.ns.chat.application.Constants;
import com.ns.chat.model.Post;
import com.ns.chat.views.manager.PostManager;
import com.tsolution.base.BaseViewModel;

public class CreatePostVM extends BaseViewModel {

    public PostManager postManager;

    public ObservableField<Post> post = new ObservableField<>();

    public CreatePostVM(@NonNull Application application) {
        super(application);
        postManager = PostManager.getInstance(application.getApplicationContext());
        post.set(new Post());
        post.get().setAuthorId(FirebaseAuth.getInstance().getCurrentUser().getUid());

    }


    public boolean validatePost() {
        boolean isValid = false;
        if(post.get().getTitle() == null  || post.get().getTitle().equals("")){
            addError("title", R.string.warning_empty_title, true);
        }else {
            if(post.get().getTitle().length() > 255){
                addError("title", R.string.error_post_title_length, true);
            }else {
                isValid = true;
                clearErro("title");
            }
        }

        if(post.get().getDescription() == null  || post.get().getDescription().equals("")){
            post.get().setDescription("");
        }

        return isValid;
    }
}
