package com.ns.chat.views.activity;


import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.ns.chat.R;
import com.ns.chat.viewmodels.LoginVM;
import com.ns.chat.views.activity.profile.ProfileFragment;
import com.ns.chat.views.adapter.SectionsPagerAdapter;
import com.ns.chat.views.components.BottomNavigationBehavior;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;


public class MainActivity extends BaseActivity {
    private SectionsPagerAdapter mSectionsPagerAdapter;
    BottomNavigationView navigation;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;

    String current_user_id;
    String deviceToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        //bottom navigation


        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        // attaching bottom sheet behaviour - hide / show on scroll
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());

        navigation.showBadge(R.id.navigation_friends).setNumber(1000);
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword("thonv2@gmail.com", "123456").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){
                    current_user_id = mAuth.getCurrentUser().getUid();
                    Fragment fragment = FriendPostsFragment.getInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString("ProfileActivity.USER_ID_EXTRA_KEY", current_user_id);
                    fragment.setArguments(bundle);
                    loadFragment(fragment);

                } else {


                    String task_result = task.getException().getMessage().toString();

                    Toast.makeText(MainActivity.this, "Error : " + task_result, Toast.LENGTH_LONG).show();

                }

            }
        });

//        if (mAuth.getCurrentUser() != null) {
//
//
//            mUserRef = ChatApplication.getReference().child(DatabaseHelper.PROFILES_DB_KEY).child(mAuth.getCurrentUser().getUid());
//
//        }
    }



    @BindingAdapter({"toastMessage"})
    public static void runMe(View view, String message) {
        if (message != null)
            Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        Fragment fragment;
        Bundle bundle;
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_posts) {
            fragment = FriendPostsFragment.getInstance();
            bundle = new Bundle();
            bundle.putString("ProfileActivity.USER_ID_EXTRA_KEY", current_user_id);
            fragment.setArguments(bundle);
            loadFragment(fragment);
            return true;
        } else if (itemId == R.id.navigation_chats) {//                fragment= ChatsFragment.getInstance();
//                loadFragment(fragment);
////                        mToolbar.setTitle("chats");
//                return true;
            fragment = ProfileFragment.getInstance();
            bundle = new Bundle();
            bundle.putString("ProfileActivity.USER_ID_EXTRA_KEY", current_user_id);
            fragment.setArguments(bundle);
            loadFragment(fragment);
            return true;
        } else if (itemId == R.id.navigation_friends) {//                fragment = FriendsFragment.getInstance();
//                loadFragment(fragment);
//                if(navigation.getBadge(item.getItemId()) != null && navigation.getBadge(item.getItemId()).isVisible()){
//                    navigation.removeBadge(item.getItemId()) ; //  remove badge notification
//
//                }
////                        mToolbar.setTitle("friends");
//                return true;

//                fragment = SettingsFragment.getInstance();
//                loadFragment(fragment);
////                        mToolbar.setTitle("Posts");
//                return true;
        }
        return false;
    };


    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

}
