package com.ns.chat.model;

/**
 * Created by Alexey on 22.05.18.
 */
public class FollowingPost {

    private String profileId;

    public FollowingPost() {
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }
}
