/*
 *  Copyright 2017 Rozdoum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.ns.chat.views.activity.postDetails;

import android.Manifest;
import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.transition.Transition;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.ns.chat.R;
import com.ns.chat.application.GlideApp;
import com.ns.chat.enums.PostStatus;
import com.ns.chat.listener.CustomTransitionListener;
import com.ns.chat.model.Comment;
import com.ns.chat.model.Post;
import com.ns.chat.utils.AnimationUtils;
import com.ns.chat.utils.FormatterUtil;
import com.ns.chat.utils.ImageUtil;
import com.ns.chat.views.activity.CreatePostActivity;
import com.ns.chat.views.activity.ImageDetailFragment;
import com.ns.chat.views.adapter.ImagesPostDetailAdapter;
import com.ns.chat.views.controllers.LikeController;
import com.ns.chat.views.dialogs.EditCommentDialog;
import com.ns.chat.views.manager.PostManager;
import com.theartofdev.edmodo.cropper.CropImage;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseViewModel;
import com.yanzhenjie.album.widget.divider.Api21ItemDivider;
import com.yanzhenjie.album.widget.divider.Divider;

import java.util.ArrayList;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;
import gun0912.tedbottompicker.TedBottomSheetDialogFragment;
import gun0912.tedbottompicker.TedRxBottomPicker;


public class PostDetailsActivityV2 extends BaseActivity implements PostDetailsView {//<PostDetailsView, PostDetailsPresenter> implements PostDetailsView, EditCommentDialog.CommentDialogCallback {

    public static final String POST_ID_EXTRA_KEY = "PostDetailsActivity.POST_ID_EXTRA_KEY";
    public static final String AUTHOR_ANIMATION_NEEDED_EXTRA_KEY = "PostDetailsActivity.AUTHOR_ANIMATION_NEEDED_EXTRA_KEY";
    public static final int UPDATE_POST_REQUEST = 1;
    public static final String POST_STATUS_EXTRA_KEY = "PostDetailsActivity.POST_STATUS_EXTRA_KEY";

    private PostDetailsVM postDetailsVM;
    private RecyclerView commentsRecyclerView;
    private ImageView authorImageView;

    //list post's image
    RecyclerView rcImages;
    ImagesPostDetailAdapter imgAdapter;
    private Toolbar mToolbar;
    private RequestManager requestManager;

    //pick Image comment
    private ImageView pickerImage;
    private List<Uri> selectedUriCmt;
    private ViewGroup mSelectedImagesContainer;
    private Button sendButton;

    //    private EditText commentEditText;
    @Nullable
    private NestedScrollView scrollView;
    private ViewGroup likesContainer;
    private ImageView likesImageView;
    private TextView commentsLabel;
    private TextView likeCounterTextView;
    private TextView commentsCountTextView;
    private TextView watcherCounterTextView;
    private TextView authorTextView;
    private TextView dateTextView;

    private ProgressBar progressBar;


    private TextView titleTextView;
    private TextView descriptionEditText;
    private ProgressBar commentsProgressBar;

    private TextView warningCommentsTextView;


    private MenuItem complainActionMenuItem;
    private MenuItem editActionMenuItem;
    private MenuItem deleteActionMenuItem;

    private String postId;
    private Integer selectedPost;
    private PostManager postManager;
    private LikeController likeController;
    private boolean authorAnimationInProgress = false;

    private boolean isAuthorAnimationRequired;
    //    private CommentsAdapter commentsAdapter;
    private ActionMode mActionMode;
    private boolean isEnterTransitionFinished = false;
    private List<String> imagesTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        requestManager = Glide.with(this);
//        setContentView(R.layout.activity_post_details);
        postDetailsVM = (PostDetailsVM) viewModel;
        BaseAdapter adapter = new BaseAdapter(R.layout.comment_list_item, viewModel, this::onItemClick, this);
        commentsRecyclerView = findViewById(R.id.commentsRecyclerView);
        commentsRecyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        commentsRecyclerView.setLayoutManager(layoutManager);
        commentsRecyclerView.setAdapter(adapter);
        postDetailsVM.setView(this);

        commentsRecyclerView.addItemDecoration(new DividerItemDecoration(commentsRecyclerView.getContext(),
                ((LinearLayoutManager) commentsRecyclerView.getLayoutManager()).getOrientation()));


        postManager = PostManager.getInstance(this);

        isAuthorAnimationRequired = getIntent().getBooleanExtra(AUTHOR_ANIMATION_NEEDED_EXTRA_KEY, false);
        postId = getIntent().getStringExtra(POST_ID_EXTRA_KEY);
        authorImageView = findViewById(R.id.authorImageView);

        incrementWatchersCount();

//        titleTextView = findViewById(R.id.titleTextView);
//        descriptionEditText = findViewById(R.id.descriptionEditText);
//        postImageView = findViewById(R.id.postImageView);
        progressBar = findViewById(R.id.progressBar);
//        commentsRecyclerView = findViewById(R.id.commentsRecyclerView);
        scrollView = findViewById(R.id.scrollView);
        commentsLabel = findViewById(R.id.commentsLabel);
//        commentEditText = findViewById(R.id.commentEditText);
        likesContainer = findViewById(R.id.likesContainer);
        likesImageView = findViewById(R.id.likesImageView);
//
//        authorTextView = findViewById(R.id.authorTextView);
        likeCounterTextView = findViewById(R.id.likeCounterTextView);
        commentsCountTextView = findViewById(R.id.commentsCountTextView);
        watcherCounterTextView = findViewById(R.id.watcherCounterTextView);
        dateTextView = findViewById(R.id.dateTextView);

        //pickImage comment
        pickerImage = findViewById(R.id.pickerImage);
        sendButton = findViewById(R.id.sendButton);
        mSelectedImagesContainer = findViewById(R.id.selected_photos_container);

//        commentsProgressBar = findViewById(R.id.commentsProgressBar);
//        warningCommentsTextView = findViewById(R.id.warningCommentsTextView);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.button_post_comment);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isAuthorAnimationRequired) {
            authorImageView.setScaleX(0);
            authorImageView.setScaleY(0);

            // Add a listener to get noticed when the transition ends to animate the fab button
            getWindow().getSharedElementEnterTransition().addListener(new CustomTransitionListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onTransitionEnd(Transition transition) {
                    super.onTransitionEnd(transition);
                    //disable execution for exit transition
                    if (!isEnterTransitionFinished) {
                        isEnterTransitionFinished = true;
                        AnimationUtils.showViewByScale(authorImageView)
                                .setListener(authorAnimatorListener)
                                .start();
                    }
                }
            });
        }

        initRecyclerView();
        initListeners();

        postDetailsVM.loadPost(postId, selectedPost);
        postDetailsVM.getCommentsList(this, postId);
        supportPostponeEnterTransition();

    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        if(v.getId() == R.id.commentImageView){
            previewImage(0, ((Comment)o).getUrlImages());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        postManager.closeListeners(this);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    hideKeyboard();
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        getIntent().putExtra("postSelected", postDetailsVM.post.get());
        setResult(PostDetailsActivityV2.UPDATE_POST_REQUEST, getIntent());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                && isAuthorAnimationRequired
                && !authorAnimationInProgress
                && !AnimationUtils.isViewHiddenByScale(authorImageView)) {

            ViewPropertyAnimator hideAuthorAnimator = AnimationUtils.hideViewByScale(authorImageView);
            hideAuthorAnimator.setListener(authorAnimatorListener);
            hideAuthorAnimator.withEndAction(PostDetailsActivityV2.this::onBackPressed);
        } else {
            super.onBackPressed();
        }
    }

    private void initListeners() {
//        postImageView.setOnClickListener(v -> postDetailsVM.onPostImageClick());

//        commentEditText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                sendButton.setEnabled(charSequence.toString().trim().length() > 0);
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });

//        sendButton.setOnClickListener(v -> postDetailsVM.onSendButtonClick());

        commentsCountTextView.setOnClickListener(view -> scrollToFirstComment());

//        authorImageView.setOnClickListener(v -> postDetailsVM.onAuthorClick(v));
//        authorTextView.setOnClickListener(v -> postDetailsVM.onAuthorClick(v));

        likesContainer.setOnClickListener(v -> {
            if (likeController != null && postDetailsVM.isPostExist()) {
                likeController.handleLikeClickAction(this, postDetailsVM.post.get());
            }
        });

        //long click for changing animation
        likesContainer.setOnLongClickListener(v -> {
            if (likeController != null) {
                likeController.changeAnimationType();
                return true;
            }

            return false;
        });

        //picker Image
        pickerImage.setOnClickListener(v -> pickImagesForComment());
        sendButton.setOnClickListener(v -> {
            postDetailsVM.onSendButtonClick((ArrayList<Uri>) selectedUriCmt);
        });
    }

    private void pickImagesForComment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            }else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        }else {
            pickImage();
        }


    }

    private void pickImage(){
        TedBottomPicker.with(PostDetailsActivityV2.this)
                .setPeekHeight(getResources().getDisplayMetrics().heightPixels/2)
//                        .setPeekHeight(1600)
                .showTitle(false)
                .setCompleteButtonText("Done")
                .setEmptySelectionText("No Select")
                .setSelectedUriList(selectedUriCmt)
                .showMultiImage(uriList -> {
                    selectedUriCmt = uriList;
                    showUriList(uriList);
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showUriList(List<Uri> uriList) {
        // Remove all views before
        // adding the new ones.
        mSelectedImagesContainer.removeAllViews();
        mSelectedImagesContainer.setVisibility(View.VISIBLE);

        int widthPixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        int heightPixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());

        for (Uri uri : uriList) {
            View imageHolder = LayoutInflater.from(this).inflate(R.layout.image_item, null);
            ImageView thumbnail = imageHolder.findViewById(R.id.media_image);
            requestManager
                    .load(uri.toString())
                    .apply(new RequestOptions().fitCenter())
                    .into(thumbnail);
            mSelectedImagesContainer.addView(imageHolder);
            thumbnail.setLayoutParams(new FrameLayout.LayoutParams(widthPixel, heightPixel));
        }

    }

    private void initRecyclerView() {

        selectedPost = getIntent().getIntExtra("position", 0);

        commentsProgressBar = findViewById(R.id.commentsProgressBar);
        warningCommentsTextView = findViewById(R.id.warningCommentsTextView);

        //init post's image

        //init images view
        rcImages = findViewById(R.id.rcImages);
        Divider divider = new Api21ItemDivider(Color.TRANSPARENT, 10, 10);
        rcImages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rcImages.addItemDecoration(divider);
        imgAdapter = new ImagesPostDetailAdapter(this, (view, position) -> {
            previewImage(position, imagesTitle);
        });
        rcImages.setAdapter(imgAdapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rcImages);

//        commentsAdapter = new CommentsAdapter();
//        commentsAdapter.setCallback(new CommentsAdapter.Callback() {
//            @Override
//            public void onLongItemClick(View view, int position) {
//                Comment selectedComment = commentsAdapter.getItemByPosition(position);
//                startActionMode(selectedComment);
//            }
//
//            @Override
//            public void onAuthorClick(String authorId, View view) {
//                openProfileActivity(authorId, view);
//            }
//        });
//        commentsRecyclerView.setAdapter(commentsAdapter);
//        commentsRecyclerView.setNestedScrollingEnabled(false);
//        commentsRecyclerView.addItemDecoration(new DividerItemDecoration(commentsRecyclerView.getContext(),
//                ((LinearLayoutManager) commentsRecyclerView.getLayoutManager()).getOrientation()));


    }

    private void previewImage(int position, List<String> imags) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("images", (ArrayList)imags);
        bundle.putInt("position", position);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ImageDetailFragment newFragment = ImageDetailFragment.newInstance();
        newFragment.setArguments(bundle);
        newFragment.show(ft, "slideshow");
    }



    private void startActionMode(Comment selectedComment) {
        if (mActionMode != null) {
            return;
        }

        //check access to modify or remove post
        if (postDetailsVM.hasAccessToEditComment(selectedComment.getAuthorId()) || postDetailsVM.hasAccessToModifyPost()) {
            mActionMode = startSupportActionMode(new ActionModeCallback(selectedComment));
        }
    }

    private void incrementWatchersCount() {
        postManager.incrementWatchersCount(postId);
        Intent intent = getIntent();
        setResult(RESULT_OK, intent.putExtra(POST_STATUS_EXTRA_KEY, PostStatus.UPDATED));
    }

    @Override
    public void scrollToFirstComment() {
        showCommentProgress(false);
        mSelectedImagesContainer.removeAllViews();
        mSelectedImagesContainer.setVisibility(View.GONE);
        scrollView.smoothScrollTo(0, commentsLabel.getTop());
    }

    @Override
    public void clearCommentField() {
//        commentEditText.setText(null);
//        commentEditText.clearFocus();
//        commentText = "";
        hideKeyboard();
    }

    private void scheduleStartPostponedTransition(final ImageView imageView) {
        imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                supportStartPostponedEnterTransition();
                return true;
            }
        });
    }

    @Override
    public void openProfileActivity(String userId, View view) {
//        Intent intent = new Intent(PostDetailsActivityV2.this, ProfileActivity.class);
//        intent.putExtra(ProfileActivity.USER_ID_EXTRA_KEY, userId);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && view != null) {
//
//            ActivityOptions options = ActivityOptions.
//                    makeSceneTransitionAnimation(PostDetailsActivity.this,
//                            new android.util.Pair<>(view, getString(R.string.post_author_image_transition_name)));
//            startActivity(intent, options.toBundle());
//        } else {
//            startActivity(intent);
//        }
    }

    @Override
    public void setTitle(String title) {
//        titleTextView.setText(title);
    }

    @Override
    public void setDescription(String description) {
//        descriptionEditText.setText(description);
    }


    @Override
    public void loadPostDetailImage(ArrayList<String> imageTitle) {
        this.imagesTitle = imageTitle;
        progressBar.setVisibility(View.GONE);
        imgAdapter.notifyDataSetChanged(imageTitle);
    }

    @Override
    public void loadAuthorPhoto(String photoUrl) {
        ImageUtil.loadImage(GlideApp.with(getApplicationContext()), photoUrl, authorImageView, DiskCacheStrategy.DATA);
    }

    @Override
    public void setAuthorName(String username) {
//        authorTextView.setText(username);
    }

    @Override
    public void initLikeController(@NonNull Post post) {
        getSupportActionBar().setTitle(postDetailsVM.post.get().getTitle());
        likeController = new LikeController(this, post, likeCounterTextView, likesImageView, false);
    }

    @Override
    public void updateCounters(@NonNull Post post) {
        long commentsCount = post.getCommentsCount();
        commentsCountTextView.setText(String.valueOf(commentsCount));
        commentsLabel.setText(String.format(getString(R.string.label_comments), commentsCount));
        likeCounterTextView.setText(String.valueOf(post.getLikesCount()));
        likeController.setUpdatingLikeCounter(false);

        watcherCounterTextView.setText(String.valueOf(post.getWatchersCount()));

        CharSequence date = FormatterUtil.getRelativeTimeSpanStringShort(this, post.getCreatedDate());
        dateTextView.setText(date);

        postDetailsVM.updateCommentsVisibility(commentsCount);
    }

    @Override
    public void initLikeButtonState(boolean exist) {
        if (likeController != null) {
            likeController.initLike(exist);
        }
    }

    @Override
    public void showComplainMenuAction(boolean show) {
        if (complainActionMenuItem != null) {
            complainActionMenuItem.setVisible(show);
        }
    }

    @Override
    public void showEditMenuAction(boolean show) {
        if (editActionMenuItem != null) {
            editActionMenuItem.setVisible(show);
        }
    }

    @Override
    public void showDeleteMenuAction(boolean show) {
        if (deleteActionMenuItem != null) {
            deleteActionMenuItem.setVisible(show);
        }
    }

    @Override
    public String getCommentText() {
        return postDetailsVM.getCommentText().get();
    }

//    @Override
//    public String getCommentText() {
//        return commentEditText.getText().toString();
//    }

    @Override
    public void openEditPostActivity(Post post) {
//        Intent intent = new Intent(PostDetailsActivity.this, EditPostActivity.class);
//        intent.putExtra(EditPostActivity.POST_EXTRA_KEY, post);
//        startActivityForResult(intent, EditPostActivity.EDIT_POST_REQUEST);
    }

    @Override
    public void showCommentProgress(boolean show) {
        commentsProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showCommentsWarning(boolean show) {
        warningCommentsTextView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showCommentsRecyclerView(boolean show) {
        commentsRecyclerView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onCommentsListChanged(List<Comment> list) {
//        commentsAdapter.setList(list);
        commentsRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showCommentsLabel(boolean show) {
        commentsLabel.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void openEditCommentDialog(Comment comment) {
        EditCommentDialog editCommentDialog = new EditCommentDialog();
        Bundle args = new Bundle();
        args.putString(EditCommentDialog.COMMENT_TEXT_KEY, comment.getText());
        args.putString(EditCommentDialog.COMMENT_ID_KEY, comment.getId());
        editCommentDialog.setArguments(args);
        editCommentDialog.show(getFragmentManager(), EditCommentDialog.TAG);
    }

    @Override
    public void onCommentChanged(String newText, String commentId) {
        postDetailsVM.updateComment(newText, commentId);
    }

    @Override
    public void onPostRemoved() {
        Intent intent = getIntent();
        setResult(RESULT_OK, intent.putExtra(POST_STATUS_EXTRA_KEY, PostStatus.REMOVED));
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_post_details;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PostDetailsVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.commentsRecyclerView;
    }

    @Override
    public void process(String s, View view, BaseViewModel baseViewModel, Throwable throwable) {

    }

    private class ActionModeCallback implements ActionMode.Callback {

        Comment selectedComment;

        ActionModeCallback(Comment selectedComment) {
            this.selectedComment = selectedComment;
        }

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.comment_context_menu, menu);

            menu.findItem(R.id.editMenuItem).setVisible(postDetailsVM.hasAccessToEditComment(selectedComment.getAuthorId()));

            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }
        // Called when the user selects a contextual menu item

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int itemId = item.getItemId();
            if (itemId == R.id.editMenuItem) {
                openEditCommentDialog(selectedComment);
                mode.finish(); // Action picked, so close the CAB
                return true;
            } else if (itemId == R.id.deleteMenuItem) {
                postDetailsVM.removeComment(selectedComment.getId());
                mode.finish();
                return true;
            }
            return false;
        }
        // Called when the user exits the action mode

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    }

    Animator.AnimatorListener authorAnimatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {
            authorAnimationInProgress = true;
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            authorAnimationInProgress = false;
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            authorAnimationInProgress = false;
        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post_details_menu, menu);
//        complainActionMenuItem = menu.findItem(R.id.complain_action);
//        editActionMenuItem = menu.findItem(R.id.edit_post_action);
//        deleteActionMenuItem = menu.findItem(R.id.delete_post_action);
//        postDetailsVM.updateOptionMenuVisibility();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!postDetailsVM.isPostExist()) {
            return super.onOptionsItemSelected(item);
        }
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (itemId == R.id.complain_action) {
            postDetailsVM.doComplainAction();
            return true;
        } else if (itemId == R.id.edit_post_action) {
            postDetailsVM.editPostAction();
            return true;
        } else if (itemId == R.id.delete_post_action) {
            postDetailsVM.attemptToRemovePost();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
