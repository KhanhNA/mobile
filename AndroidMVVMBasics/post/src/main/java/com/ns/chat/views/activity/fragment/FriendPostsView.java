/*
 * Copyright 2018 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ns.chat.views.activity.fragment;

import android.view.View;

import com.ns.chat.model.Post;
import com.ns.chat.views.activity.base.BaseFragmentView;

import java.util.List;

/**
 * Created by Alexey on 03.05.18.
 */

public interface FriendPostsView extends BaseFragmentView {
    void openPostDetailsActivity(String postId, View v);

    void openProfileActivity(String userId, View view);

    void onFollowingPostsLoaded(List<Post> list);

    void onFollowingPostsAdd(Post post);
    void onFollowingPostsChange(Post post);
    void onFollowingPostsRemoved(Post post);

    void showLocalProgress();

    void hideLocalProgress();

    void showEmptyListMessage(boolean show);
}
