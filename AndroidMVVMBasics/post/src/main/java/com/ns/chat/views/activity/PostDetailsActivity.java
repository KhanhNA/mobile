package com.ns.chat.views.activity;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ns.chat.R;
import com.ns.chat.application.GlideApp;
import com.ns.chat.enums.PostStatus;
import com.ns.chat.listener.CustomTransitionListener;
import com.ns.chat.listener.OnPostChangedListener;
import com.ns.chat.model.Post;
import com.ns.chat.utils.AnimationUtils;
import com.ns.chat.viewmodels.FriendPostsVM;
import com.ns.chat.views.controllers.LikeController;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

public class PostDetailsActivity extends BaseActivity {
    public static final String POST_ID_EXTRA_KEY = "PostDetailsActivity.POST_ID_EXTRA_KEY";
    public static final String AUTHOR_ANIMATION_NEEDED_EXTRA_KEY = "PostDetailsActivity.AUTHOR_ANIMATION_NEEDED_EXTRA_KEY";
    public static final int UPDATE_POST_REQUEST = 1;
    public static final String POST_STATUS_EXTRA_KEY = "PostDetailsActivity.POST_STATUS_EXTRA_KEY";

    private boolean isAuthorAnimationRequired;
    private String postId;
    private ImageView authorImageView;
    private boolean isEnterTransitionFinished = false;
    private boolean authorAnimationInProgress = false;
    private boolean isPostExist;
    private LikeController likeController;

    FriendPostsVM friendPostsVM;

    private ImageView likesImageView;
    private TextView likeCounterTextView;
    //    private ImageView postImageView;
    private MenuItem complainActionMenuItem;
    private MenuItem editActionMenuItem;
    private MenuItem deleteActionMenuItem;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        friendPostsVM = (FriendPostsVM) viewModel;
        initView();
        isAuthorAnimationRequired = getIntent().getBooleanExtra(AUTHOR_ANIMATION_NEEDED_EXTRA_KEY, false);
        postId = getIntent().getStringExtra(POST_ID_EXTRA_KEY);
        incrementWatchersCount();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isAuthorAnimationRequired) {
            authorImageView.setScaleX(0);
            authorImageView.setScaleY(0);

            // Add a listener to get noticed when the transition ends to animate the fab button
            getWindow().getSharedElementEnterTransition().addListener(new CustomTransitionListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onTransitionEnd(Transition transition) {
                    super.onTransitionEnd(transition);
                    //disable execution for exit transition
                    if (!isEnterTransitionFinished) {
                        isEnterTransitionFinished = true;
                        AnimationUtils.showViewByScale(authorImageView)
                                .setListener(authorAnimatorListener)
                                .start();
                    }
                }
            });
        }
        loadPost();
        supportPostponeEnterTransition();
    }

    public void loadPost() {
        friendPostsVM.getPostManager().getPost(this, postId, new OnPostChangedListener() {
            @Override
            public void onObjectChanged(Post obj) {
//                ifViewAttached(view -> {
                if (obj != null) {
//                        friendPostsVM.postDetail.set(obj);
                    isPostExist = true;
//                        initLikeController(friendPostsVM.postDetail.get());
//                        fillInUI(friendPostsVM.postDetail.get());
//                        initLikeButtonState(friendPostsVM.postDetail.get());
                    updateOptionMenuVisibility();
                }
//                    else if (!postRemovingProcess) {
//                        isPostExist = false;
//                        view.onPostRemoved();
//                        view.showNotCancelableWarningDialog(getString(R.string.error_post_was_removed));
//                    }
//                });
            }

            @Override
            public void onError(String errorText) {
//                ifViewAttached(view -> {
//                    view.showNotCancelableWarningDialog(errorText);
//                });
            }
        });
    }

    //    private void fillInUI(@NonNull Post post) {
////        ifViewAttached(view -> {
//            setTitle(post.getTitle());
//            friendPostsVM.getPostManager().loadImageMediumSize(GlideApp.with(this), post.getImageTitle(), postImageView, () -> {
//                scheduleStartPostponedTransition(postImageView);
//                progressBar.setVisibility(View.GONE);
//            });
////            loadAuthorProfile();
////        });
//    }
    private void scheduleStartPostponedTransition(final ImageView imageView) {
        imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                supportStartPostponedEnterTransition();
                return true;
            }
        });
    }

    public void updateOptionMenuVisibility() {
//        ifViewAttached(view -> {
//            if (friendPostsVM.postDetail.get() != null) {
        showEditMenuAction(hasAccessToModifyPost());
        showDeleteMenuAction(hasAccessToModifyPost());
//                showComplainMenuAction(!friendPostsVM.postDetail.get().isHasComplain());
//            }
//        });
    }

    public boolean hasAccessToModifyPost() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return currentUser != null;// && friendPostsVM.postDetail.get() != null && friendPostsVM.postDetail.get().getAuthorId().equals(currentUser.getUid());
    }

    public void showEditMenuAction(boolean show) {
        if (editActionMenuItem != null) {
            editActionMenuItem.setVisible(show);
        }
    }

    public void showComplainMenuAction(boolean show) {
        if (complainActionMenuItem != null) {
            complainActionMenuItem.setVisible(show);
        }
    }

    public void showDeleteMenuAction(boolean show) {
        if (deleteActionMenuItem != null) {
            deleteActionMenuItem.setVisible(show);
        }
    }

    public void initLikeController(@NonNull Post post) {
        likeController = new LikeController(this, post, likeCounterTextView, likesImageView, false);
    }

    private void initView() {
        progressBar = findViewById(R.id.progressBar);
//        postImageView = findViewById(R.id.postImageView);
        likesImageView = findViewById(R.id.likesImageView);
        likeCounterTextView = findViewById(R.id.likeCounterTextView);
        authorImageView = findViewById(R.id.authorImageView);
    }

    private void incrementWatchersCount() {
        friendPostsVM.incrementWatchersCount(postId);
        Intent intent = getIntent();
        setResult(RESULT_OK, intent.putExtra(POST_STATUS_EXTRA_KEY, PostStatus.UPDATED));
    }


    @Override
    public int getLayoutRes() {
        return R.layout.activity_post_details;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return FriendPostsVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.commentsRecyclerView;
    }

    Animator.AnimatorListener authorAnimatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {
            authorAnimationInProgress = true;
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            authorAnimationInProgress = false;
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            authorAnimationInProgress = false;
        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

}
