/*
 * Copyright 2017 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ns.chat.model;


import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.ns.chat.utils.FormatterUtil;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Comment extends BaseModel {

    private String id;
    private String text;
    private String authorId;
    private long createdDate;
    private ArrayList<String> urlImages;

    public Comment() {
        // Default constructor required for calls to DataSnapshot.getValue(Comment.class)
    }

    public Comment(String text) {

        this.text = text;
        this.createdDate = Calendar.getInstance().getTimeInMillis();
    }


    @Override
    public String getIndex() {
        return index == null?null:super.getIndex();
//        return super.getIndex();
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("authorId", authorId);
        result.put("createdDate", createdDate);
        result.put("id", id);
        result.put("text", text);
        result.put("urlImages", urlImages);
        return result;
    }
}
