/*
 * Copyright 2017 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ns.chat.model;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.IgnoreExtraProperties;
import com.ns.chat.application.GlideApp;
import com.ns.chat.enums.ItemType;
import com.ns.chat.utils.ImageUtil;
import com.ns.chat.views.manager.PostManager;

import java.io.Serializable;

@IgnoreExtraProperties
public class Profile implements Serializable, LazyLoading {

    private String device_token;
    private String email;
    private String id;
    private String image;
    private long likesCount;
    private String name;
//    private Long online;
    private String status;
    private String thumb_image;

    private String registrationToken;
    private ItemType itemType;



    public Profile() {
        // Default constructor required for calls to DataSnapshot.getValue(Profile.class)
    }

    public Profile(String id) {
        this.id = id;
    }

    public Profile(ItemType load) {
        itemType = load;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public String getRegistrationToken() {
        return registrationToken;
    }

    public void setRegistrationToken(String registrationToken) {
        this.registrationToken = registrationToken;
    }

    @Override
    public ItemType getItemType() {
        return itemType;
    }

    @Override
    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThumb_image() {
        if(thumb_image == null){
            thumb_image = "default";
        }
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        if(thumb_image == null){
            thumb_image = "default";
        }
        this.thumb_image = thumb_image;
    }

    @BindingAdapter("imageProfile")
    public static void loadImage(ImageView view, String url) {
        if(url != null && !url.equals("")){
            ImageUtil.loadImage(GlideApp.with(view.getContext()), url, view, DiskCacheStrategy.DATA);
        }
    }
}
