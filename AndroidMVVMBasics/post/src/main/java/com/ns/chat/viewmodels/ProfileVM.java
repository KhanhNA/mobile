/*
 * Copyright 2018 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ns.chat.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.view.View;

import androidx.annotation.NonNull;

import com.ns.chat.R;
import com.ns.chat.enums.FollowState;
import com.ns.chat.enums.PostStatus;
import com.ns.chat.listener.OnDataChangedListener;
import com.ns.chat.listener.OnObjectChangedListenerSimple;
import com.ns.chat.listener.OnPostChangedListener;
import com.ns.chat.model.Post;
import com.ns.chat.model.Profile;
import com.ns.chat.utils.LogUtil;
import com.ns.chat.views.activity.profile.ProfileView;
import com.ns.chat.views.components.FollowButton;
import com.ns.chat.views.manager.FollowManager;
import com.ns.chat.views.manager.PostManager;
import com.ns.chat.views.manager.ProfileManager;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.List;


/**
 * Created by Alexey on 03.05.18.
 */

public class ProfileVM extends BaseVM { //<ProfileView> {

    private final FollowManager followManager;
//    private Activity activity;
    private ProfileManager profileManager;
    private PostManager postManager;

    private Profile profile;

    public ProfileVM(@NonNull Application application) {
        super(application);
        Context context = application.getApplicationContext();
        profileManager = ProfileManager.getInstance(context);
        followManager = FollowManager.getInstance(context);
        postManager = PostManager.getInstance(context);
    }


//    ProfileVM(Activity activity) {
//        super(activity);
//        this.activity = activity;
//        profileManager = ProfileManager.getInstance(context);
//        followManager = FollowManager.getInstance(context);
//    }

    private void followUser(Activity activity, String targetUserId) {
//        ifViewAttached(BaseView::showProgress);
        followManager.followUser(activity, getCurrentUserId(), targetUserId, success -> {

                if (success) {
                    ((ProfileView)view).setFollowStateChangeResultOk();
                    checkFollowState(targetUserId);
                } else {
                    LogUtil.logDebug(TAG, "followUser, success: " + false);
                }

        });
    }

    public void unfollowUser(Activity activity, String targetUserId) {
//        ifViewAttached(BaseView::showProgress);
        followManager.unfollowUser(activity, getCurrentUserId(), targetUserId, success ->
                {
                    if (success) {
                        ((ProfileView)view).setFollowStateChangeResultOk();
                        checkFollowState(targetUserId);
                    } else {
                        LogUtil.logDebug(TAG, "unfollowUser, success: " + false);
                    }
                });
    }

    public void onFollowButtonClick(Activity activity, int state, String targetUserId) {
        if (checkInternetConnection() && checkAuthorization()) {
            if (state == FollowButton.FOLLOW_STATE || state == FollowButton.FOLLOW_BACK_STATE) {
                followUser(activity, targetUserId);
            } else if (state == FollowButton.FOLLOWING_STATE && profile != null) {
//                ifViewAttached(view -> view.showUnfollowConfirmation(profile));
            }
        }
    }

    public void checkFollowState(String targetUserId) {
        String currentUserId = getCurrentUserId();

        if (currentUserId != null) {
            if (!targetUserId.equals(currentUserId)) {
                followManager.checkFollowState(currentUserId, targetUserId, followState -> {
//                    ifViewAttached(view -> {
//                        view.hideProgress();
                    ((ProfileView)view).updateFollowButtonState(followState);

//                    });
                });
            } else {
                ((ProfileView)view).updateFollowButtonState(FollowState.MY_PROFILE);
            }
        } else {
            ((ProfileView)view).updateFollowButtonState(FollowState.NO_ONE_FOLLOW);
        }
    }

    public void getFollowersCount(String targetUserId) {
        followManager.getFollowersCount(context, targetUserId, count -> {
//            ifViewAttached(view -> view.updateFollowersCount((int) count));
        });
    }

    public void getFollowingsCount(String targetUserId) {
        followManager.getFollowingsCount(context, targetUserId, count -> {
            ((ProfileView)view).updateFollowingsCount((int) count);
        });
    }

    void onPostClick(Post post, View postItemView) {
        PostManager.getInstance(context).isPostExistSingleValue(post.getId(), exist -> {

                if (exist) {
                    ((ProfileView)view).openPostDetailsActivity(post, postItemView);
                } else {
//                    ((ProfileView)view).showSnackBar(R.string.error_post_was_removed);
                }

        });
    }

    public Spannable buildCounterSpannable(String label, int value) {
        SpannableStringBuilder contentString = new SpannableStringBuilder();
        contentString.append(String.valueOf(value));
        contentString.append("\n");
        int start = contentString.length();
        contentString.append(label);
        contentString.setSpan(new TextAppearanceSpan(context, R.style.TextAppearance_AppCompat), start, contentString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return contentString;
    }

    public void onEditProfileClick() {
//        if (checkInternetConnection()) {
//            ifViewAttached(ProfileView::startEditProfileActivity);
//        }
    }

    public void onCreatePostClick() {
//        if (checkInternetConnection()) {
//            ifViewAttached(ProfileView::openCreatePostActivity);
//        }
    }

    public void loadPosts(String userId){
        OnDataChangedListener<Post> onPostsDataChangedListener = new OnDataChangedListener<Post>() {
            @Override
            public void onListChanged(List<Post> list) {
//                setList(list);
                setData(baseModels, list);
                ((ProfileView)view).onPostUpdated();
//                callBack.onPostsListChanged(list.size());
            }
        };
        postManager.getPostsListByUser(onPostsDataChangedListener, userId);
    }


    public void loadProfile(Context activityContext, String userID) {
        profileManager.getProfileValue(activityContext, userID, new OnObjectChangedListenerSimple<Profile>() {
            @Override
            public void onObjectChanged(Profile obj) {
                profile = obj;
                if(obj == null){
                    return;
                }

                ((ProfileView)view).setProfileName(profile.getName());

                    if (profile.getImage() != null) {
                        ((ProfileView)view).setProfilePhoto(profile.getImage());
                    } else {
                        ((ProfileView)view).setDefaultProfilePhoto();
                    }

                    int likesCount = (int) profile.getLikesCount();
                    String likesLabel = context.getResources().getQuantityString(R.plurals.likes_counter_format, likesCount, likesCount);
                ((ProfileView)view).updateLikesCounter(buildCounterSpannable(likesLabel, likesCount));

            }
        });
    }

    public void onPostListChanged(int postsCount) {

            String postsLabel = context.getResources().getQuantityString(R.plurals.posts_counter_format, postsCount, postsCount);
        ((ProfileView)view).updatePostsCounter(buildCounterSpannable(postsLabel, postsCount));
        ((ProfileView)view).showLikeCounter(true);
        ((ProfileView)view).showPostCounter(true);
        ((ProfileView)view).hideLoadingPostsProgress();



        }

    public void checkPostChanges(Intent data) {

//            if (data != null) {
//                PostStatus postStatus = (PostStatus) data.getSerializableExtra(PostDetailsActivity.POST_STATUS_EXTRA_KEY);
//
//                if (postStatus.equals(PostStatus.REMOVED)) {
//                    view.onPostRemoved();
//                } else if (postStatus.equals(PostStatus.UPDATED)) {
//                    view.onPostUpdated();
//                }
//            }
        ;
    }
}
