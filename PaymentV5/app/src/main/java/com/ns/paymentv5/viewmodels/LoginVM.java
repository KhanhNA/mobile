package com.ns.paymentv5.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableField;

import com.ns.paymentv5.api.BaseApiManager;
import com.ns.paymentv5.api.BaseURL;
import com.ns.paymentv5.models.AuthenticationRequest;
import com.ns.paymentv5.models.User;
import com.ns.paymentv5.utils.Constants;
import com.ns.paymentv5.viewmodels.base.BaseVM;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class LoginVM extends BaseVM {
    private ObservableField<String> usernameError = new ObservableField<>();
    private ObservableField<String> passwordError = new ObservableField<>();

    private ObservableField<String> username = new ObservableField<>();
    private ObservableField<String> password = new ObservableField<>();
    private MutableLiveData<Boolean> loginSuccess = new MutableLiveData<>();
    private MutableLiveData<Boolean> loginFail = new MutableLiveData<>();
    private MutableLiveData<Boolean> showProgress = new MutableLiveData<>();

    public void login() {
        String user = username.get();
        String pass = password.get();
        if (isCredentialsValid(user, pass)) {
            showProgress.setValue(true);
            getCompositeDisposable().add(getDataManager()
                    .login(new AuthenticationRequest(user, pass))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<User>() {
                        @Override
                        public void onComplete() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            loginFail.setValue(true);
                            showProgress.setValue(false);
                            String errorMessage;
                            try {
                                if (e instanceof HttpException) {
                                    if (((HttpException) e).code() == 503) {
                                    } else {
                                        ((HttpException) e).response().errorBody().string();
                                    }
                                }
                            } catch (Throwable throwable) {
                                RxJavaPlugins.getErrorHandler();
                            }
                        }

                        @Override
                        public void onNext(User user) {
                            showProgress.setValue(false);
                            if (user != null) {
                                final String userName = user.getUsername();
                                final long userID = user.getUserId();
                                saveAuthenticationTokenForSession(userName, userID, user.getBase64EncodedAuthenticationKey());
                                loginSuccess.setValue(true);
                            } else {

                            }
                        }
                    })
            );
        }

    }

    private boolean isCredentialsValid(String username, String password) {
        boolean credentialValid = true;
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            usernameError.set("Test User Name");
            passwordError.set("Test Pass word");
            return false;
        }

        final String correctUsername = username.replaceFirst("\\s++$", "").trim();
//        if (username.matches("\\s*") || username.isEmpty()) {
//            usernameError.set("Test User Name");
//            credentialValid = false;
//        } else if (username.length() < 5) {
//            usernameError.set("Test User Name");
//            credentialValid = false;
//        } else if (correctUsername.contains(" ")) {
//            usernameError.set("Test User Name");
//            credentialValid = false;
//        } else {
//
//        }
//
//        if (password.isEmpty()) {
//            credentialValid = false;
//        } else if (password.length() < 6) {
//            credentialValid = false;
//        } else {
//        }

        return credentialValid;
    }

    private void saveAuthenticationTokenForSession(String userName, long userID, String authToken) {
        Constants.TOKEN = authToken;
        BaseApiManager.createService(new BaseURL().getDefaultBaseUrl(), authToken);
        reInitializeService();
    }

    private void reInitializeService() {

    }

    public ObservableField<String> getUsername() {
        return username;
    }

    public ObservableField<String> getPassword() {
        return password;
    }

    public MutableLiveData<Boolean> getLoginSuccess() {
        return loginSuccess;
    }

    public MutableLiveData<Boolean> getLoginFail() {
        return loginFail;
    }

    public MutableLiveData<Boolean> getShowProgress() {
        return showProgress;
    }

    public ObservableField<String> getUsernameError() {
        return usernameError;
    }

    public ObservableField<String> getPasswordError() {
        return passwordError;
    }
}
