package com.ns.paymentv5.models;

import java.io.Serializable;
import java.math.BigDecimal;

public class PaymentDetail implements Serializable {
    private final BigDecimal transferAmount;
    private String transferDescription;

    public PaymentDetail(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public String getTransferDescription() {
        return transferDescription;
    }

    public void setTransferDescription(String transferDescription) {
        this.transferDescription = transferDescription;
    }


}
