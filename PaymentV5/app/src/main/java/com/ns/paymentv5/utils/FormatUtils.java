package com.ns.paymentv5.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class FormatUtils {
    private static DecimalFormat formatter;

    static public String formatCurrency(BigDecimal money) {
        if (money == null) {
            return "0 đ";
        }
        if (formatter == null) {
            formatter = new DecimalFormat("###,###,###,###");
        }

        return formatter.format(money) + " đ";
    }
}
