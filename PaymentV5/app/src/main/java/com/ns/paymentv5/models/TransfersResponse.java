package com.ns.paymentv5.models;

import java.io.Serializable;

public class TransfersResponse implements Serializable {
    private Long savingsId;
    private Long resourceId;

    public Long getSavingsId() {
        return savingsId;
    }

    public void setSavingsId(Long savingsId) {
        this.savingsId = savingsId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }
}
