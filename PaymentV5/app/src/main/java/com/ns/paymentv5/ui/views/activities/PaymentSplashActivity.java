package com.ns.paymentv5.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;

import com.ns.paymentv5.ui.views.base.BaseActivity;

public class PaymentSplashActivity extends BaseActivity {


    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = new Intent(this, PaymentLoginActivity.class);
        startActivity(intent);
        finish();
    }
}
