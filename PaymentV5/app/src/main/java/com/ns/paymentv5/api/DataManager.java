package com.ns.paymentv5.api;

import com.ns.paymentv5.models.AuthenticationRequest;
import com.ns.paymentv5.models.PaymentDetail;
import com.ns.paymentv5.models.TransfersResponse;
import com.ns.paymentv5.models.User;

import io.reactivex.Observable;


public class DataManager {
    private final BaseApiManager baseApiManager;

    public DataManager( BaseApiManager baseApiManager) {
        this.baseApiManager = baseApiManager;
    }

    public Observable<User> login(AuthenticationRequest request) {
        return baseApiManager.getAuthenticationApi().authenticate(request);
    }

    public Observable<TransfersResponse> transfers(PaymentDetail request) {
        return baseApiManager.getTransfersService().transfers(request);
    }
}
