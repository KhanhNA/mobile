/*
 * This project is licensed under the open source MPL V2.
 * See https://github.com/openMF/android-client/blob/master/LICENSE.md
 */

package com.ns.paymentv5.api;


import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class SelfServiceInterceptor implements Interceptor {

    private static final String HEADER_AUTH = "Authorization";
    private static final String BASIC = "Basic ";
    private String authToken;

    SelfServiceInterceptor(String authToken) {
        this.authToken = authToken;
    }

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request chainRequest = chain.request();
        Builder builder = chainRequest.newBuilder();

        if (!TextUtils.isEmpty(authToken)) {
            builder.header(HEADER_AUTH,  SelfServiceInterceptor.BASIC + authToken);
        }

        Request request = builder.build();
        return chain.proceed(request);
    }
}
