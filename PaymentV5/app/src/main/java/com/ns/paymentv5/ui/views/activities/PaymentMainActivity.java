package com.ns.paymentv5.ui.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ns.paymentv5.R;
import com.ns.paymentv5.databinding.PaymentHomeBinding;
import com.ns.paymentv5.models.PaymentDetail;
import com.ns.paymentv5.utils.PaymentConfiguration;
import com.ns.paymentv5.viewmodels.PaymentLibVM;

public class PaymentMainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        PaymentDetail paymentDetail = (PaymentDetail) intent.getSerializableExtra(PaymentConfiguration.PAYMENT_DETAIL);
        if (paymentDetail != null) {
            PaymentLibVM paymentLibVM = new PaymentLibVM();
            PaymentHomeBinding activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.payment_home);
            activityLoginBinding.setViewModel(paymentLibVM);
            paymentLibVM.setPaymentDetail(paymentDetail);
            paymentLibVM.getRsError().observe(this, errorDetail -> {
                if (errorDetail != null && errorDetail.getMessage() != null) {
                    //
                    Toast.makeText(PaymentMainActivity.this, errorDetail.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            findViewById(R.id.btnPayment).setOnClickListener(view -> {
                if (paymentLibVM.getDescription().get() != null && !"".equals(paymentLibVM.getDescription().get())) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(PaymentConfiguration.PAYMENT_RESULT, paymentLibVM.getDescription());
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Toast.makeText(PaymentMainActivity.this, "Description not empty", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            showError();
        }

    }

    private void showError() {

    }
}
