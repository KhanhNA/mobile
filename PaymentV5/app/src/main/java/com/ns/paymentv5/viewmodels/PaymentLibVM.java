package com.ns.paymentv5.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableField;
import android.util.Log;

import com.ns.paymentv5.models.ErrorDetail;
import com.ns.paymentv5.models.PaymentDetail;
import com.ns.paymentv5.models.TransfersResponse;
import com.ns.paymentv5.utils.FormatUtils;
import com.ns.paymentv5.viewmodels.base.BaseVM;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import lombok.Setter;
import retrofit2.adapter.rxjava2.HttpException;

@Setter
public class PaymentLibVM extends BaseVM {
    private ObservableField<String> description = new ObservableField<>();
    private PaymentDetail paymentDetail;
    private MutableLiveData<TransfersResponse> response = new MutableLiveData<>();
    private MutableLiveData<ErrorDetail> rsError = new MutableLiveData<>();
    private ObservableField<Boolean> loading = new ObservableField<>();

    public PaymentLibVM() {
        loading.set(false);
    }

    public String getTransferAmount() {
        return FormatUtils.formatCurrency(paymentDetail.getTransferAmount());
    }

    public ObservableField<String> getDescription() {
        return description;
    }

    public void setDescription(ObservableField<String> description) {
        this.description = description;
    }

    public MutableLiveData<TransfersResponse> getResponse() {
        return response;
    }

    public MutableLiveData<ErrorDetail> getRsError() {
        return rsError;
    }

    public ObservableField<Boolean> getLoading() {
        return loading;
    }
}
