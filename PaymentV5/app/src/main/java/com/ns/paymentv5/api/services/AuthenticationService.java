package com.ns.paymentv5.api.services;


import com.ns.paymentv5.models.AuthenticationRequest;
import com.ns.paymentv5.models.User;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthenticationService {
    @POST("authenticate")
    Observable<User> authenticate(@Body AuthenticationRequest request);
}
