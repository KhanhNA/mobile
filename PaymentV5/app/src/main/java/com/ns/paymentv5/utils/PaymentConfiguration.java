package com.ns.paymentv5.utils;

import com.ns.paymentv5.models.PaymentDetail;

public class PaymentConfiguration {
    public static String PAYMENT_RESULT = "payment_result";
    public static String PAYMENT_DETAIL = "payment_detail";
    public static String PAYMENT_TOKEN = "tokenMifos";
}
