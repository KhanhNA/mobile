package com.ns.paymentv5.ui.views.binding;

import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputLayout;

public class InputTextLayoutBinding {
    @BindingAdapter("app:errorText")
    public static void setErrorMessage(TextInputLayout view, String errorMessage) {
        view.setError(errorMessage);
    }
}
