package com.ns.paymentv5.api.services;

import com.ns.paymentv5.models.AuthenticationRequest;
import com.ns.paymentv5.models.PaymentDetail;
import com.ns.paymentv5.models.TransfersResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TransfersService {
    @POST("transfer")
    Observable<TransfersResponse> transfers(@Body PaymentDetail request);
}
