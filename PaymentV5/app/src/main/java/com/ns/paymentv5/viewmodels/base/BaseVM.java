package com.ns.paymentv5.viewmodels.base;

import android.databinding.BaseObservable;

import com.ns.paymentv5.api.BaseApiManager;
import com.ns.paymentv5.api.DataManager;

import io.reactivex.disposables.CompositeDisposable;

public class BaseVM extends BaseObservable {
    private CompositeDisposable compositeDisposable;
    private DataManager dataManager;

    public BaseVM() {
        this.dataManager = new DataManager(BaseApiManager.getInstance());
        compositeDisposable = new CompositeDisposable();
    }

    protected CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    protected DataManager getDataManager() {
        return dataManager;
    }
}
