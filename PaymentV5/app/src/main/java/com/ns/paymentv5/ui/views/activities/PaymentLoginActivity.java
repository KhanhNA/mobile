package com.ns.paymentv5.ui.views.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.widget.Toast;

import com.ns.paymentv5.R;
import com.ns.paymentv5.databinding.PaymentLoginBinding;
import com.ns.paymentv5.models.PaymentDetail;
import com.ns.paymentv5.models.TransfersResponse;
import com.ns.paymentv5.ui.views.base.BaseActivity;
import com.ns.paymentv5.utils.Constants;
import com.ns.paymentv5.utils.PaymentConfiguration;
import com.ns.paymentv5.viewmodels.LoginVM;


public class PaymentLoginActivity extends BaseActivity {

    private ProgressDialog progressDialog;
    private LoginVM loginVM;
    private PaymentDetail paymentDetail;
    private int PAYMENT_HOME = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        paymentDetail = (PaymentDetail) intent.getSerializableExtra(PaymentConfiguration.PAYMENT_DETAIL);
        if (paymentDetail != null) {
            loginVM = new LoginVM();
            PaymentLoginBinding activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.payment_login);
            activityLoginBinding.setViewModel(loginVM);
            listening();
        } else {
            showError();
        }
    }

    private void showError() {

    }

    private void listening() {
        loginVM.getShowProgress().observe(this, show -> {
            if (show != null && show) {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
                    progressDialog.setCancelable(false);
                }
                progressDialog.show();
            } else {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }


            }
        });
        loginVM.getLoginSuccess().observe(this, success -> {
            Intent intent = new Intent(getBaseContext(), PaymentMainActivity.class);
            intent.putExtra(PaymentConfiguration.PAYMENT_DETAIL, paymentDetail);
            startActivityForResult(intent, PAYMENT_HOME);
        });
        loginVM.getLoginFail().observe(this, success -> {
            runOnUiThread(() -> {
                Toast.makeText(PaymentLoginActivity.this, getString(R.string.login_error), Toast.LENGTH_LONG).show();
            });
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_HOME) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    String result = extras.getString(PaymentConfiguration.PAYMENT_RESULT, "");
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(PaymentConfiguration.PAYMENT_RESULT, result);
                    returnIntent.putExtra(PaymentConfiguration.PAYMENT_TOKEN, Constants.TOKEN);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                onBackPressed();
            }
        }
    }
}
