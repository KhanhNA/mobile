package com.ns.paymentv5.api;

public class BaseURL {

    private static final String API_ENDPOINT = "demo.nextsolutions.com.vn:41019";
    private static final String API_PATH = "/payment-gateway/api/v1/";
    private static final String PROTOCOL_HTTPS = "http://";
    private static final String PROTOCOL_HTTP = "http://";

    public String getDefaultBaseUrl() {
        return PROTOCOL_HTTPS + API_ENDPOINT;
    }

    String getUrl(String endpoint) {
        return endpoint + API_PATH;
    }

}
