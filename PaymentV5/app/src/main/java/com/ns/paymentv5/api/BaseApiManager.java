package com.ns.paymentv5.api;

import com.ns.paymentv5.api.services.AuthenticationService;
import com.ns.paymentv5.api.services.TransfersService;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseApiManager {
    private static BaseApiManager baseApiManager;

    private BaseApiManager() {
        createService(new BaseURL().getDefaultBaseUrl(), "");
    }

    public static BaseApiManager getInstance() {
        if (baseApiManager == null) {
            baseApiManager = new BaseApiManager();
        }
        return baseApiManager;
    }

    private static Retrofit retrofit;

    private static AuthenticationService authenticationApi;
    private static TransfersService transfersService;

    private static <T> T createApi(Class<T> clazz) {
        return retrofit.create(clazz);
    }

    public static void createService(String endpoint, String authToken) {
        retrofit = new Retrofit.Builder()
                .baseUrl(new BaseURL().getUrl(endpoint))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(new SelfServiceOkHttpClient(authToken).getMifosOkHttpClient())
                .build();
        init();
    }

    private static void init() {
        authenticationApi = createApi(AuthenticationService.class);
        transfersService = createApi(TransfersService.class);
    }

    TransfersService getTransfersService() {
        return transfersService;
    }

    AuthenticationService getAuthenticationApi() {
        return authenticationApi;
    }
}
