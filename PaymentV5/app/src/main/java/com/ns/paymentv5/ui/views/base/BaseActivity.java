package com.ns.paymentv5.ui.views.base;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import android.widget.Toolbar;


public class BaseActivity extends AppCompatActivity {

    protected Toolbar toolbar;
    private ProgressDialog progress;
    /**
     * This method is use to provide back button feature in the toolbar of activities
     */
    protected void showBackButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void showToast(String message) {
        showToast(message, Toast.LENGTH_SHORT);
    }
    public void showToast(@NonNull String message, int toastType) {
        Toast.makeText(BaseActivity.this, message, toastType).show();
    }

}
