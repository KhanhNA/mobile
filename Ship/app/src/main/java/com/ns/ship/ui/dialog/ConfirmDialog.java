package com.ns.ship.ui.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ns.ship.R;
import com.ns.ship.databinding.DialogComfirmBinding;
import com.ns.ship.listener.DialogListener;
import com.ns.ship.model.DeliveryDetail;
import com.ns.ship.viewmodels.DialogConfirmViewModel;

import lombok.Setter;

@Setter
public class ConfirmDialog extends DialogFragment {
    private DeliveryDetail deliveryDetail;
    private DialogListener listener;
    private DialogConfirmViewModel viewModel;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogComfirmBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout. dialog_comfirm, null, false);
        viewModel = new DialogConfirmViewModel();
        binding.setViewmodel(viewModel);
        viewModel.getDeliveryDetailObservable().set(deliveryDetail);
        listen();
        return binding.getRoot();
    }

    private void listen() {
        viewModel.getCreateSuccess().observe(this, success -> {
            listener.onResult(200, viewModel.getDeliveryDetailObservable().get());
        });

    }
}
