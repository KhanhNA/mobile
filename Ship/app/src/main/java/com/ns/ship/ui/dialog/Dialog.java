package com.ns.ship.ui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ns.ship.ui.adapter.MyAdapterV2;
import com.ns.ship.R;
import com.ns.ship.model.Car;

import java.util.ArrayList;
import java.util.List;

public class Dialog extends DialogFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog, container, false);
        RecyclerView recyclerView = v.findViewById(R.id.my_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("Xe thùng mui bạt", R.drawable.v2));
        cars.add(new Car("Xe tải thùng kín", R.drawable.v2));

        cars.add(new Car("Xe đầu kéo container", R.drawable.v3));

        cars.add(new Car("Xe công lạnh", R.drawable.v4));
        cars.add(new Car("Xe tải đông lạnh", R.drawable.v3));
        cars.add(new Car("Xe siêu trường siêu trọng", R.drawable.v2));

        cars.add(new Car("Xe tải cẩu", R.drawable.v2));

        RecyclerView.Adapter mAdapter = new MyAdapterV2(cars);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        return v;
    }
}
