package com.ns.ship.listener;

public interface AdapterListener {
    void onItemClick(int code, Object object);
}
