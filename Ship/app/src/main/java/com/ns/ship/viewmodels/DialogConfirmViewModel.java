package com.ns.ship.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import com.ns.ship.model.DeliveryDetail;
import com.ns.ship.model.OrderDetail;
import com.ns.ship.utils.AppUtil;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Getter
@Setter
public class DialogConfirmViewModel extends ViewModel {
    private ObservableField<DeliveryDetail> deliveryDetailObservable = new ObservableField<>();
    private MutableLiveData<Boolean> createSuccess = new MutableLiveData<>();
    public void createOrderDelivery(){
        DeliveryDetail deliveryDetail = deliveryDetailObservable.get();

        AppUtil.getAPIService().saveOrder(deliveryDetail).enqueue(new Callback<DeliveryDetail>() {
            @Override
            public void onResponse(Call<DeliveryDetail> call, Response<DeliveryDetail> response) {
                if(response.isSuccessful()){
                    DeliveryDetail result = response.body();
                    createSuccess.setValue(true);
                }
            }

            @Override
            public void onFailure(Call<DeliveryDetail> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
