package com.ns.ship.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSize implements Serializable {
    private Double length;
    private Double width;
    private Double height;
    private Double weight;
}
