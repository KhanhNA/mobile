package com.ns.ship.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDetail implements Serializable {
    List<ProductSize> productSizes;
    private String toLocation;
    private String fromWarehose;
    private Double fromLongitude;
    private Double fromLatitude;
    private Double toLongitude;
    private Double toLatitude;


}
