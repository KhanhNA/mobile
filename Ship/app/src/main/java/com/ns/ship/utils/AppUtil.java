package com.ns.ship.utils;

import android.app.Application;

import com.ns.ship.utils.api.OrderDeliveryService;

public class AppUtil {
    private static String SERVER_URL = "http://demo.nextsolutions.com.vn:20998";
    public static OrderDeliveryService getAPIService() {

        return HttpHelper.getClient(SERVER_URL).create(OrderDeliveryService.class);
    }

}
