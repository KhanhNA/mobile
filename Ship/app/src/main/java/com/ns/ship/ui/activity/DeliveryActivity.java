package com.ns.ship.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.ns.ship.R;
import com.ns.ship.databinding.DeliveryActivityBinding;
import com.ns.ship.listener.AdapterListener;
import com.ns.ship.listener.DialogListener;
import com.ns.ship.model.Car;
import com.ns.ship.model.DeliveryDetail;
import com.ns.ship.model.OrderDetail;
import com.ns.ship.ui.adapter.MyAdapter;
import com.ns.ship.ui.dialog.ConfirmDialog;
import com.ns.ship.utils.DeliveryConfiguration;
import com.ns.ship.viewmodels.DeliveryViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DeliveryActivity extends AppCompatActivity implements AdapterListener, DialogListener {


    private DeliveryViewModel deliveryViewModel;
    private ConfirmDialog confirmDialog = new ConfirmDialog();
    public DeliveryActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DeliveryActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.delivery_activity);
        deliveryViewModel = new DeliveryViewModel();
        binding.setViewModel(deliveryViewModel);
        if (getIntent().hasExtra(DeliveryConfiguration.ORDER_DETAIL)) {
            OrderDetail orderDetail = (OrderDetail) getIntent().getSerializableExtra(DeliveryConfiguration.ORDER_DETAIL);
            // todo get thong tin
        }

        confirmDialog.setListener(this);
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        List<Car> cars = data();
        deliveryViewModel.setCar(cars.get(0));
        MyAdapter mAdapter = new MyAdapter(cars);
        mAdapter.setListener(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

        setOption();
    }


    private void setOption() {
        Button button = findViewById(R.id.book);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeliveryDetail deliveryDetail = deliveryViewModel.getDetailObservableField().get();
                if (deliveryDetail != null) {
                    // call API get money
                    deliveryDetail.setCost(10000.1);
                    deliveryDetail.setTimeEstimate(Calendar.getInstance().getTime());
                }
                confirmDialog.setDeliveryDetail(deliveryDetail);
                confirmDialog.show(getSupportFragmentManager(), "confirm");
            }
        });
    }

    private List<Car> data() {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("Xe thùng mui bạt", R.drawable.v2));
        cars.add(new Car("Xe tải thùng kín", R.drawable.v2));

        cars.add(new Car("Xe đầu kéo container", R.drawable.v3));

        cars.add(new Car("Xe công lạnh", R.drawable.v4));
        cars.add(new Car("Xe tải đông lạnh", R.drawable.v3));
        cars.add(new Car("Xe siêu trường siêu trọng", R.drawable.v2));

        cars.add(new Car("Xe tải cẩu", R.drawable.v2));
        return cars;
    }


    @Override
    public void onItemClick(int code, Object object) {
        Car carSelected = (Car) object;
        deliveryViewModel.setCar(carSelected);
    }

    @Override
    public void onResult(int code, Object object) {
        if (code == 200) {
            DeliveryDetail detail = (DeliveryDetail) object;
            if (detail !=null) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(DeliveryConfiguration.RESULT, detail);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        }
    }
}
