package com.ns.ship.utils.api;

import com.ns.ship.model.DeliveryDetail;
import com.ns.ship.model.OrderDetail;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.POST;

public interface OrderDeliveryService {
    @POST("/delivery")
    Call<DeliveryDetail> saveOrder(@Body DeliveryDetail orderDetail);
}
