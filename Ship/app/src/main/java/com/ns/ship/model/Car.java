package com.ns.ship.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Car implements Serializable {
    private String name;
    private int src;
    private String description;
    private Double maxWeight;
    private Double length;
    private Double width;
    private Double height;

    public Car(String name, int src) {
        this.name = name;
        this.src = src;
    }
}
