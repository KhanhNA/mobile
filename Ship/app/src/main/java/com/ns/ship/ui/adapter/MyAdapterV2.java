package com.ns.ship.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ns.ship.R;
import com.ns.ship.model.Car;

import java.util.List;

public class MyAdapterV2 extends RecyclerView.Adapter<MyAdapterV2.ViewHolder> {
    private List<Car> cars;

    public MyAdapterV2(List<Car> cars) {
        this.cars = cars;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.dialog_item, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView, cars.get(i).getName());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Car car = cars.get(position);

        // Set item views based on your views and data model
        TextView textView = viewHolder.nameTextView;
        ImageView imageView = viewHolder.imageView;
        imageView.setImageResource(car.getSrc());
        textView.setText(car.getName());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        // Handle key up and key down and attempt to move selection
        recyclerView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();

                // Return false if scrolled to the bounds and allow focus to move off the list
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
//                        return tryMoveSelection(lm, 1);
                        Log.e("NMQ", "as");
                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
//                        return tryMoveSelection(lm, -1);
                        Log.e("NMQ", "BCDED");
                    }
                }

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return cars == null ? 0 : cars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public ImageView imageView;
        public LinearLayout linearLayout;
        public ViewHolder(View itemView, String car) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.text);
            imageView = itemView.findViewById(R.id.image1);
            linearLayout = itemView.findViewById(R.id.view);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("NMQ", "NMQ");
                }
            });
        }
    }
}
