package com.ns.ship.listener;

public interface DialogListener {
    void onResult(int code, Object object);
}
