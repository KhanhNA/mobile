package com.ns.ship.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ns.ship.R;
import com.ns.ship.listener.AdapterListener;
import com.ns.ship.model.Car;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Car> cars;
    private AdapterListener listener;

    public LinearLayout linearLayoutSelected;

    public void setListener(AdapterListener listener) {
        this.listener = listener;
    }

    public MyAdapter(List<Car> cars) {
        this.cars = cars;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.recycler_view_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView, cars.get(i).getName());
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Car car = cars.get(position);

        // Set item views based on your views and data model
        TextView textView = viewHolder.nameTextView;
        ImageView imageView = viewHolder.imageView;
        imageView.setImageResource(car.getSrc());
        textView.setText(car.getName());
    }

//    @Override
//    public void onAttachedToRecyclerView(@NonNull final RecyclerView recyclerView) {
//        super.onAttachedToRecyclerView(recyclerView);
//        // Handle key up and key down and attempt to move selection
//        recyclerView.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();
//
//                // Return false if scrolled to the bounds and allow focus to move off the list
//                if (event.getAction() == KeyEvent.ACTION_DOWN) {
//                    if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
////                        return tryMoveSelection(lm, 1);
//                        Log.e("NMQ", "as");
//                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
////                        return tryMoveSelection(lm, -1);
//                        Log.e("NMQ", "BCDED");
//                    }
//                }
//
//                return false;
//            }
//        });
//    }

    @Override
    public int getItemCount() {
        return cars == null ? 0 : cars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public ImageView imageView;
        public LinearLayout linearLayout;

        public ViewHolder(View itemView, String car) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.text);
            imageView = itemView.findViewById(R.id.image1);
            linearLayout = itemView.findViewById(R.id.border);

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (linearLayout != null) {
                        linearLayout.setBackgroundResource(R.drawable.border_selected);
                        if (linearLayoutSelected == null) {
                            linearLayoutSelected = linearLayout;
                        } else {
                            linearLayoutSelected.setBackgroundResource(R.drawable.border);
                        }
                        linearLayoutSelected = linearLayout;
                        listener.onItemClick(200, cars.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
