package com.ns.ship.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliveryDetail implements Serializable {
    private Car car;
    private Double cost;
    private Date timeEstimate;

    private Boolean vatInvoice = false;
    private Boolean fastDelivery = false;
    private Boolean cargoInsurance = false;
    private Boolean infoConfidential = false;

    public Boolean getVatInvoice() {
        return vatInvoice == null ? false : vatInvoice;
    }

    public Boolean getFastDelivery() {
        return fastDelivery == null ? false : fastDelivery;
    }

    public Boolean getCargoInsurance() {
        return cargoInsurance == null ? false : cargoInsurance;
    }

    public Boolean getInfoConfidential() {
        return infoConfidential == null ? false : infoConfidential;
    }
}
