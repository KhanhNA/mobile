package com.ns.ship.viewmodels;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import com.ns.ship.model.Car;
import com.ns.ship.model.DeliveryDetail;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliveryViewModel extends ViewModel {
    ObservableField<DeliveryDetail> detailObservableField = new ObservableField<>(new DeliveryDetail());
    public void checkVatInvoice(){
        DeliveryDetail deliveryDetail = detailObservableField.get();

        if(deliveryDetail!=null){
            if(deliveryDetail.getVatInvoice()==null){
                deliveryDetail.setVatInvoice(true);
            }
            else{
                deliveryDetail.setVatInvoice(!deliveryDetail.getVatInvoice());
            }
        }
    }
    public void checkCargoInsurance(){
        DeliveryDetail deliveryDetail = detailObservableField.get();

        if(deliveryDetail!=null){
            if(deliveryDetail.getCargoInsurance()==null){
                deliveryDetail.setCargoInsurance(true);
            }
            else{
                deliveryDetail.setCargoInsurance(!deliveryDetail.getCargoInsurance());
            }
        }
    }
    public void checkInfoConfidential(){
        DeliveryDetail deliveryDetail = detailObservableField.get();

        if(deliveryDetail!=null){
            if(deliveryDetail.getInfoConfidential()==null){
                deliveryDetail.setInfoConfidential(true);
            }
            else{
                deliveryDetail.setInfoConfidential(!deliveryDetail.getInfoConfidential());
            }
        }
    }
    public void checkFastDelivery(){
        DeliveryDetail deliveryDetail = detailObservableField.get();

        if(deliveryDetail!=null){
            if(deliveryDetail.getFastDelivery()==null){
                deliveryDetail.setFastDelivery(true);
            }
            else{
                deliveryDetail.setFastDelivery(!deliveryDetail.getFastDelivery());
            }
        }
    }

    public void setCar(Car carSelected){
        DeliveryDetail deliveryDetail = detailObservableField.get();
        if(deliveryDetail!=null){
            deliveryDetail.setCar(carSelected);
        }
    }

    public void calculatorCost(){

    }
}
