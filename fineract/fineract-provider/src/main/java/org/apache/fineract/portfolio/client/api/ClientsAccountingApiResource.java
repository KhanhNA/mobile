/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.fineract.portfolio.client.api;

import io.swagger.annotations.*;
import io.swagger.util.Json;
import org.apache.fineract.commands.domain.CommandWrapper;
import org.apache.fineract.commands.service.CommandWrapperBuilder;
import org.apache.fineract.commands.service.PortfolioCommandSourceWritePlatformService;
import org.apache.fineract.infrastructure.bulkimport.service.BulkImportWorkbookPopulatorService;
import org.apache.fineract.infrastructure.bulkimport.service.BulkImportWorkbookService;
import org.apache.fineract.infrastructure.core.api.ApiRequestParameterHelper;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.core.serialization.ToApiJsonSerializer;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.accountdetails.data.AccountSummaryCollectionData;
import org.apache.fineract.portfolio.accountdetails.service.AccountDetailsReadPlatformService;
import org.apache.fineract.portfolio.client.data.ClientData;
import org.apache.fineract.portfolio.client.service.ClientReadPlatformService;
import org.apache.fineract.portfolio.loanaccount.guarantor.service.GuarantorReadPlatformService;
import org.apache.fineract.portfolio.savings.service.SavingsAccountReadPlatformService;
import org.apache.fineract.useradministration.data.RoleData;
import org.apache.fineract.useradministration.service.RoleReadPlatformService;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Path("/clientsAccounting")
@Component
@Scope("singleton")
@Api(value = "Client", description = "Clients are people and businesses that have applied (or may apply) to an MFI for loans.\n" + "\n" + "Clients can be created in Pending or straight into Active state.")
public class ClientsAccountingApiResource {
    public static final String ROLE_USER_STR = "User";

    private final PlatformSecurityContext context;
    private final ClientReadPlatformService clientReadPlatformService;
    private final ToApiJsonSerializer<ClientData> toApiJsonSerializer;
    private final ToApiJsonSerializer<AccountSummaryCollectionData> clientAccountSummaryToApiJsonSerializer;
    private final ApiRequestParameterHelper apiRequestParameterHelper;
    private final PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService;
    private final AccountDetailsReadPlatformService accountDetailsReadPlatformService;
    private final SavingsAccountReadPlatformService savingsAccountReadPlatformService;
    private final BulkImportWorkbookService bulkImportWorkbookService;
    private final BulkImportWorkbookPopulatorService bulkImportWorkbookPopulatorService;
    private final GuarantorReadPlatformService guarantorReadPlatformService;
    private final RoleReadPlatformService roleReadPlatformService;
    private static HashMap<Integer, Object> mapProductByClientTypeID;

    static {
        mapProductByClientTypeID = new HashMap<>();
        mapProductByClientTypeID.put(1, new Integer[]{4});//L1
        mapProductByClientTypeID.put(2, new Integer[]{});//NPP
    }


    @Autowired
    public ClientsAccountingApiResource(final PlatformSecurityContext context, final ClientReadPlatformService readPlatformService,
                                        final ToApiJsonSerializer<ClientData> toApiJsonSerializer,
                                        final ToApiJsonSerializer<AccountSummaryCollectionData> clientAccountSummaryToApiJsonSerializer,
                                        final ApiRequestParameterHelper apiRequestParameterHelper,
                                        final PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService,
                                        final AccountDetailsReadPlatformService accountDetailsReadPlatformService,
                                        final SavingsAccountReadPlatformService savingsAccountReadPlatformService,
                                        final BulkImportWorkbookPopulatorService bulkImportWorkbookPopulatorService,
                                        final BulkImportWorkbookService bulkImportWorkbookService,
                                        final GuarantorReadPlatformService guarantorReadPlatformService,
                                        final RoleReadPlatformService roleReadPlatformService) {
        this.context = context;
        this.clientReadPlatformService = readPlatformService;
        this.toApiJsonSerializer = toApiJsonSerializer;
        this.clientAccountSummaryToApiJsonSerializer = clientAccountSummaryToApiJsonSerializer;
        this.apiRequestParameterHelper = apiRequestParameterHelper;
        this.commandsSourceWritePlatformService = commandsSourceWritePlatformService;
        this.accountDetailsReadPlatformService = accountDetailsReadPlatformService;
        this.savingsAccountReadPlatformService = savingsAccountReadPlatformService;
        this.bulkImportWorkbookPopulatorService = bulkImportWorkbookPopulatorService;
        this.bulkImportWorkbookService = bulkImportWorkbookService;
        this.guarantorReadPlatformService = guarantorReadPlatformService;
        this.roleReadPlatformService = roleReadPlatformService;
    }


    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Create a Client", httpMethod = "POST", notes = "Note:\n\n" + "1. You can enter either:firstname/middlename/lastname - for a person (middlename is optional) OR fullname - for a business or organisation (or person known by one name).\n" + "\n" + "2.If address is enable(enable-address=true), then additional field called address has to be passed.\n\n" + "Mandatory Fields: firstname and lastname OR fullname, officeId, active=true and activationDate OR active=false, if(address enabled) address\n\n" + "Optional Fields: groupId, externalId, accountNo, staffId, mobileNo, savingsProductId, genderId, clientTypeId, clientClassificationId")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "body", dataType = "ClientData", required = true, type = "body", dataTypeClass = ClientsApiResourceSwagger.PostClientsAccountingRequest.class)})
    @ApiResponses({@ApiResponse(code = 200, message = "OK", response = ClientsApiResourceSwagger.PostClientsAccountingResponse.class)})
    //public String create(@ApiParam(hidden = true) final String apiRequestBodyAsJson, String abc ) {
    @Transactional(rollbackFor = Exception.class)

    public String create(final JSONObject apiRequestBodyAsJsons) throws Exception{

         CommandProcessingResult resultClient = null, resultSavings;
         CommandWrapper commandRequest; //
//        try {
            //1. tao client
            JSONObject obj0 = apiRequestBodyAsJsons;
            String username = obj0.getString("username");
            String email = obj0.getString("email");
            String firstname = obj0.getString("firstname");
            String activationDate = obj0.getString("activationDate");
            Integer clientTypeId = obj0.getInt("clientTypeId");
            obj0.remove("username");
            obj0.remove("email");
            obj0.remove("clientTypeId");
//            obj0.remove("firstname");
            commandRequest = new CommandWrapperBuilder() //
                    .createClient() //
                    .withJson(obj0.toString()) //
                    .build();


//            String user = String.format("{\"username\":%s, \"firstname\":%s," +
//                    "\"lastname\": \"%s\"," +
//                    "\"email\": %s,",
//                "officeId": 1,
//                "staffId": 1,
//                "roles": [2,3],
//        "sendPasswordToEmail": true, + obj0.getString("username")
        resultClient = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);

        Long clientId = resultClient.getClientId();

        // Only set role User when create new user
        Collection<RoleData> roleData = roleReadPlatformService.retrieveAll();
        JSONArray roles = new JSONArray();
        for (RoleData role : roleData) {
            if (ROLE_USER_STR.equals(role.getName())) {
                roles.put(role.getId());
                break;
            }
        }

        //2. tao user
        JSONObject objUser = new JSONObject();
        objUser.put("username", username);
        objUser.put("lastname", obj0.get("lastname"));
        objUser.put("firstname", firstname);

        objUser.put("email", email);
        objUser.put("officeId", obj0.get("officeId"));
        objUser.put("password", "123113");
        objUser.put("repeatPassword", "123113");

        objUser.put("roles", roles);
        objUser.put("sendPasswordToEmail", false);

        commandRequest = new CommandWrapperBuilder()
                .createUser()
                .withJson(objUser.toString())
                .build();
        this.commandsSourceWritePlatformService.logCommandSource(commandRequest);

        Object locale = obj0.get("locale");
        Object dateFormat = obj0.get("dateFormat");
        Object now = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        JSONObject objProduct = new JSONObject();
        objProduct.put("clientId", clientId);

        
        objProduct.put("locale", locale);
        objProduct.put("dateFormat", dateFormat);
        objProduct.put("submittedOnDate", now);

        JSONObject objApprove = new JSONObject();
        objApprove.put("locale", locale);
        objApprove.put("dateFormat", dateFormat);
        objApprove.put("approvedOnDate", now);

        JSONObject objActivation = new JSONObject();
        objActivation.put("locale", locale);
        objActivation.put("dateFormat", dateFormat);
        objActivation.put("activatedOnDate", now);

        //3. tao product
        Integer[] productIds = (Integer[]) (mapProductByClientTypeID.get(clientTypeId));
        for (Integer productId : productIds) {
            objProduct.put("productId", productId);
            commandRequest = new CommandWrapperBuilder() //
                    .createSavingsAccount()
                    .withJson(objProduct.toString()) //
                    .build();
            resultSavings = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);
            commandRequest = new CommandWrapperBuilder().withJson(objApprove.toString()).approveSavingsAccountApplication(resultSavings.getSavingsId()).build();
            this.commandsSourceWritePlatformService.logCommandSource(commandRequest);
            commandRequest = new CommandWrapperBuilder().withJson(objActivation.toString()).savingsAccountActivation(resultSavings.getSavingsId()).build();
            this.commandsSourceWritePlatformService.logCommandSource(commandRequest);
        }
        return this.toApiJsonSerializer.serialize(resultClient);
    }


}
