/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.fineract.batch.command.internal;

import org.apache.fineract.batch.command.CommandStrategy;
import org.apache.fineract.batch.domain.BatchRequest;
import org.apache.fineract.batch.domain.BatchResponse;
import org.apache.fineract.batch.exception.ErrorHandler;
import org.apache.fineract.batch.exception.ErrorInfo;
import org.apache.fineract.portfolio.loanaccount.rescheduleloan.api.RescheduleLoansApiResource;
import org.apache.fineract.portfolio.savings.api.SavingsAccountTransactionsApiResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.UriInfo;

@Component
public class SavingsAccountsTransactionsCommandStrategy implements CommandStrategy {

    private final SavingsAccountTransactionsApiResource savingsAccountTransactionsApiResource;

    @Autowired
    public SavingsAccountsTransactionsCommandStrategy(final SavingsAccountTransactionsApiResource savingsAccountTransactionsApiResource) {
        this.savingsAccountTransactionsApiResource = savingsAccountTransactionsApiResource;
    }

    @Override
    public BatchResponse execute(BatchRequest request, UriInfo uriInfo) {
        final BatchResponse response = new BatchResponse();
        final String responseBody;

        response.setRequestId(request.getRequestId());
        response.setHeaders(request.getHeaders());

        // Try-catch blocks to map exceptions to appropriate status codes
        try {
            final String[] pathParameters = request.getRelativeUrl().split("/");
            Long savingsId = Long.parseLong(pathParameters[1]);
            String command = request.getRelativeUrl().substring(request.getRelativeUrl().indexOf("command=")+8);

            // Calls 'transaction' function from 'savingsAccountTransactionsApiResource' to create a
            // transaction
            responseBody = savingsAccountTransactionsApiResource.transaction(savingsId, command, request.getBody());

            response.setStatusCode(200);
            // Sets the body of the response after the successful request
            response.setBody(responseBody);

        } catch (RuntimeException e) {

            // Gets an object of type ErrorInfo, containing information about
            // raised exception
            ErrorInfo ex = ErrorHandler.handler(e);

            response.setStatusCode(ex.getStatusCode());
            response.setBody(ex.getMessage());
        }

        return response;
    }

}
