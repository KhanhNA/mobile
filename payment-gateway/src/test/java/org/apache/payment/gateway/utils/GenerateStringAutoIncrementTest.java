package org.apache.payment.gateway.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.payment.gateway.dto.mytelpay.PayRequest;
import org.junit.Test;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class GenerateStringAutoIncrementTest {

    @Test
    public void given0000ThenReturn0001() {
        assertEquals("0001", AutoIncrementStringGenerator.generate("0000"));
        assertEquals("ZZZB", AutoIncrementStringGenerator.generate("zzza"));
        assertEquals("0020", AutoIncrementStringGenerator.generate("001z"));
    }

    @Test
    public void name() throws JsonProcessingException {
        PayRequest payRequest = new PayRequest();
        // convert from Object to Json Formatted String.
//        ObjectMapper objectMapper = new ObjectMapper();
//        String strPayRequest = objectMapper.writeValueAsString(payRequest);
//        String seceretKey = "0099876543duabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//        String jwt = createJWT("279a4d4b-9048-47b3-a344-49382e61d7b7", "mytelpay", strPayRequest, 600_000, seceretKey);
//        System.out.println("jwt:" + jwt);
    }
}
