package org.apache.payment.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "fineract")
@Data
public class FineractConfiguration {
    private String protocol = "https";
    private String host = "localhost";
    private int port = 8443;
    private String basePath = "https://localhost:8443/fineract-provider/api/v1";
    private String superUsername = "mifos";
    private String password = "password";
}
