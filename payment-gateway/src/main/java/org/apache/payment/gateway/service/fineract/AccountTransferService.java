package org.apache.payment.gateway.service.fineract;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.payment.gateway.dto.mytelpay.PayRequest;
import org.apache.payment.gateway.dto.request.TransferRequest;

import java.io.IOException;

public interface AccountTransferService {
    Object transfer(String token, TransferRequest request) throws IOException;

    String sendRequestToMytelPay(PayRequest payload) throws JsonProcessingException;
}
