package org.apache.payment.gateway.repository.beyonic;

import org.apache.payment.gateway.config.hibernate.AbstractBaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sanyam Goel created on 24/7/18
 */
@Repository
public class PaymentRepository extends AbstractBaseRepository {
}
