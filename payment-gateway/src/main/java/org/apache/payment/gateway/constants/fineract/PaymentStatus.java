package org.apache.payment.gateway.constants.fineract;

public enum PaymentStatus {
    INITIALIZED(1),
    PROCESSED(2),
    PROCESSED_WITH_ERRORS(3),
    REJECTED(4),
    CANCELLED(5);

    private final int value;

    PaymentStatus(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
