package org.apache.payment.gateway.controllers.error;

public class PaymentGatewayNotFoundException extends RuntimeException {
    public PaymentGatewayNotFoundException(String message) {
        super(message);
    }
}
