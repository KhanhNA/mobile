package org.apache.payment.gateway.constants.fineract;

public interface FineractContstants {
    String LOCALE = "en";
    String DATE_FORMAT = "yyyy-MM-dd";
}
