package org.apache.payment.gateway.dto.request.validators.impl;

import org.apache.payment.gateway.config.properties.MytelPayConfiguration;
import org.apache.payment.gateway.dto.request.validators.MytelPayPartnerId;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MytelPayPartnerIdValidator implements ConstraintValidator<MytelPayPartnerId, String> {

   private final MytelPayConfiguration mytelPayConfiguration;

   public MytelPayPartnerIdValidator(MytelPayConfiguration mytelPayConfiguration) {
      this.mytelPayConfiguration = mytelPayConfiguration;
   }

   public boolean isValid(String value, ConstraintValidatorContext context) {
      return value.equals(mytelPayConfiguration.getPartnerId());
   }
}
