package org.apache.payment.gateway.dto.request;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class MytelPayConfirmPayload {

    private String mytpTransId;

    @Valid
    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    private PayRequest payRequest;

    private String responseDate;

    private Integer statusCode;

    private String statusMessage;
}

