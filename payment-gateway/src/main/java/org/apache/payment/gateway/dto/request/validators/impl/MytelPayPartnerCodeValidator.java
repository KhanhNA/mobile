package org.apache.payment.gateway.dto.request.validators.impl;

import org.apache.payment.gateway.config.properties.MytelPayConfiguration;
import org.apache.payment.gateway.dto.request.validators.MytelPayPartnerCode;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MytelPayPartnerCodeValidator implements ConstraintValidator<MytelPayPartnerCode, String> {

   private final MytelPayConfiguration mytelPayConfiguration;

   public MytelPayPartnerCodeValidator(MytelPayConfiguration mytelPayConfiguration) {
      this.mytelPayConfiguration = mytelPayConfiguration;
   }

   public boolean isValid(String value, ConstraintValidatorContext context) {
      return value.equals(mytelPayConfiguration.getPartnerCode());
   }
}
