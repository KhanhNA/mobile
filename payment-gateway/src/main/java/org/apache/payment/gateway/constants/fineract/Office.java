package org.apache.payment.gateway.constants.fineract;

public enum Office {
    HEAD_OFFICE(1),
    DISTRIBUTOR(2),
    MERCHANT(3);

    Office(int id) {
        this.id = id;
    }

    private int id;

    public int value() {
        return id;
    }
}
