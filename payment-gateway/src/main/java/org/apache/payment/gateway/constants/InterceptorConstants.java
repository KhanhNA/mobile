package org.apache.payment.gateway.constants;

/**
 * @author Rahul Goel created on 2/6/18
 */
public class InterceptorConstants {
    public static final String INTERCEPTOR_PATTERNS = "/payment-gateway/api/v1/**";
}
