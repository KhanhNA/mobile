package org.apache.payment.gateway.dto.fineract.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LoyaltyAccount extends Account {
    private Long totalPoint;
}
