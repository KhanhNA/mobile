package org.apache.payment.gateway.dto.fineract.request;

import lombok.Builder;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Builder
public class SavingAccountRequestBuilder {
    private Long externalId;
    private Long productId;
    private final Boolean allowOverdraft = false;
    private final Object[] charges = {};
    private final Integer clientId = 1;
    private final String dateFormat = "yyyy-MM-dd";
    private final Boolean enforceMinRequiredBalance = false;
    private final Integer interestCalculationDaysInYearType = 365;
    private final Integer interestCalculationType = 1;
    private final Integer interestCompoundingPeriodType = 1;
    private final Integer interestPostingPeriodType = 4;
    private final String locale = "en";
    private final String monthDayFormat = "dd-MM";
    private final Integer nominalAnnualInterestRate = 0;
    private final String submittedOnDate = new SimpleDateFormat(dateFormat).format(new Date());
    private final Boolean withHoldTax = false;
    private final Boolean withdrawalFeeForTransfers = false;
}
