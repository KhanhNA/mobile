package org.apache.payment.gateway.dto.request.validators;

import org.apache.payment.gateway.dto.request.validators.impl.MytelPayServiceCodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = MytelPayServiceCodeValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MytelPayServiceCode {
    String message() default "{pg.validation.constraints.MytelPayServiceCode.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
