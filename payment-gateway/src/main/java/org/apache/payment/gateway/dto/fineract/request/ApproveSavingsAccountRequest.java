package org.apache.payment.gateway.dto.fineract.request;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class ApproveSavingsAccountRequest {
    private final String locale;
    private final String dateFormat ;
    private final String approvedOnDate;

    public ApproveSavingsAccountRequest() {
        locale = "en";
        dateFormat = "dd/MM/yyyy";
        approvedOnDate = new SimpleDateFormat(dateFormat).format(new Date());
    }
}
