package org.apache.payment.gateway.dto.fineract.response;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ClientResponseModel {
    private Long id;
    private String accountNo;
    private String externalId;
    private String firstname;
    private String lastname;
    private String displayName;
    private String officeId;
    private String officeName;
    private Long savingsAccountId;
    private Boolean active;
    private LocalDate activationDate;
}
