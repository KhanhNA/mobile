package org.apache.payment.gateway.dto.fineract.request;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Builder
@Getter
public class PostDepositTransferRequestBuilder {
    private final String dateFormat = "yyyy/MM/dd";
    private final String transactionDate = new SimpleDateFormat(dateFormat).format(new Date());
    private String locale = "en";
    private Integer paymentTypeId = 1;
    private BigDecimal transactionAmount;
}
