package org.apache.payment.gateway.dto.mytelpay;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PayRequest {
    Double amount = 30d; // TODO: so tien nap
    String cancelUrlCallback = "https://www.youtube.com/?gl=VN";
    String dateTime = "2020-03-25 13:37:05"; // TODO: ngay nap
    String extTransRefId = "extTransRefId-20200304-160800";
    String failUrlCallback = "https://www.youtube.com/?gl=VN";
    String orderDescription = "order-description-2020-String ";
    String orderId = "orderId-2020-String"; // TODO: accountId
    String partnerId = "279a4d4b-9048-47b3-a344-49382e61d7b7";
    String phoneNumber = "+84389397968";
    String remark = "string remarkkk ";
    String serviceCode = "MFUNCTIONS_PAYMENT_WEBVIEW";
    String successUrlCallback = "https://www.google.com/";
    String walletPaymentStatusCallbackUrl = "http://localhost:9999/wallet-payment-status-url"; // TODO: callback de tra cuu trong tai khoan co bao nhieu
}
