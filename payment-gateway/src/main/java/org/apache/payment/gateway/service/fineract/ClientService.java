package org.apache.payment.gateway.service.fineract;

import org.apache.payment.gateway.dto.fineract.request.ClientRequestBuilder;
import org.apache.payment.gateway.dto.fineract.response.ClientResponseModel;
import org.apache.payment.gateway.dto.fineract.response.FineractResponseModel;

import java.io.IOException;
import java.util.Map;

public interface ClientService {
    ClientResponseModel getClient(Long id, String authorizationToken);

    FineractResponseModel createClient(ClientRequestBuilder requestBuilder, String authorizationToken);

    FineractResponseModel updateClient(Long id, ClientRequestBuilder requestBuilder, String authorizationToken);

    Object getClientAccounts(Long clientId, String authorizationToken) throws IOException;

    Object deposit(Long accountId, Double transactionAmount);

    FineractResponseModel closeClient(Long id, String token);

    FineractResponseModel reactivateClient(Long id, String token);
}
