package org.apache.payment.gateway.dto.request.validators;

import org.apache.payment.gateway.dto.request.validators.impl.MytelPayPartnerIdValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = MytelPayPartnerIdValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MytelPayPartnerId {
    String message() default "{pg.validation.constraints.MytelPayPartnerId.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
