package org.apache.payment.gateway.controllers.mytelpay;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.payment.gateway.config.aspect.Loggable;
import org.apache.payment.gateway.controllers.error.ApiError;
import org.apache.payment.gateway.dto.request.MytelPayConfirmPayload;
import org.apache.payment.gateway.dto.request.PayRequest;
import org.apache.payment.gateway.service.fineract.ClientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/helper")
@RequiredArgsConstructor
public class PaymentConfirmController {

    private final ClientService clientService;

    @Loggable
    @PostMapping("/pi/walletConfirm")
    public ResponseEntity<?> payConfirm(@Valid @RequestBody MytelPayConfirmPayload payload) {
        PayRequest payRequest = payload.getPayRequest();
        try {
            clientService.deposit(Long.valueOf(payRequest.getOrderId()), payRequest.getAmount());
            payload.setStatusCode(0);
            payload.setStatusMessage("Success");
            return ResponseEntity.ok(payload);
        } catch (HttpServerErrorException | HttpClientErrorException e) {
            payload.setStatusCode(e.getRawStatusCode());
            payload.setStatusMessage(e.getResponseBodyAsString());
            return ResponseEntity.status(e.getStatusCode()).body(payload);
        } catch (Exception e) {
            payload.setStatusCode(500);
            payload.setStatusMessage(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(payload);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    public MytelPayConfirmPayload handleMethodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex, HttpServletRequest request) throws IOException {
        BindingResult bindingResult = ex.getBindingResult();
        MytelPayConfirmPayload mytelPayConfirmPayload = (MytelPayConfirmPayload) bindingResult.getTarget();
        if(Objects.isNull(mytelPayConfirmPayload))
            mytelPayConfirmPayload = new MytelPayConfirmPayload();

        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), request.getRequestURI(), "Bad Request for validation failed cases");
        Map<String, Object> fieldErrors = new HashMap<>();
        for (FieldError error : bindingResult.getFieldErrors())
            fieldErrors.put(error.getField(), error.getDefaultMessage());

        apiError.setFieldErrorValidator(fieldErrors);

        ObjectMapper objectMapper = new ObjectMapper();
        String message = objectMapper.writeValueAsString(apiError);
        mytelPayConfirmPayload.setStatusMessage(message);
        mytelPayConfirmPayload.setStatusCode(400);
        return mytelPayConfirmPayload;
    }

}
