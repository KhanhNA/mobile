package org.apache.payment.gateway.dto.request;

import lombok.Data;
import org.apache.payment.gateway.dto.request.validators.MytelPayPartnerCode;
import org.apache.payment.gateway.dto.request.validators.MytelPayPartnerId;
import org.apache.payment.gateway.dto.request.validators.MytelPayServiceCode;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class PayRequest {

    private Double cashback;

    private Double discount;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @MytelPayPartnerCode
    private String partnerCode;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @MytelPayPartnerId
    private String partnerId;

//    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
//    @Pattern(regexp = "[0-9]+", message = "{pg.validation.constraints.Number.message}")
    private String extTransRefId;

    private String phoneNumber;

    private Double amount;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @Pattern(regexp = "[0-9]+", message = "{pg.validation.constraints.Number.message}")
    private String orderId;

    private String orderDescription;

    private String walletPaymentStatusCallbackUrl;

    private String successUrlCallback;

    private String failUrlCallback;

    private String cancelUrlCallback;

    private String dateTime;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @MytelPayServiceCode
    private String serviceCode;

    private String remark;
}
