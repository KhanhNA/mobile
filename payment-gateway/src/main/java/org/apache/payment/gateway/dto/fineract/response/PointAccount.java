package org.apache.payment.gateway.dto.fineract.response;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PointAccount extends Account {
    private Long totalPoint;

    @Builder
    public PointAccount(Long accountId, Long clientId, Long productId, String productName, String accountNo, String currencyCode, String currencyName, Long totalPoint) {
        super(accountId, clientId, productId, productName, accountNo, currencyCode, currencyName);
        this.totalPoint = totalPoint;
    }
}
