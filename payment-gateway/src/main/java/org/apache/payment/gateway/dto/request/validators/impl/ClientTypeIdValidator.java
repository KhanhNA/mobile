package org.apache.payment.gateway.dto.request.validators.impl;

import lombok.RequiredArgsConstructor;
import org.apache.payment.gateway.config.properties.PaymentGatewayClientTypeConfiguration;
import org.apache.payment.gateway.dto.request.validators.ClientTypeId;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
public class ClientTypeIdValidator implements ConstraintValidator<ClientTypeId, Integer> {

    private final PaymentGatewayClientTypeConfiguration paymentGatewayClientTypeConfiguration;

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        if (value == null)
            return true;
        return value == paymentGatewayClientTypeConfiguration.getMerchant() ||
                value == paymentGatewayClientTypeConfiguration.getDistributor();
    }

}
