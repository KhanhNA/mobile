package org.apache.payment.gateway.dto.fineract.request;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class TransferDetailsRequestBuilder {
    private Long toNPPClientId;
    private BigDecimal amount;
    private Integer productId;

    @Builder
    public TransferDetailsRequestBuilder(Long toNPPClientId, BigDecimal amount, Integer productId) {
        this.toNPPClientId = toNPPClientId;
        this.amount = amount;
        this.productId = productId;
    }
}
