package org.apache.payment.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "payment-gateway.client-type")
@Data
public class PaymentGatewayClientTypeConfiguration {
    private int merchant = 1;
    private int distributor = 2;
}
