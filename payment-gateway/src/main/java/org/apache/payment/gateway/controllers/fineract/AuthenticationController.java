package org.apache.payment.gateway.controllers.fineract;

import lombok.RequiredArgsConstructor;
import org.apache.payment.gateway.config.aspect.Loggable;
import org.apache.payment.gateway.config.properties.FineractApiConfiguration;
import org.apache.payment.gateway.dto.fineract.request.AuthenticationRequest;
import org.apache.payment.gateway.utils.HttpHeaderUtil;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.nio.charset.Charset;
import java.util.Objects;

@RestController
@RequestMapping("/payment-gateway/api/v1")
@RequiredArgsConstructor
public class AuthenticationController {

    private final RestTemplate restTemplate;
    private final FineractApiConfiguration fineractApiConfiguration;

    @Loggable
    @PostMapping("/authentication")
    public ResponseEntity<Object> authenticate(@Valid @RequestBody AuthenticationRequest body) {
        try {
            ResponseEntity<Object> response = this.restTemplate.exchange(
                    getAuthenticateUri(body.getUsername(), body.getPassword()),
                    HttpMethod.POST,
                    HttpHeaderUtil.getFineractEntityInstance(),
                    Object.class);
            return ResponseEntity.ok(Objects.requireNonNull(response.getBody()));
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (e.getStatusCode().is4xxClientError()) {
                throw new HttpClientErrorException(
                        e.getStatusCode(),
                        e.getStatusText(),
                        e.getResponseBodyAsByteArray(),
                        Charset.defaultCharset()
                );
            }
            throw e;
        }
    }

    private String getAuthenticateUri(String username, String password) {
        return UriComponentsBuilder.fromUriString(fineractApiConfiguration.getAuthentication())
                .queryParam("username", username)
                .queryParam("password", password)
                .toUriString();
    }
}
