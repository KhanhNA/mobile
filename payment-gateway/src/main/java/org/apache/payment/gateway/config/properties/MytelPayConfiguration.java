package org.apache.payment.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "mytel-pay")
@Data
public class MytelPayConfiguration {
    private String partnerId = "279a4d4b-9048-47b3-a344-49382e61d7b7";
    private String partnerCode = "MFUNCTIONS";
    private String serviceId = "b6012401-8a21-492a-bd1b-22ea26278073";
    private String serviceCode = "MFUNCTIONS_PAYMENT_WEBVIEW";
    private String secretKey = "0099876543duabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private String serviceName = "MFUNCTIONS";
    private String issuer = "mytelpay";
    private long ttlMillis = 600_000;
    private String walletConfirmPath = "/helper/pi/walletConfirm";
    private String payRequestPath = "https://pay.mytel.com.mm/payRequest/#!/request";
}
