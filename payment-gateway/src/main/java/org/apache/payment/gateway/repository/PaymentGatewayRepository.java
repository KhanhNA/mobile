package org.apache.payment.gateway.repository;

import org.apache.payment.gateway.config.hibernate.AbstractBaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Rahul Goel created on 3/6/18
 */

@Repository
public class PaymentGatewayRepository extends AbstractBaseRepository {
}
