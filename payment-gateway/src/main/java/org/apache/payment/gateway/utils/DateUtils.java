package org.apache.payment.gateway.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    public static String currentDateTime() {
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        return new SimpleDateFormat(dateFormat).format(new Date());
    }
}
