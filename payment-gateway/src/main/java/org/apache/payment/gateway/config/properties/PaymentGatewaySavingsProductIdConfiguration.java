package org.apache.payment.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "payment-gateway.savings-product-id")
@Data
public class PaymentGatewaySavingsProductIdConfiguration {
    private int merchant = 3;
    private int distributor = 5;
}
