package org.apache.payment.gateway.dto.fineract.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class AuthenticationRequest {

    @NotBlank(message = "username is required field")
    private String username;

    @NotBlank(message = "password is required field")
    private String password;
}
