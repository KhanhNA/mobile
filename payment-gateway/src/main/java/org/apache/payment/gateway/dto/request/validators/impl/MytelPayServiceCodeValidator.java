package org.apache.payment.gateway.dto.request.validators.impl;

import org.apache.payment.gateway.config.properties.MytelPayConfiguration;
import org.apache.payment.gateway.dto.request.validators.MytelPayServiceCode;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MytelPayServiceCodeValidator implements ConstraintValidator<MytelPayServiceCode, String> {

   private final MytelPayConfiguration mytelPayConfiguration;

   public MytelPayServiceCodeValidator(MytelPayConfiguration mytelPayConfiguration) {
      this.mytelPayConfiguration = mytelPayConfiguration;
   }

   public boolean isValid(String value, ConstraintValidatorContext context) {
      return value.equals(mytelPayConfiguration.getServiceCode());
   }
}
