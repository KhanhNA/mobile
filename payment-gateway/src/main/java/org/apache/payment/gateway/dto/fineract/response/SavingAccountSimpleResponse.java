package org.apache.payment.gateway.dto.fineract.response;

import lombok.Data;

@Data
public class SavingAccountSimpleResponse {
    private Long clientId;
    private Long officeId;
    private Long resourceId;
    private Long savingsId;
}
