package org.apache.payment.gateway.constants.fineract;

public enum ProductType {
    WALLET(3),
    POINT(4),
    LOYALTY(5);

    int  value;

    ProductType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
