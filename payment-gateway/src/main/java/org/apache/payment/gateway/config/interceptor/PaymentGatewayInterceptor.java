package org.apache.payment.gateway.config.interceptor;

import org.apache.payment.gateway.config.properties.FineractApiConfiguration;
import org.apache.payment.gateway.config.properties.FineractConfiguration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.net.URI;

/**
 * @author Rahul Goel created on 2/6/18
 */

@Component
public class PaymentGatewayInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!request.getServletPath().equals("/payment-gateway/api/v1/authentication")) {
            String authorization = request.getHeader("Authorization");
            if (authorization == null || !authorization.matches("Basic .+")) {
                response.setStatus(401);
                PrintWriter out = response.getWriter();
                out.write("Unauthorized");
                out.flush();
                out.close();
                return false;
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}
