package org.apache.payment.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.apache.payment.gateway.domains.FineractTransaction;

public interface FineractTransactionRepository extends JpaRepository<FineractTransaction, Long>, JpaSpecificationExecutor<FineractTransaction> {
}
