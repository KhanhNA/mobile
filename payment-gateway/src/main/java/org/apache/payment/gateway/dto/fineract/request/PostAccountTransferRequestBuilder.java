package org.apache.payment.gateway.dto.fineract.request;

import lombok.Builder;
import lombok.Getter;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Builder
public class PostAccountTransferRequestBuilder {

    private final Long fromOfficeId = 1L;
    private final Long fromAccountType = 2L;

    private final Long toOfficeId = 1L;
    private final Long toClientId = 1L;
    private final Long toAccountType = 2L;
    private final Long toAccountId = 2L;

    private final String dateFormat = "yyyy/MM/dd";
    private final String locale = "en";
    private final String transferDate = new SimpleDateFormat(dateFormat).format(new Date());

    private Long fromClientId;
    private Long fromAccountId;
    private Double transferAmount;
    private String transferDescription;

}
