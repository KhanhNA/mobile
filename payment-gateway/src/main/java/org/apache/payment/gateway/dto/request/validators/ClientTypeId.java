package org.apache.payment.gateway.dto.request.validators;

import org.apache.payment.gateway.dto.request.validators.impl.ClientTypeIdValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ClientTypeIdValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ClientTypeId {
    String message() default "{pg.validation.constraints.ClientTypeId.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
