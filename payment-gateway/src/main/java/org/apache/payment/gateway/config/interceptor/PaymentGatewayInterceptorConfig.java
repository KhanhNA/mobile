package org.apache.payment.gateway.config.interceptor;

import lombok.RequiredArgsConstructor;
import org.apache.payment.gateway.constants.InterceptorConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author Rahul Goel created on 2/6/18
 */
@Configuration
@RequiredArgsConstructor
public class PaymentGatewayInterceptorConfig implements WebMvcConfigurer {

    private final PaymentGatewayInterceptor paymentGatewayInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(paymentGatewayInterceptor).addPathPatterns(InterceptorConstants.INTERCEPTOR_PATTERNS.split(","));
    }
}
