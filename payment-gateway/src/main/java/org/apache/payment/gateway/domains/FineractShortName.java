package org.apache.payment.gateway.domains;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="fineract_short_name")
public class FineractShortName {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "short_name")
    private String shortName;

    public FineractShortName(String shortName) {
        this.shortName = shortName;
    }
}
