package org.apache.payment.gateway.constants.fineract;

public enum PaymentType {

    MONEY_TRANSFER(1),
    COIN_TRANSFER(2),
    LOYALTY_TRANSFER(3);

    private final int value;

    PaymentType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
