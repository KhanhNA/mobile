package org.apache.payment.gateway.repository.beyonic;

import org.apache.payment.gateway.config.hibernate.AbstractBaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sanyam Goel created on 29/7/18
 */
@Repository
public class CollectionRequestRepository extends AbstractBaseRepository {
}
