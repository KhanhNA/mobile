package org.apache.payment.gateway.controllers.fineract;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.payment.gateway.config.aspect.Loggable;
import org.apache.payment.gateway.config.properties.FineractOfficeIdConfiguration;
import org.apache.payment.gateway.controllers.error.PaymentGatewayNotFoundException;
import org.apache.payment.gateway.dto.fineract.request.ClientRequestBuilder;
import org.apache.payment.gateway.dto.fineract.response.ClientResponseModel;
import org.apache.payment.gateway.dto.fineract.response.FineractResponseModel;
import org.apache.payment.gateway.dto.request.PostClientRequest;
import org.apache.payment.gateway.dto.request.PutClientRequest;
import org.apache.payment.gateway.service.fineract.ClientService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/payment-gateway/api/v1")
@Log4j2
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;
    private final FineractOfficeIdConfiguration fineractOfficeIdConfiguration;

    @Loggable
    @GetMapping("/clients/{clientId:[0-9]+}")
    public ResponseEntity<ClientResponseModel> getClient(@PathVariable Long clientId, @RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(clientService.getClient(clientId, token));
    }

    @Loggable
    @PostMapping("/clients")
    public ResponseEntity<FineractResponseModel> createClient(@RequestHeader("Authorization") String token,
                                                              @Valid @RequestBody PostClientRequest request) {
        Integer clientTypeId = request.getClientTypeId();
        ClientRequestBuilder clientRequestBuilder = ClientRequestBuilder
                .builder()
                .officeId(fineractOfficeIdConfiguration.getOfficeByClientType(clientTypeId))
                .username(request.getUsername())
                .accountNo(request.getUsername()) // for mapping column in client table
                .email(request.getEmail())
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .clientTypeId(clientTypeId)
                .savingsProductId(request.getSavingsProductId())
                .externalId(request.getExternalId())
                .build();
        return ResponseEntity.ok(clientService.createClient(clientRequestBuilder, token));
    }

    @Loggable
    @PutMapping("/clients/{id}")
    public ResponseEntity<FineractResponseModel> updateClient(@RequestHeader("Authorization") String token,
                                                              @PathVariable Long id,
                                                              @Valid @RequestBody PutClientRequest request) {
        Integer clientTypeId = request.getClientTypeId();
        ClientRequestBuilder clientRequestBuilder = ClientRequestBuilder
                .builder()
                .officeId(fineractOfficeIdConfiguration.getOfficeByClientType(clientTypeId))
                .username(request.getUsername())
                .accountNo(request.getUsername()) // for mapping column in client table
                .email(request.getEmail())
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .clientTypeId(clientTypeId)
                .savingsProductId(request.getSavingsProductId())
                .externalId(request.getExternalId())
                .build();
        return ResponseEntity.ok(clientService.updateClient(id, clientRequestBuilder, token));
    }

    @Loggable
    @PostMapping(value = "/clients/{id}", params = {"command"})
    public ResponseEntity<FineractResponseModel> handleClientCommand(@RequestHeader("Authorization") String token,
                                                              @PathVariable Long id,
                                                              @RequestParam(name = "command") String command) {
        switch (command) {
            case "close":
                return ResponseEntity.ok(clientService.closeClient(id, token));
            case "reactivate":
                return ResponseEntity.ok(clientService.reactivateClient(id, token));
            default:
                throw new PaymentGatewayNotFoundException("Not found");
        }
    }

    @GetMapping("/clients/{clientId:[0-9]+}/accounts")
    public ResponseEntity<?> getClientAccounts(@PathVariable Long clientId, @RequestHeader("Authorization") String token) throws IOException {
        return ResponseEntity.ok(clientService.getClientAccounts(clientId, token));
    }
}
