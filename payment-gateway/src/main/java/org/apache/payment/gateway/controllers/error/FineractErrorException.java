package org.apache.payment.gateway.controllers.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class FineractErrorException extends RuntimeException {
    public FineractErrorException(String message) {
        super(message);
    }
}
