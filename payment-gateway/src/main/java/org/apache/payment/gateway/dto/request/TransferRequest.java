package org.apache.payment.gateway.dto.request;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class TransferRequest {

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    private Long merchantWalletId;

    @NotBlank(message = "{pg.validation.constraints.NotBlank.message}")
    private String description;

    @Valid
    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @Size(min = 1, message = "{pg.validation.constraints.Min.message}")
    private List<TransferDetailsRequest> details;
}
