package org.apache.payment.gateway.controllers.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class ApiError {
    private long timestamp = new Date().getTime();
    private int status;
    private String path;
    private String message;
    private Object developerMessage;
    private Map<String, Object> fieldErrorValidator;

    @Builder
    public ApiError(int status, String path, String message) {
        this.status = status;
        this.path = path;
        this.message = message;
    }
}
