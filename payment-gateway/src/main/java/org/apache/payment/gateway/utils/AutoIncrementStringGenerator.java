package org.apache.payment.gateway.utils;

/**
 * @author NMQ
 * @version 1.0
 */
public class AutoIncrementStringGenerator {

    private static final Character[] characters = new Character[]{
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z'};

    /**
     *
     * @param str string need to increment 1
     * @return string increment with 1
     */
    public static String generate(String str) {
        if (str == null || str.isEmpty()) {
            throw new RuntimeException("str must not be null");
        }
        str = str.toUpperCase();
        StringBuilder result = new StringBuilder();
        int start = str.length() - 1;
        int remainder = 0;
        for (int i = start; i >= 0; i--) {
            int position = getPosition(str.charAt(i));
            int max = characters.length;
            int newPosition;
            int temp;
            if (i == start) {
                temp = position + 1 + remainder;
            } else {
                temp = position + remainder;
            }
            if (temp == max) {
                remainder = 1;
                newPosition = 0;
            } else {
                remainder = 0;
                newPosition = temp;
            }
            result.insert(0, characters[newPosition]);
        }
        return result.toString();
    }

    private static int getPosition(char c) {
        for (int i = 0; i < characters.length; i++) {
            if (c == characters[i]) {
                return i;
            }
        }
        return -1;
    }
}
