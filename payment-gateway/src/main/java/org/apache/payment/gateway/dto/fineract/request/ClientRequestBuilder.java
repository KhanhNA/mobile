package org.apache.payment.gateway.dto.fineract.request;

import lombok.Builder;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class ClientRequestBuilder {

    private final boolean active;
    private final String locale;
    private final String dateFormat;
    private final String activationDate;

    private Integer officeId;
    private String username;
    private String accountNo;
    private String email;
    private String firstname;
    private String lastname;
    private Integer clientTypeId; // npp = 2, l1 = 1
    private Integer savingsProductId;
    private Long externalId;

    @Builder
    public ClientRequestBuilder(
            Integer officeId,
            String username,
            String email,
            String firstname,
            String lastname,
            Integer clientTypeId,
            Long externalId,
            Integer savingsProductId,
            String accountNo
    ) {
        this.officeId = officeId;
        this.username = username;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.clientTypeId = clientTypeId;
        this.externalId = externalId;
        this.savingsProductId = savingsProductId;
        this.active = true;
        this.locale = "en";
        this.dateFormat = "yyyy-MM-dd";
        this.activationDate = new SimpleDateFormat(dateFormat).format(new Date());
        this.accountNo = accountNo;
    }
}
