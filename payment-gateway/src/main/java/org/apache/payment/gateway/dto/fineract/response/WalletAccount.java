package org.apache.payment.gateway.dto.fineract.response;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class WalletAccount extends Account {
    private Double accountBalance;

    @Builder
    public WalletAccount(
            Long accountId,
            Long clientId,
            Long productId,
            String productName,
            String accountNo,
            String currencyCode,
            String currencyName,
            Double accountBalance
    ) {
        super(accountId, clientId, productId, productName, accountNo, currencyCode, currencyName);
        this.accountBalance = accountBalance;
    }
}
