package org.apache.payment.gateway.dto.request.validators;

import org.apache.payment.gateway.dto.request.validators.impl.MytelPayPartnerCodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = MytelPayPartnerCodeValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MytelPayPartnerCode {
    String message() default "{pg.validation.constraints.MytelPayPartnerCode.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
