package org.apache.payment.gateway.dto.fineract.response;

import lombok.Data;

@Data
public class ResourceResponse {
    private Long resourceId;
}
