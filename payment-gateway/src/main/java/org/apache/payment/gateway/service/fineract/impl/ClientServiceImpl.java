package org.apache.payment.gateway.service.fineract.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.payment.gateway.config.aspect.Loggable;
import org.apache.payment.gateway.config.properties.FineractApiConfiguration;
import org.apache.payment.gateway.dto.fineract.request.ClientRequestBuilder;
import org.apache.payment.gateway.dto.fineract.request.FineractDepositRequest;
import org.apache.payment.gateway.dto.fineract.response.*;
import org.apache.payment.gateway.service.fineract.ClientService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import static org.apache.payment.gateway.utils.HttpHeaderUtil.generateDefaultFineractHeaders;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final RestTemplate restTemplate;
    private final FineractApiConfiguration fineractApiConfiguration;

    @Override
    @Loggable
    public ClientResponseModel getClient(Long id, String authorizationToken) {
        try {
            ResponseEntity<ClientResponseModel> response = restTemplate.exchange(
                    fineractApiConfiguration.getClient(),
                    HttpMethod.GET,
                    new HttpEntity<>(generateDefaultFineractHeaders(authorizationToken)),
                    ClientResponseModel.class,
                    id);
            if (response.getStatusCode().is2xxSuccessful())
                return response.getBody();
            else
                return null;
        } catch (HttpServerErrorException e) {
            throw new HttpServerErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        }
    }

    @Override
    @Loggable
    public FineractResponseModel createClient(ClientRequestBuilder requestBuilder, String authorizationToken) {
        try {
            ResponseEntity<FineractResponseModel> response = restTemplate.postForEntity(
                    fineractApiConfiguration.getClientsAccounting(),
                    new HttpEntity<>(requestBuilder, generateDefaultFineractHeaders(authorizationToken)),
                    FineractResponseModel.class);
            if (response.getStatusCode().is2xxSuccessful())
                return response.getBody();
            else
                return null;
        } catch (HttpServerErrorException e) {
            throw new HttpServerErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        }
    }

    @Override
    public FineractResponseModel updateClient(Long id, ClientRequestBuilder requestBuilder, String authorizationToken) {
        try {
            ResponseEntity<FineractResponseModel> response = restTemplate.exchange(
                    fineractApiConfiguration.getClient() + "/{clientId}",
                    HttpMethod.PUT,
                    new HttpEntity<>(requestBuilder, generateDefaultFineractHeaders(authorizationToken)),
                    FineractResponseModel.class,
                    id);
            return response.getBody();
        } catch (HttpServerErrorException e) {
            throw new HttpServerErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        }
    }


    @Override
    @Loggable
    public Object getClientAccounts(Long clientId, String authorizationToken) throws IOException {
        ResponseEntity<String> response = this.restTemplate.exchange(
                fineractApiConfiguration.getClientAccounts(),
                HttpMethod.GET,
                new HttpEntity<>(generateDefaultFineractHeaders(authorizationToken)),
                String.class,
                clientId);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(response.getBody());
        JsonNode savingsAccounts = jsonNode.get("savingsAccounts");

        Map<String, Account> accounts = new HashMap<>();
        if (savingsAccounts.isArray()) {
            for (JsonNode node : savingsAccounts) {
                long id = node.get("id").asLong();
                String accountNo = node.get("accountNo").asText();
                Long productId = node.get("productId").asLong();
                String productName = node.get("productName").asText();
                JsonNode currency = node.get("currency");
                String currencyCode = currency.get("code").asText();
                String currencyName = currency.get("name").asText();
                double accountBalance = 0;
                if (node.has("accountBalance")) {
                    accountBalance = node.get("accountBalance").asDouble();
                }
                if (productName.lastIndexOf("WALLET") != -1) {
                    accounts.put("WALLET", WalletAccount
                            .builder()
                            .clientId(clientId)
                            .accountId(id)
                            .accountNo(accountNo)
                            .productId(productId)
                            .currencyCode(currencyCode)
                            .currencyName(currencyName)
                            .productName(productName)
                            .accountBalance(accountBalance)
                            .build());
                } else if (productName.lastIndexOf("POINT") != -1) {
                    accounts.put("POINT", PointAccount
                            .builder()
                            .clientId(clientId)
                            .accountId(id)
                            .accountNo(accountNo)
                            .productId(productId)
                            .currencyCode(currencyCode)
                            .currencyName(currencyName)
                            .productName(productName)
                            .totalPoint((long) accountBalance)
                            .build());
                }
            }
        }
        return accounts;
    }

    @Override
    public Object deposit(Long accountId, Double transactionAmount) {
        FineractDepositRequest fineractDepositRequest = new FineractDepositRequest(transactionAmount);
        ResponseEntity<FineractResponseModel> response = restTemplate.exchange(
                fineractApiConfiguration.getDeposit(),
                HttpMethod.POST,
                new HttpEntity<>(fineractDepositRequest, generateDefaultFineractHeaders()),
                FineractResponseModel.class,
                accountId);
        return response.getBody();
    }

    @Override
    public FineractResponseModel closeClient(Long id, String token) {
        ResponseEntity<FineractResponseModel> response = restTemplate.exchange(
                fineractApiConfiguration.getClients() + "/{clientId}/?command=close",
                HttpMethod.POST,
                new HttpEntity<>(generateDefaultFineractHeaders(token)),
                FineractResponseModel.class,
                id);
        return response.getBody();
    }

    @Override
    public FineractResponseModel reactivateClient(Long id, String token) {
        ResponseEntity<FineractResponseModel> response = restTemplate.exchange(
                fineractApiConfiguration.getClients() + "/{clientId}/?command=reactivate",
                HttpMethod.POST,
                new HttpEntity<>(generateDefaultFineractHeaders(token)),
                FineractResponseModel.class,
                id);
        return response.getBody();
    }
}
