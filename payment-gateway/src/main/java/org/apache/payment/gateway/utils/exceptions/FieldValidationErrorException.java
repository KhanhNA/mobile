package org.apache.payment.gateway.utils.exceptions;

import org.springframework.validation.FieldError;

import java.util.List;

public class FieldValidationErrorException extends RuntimeException {

    private List<FieldError> fieldErrors;

    public FieldValidationErrorException(List<FieldError> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }
}
