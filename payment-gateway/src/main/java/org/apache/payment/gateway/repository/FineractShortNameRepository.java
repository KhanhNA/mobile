package org.apache.payment.gateway.repository;

import org.apache.payment.gateway.domains.FineractShortName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FineractShortNameRepository extends JpaRepository<FineractShortName, Long> {

    @Query(value = "SELECT short_name FROM fineract_short_name fsn WHERE id = (SELECT max(id) from fineract_short_name)", nativeQuery = true)
    String latestShortName();
}
