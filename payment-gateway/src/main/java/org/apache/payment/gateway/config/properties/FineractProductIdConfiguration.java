package org.apache.payment.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "fineract.product-id")
@Data
public class FineractProductIdConfiguration {
    private int wallet = 3;
    private int point = 4;
    private int loyalty = 5;
}
