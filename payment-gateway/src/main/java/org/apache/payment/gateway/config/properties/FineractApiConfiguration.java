package org.apache.payment.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "fineract.api")
@Data
public class FineractApiConfiguration {
    private String authentication = "https://localhost:8443/fineract-provider/api/v1/authentication";
    private String clients = "https://localhost:8443/fineract-provider/api/v1/clients";
    private String client = "https://localhost:8443/fineract-provider/api/v1/clients/{clientId}";
    private String clientAccounts = "https://localhost:8443/fineract-provider/api/v1/clients/{clientId}/accounts";
    private String accountTransfersExtension = "https://localhost:8443/fineract-provider/api/v1/accounttransfersext";
    private String clientsAccounting = "https://localhost:8443/fineract-provider/api/v1/clientsAccounting";
    private String deposit = "https://localhost:8443/fineract-provider/api/v1/savingsaccounts/{accountId}/transactions?command=deposit";
}
