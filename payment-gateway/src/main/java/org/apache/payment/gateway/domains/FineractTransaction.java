package org.apache.payment.gateway.domains;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "fineract_transaction")
@NoArgsConstructor
public class FineractTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Builder
    public FineractTransaction(Long orderId, Long accountId, BigDecimal transferAmount, Integer paymentType, Integer paymentStatus, Date createdAt, Date updatedAt, Long clientId, Long externalId, Long savingsAccountId, String accountNo, String currencyCode) {
        this.orderId = orderId;
        this.accountId = accountId;
        this.transferAmount = transferAmount;
        this.paymentType = paymentType;
        this.paymentStatus = paymentStatus;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.clientId = clientId;
        this.externalId = externalId;
        this.savingsAccountId = savingsAccountId;
        this.accountNo = accountNo;
        this.currencyCode = currencyCode;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "transfer_amount")
    private BigDecimal transferAmount;

    @Column(name = "payment_type")
    private Integer paymentType;

    @Column(name = "payment_status")
    private Integer paymentStatus;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "external_id", nullable = false)
    private Long externalId;

    @Column(name = "savings_account_id")
    private Long savingsAccountId;

    @Column(name = "account_no")
    private String accountNo;

    @Column(name = "currency_code")
    private String currencyCode;

}
