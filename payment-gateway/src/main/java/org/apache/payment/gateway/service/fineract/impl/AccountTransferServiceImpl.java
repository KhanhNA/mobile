package org.apache.payment.gateway.service.fineract.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.payment.gateway.config.properties.FineractApiConfiguration;
import org.apache.payment.gateway.config.properties.MytelPayConfiguration;
import org.apache.payment.gateway.dto.fineract.request.TransferDetailsRequestBuilder;
import org.apache.payment.gateway.dto.fineract.request.TransferRequestBuilder;
import org.apache.payment.gateway.dto.mytelpay.PayRequest;
import org.apache.payment.gateway.dto.request.MytelPayRequestPayload;
import org.apache.payment.gateway.dto.request.TransferRequest;
import org.apache.payment.gateway.service.fineract.AccountTransferService;
import org.apache.payment.gateway.utils.exceptions.PgBaseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.payment.gateway.utils.HttpHeaderUtil.generateDefaultFineractHeaders;
import static org.apache.payment.gateway.utils.MytelPayUtils.createJWT;

@Service
@RequiredArgsConstructor
public class AccountTransferServiceImpl implements AccountTransferService {

    private final FineractApiConfiguration fineractApiConfiguration;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final MytelPayConfiguration mytelPayConfiguration;

    @Override
    public Object transfer(String token, TransferRequest request) throws IOException {
        TransferRequestBuilder requestBuilder = TransferRequestBuilder
                .builder()
                .fromClientId(request.getMerchantWalletId())
                .transferDescription(request.getDescription())
                .details(request.getDetails()
                        .stream()
                        .map(transferDetailsRequest -> TransferDetailsRequestBuilder
                                .builder()
                                .toNPPClientId(transferDetailsRequest.getDistributorWalletId())
                                .amount(transferDetailsRequest.getAmount())
                                .productId(transferDetailsRequest.getProductId())
                                .build()).collect(Collectors.toList()
                        )
                )
                .build();
        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    fineractApiConfiguration.getAccountTransfersExtension(),
                    HttpMethod.POST,
                    new HttpEntity<>(requestBuilder, generateDefaultFineractHeaders(token)),
                    String.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                return response.getBody();
            } else {
                throw new RuntimeException(response.getBody());
            }
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsByteArray(), Charset.defaultCharset());
        } catch (Exception e) {
            throw new PgBaseException(e.getMessage());
        }
    }

    @Override
    public String sendRequestToMytelPay(PayRequest payload) throws JsonProcessingException {
        String jwt = createJWT(
                mytelPayConfiguration.getPartnerId(),
                mytelPayConfiguration.getIssuer(),
                objectMapper.writeValueAsString(payload),
                mytelPayConfiguration.getTtlMillis(),
                mytelPayConfiguration.getSecretKey()
        );
        ResponseEntity<String> response = restTemplate.getForEntity(String.format("%s?data=%s", mytelPayConfiguration.getPayRequestPath(), jwt), String.class);
        return response.getBody();
    }
}
