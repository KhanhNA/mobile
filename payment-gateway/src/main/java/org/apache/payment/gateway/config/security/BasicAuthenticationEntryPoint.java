package org.apache.payment.gateway.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.payment.gateway.dto.request.MytelPayConfirmPayload;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class BasicAuthenticationEntryPoint implements AuthenticationEntryPoint {

    ObjectMapper objectMapper;

    public BasicAuthenticationEntryPoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException {

        MytelPayConfirmPayload mytelPayConfirmPayload;
        try {
            mytelPayConfirmPayload = objectMapper.readValue(request.getReader(), MytelPayConfirmPayload.class);
            if (Objects.isNull(mytelPayConfirmPayload))
                mytelPayConfirmPayload = new MytelPayConfirmPayload();

        } catch (Exception ex) {
            mytelPayConfirmPayload = new MytelPayConfirmPayload();
        }

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        mytelPayConfirmPayload.setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
        mytelPayConfirmPayload.setStatusMessage(e.getLocalizedMessage());
        response.getOutputStream().print(objectMapper.writeValueAsString(mytelPayConfirmPayload));
    }
}
