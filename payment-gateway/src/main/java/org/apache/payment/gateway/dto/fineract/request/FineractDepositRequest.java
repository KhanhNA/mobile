package org.apache.payment.gateway.dto.fineract.request;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
public class FineractDepositRequest {
    private String locale = "en";
    private String dateFormat = "yyyy-MM-dd";
    private String transactionDate = new SimpleDateFormat(dateFormat).format(new Date());
    private Double transactionAmount;
    private String paymentTypeId = "1";

    @Builder
    public FineractDepositRequest(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
}
