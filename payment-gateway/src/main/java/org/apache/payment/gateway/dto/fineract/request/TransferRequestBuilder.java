package org.apache.payment.gateway.dto.fineract.request;

import lombok.Builder;
import lombok.Data;
import org.apache.payment.gateway.constants.fineract.FineractContstants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Data
public class TransferRequestBuilder {
    private final String locale;
    private final String dateFormat;
    private final String transferDate;

    private Long fromClientId;
    private String transferDescription;
    private List<TransferDetailsRequestBuilder> details;

    @Builder
    public TransferRequestBuilder(Long fromClientId, String transferDescription, List<TransferDetailsRequestBuilder> details) {
        this.fromClientId = fromClientId;
        this.transferDescription = transferDescription;
        this.details = details;
        locale = FineractContstants.LOCALE;
        dateFormat = FineractContstants.DATE_FORMAT;
        transferDate = new SimpleDateFormat(dateFormat).format(new Date());
    }
}
