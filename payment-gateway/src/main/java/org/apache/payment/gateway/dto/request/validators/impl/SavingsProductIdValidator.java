package org.apache.payment.gateway.dto.request.validators.impl;

import lombok.RequiredArgsConstructor;
import org.apache.payment.gateway.config.properties.FineractProductIdConfiguration;
import org.apache.payment.gateway.dto.request.validators.SavingsProductId;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
public class SavingsProductIdValidator implements ConstraintValidator<SavingsProductId, Integer> {

    private final FineractProductIdConfiguration fineractProductIdConfiguration;

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        return value == fineractProductIdConfiguration.getWallet() ||
                value == fineractProductIdConfiguration.getPoint()
                || value == fineractProductIdConfiguration.getLoyalty();
    }

}
