package org.apache.payment.gateway.dto.request.validators;

import org.apache.payment.gateway.dto.request.validators.impl.SavingsProductIdValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = SavingsProductIdValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SavingsProductId {
    String message() default "{pg.validation.constraints.SavingsProductId.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
