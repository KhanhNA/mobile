package org.apache.payment.gateway.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class MytelPayRequestPayload {
    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    private Double amount;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @Pattern(regexp = "^[0-9]*$", message = "{pg.validation.constraints.Number.message}")
    private String accountId;
}
