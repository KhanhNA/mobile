package org.apache.payment.gateway.config.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "fineract.office-id")
@Data
public class FineractOfficeIdConfiguration {

    PaymentGatewayClientTypeConfiguration paymentGatewayClientTypeConfiguration;

    private int headOffice = 1;
    private int distributor = 2;
    private int merchant = 3;

    @Autowired
    public void setPaymentGatewayClientTypeConfiguration(PaymentGatewayClientTypeConfiguration paymentGatewayClientTypeConfiguration) {
        this.paymentGatewayClientTypeConfiguration = paymentGatewayClientTypeConfiguration;
    }

    public int getOfficeByClientType(int clientType) {
        if (clientType == paymentGatewayClientTypeConfiguration.getDistributor()) {
            return distributor;
        } else if (clientType == paymentGatewayClientTypeConfiguration.getMerchant()) {
            return merchant;
        } else {
            return headOffice;
        }
    }
}
