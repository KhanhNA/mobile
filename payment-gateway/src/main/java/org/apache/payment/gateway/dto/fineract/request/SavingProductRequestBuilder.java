package org.apache.payment.gateway.dto.fineract.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SavingProductRequestBuilder {
    private String name;
    private String shortName;
    private String currencyCode;
    private final String inMultiplesOf = "0";
    private final Integer digitsAfterDecimal = 0;
    private final String nominalAnnualInterestRate = "0";
    private final Integer interestCompoundingPeriodType = 1;
    private final Integer interestPostingPeriodType = 4;
    private final Integer interestCalculationType = 1;
    private final Integer interestCalculationDaysInYearType = 365;
    private final String accountingRule = "1";
    private final String minRequiredOpeningBalance = "0";
    private final String lockinPeriodFrequency = "0";
    private final Integer lockinPeriodFrequencyType = 0;
    private final Boolean enforceMinRequiredBalance = false;
    private final String minRequiredBalance = "0";
    private final String minBalanceForInterestCalculation = "0";
    private final Object[] paymentChannelToFundSourceMappings = {};
    private final Object[] feeToIncomeAccountMappings = {};
    private final Object[] penaltyToIncomeAccountMappings = {};
    private final String locale = "en";
}
