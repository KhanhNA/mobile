package org.apache.payment.gateway.dto.request;

import lombok.Data;
import org.apache.payment.gateway.dto.request.validators.ClientTypeId;
import org.apache.payment.gateway.dto.request.validators.SavingsProductId;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class PostClientRequest {

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @Size(min = 6, max = 32, message = "{pg.validation.constraints.Size.message}")
    private String username;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @Email(message = "{pg.validation.constraints.Email.message}")
    @Size(min = 6, max = 120, message = "{pg.validation.constraints.Size.message}")
    private String email;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
//    @Size(min = 6, max = 45, message = "{pg.validation.constraints.Size.message}")
    private String firstname;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
//    @Size(min = 6, max = 45, message = "{pg.validation.constraints.Size.message}")
    private String lastname;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @ClientTypeId
    private Integer clientTypeId;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @SavingsProductId
    private Integer savingsProductId;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    private Long externalId;
}
