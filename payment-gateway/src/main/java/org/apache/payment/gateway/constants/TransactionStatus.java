package org.apache.payment.gateway.constants;

/**
 * @author Sanyam Goel created on 29/7/18
 */
public enum TransactionStatus {
    PAYMENT_INITIATED,
    PAYMENT_DECLINED,
    PAYMENT_SUCCESS;
}
