package org.apache.payment.gateway.dto.fineract.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Account {
    protected Long accountId;
    protected Long clientId;
    protected Long productId;
    protected String productName;
    protected String accountNo;
    protected String currencyCode;
    protected String currencyName;
}
