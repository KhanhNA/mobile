package org.apache.payment.gateway.controllers.fineract;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.payment.gateway.config.aspect.Loggable;
import org.apache.payment.gateway.config.properties.MytelPayConfiguration;
import org.apache.payment.gateway.dto.mytelpay.PayRequest;
import org.apache.payment.gateway.dto.request.MytelPayRequestPayload;
import org.apache.payment.gateway.dto.request.TransferRequest;
import org.apache.payment.gateway.service.fineract.AccountTransferService;
import org.apache.payment.gateway.utils.DateUtils;
import org.apache.payment.gateway.utils.HttpHeaderUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

import static org.apache.payment.gateway.utils.HttpHeaderUtil.*;
import static org.apache.payment.gateway.utils.MytelPayUtils.createJWT;

@RestController
@RequestMapping("/payment-gateway/api/v1")
@RequiredArgsConstructor
public class AccountTransferController {

    private final AccountTransferService accountTransferService;

    @Loggable
    @PostMapping("/transfer")
    public ResponseEntity<?> transfer(@RequestHeader("Authorization") String token, @RequestBody @Valid TransferRequest request) throws IOException {
        return ResponseEntity.ok(accountTransferService.transfer(token, request));
    }

    @Loggable
    @PostMapping("/mytel-pay-request")
    public ResponseEntity<Void> sendRequest(@Valid @RequestBody MytelPayRequestPayload payload) throws JsonProcessingException {
        accountTransferService.sendRequestToMytelPay(PayRequest.builder()
                .amount(payload.getAmount())
                .orderId(payload.getAccountId())
                .dateTime(DateUtils.currentDateTime())
                .build());
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
