package org.apache.payment.gateway.utils;

import org.apache.payment.gateway.constants.beyonic.BeyonicConstants;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

/**
 * @author Sanyam Goel created on 27/7/18
 */
public class HttpHeaderUtil {

    private static HttpHeaders headers = new HttpHeaders();

    public static HttpEntity<?> getHttpEntityInstance() {
        headers.set("Authorization", "Token " + BeyonicConstants.API_TOKEN);
        return new HttpEntity<>(headers);
    }

    public static HttpEntity<?> getFineractEntityInstance() {
        HttpHeaders fineractHeaders = new HttpHeaders();
        fineractHeaders.add("Fineract-Platform-TenantId", "default");
        return new HttpEntity<>(fineractHeaders);
    }

    public static HttpHeaders generateDefaultFineractHeaders(String token) {
        HttpHeaders fineractHeaders = new HttpHeaders();
        fineractHeaders.add("Authorization", token);
        fineractHeaders.add("Fineract-Platform-TenantId", "default");
        return fineractHeaders;
    }

    public static HttpHeaders generateDefaultFineractHeaders() {
        HttpHeaders fineractHeaders = new HttpHeaders();
        fineractHeaders.setBasicAuth("mifos", "password");
        fineractHeaders.add("Fineract-Platform-TenantId", "default");
        return fineractHeaders;
    }
}
