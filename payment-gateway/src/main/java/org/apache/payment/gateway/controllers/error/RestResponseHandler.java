package org.apache.payment.gateway.controllers.error;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.payment.gateway.dto.response.ErrorResponseModel;
import org.apache.payment.gateway.dto.response.ResponseModel;
import org.apache.payment.gateway.enums.PgExceptionType;
import org.apache.payment.gateway.utils.exceptions.PgHibernateException;
import org.apache.payment.gateway.utils.exceptions.PgResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Rahul Goel created on 16/6/18
 */
@RestControllerAdvice
@RequiredArgsConstructor
public class RestResponseHandler {

    private static Logger logger = LogManager.getLogger(RestResponseHandler.class);

    public <T> ResponseEntity<ResponseModel<T>> responseStandardizer(T object, HttpStatus httpStatus) {
        ResponseModel<T> responseModel = new ResponseModel<>(object, httpStatus.value(), null);
        return new ResponseEntity<>(responseModel, httpStatus);
    }

    public <T> ResponseEntity<ResponseModel<T>> responseStandardizer(T object) {
        return this.responseStandardizer(object, HttpStatus.OK);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ApiError> handleMethodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex, HttpServletRequest request) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), request.getRequestURI(), ex.getLocalizedMessage());
        Map<String, Object> fieldErrors = new HashMap<>();
        BindingResult bindingResult = ex.getBindingResult();
        for (FieldError error : bindingResult.getFieldErrors())
            fieldErrors.put(error.getField(), error.getDefaultMessage());

        apiError.setFieldErrorValidator(fieldErrors);
        return ResponseEntity.badRequest().body(apiError);
    }

    @ExceptionHandler({HttpClientErrorException.class})
    public ResponseEntity<ApiError> handleHttpClientErrorException(HttpClientErrorException ex, HttpServletRequest request) {
        ApiError apiError = new ApiError(ex.getRawStatusCode(), request.getRequestURI(), ex.getLocalizedMessage());
        apiError.setDeveloperMessage(ex.getResponseBodyAsString());
        return ResponseEntity.status(ex.getStatusCode()).body(apiError);
    }

    @ExceptionHandler({HttpServerErrorException.class})
    public ResponseEntity<ApiError> handleHttpServerErrorException(HttpServerErrorException ex, HttpServletRequest request) {
        ApiError apiError = new ApiError(ex.getRawStatusCode(), request.getRequestURI(), ex.getLocalizedMessage());
        apiError.setDeveloperMessage(ex.getResponseBodyAsString());
        return ResponseEntity.status(ex.getStatusCode()).body(apiError);
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<ApiError> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, HttpServletRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(400, request.getRequestURI(), ex.getLocalizedMessage()));
    }

    @ExceptionHandler({PaymentGatewayNotFoundException.class})
    public ResponseEntity<ApiError> handlePaymentNotFoundException(PaymentGatewayNotFoundException ex, HttpServletRequest request) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiError(404, request.getRequestURI(), ex.getLocalizedMessage()));
    }

    /*
     * functions for handling payment-gateway custom exceptions.
     *
     * */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({PgResourceNotFoundException.class})
    public ResponseModel<Object> resourceNotFoundExceptionHandler(PgResourceNotFoundException ex) {
        logger.error("Exception : ", ex);
        ErrorResponseModel error = new ErrorResponseModel(PgExceptionType.PgResourceNotFoundException, ex.getErrorCode(), ex.getMessage(), Instant.now().toEpochMilli(), this.getErrors(ex.getMessage()));
        return new ResponseModel<>(null, HttpStatus.NOT_FOUND.value(), error);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({PgHibernateException.class})
    public ResponseModel<Object> hibernateExceptionHandler(PgHibernateException ex) {
        logger.error("Exception : ", ex);
        ErrorResponseModel error = new ErrorResponseModel(PgExceptionType.PgResourceNotFoundException, ex.getErrorCode(), ex.getMessage(), Instant.now().toEpochMilli(), this.getErrors(ex.getMessage()));
        return new ResponseModel<>(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), error);
    }

    private ArrayList<String> getErrors(String... errors) {
        ArrayList<String> errorMessages = new ArrayList<>();
        Collections.addAll(errorMessages, errors);
        return errorMessages;
    }

}

