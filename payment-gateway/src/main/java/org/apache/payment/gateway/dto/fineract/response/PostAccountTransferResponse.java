package org.apache.payment.gateway.dto.fineract.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PostAccountTransferResponse {

    @ApiModelProperty(example = "1")
    public Integer savingsId;

    @ApiModelProperty(example = "1")
    public Integer resourceId;

}
