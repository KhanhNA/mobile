package org.apache.payment.gateway.dto.request;

import lombok.Data;
import org.apache.payment.gateway.dto.request.validators.SavingsProductId;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class TransferDetailsRequest {

    // @NotNull(message = "distributorWalletId is required field")
    private Long distributorWalletId;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @Min(value = 1, message = "{pg.validation.constraints.Min.message}")
    private BigDecimal amount;

    @NotNull(message = "{pg.validation.constraints.NotNull.message}")
    @SavingsProductId
    private Integer productId;
}
