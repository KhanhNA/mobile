package org.apache.payment.gateway.dto.fineract.request;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class ActivateSavingsAccountRequest {
    private final String locale;
    private final String dateFormat ;
    private final String activatedOnDate;

    public ActivateSavingsAccountRequest() {
        locale = "en";
        dateFormat = "dd/MM/yyyy";
        activatedOnDate = new SimpleDateFormat(dateFormat).format(new Date());
    }
}
