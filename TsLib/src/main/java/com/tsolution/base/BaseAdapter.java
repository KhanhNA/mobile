package com.tsolution.base;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.tsolution.base.holder.GenericViewHolder;
import com.tsolution.base.listener.AdapterActionsListener;
import com.tsolution.base.listener.OwnerView;

import java.util.List;

public class BaseAdapter extends RecyclerView.Adapter<GenericViewHolder> implements AdapterActionsListener { //RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    BaseViewModel viewModel;
    OwnerView listener;
    private int layoutId;
    private BaseActivity activity;
    private ObservableField<List> datas;
    public BaseAdapter(@LayoutRes int loId, BaseViewModel vm, BaseActivity a) {
        layoutId = loId;
        this.viewModel = vm;
        this.activity = a;
        datas = viewModel.baseModels;
    }


    public BaseAdapter(@LayoutRes int loId, ObservableField data, BaseActivity a) {
        layoutId = loId;
        datas = data;
        this.activity = a;
    }


    public BaseAdapter(@LayoutRes int layoutId, BaseViewModel viewModel, OwnerView listener, BaseActivity a) {

        this.viewModel = viewModel;
        this.listener = listener;
        this.layoutId = layoutId;
        this.activity = a;
        datas = viewModel.baseModels;
    }

    public void replaceData(List<BaseModel> tasks) {
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                this.layoutId, parent, false);
        return new GenericViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
//        ObservableField<List<BaseModel>> lst = viewModel.getModels();
        BaseModel bm = (BaseModel)datas.get().get(position);
        bm.index = position + 1;
        holder.setBinding(bm, this);
    }


    @Override
    public int getItemCount() {
        return datas.get() == null? 0: datas.get().size();
    }

    @Override
    public void adapterAction(View view, BaseModel baseModel) {
        Toast.makeText(view.getContext(), "Hello",Toast.LENGTH_LONG).show();
        if(listener != null) {
            listener.onClicked(view, baseModel);
        }
    }
    @Override
    final public void onAdapterClicked(View view, BaseModel bm) {
        AdapterActionsListener.super.onAdapterClicked(view,bm);
    }

    @Override
    public BaseActivity getBaseActivity() {
        return activity;
    }
    @Override
    public int getLayoutRes() {
        return layoutId;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return viewModel.getClass();
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
