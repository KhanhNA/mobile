package com.tsolution.base;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tsolution.base.holder.GenericViewHolder;
import com.tsolution.base.listener.AdapterActionsListener;
import com.tsolution.base.listener.OwnerView;

import java.util.List;

public abstract class BaseBinding extends ViewDataBinding {


    protected BaseBinding(android.databinding.DataBindingComponent bindingComponent, View root, int localFieldCount) {
        super(bindingComponent, root, localFieldCount);
    }

    protected BaseBinding(Object bindingComponent, View root, int localFieldCount) {
        super(bindingComponent, root, localFieldCount);
    }
    public void setViewModel(BaseViewModel vm){

    }

}
