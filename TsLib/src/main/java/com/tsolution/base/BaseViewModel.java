package com.tsolution.base;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.tsolution.base.listener.ResponseResult;
import com.tsolution.base.listener.ViewFunction;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


public class BaseViewModel<T extends BaseModel> extends AndroidViewModel {
    protected ObservableField<T> model;//du lieu o man hinh view
    public ObservableField<List<BaseModel>> baseModels; //danh sach trong recycleview o man hinh view

    protected ViewFunction view;//dung de goi tu viewmodel ra tang view ham process



    protected MutableLiveData<Throwable> appException = new MutableLiveData<>();
    protected MutableLiveData<Integer> processing = new MutableLiveData<>();
    protected MutableLiveData<AlertModel> alertModel = new MutableLiveData<>();
    protected MyCallBack cb = new MyCallBack(appException);



    public BaseViewModel(@NonNull Application application) {
        super(application);
        baseModels = new ObservableField<>();
        model = new ObservableField<>();
    }


    public int getCount(){
        if(baseModels == null){
            return 0;
        }
        return baseModels.get().size();
    }

    protected MyCallBack callBack(ResponseResult result, Class<?> elem, Class<?> type){
        cb.setFunc(result);
        cb.setType(type);
        cb.setElement(elem);
        return cb;
    }
    protected MyCallBack callBackOne(ResponseResult result, Class<?> elem){
        cb.setFunc(result);
        cb.setType(null);
        cb.setElement(elem);
        return cb;
    }
    protected MyCallBack callBackList(ResponseResult result, Class<?> elem){
        cb.setFunc(result);
        cb.setType(List.class);
        cb.setElement(elem);
        return cb;
    }

    public void invokeFunc(String methodName, Object... params){
        try{
            processing.setValue(R.string.wait);
            Method method = null;
            Class[] arg = null;
            Method[] methods = getClass().getMethods();
            for( Method m: methods){
                if(methodName.equals(m.getName())){
                    method = m;
                    break;
                }
            }
            if(method == null){
                throw new NoSuchMethodException(methodName);
            }

            method.invoke(this, params);



        }catch (Throwable e){
            appException.setValue(e);
        }finally {
            processing.setValue(null);
        }
    }


    public List getCheckedItems() {
        List<BaseModel> lst = new ArrayList<>();
        for(BaseModel bm: baseModels.get()){
            if(bm.checked != null && bm.checked){
                lst.add(bm);
            }

        }
        return lst;
    }

    public ObservableField<List<BaseModel>> getModels() {
        return baseModels;
    }
    //    public  ObservableField<> getModels(){
//        return baseModels;
//    }
//    protected void processError(String code, Throwable t){
//        if(view != null){
//            view.action(code, null, this, t);
////            ownerView.onClicked(null, null, t);
//        }else{
//            t.printStackTrace();
//
//        }
//
//    }

    public void setView(ViewFunction function){
        this.view = function;
    }
    public void init() {

    }

//    @Bindable
    public ObservableField<T> getModel() {
        return model;
    }

    public void setModel(ObservableField<T> model) {
        this.model = model;
        model.notifyChange();
    }

    public MutableLiveData<Throwable> getAppException() {
        return appException;
    }

    public void setAppException(MutableLiveData<Throwable> appException) {
        this.appException = appException;
    }

    public MutableLiveData<Integer> getProcessing() {
        return processing;
    }

    public void setProcessing(MutableLiveData<Integer> processing) {
        this.processing = processing;
    }

    public MutableLiveData<AlertModel> getAlertModel() {
        return alertModel;
    }

    public void setAlertModel(MutableLiveData<AlertModel> alertModel) {
        this.alertModel = alertModel;
    }

    public BaseViewModel getViewModel() {
        return this;
    }


    public Object getListener(){
        return this;
    }
    public void setData(List<BaseModel> lst){
        List list = (List)baseModels.get();
        if(list == null){
            baseModels.set(lst);
        }else{
            list.clear();
            list.addAll(lst);
        }
        baseModels.notifyChange();
    }
    public void setData(ObservableField datas, List<BaseModel> lst){
        if(datas == null){
            datas = baseModels;
        }
        List list = (List)datas.get();
        if(list == null){
            datas.set(lst);
        }else{
            list.clear();
            list.addAll(lst);
        }
        datas.notifyChange();
    }
}
