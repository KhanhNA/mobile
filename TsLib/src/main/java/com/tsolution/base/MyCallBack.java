package com.tsolution.base;


import android.arch.lifecycle.MutableLiveData;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.base.listener.ResponseResult;

import lombok.Data;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Data
public class MyCallBack implements Callback<ResponseBody> {
    private ResponseResult func;
    private Class element;
    private Class type;
    private MutableLiveData<Throwable> appException;
    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {
            try {
                ResponseBody responseBody = response.body();
                Class<?> out;
                if (responseBody != null) {
                    String s = responseBody.string();
                    ObjectMapper objectMapper = new ObjectMapper();
                    Object value;
                    if(type == null){
                        value = objectMapper.readValue(s, element);
                    }else {
                        value = objectMapper.readValue(s, objectMapper.getTypeFactory().constructCollectionType(type, element));
                    }
//                    Object value = objectMapper.
                    //Method m = parent.getClass().getMethod(functionName, List.class);



                    //m.invokeFunc(parent, value);
                    if(func != null) {
                        func.onResponse(call, response, value, null);
                    }


                }
            } catch (Throwable e) {
                if(appException != null){
                    appException.setValue(e);
                }else {
                    if(func != null) {
                        func.onResponse(call, response, null, e);
                    }
                }
            }

        } else {
//                    handleResponseError(serviceName, response.code(), response.body());
            if(appException != null){
                appException.setValue(new AppException(response.code(), response.toString()));
            }else {
                if(func != null) {
                    func.onResponse(call, response, null, new AppException(response.code(), response.toString()));
                }
            }
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        if(appException != null){
            appException.setValue(t);
        }else {
            if(func != null) {
                func.onResponse(call, null, null, t);
            }
        }
    }

    public MyCallBack(MutableLiveData<Throwable> ex, ResponseResult result, Class<?> elem, Class<?> type) {
        this.func = result;
        this.element = elem;
        this.type = type;
        this.appException = ex;
    }
    public MyCallBack(MutableLiveData<Throwable> ex) {
        this.appException = ex;
    }

}
