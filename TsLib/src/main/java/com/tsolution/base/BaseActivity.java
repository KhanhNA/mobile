package com.tsolution.base;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.tsolution.base.listener.DefaultFunctionActivity;
import com.tsolution.base.listener.ViewActionsListener;

public  abstract class BaseActivity extends AppCompatActivity  implements ViewActionsListener, DefaultFunctionActivity {
    protected ProgressDialog.Builder pdb;
    protected ProgressDialog pd;
    private AlertDialog.Builder alertDialog;
    protected BaseViewModel viewModel;
    protected ViewDataBinding binding;


    public BaseActivity() {

    }

    public ProgressDialog.Builder getPdb() {
        return pdb;
    }

    public void setPdb(ProgressDialog.Builder pdb) {
        this.pdb = pdb;
    }

    public ProgressDialog getPd() {
        return pd;
    }

    public void setPd(ProgressDialog pd) {
        this.pd = pd;
    }

    public void setAlertDialog(AlertDialog.Builder alertDialog) {
        this.alertDialog = alertDialog;
    }

    public AlertDialog.Builder getAlertDialog() {
        return alertDialog;
    }



    @Override
    public BaseActivity getBaseActivity() {
        return this;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try {
            viewModel = getVMClass().newInstance(); //(T) ViewModelProviders.of(this).get(getVMClass());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        binding = DataBindingUtil.setContentView(this, getLayoutRes());






        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        ApplicationCrashHandler.installHandler(viewModel.getAppException(), this, savedInstanceState);
        viewModel.setView(this::processFromVM);
        binding.setVariable(BR.viewModel,viewModel);
        binding.setVariable(BR.listener, this);

        viewModel.getAppException().observe(this, ex -> {
            processError("error",null, viewModel, (Throwable) viewModel.getAppException().getValue());

        });
        viewModel.getProcessing().observe(this, b -> {
            Integer l = (Integer)viewModel.getProcessing().getValue();
            if( l== null)
                closeProcess();
            else
                showProcessing(l);

        });
//        viewModel.getAlertModel().observe(this, alert -> {
//            AlertModel a = (AlertModel) alert;
//            showAlertDialog(a.msg,a.funcPositive);
//
//
//        });



//        Thread thread = Thread.currentThread();
//        thread.setUncaughtExceptionHandler((v, exc) ->
//        {
//            handleError(this,this.getLocalClassName(),Constants.CODE.FATAL_ERROR,exc);
//        });
    }


    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        showProcessing(getResources().getString(R.string.wait));
    }


    public BaseViewModel getViewModel() {
        return viewModel;
    }


    public Object getListener(){
        return this;
    }


    @Override
    final public void onClicked(View view, BaseViewModel viewModel) {
        ViewActionsListener.super.onClicked(view,viewModel);
    }
    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }
}
