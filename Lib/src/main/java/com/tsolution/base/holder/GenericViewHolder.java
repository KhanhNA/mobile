package com.tsolution.base.holder;


import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.tsolution.base.BR;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterActionsListener;


public class GenericViewHolder extends RecyclerView.ViewHolder {
    final ViewDataBinding  binding;

    public GenericViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;

    }
    public void setBinding(BaseModel obj, BaseViewModel viewModel, AdapterActionsListener listener) {
        binding.setVariable(BR.viewHolder, obj);
        binding.setVariable(BR.viewModel, viewModel);
        binding.setVariable(BR.listener, listener);

//        obj.bindingAction();
        binding.executePendingBindings();
    }



}
