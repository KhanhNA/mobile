package com.tsolution.base;


import android.arch.lifecycle.MutableLiveData;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.tsolution.base.BR;
import com.tsolution.base.listener.ResponseResult;
import com.tsolution.base.listener.ViewFunction;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class BaseViewModel<T> extends BaseObservable {

    protected T model;//du lieu o man hinh view
    public ObservableList<BaseModel> baseModels; //danh sach trong recycleview o man hinh view

    protected ViewFunction view;//dung de goi tu viewmodel ra tang view ham process

    public BaseViewModel() {
        baseModels = new ObservableArrayList<>();
    }


    protected MutableLiveData<Throwable> appException = new MutableLiveData<>();
    protected MutableLiveData<Integer> processing = new MutableLiveData<>();
    protected MutableLiveData<AlertModel> alertModel = new MutableLiveData<>();
    protected MyCallBack cb = new MyCallBack(appException);
    public int getCount(){
        if(baseModels == null){
            return 0;
        }
        return baseModels.size();
    }

    protected MyCallBack callBack(ResponseResult result, Class<?> elem, Class<?> type){
        cb.setFunc(result);
        cb.setType(type);
        cb.setElement(elem);
        return cb;
    }
    protected MyCallBack callBackOne(ResponseResult result, Class<?> elem){
        cb.setFunc(result);
        cb.setType(null);
        cb.setElement(elem);
        return cb;
    }
    protected MyCallBack callBackList(ResponseResult result, Class<?> elem){
        cb.setFunc(result);
        cb.setType(List.class);
        cb.setElement(elem);
        return cb;
    }

    public void invokeFunc(String methodName, Object... params){
        try{
            processing.setValue(R.string.wait);
            Method method = null;
            Class[] arg = null;
            Method[] methods = getClass().getMethods();
            for( Method m: methods){
                if(methodName.equals(m.getName())){
                    method = m;
                    break;
                }
            }
            if(method == null){
                throw new NoSuchMethodException(methodName);
            }

            method.invoke(this, params);



        }catch (Throwable e){
            appException.setValue(e);
        }finally {
            processing.setValue(null);
        }
    }


    public List getCheckedItems() {
        List<BaseModel> lst = new ArrayList<>();
        for(BaseModel bm: baseModels){
            if(bm.checked != null && bm.checked){
                lst.add(bm);
            }

        }
        return lst;
    }
    public  ObservableList getModels(){
        return baseModels;
    }
//    protected void processError(String code, Throwable t){
//        if(view != null){
//            view.action(code, null, this, t);
////            ownerView.onClicked(null, null, t);
//        }else{
//            t.printStackTrace();
//
//        }
//
//    }

    public void setView(ViewFunction function){
        this.view = function;
    }
    public void init() {

    }

    @Bindable
    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
        notifyPropertyChanged(BR.model);
    }

    public MutableLiveData<Throwable> getAppException() {
        return appException;
    }

    public void setAppException(MutableLiveData<Throwable> appException) {
        this.appException = appException;
    }

    public MutableLiveData<Integer> getProcessing() {
        return processing;
    }

    public void setProcessing(MutableLiveData<Integer> processing) {
        this.processing = processing;
    }

    public MutableLiveData<AlertModel> getAlertModel() {
        return alertModel;
    }

    public void setAlertModel(MutableLiveData<AlertModel> alertModel) {
        this.alertModel = alertModel;
    }
    @Bindable
    public BaseViewModel getViewModel() {
        return this;
    }

    @Bindable
    public Object getListener(){
        return this;
    }
}
