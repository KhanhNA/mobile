//
//  UIStoryboard+Extension.swift
//  snapchatprank
//
//  Created by trungnd on 2/28/17.
//  Copyright © 2017 redvn. All rights reserved.
//

import UIKit

protocol Storyboardable {
    static var storyboardName: String { get }
    static var identifier: String { get }
}

extension UIStoryboard {
    static func loadViewController<T: Storyboardable>(_ classType: T.Type) -> T {
        guard let vc = UIStoryboard(name: classType.storyboardName,
                                    bundle: nil).instantiateViewController(withIdentifier: classType.identifier) as? T else {
            fatalError("Cannot instantiate view controller: \(classType.identifier)")
        }
        return vc
    }
}

extension Storyboardable where Self: UIViewController {
    static var identifier: String {
        return String(describing: self)
    }
}
