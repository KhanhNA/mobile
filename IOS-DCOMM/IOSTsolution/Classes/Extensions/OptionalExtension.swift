//
//  OptionalExtension.swift
//  CiPPo
//
//  Created by ntq on 3/12/19.
//  Copyright © 2019 ntq. All rights reserved.
//

import Foundation

extension Optional where Wrapped: Collection {
    var isNilOrEmpty: Bool {
        switch self {
        case .some(let collection):
            return collection.isEmpty
        case .none:
            return true
        }
    }
}
