//
//  UITableView+Extension.swift
//  VirtuaLive
//
//  Created by Le Duc Canh on 8/3/18.
//  Copyright © 2018 Danh Hung. All rights reserved.
//

import UIKit

extension UITableView {
    func registerCellNib(_ cellClass: AnyClass) {
        let className = String(describing: cellClass.self)
        register(UINib(nibName: className, bundle: nil), forCellReuseIdentifier: className)
    }
    
    func registerCellClass(_ cellClass: AnyClass) {
        register(cellClass, forCellReuseIdentifier: String(describing: cellClass.self))
    }
    
    func reloadData(completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }) { (finish) in
            completion()
        }
    }

    public func register<T: UITableViewCell>(_ cell: T.Type, for type: CellType){
        let cellNib = UINib(nibName: T.nibName, bundle: nil)
        if type == .Cell {
            self.register(cellNib, forCellReuseIdentifier: T.nibName)
        } else if type == .Header || type == .Footer{
            self.register(cellNib, forHeaderFooterViewReuseIdentifier: T.nibName)
        }
    }

    public func dequeueHeaderFooterCell<T: UITableViewHeaderFooterView>() -> T{
        let sectionCell = self.dequeueReusableHeaderFooterView(withIdentifier: T.nibName)
        return (sectionCell ?? UITableViewHeaderFooterView()) as! T
    }

    public func dequeueCell<T: UITableViewCell>(_ indexPath: IndexPath) -> T{
        return self.dequeueReusableCell(withIdentifier: T.nibName, for: indexPath) as! T
    }

    public func dequeueCell<T: UITableViewCell>() -> T{
        let cell = self.dequeueReusableCell(withIdentifier: T.nibName)
        if cell == nil { print("BaseCell: Can't dequeue cell. Check if cell not register") }
        return (cell ?? UITableViewCell()) as! T
    }
}
