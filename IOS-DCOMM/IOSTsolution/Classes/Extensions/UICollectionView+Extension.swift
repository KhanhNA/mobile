//
//  UICollectionView+Extension.swift
//  VirtuaLive
//
//  Created by Le Duc Canh on 8/3/18.
//  Copyright © 2018 Danh Hung. All rights reserved.
//

import UIKit

extension UICollectionView {
    func registerCellNib(_ cellClass: AnyClass) {
        register(UINib(nibName: getClassName(cellClass.self), bundle: nil),
                 forCellWithReuseIdentifier: getClassName(cellClass.self))
    }
    
    func registerCellClass(_ cellClass: AnyClass) {
        register(cellClass, forCellWithReuseIdentifier: String(describing: cellClass.self))
    }
    
    func registerHeaderNib(_ headerClass: AnyClass) {
        register(UINib(nibName: getClassName(headerClass.self), bundle: nil),
                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                 withReuseIdentifier: getClassName(headerClass.self))
    }
    
    func registerSectionFooterNib(_ headerClass: AnyClass) {
        register(UINib(nibName: getClassName(headerClass.self), bundle: nil),
                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                 withReuseIdentifier: getClassName(headerClass.self))
    }
    
    func dequeueReusableHeaderView(_ headerClass: AnyClass, for indexPath: IndexPath) -> UICollectionReusableView {
        return dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                withReuseIdentifier: getClassName(headerClass.self),
                                                for: indexPath)
    }
    
    private func getClassName(_ aClass: AnyClass) -> String {
        return String(describing: aClass.self)
    }
    
    
    public func register<T: UICollectionViewCell>(_ cell: T.Type, for type: CellType){
        
        let cellNib = UINib(nibName: T.nibName, bundle: nil)
        if type == .Cell{
            self.register(cellNib, forCellWithReuseIdentifier: T.nibName)
        } else if type == .Header{
            #if swift(>=4.2)
            self.register(cellNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.nibName)
            #else
            self.register(cellNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: T.nibName)
            #endif
        } else if type == .Footer{
            #if swift(>=4.2)
            self.register(cellNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.nibName)
            #else
            self.register(cellNib, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: T.nibName)
            #endif
        }
    }
    
    public func dequeueCell<T: UICollectionViewCell>(_ indexPath: IndexPath) -> T{
        return self.dequeueReusableCell(withReuseIdentifier: T.nibName, for: indexPath) as! T
    }
    
    public func dequeueCell<T: UICollectionViewCell>(for kind: String,at indexPath: IndexPath) -> T{
        return self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: T.nibName, for: indexPath) as! T
    }
}
