//
//  TextExtension.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/19/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation

struct TextMenu {
    static let DSdonmua     = "Danh sách đơn mua"
//    static let DSdonmua     = NSLocalizedString("lbl_list_order", comment: "")
    static let DSdonmua2    = "Danh sách đơn mua cấp 2"
    static let Dlycap1      = "Đại lý cấp 1"
    static let Quydoi       = "Quy đổi"
    static let Vi           = "Ví"
    static let Csefacebook  = "Chia sẻ facebook"
    static let caidat       = "Cài đặt"
    static let TTTgiup      = "Trung tâm trợ giúp"
    static let dangxuat     = "Đăng xuất"
}
