//
//  UIIMage.swift
//  SlideMenuControllerSwift
//

import UIKit

extension UIButton {

    func setUnderLine() {
        if let title = self.titleLabel?.text {
            let attributeString = NSMutableAttributedString(string: title,
                                                            attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            self.setAttributedTitle(attributeString, for: .normal)
        }
    }

    func centerImageAndButton(_ gap: CGFloat, imageOnTop: Bool = true) {
        guard let imageView = currentImage, let titleLabel = titleLabel?.text else { return }
        let sign: CGFloat = imageOnTop ? 1 : -1
        self.titleEdgeInsets = UIEdgeInsets(top: (imageView.size.height + gap) * sign,
                                            left: -imageView.size.width, bottom: 0, right: 0)
        let titleSize = titleLabel.size(withAttributes:[NSAttributedString.Key.font: self.titleLabel!.font!])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + gap) * sign, left: 0, bottom: 0, right: -titleSize.width)
    }

}
