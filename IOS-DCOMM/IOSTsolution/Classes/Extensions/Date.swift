//
//  StringExtension.swift
//  SlideMenuControllerSwift
//

import Foundation

extension Double {
    func toDateStringFromTimeStamp(format: String) -> String? {
        guard !format.isEmpty else { return nil }
        let date = Date(timeIntervalSince1970: self)
        Formatter.shared.dateFormatter.dateFormat = format
        return Formatter.shared.dateFormatter.string(from: date)
    }
}

extension Date {
    
    func stringRepresentation(format: String, timeZone: TimeZone? = TimeZone.current) -> String? {
        guard !format.isEmpty else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        let locale = Locale(identifier: "ja_JP")
        dateFormatter.locale = locale
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone
        return dateFormatter.string(from: self)
    }
    
    // Date debug log
    func dblog() -> String {
        return AppFormatters.debugConsoleDateFormatter.string(from: self)
    }
    /// Returns the a custom time interval description from another date
//    func offset(from date: Date) -> String {
//        if years(from: date)   > 0 { return "\(years(from: date))y"   }
//        if months(from: date)  > 0 { return "\(months(from: date))M"  }
//        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
//        if days(from: date)    > 0 { return "\(days(from: date))d"    }
//        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
//        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
//        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
//        return ""
//    }
    func getDayOfWeek() -> Int? {
        if let myCalendar = NSCalendar(calendarIdentifier: .gregorian) {
            let myComponents = myCalendar.components(.weekday, from: self)
            let weekDay = myComponents.weekday
            return weekDay
        } else {
            return nil
        }
    }

    func getMonth() -> Int {
        let calendar = Calendar.current
        return calendar.component(.month, from: self)
    }
    
    func toStringLocalTime(_ format: String) -> String {
        Formatter.shared.dateFormatter.dateFormat = format
        return Formatter.shared.dateFormatter.string(from: self)
    }
    
    func toStringJPTime(_ format: String) -> String {
        Formatter.shared.dateFormatter.dateFormat = format
        Formatter.shared.dateFormatter.locale = Locale(identifier: "ja_JP")
        return Formatter.shared.dateFormatter.string(from: self)
    }
}

class Formatter: NSObject {
    
    // MARK: - Properties
    static let shared = Formatter()

    public let dateFormatter = DateFormatter()
    
    // MARK: - Initializer
    private override init() {}
    
    // MARK: - Methods
    public func convertFromUTC(_ strDate: String) -> Date? {
        dateFormatter.dateFormat = AppFormatters.ISO8601
        return dateFormatter.date(from: strDate)
    }
    
    public func convertToLocal(_ date: Date, with format: String) -> String {
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: date)
    }
}

extension Date {
//    func stringDateComment() -> String? {
//        if self.isToday { return LS("TO_DAY") }
//        // same year
//        if self.yearsAgo == 0 { return self.toStringLocalTime(AppFormatters.JPdateMonth) }
//
//        return self.toStringLocalTime(AppFormatters.dateRegisterFormat)
//    }

    func isSameYear() -> Bool {
        let componentsCurrent = Calendar.current.dateComponents([.year], from: Date())
        let componentsTarget = Calendar.current.dateComponents([.year], from: self)
        if componentsCurrent.year == componentsTarget.year {
            return true
        }
        
        return false
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var startOfMonth: Date {
        let components = Calendar.current.dateComponents([.year, .month], from: startOfDay)
        return Calendar.current.date(from: components)!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfMonth)!
    }
}
