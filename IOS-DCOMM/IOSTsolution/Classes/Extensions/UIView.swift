//
//  UIView.swift
//  SlideMenuControllerSwift
//

import UIKit

extension UIView {
    
    //Load Nib View
    public func loadContentIfNeed(){
        if let contentView = Bundle.main.loadNibNamed(self.className, owner: self, options: nil)?.first as? UIView {
            self.addSubview(contentView)
            contentView.translatesAutoresizingMaskIntoConstraints = false
            contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
            contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
            contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        } else {
            fatalError("Can't Load Nib View Content")
        }
    }
    
    class func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        let className = String.className(viewType)
        guard let view = Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)?.first as? T else {
            fatalError()
        }
        return view
    }
    
    class func loadNib() -> Self {
        return loadNib(self)
    }

    class func fromNib<T: UIView>(bundle: Bundle) -> T {
        return bundle.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        self.layer.add(animation, forKey: nil)
    }
    
    // Usage: insert view.pushTransition right before changing content
    func pushTransition(duration: CFTimeInterval) {
        let animation: CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromBottom
        animation.duration = duration
        self.layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
    
    func width() -> CGFloat {
        return self.bounds.size.width
    }
    
    func height() -> CGFloat {
        return self.bounds.size.height
    }
    
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as? UIView
    }
    
    func dropShadow(color: UIColor, opacity: Float = 1, offSet: CGSize = CGSize(width: 0, height: 0), radius: CGFloat = 6, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func applySketchShadow(color: UIColor = .black,
                           alpha: Float = 2,
                           x: CGFloat = 0,
                           y: CGFloat = 0,
                           radius: CGFloat = 10,
                           spread: CGFloat = 0) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = alpha
        self.layer.shadowOffset = CGSize(width: x, height: y)
        self.layer.shadowRadius = radius
        if spread == 0 {
            self.layer.shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            self.layer.shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    
    // MARK: - IB Helpers
    @IBInspectable var borderColor: UIColor? {
        get {
            guard let borderColor = layer.borderColor else { return UIColor.clear }
            return UIColor(cgColor:  borderColor)
        }
        set {
            self.layer.borderColor = (newValue ?? UIColor.clear).cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }
    
    @IBInspectable var masksToBounds: Bool {
        get { return layer.masksToBounds }
        set { layer.masksToBounds = newValue }
    }
    
    
    @IBInspectable var clipsToBoundsView: Bool {
        get { return clipsToBounds }
        set { clipsToBounds = newValue }
    }
    
    func setBorder(_ borderWidth: CGFloat = 1, borderColor: UIColor = .lightGray, radius: CGFloat = 3) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }

    func setContraint() {
        guard let _superView = superview else { return }
        self.translatesAutoresizingMaskIntoConstraints = false

        self.addConstraint(NSLayoutConstraint.init(item: self, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1.0, constant: self.bounds.width))
        self.addConstraint(NSLayoutConstraint.init(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1.0, constant: self.bounds.height))
        NSLayoutConstraint.activate([
            self.centerXAnchor.constraint(equalTo: _superView.centerXAnchor, constant: 0)
            ])

        if #available(iOS 11, *) {
            let guide = _superView.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                self.topAnchor.constraint(equalTo: guide.topAnchor, constant: self.frame.origin.y)
                ])
        } else {
            NSLayoutConstraint.activate([
                self.topAnchor.constraint(equalTo: _superView.layoutMarginsGuide.bottomAnchor, constant: self.frame.origin.y)
                ])
        }
    }

    func makeContraintToFullWithParentView() {
        guard let parrentView = self.superview else {
            return
        }
        let dict = ["view":self]
        self.translatesAutoresizingMaskIntoConstraints = false
        parrentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: dict))
        parrentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: dict))
    }
    
    public func findAll<T>(_ viewType: T.Type) -> [T] {
        var items = [T]()
        for child in self.subviews {
            if let obj = child as? T {
                items.append(obj)
            }
            
            let result = child.findAll(viewType)
            if result.count > 0 {
                items.append(contentsOf: result)
            }
        }
        return items
    }

}

extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
