
import UIKit

extension Int {
    func toString(numberStyle: NumberFormatter.Style = .decimal,
                  maximumFractionDigits: Int = 2,
                  locale: String? = "ja_JP") -> String {
        
        let outputString = NSNumber(value: self).toString(numberStyle: numberStyle,
                                                          maximumFractionDigits: maximumFractionDigits,
                                                          locale: locale)
        return outputString
    }
}

extension Double {
    func toString(numberStyle: NumberFormatter.Style = .decimal,
                  maximumFractionDigits: Int = 2,
                  locale: String? = "ja_JP") -> String {
        
        let outputString = NSNumber(value: self).toString(numberStyle: numberStyle,
                                                          maximumFractionDigits: maximumFractionDigits,
                                                          locale: locale)
        return outputString
    }
}

extension NSNumber {
    func toString(numberStyle: NumberFormatter.Style = .decimal,
                  maximumFractionDigits: Int = 2,
                  locale: String? = "ja_JP") -> String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = numberStyle
        formatter.maximumFractionDigits = maximumFractionDigits
        if let locale_ = locale {
            formatter.locale = Locale(identifier: locale_)
        }
        
        let outputString = formatter.string(from: self) ?? ""
        return outputString
    }
}


//MARK: Random

extension Int {
     static func random(min: Int, max: Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
}
