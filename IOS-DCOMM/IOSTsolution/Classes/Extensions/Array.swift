//
//  Array.swift
//  GEnglish
//
//  Created by Taopd on 9/25/17.
//  Copyright © 2017 Taopd. All rights reserved.
//

import Foundation

extension Array {
    /// Returns an array containing this sequence shuffled
    var shuffled: Array {
        var elements = self
        return elements.shuffle()
    }
    /// Shuffles this sequence in place
    @discardableResult
    mutating func shuffle() -> Array {
        indices.dropLast().forEach { position in
            guard case let b = Int(arc4random_uniform(UInt32(count - position))) + position, b != position else {
                return
            }
            self.swapAt(position, b)
        }
        return self
    }
    var chooseOne: Element { return self[Int(arc4random_uniform(UInt32(count)))] }
    func choose(_ index: Int) -> Array {
        return Array(shuffled.prefix(index))
    }
    func rearrange (fromIndex: Int, toIndex: Int) -> Array {
        var arr = self
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        return arr
    }
}

extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
