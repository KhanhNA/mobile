//
//  NSDecimalNumber+Extension.swift
//  sapco
//
//  Created by Eddie Luke Atmey on 12/27/17.
//  Copyright © 2017 Salesforce. All rights reserved.
//

import Foundation

extension NSDecimalNumber {

    class func bs_decimalNumber(with anotherNumber: NSNumber) -> NSDecimalNumber {
        return NSDecimalNumber(decimal: anotherNumber.decimalValue)
    }

    func bs_absoluteValue() -> NSDecimalNumber {
        if self.bs_isSmallerThan(NSDecimalNumber.zero) {
            return multiplying(by: NSDecimalNumber(mantissa: 1, exponent: 0, isNegative: true))
        }
        return self
    }

    func bs_isGreaterThan(_ anotherNumber: NSNumber) -> Bool {
        return self.compare(anotherNumber) == .orderedDescending
    }

    func bs_isEqual(to anotherNumber: NSNumber) -> Bool {
        return self.compare(anotherNumber) == .orderedSame
    }

    func bs_isSmallerThan(_ anotherNumber: NSNumber) -> Bool {
        return self.compare(anotherNumber) == .orderedAscending
    }

// Enable after remove pod 'Money'
//    static func >  (left: NSDecimalNumber, right: NSDecimalNumber) -> Bool { return left.bs_isGreaterThan(right) }
//    static func >= (left: NSDecimalNumber, right: NSDecimalNumber) -> Bool { return !left.bs_isSmallerThan(right) }
//    static func <  (left: NSDecimalNumber, right: NSDecimalNumber) -> Bool { return left.bs_isSmallerThan(right) }
//    static func <= (left: NSDecimalNumber, right: NSDecimalNumber) -> Bool { return !left.bs_isGreaterThan(right) }

    // Non-Opt
    static func + (left: NSDecimalNumber, right: NSDecimalNumber) -> NSDecimalNumber { return left.adding(right) }
    static func - (left: NSDecimalNumber, right: NSDecimalNumber) -> NSDecimalNumber { return left.subtracting(right) }
    static func * (left: NSDecimalNumber, right: NSDecimalNumber) -> NSDecimalNumber { return left.multiplying(by: right) }
    static func / (left: NSDecimalNumber, right: NSDecimalNumber) -> NSDecimalNumber { return left.dividing(by: right) }

    static func += (left: inout NSDecimalNumber, right: NSDecimalNumber) { left = left + right }
    static func -= (left: inout NSDecimalNumber, right: NSDecimalNumber) { left = left - right }
    static func *= (left: inout NSDecimalNumber, right: NSDecimalNumber) { left = left * right }
    static func /= (left: inout NSDecimalNumber, right: NSDecimalNumber) { left = left / right }

    // Optional
    static func + (left: NSDecimalNumber?, right: NSDecimalNumber) -> NSDecimalNumber? { return left?.adding(right) }
    static func - (left: NSDecimalNumber?, right: NSDecimalNumber) -> NSDecimalNumber? { return left?.subtracting(right) }
    static func * (left: NSDecimalNumber?, right: NSDecimalNumber) -> NSDecimalNumber? { return left?.multiplying(by: right) }
    static func / (left: NSDecimalNumber?, right: NSDecimalNumber) -> NSDecimalNumber? { return left?.dividing(by: right) }

    static func += (left: inout NSDecimalNumber?, right: NSDecimalNumber) { left = left + right }
    static func -= (left: inout NSDecimalNumber?, right: NSDecimalNumber) { left = left - right }
    static func *= (left: inout NSDecimalNumber?, right: NSDecimalNumber) { left = left * right }
    static func /= (left: inout NSDecimalNumber?, right: NSDecimalNumber) { left = left / right }
}

extension BinaryFloatingPoint {
//    var dcmn: NSDecimalNumber {
//        guard let stringInterpolation = self as? DefaultStringInterpolation else { return .zero }
//        let dcm = NSDecimalNumber(string: String(stringInterpolation: stringInterpolation))
//        return dcm == .notANumber ? .zero : dcm
//    }
}

extension BinaryInteger {
//    var dcmn: NSDecimalNumber {
//        guard let stringInterpolation = self as? DefaultStringInterpolation else { return .zero }
//        let dcm = NSDecimalNumber(string: String(stringInterpolation: stringInterpolation))
//        return dcm == .notANumber ? .zero : dcm
//    }
}

