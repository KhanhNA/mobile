//
//  UIAlertView+UIImagePickerViewController.swift
//  CiPPo
//
//  Created by ntq on 12/24/18.
//  Copyright © 2018 ntq. All rights reserved.
//

import UIKit
import Photos
import AssetsLibrary

protocol PhotoPickerProtocol: UIImagePickerControllerDelegate & UINavigationControllerDelegate {}

extension PhotoPickerProtocol where Self: UIViewController {
    
    func alertSelectPhoto(_ sender: AnyObject, removePhotoHandler: (() -> Void)? = nil) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: LS("ACTION_SHEET_CAMERA"), style: .default) { _ in self.showImagePickerForSourceType(sourceType: .camera) })
        alert.addAction(UIAlertAction(title: LS("ACTION_SHEET_ALBUM"), style: .default) { _ in self.showImagePickerForSourceType(sourceType: .photoLibrary) })
        if removePhotoHandler != nil {
            alert.addAction(UIAlertAction(title: LS("GLOBAL_CANCEL"), style: .destructive) { _ in removePhotoHandler?() })
        }
        
        alert.addAction(UIAlertAction(title: LS("GLOBAL_CANCEL"), style: .cancel, handler: nil))
        if Screen.isIpad {
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as! UIButton
                popoverController.sourceRect = (sender as! UIButton).bounds
            }
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    private func showImagePickerForSourceType(sourceType: UIImagePickerController.SourceType) {
        
        guard UIImagePickerController.isSourceTypeAvailable(sourceType) else { return }
        if sourceType == .camera { accessToCamera() }
        else { accessToPhotoLibrary() }
    }
    
    private func accessToCamera() {
        requestForCamera { [weak self] (success) in
            DispatchQueue.main.async {
                if success { self?.show(.camera); return }
                
                // TODO: Ask for actual message
                let alert  = UIAlertController(title: LS("GLOBAL_WARNING"), message: LS("ALERT_NO_PERMISSION_CAMERA"), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: LS("GLOBAL_OK"), style: .default, handler: { _ in
                    Utils.showAppSetting()
                }))
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
    private func accessToPhotoLibrary() {
        requestForPhotoLibrary { [weak self] (success) in
            DispatchQueue.main.async {
                if success { self?.show(.photoLibrary); return }
                
                // TODO: Ask for actual message
                let alert  = UIAlertController(title: LS("GLOBAL_WARNING"), message: LS("ALERT_NO_PERMISSION_LIBRARY"), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: LS("GLOBAL_OK"), style: .default, handler: { _ in
                    Utils.showAppSetting()
                }))
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func show(_ type: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        //imagePicker.allowsEditing = false
        imagePicker.sourceType = type
        self.present(imagePicker, animated: true, completion: nil)
    }
}
