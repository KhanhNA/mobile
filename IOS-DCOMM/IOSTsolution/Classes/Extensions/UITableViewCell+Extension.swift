//
//  UITableViewCell+Extension.swift
//  UPS
//
//  Created by QuangDV on 2019/01/18.
//  Copyright © 2019 Le Duc Canh. All rights reserved.
//

import UIKit


public enum CellType{
    case Cell, Header, Footer
}

public protocol IBaseCell {
    static var nibName : String { get }
    func binding(_ data: Any?)
}

//MARK: Access Implement IBaseCell
extension IBaseCell where Self : UITableViewCell{
    public static var nibName : String {
        return String(describing: self)
    }
}
extension IBaseCell where Self : UITableViewHeaderFooterView{
    public static var nibName : String {
        return String(describing: self)
    }
}
extension IBaseCell where Self : UICollectionReusableView {
    public static var nibName : String {
        return String(describing: self)
    }
}

//MARK: Default Implement IBaseCell For TableView, CollectionView
extension UITableViewCell: IBaseCell{
    @objc public func binding(_ data: Any?) {
    }
}
extension UITableViewHeaderFooterView: IBaseCell{
    @objc public func binding(_ data: Any?) {
    }
}
extension UICollectionViewCell: IBaseCell{
    @objc public func binding(_ data: Any?) {
    }
}
