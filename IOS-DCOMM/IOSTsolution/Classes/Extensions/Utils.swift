//
//  Utils.swift
//  SmartMe
//
//  Created by AQU on 5/22/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    
    static func bundleVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    }
    
    static func bundleVersionString() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    static func checkVersionLessThan(_ currentVersion: String, _ storeVersion: String) -> Bool {
        if storeVersion.compare(currentVersion, options: .numeric) == .orderedDescending {
            return true
        }
        return false
    }
    
    /// Function open an url in browser
    ///
    /// - Parameter url: url will open.
    static func openURL(_ url: String) {
        if url.count == 0 {
            return
        }
        let urlOpen: URL = URL(string: url)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(urlOpen, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(urlOpen)
        }
    }
    
    static let today = Date()
    
    static func showAlert(controller: UIViewController?,
                          title: String? = nil, message: String? = nil,
                          nameBtnDismiss: String? = nil,
                          nameBtnAccept: String,
                          handler: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        if nameBtnDismiss != nil {
            alertController.addAction(UIAlertAction(title: nameBtnDismiss,
                                                    style: .default, handler: nil
            ))
        }
        alertController.addAction(UIAlertAction(title: nameBtnAccept,
                                                style: .default, handler: {
                                                    alert -> Void in
                                                    if let completionHandler = handler {
                                                        completionHandler()
                                                    }
        }))
        controller?.present(alertController, animated: true, completion: nil)
    }
    
    // Show App Setting
    static func showAppSetting() {
        DispatchQueue.main.async {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    // Checking for setting is opened or not
                    DEBUGLog("Setting is opened: \(success)")
                })
            }
        }
    }
}
