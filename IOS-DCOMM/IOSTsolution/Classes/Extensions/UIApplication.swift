//
//  UIApplication.swift
//  SlideMenuControllerSwift
//

import UIKit

extension UIApplication {
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        return viewController
    }
    
    class func tabbarViewController(_ keyWindow: UIWindow) -> UITabBarController? {
        if let tabController = keyWindow.rootViewController as? UITabBarController {
            return tabController
        }
        if let vc = UIApplication.topViewController() {
            if let tabController = vc.tabBarController {
                return tabController
            }            
        }
        
        return nil
    }
}

extension Equatable {
    func shareAction(_ barButton: UIBarButtonItem) {
        let activity = UIActivityViewController(activityItems: [self], applicationActivities: nil)
        if Screen.isIpad {
            activity.popoverPresentationController?.barButtonItem = barButton
        }
        
        UIApplication.topViewController()?.present(activity, animated: true, completion: nil)
    }
}
