//
//  BaseViewController.swift
//  SmartMe
//
//  Created by AQU on 5/22/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import UIKit
import SwiftMessages

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setColorTitleNavi()
        self.navigationController?.navigationBar.tintColor = .white
    }
    
//    func showMessage(message: String, type: MessageType) {
//        let model = MessageModel(icon: MessageType.icon(with: type.key), title: type.rawValue, subTitle: message)
//        MessageCustomView.showMessageView(model: model) { [weak self] _ in
//        }
//    }
    
    func creatBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.popViewController))
        self.navigationItem.leftBarButtonItem = backBtn
    }

    @objc func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }

}
