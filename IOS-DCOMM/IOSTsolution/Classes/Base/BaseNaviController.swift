//
//  BaseNaviController.swift
//  SmartMe
//
//  Created by ntq on 5/23/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import UIKit

protocol BaseNaviControllerProtocol {
    
}

class BaseNaviController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
      //  setupNaviController()
    }
    
    private func setupNaviController() {
      setupGradientNavi()
    }
    
    private func setupGradientNavi() {
        let gradientLayer = CAGradientLayer()
        var updatedFrame = self.navigationBar.bounds
        updatedFrame.size.height += UIApplication.shared.statusBarFrame.size.height
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor(hex: ColorHex.f2a66a).cgColor, UIColor(hex: ColorHex.e76b85).cgColor, UIColor(hex: ColorHex.ea7184).cgColor] // start color and end color
        gradientLayer.startPoint = CGPoint(x: 1.2, y: 0.3) // Horizontal gradient start
        gradientLayer.locations = [0, 0.3, 1 ];
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0) // Horizontal gradient end
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
    }
}

public extension UINavigationController {
    func setTransparentNavi() {
        self.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationBar.shadowImage = nil
    }
    
    func setColorTitleNavi(_ color: UIColor = .white) {
        let titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: color
        ]
        self.navigationBar.titleTextAttributes = titleTextAttributes
    }
}
