//
//  BaseMVPProtocol.swift
//
//  Created by Taopd on 4/8/18.
//  Copyright © 2018 Taopd. All rights reserved.
//

import UIKit

protocol BaseMVPProtocol {
    func attachView(_ view: Any)
}

protocol StoryboardBased: class {
    static var storyboard: UIStoryboard { get }
}

extension Storyboardable {
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: String(describing: self), bundle: nil)
    }
}

extension StoryboardBased where Self: UIViewController {
    static func instantiate() -> Self {
        guard let vc = storyboard.instantiateInitialViewController() as? Self else {
            fatalError("The VC of \(Self.className) is not of class \(self)")
        }
        return vc
    }
}

protocol presenter {
    associatedtype T
    var view: T? { get set }

    func attachView(view: T?)
    func detach()
}

protocol Presenter: class {
    associatedtype T
    var view: T? { get set }

    func attachView(view: T?)
}

extension Presenter {
    func attachView(view: T?) {
        self.view = view
    }
}

class BasePresenter<K>: Presenter {
    typealias T = K
    var view: K?

    func attachView(view: K?) {
        self.view = view
    }
}

protocol BaseVCProtocol: class {
    associatedtype P: Presenter
    var presenter: P { get set }

    func createPresenter() -> P
}


