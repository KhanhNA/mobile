//
//  Dispatch.swift
//  iQRVN
//
//  Created by Taopd on 4/25/17.
//  Copyright © 2017 Taopd. All rights reserved.
//

import Foundation

class Dispatch: NSObject {
    static func async(_ closure: @escaping () -> Void) {
        DispatchQueue.main.async {
            closure()
        }
    }
    
    static func asyncAfter(_ timeDelay: Double, _ closure: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + timeDelay, execute: {
            closure()
        })
    }
}
