//
//  AppColor.swift
//  SmartMe
//
//  Created by AQU on 5/22/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import UIKit

struct AppColor {
    static let Primary: UIColor = UIColor(rgb: 0x000000)
}
