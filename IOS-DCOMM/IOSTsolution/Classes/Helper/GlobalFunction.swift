//
//  GlobalFunction.swift
//  SmartMe
//
//  Created by AQU on 5/22/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import Foundation
import UIKit
import Photos

func showAlertMessageError(title: String, message: String? = nil, titleButtonOk: String = LS("GLOBAL_OK"), titleCancel: String? = nil, sender: UIViewController, complete: ((Bool)->Void)?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    alert.addAction(UIAlertAction(title: titleButtonOk, style: .default, handler: { (_ ) in
        complete?(true)
        alert.dismiss(animated: true, completion: {
        })
        
    }))
    
    if let title = titleCancel {
        alert.addAction(UIAlertAction(title: title, style: .default , handler: { (_ ) in
            alert.dismiss(animated: true, completion: {
                complete?(false)
            })
        }))
    }
    DispatchQueue.main.async {
        sender.present(alert, animated: true)
    }
}

func randomColor<T>(arrColor: [T], dataSource: inout [T?], numberColum: Int)  where T: Comparable{
    var element:[T] = []
    for (index, _) in dataSource.enumerated() {
        // reload Color
        if element.isEmpty == true { element = arrColor }
        
        // left color
        if index >= numberColum {
            // last 2 element
            if element.count == 2, numberColum > 2, let toplastColor = dataSource[index + 1 - numberColum], element.contains(toplastColor) {
                dataSource[index] = toplastColor
            } else {
                dataSource[index] = element.filter { return $0 != dataSource[index - numberColum] }.randomElement()
            }
        } else {
            dataSource[index] = element.randomElement() ?? nil
        }
        element.removeAll { $0 == dataSource[index] }
    }
}

var tomorrow: Int {
    guard let today = Date().getDayOfWeek() else {
        return 0
    }
    
    // getDayOfWeek start 1(Sunday)->7(Saturday)
    // Server require 0(Sunday) -> 6(Saturday)
    // ==> Tomorrow of Sat then equal 0
    var dayOfTomorow: Int
    switch today {
    case 1:
        dayOfTomorow = 1
    case 2:
        dayOfTomorow = 2
    case 3:
        dayOfTomorow = 3
    case 4:
        dayOfTomorow = 4
    case 5:
        dayOfTomorow = 5
    case 6:
        dayOfTomorow = 6
    case 7:
        dayOfTomorow = 0
    default:
        dayOfTomorow = 0
    }
    
    return dayOfTomorow
}

func roundingFloatToInt(input: Float?, maximumFractionDigits: Int, minimumFractionDigits: Int = 0) -> String {
    guard let _input = input else { return "0" }
    let numberFormatter = NumberFormatter()
    numberFormatter.minimumFractionDigits = minimumFractionDigits
    numberFormatter.maximumFractionDigits = maximumFractionDigits
    return numberFormatter.string(from: NSNumber(value: _input)) ?? ""
}

func requestForCamera(_ callback: @escaping (Bool) -> Void) {
    
    switch AVCaptureDevice.authorizationStatus(for: .video) {
    case .denied, .restricted, .notDetermined: AVCaptureDevice.requestAccess(for: AVMediaType.video) { callback($0) }
    case .authorized: callback(true)
    @unknown default:
        fatalError()
    }
}

func requestForPhotoLibrary(_ callback: @escaping (Bool) -> Void) {
    
    switch PHPhotoLibrary.authorizationStatus() {
    case .denied, .restricted, .notDetermined: AVCaptureDevice.requestAccess(for: AVMediaType.video) { callback($0) }
    case .authorized: callback(true)
    @unknown default:
        fatalError()
    }
}

func calculateHeight(text: String, fontSize: CGFloat, fontName: String, width: CGFloat) -> CGFloat {
    let font = UIFont(name: fontName, size: fontSize)
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    
    return label.frame.height
}


func stringFromBundle(_ fileName: String, ofType: String) -> String? {
    if let filepath = Bundle.main.path(forResource: fileName, ofType: ofType) {
        do {
            let contents = try String(contentsOfFile: filepath)
            return contents
        } catch {
            return nil
        }
    } else {
        return nil
    }
}
