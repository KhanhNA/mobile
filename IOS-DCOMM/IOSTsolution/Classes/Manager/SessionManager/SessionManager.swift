//
//  SessionManager.swift
//  SmartMe
//
//  Created by AQU on 5/22/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import Foundation

class SessionManager {
    static let shared = SessionManager()
    var accountModel: AccountModel? {
        get {
            return self.loadCurrentUserFromKeyChain()
        }
    }
    
    internal func loadCurrentUserFromKeyChain() -> AccountModel? {
        let keychain = KeyChainService(account: AccountName.token.rawValue)
        guard let dataKeyChain = try? keychain.readKeyChain(),
            let data = dataKeyChain.data(using: String.Encoding.utf8) else { return nil }
        let instance = try? JSONDecoder().decode(AccountModel.self, from: data)
        return instance
    }
    
    internal func saveToUserDefault(accountModel: AccountModel) {
        let keychain = KeyChainService(account: AccountName.token.rawValue)
        guard let data = try? JSONEncoder().encode(accountModel) else { return }
        guard let jsonString = String(data: data, encoding: .utf8) else { return }
        try? keychain.saveKeyChain(jsonString)
    }
}
