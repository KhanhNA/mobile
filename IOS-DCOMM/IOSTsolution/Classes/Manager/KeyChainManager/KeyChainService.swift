//
//  KeyChainService.swift
//  SmartMe
//
//  Created by Bùi Minh Tiến on 12/13/18.
//  Copyright © 2018 ntq. All rights reserved.
//

import UIKit

enum AccountName: String {

    case token

}

struct KeyChainService {

    enum KeychainError: Error {
        case noPassword
        case unexpectedPasswordData
        case unexpectedItemData
        case unhandledError(status: OSStatus)
    }

    let service: String = Const.PROJECT_NAME
    private(set) var account: String
    // Using share keychain
    let accessGroup: String?

    init(account: String, accessGroup: String? = nil) {
        self.account = account
        self.accessGroup = accessGroup
    }

    // MARK: Keychain access

    func readKeyChain() throws -> String  {
        var query = KeyChainService.keychainQuery(withService: service, account: account, accessGroup: accessGroup)
        query[kSecMatchLimit as String] = kSecMatchLimitOne
        query[kSecReturnAttributes as String] = kCFBooleanTrue
        query[kSecReturnData as String] = kCFBooleanTrue
        var queryResult: AnyObject?
        let status = withUnsafeMutablePointer(to: &queryResult) {
            SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0))
        }
        guard status != errSecItemNotFound else { throw KeychainError.noPassword }
        guard status == noErr else { throw KeychainError.unhandledError(status: status) }
        guard let existingItem = queryResult as? [String : AnyObject],
            let data = existingItem[kSecValueData as String] as? Data,
            let contentString = String(data: data, encoding: String.Encoding.utf8)
            else {
                throw KeychainError.unexpectedPasswordData
        }
        return contentString
    }

    func saveKeyChain(_ content: String) throws {
        let encodedContent = content.data(using: String.Encoding.utf8)!
        do {
            try _ = readKeyChain()
            var attributesToUpdate = [String : AnyObject]()
            attributesToUpdate[kSecValueData as String] = encodedContent as AnyObject?
            let query = KeyChainService.keychainQuery(withService: service, account: account, accessGroup: accessGroup)
            let status = SecItemUpdate(query as CFDictionary, attributesToUpdate as CFDictionary)
            guard status == noErr else { throw KeychainError.unhandledError(status: status) }
        }
        catch KeychainError.noPassword {
            var newItem = KeyChainService.keychainQuery(withService: service, account: account, accessGroup: accessGroup)
            newItem[kSecValueData as String] = encodedContent as AnyObject?
            let status = SecItemAdd(newItem as CFDictionary, nil)
            guard status == noErr else { throw KeychainError.unhandledError(status: status) }
        }
    }

    func deleteItem() throws {
        let query = KeyChainService.keychainQuery(withService: service, account: account, accessGroup: accessGroup)
        let status = SecItemDelete(query as CFDictionary)
        guard status == noErr || status == errSecItemNotFound else { throw KeychainError.unhandledError(status: status) }
    }

    // MARK: Convenience

    private static func keychainQuery(withService service: String, account: String? = nil,
                                      accessGroup: String? = nil) -> [String : AnyObject] {
        var query = [String : AnyObject]()
        query[kSecClass as String] = kSecClassGenericPassword
        query[kSecAttrService as String] = service as AnyObject?
        if let account = account { query[kSecAttrAccount as String] = account as AnyObject? }
        if let accessGroup = accessGroup { query[kSecAttrAccessGroup as String] = accessGroup as AnyObject? }
        return query
    }

}

