//
//  Router.swift
//  SoccerResults
//
//  Created by taopd on 3/1/17.
//  Copyright © 2017 ttt. All rights reserved.
//

import UIKit
import Alamofire
enum Router: URLRequestConvertible {
    case login(body: Parameters?)
    case categories(body: Parameters?)
    case trandeMake(body: Parameters?)
    case allProduct(para: Parameters?)
    case selectItem(para: Parameters?)
    case ProductCategory(para: Parameters?)
    case orders(body: Parameters?)
    case mechant(body:Parameters?)
    case contact(body:Parameters?)
    
    var result: (path: String, method: HTTPMethod, body: Parameters?, params: Parameters?) {
        switch self {
        case .login(let body):
            return (APIPath.login, .get, body, nil)
        case .categories(let body):
            return (APIPath.categories, .get, body, nil)
        case .trandeMake(body: let body):
            return (APIPath.trandeMake, .get, body, nil)
        case .allProduct(para: let para):
            return (APIPath.allProduct, .get, nil, para)
        case .selectItem(para: let para):
            return (APIPath.SelectItem, .get, nil, para)
        case .ProductCategory(para: let para):
            return (APIPath.ProductsCategory, .get, nil, para)
        case .orders(body: let body):
            return (APIPath.orders ,.post ,body ,nil)
        case .mechant(body: let body):
            return (APIPath.mechant, .post,body ,nil)
        case .contact(body: let body):
            return (APIPath.mechant, .post ,body ,nil)
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var url: URL = try APIRequest.GetDataURLString.asURL()
        let result = self.result
        if result.path == APIPath.login {
            url = try APIRequest.baseURLString.asURL()
        }
      var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
       // urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        if let params = result.params {
            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
            return urlEncode
        }
        return urlRequest
    }
}
enum RouterJson: URLRequestConvertible {
    
    case mechant(body:NSDictionary?)
    case order(body:NSDictionary?)

    var result: (path: String, method: HTTPMethod, body: NSDictionary?, params: Parameters?) {
        switch self {
        case .mechant(body: let body):
            return (APIPath.mechant, .post, body, nil)
        case .order(body: let body):
            return (APIPath.orders, .post, body, nil)
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        //        var url: URL = try APIRequest.GetDataURLString.asURL()
        var url: URL = try "http://14.248.144.22:8234/api/v1/merchants?page=0&langId=1".asURL()
        let result = self.result
        if result.path == APIPath.login {
            url = try APIRequest.baseURLString.asURL()
        }
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(""))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        if let params = result.params {
            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
            return urlEncode
        }
        return urlRequest
    }    
    

}

enum RouterJsonData: URLRequestConvertible {
    case order(body:NSDictionary?)
    var result: (path: String, method: HTTPMethod, body: NSDictionary?, params: Parameters?) {
        switch self {
        case .order(body: let body):
            return (APIPath.orders, .post, body, nil)
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var url: URL = try "http://14.248.144.22:8234".asURL()
//        var url: URL = try "http://192.168.1.4:8234".asURL()
        let result = self.result
        if result.path == APIPath.orders {
//            url = try APIRequest.baseURLString.asURL()
        }
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        if let params = result.params {
            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
            return urlEncode
        }
        return urlRequest
    }
    
    
}

enum RouterJsonContact: URLRequestConvertible {
    case contact(body:NSDictionary?)
    var result: (path: String, method: HTTPMethod, body: NSDictionary?, params: Parameters?) {
        switch self {
        case .contact(body: let body):
            return (APIPath.mechant, .post, body, nil)
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var url: URL = try "http://14.248.144.22:8234".asURL()
        //        var url: URL = try "http://192.168.1.4:8234".asURL()
        let result = self.result
        if result.path == APIPath.mechant {
            //            url = try APIRequest.baseURLString.asURL()
        }
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        if let params = result.params {
            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
            return urlEncode
        }
        return urlRequest
    }
    
    
}


