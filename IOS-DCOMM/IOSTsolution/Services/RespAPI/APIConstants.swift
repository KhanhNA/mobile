//
//  APIConstants.swift
//  SmartMe
//
//  Created by AQU on 5/22/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import Foundation
// MARK: - APIPath
struct APIPath {
    static let login                        = "/user/me"
    static let categories                   = "/api/v1/categories"
    static let trandeMake                   = "/api/v1/manufacturers"
    static let allProduct                   = "/api/v1/products"
    static var SelectItem                   = ""
    static let ProductsCategory             = "/api/v1/products/category"
    static let orders                       = "/api/v1/orders"
    static let mechant                      = "/api/v1/merchants"
}

// MARK: - APIParamKeys
struct APIParamKeys {
    static let phoneNumber                  = "phoneNumber"
    static let userId                       = "userId"
    static let verifyCode                   = "verifyCode"
    static let userName                     = "userName"
    static let email                        = "email"
    static let userPassword                 = "userPassword"
    static let avatarPath                   = "avatarPath"
    static let regionId                     = "regionId"
    static let schoolId                     = "schoolId"
    static let password                     = "password"
    static let searchKey                    = "searchKey"
    static let newPassword                  = "newPassword"
    static let learnClass                   = "class"
    static let unitId                       = "unitId"
    static let lessonId                     = "lessonId"
    static let examTypeId                   = "examTypeId"
    static let examId                       = "examId"
    static let successAnswer                = "successAnswer"
    static let wrongAnswer                  = "wrongAnswer"
    static let type                         = "type"
}
