//
//  APIRequest.swift
//  MyClean
//
//  Created by Taopd on 5/13/18.
//  Copyright © 2018 Taopd. All rights reserved.
//

import UIKit
import Alamofire

class Connectivity {
    class var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

struct ResponseModel<M: Codable>: Codable {
//    var status: ResultCode?
    var userAuthentication: M?
    var content: [M]?
    var totalPages: Int?
    var data : M?
    var productName : String?

    private enum CodingKeys: String, CodingKey {
//        case status
    //    case name
        case userAuthentication
        case content
        case totalPages
        case data = ""
        
    }
}

struct ResponsePortalModel<M: Codable>: Codable {
    var message: String?
    var result: M?
    var errorMessage: String? {
        return message
    }
    
    private enum CodingKeys: String, CodingKey {
        case message
        case result
    }
}

struct ResponseError {
    
    var status: ResultCode
}

struct EmptyModel: Codable {}

class APIRequest {

    static let baseURLString    = Const.BASE_URL
    static let GetDataURLString = Const.URL_GETDATA

    @discardableResult
    static func request<M: Codable>(_ router: URLRequestConvertible,
                                    requiredToken: Bool = false,
                                    isSkipError: Bool = false,
                                    failure: ((ResponseError) -> Void)? = nil,
                                    completion: ((ResponseModel<M>?) -> Void)?) -> Request? {
        guard Connectivity.isConnectedToInternet else {
            DEBUGLog("--------Disconnect network--------")
            completion?(nil)
            return nil
        }
        
        let manager = Session.manager
        if requiredToken, let token = SessionManager.shared.accountModel?.access_token {
            let adapter = CPRequestAdapter(accessToken: token)
            manager.adapter = adapter
        } else {
            let adapter = CPRequestAdapter(accessToken: nil)
            manager.adapter = adapter
        }
        return Session.request(router).responseJSON { (dataResponse) in
            if let error = dataResponse.error {
                if !isSkipError {
                    DEBUGLog(error.localizedDescription)
                    completion?(nil)
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    
                } else {
                    completion?(nil)
                }
                return
            }
            
            // Request failure
            if let httpStatusCode = dataResponse.response?.statusCode, (httpStatusCode >= 300 || httpStatusCode < 200) {
                // False
                // Process token Expired
                if httpStatusCode == ResultCode.httpUnauthorized.rawValue {
                    Dispatch.async {
                    }
                    completion?(nil)
                    return
                }
            }
            // Response no data
            guard let data = dataResponse.data else {
                DEBUGLog("No Data")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
                return
            }
            // Process parse data
            do {
                let responseModel = try JSONDecoder().decode(ResponseModel<M>.self, from: data)
                if responseModel.userAuthentication == nil {
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    completion?(nil)
                } else {
                    completion?(responseModel)
                }
                 if responseModel.content == nil {
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    completion?(nil)
                } else {
                    completion?(responseModel)
                }
            }
            catch {
                DEBUGLog("PARSE ERROR: \(error)")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
            }
        }
    }
}

extension APIRequest {
    @discardableResult
    static func requestPortal<M: Codable>(_ router: URLRequestConvertible, requiredToken: Bool = false,
                                    failure: ((ResponseError) -> Void)? = nil,
                                    completion: ((ResponsePortalModel<M>?) -> Void)?) -> Request? {
        guard Connectivity.isConnectedToInternet else {
            DEBUGLog("--------Disconnect network--------")
            completion?(nil)
            return nil
        }
        
        return Session.request(router).responseJSON { (dataResponse) in
            if let error = dataResponse.error {
                DEBUGLog(error.localizedDescription)
                completion?(nil)
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                return
            }
            
            // Request failure
            if let httpStatusCode = dataResponse.response?.statusCode, (httpStatusCode >= 300 || httpStatusCode < 200) {
                // False
                // Process token Expired
                if httpStatusCode == ResultCode.httpUnauthorized.rawValue {
                    completion?(nil)
                    return
                }
            }
            
            // Response no data
            guard let data = dataResponse.data else {
                DEBUGLog("No Data")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
                return
            }
            
            // Process parse data
            do {
                let responseModel = try JSONDecoder().decode(ResponsePortalModel<M>.self, from: data)
                if responseModel.result == nil {
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    completion?(nil)
                } else {
                    completion?(responseModel)
                }
            }
            catch {
                DEBUGLog("PARSE ERROR: \(error)")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
            }
        }
    }
}

private struct CPRequestAdapter: RequestAdapter  {
    private var accessToken: String?
    private var refreshToken: String?

    init(accessToken: String? = nil) {
        self.accessToken = accessToken
    }

    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
      //  urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        guard let accessToken = accessToken else { return urlRequest }
        urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
        return urlRequest
    }
}

extension ResponseModel {
//    var errorMessage: String? {
//        return message ?? message_en
//    }
}
