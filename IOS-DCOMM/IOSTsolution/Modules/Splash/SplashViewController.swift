//
//  SplashViewController.swift
//  SmartMe
//
//  Created by AQU on 5/22/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        showNextView()
    }
    
    private func showNextView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            AppDelegate.shared.rootViewController.showLoginView()
        }
    }
}
