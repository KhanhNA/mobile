//
//  AcccoutModel.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/9/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation
struct AccountModel: Codable {
    var access_token: String?
    var token_type: String?
    var refresh_token: String?
    var expires_in: Int?
    var scope : String?

}
struct merchantModel:Codable {
    var merchantId: Int?
}
