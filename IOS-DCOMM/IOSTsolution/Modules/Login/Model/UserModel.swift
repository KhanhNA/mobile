//
//  userModel.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/11/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation

struct UserModel: Codable {
    var name: String?
   // var lastName: String?
    var principal: principalModel?
}
struct principalModel: Codable {
    var roles: [Roles]?
}
struct Roles: Codable {
    var id: Int?
    var roleName: String?
    var description: String?

}
//struct merchantModel: Codable {
//    var merchantId: Int?
//}
