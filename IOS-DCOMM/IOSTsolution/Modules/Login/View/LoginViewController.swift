//
//  LoginViewController.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/9/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import RAMAnimatedTabBarController
class LoginViewController: UIViewController {
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldPass: UITextField!
    
    @IBOutlet weak var buttonSavePass: UIButton!
    @IBOutlet weak var imageChecker: UIImageView!
    lazy var presenter: loginInputProtocol = LoginPresenter()
    var user = AccountModel()
    var mechants = [merchantModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        textFieldUsername.text = "admin"
        textFieldPass.text = "abc@123"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func ButtonLogin_Action(_ sender: Any) {
        guard let username = textFieldUsername.text, !(textFieldUsername.text?.isEmpty)! else { return }
        guard let password = textFieldPass.text, !(textFieldPass.text?.isEmpty)! else { return }
        presenter.requestLogin(username: username, password: password)
    }
    @IBAction func ButtonSavePassword_Action(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = sender.isSelected ? UIImage(named: "checked.jpg") : UIImage(named: "unchecked.jpg")
        self.buttonSavePass.setImage(image, for: .normal)
    }
    
    @IBAction func ButtonForgetPass_Action(_ sender: Any) {
        
    }
}
extension LoginViewController: LoginOutputProtocol {
    func didFishMechants(_ Mechants: [merchantModel]) {
        mechants = Mechants
        Global.mechantsId = mechants[0].merchantId
    }
    
    func didFinishLogin(_ userModel: principalModel) {
        guard  let name = userModel.roles else {
            return
        }
        let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "hometab") as! TarbarVC
        present(secondViewController, animated: true, completion: nil)
    }

    func didFinishError(errorMessage: String?) {
        print("\(errorMessage)")
    }


}
