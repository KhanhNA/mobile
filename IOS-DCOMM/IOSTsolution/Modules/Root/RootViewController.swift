//
//  RootViewController.swift
//  MyClean
//
//  Created by Taopd on 11/26/2018.
//  Copyright © 2018 Taopd. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    private var current: UIViewController
    
    init() {
        self.current = SplashViewController(nibName: SplashViewController.className, bundle: nil)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChild(current)
        current.view.frame = view.bounds
        view.addSubview(current.view)
        current.didMove(toParent: self)
    }
    
    func showLoginView() {
        let mainVC = LoginViewController(nibName: LoginViewController.className, bundle: nil)
        let naviVC = BaseNaviController(rootViewController: mainVC)
        animateFadeTransition(to: naviVC)
    }
    
    private func animateFadeTransition(to new: UIViewController, completion: (() -> Void)? = nil) {
        current.willMove(toParent: nil)
        addChild(new)
        transition(from: current, to: new, duration: 0.3, options: [.curveEaseIn, .curveEaseOut], animations: {
        }) { completed in
            self.current.removeFromParent()
            new.didMove(toParent: self)
            self.current = new
            completion?()
        }
    }
}
