//
//  HomePresenter.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/25/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation
import Alamofire
protocol homeInputProtocol: BaseMVPProtocol {
    func getCategories()
    func getTradeMake()
    func GetAllProduct(page: Int, langId: Int)
    func getProductCategory(categoryId: Int, page: Int, LangId:Int)
}
protocol homeOutPutProtocol: class {
    func didFinshHome(_ categoriesModel: [modelCategories])
    func didFinshHomeError(error: String? )
    func didFinshTrade(_ tradeMakeModel: [tradeMakeModel])
    func didFinshTradeError(error: String?)
    func didFinshAllProduct(_ AllProductModel: [AllProductModel], pageToTal: Int)
    func didFinshAllProduct(error: String?)
    func didFishProducCategory(_ ProducCategory: [AllProductModel])
    func didFishProductCategoryError(error: String)
}
class HomePresenter: homeInputProtocol {
    func getProductCategory(categoryId: Int, page: Int, LangId:Int) {
        var para = CSParamater()
        para[.page] = page
        para[.categoryProductId] = categoryId
        para[.langId] = LangId
        let router = Router.ProductCategory(para: para.asParamater)
        // ProgressHUD.showProgress()
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<AllProductModel>?) in
            //    ProgressHUD.hideProgress()
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFishProductCategoryError(error: "lỗi ko có data")
                return
            }
            guard let categoriesProduct = resenponseMD.content else {
                self?.delegate?.didFishProductCategoryError(error: "lỗi  data")
                return
            }
            self?.delegate?.didFishProducCategory(categoriesProduct)
            
        }
    }
    

    weak var delegate: homeOutPutProtocol?
    func attachView(_ view: Any) {
        delegate = view as? homeOutPutProtocol
    }

    func getTradeMake() {
        let router = Router.trandeMake(body: nil)
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<tradeMakeModel>?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshHomeError(error: "lỗi ko có data")
                return
            }
            guard let trade = resenponseMD.content else {
                self?.delegate?.didFinshHomeError(error: "lỗi  data")
                return
            }
            
            self?.delegate?.didFinshTrade(trade)

        }
    }

    func getCategories() {
        let router = Router.categories(body: nil)
       // ProgressHUD.showProgress()
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<modelCategories>?) in
        //    ProgressHUD.hideProgress()

            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshHomeError(error: "lỗi ko có data")
                return
            }
            guard let categories = resenponseMD.content else {
                self?.delegate?.didFinshHomeError(error: "lỗi  data")
                return
            }
            self?.delegate?.didFinshHome(categories)

        }
    }

    func GetAllProduct(page: Int, langId: Int) {
        var body = CSParamater()
        body[.page] = page
        body[.langId] = langId
        let router = Router.allProduct(para: body.asParamater)
        ProgressHUD.showProgress()
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<AllProductModel>?) in
            ProgressHUD.hideProgress()
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshHomeError(error: "lỗi ko có data")
                return
            }
            guard let allProduct = resenponseMD.content else {
                self?.delegate?.didFinshHomeError(error: "lỗi  data")
                return
            }
            guard let page = resenponseMD.totalPages else {return}
            self?.delegate?.didFinshAllProduct(allProduct, pageToTal: page)
        }
    }
}

