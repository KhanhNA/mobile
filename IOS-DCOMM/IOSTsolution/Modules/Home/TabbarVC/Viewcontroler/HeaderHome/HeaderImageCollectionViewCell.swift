//
//  HeaderImageCollectionViewCell.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 8/15/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit
import ImageSlideshow
class HeaderImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageSlidesView: ImageSlideshow!

    let localSource = [BundleImageSource(imageString: "img1"), BundleImageSource(imageString: "img2"), BundleImageSource(imageString: "img3"), BundleImageSource(imageString: "img4"),BundleImageSource(imageString: "img5")]

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
                imageSlidesView.slideshowInterval = 3.0
                imageSlidesView.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
                imageSlidesView.contentScaleMode = UIViewContentMode.scaleAspectFill

                let pageControl = UIPageControl()
                pageControl.currentPageIndicatorTintColor = UIColor.lightGray
                pageControl.pageIndicatorTintColor = UIColor.black
                imageSlidesView.pageIndicator = pageControl

                // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
                imageSlidesView.activityIndicator = DefaultActivityIndicator()
              //  slideShowImage.delegate = self

                // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
                imageSlidesView.setImageInputs(localSource)
    }
    

}
