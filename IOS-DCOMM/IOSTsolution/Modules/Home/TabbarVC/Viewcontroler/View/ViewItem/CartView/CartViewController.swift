//
//  CartViewController.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 8/8/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit
import CoreData
class CartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {


    @IBOutlet weak var tableViewCart: UITableView!
    var cartArray: [NSManagedObject] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewCart.dataSource = self
        tableViewCart.delegate = self
        tableViewCart.rowHeight = 100
        tableViewCart.reloadData()
        tableViewCart.register(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Cart")

        do {
            cartArray = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Global.arrItem.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let person = Global.arrItem[indexPath.row]
        let cell = tableViewCart.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CartTableViewCell
        cell.imageCart.kf.setImage(with: URL(string: person.urlImage!))
        cell.nameProductCart.text = person.productName ?? ""
        cell.OrderQuatity.text = String(person.quantity!) ?? ""
        cell.priceProductCart.text = String(person.price!) ?? ""
        return cell
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            managedContext.delete(cartArray[indexPath.row] as NSManagedObject)
            cartArray.remove(at: indexPath.row)
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            self.tableViewCart.deleteRows(at: [indexPath], with: .fade)
            self.tableViewCart.reloadData()
            // remove the deleted item from the `UITableView`
           // self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            // Delete the row from the data source
            //tableView!.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)

        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func ButtonBack_Action(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
