//
//  packCollectionViewCell.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 8/9/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class packCollectionViewCell: UICollectionViewCell {

     var callBack: (() -> Void)?
    
    @IBOutlet weak var buttonPacking: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buttonPacking.backgroundColor = UIColor.lightGray
    }
    @IBAction func buttonPacking_Action(_ sender: UIButton) {
        if let action = callBack {
            action()
        }
    }

    
}
