//
//  SelectItemViewController.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/27/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit
import  Alamofire
import Cosmos
import ImageSlideshow
import CoreData
class SelectItemViewController: UIViewController {

    @IBOutlet weak var imageShowProduct: ImageSlideshow!
    @IBOutlet weak var priceLB: UILabel!
    @IBOutlet weak var titleProductLB: UILabel!
    @IBOutlet weak var priceproductLB: UILabel!
    @IBOutlet weak var priceOrgProductLB: UILabel!
    @IBOutlet weak var starAvgView: CosmosView!
    @IBOutlet weak var rankProductLB: UILabel!
    @IBOutlet weak var packageLB: UILabel!
    @IBOutlet weak var desProductView: UITextView!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var packingColllectionView: UICollectionView!

    var cartProduct: [NSManagedObject] = []
    var myItem = ItemModel()
    var arrProduct = AllProductModel()
    let attrs1 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.black]
    let attrs2 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.red]
    var urlImage = String()
    var urlImageProduct = String()
    var quantity = Int()
    var priceProduct = Double()
    var localSource = [AlamofireSource(urlString: "")]

    override func viewDidLoad() {
        super.viewDidLoad()
       tabBarController?.tabBar.isHidden = true
      //  getDataItem()

        packingColllectionView.delegate = self
        packingColllectionView.dataSource = self
        packingColllectionView.register(UINib(nibName: "packCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        getTestAPI{ (posts) in
            self.myItem = posts
            for i in self.myItem.packingProductList ?? [] {
                if self.arrProduct.packingProductId == i.packingProductId {
                    self.priceproductLB.text = "\(df2so(i.price ?? 0.0)) vnđ"
                    self.priceOrgProductLB.text = "\(df2so(Double(i.orgPrice ?? 0))) vnđ"
                    self.priceLB.text = "\(df2so(i.price ?? 0.0)) vnđ"
                    self.quantity = i.quantity ?? 0
                    self.priceProduct = i.price ?? 0.0
                    self.urlImageProduct = i.packingUrl ?? ""
                }
            }

               // guard let star = self.myItem.reviewAvg else {return}
                let DoublePrice = Double(self.myItem.productPrice ?? 0)
               // self.priceLB.text = "\(df2so(DoublePrice)) vnđ"
                self.titleProductLB.text = self.myItem.productName
                self.nameProduct.text = self.myItem.productName
                self.starAvgView.rating = Double(self.myItem.reviewAvg ?? Int(0.0))
                self.starAvgView.text = String(Double(self.myItem.reviewAvg ?? Int(0.0)))
                let ranktext1 = NSMutableAttributedString(string:"Lượt đánh giá: ", attributes:self.attrs1)
                let ranktext2 = NSMutableAttributedString(string:"0", attributes:self.attrs2)
                ranktext1.append(ranktext2)
                self.rankProductLB.attributedText = ranktext1
                self.desProductView.attributedText = self.myItem.desProduct?.htmlToAttributedString

            self.imageShowProduct.slideshowInterval = 3.0
            self.imageShowProduct.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
            self.imageShowProduct.contentScaleMode = UIViewContentMode.scaleAspectFill

            let pageControl = UIPageControl()
            pageControl.currentPageIndicatorTintColor = UIColor.lightGray
            pageControl.pageIndicatorTintColor = UIColor.black
            self.imageShowProduct.pageIndicator = pageControl


            // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
            self.imageShowProduct.activityIndicator = DefaultActivityIndicator()

            for i in self.myItem.imageList ?? [] {
                self.urlImage = i
            }
            self.localSource =  [AlamofireSource(urlString: self.urlImage)]

//            self.imageShowProduct.setImageInputs([
//                AlamofireSource(urlString: self.urlImage)!
//                ])
            //  slideShowImage.delegate = self

            // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
            self.imageShowProduct.setImageInputs(self.localSource as? [InputSource] ?? [])
        }
            DispatchQueue.main.async {
                self.packingColllectionView.reloadData()
        }

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Cart")

        do {
            cartProduct = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    func getTestAPI(completed:@escaping (_ posts: ItemModel)->Void){
        guard let token = SessionManager.shared.accountModel?.access_token else {return}
        let para = [
            "langId"    : 1,
            ] as? [String : Any]
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        guard let testUrl = URL(string: "\(Const.URL_GETDATA)\(APIPath.SelectItem)") else {return}
        Alamofire.request(testUrl, method: .get, parameters: para, headers: headers).responseJSON {
            (response) in
            if response.result.isSuccess {

                guard let data = response.data else {return}

                do{
                    let myResponse = try JSONDecoder().decode(ItemModel.self, from: data)

                    completed(myResponse)
                }
                catch{}
            }
        }
    }
    func saveItem(item: AllProductModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext = appDelegate.persistentContainer.viewContext

        do {
            try managedContext.save()
            Global.arrItem.append(item)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }


    @IBAction func buttonBack_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonToCart_Action(_ sender: UIButton) {

        let vc = CartViewController(nibName: CartViewController.className, bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func ButtonAddCart_Action(_ sender: Any) {
        let item = AllProductModel()
        for item in Global.arrItem {
            if item.id  == arrProduct.id {
            return
        }
    }
        saveItem(item: arrProduct)
        self.navigationController?.popViewController(animated: true)
}


}

extension SelectItemViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return myItem.packingProductList?.count ?? 0

    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = packingColllectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! packCollectionViewCell
        let dataCell = myItem.packingProductList?[indexPath.row]
         cell.buttonPacking.setTitle(String(dataCell?.quantity ?? 0) , for: .normal)
            if arrProduct.packingProductId == dataCell?.packingProductId {
                cell.buttonPacking.backgroundColor = .red
            }
            cell.callBack = {
                cell.buttonPacking.backgroundColor = .lightGray
                self.priceproductLB.text = "\(df2so((dataCell?.price ?? 0.0))) vnđ"
                self.priceOrgProductLB.text = "\(df2so(Double(dataCell?.orgPrice ?? 0))) vnđ"
                self.urlImageProduct = dataCell?.packingUrl ?? ""
                self.quantity = dataCell?.quantity ?? 0
                self.priceProduct = dataCell?.price ?? 0.0
                cell.buttonPacking.backgroundColor = .red
        }
        return cell
    }
    //MARK: - UICollectionViewDelegateFlowLayout

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 30.0, height: 30.0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }


}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
