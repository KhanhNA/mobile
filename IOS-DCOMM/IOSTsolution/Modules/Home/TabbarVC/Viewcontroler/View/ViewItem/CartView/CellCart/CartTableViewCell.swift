//
//  CartTableViewCell.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 8/10/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit
import Alamofire
import MarqueeLabel
class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var OrderQuatity: UILabel!
    @IBOutlet weak var imageCart: UIImageView!
    @IBOutlet weak var nameProductCart: MarqueeLabel!
    @IBOutlet weak var priceProductCart: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func fillData(nameProduct: String, urlImage: String, price:Double,quantity: Int,orderQuantity: Double ){

        nameProductCart.text = nameProduct
        DispatchQueue.global().async { [weak self] in
            guard let url = URL(string: urlImage) else {return}
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.imageCart.image = image
                    }
                }
            }
        }
        priceProductCart.text = "\(df2so(price)) vnđ | \(quantity)"
        OrderQuatity.text = String(orderQuantity)
    }
}

