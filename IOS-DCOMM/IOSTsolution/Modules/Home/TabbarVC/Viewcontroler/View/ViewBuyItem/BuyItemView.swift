//
//  BuyItemView.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/31/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class BuyItemView: UIView {

    @IBOutlet var contenView: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    let kCONTENT_XIB_NAME = "BuyItemView"

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contenView.fixInView(self)
    }

}

