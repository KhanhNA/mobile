//
//  TrademarkCollectionViewCell.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/26/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class TrademarkCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var LBtext: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func filldata(text: String, textImage: String) {
        LBtext.text = text
        self.imageView.kf.setImage(with: URL(string: textImage))
        // imageProduct.image = UIImage(named: textImage)
//        DispatchQueue.main.async {
//            guard let url = URL(string: textImage) else {return}
//            let data = try? Data(contentsOf: url)
//            if let imageData = data {
//                let image = UIImage(data: imageData)
//                self.imageView.image = image
//            }
//        }
    }

}
