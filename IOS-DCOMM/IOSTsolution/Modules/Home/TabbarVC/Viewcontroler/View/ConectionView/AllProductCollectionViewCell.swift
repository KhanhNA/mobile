//
//  AllProductCollectionViewCell.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/26/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit
import  Cosmos

class AllProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageAllProduct: UIImageView!
    @IBOutlet weak var titleLBProduct: UILabel!
    @IBOutlet weak var quantityProductLB: UILabel!
    @IBOutlet weak var orgPriceProductLB: UILabel!

    @IBOutlet weak var priceProductLB: UILabel!
    @IBOutlet weak var rankStarProduct: CosmosView!
    var callBack: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    func fillData(item: AllProductModel) {
        titleLBProduct.text = item.productName
        let DoublePrice = Double(Int(item.price!))
        let DoubleOrgPrice = Double(Int(item.orgPrice!))
        let attrString = NSAttributedString(string: "\(df2so(DoubleOrgPrice))   ", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
        orgPriceProductLB.attributedText = attrString
        priceProductLB.text = "\(df2so(DoublePrice)) vnđ"
        rankStarProduct.rating = item.reviewAvg ?? 0.0
        quantityProductLB.text = String("/ \(item.quantity!)")
        self.imageAllProduct.kf.setImage(with: URL(string: item.urlImage!))
    }

    @IBAction func actionButton_Buy(_ sender: Any) {
        if let action = callBack {
            action()
        }
    }

}
func df2so(_ price: Double) -> String{
    let numberFormatter = NumberFormatter()
    numberFormatter.groupingSeparator = ","
    numberFormatter.groupingSize = 3
    numberFormatter.usesGroupingSeparator = true
    numberFormatter.decimalSeparator = "."
    numberFormatter.numberStyle = .decimal
    numberFormatter.maximumFractionDigits = 2
    return numberFormatter.string(from: price as NSNumber)!
}
