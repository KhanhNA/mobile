//
//  ProductCollectionViewCell.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/23/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var LBproduct: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func filldata(text: String, textImage: String) {
        LBproduct.text = text
        self.imageProduct.kf.setImage(with: URL(string: textImage))

//       // imageProduct.image = UIImage(named: textImage)
//        DispatchQueue.main.async {
//            guard let url = URL(string: textImage) else {return}
//            let data = try? Data(contentsOf: url)
//            if let imageData = data {
//                let image = UIImage(data: imageData)
//                self.imageProduct.image = image
//            }
//        }
//
    }

}
