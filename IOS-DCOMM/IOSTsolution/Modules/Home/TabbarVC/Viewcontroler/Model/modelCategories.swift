//
//  modelCategories.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/25/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation
struct modelCategories: Codable {
    var name: String?
    var urlImage: String?
    var categoryId:Int?
    var description:String?
    var languageId:Int?
    var title:String?
}
struct tradeMakeModel: Codable {
    var title: String?
    var urlImage: String?
}
struct AllProductModel: Codable {
    var productName: String?
    var urlImage: String?
    var reviewAvg: Double?
    var price: Double?
    var orgPrice: Double?
    var quantity: Int?
    var page: Int?
    var id: Int?
    var packingProductId: Int?
    var manufacturerId: Int?
    var productCode: Int?
    var description: String?
    var reviewCount: Double?
    var typePromotion: Int?
    var marketPrice: Double?
    var discountPercent: Double?
}
struct ItemModel: Codable {
    var id: Int?
    var productName: String?
    var urlImage: String?
    var reviewAvg: Int?
    var productPrice: Int?
    var productQuantity: Int?
    var desProduct: String?
    var imageList: [String]?
    var packingProductList: [packingProductList]?
}
struct packingProductList: Codable {
    var productId: Int?
    var packingUrl: String?
    var packingProductId: Int?
    var price: Double?
    var discountPercent: Double?
    var orgPrice: Int?
    var marketPrice: Int?
    var quantity: Int?
}


