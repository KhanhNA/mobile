//
//  HomeStoreboardVC.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/17/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit
import  ImageSlideshow
import CoreData
import Kingfisher
class HomeVC: UIViewController {
    lazy var presenter: homeInputProtocol = HomePresenter()
    @IBOutlet weak var slideShowImage: ImageSlideshow!
    @IBOutlet weak var ProductCollectionView: UICollectionView!
    @IBOutlet weak var TrandeMakeCollectionView: UICollectionView!
    @IBOutlet weak var AllProductCollectionView: UICollectionView!
    @IBOutlet weak var viewSaleProduct: UIView!
    @IBOutlet weak var viewMail: UIView!
    @IBOutlet weak var lbViewSale: UILabel!
    @IBOutlet weak var LBpriceViewSale: UILabel!
    @IBOutlet weak var txtFileViewSale: UITextField!
    
    
    var cart: [NSManagedObject] = []
    var number = 1
    var page = Int()
    var langID = Int()
    var demoView: UIView!
    
    // Model Save //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //        ProductCollectionView.delegate = self
        //        ProductCollectionView.dataSource = self
        //        ProductCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        //        TrandeMakeCollectionView.delegate = self
        //        TrandeMakeCollectionView.dataSource = self
        //        TrandeMakeCollectionView.register(UINib(nibName: "TrademarkCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "celltrade")
        
        AllProductCollectionView.delegate = self
        AllProductCollectionView.dataSource = self
        AllProductCollectionView.register(UINib(nibName: "AllProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cellproduct")
        AllProductCollectionView.register(UINib(nibName: "HeaderImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cellheader")
        AllProductCollectionView.register(UINib(nibName: "AllCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AllCollectionViewCell")
        //        slideShowImage.slideshowInterval = 3.0
        //        slideShowImage.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        //        slideShowImage.contentScaleMode = UIViewContentMode.scaleAspectFill
        //
        //        let pageControl = UIPageControl()
        //        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        //        pageControl.pageIndicatorTintColor = UIColor.black
        //        slideShowImage.pageIndicator = pageControl
        //
        //        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        //        slideShowImage.activityIndicator = DefaultActivityIndicator()
        //      //  slideShowImage.delegate = self
        //
        //        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        //        slideShowImage.setImageInputs(localSource)
        // Do any additional setup after loading the view.
        page = 0
        langID = 1
        presenter.attachView(self)
        presenter.GetAllProduct(page: page, langId: langID)
        
        
//        demoView = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 100, width: UIScreen.main.bounds.width, height: 100))
//        demoView.backgroundColor = .red
//        demoView.isHidden = true
//        view.addSubview(demoView)
        txtFileViewSale.text = String(number)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Cart")
        
        do {
            cart = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func save(productName: String, url: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Cart", in: managedContext)!
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        person.setValue(productName, forKeyPath: "productName")
        person.setValue(url, forKeyPath: "url")
        
        
        do {
            try managedContext.save()
            cart.append(person)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    @IBAction func ButtonCart_Action(_ sender: Any) {
        let vc = CartViewController(nibName: CartViewController.className, bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func buttonHide_Action(_ sender: Any) {
        viewSaleProduct.fadeOut()
    }
    @IBAction func ButtonTru_Action(_ sender: Any) {
        
        if number <=  0
        { number = 0
            return
            
        } else {
            number -= 1
            txtFileViewSale.text = String(number)
        }
        
    }
    @IBAction func buttonCong_Action(_ sender: Any) {
        number += 1
        txtFileViewSale.text = String(number)
    }
    
    @IBAction func ButtonAddProduct_Action(_ sender: Any) {
        
        print("numberolder", number)
        save(productName: "test ti nao", url: "test URl")
        for i in cart {
        }
    }
}
var ArrCategories = [modelCategories]()
var ArrTrademake  = [tradeMakeModel]()
var ArrProduct = [AllProductModel]()
var pageTotal = Int()

let attrs1 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.black]

let attrs2 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegate, homeOutPutProtocol , UICollectionViewDelegateFlowLayout {
    func didFishProducCategory(_ ProducCategory: [AllProductModel]) {
        
    }
    
    func didFishProductCategoryError(error: String) {
        
    }
    
    func didFinshAllProduct(_ AllProductModel: [AllProductModel], pageToTal: Int) {
        ArrProduct += AllProductModel
        pageTotal = pageToTal
        AllProductCollectionView.reloadData()
    }
    
    func didFinshAllProduct(error: String?) {
        
    }
    
    func didFinshTrade(_ tradeMakeModel: [tradeMakeModel]) {
        ArrTrademake = tradeMakeModel
        TrandeMakeCollectionView.reloadData()
    }
    
    func didFinshTradeError(error: String?) {
        
    }
    
    
    func didFinshHome(_ categoriesModel: [modelCategories]) {
        ArrCategories = categoriesModel
        ProductCollectionView.reloadData()
    }
    
    func didFinshHomeError(error: String?) {
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var returnValue = Int()
        
        if section == 0 {
            returnValue = 1
        }else if section == 1 {
            return  1
        } else if section == 2 {
            returnValue = ArrProduct.count
            
        }
        return returnValue
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var returnValue = CGSize()
        if indexPath.section == 0 {
            returnValue = CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height / 4.0 - 8)
            
        } else if indexPath.section == 1{
            return CGSize(width: UIScreen.main.bounds.size.width, height: 30)
        } else{
            returnValue =  CGSize(width: (UIScreen.main.bounds.width - 40) / 2 , height: (UIScreen.main.bounds.width) / 2 )
        }
        return returnValue
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cellall = UICollectionViewCell()
        let cell = AllProductCollectionView.dequeueReusableCell(withReuseIdentifier: "cellproduct", for: indexPath) as! AllProductCollectionViewCell
        let cellHead = AllProductCollectionView.dequeueReusableCell(withReuseIdentifier: "cellheader", for: indexPath) as! HeaderImageCollectionViewCell
        let cellAl = AllProductCollectionView.dequeueReusableCell(withReuseIdentifier: "AllCollectionViewCell", for: indexPath) as! AllCollectionViewCell
        
        cell.callBack = {
            let ranktext1 = NSMutableAttributedString(string:"\(df2so(ArrProduct[indexPath.row].price ?? 0)) đ |", attributes:attrs1)
            let ranktext2 = NSMutableAttributedString(string:String(ArrProduct[indexPath.row].quantity ?? 0), attributes:attrs2)
            ranktext1.append(ranktext2)
            self.LBpriceViewSale.attributedText = ranktext1
            self.lbViewSale.text = ArrProduct[indexPath.row].productName
            self.viewSaleProduct.fadeIn()
        }
        let setion  = indexPath.section
        
        if setion == 0 {
            cellall = cellHead
        } else if setion == 1 {
            cellall = cellAl
        }else {
            cellall = cell
            let data = ArrProduct[indexPath.row]
            cell.fillData(item: data)
        }
        return cellall
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == ArrProduct.count - 1 {  //numberofitem count
            if page < pageTotal {
                page += 1
            } else {
                return
            }
            updateNextSet()
        } else {return}
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = SelectItemViewController(nibName: SelectItemViewController.className, bundle: nil)
        guard let id = ArrProduct[indexPath.row].id else {return}
        let urlItem = "/api/v1/products/\(id)"
        APIPath.SelectItem = urlItem
        vc.arrProduct = ArrProduct[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    func updateNextSet(){
        print(page)
        presenter.GetAllProduct(page: page, langId: langID)
        //requests another set of data (20 more items) from the server.
        //DispatchQueue.main.async(execute: AllProductCollectionView.reloadData)
    }
    
    
}

extension UIView {
    
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.layoutSubviews, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.layoutSubviews, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }
}

