//
//  CategoryCollectionViewCell.swift
//  IOSTsolution
//
//  Created by TheLightLove on 12/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblNameSearch: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
