//
//  categoryModel.swift
//  IOSTsolution
//
//  Created by TheLightLove on 13/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

/*
 "id": 2,
 "manufacturerId": 3,
 "productCode": null,
 "productName": "IphoeX thế hệ mới với hệ điều hành android mạnh mẽ. Ram cực lớn kết hợp với bộ nhớ trong không giới hạn.",
 "description": null,
 "urlImage": "https://images.vov.vn/w800/uploaded/0jth2lsxjrk/2017_03_10/cheese_cake_DBWH.jpg",
 "reviewAvg": null,
 "reviewCount": null,
 "price": 130000.0,
 "quantity": 12,
 "typePromotion": null,
 "orgPrice": 5353.0,
 "marketPrice": 120000.0,
 "discountPercent": 4.0,
 "packingProductId": 73,
 "objLang": null
 */

import Foundation

struct ProductCategory: Codable {
    var id: Int?
    var manufacturerId: Int?
    var productCode: Int?
    var productName: String?
    var description: String?
    var urlImage: String?
    var reviewAvg: Double?
    var reviewCount: Double?
    var price: Double?
    var quantity: Int?
    var typePromotion: Int?
    var orgPrice: Double?
    var marketPrice: Double?
    var discountPercent: Double?
    var packingProductId: Double?
}

