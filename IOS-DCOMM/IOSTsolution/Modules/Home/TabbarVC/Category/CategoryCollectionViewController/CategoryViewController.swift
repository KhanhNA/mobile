//
//  CategoryViewController.swift
//  IOSTsolution
//
//  Created by TheLightLove on 12/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , homeOutPutProtocol, UICollectionViewDelegateFlowLayout {

    
    lazy var presenter: homeInputProtocol = HomePresenter()
    var arrCategories = [modelCategories]()
    var arrTrademake  = [tradeMakeModel]()
    var arrProductCategory = [AllProductModel]()
    var categoryId: Int? = nil
    let page = 0
    var langId: Int? = nil
    var number = 1
    var index:Int?
    @IBOutlet weak var ProductcollectionView: UICollectionView!
    @IBOutlet weak var ProductCategoryCollectionView: UICollectionView!
    @IBOutlet weak var viewSaleProduct: UIView!
    @IBOutlet weak var lbViewSale: UILabel!
    @IBOutlet weak var LBpriceViewSale: UILabel!
    @IBOutlet weak var txtFileViewSale: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        ProductcollectionView.delegate = self
        ProductcollectionView.dataSource = self
        let nib1 = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        self.ProductcollectionView.register(nib1, forCellWithReuseIdentifier: "CategoryCollectionViewCell")
        ProductCategoryCollectionView.delegate = self
        ProductCategoryCollectionView.dataSource = self
        ProductCategoryCollectionView.register(UINib(nibName: "AllProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cellproduct")
        presenter.attachView(self)
        presenter.getCategories()
        presenter.getTradeMake()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonHide_Action(_ sender: Any) {
        viewSaleProduct.fadeOutOk()
    }
    @IBAction func ButtonTru_Action(_ sender: Any) {
        
        if number <=  1
        { number = 1
            return
            
        } else {
            number -= 1
            txtFileViewSale.text = String(number)
        }
        
    }
    @IBAction func buttonCong_Action(_ sender: Any) {
        number += 1
        txtFileViewSale.text = String(number)
    }
    
    @IBAction func ButtonAddProduct_Action(_ sender: Any) {
        let item = AllProductModel()
        for item in Global.arrItem {
            if item.id  == arrProductCategory[index!].id {
                return
            }
        }
        saveItem(item: arrProductCategory[index!])
        viewSaleProduct.fadeOutOk()
    }
    
    func saveItem(item: AllProductModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            try managedContext.save()
            Global.arrItem.append(item)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
        

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.ProductcollectionView {
            return arrCategories.count
        } else {
            return arrProductCategory.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == ProductcollectionView{
            return CGSize(width: UIScreen.main.bounds.width / 4, height: self.ProductcollectionView.bounds.height + 20)
        }else{
            return CGSize(width: (UIScreen.main.bounds.width - 40) / 2, height: (UIScreen.main.bounds.size.width)/2 + 20)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("\(scrollView)")
    }

    
    func didFinshAllProduct(_ AllProductModel: [AllProductModel], pageToTal: Int) {

    }
    
    func didFinshAllProduct(error: String?) {
        
    }
    
    func didFinshTrade(_ tradeMakeModel: [tradeMakeModel]) {
        
    }
    
    func didFinshTradeError(error: String?) {
        
    }
    
    
    func didFinshHome(_ categoriesModel: [modelCategories]) {
        self.arrCategories = categoriesModel
        ProductcollectionView.reloadData()
    }
    
    func didFinshHomeError(error: String?) {
        
    }
    
    func didFishProducCategory(_ ProducCategory: [AllProductModel]) {
        self.arrProductCategory = ProducCategory
        ProductCategoryCollectionView.reloadData()
    }
    
    func didFishProductCategoryError(error: String) {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == ProductcollectionView {
            let cell = ProductcollectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
            let cate = arrCategories[indexPath.row]
            cell.imgCategory.kf.setImage(with: URL(string: cate.urlImage ?? ""))
            cell.lblNameSearch.text = cate.name
            return cell
        }else{
            let cell = ProductCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "cellproduct", for: indexPath as IndexPath) as! AllProductCollectionViewCell
            let data = arrProductCategory[indexPath.row]
            cell.fillData(item: data)
//            cell.fillData(text: data.productName ?? "" , textImage: data.urlImage ?? "", price: data.price ?? 0.0, orgprice: data.orgPrice ?? 0.0, quantity: data.quantity ?? 0, rankStar: data.reviewAvg ?? 0)
            cell.callBack = {
                self.index = indexPath.row
                let ranktext1 = NSMutableAttributedString(string:"\(df2so(data.price ?? 0)) đ |", attributes:attrs1)
                let ranktext2 = NSMutableAttributedString(string:String(data.quantity ?? 0), attributes:attrs2)
                ranktext1.append(ranktext2)
                self.LBpriceViewSale.attributedText = ranktext1
                self.lbViewSale.text = data.productName
                self.viewSaleProduct.backgroundColor = .white
                self.viewSaleProduct.fadeInOk()
            }
            return cell
           
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == ProductcollectionView {
            let cate = arrCategories[indexPath.row]
            self.categoryId = cate.categoryId
            self.langId = cate.languageId
            getProductCategory()
        }else{
            let vc = SelectItemViewController(nibName: SelectItemViewController.className, bundle: nil)
            guard let id = arrProductCategory[indexPath.row].id else {
                return
            }
            let urlItem = "/api/v1/products/\(id)"
            APIPath.SelectItem = urlItem
            vc.arrProduct = arrProductCategory[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getProductCategory(){
        presenter.getProductCategory(categoryId: categoryId ?? 0, page: page, LangId: langId ?? 0)
    }
}

extension UIView {
    
    func fadeInOk(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.layoutSubviews, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOutOk(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.layoutSubviews, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }
}
