//
//  WalletVC.swift
//  IOSTsolution
//
//  Created by TheLightLove on 14/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class WalletVC: UIViewController {
    @IBOutlet weak var tbView: UITableView!
    var arrPendingOrder = [PendingModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onSetting(_ sender: Any) {
    }
    
}
extension WalletVC: UITableViewDataSource , UITableViewDelegate ,HistoryOrderOutPutProtocol{
    func didFishOrderhistoryCategory(_ PendingOrder: [PendingModel]) {
        self.arrPendingOrder = PendingOrder
        self.tbView.reloadData()
    }
    
    func didFishOderhistoryError(error: String) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbView.dequeueReusableCell(withIdentifier: "WalletTableViewCell") as? WalletTableViewCell
        return cell!
    }
    
    
}
