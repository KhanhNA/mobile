//
//  settingTableViewCell.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/18/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class settingTableViewCell: UITableViewCell {

    @IBOutlet weak var SettingImageView: UIImageView!
    @IBOutlet weak var SettingNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func filldata(text: String) {
        SettingNameLabel.text = text
        SettingImageView.image = UIImage(named: text)
    }
    
    
}
