//
//  HistoryPresenter.swift
//  IOSTsolution
//
//  Created by TheLightLove on 13/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation
import Alamofire
protocol HistoryOrderInputProtocol: BaseMVPProtocol {
    func getOrderHistory(langId: Int, merchantId: Int, orderType:Int)
}
protocol HistoryOrderOutPutProtocol: class {
    func didFishOrderhistoryCategory(_ PendingOrder: [PendingModel])
    func didFishOderhistoryError(error: String)
}

class HistoryPresenter: HistoryOrderInputProtocol {
    func getOrderHistory(langId: Int, merchantId: Int, orderType: Int) {
        let body: NSDictionary = ["langId":1,
                                  "merchantId":merchantId,
                                  "orderType":orderType]
        
        let router = RouterJsonData.order(body: body)
        // ProgressHUD.showProgress()
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<PendingModel>?) in
            //    ProgressHUD.hideProgress()
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFishOderhistoryError(error: "lỗi ko có data")
                return
            }
            guard let PendingModel = resenponseMD.content else {
                self?.delegate?.didFishOderhistoryError(error: "lỗi  data")
                return
            }
            self?.delegate?.didFishOrderhistoryCategory(PendingModel)
        }
    }
    weak var delegate: HistoryOrderOutPutProtocol?

    func attachView(_ view: Any) {
        delegate = view as? HistoryOrderOutPutProtocol
    }
    
    
}
