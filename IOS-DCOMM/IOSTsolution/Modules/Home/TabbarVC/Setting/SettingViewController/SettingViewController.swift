//
//  SettingViewController.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/18/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
class SettingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,UIAlertViewDelegate {
    let arrNameMenu = ["lbl_list_order","lbl_list_order_LV2","lbl_agency","lbl_exchange","lbl_wallet","lbl_share_facbook","lbl_setting","lbl_center_help","lbl_logout"]
    var arrImage = [UIImage(named: "Danh sách đơn mua"),UIImage(named: "Danh sách đơn mua cấp 2"),UIImage(named: "Đại lý cấp 1"),UIImage(named: "Quy đổi"),UIImage(named: "Ví"),UIImage(named: "Chia sẻ facebook"),UIImage(named: "Cài đặt"),UIImage(named: "Trung tâm trợ giúp"),UIImage(named: "Đăng xuất")]
    
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
                tableView.register(UINib(nibName: "settingTableViewCell", bundle: nil), forCellReuseIdentifier: "settingTableViewCell")
                tableView.delegate = self
                tableView.dataSource = self
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNameMenu.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingTableViewCell", for: indexPath) as! settingTableViewCell
        cell.SettingNameLabel.text = NSLocalizedString(arrNameMenu[indexPath.row], comment: "")
        cell.SettingImageView.image = arrImage[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = HistoryOrderViewController(nibName:"HistoryOrderViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1 {

        }else if indexPath.row == 2 {
            
        }else if indexPath.row == 3 {
            
        }else if indexPath.row == 4 {
            let vc = WalletVC(nibName:"WalletVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 5 {
            let content = ShareLinkContent()
            content.contentURL = URL(string: NSLocalizedString("url_share_facbook", comment: ""))!
            let shareDialog = ShareDialog()
            shareDialog.fromViewController = self
            shareDialog.shareContent = content
            shareDialog.show()
        }else if indexPath.row == 6{
            let vc = SettingInformationVC(nibName: SettingInformationVC.className, bundle: nil)
            navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 7 {
            let vc = AboutVC(nibName: "AboutVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let alert:UIAlertController = UIAlertController(title: NSLocalizedString("msg_logout", comment: ""), message: NSLocalizedString("app_name", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("btn_cancel", comment: ""), style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("btn_ok", comment: ""), style: .default, handler: { action in
            self.dismiss(animated: true, completion: {
                return
                })
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func regisNotification(){
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(pushHistoryOder),
            name: UIDevice.batteryLevelDidChangeNotification,
            object: nil)
    }
    
    @objc func pushHistoryOder() {

    }

}
