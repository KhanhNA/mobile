//
//  SettingAcountVC.swift
//  IOSTsolution
//
//  Created by TheLightLove on 14/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class SettingAcountVC: UIViewController, UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate ,UIActionSheetDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var imgProfile: UIImageView!
    var pickerAvatar: UIImagePickerController?
    private var isSelectedImg = false
    @IBOutlet weak var lblBirthDay: UILabel!
    @IBOutlet weak var pickerView: UIDatePicker!
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var lblAcount: UILabel!
    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var lblBirthday1: UILabel!
    @IBOutlet weak var lblSex: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblAndress: UILabel!
    @IBOutlet weak var lblImageProfile: UILabel!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var btnUpdate: UIButton!
        
    

    override func viewDidLoad() {
        super.viewDidLoad()
        viewPicker.isHidden = true
        imgProfile.layer.cornerRadius = 40
        imgProfile.layer.masksToBounds = true
        isSelectedImg = false
        setText()
        
        
        // Do any additional setup after loading the view.
    }
    
    func createTableView(){
    }
    
    func setText(){
        self.lblBirthDay.text = NSLocalizedString("lbl_birthday", comment: "")
        self.lblAcount.text   = NSLocalizedString("lbl_acount", comment: "")
        self.lblFullname.text = NSLocalizedString("lbl_full_name", comment: "")
        self.lblBirthday1.text = NSLocalizedString("lbl_birthday_1", comment: "")
        self.lblSex.text = NSLocalizedString("lbl_sex", comment: "")
        self.lblPhoneNumber.text = NSLocalizedString("lbl_phone_number", comment: "")
        self.lblAndress.text = NSLocalizedString("lbl_andress", comment: "")
        self.lblImageProfile.text = NSLocalizedString("lbl_image_profile", comment: "")
        self.lblProfile.text = NSLocalizedString("lbl_profile", comment: "")
        self.btnUpdate.titleLabel?.text = NSLocalizedString("btn_update", comment: "")
    }
    
    @IBAction func onBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onChooseProfile(_ sender: Any) {
        let alertController = UIAlertController(title: "Ecommerce", message: "", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {(_ action: UIAlertAction?) -> Void in
            self.pickerAvatar = UIImagePickerController()
            self.pickerAvatar?.delegate = self
            self.pickerAvatar?.allowsEditing = true
            self.pickerAvatar?.sourceType = .camera
            if let anAvatar = self.pickerAvatar {
                self.present(anAvatar, animated: true)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Choose From Galery" , style: .default, handler: {(_ action: UIAlertAction?) -> Void in
            self.pickerAvatar = UIImagePickerController()
            self.pickerAvatar?.delegate = self
            self.pickerAvatar?.allowsEditing = true
            self.pickerAvatar?.sourceType = .photoLibrary
            if let anAvatar = self.pickerAvatar {
                self.present(anAvatar, animated: true)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel" , style: .default, handler: {(_ action: UIAlertAction?) -> Void in
//            self.dismiss(animated: true, completion: {
//                return
//            })
        }))
        DispatchQueue.main.async(execute: {() -> Void in
            self.present(alertController, animated: true) {() -> Void in }
        })
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        if buttonIndex < 2 {
            OperationQueue.main.addOperation({
                self.showImagePicker(withSelection: buttonIndex)
            })
        }
    }
    
    func showImagePicker(withSelection selection: Int) {
        pickerAvatar = UIImagePickerController()
        pickerAvatar?.delegate = self
        pickerAvatar?.allowsEditing = true
        switch selection {
        case 1:
            pickerAvatar?.sourceType = .photoLibrary
        case 0:
            pickerAvatar?.sourceType = .camera
        default:
            break
        }
        if let anAvatar = pickerAvatar {
            present(anAvatar, animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        let chosenImage = info[.editedImage] as? UIImage
        imgProfile.image = chosenImage
        isSelectedImg = true
    }

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 1
    }
    
    @IBAction func onBirthDay(_ sender: Any) {
        viewPicker.isHidden = false
        self.viewPicker.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    func dateBirthday() {
        let date:NSDate = self.pickerView!.date as NSDate
        print("\(date)")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd "
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: date as Date)
        self.lblBirthDay.text = timeStamp
        print("\(timeStamp)")
        
    }
    @IBAction func onDone(_ sender: Any) {
        dateBirthday()
        self.viewPicker.isHidden = true
    }
    
    @IBAction func onUpdate(_ sender: Any) {
        
    }
}


