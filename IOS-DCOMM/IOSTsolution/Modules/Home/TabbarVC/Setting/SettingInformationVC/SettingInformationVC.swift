//
//  SettingInformationVC.swift
//  IOSTsolution
//
//  Created by TheLightLove on 14/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class SettingInformationVC: UIViewController {
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var btnFax: UIButton!
    @IBOutlet weak var btnPhoneNumber: UIButton!
    @IBOutlet weak var btnBankAcount: UIButton!
    @IBOutlet weak var btnSettingLaguage: UIButton!
    @IBOutlet weak var btnForgetPassword: UIButton!
    @IBOutlet weak var lblSetting: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        setText()
    }
    
    
    func setText() {
        self.btnSetting.titleLabel?.text = NSLocalizedString("btn_setting", comment: "")
        self.btnFax.titleLabel?.text = NSLocalizedString("btn_fax", comment: "")
        self.btnPhoneNumber.titleLabel?.text = NSLocalizedString("btn_phone_number", comment: "")
        self.btnBankAcount.titleLabel?.text = NSLocalizedString("btn_bank_acount", comment: "")
        self.btnSettingLaguage.titleLabel?.text = NSLocalizedString("btn_setting_laguage", comment: "")
        self.btnForgetPassword.titleLabel?.text = NSLocalizedString("btn_forget_password", comment: "")
        self.lblSetting.text = NSLocalizedString("lbl_setting_profile", comment: "")
    }
    
    
    @IBAction func onSettingAcount(_ sender: Any) {
        let vc = SettingAcountVC(nibName: SettingAcountVC.className, bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func onFax(_ sender: Any) {
        
    }
    
    @IBAction func onPhoneNumber(_ sender: Any) {
    }
    
    @IBAction func onBankAcount(_ sender: Any) {
    }
    
    @IBAction func onSettingLaguage(_ sender: Any) {
    }
    
    @IBAction func onForgetPass(_ sender: Any) {
    }
    
    @IBAction func onBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
