//
//  HistoryOrderViewController.swift
//  IOSTsolution
//
//  Created by TheLightLove on 13/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class HistoryOrderViewController: UIViewController {
    lazy var presenter: HistoryOrderInputProtocol = HistoryPresenter()
    var arrPending = [PendingModel]()

    @IBOutlet weak var lblTextCancel: UILabel!
    @IBOutlet weak var lblTextReceived: UILabel!
    @IBOutlet weak var lblTextDelivering: UILabel!
    @IBOutlet weak var lblTextPending: UILabel!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var lblPending: UILabel!
    @IBOutlet weak var lblDelivering: UILabel!
    @IBOutlet weak var lblReceived: UILabel!
    @IBOutlet weak var lblCancel: UILabel!
    @IBOutlet weak var btnPending: UIButton!
    @IBOutlet weak var btnDelivering: UIButton!
    @IBOutlet weak var btnReceived: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblListorder: UILabel!
    var orderType:Int = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblPending.isHidden = false
        self.lblDelivering.isHidden = true
        self.lblCancel.isHidden = true
        self.lblReceived.isHidden = true
        self.tbView.delegate = self
        self.tbView.dataSource = self
        tbView.register(UINib(nibName: "HistoryOrderPendingCell", bundle: nil), forCellReuseIdentifier: "HistoryOrderPendingCell")
        setText()
        presenter.attachView(self)
        presenter.getOrderHistory(langId : 1 , merchantId: Global.mechantsId! , orderType: self.orderType)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    func setText() {
        self.lblTextPending?.text = NSLocalizedString("btn_pending", comment: "")
        self.lblTextDelivering?.text = NSLocalizedString("btn_delivering", comment: "")
        self.lblTextReceived?.text = NSLocalizedString("btn_received", comment: "")
        self.lblTextCancel?.text = NSLocalizedString("btn_cancel", comment: "")
        self.lblListorder?.text = NSLocalizedString("lbl_list_order", comment: "")
    }

    
    @IBAction func onPending(_ sender: Any) {
        self.lblPending.isHidden = false
        self.lblDelivering.isHidden = true
        self.lblCancel.isHidden = true
        self.lblReceived.isHidden = true
    }
    
    @IBAction func onDelivering(_ sender: Any) {
        self.lblPending.isHidden = true
        self.lblDelivering.isHidden = false
        self.lblCancel.isHidden = true
        self.lblReceived.isHidden = true
    }
    @IBAction func onReceived(_ sender: Any) {
        self.lblPending.isHidden = true
        self.lblDelivering.isHidden = true
        self.lblCancel.isHidden = true
        self.lblReceived.isHidden = false
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.lblPending.isHidden = true
        self.lblDelivering.isHidden = true
        self.lblCancel.isHidden = false
        self.lblReceived.isHidden = true
        
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension HistoryOrderViewController:UITableViewDelegate, UITableViewDataSource , HistoryOrderOutPutProtocol {
    func didFishOrderhistoryCategory(_ PendingOrder: [PendingModel]) {
         ProgressHUD.showProgress()
        self.arrPending = PendingOrder
        self.tbView.reloadData()
         ProgressHUD.hideProgress() 

    }
    
    func didFishOderhistoryError(error: String) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPending.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryOrderPendingCell") as? HistoryOrderPendingCell
        let pending = self.arrPending[indexPath.row] as PendingModel
        cell!.imgShop.kf.setImage(with: URL(string: pending.merchantImgUrl ?? ""))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-mm-dd'T'HH:mm:ss"
        let dateFromInputString = dateFormatter.date(from: pending.orderDate ?? "")
        dateFormatter.dateFormat = "dd-mm-yyyy"
        cell!.lblDate.text = dateFormatter.string(from: dateFromInputString!)
        cell!.lblNameProduct.text = pending.productName ?? ""
        cell?.lblQuantity.text = String(pending.quantity!) ?? ""
        cell?.lblPrice.text = String(pending.productPrice!) ?? ""
        return cell!
    }
    
}
