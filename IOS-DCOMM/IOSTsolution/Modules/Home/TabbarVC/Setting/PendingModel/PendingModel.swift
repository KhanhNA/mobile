//
//  PendingModel.swift
//  IOSTsolution
//
//  Created by TheLightLove on 13/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation

struct PendingModel:Codable {
    var orderId: Int?
    var orderNo:String?
    var merchantId:Int?
    var recvStoreId:Int?
    var amount:Double?
    var vat:Double?
    var incentiveAmount:Double?
    var quantity:Int?
    var orderStatus:Int?
    var orderType:Int?
    var orderDate:String?
    var tax:Double?
    var paymentStatus:Int?
    var deliveryStatus:Int?
    var packingUrl:String?
    var packingProductId:Int?
    var orderPackingId:Int?
    var productName:String?
    var merchantImgUrl:String?
    var productPrice:Double?
    var productQuantity:Int?
    
    
}
