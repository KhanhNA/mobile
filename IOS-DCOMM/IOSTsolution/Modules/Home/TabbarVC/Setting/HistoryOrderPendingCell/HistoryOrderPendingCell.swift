//
//  HistoryOrderPendingCell.swift
//  IOSTsolution
//
//  Created by TheLightLove on 13/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class HistoryOrderPendingCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblShipping: UILabel!
    @IBOutlet weak var imgShop: UIImageView!
    @IBOutlet weak var lblNameProduct: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onSeemore(_ sender: Any) {
    }
    
}
