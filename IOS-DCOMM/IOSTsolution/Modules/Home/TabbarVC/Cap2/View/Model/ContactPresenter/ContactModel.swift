
//
//  ContactModel.swift
//  IOSTsolution
//
//  Created by TheLightLove on 16/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation

struct ContactModel:Codable {
    var merchantId:Int?
    var merchantCode:String?
    var merchantName:String?
    var parentMarchantId:Int?
    var merchantTypeId:Int?
    var userName:String?
    var fullName:String?
    var defaultStoreId:Int?
    var tax:Int?
    var merchantImgUrl:URL?
    var mobilePhone:String?
    var activeCode:String?
    var activeDate:String?
    var registerDate:String?
    var activeStatus:Int?
    var status:Int?
    var address:String?
}
