//
//  ContactPresenter.swift
//  IOSTsolution
//
//  Created by TheLightLove on 16/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import Foundation
import Alamofire

protocol ContacInputProtocol: BaseMVPProtocol {
    func requestContact(parentMerchantId: Int, status: Int)
    
}
protocol ContactOutputProtocol: class {
    func didFinishError(errorMessage: String?)
    func didFishContact(_ Mechants: [ContactModel])
}


class contactPresenter: ContactOutputProtocol {
    func didFinishError(errorMessage: String?) {
        
    }
    
    func didFishContact(_ Mechants: [ContactModel]) {
        
    }
    
    
    var user = AccountModel()
    weak var delegate: ContactOutputProtocol?
    func attachView(_ view: Any) {
        delegate = view as? ContactOutputProtocol
    }
    func requestContact(parentMerchantId: Int, status: Int) {
        let severUrl = Const.GETTOKEN
        ProgressHUD.showProgress()
        Alamofire.request(severUrl, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            ProgressHUD.hideProgress()
            switch response.result {
            case .success(let data):
                print("isi: \(data)")
                if let json = response.value as?  [String: Any] {
                    let access_token = json["access_token"] as? String
                    self.user.access_token = access_token
                    DispatchQueue.main.async {
                        self.getContact(parentMerchantId: Global.mechantsId!, status: 1)
                    }
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
            SessionManager.shared.saveToUserDefault(accountModel: self.user)
        }
    }
    
    
    func getContact(parentMerchantId: Int , status: Int) {
        let body: NSDictionary? = [
            "parentMerchantId":parentMerchantId,
            "status":status
        ]
        var para = CSParamater()
        para[.page] = 0
        para[.langId] = 1
        let router = RouterJsonContact.contact(body: body)
        ProgressHUD.showProgress()
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<ContactModel>?) in
            ProgressHUD.hideProgress()
            guard let reseponseMd = responseModel else {
                self?.delegate?.didFinishError(errorMessage: "Data not exist")
                return
            }
            guard let loginResponse = responseModel?.content else {
                self?.delegate?.didFinishError(errorMessage: "Data Error")
                return
            }
            self?.delegate?.didFishContact(loginResponse)
            
        }
    }
    
    
}
