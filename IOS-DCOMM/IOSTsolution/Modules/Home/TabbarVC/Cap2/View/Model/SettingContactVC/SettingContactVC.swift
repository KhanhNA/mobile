//
//  SettingContactVC.swift
//  IOSTsolution
//
//  Created by TheLightLove on 16/08/2019.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class SettingContactVC: UIViewController ,UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate ,UIActionSheetDelegate,UINavigationControllerDelegate{

    
    @IBOutlet weak var imgProfile: UIImageView!
    var pickerAvatar: UIImagePickerController?
    private var isSelectedImg = false
    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtTax: UITextField!
    @IBOutlet weak var txtAndress: UITextField!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var lblNameContact: UILabel!
    @IBOutlet weak var txtCode: UITextField!
    
    var contact = [ContactEntry]()
    var index:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        let contactObj = contact[index!]
        txtPhoneNumber.text = contactObj.phone ?? ""
        lblNameContact.text = contactObj.name ?? ""
    }
    
    func sexText() {
        txtPhoneNumber.placeholder = NSLocalizedString("lbl_phone_number", comment: "")
        txtTax.placeholder = NSLocalizedString("txt_tax", comment: "")
        txtAndress.placeholder = NSLocalizedString("lbl_andress", comment: "")
        txtMessage.placeholder = NSLocalizedString("txt_message", comment: "")
        txtCode.placeholder = NSLocalizedString("txt_code", comment: "")
    }

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onChooseProfile(_ sender: Any) {
        let alertController = UIAlertController(title: "Ecommerce", message: "", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {(_ action: UIAlertAction?) -> Void in
            self.pickerAvatar = UIImagePickerController()
            self.pickerAvatar?.delegate = self
            self.pickerAvatar?.allowsEditing = true
            self.pickerAvatar?.sourceType = .camera
            if let anAvatar = self.pickerAvatar {
                self.present(anAvatar, animated: true)
            }
        }))
    alertController.addAction(UIAlertAction(title: "Choose From Galery" , style: .default, handler: {(_ action: UIAlertAction?) -> Void in
    self.pickerAvatar = UIImagePickerController()
    self.pickerAvatar?.delegate = self
    self.pickerAvatar?.allowsEditing = true
    self.pickerAvatar?.sourceType = .photoLibrary
    if let anAvatar = self.pickerAvatar {
    self.present(anAvatar, animated: true)
    }
    }))
    alertController.addAction(UIAlertAction(title: "Cancel" , style: .default, handler: {(_ action: UIAlertAction?) -> Void in
//    self.dismiss(animated: true, completion: {
//    return
//    })
    }))
    DispatchQueue.main.async(execute: {() -> Void in
    self.present(alertController, animated: true) {() -> Void in }
    })
}

func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
    if buttonIndex < 2 {
        OperationQueue.main.addOperation({
            self.showImagePicker(withSelection: buttonIndex)
        })
    }
}

func showImagePicker(withSelection selection: Int) {
    pickerAvatar = UIImagePickerController()
    pickerAvatar?.delegate = self
    pickerAvatar?.allowsEditing = true
    switch selection {
    case 1:
        pickerAvatar?.sourceType = .photoLibrary
    case 0:
        pickerAvatar?.sourceType = .camera
    default:
        break
    }
    if let anAvatar = pickerAvatar {
        present(anAvatar, animated: true)
    }
}

func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    picker.dismiss(animated: true)
    let chosenImage = info[.editedImage] as? UIImage
    imgProfile.image = chosenImage
    isSelectedImg = true
}
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 1
    }
    @IBAction func onRegister(_ sender: Any) {
        let contactObj = contact[index!]
        Global.contactObj = contactObj
    }
    
}
