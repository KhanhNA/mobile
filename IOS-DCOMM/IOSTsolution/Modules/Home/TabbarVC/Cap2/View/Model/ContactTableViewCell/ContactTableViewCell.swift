//
//  ContactTableViewCell.swift
//  IOSTsolution
//
//  Created by Phùng Mạnh on 7/18/19.
//  Copyright © 2019 Phùng Mạnh. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var ContactImageView: UIImageView!
    @IBOutlet weak var ContactNameLabel: UILabel!
    @IBOutlet weak var ContactPhoneLabel: UILabel!
    @IBOutlet weak var ContactMoneyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        setCircularAvatar()
    }
    func setCircularAvatar() {
        ContactImageView.layer.cornerRadius = ContactImageView.bounds.size.width / 2.0
        ContactImageView.layer.masksToBounds = true
    }
    func configureWithContactEntry(_ contact: ContactEntry) {
        ContactNameLabel.text = contact.name
        ContactMoneyLabel.text = contact.money ?? ""
        ContactPhoneLabel.text = contact.phone ?? ""
        ContactImageView.image = contact.image ?? UIImage(named: "no_image")
        setCircularAvatar()
    }

}
