<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:bind="http://schemas.android.com/tools"
    xmlns:card_view="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <import type="android.text.Html" />

        <import type="com.ts.dcommerce.util.FontManager" />

        <import type="com.ts.dcommerce.base.AppController" />

        <variable
            name="viewHolder"
            type="com.ts.dcommerce.model.dto.ProductDto" />

        <variable
            name="listenerAdapter"
            type="com.tsolution.base.listener.AdapterListener" />
    </data>

    <androidx.cardview.widget.CardView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_margin="5dp"
        android:background="#fff"
        app:cardCornerRadius="15dp"
        card_view:cardElevation="5dp"
        card_view:cardUseCompatPadding="true">

        <RelativeLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:clickable="true"
            android:focusable="true">

            <ImageView
                android:id="@+id/imgProduct"
                android:layout_width="match_parent"
                android:layout_height="140dp"
                android:layout_alignParentTop="true"
                android:layout_centerHorizontal="true"
                android:onClick="@{(v)->listenerAdapter.onItemClick(v,viewHolder)}"
                android:padding="5dp"
                bind:imageUrl="@{viewHolder.urlImage}" />

            <FrameLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_alignParentTop="true"
                android:layout_alignParentEnd="true"
                android:padding="2dp"
                android:visibility="@{viewHolder.discountPercent == null || viewHolder.discountPercent.intValue()==0 ? View.GONE : View.VISIBLE}">

                <ImageView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center"
                    android:src="@drawable/discount_sticker" />

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center"
                    android:text="@{viewHolder.discountPercent.intValue() + `%`}"
                    android:textColor="#fff"
                    android:textStyle="bold" />
            </FrameLayout>

            <TextView
                android:id="@+id/txtNameProduct"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@+id/imgProduct"
                android:layout_alignParentStart="true"
                android:layout_alignParentEnd="true"
                android:layout_marginEnd="10dp"
                android:ellipsize="end"
                android:maxLines="2"
                android:minLines="2"
                android:onClick="@{(v)->listenerAdapter.onItemClick(v,viewHolder)}"
                android:paddingStart="5dp"
                android:paddingEnd="1dp"
                android:text="@{viewHolder.productName}"
                android:textColor="@color/gray2"
                android:textSize="14sp" />

            <TextView
                android:id="@+id/txtPrice"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@+id/txtNameProduct"
                android:layout_alignStart="@+id/txtNameProduct"
                android:layout_marginTop="2dp"
                android:onClick="@{(v)->listenerAdapter.onItemClick(v,viewHolder)}"
                android:paddingStart="5dp"
                android:paddingEnd="1dp"
                android:text="@{AppController.getInstance().formatCurrency(viewHolder.price)}"
                android:textColor="@color/da_cam"
                android:textSize="14sp" />

            <TextView
                android:id="@+id/txtQuantity"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/txtNameProduct"
                android:layout_marginTop="2dp"
                android:layout_marginBottom="12dp"
                android:layout_toEndOf="@+id/txtPrice"
                android:paddingStart="2dp"
                android:paddingEnd="5dp"
                android:text="@{`/`+ viewHolder.quantity}"
                android:textSize="12sp" />

            <TextView
                android:id="@+id/txtOriginPrice"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@+id/txtPrice"
                android:layout_alignStart="@+id/txtNameProduct"
                android:layout_marginTop="2dp"
                android:paddingStart="5dp"
                android:paddingEnd="1dp"
                android:text="@{Html.fromHtml(`&lt;span style=\'text-decoration:line-through\'>`+ AppController.getInstance().formatCurrency(viewHolder.marketPrice) +`&lt;/span>`)}"
                android:textSize="12sp"
                android:visibility="@{viewHolder.marketPrice == null ? View.INVISIBLE : View.VISIBLE}" />

            <FrameLayout
                android:id="@+id/frCart"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@+id/txtOriginPrice"
                android:layout_alignParentEnd="true"
                android:onClick="@{(v)->listenerAdapter.onItemClick(v,viewHolder)}"
                android:orientation="vertical"
                android:padding="5dp"
                android:paddingEnd="5dp">

                <com.ts.dcommerce.widget.IconText
                    android:id="@+id/iconCart"
                    android:layout_width="wrap_content"
                    android:layout_height="match_parent"
                    android:layout_marginTop="5dp"
                    android:paddingEnd="10dp"
                    android:text="@string/icon_cart"
                    android:textColor="@color/da_cam"
                    android:textSize="24sp"
                    app:typefaceAsset="fonts/font_awesome_pro.otf" />

                <TextView
                    android:id="@+id/txtQuantityProductCart"
                    android:layout_width="20dp"
                    android:layout_height="20dp"
                    android:layout_gravity="end"
                    android:background="@drawable/circle"
                    android:gravity="center"
                    android:padding="1dp"
                    android:text="@{`` + viewHolder.quantityOrdered}"
                    android:textColor="@color/white"
                    android:textSize="10sp"
                    android:visibility="@{viewHolder.quantityOrdered == 0 ? View.GONE : View.VISIBLE}" />
            </FrameLayout>

            <RatingBar
                android:id="@+id/ratingBar"
                style="?android:attr/ratingBarStyleSmall"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@+id/txtOriginPrice"
                android:layout_marginStart="8dp"
                android:isIndicator="true"
                android:max="5"
                android:paddingTop="5dp"
                android:progressTint="#FDDB39"
                android:rating="@{viewHolder.reviewAvg == null ? 0f : viewHolder.reviewAvg}" />
        </RelativeLayout>
    </androidx.cardview.widget.CardView>
</layout>


