package com.ts.dcommerce.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ts.dcommerce.R;
import com.ts.dcommerce.model.db.Administrative;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.viewmodel.AdministrativeVM;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AdministrativeFragment extends BaseFragment {

    private AdministrativeVM addressVM;
    private Intent intent;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        addressVM = (AdministrativeVM) viewModel;
        intent = getBaseActivity().getIntent();
        if (intent != null) {
            if(intent.hasExtra("ADMINISTRATIVE_ID")) {
                int administrativeId = intent.getIntExtra("ADMINISTRATIVE_ID", -1);
                if (administrativeId != -1) {
                    addressVM.getAdministrative(administrativeId);
                } else {
                    addressVM.getAdministrative(-1);
                }
                assert view != null;
                setupToolbar(view, intent.getIntExtra("ADMINISTRATIVE_ACTION", -1));
            }else {
                String administrativeName = intent.getStringExtra("ADMINISTRATIVE_NAME");
                Administrative administrative  = addressVM.getAdministrativeByName(administrativeName);
                if(administrative != null){
                    addressVM.getAdministrative(administrative.getParentId());
                }else {
                    getActivity().finish();
                    ToastUtils.showToast(R.string.requite_province);
                }
            }
        }

        initView();

        return view;
    }

    private void initView() {
        //setup recycleView
        BaseAdapter adapter = new BaseAdapter(R.layout.item_administrative,
                addressVM, this::onClick, getBaseActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    /**
     *
     * @param v view
     * @param administrationAction
     * 1: select province
     * 2: select district
     * 3: select village
     */
    public void setupToolbar(View v, int administrationAction){
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            switch (administrationAction){
                case 1:
                    appCompatActivity.getSupportActionBar().setTitle(R.string.province_field);
                    break;
                case 2:
                    appCompatActivity.getSupportActionBar().setTitle(R.string.district_field);
                    break;
                case 3:
                    appCompatActivity.getSupportActionBar().setTitle(R.string.village_field);
                    break;
                default:
                    appCompatActivity.getSupportActionBar().setTitle(R.string.select_location);
            }

        }
    }

    private void onClick(View view, BaseModel baseModel) {
        intent.putExtra("ADMINISTRATIVE", baseModel);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_administrative;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return AdministrativeVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcAdministrative;
    }
}
