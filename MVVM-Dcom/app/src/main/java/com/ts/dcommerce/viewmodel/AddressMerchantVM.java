package com.ts.dcommerce.viewmodel;

import android.app.Application;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressMerchantVM extends BaseViewModel {
    ObservableField<MerchantAddressDto> addressDto = new ObservableField<>();
    ObservableBoolean isLoading = new ObservableBoolean();
    ObservableField<Integer> numAddress = new ObservableField<>();
    private OrderConfirmVM orderConfirmVM;
    private boolean isSetting = false;

    public AddressMerchantVM(@NonNull Application application) {
        super(application);
        addressDto.set(new MerchantAddressDto());
    }

    public void init(OrderConfirmVM orderConfirmVM) {
        this.orderConfirmVM = orderConfirmVM;
    }

    public void getMerchantAddress() {
        isLoading.set(true);
        callApi(HttpHelper.getInstance().getApi().getMerchantAddress(AppController.getMerchantId())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<MerchantAddressDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<MerchantAddressDto> response) {

                        if (!isSetting) {
                            getMerchantAddressSuccess(response);
                        } else {
                            getSettingAddress(response);
                        }


                    }

                }));
    }

    private void getSettingAddress(ServiceResponse<MerchantAddressDto> value) {
        if (value != null && TsUtils.isNotNull(value.getArrData())) {
            setData(value.getArrData());
            baseModels.notifyChange();
            try {
                view.action("getAddress", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
        isLoading.set(false);
    }

    private void getMerchantAddressSuccess(ServiceResponse<MerchantAddressDto> value) {
        int positionDefault = 0;
        if (value != null && TsUtils.isNotNull(value.getArrData())) {
            numAddress.set(value.getArrData().size());
            List<MerchantAddressDto> merchantAddressDtos = value.getArrData();
            for (int i = 0; i < merchantAddressDtos.size(); i++) {
                MerchantAddressDto addressDTO = merchantAddressDtos.get(i);
                if (addressDTO.getIsSelect() == 1) {
                    positionDefault = i;
                    addressDTO.checked = true;
                    orderConfirmVM.changeAddress(addressDTO);
                } else {
                    addressDTO.checked = false;
                }
            }
            Collections.swap(merchantAddressDtos, 0, positionDefault);
            if (orderConfirmVM != null && orderConfirmVM.getMerchantAddressDto() == null && merchantAddressDtos.size() > 0) {
                merchantAddressDtos.get(0).checked = true;
                orderConfirmVM.changeAddress(merchantAddressDtos.get(0));
            }
            setData(merchantAddressDtos);
            baseModels.notifyChange();
            try {
                view.action("getAddressSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
        isLoading.set(false);
    }

    //invoke func
    public void onRefresh() {
        getMerchantAddress();
    }

}
