package com.ts.dcommerce.ui.fragment;

import com.ts.dcommerce.R;

public class TemplateOrderDialog extends BaseDialogFragment {
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_order_template;
    }
}
