package com.ts.dcommerce.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceResponse<T> {
    @SerializedName("content")
    private List<T> arrData;

    @SerializedName("totalPages")
    private Integer totalPages;
}
