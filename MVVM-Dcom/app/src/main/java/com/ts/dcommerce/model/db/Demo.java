package com.ts.dcommerce.model.db;

import com.tsolution.base.BaseModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(
        indexes = {
                @Index(value = "productId DESC, quantity DESC", unique = true)
        }
)
public class Demo extends BaseModel {
    private Long productId;
    private String productName;
    private String packingProductCode;
    private Double price;
    private String url;
    private Integer quantity;
    private Double orderQuantity;
    private Long packingProductId;

    @Generated(hash = 1803093053)
    public Demo(Long productId, String productName, String packingProductCode, Double price,
            String url, Integer quantity, Double orderQuantity, Long packingProductId) {
        this.productId = productId;
        this.productName = productName;
        this.packingProductCode = packingProductCode;
        this.price = price;
        this.url = url;
        this.quantity = quantity;
        this.orderQuantity = orderQuantity;
        this.packingProductId = packingProductId;
    }

    @Generated(hash = 571290164)
    public Demo() {
    }

    // Dung xoa
    public String  getOrderQuantityStr(){
        return orderQuantity == null || orderQuantity < 0 ? "1" : orderQuantity.intValue()+"";
    }

    public void setOrderQuantityStr(String s){
        if(s.lastIndexOf(".")!=-1 || "".equals(s)){
            return;
        }
        if(s.equals("0")){
            orderQuantity = 1d;
        }
        else{
            orderQuantity = Double.parseDouble(s);
        }
    }
    // End

    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPackingProductCode() {
        return this.packingProductCode;
    }

    public void setPackingProductCode(String packingProductCode) {
        this.packingProductCode = packingProductCode;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getOrderQuantity() {
        return this.orderQuantity;
    }

    public void setOrderQuantity(Double orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public Long getPackingProductId() {
        return this.packingProductId;
    }

    public void setPackingProductId(Long packingProductId) {
        this.packingProductId = packingProductId;
    }
    
}