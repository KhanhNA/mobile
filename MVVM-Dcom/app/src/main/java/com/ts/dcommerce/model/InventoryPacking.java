package com.ts.dcommerce.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InventoryPacking {
    private String productPackingCode;
    private Integer quantity;
    private Integer inventory;
    private Integer maxDayToWait;

    public InventoryPacking(String code, Integer q) {
        this.productPackingCode = code;
        this.quantity = q;
    }
}
