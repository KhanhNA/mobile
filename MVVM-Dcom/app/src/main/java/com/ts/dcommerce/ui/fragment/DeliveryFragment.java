package com.ts.dcommerce.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.CustomArrayAdapter;
import com.ts.dcommerce.adapter.MethodAdapter;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.model.InventoryPacking;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.db.DemoDao;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.model.dto.MerchantWareHouseDTO;
import com.ts.dcommerce.model.dto.MethodDTO;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.model.dto.OrderProductCartDTO;
import com.ts.dcommerce.model.dto.stockAvailable.AvailableStock;
import com.ts.dcommerce.model.dto.stockAvailable.CheckInventoryDTO;
import com.ts.dcommerce.ui.MainActivity;
import com.ts.dcommerce.ui.NewAddressActivity;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.AddressMerchantVM;
import com.ts.dcommerce.viewmodel.DeliveryVM;
import com.ts.dcommerce.viewmodel.OrderConfirmVM;
import com.ts.dcommerce.widget.BottomSheetEditCart;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.greendao.rx.RxDao;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DeliveryFragment extends BaseFragment implements BottomSheetEditCart.CallbackUpdate {
    @SuppressLint("StaticFieldLeak")
    private static DeliveryFragment mInstance;
    private OrderConfirmVM orderConfirmVM;
    private DeliveryVM deliveryVM;
    private int selectedWareHousePosition;
    private BaseAdapter adapterProduct;
    public static boolean isRefresh = false;
    private RxDao<Demo, Void> noteDao;
    private AddressMerchantVM addressMerchantVM;
    private BaseAdapter addressAdapter;
    private RecyclerView rcAddress;
    private Spinner spStore;
    private ProgressDialog progressDialog;
    private List<String> stores;

    public static DeliveryFragment getInstance() {
        if (mInstance == null) {
            mInstance = new DeliveryFragment();
        }
        return mInstance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        orderConfirmVM = ViewModelProviders.of(getBaseActivity()).get(OrderConfirmVM.class);
        deliveryVM = (DeliveryVM) viewModel;
        addressMerchantVM = new AddressMerchantVM(AppController.getInstance());
        binding.setVariable(com.ts.dcommerce.BR.viewModelAddress, addressMerchantVM);
        //
        initView();
        //Get Address Of Merchant
        addressAdapter = new BaseAdapter(R.layout.item_address, addressMerchantVM, this::onItemClick, getBaseActivity());
        rcAddress.setAdapter(addressAdapter);
        addressMerchantVM.init(orderConfirmVM);
        addressMerchantVM.setView(this::processFromVM);
        addressMerchantVM.getMerchantAddress();
        //
        DaoSession daoSession = AppController.getInstance().getDaoSession();
        noteDao = daoSession.getDemoDao().rx();

        // RecyclerView 1 sản phẩm
        adapterProduct = new BaseAdapter(R.layout.item_order_confirm, orderConfirmVM.arrOrderProduct, this::onItemClick, getBaseActivity());
        recyclerView.setAdapter(adapterProduct);
        GridLayoutManager lmProduct =
                new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(lmProduct);
        //
        deliveryVM.getSelectedStorePosition().observe(this, this::checkAvailable);

        return binding.getRoot();
    }

    private void checkAvailable(int position) {
        try {
            List<MerchantWareHouseDTO> lst = deliveryVM.getLstStore().get();
            if (TsUtils.isNotNull(lst)) {
                showProgress();
                orderConfirmVM.getOrderProductCartDTO().setMerchantWareHouseCode(lst.get(position).getStoreCode());
                // Kiem tra ton kho
                getStore(lst);

                List<InventoryPacking> packings = new ArrayList<>();
                if (TsUtils.isNotNull(orderConfirmVM.arrOrderProduct.getBaseModelsE()) && TsUtils.isNotNull(stores)) {
                    for (OrderPackingDTO packingDTO : (List<OrderPackingDTO>) orderConfirmVM.arrOrderProduct.getBaseModelsE()) {
                        packings.add(new InventoryPacking(packingDTO.getPackingProductCode(), packingDTO.getQuantity()));
                    }
                    callApiCheckAvailable(packings, stores, stores.get(position));
                } else {
                    closeProgress();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getStore(List<MerchantWareHouseDTO> lst) {
        if (!TsUtils.isNotNull(stores)) {
            for (int i = 0; i < lst.size(); i++) {
                stores.add(lst.get(i).getStoreCode());
            }
        }
    }

    private void callApiCheckAvailable(List<InventoryPacking> packings, List<String> stores, String destinationStore) {
        CheckInventoryDTO dto = new CheckInventoryDTO();
        dto.setDestinationStore(destinationStore);
        dto.setMerchantOrderInventoryDetails(packings);
        dto.setStores(stores);
        deliveryVM.checkAvailable(dto);
    }

    private void initView() {
        //
        progressDialog = new ProgressDialog(getBaseActivity());
        progressDialog.setMessage(getString(R.string.checking_available_stock));

        stores = new ArrayList<>();
        deliveryVM.init(orderConfirmVM);
        deliveryVM.getStoreOfMerchant();
        View rootView = binding.getRoot();

        spStore = rootView.findViewById(R.id.spToStore);
        rcAddress = rootView.findViewById(R.id.rcAddress);
        RecyclerView.LayoutManager layoutManagerAddress = new LinearLayoutManager(this.getActivity());
        rcAddress.setLayoutManager(layoutManagerAddress);
        //
        RecyclerView rcDelivery = rootView.findViewById(R.id.rcDelivery);
        // Dump Data
        ArrayList<MethodDTO> arrPayment = new ArrayList<>();
        arrPayment.add(new MethodDTO(2L, getString(R.string.ship), Constants.SHIP_COD, R.drawable.delivery_truck));
        arrPayment.add(new MethodDTO(1L, getString(R.string.pickup), Constants.GO_STORE, R.drawable.store));
        // Default Ship method
        orderConfirmVM.getOrderProductCartDTO().setShipMethod(arrPayment.get(0));
        //
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        MethodAdapter mAdapter = new MethodAdapter(arrPayment);
        rcDelivery.setLayoutManager(layoutManager);
        rcDelivery.setAdapter(mAdapter);
        //
        mAdapter.setListener(methodDTO -> {
            orderConfirmVM.getOrderProductCartDTO().setShipMethodId(methodDTO.getPaymentMethodId());
            orderConfirmVM.getOrderProductCartDTO().setShipMethod(methodDTO);
            if (Constants.SHIP_COD.equalsIgnoreCase(methodDTO.getPaymentCode())) {
                deliveryVM.getIsStoreShow().set(false);
            } else if (Constants.GO_STORE.equalsIgnoreCase(methodDTO.getPaymentCode())) {
                deliveryVM.getIsStoreShow().set(true);
            }
        });
    }

    @Override
    public void adapterAction(View view, BaseModel baseModel) {
        OrderPackingDTO orderPackingDTO = ((OrderPackingDTO) baseModel);
        noteDao.getDao().queryBuilder().where(DemoDao.Properties.PackingProductId.eq(orderPackingDTO.getPackingProductId()))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
        if (TsUtils.isNotNull(orderConfirmVM.arrOrderProduct.getBaseModelsE())) {
            for (int i = 0; i < orderConfirmVM.arrOrderProduct.getBaseModelsE().size(); i++) {
                OrderPackingDTO dto = (OrderPackingDTO) orderConfirmVM.arrOrderProduct.getBaseModelsE().get(i);
                if (dto.getPackingProductId().equals(orderPackingDTO.getPackingProductId())) {
                    orderConfirmVM.arrOrderProduct.getBaseModelsE().remove(i);
                    DeliveryFragment.isRefresh = true;
                    adapterProduct.notifyDataSetChanged();
                    //
                    checkAvailable(deliveryVM.getSelectedStorePosition().getValue());
                    ToastUtils.showToast(R.string.delete_success);
                    break;
                }
            }
        }
        // Kiem tra gio hang sau khi xoa
        if (!TsUtils.isNotNull(orderConfirmVM.arrOrderProduct.getBaseModelsE())) {
            showContinueShopping();
        }
    }

    private void showContinueShopping() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getBaseActivity());
        // set title
        alertDialogBuilder.setTitle(getString(R.string.empty_cart));
        alertDialogBuilder.setCancelable(false);
        // set dialog message
        alertDialogBuilder
                .setIcon(R.drawable.empty_cart)
                .setMessage(getString(R.string.continue_shopping))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), (dialog, id) -> {
                    Intent intent = new Intent(getBaseActivity(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public void onItemClick(View view, BaseModel model) {
        switch (view.getId()) {
            case R.id.btnEdit:
                BottomSheetEditCart sheetBehavior = new BottomSheetEditCart((OrderPackingDTO) model);
                sheetBehavior.setCallbackUpdate(this);
                assert getFragmentManager() != null;
                sheetBehavior.show(getFragmentManager(), sheetBehavior.getTag());
                break;
            case R.id.btnDel:
                showAlertDialog(R.string.delete_product, this, model);
                break;
            case R.id.rbChoose:
                if (!model.checked) {
                    List baseModelsE = addressMerchantVM.getBaseModelsE();
                    if (TsUtils.isNotNull(baseModelsE)) {
                        model.checked = true;
                        if (orderConfirmVM.getMerchantAddressDto() != null) {
                            orderConfirmVM.getMerchantAddressDto().checked = false;
                        }
                        orderConfirmVM.changeAddress((MerchantAddressDto) model);
                        List<MerchantAddressDto> dtos = (List<MerchantAddressDto>) addressMerchantVM.baseModels.get();
                        addressMerchantVM.baseModels.notifyChange();
                        if (dtos != null) {
                            addressAdapter.notifyItemChanged(dtos.indexOf(orderConfirmVM.getMerchantAddressDto()));
                        }
                    }
                    orderConfirmVM.getSwipeable().set(true);

                } else {
                    model.checked = false;
                    orderConfirmVM.resetAddress();
                    orderConfirmVM.getSwipeable().set(false);
                }
                break;
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.btnPayment) {
            EventBus.getDefault().post(1);
        }

        switch (view.getId()) {
            case R.id.spToStore:
                orderConfirmVM.setWareHouse(selectedWareHousePosition);
                break;
            case R.id.fast_delivery:
                break;
            case R.id.me_delivery:
//                Intent delivery = new Intent(getBaseActivity(), DeliveryActivity.class);
//                OrderDetail orderDetail = new OrderDetail();
//                delivery.putExtra(DeliveryConfiguration.ORDER_DETAIL, orderDetail);
//                startActivityForResult(delivery, DELIVERY);
                break;
            case R.id.lnAddAddress:
                Intent intent = new Intent(getBaseActivity(), NewAddressActivity.class);
                startActivityForResult(intent, 1);
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                addressMerchantVM.getMerchantAddress();
            }
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "getAddressSuccess":
                addressAdapter.notifyDataSetChanged();
                break;
            case "checkAvailableSuccess":
                closeProgress();
                break;
            case "getStoreSuccess":
                CustomArrayAdapter storeAdapter = new CustomArrayAdapter(getBaseActivity(), R.layout.item_merchant_warehouse, Objects.requireNonNull(deliveryVM.getLstStore().get()));
                spStore.setAdapter(storeAdapter);
                // Default store
                List<MerchantWareHouseDTO> lst = deliveryVM.getLstStore().get();
                if (TsUtils.isNotNull(lst)) {
                    orderConfirmVM.getOrderProductCartDTO().setMerchantWareHouseCode(lst.get(0).getStoreCode());
                }
                break;
        }

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_delivery;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DeliveryVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.recyclerSanPham;
    }

    @Override
    public void callBack(OrderPackingDTO dto) {
        adapterProduct.notifyItemChanged(dto.index - 1);
        // Kiem tra ton kho
        checkAvailable(deliveryVM.getSelectedStorePosition().getValue());
    }

    private void showProgress() {
        if (!getBaseActivity().isFinishing() && !progressDialog.isShowing()) {
            try {
                progressDialog.show();
            } catch (WindowManager.BadTokenException e) {
                Log.e("WindowManagerBad ", e.toString());
            }
        }
    }

    private void closeProgress() {
        if (progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (WindowManager.BadTokenException e) {
                Log.e("WindowManagerBad ", e.toString());
            }
        }
    }
}
