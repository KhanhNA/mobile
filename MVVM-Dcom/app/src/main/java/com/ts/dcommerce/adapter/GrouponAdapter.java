package com.ts.dcommerce.adapter;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ts.dcommerce.BR;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.databinding.ItemGrouponV2Binding;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

/**
 * Created by PhamBien.
 */
public class GrouponAdapter extends RecyclerView.Adapter<GrouponAdapter.ViewHolder> implements AdapterListener {
    private AdapterListener adapterListener;
    private List<ProductPackingDto> datas;
    private LayoutInflater layoutInflater;
    private BaseActivity mContext;

    public GrouponAdapter(BaseActivity context, List<ProductPackingDto> arr, AdapterListener listener) {
        this.datas = arr;
        this.adapterListener = listener;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        ItemGrouponV2Binding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_groupon_v2, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        ProductPackingDto productDto = datas.get(position);
        productDto.index = position + 1;
        Glide.with(mContext).load(AppController.BASE_URL + "files" + productDto.getPackingUrl()).into(holder.imgPacking);
        holder.bind(productDto);
        //count douwn
        if (holder.timer != null) {
            holder.timer.cancel();
        }
        if (productDto.getDuration() != null) {
            holder.timer = new CountDownTimer((productDto.getDuration() - 1) * 60 * 1000L, 1000) {
                public void onTick(long millisUntilFinished) {
                    long seconds = millisUntilFinished / 1000;
                    long minutes = seconds / 60;
                    long hours = minutes / 60;
                    long days = hours / 24;
                    String time;
                    if (days > 0) {
                        time = days + " days " + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
                    } else {
                        time = hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
                    }
                    holder.txtDuration.setText(time);
                }

                public void onFinish() {
                    holder.txtDuration.setText("Time up!");
                }
            }.start();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);

    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public void onItemClick(View v, Object o) {
        if (adapterListener != null) {
            adapterListener.onItemClick(v, o);
        }
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemGrouponV2Binding itemProductBinding;
        private CountDownTimer timer;
        public TextView txtDuration;
        public ImageView imgPacking;

        public ViewHolder(ItemGrouponV2Binding view) {
            super(view.getRoot());
            this.itemProductBinding = view;
            txtDuration = itemView.findViewById(R.id.txtDuration);
            imgPacking = itemView.findViewById(R.id.imgPacking);
        }

        public void bind(Object obj) {
            itemProductBinding.setVariable(BR.viewHolder, obj);
            itemProductBinding.setVariable(BR.listener, adapterListener);
            itemProductBinding.executePendingBindings();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}