
package com.ts.dcommerce.model.dto.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountPayment {

    @SerializedName("WALLET")
    @Expose
    private Wallet wallet;

    @SerializedName("POINT")
    @Expose
    private Point point;

    @SerializedName("coin")
    @Expose
    private Coin coin;


}
