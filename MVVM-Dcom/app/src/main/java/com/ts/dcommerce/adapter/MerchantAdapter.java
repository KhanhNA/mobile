package com.ts.dcommerce.adapter;

import androidx.annotation.NonNull;

import com.ts.dcommerce.viewmodel.MerchantVM;
import com.tsolution.base.listener.AdapterListener;

public class MerchantAdapter extends BaseAdapterV2 {
    private MerchantVM viewModel;

    public MerchantAdapter(int item, MerchantVM vm, AdapterListener listener) {
        super(item, vm, listener);
        this.viewModel = vm;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        viewHolder.bindWithVM(viewModel);
    }

}
