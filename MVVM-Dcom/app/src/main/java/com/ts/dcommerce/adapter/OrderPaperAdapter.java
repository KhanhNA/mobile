package com.ts.dcommerce.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ts.dcommerce.ui.fragment.OrderChildFragment;

import lombok.Getter;

@Getter
public class OrderPaperAdapter extends FragmentPagerAdapter {
    private String[] TITLES;
    private OrderChildFragment orderChildFragment;
    public OrderPaperAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        orderChildFragment = new OrderChildFragment();
        orderChildFragment.setCurrentTab(position);
        return orderChildFragment;
    }


}