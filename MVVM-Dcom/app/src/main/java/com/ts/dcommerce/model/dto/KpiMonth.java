package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KpiMonth extends BaseModel {
    private Long id;
    private String kpiName;
    private Date month;
    private Long kpiId;
    private Long merchantId;
    private Double plan;
    private Double done;
    private Double bonus;
    private String createUser;
    private Date createDate;
    private String updateUser;
    private Date updateDate;

    public KpiMonth() {
    }

    public KpiMonth(Long id, Date month, Long kpiId, Long merchantId, Double plan, Double done, Double bonus, String createUser, Date createDate, String updateUser, Date updateDate) {
        this.id = id;
        this.month = month;
        this.kpiId = kpiId;
        this.merchantId = merchantId;
        this.plan = plan;
        this.done = done;
        this.bonus = bonus;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
    }
}
