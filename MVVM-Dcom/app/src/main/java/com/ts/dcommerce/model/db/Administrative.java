package com.ts.dcommerce.model.db;

import com.tsolution.base.BaseModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;

@Entity(
        indexes = {
                @Index(value = "id DESC", unique = true)
        }
)
public class Administrative extends BaseModel {
    private Integer id;
    private Integer parentId;
    private String name;
@Generated(hash = 1725084820)
public Administrative(Integer id, Integer parentId, String name) {
    this.id = id;
    this.parentId = parentId;
    this.name = name;
}
@Generated(hash = 115737639)
public Administrative() {
}
public Integer getId() {
    return this.id;
}
public void setId(Integer id) {
    this.id = id;
}
public Integer getParentId() {
    return this.parentId;
}
public void setParentId(Integer parentId) {
    this.parentId = parentId;
}
public String getName() {
    return this.name;
}
public void setName(String name) {
    this.name = name;
}
}
