package com.ts.dcommerce.viewmodel;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import android.provider.ContactsContract;
import android.util.Log;

import androidx.annotation.NonNull;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.db.ContactDB;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.model.dto.OverviewL2DTO;
import com.ts.dcommerce.model.dto.TokenFbMerchantDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.StringUtils;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.greendao.rx.RxDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantVM extends BaseViewModel {
    private RxDao<ContactDB, Void> noteDao;
    private ObservableField<Integer> lstChecked = new ObservableField<>(0);
    private ObservableField<Boolean> edit = new ObservableField<>(false);
    public ObservableBoolean isLoading = new ObservableBoolean();
    List<MerchantDto> listMerchant, contacts;
    HashMap<String, MerchantDto> hashMapMerchant;
    BaseViewModel arrMerchant;
    ObservableField<OverviewL2DTO> overViewL2 = new ObservableField<>();


    public MerchantVM(@NonNull Application application) {
        super(application);
        isLoading.set(false);
        edit.set(false);
        arrMerchant = new BaseViewModel(application);
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        noteDao = daoSession.getContactDBDao().rx();
    }


    public void getMerchants() {
        //set MerchantDto

        MerchantDto merchantDto = new MerchantDto();
        merchantDto.setParentMarchantId(AppController.getMerchantId());
        merchantDto.setStatus(1);
        callApi(HttpHelper.getInstance().getApi().getBaseMerchants(merchantDto, 0, 0)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<MerchantDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<MerchantDto> response) {
                        getMerchantsSuccess(response);
                    }
                }));
    }

    private void getMerchantsSuccess(ServiceResponse<MerchantDto> value) {
        if (value != null) {
            listMerchant = new ArrayList<>();
            hashMapMerchant = new HashMap<>();
            listMerchant = value.getArrData();

            for (MerchantDto merchantDto : listMerchant) {
                hashMapMerchant.put(merchantDto.getMobilePhone(), merchantDto);
                merchantDto.setRegister(true);
            }
            try {
                view.action("getMerchants", null, null, null);
            } catch (com.tsolution.base.exceptionHandle.AppException e) {
                e.printStackTrace();
            }


        }
        isLoading.set(false);
    }

    public void getTotalRegisters() {
        callApi(HttpHelper.getInstance().getApi().overview(AppController.getMerchantId())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<OverviewL2DTO>() {
                    @Override
                    public void onSuccess(OverviewL2DTO dto) {
                        if (dto != null) {
                            overViewL2.set(dto);
                        }
                    }
                }));
    }

    /**
     * @author Good_Boy
     */
    public void getContacts(Context mContext) {
        contacts = new ArrayList<>();
        // Get the ContentResolver
        ContentResolver cr = mContext.getContentResolver();
        //get img contact from sqlLite
        HashMap<String, String> imgs = getContactsDB();
        // Get the Cursor of all the contacts
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        // Move the cursor to first. Also check whether the cursor is empty or not.
        assert cursor != null;
        if (cursor.moveToFirst()) {
            // Iterate through the cursor
            do {
                // Get the contacts name
                MerchantDto contact = new MerchantDto();
                contact.setFullName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                contact.setMobilePhone(number);
                String img = imgs.get(number);
                if (!StringUtils.isNullOrEmpty(img)) {
                    contact.setMerchantImgUrl(img);
                }
                contact.setContactEmail(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DISPLAY_NAME)));
                MerchantDto merchantDto = hashMapMerchant.get(contact.getMobilePhone());
                if (merchantDto != null) {
                    int index = listMerchant.indexOf(contact);
                    if (index != -1) {
                        listMerchant.get(index).setFullName(contact.getFullName());
                    }
                    continue;
                }
                contacts.add(contact);
            } while (cursor.moveToNext());
        }
        cursor.close();
        sortContact((ArrayList<MerchantDto>) contacts);
        setData(contacts);
        sortContact((ArrayList<MerchantDto>) listMerchant);
        arrMerchant.setData(listMerchant);
    }

    private void sortContact(ArrayList<MerchantDto> temp) {
        Collections.sort(temp);
    }

    private HashMap<String, String> getContactsDB() {
        HashMap<String, String> imgs = new HashMap<>();
        List<ContactDB> list = noteDao.getDao().queryBuilder().list();
        for (ContactDB contactDB : list) {
            imgs.put(contactDB.getContactId(), contactDB.getUrlImg());
        }
        return imgs;
    }

    //invoke
    public void onRefresh() {
        isLoading.set(true);
        getMerchants();
    }

    public void updateMerchant(MerchantDto merchantDto) {
        callApi(HttpHelper.getInstance().getApi().updateMerchant(merchantDto)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<MerchantDto>() {
                    @Override
                    public void onSuccess(MerchantDto categoryServiceResponse) {
                        System.out.println("Update thanh cong");
                    }
                }));
    }

    public void updateToken(TokenFbMerchantDTO fbMerchantDTO) {
        callApi(HttpHelper.getInstance().getApi().updateToken(fbMerchantDTO)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<TokenFbMerchantDTO>() {
                    @Override
                    public void onSuccess(TokenFbMerchantDTO categoryServiceResponse) {
                        System.out.println("Update thanh cong");
                    }
                }));
    }


}
