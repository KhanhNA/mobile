package com.ts.dcommerce.ui;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ts.dcommerce.R;
import com.ts.dcommerce.util.IntentConstants;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.viewmodel.RegisterMerchantVM;
import com.ts.dcommerce.viewmodel.UserInfoVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import java.util.Objects;

public class RegisterMerchantActivity extends BaseActivity implements View.OnFocusChangeListener {
    RegisterMerchantVM registerMerchantVM;
    EditText edHoTenDK, edPassWord, edInputAgainPass, edPhone;
    TextInputLayout input_edHoTenDK;
    TextInputLayout input_edMatKhauDK;
    TextInputLayout input_edNhapLaiMatKhauDK;
//    TextInputLayout input_edHoTenLogin;
    TextInputLayout input_edPhone;
    Boolean checkInfo = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerMerchantVM = (RegisterMerchantVM) viewModel;
        String inputCode = (String) Objects.requireNonNull(getIntent().getExtras().get(IntentConstants.INTENT_INPUT_CODE));
        registerMerchantVM.setActiveCode(inputCode);
        initView();
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case IntentConstants.INTENT_REGISTER_SUCCESS:
                runOnUiThread(() -> {
                    ToastUtils.showToast(getString(R.string.create_account_success));
                });
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result_inputCode", ((RegisterMerchantVM) viewModel).userName);
                returnIntent.putExtra("result_inputCode_parentName", ((RegisterMerchantVM) viewModel).parentMerchantName);
                setResult(1111, returnIntent);
                finish();
                break;
            case IntentConstants.INTENT_START_MAIN:
                startActivity(new Intent(RegisterMerchantActivity.this, MainActivity.class));
                break;
            case IntentConstants.INTENT_NOT_INPUT_FULL_NAME:
                errorNotInput(input_edHoTenDK, edHoTenDK);
                break;
            case IntentConstants.INTENT_NOT_INPUT_MOBILE_PHONE:
                errorNotInput(input_edPhone, edPhone);
                break;
//            case IntentConstants.INTENT_NOT_INPUT_NAME_LOGIN:
//                errorNotInput(input_edHoTenLogin, edHoTenLogin);
//                break;
            case IntentConstants.INTENT_NOT_INPUT_PASSWORD:
                errorNotInput(input_edMatKhauDK, input_edMatKhauDK.getEditText());
                break;
            case IntentConstants.INTENT_NOT_INPUT_PASSWORD_SECOND:
                comparePassword();
//                errorNotInput(input_edHoTenDK,edHoTenDK);
                break;
            case IntentConstants.INTENT_PASSWORD_NOT_EQUALS:
                comparePassword();
                break;
        }

    }

    private void initView() {
        edHoTenDK = findViewById(R.id.edHoTenDK);
        edPassWord = findViewById(R.id.edMatKhauDK);
        edInputAgainPass = findViewById(R.id.edNhapLaiMatKhauDK);
//        edHoTenLogin = findViewById(R.id.edHoTenLogin);
        edPhone = findViewById(R.id.edPhone);
        input_edHoTenDK = findViewById(R.id.input_edHoTenDK);
        input_edMatKhauDK = findViewById(R.id.input_edMatKhauDK);
        input_edNhapLaiMatKhauDK = findViewById(R.id.input_edNhapLaiMatKhauDK);
//        input_edHoTenLogin = findViewById(R.id.input_edHoTenLogin);
        input_edPhone = findViewById(R.id.input_edPhone);

        edHoTenDK.setOnFocusChangeListener(this);
//        edHoTenLogin.setOnFocusChangeListener(this);
        edPhone.setOnFocusChangeListener(this);
        edPassWord.setOnFocusChangeListener(this);
        edInputAgainPass.setOnFocusChangeListener(this);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_register_level_2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return RegisterMerchantVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    private void errorNotInput(TextInputLayout textInputLayout, final View view) {
        String chuoi = ((EditText) view).getText().toString();
        if ("".equals(chuoi.trim())) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(getString(R.string.YOU_NOT_INPUT));
            checkInfo = false;
        } else {
            textInputLayout.setErrorEnabled(false);
            textInputLayout.setError("");
            checkInfo = true;
        }
    }

    @Override
    public void onFocusChange(final View view, final boolean b) {
        int id = view.getId();
        switch (id) {
            case R.id.edHoTenDK:
                if (!b) {
                    errorNotInput(input_edHoTenDK, edHoTenDK);
                }
                break;
            case R.id.edPhone:
                if (!b) {
                    errorNotInput(input_edPhone, edPhone);
                }
                break;
//            case R.id.edHoTenLogin:
//                if (!b) {
//                    errorNotInput(input_edHoTenLogin, edHoTenLogin);
//                }
//                break;

            case R.id.edMatKhauDK:
                break;

            case R.id.edNhapLaiMatKhauDK:
                if (!b) {
                    errorNotInput(input_edNhapLaiMatKhauDK, input_edNhapLaiMatKhauDK.getEditText());
                    comparePassword();
                }
                break;

        }
    }

    private void comparePassword() {
        String chuoi = edInputAgainPass.getText().toString();
        String matkhau = edPassWord.getText().toString();
        if (!chuoi.equals(matkhau)) {
            input_edNhapLaiMatKhauDK.setErrorEnabled(true);
            input_edNhapLaiMatKhauDK.setError(getString(R.string.PASSWORD_NOT_COINCIDE));
            checkInfo = false;
        } else {
            input_edNhapLaiMatKhauDK.setErrorEnabled(false);
            input_edNhapLaiMatKhauDK.setError("");
            checkInfo = true;
        }
    }
}
