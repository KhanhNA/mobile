package com.ts.dcommerce.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ns.chat.BR;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.ui.NewAddressActivity;
import com.ts.dcommerce.viewmodel.AddressMerchantVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class AddressSettingFragment extends BaseFragment {
    private int REQUEST_CODE_ADD_ADDRESS = 111;
    private BaseAdapterV2 baseAdapter;
    private AddressMerchantVM addressMerchantVM;
    Toolbar toolbar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreateView(inflater, container, savedInstanceState);
        addressMerchantVM = (AddressMerchantVM) viewModel;
        addressMerchantVM.setSetting(true);
        baseAdapter = new BaseAdapterV2(R.layout.item_setting_address, addressMerchantVM, this);
        recyclerView.setAdapter(baseAdapter);


        addressMerchantVM.getMerchantAddress();
        initView(binding.getRoot());
        return binding.getRoot();
    }

    private void initView(View v) {

        toolbar = v.findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        assert appCompatActivity != null;
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        if(v.getId() == R.id.itemAddress){
            Intent intent = new Intent(getBaseActivity(), NewAddressActivity.class);
            intent.putExtra("MERCHANT_ADDRESS", (MerchantAddressDto)o);
            startActivityForResult(intent, REQUEST_CODE_ADD_ADDRESS);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == REQUEST_CODE_ADD_ADDRESS && data != null){
                MerchantAddressDto result = (MerchantAddressDto) data.getSerializableExtra("MERCHANT_ADDRESS");
                if(result != null){

                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if ("getAddress".equals(action)) {
            baseAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_address_setting;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return AddressMerchantVM.class;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if(view.getId() == R.id.lnAddAddress){
            Intent intent = new Intent(getBaseActivity(), NewAddressActivity.class);
            startActivityForResult(intent, REQUEST_CODE_ADD_ADDRESS);
        }
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcAddress;
    }
}
