package com.ts.dcommerce.model.dto;


import androidx.lifecycle.ViewModel;
import androidx.databinding.BindingAdapter;
import android.widget.ImageView;

import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationMerchantDTO extends BaseModel {
    private Long id;
    private String title;
    private String message;
    private Long actionId;
    private Long merchantReceiverId;
    private Long senderId;
    private Integer typeId;//1:Order; 2: Groupon; 3: Gioi thieu
    private String customData;
    private Date createDate;
    private String token;
    private String topic;
    private String senderName;
    private Integer isSeen; // 0: Chua xem , 1:Da xem
    private String click_action;
    @BindingAdapter("android:src")
    public static void setImageResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }

    private int img;

}
