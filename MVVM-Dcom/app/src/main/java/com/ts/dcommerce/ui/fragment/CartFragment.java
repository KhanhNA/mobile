package com.ts.dcommerce.ui.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.ts.dcommerce.R;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.CartVM;
import com.ts.dcommerce.widget.ClearableEditText;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterActionsListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CartFragment extends BaseFragment implements AdapterActionsListener, ClearableEditText.Listener {
    private CartVM cartVM;
    private BottomSheetBehavior sheetBehavior;
    private TextView hideBottomSheet;
    private List<Demo> packingsGroupon;
    private Long registerGrouponId;
    private BaseAdapter cartAdapter;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreateView(inflater, container, savedInstanceState);
        cartVM = (CartVM) viewModel;
        packingsGroupon = new ArrayList<>();
        if (getBaseActivity().getIntent() != null && getBaseActivity().getIntent().hasExtra("packingDTOS")) {
            packingsGroupon = (List<Demo>) getBaseActivity().getIntent().getExtras().getSerializable("packingDTOS");
            cartVM.setData(packingsGroupon);
        }
        if (getBaseActivity().getIntent() != null && getBaseActivity().getIntent().hasExtra("registerGrouponId")) {
            registerGrouponId = getBaseActivity().getIntent().getExtras().getLong("registerGrouponId");

        }
        initView();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (TsUtils.isNotNull(packingsGroupon)) {
            cartVM.getNumProduct().set(1L);
            cartVM.calOrder();
        } else {
            cartVM.getAllFromCart();
        }

    }

    private void initView() {
        View v = binding.getRoot();
        //
        progressDialog = new ProgressDialog(getBaseActivity());
        progressDialog.setMessage(getString(R.string.loading));
        //
        Button btnBuyNow = v.findViewById(R.id.btnBuyNow);
        ClearableEditText clearableEditText = v.findViewById(R.id.edCouponCode);
        clearableEditText.setListener(this);
        hideBottomSheet = v.findViewById(R.id.hideBottomSheet);
        LinearLayout bottomSheet = v.findViewById(R.id.bottom_sheet);
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        cartAdapter = new BaseAdapter(R.layout.item_cart, cartVM, this::onClick, getBaseActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cartAdapter);
        //
//        animationBox(v);
        //
        hideBottomSheet.setOnClickListener(v1 -> {
            Drawable icon;
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
                icon = getBaseActivity().getResources().getDrawable(R.drawable.ic_collapse_small_holo_light);
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            } else {
                icon = getBaseActivity().getResources().getDrawable(R.drawable.ic_expand_small_holo_light);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
            hideBottomSheet.setCompoundDrawablesWithIntrinsicBounds(null, null, null, icon);
        });
        btnBuyNow.setOnClickListener(view -> {
            if (registerGrouponId != null) {
                Objects.requireNonNull(cartVM.getOrderProductCartDTO().get()).setRegisterGrouponId(registerGrouponId);
            }
            Intent intent = new Intent(getContext(), CommonActivity.class);
            intent.putExtra("FRAGMENT", OrderConfirmFragment.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("orderProduct", cartVM.getOrderProductCartDTO().get());
            intent.putExtras(bundle);
            startActivity(intent);
        });
        (v.findViewById(R.id.lnPromotionProduct)).setOnClickListener(view -> {
            if (TsUtils.isNotNull(cartVM.getIncentivePackings())) {
                FragmentManager fm = getChildFragmentManager();
                IncentivePackingDialogFragment dialogFragment = new IncentivePackingDialogFragment(cartVM.getIncentivePackings());
                dialogFragment.show(fm, "fragment_incentive_packing");
            }
        });
        (v.findViewById(R.id.lnPromotionApllies)).setOnClickListener(view -> {
            if (cartVM.getOrderProductCartDTO() != null && TsUtils.isNotNull(Objects.requireNonNull(cartVM.getOrderProductCartDTO().get()).getMapIncentives())) {
                showProgress();
                cartVM.getIncentiveFromId(cartVM.getOrderProductCartDTO().get().getMapIncentives());
            }
        });
    }

    private void showProgress() {
        if (!getBaseActivity().isFinishing() && !progressDialog.isShowing()) {
            try {
                progressDialog.show();
            } catch (WindowManager.BadTokenException e) {
                Log.e("WindowManagerBad ", e.toString());
            }
        }
    }

    private void closeProgress() {
        if (progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (WindowManager.BadTokenException e) {
                Log.e("WindowManagerBad ", e.toString());
            }
        }
    }


    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.continueShopping) {
            Objects.requireNonNull(getActivity()).finish();
        } else if (view.getId() == R.id.templateOrder) {
            Intent i = new Intent(getActivity(), CommonActivity.class);
            i.putExtra("FRAGMENT", TemplateOrderFragment.class);
            i.putExtra("ORDER_TYPE", 1);//Đơn đại lý đặt
            i.putExtra("POSITION", 0);
            if (getActivity() != null) {
                getActivity().startActivity(i);
            }
        }
    }

    private void onClick(View view, BaseModel baseModel) {
        if (view.getId() == R.id.imgDeleteProduct) {
            showAlertDialog(R.string.CONFIRM_DELETE, this, baseModel);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_cart;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CartVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcCart;
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "updateListCart":
                //Calculator order
                if (TsUtils.isNotNull(viewModel.getBaseModelsE())) {
                    cartVM.calOrder();
                } else {
                    cartVM.clearCalOrder();
                    sheetBehavior.setHideable(true);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }

                break;
            case "deleteCartSuccess":
                cartVM.getAllFromCart();
                ProductFragmentV2.isChangeProduct = true;
                CategoriesDetailFragment.isChangeProduct = true;
                break;
            case "couponError":
                getBaseActivity().runOnUiThread(() -> Toast.makeText(getBaseActivity(), getString(R.string.coupon_not_correct), Toast.LENGTH_LONG).show());
                break;
            case "packingProductIsNull":
                getBaseActivity().runOnUiThread(() -> Toast.makeText(getBaseActivity(), getString(R.string.product_not_exist), Toast.LENGTH_LONG).show());
                break;
            case "updateCartList":
                cartAdapter.notifyDataSetChanged();
                break;
            case "getIncentiveFromIdSuccess":
                closeProgress();
                if (TsUtils.isNotNull(cartVM.getArrIncentive())) {
                    FragmentManager fm = getChildFragmentManager();
                    PromotionDialogFragment editNameDialogFragment = new PromotionDialogFragment(cartVM.getArrIncentive());
                    editNameDialogFragment.show(fm, "fragment_webview");
                }
                break;
            case "getIncentiveFromIdError":
                closeProgress();
                break;

        }
    }

    @Override
    public void adapterAction(View view, BaseModel baseModel) {
        cartVM.deleteProduct(((Demo) baseModel));
    }

    @Override
    public void didClearText() {
        cartVM.calOrder();
    }

    private void animationBox(View v) {
        ImageView imgBox = v.findViewById(R.id.imgBox);
//        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        rotate.setInterpolator(new LinearInterpolator());
//        rotate.setDuration(2000);
//        rotate.setRepeatCount(Animation.INFINITE);
//        imgBox.startAnimation(rotate);

        TranslateAnimation transAnim = new TranslateAnimation(0, 0, 0, -12);
        transAnim.setDuration(2200);
        transAnim.setRepeatCount(Animation.INFINITE);
        transAnim.setFillAfter(true);
        transAnim.setInterpolator(new BounceInterpolator());
        transAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final int left = imgBox.getLeft();
                final int top = imgBox.getTop();
                final int right = imgBox.getRight();
                final int bottom = imgBox.getBottom() - 12;
                imgBox.layout(left, top, right, bottom);

            }
        });
        imgBox.startAnimation(transAnim);
    }
}