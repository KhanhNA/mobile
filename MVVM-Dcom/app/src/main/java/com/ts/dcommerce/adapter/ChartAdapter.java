package com.ts.dcommerce.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.KpiMonth;

import java.util.ArrayList;
import java.util.List;

public class ChartAdapter extends RecyclerView.Adapter<ChartAdapter.ViewHolder> {
    private Activity activity;
    private List<KpiMonth> datas;

    public ChartAdapter(Activity activity, List<KpiMonth> datas) {
        this.activity = activity;
        this.datas = datas;
    }

    public void update(List<KpiMonth> datas){
        this.datas = datas;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_pie_chart, parent, false);
        return new ChartAdapter.ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        KpiMonth kpiMonth = datas.get(position);
        holder.txtKpiName.setText(kpiMonth.getKpiName());
        setData(holder.pieChart, kpiMonth.getDone(), kpiMonth.getPlan(), kpiMonth.getBonus());
//        setData(holder.pieChart, 70000, 100000d, 0);

    }


    private SpannableString generateCenterSpannableText(SpannableString s) {
        s.setSpan(new RelativeSizeSpan(1.5f), s.toString().indexOf(':') + 1, s.length(), 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.toString().indexOf(':') + 1, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.toString().indexOf(':') + 1, s.length(), 0);
        return s;
    }

    @SuppressLint("DefaultLocale")
    private void setData(PieChart chart, double done, double plan, double bonus) {

        //format pie chart

        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);
        chart.setDragDecelerationFrictionCoef(0.95f);
        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);
        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);
        chart.setDrawCenterText(true);
        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);
        chart.animateY(1500, Easing.EaseInOutQuad);
        chart.setDrawEntryLabels(false);
        chart.setUsePercentValues(true);
        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        //set data for each pie chart
        ArrayList<PieEntry> entries = new ArrayList<>();

        //format center text
        SpannableString s = new SpannableString(activity.getResources().getString(R.string.BONUS) + ": "
                + AppController.formatNumber(bonus));

        chart.setCenterText(generateCenterSpannableText(s));
        entries.add(new PieEntry((float)done,  activity.getResources().getString(R.string.DONE) + ": " + AppController.formatNumber(done)));
        if(done < plan) {
            entries.add(new PieEntry((float) (plan - done), activity.getResources().getString(R.string.IN_PROGRESS) + ": " + AppController.formatNumber((plan - done))));
        }
        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setDrawValues(true);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setValueTextSize(15f);

        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<>();
        for (int c : ColorTemplate.MATERIAL_COLORS)
            colors.add(c);
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        chart.setData(data);
        // undo all highlights
        chart.invalidate();
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtKpiName;
        PieChart pieChart;

        public ViewHolder(View itemView) {
            super(itemView);
            txtKpiName = itemView.findViewById(R.id.txtKpiName);
            pieChart = itemView.findViewById(R.id.pieChart);
        }
    }
}
