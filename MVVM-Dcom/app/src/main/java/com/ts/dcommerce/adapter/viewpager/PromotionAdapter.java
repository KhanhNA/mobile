package com.ts.dcommerce.adapter.viewpager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ts.dcommerce.model.dto.IncentiveDescriptionDTO;
import com.ts.dcommerce.ui.fragment.DetailPromotionFragment;

import java.util.List;

public class PromotionAdapter extends FragmentPagerAdapter {
    private List<IncentiveDescriptionDTO> arrPromotion;

    public PromotionAdapter(FragmentManager fragmentManager, List<IncentiveDescriptionDTO> arr) {
        super(fragmentManager);
        this.arrPromotion = arr;
    }

    @Override
    public int getCount() {
        return arrPromotion != null ? arrPromotion.size() : 0;
    }

    @Override
    public Fragment getItem(int position) {
        return new DetailPromotionFragment(arrPromotion.get(position).getDescription());
    }

}
