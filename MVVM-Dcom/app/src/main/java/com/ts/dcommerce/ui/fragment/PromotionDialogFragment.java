package com.ts.dcommerce.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.TabAdapter;
import com.ts.dcommerce.model.dto.IncentiveDescriptionDTO;

import java.util.List;

@SuppressLint("ValidFragment")
public class PromotionDialogFragment extends DialogFragment {
    private List<IncentiveDescriptionDTO> arrIncentive;

    @SuppressLint("ValidFragment")
    public PromotionDialogFragment(List<IncentiveDescriptionDTO> arr) {
        this.arrIncentive = arr;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promotion, container, false);
        ViewPager vpPromotion = view.findViewById(R.id.vpPromotion);
        TabLayout tabLayout = view.findViewById(R.id.tabs);

        view.findViewById(R.id.btnDismiss).setOnClickListener(view1 -> dismiss());
        TabAdapter promotionAdapter = new TabAdapter(getChildFragmentManager());
        for (IncentiveDescriptionDTO description : arrIncentive) {
            promotionAdapter.addFragment(new DetailPromotionFragment(description.getDescription()), description.getName());
        }
        vpPromotion.setAdapter(promotionAdapter);
        tabLayout.setupWithViewPager(vpPromotion);
        return view;

    }
}
