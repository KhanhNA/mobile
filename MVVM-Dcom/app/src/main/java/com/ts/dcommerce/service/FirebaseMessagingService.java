package com.ts.dcommerce.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.RemoteMessage;
import com.ts.dcommerce.R;
import com.ts.dcommerce.model.dto.NotificationMerchantDTO;

import org.greenrobot.eventbus.EventBus;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    String TAG = FirebaseMessagingService.class.getName();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        String NOTIFICATION_CHANNEL_ID = "dcommerce_notification";
        try {
            // Check if message contains a notification payload.
            final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
            final NotificationMerchantDTO notificationDTO = mapper.convertValue(remoteMessage.getData(), NotificationMerchantDTO.class);
            if (notificationDTO != null) {
                EventBus.getDefault().post(notificationDTO);
                sendNotification(notificationDTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // message, here is where that should be initiated. See sendNotification method below.
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void sendNotification(NotificationMerchantDTO nofiDTO) {
        Intent notificationIntent = new Intent(nofiDTO.getClick_action());
        notificationIntent.putExtra("actionId", nofiDTO.getActionId());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "dcommerce_notification";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            // Configure the notification channel.
            notificationChannel.setDescription(nofiDTO.getMessage());
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launchers)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentTitle(nofiDTO.getTitle())
                .setContentIntent(pendingIntent)
                .setContentText(nofiDTO.getMessage());
        notificationManager.notify(1, notificationBuilder.build());
    }
}
