package com.ts.dcommerce.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ts.dcommerce.model.Category;
import com.ts.dcommerce.ui.fragment.CategoriesDetailFragment;

import java.util.List;

public class CategoriesPapersAdapter extends FragmentPagerAdapter {
    private List<Category> list;

    public CategoriesPapersAdapter(FragmentManager fm, List<Category> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return list.get(position).getTitle();
    }

    @Override
    public int getCount() {
        return list != null ? list.size() : 0;
    }

    @Override
    public Fragment getItem(int position) {
        CategoriesDetailFragment fragment = new CategoriesDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        fragment.setArguments(bundle);
        return fragment;

    }
}
