package com.ts.dcommerce.model.dto;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.MyApplication;
import com.ts.dcommerce.model.PaymentMethod;
import com.ts.dcommerce.model.ShipMethod;
import com.tsolution.base.BaseModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class OrderDTO extends BaseModel implements Serializable {
    public Integer isSaveOrder;
    /**
     *
     */
    public Long orderId;

    /**
     * Mã đơn hàng
     */
    public String orderNo;

    /**
     *
     */
    public Long merchantId;
    public Long merchantTypeId;
    /**
     *
     */
    public String merchantName;
    /**
     *
     */
    public Long paymentMethodId;
    public PaymentMethod paymentMethod;

    /**
     *
     */
    public Long orderInvoiceId;

    /**
     *
     */
    public ShipMethod shipMethod;

    /**
     *
     */
    public Double shipCharge;

    /**
     *
     */
    public Long shpCartId;

    /**
     *
     */
    public Long recvStoreId;

    /**
     *
     */
    public Double amount;

    /**
     *
     */
    public Double vat;

    /**
     *
     */
    public Double incentiveAmount;

    /**
     *
     */
    public Double quantity;

    /**
     *
     */
    public Double recvLat;

    /**
     *
     */
    public Double recvLong;

    /**
     *
     */
    public String recvAddr;
    public Date orderDate;

    /**
     *
     */
    public Integer orderStatus;

    public Double remainQuantity;
    public Double remainAmount;
    public Boolean isSuccess;
    public Integer orderType;
    public Long langId;
    public Long parentMerchantId;
    public Double walletAmount;
    public String merchantCode;
//    public Date orderDate;


    /**
     * Đã thanh toán chưa
     */

    public Integer paymentStatus;

    /**
     * Đơn hàng đã giao cho khách hàng
     */

    public Integer deliveryStatus;

    /**
     * Ngày thanh toán
     */

    public Date paymentDate;

    /**
     * Ngày giao hàng
     */

    public Date deliveryDate;
    public String merchantImgUrl;

    public String packingUrl;
    public Long packingProductId;
    public Long orderPackingId;
    public String productName;
    public Double productPrice;
    public String productQuantity;
    public String reason;
    public List<OrderPackingDTO> orderPackings;
    public HashMap<String, PackingDescriptionDto> packingDescriptions;
    public List<OrderPackingDTO> promotionPacking;
    private Long coin;
    private Long point;
    private String orderName;
    private Long orderTemplateId;
    public void addPackingProduct(OrderPackingDTO packingProduct) {
        if (this.orderPackings == null) {
            this.orderPackings = new ArrayList<>();
        }
        this.orderPackings.add(packingProduct);
    }

    public String getAmountStr() {
        return amount == null ? "" : String.valueOf(amount);
    }

    public String getOrderDateStr() {

        return orderDate == null ? "" : AppController.formatDate.format(orderDate);
    }

}
