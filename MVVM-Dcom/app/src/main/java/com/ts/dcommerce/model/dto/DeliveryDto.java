package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliveryDto extends BaseModel {
    private Long id;
    private String name;
    private Double fee;
    private Date receivedDate;

    public DeliveryDto(Long id,String name, Double fee, Date receivedDate) {
        this.id = id;
        this.name = name;
        this.fee = fee;
        this.receivedDate = receivedDate;
    }
}
