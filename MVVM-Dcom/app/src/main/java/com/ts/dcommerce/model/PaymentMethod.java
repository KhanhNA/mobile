package com.ts.dcommerce.model;

import com.tsolution.base.BaseModel;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentMethod extends BaseModel implements Serializable {

    private Long paymentMethodId;
    private String paymentMethodName;
}
