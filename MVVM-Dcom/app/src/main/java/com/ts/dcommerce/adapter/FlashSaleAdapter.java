package com.ts.dcommerce.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.ui.ProductDetailActivity;
import com.tsolution.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Good_Boy
 */

public class FlashSaleAdapter extends RecyclerView.Adapter<FlashSaleAdapter.ViewHolder> {

    private BaseActivity mContext;
    private List<ProductPackingDto> data;

    public FlashSaleAdapter(BaseActivity context) {
        this.mContext = context;
        data = new ArrayList<>();
    }

    public void updateData(List<ProductPackingDto> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_flashsale_v2, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductPackingDto productPackingDto = data.get(position);
        Glide.with(mContext).load(AppController.BASE_URL + "files" + productPackingDto.getPackingUrl()).into(holder.imgPacking);
        holder.txtPackingName.setText(productPackingDto.getProductName());
        holder.txtPackingPrice.setText(AppController.getInstance().formatCurrency(productPackingDto.getPrice()));
        holder.txtPackingQuantity.setText(" / " + productPackingDto.getQuantity());

    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgPacking;
        private TextView txtPackingName, txtPackingPrice, txtPackingQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            imgPacking = itemView.findViewById(R.id.imgPacking);
            txtPackingName = itemView.findViewById(R.id.txtPackingName);
            txtPackingPrice = itemView.findViewById(R.id.txtPackingPrice);
            txtPackingQuantity = itemView.findViewById(R.id.txtPackingQuantity);
            itemView.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putLong("productId", data.get(getAdapterPosition()).getProductId());
                bundle.putLong("packingProductId", data.get(getAdapterPosition()).getPackingProductId());
                bundle.putBoolean("discountPercent", true);
                Intent intent = new Intent(mContext, ProductDetailActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            });
        }


    }


}