package com.ts.dcommerce.model.dto;


import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CustomErrorResponse {
    private LocalDateTime timestamp;
    private String code;
    private String[] error;

}
