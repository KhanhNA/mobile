package com.ts.dcommerce.model.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class IncentiveDescriptionDTO implements Serializable {
    private String name;
    private String description;
    private Integer incentiveType;
    private Long incentiveProgramId;
}
