package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.*;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.*;
import com.ts.dcommerce.ui.*;
import com.ts.dcommerce.util.*;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

public class MeFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return binding.getRoot();
    }


    @Override
    public void action(View v, BaseViewModel viewModel) {
        Intent i;
//        showProcessing(R.string.wait);
        switch (v.getId()) {
//            case R.id.btnUserInfo:
//                i = new Intent(getActivity(), AccountSettingActivity.class);
//                //thay bằng id user
//                i.putExtra(Constants.MERCHANT_ID, (long)1);
//                getContext().startActivity(i);
//                break;
            case R.id.btnDonMua:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", OrderFragment.class);
                i.putExtra("ORDER_TYPE", 1);//Đơn đại lý đặt
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }

                break;
            case R.id.btnOrderTemplate:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", TemplateOrderFragment.class);
                i.putExtra("ORDER_TYPE", 1);//Đơn đại lý đặt
                i.putExtra("POSITION", 0);
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }

                break;
            case R.id.btnDonMuaCap2:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", OrderLv2Fragment.class);
                i.putExtra("ORDER_TYPE", 2);//Đơn đại lý cấp 2 gửi lên
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }
                break;
            case R.id.btnKPI:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", KpiChartFragment.class);
                i.putExtra(Constants.MERCHANT_ID, AppController.getMerchantId());
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }
                break;
            case R.id.btnVi:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", WalletFragment.class);
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }
                break;
//            case R.id.btnShareFB:
//                break;
            case R.id.btnSetting:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", SettingFragment.class);
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }
                break;
            case R.id.btnChangeMerchant:
                showDialogConfirmChangeMerchant();
                break;
            case R.id.btnGroupon:
                Intent intent = new Intent(getBaseActivity(), GrouponDetailActivity.class);
                startActivity(intent);
                break;
//            case R.id.btnHelp:
//                break;
//            case R.id.btnAgentLv1:
//                i = new Intent(getActivity(), AgentLv1Activity.class);
//                i.putExtra("index", 1);
//                getContext().startActivity(i);
//                break;
        }
//        closeProcess();
    }

    private void showDialogConfirmChangeMerchant() {
        AlertDialog.Builder dialogConfirm = new AlertDialog.Builder(getContext());
        dialogConfirm.setMessage("Đổi đại lý giỏ hàng sẽ bị mất! Bạn muốn thay đổi đại lý ?");
        dialogConfirm.setCancelable(false);
        dialogConfirm.setPositiveButton("OK", (dialog, which) -> {
            showDialogInputCode();
        });
        dialogConfirm.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        dialogConfirm.show();
    }

    private void showDialogInputCode() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View input_code = getLayoutInflater().inflate(R.layout.dialog_input_code_login, null);
        builder.setView(input_code);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", (dialog, which) -> {
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        //overide lại button để hiện thi text loi
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            dialog.dismiss();
            AppController.getInstance().getDaoSession().getContactDBDao().deleteAll();
            AppController.getInstance().getDaoSession().getDemoDao().deleteAll();
            AppController.getInstance().getDaoSession().getDmQuanHuyenDao().deleteAll();
            // Intent loginActivity
            Intent intent = new Intent(getBaseActivity(), LoginActivityV2.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_me;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
