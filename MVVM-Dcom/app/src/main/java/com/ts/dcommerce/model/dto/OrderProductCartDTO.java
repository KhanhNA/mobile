package com.ts.dcommerce.model.dto;

import com.ts.dcommerce.base.AppController;
import com.tsolution.base.BaseModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
public class OrderProductCartDTO extends BaseModel implements Serializable {

    public Long orderId;
    /**
     * Mã đơn hàng
     */
    public String orderNo;
    public Long merchantId;
    public Long merchantTypeId;
    public String merchantName;
    /**
     *
     */
    public Integer isSaveOrder;
    /**
     *
     */
    public Double amount;
    /**
     *
     */
    public Double vat;

    /**
     *
     */
    public Double incentiveAmount;
    /**
     *
     */
    public Double recvLong;
    /**
     *
     */
    public String recvAddr;
    /**
     *
     */
    public Long recvStoreId;
    /**
     *
     */
    public Double recvLat;
    /**
     *
     */
    public Double shipCharge;

    public Long shipMethodId = 1L;
    public MethodDTO shipMethod;
    /**
     *
     */
    public Date orderDate;
    public Long paymentMethodId;

    public Double quantity;
    public Integer orderStatus;

    public Double remainQuantity;
    public Double remainAmount;
    public Double walletAmount;
    public Double tax;
    /**
     * Đã thanh toán chưa
     */

    public Integer paymentStatus;
    /**
     * Đơn hàng đã giao cho khách hàng
     */

    /**
     * QR code để nhận hàng
     */
    public String logisticCode;
    public String logisticQRCode;

    public Integer deliveryStatus;
    public Long orderType;
    public Long merchantAddressId;
    public Long parentMerchantId;
    public List<OrderPackingDTO> orderPackings;
    public CouponDTO coupon = new CouponDTO();
    private Long registerGrouponId;
    private Long coin;
    private Long point;
    private String merchantWareHouseCode;
    private Double incentiveCoupon;
    private boolean incentiveProduct;
    // Danh sach KM order dang duoc ap dung
    private List<Long> mapIncentives;
    // Tong tien hang chua tru khuyen mai va + VAT
    private double orgAmount;
    public String deliveryDate;
    public String description;
    public List<OrderPackingDTO> promotionPacking;
    public String tokenMifos;
    public void addPackingProduct(OrderPackingDTO packingProduct) {
        if (this.orderPackings == null) {
            this.orderPackings = new ArrayList<>();
        }
        this.orderPackings.add(packingProduct);
    }

    public String getOrderDateStr() {
        return orderDate == null ? "" : AppController.formatDate.format(orderDate);
    }

    public String getAmountStr() {
        return AppController.getInstance().formatCurrency(amount);
    }
}
