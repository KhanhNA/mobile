package com.ts.dcommerce.model.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ReceptionAddress  implements Serializable {
    public ReceptionAddress() {
    }

    String name;
    String phone;
    String addres;
}
