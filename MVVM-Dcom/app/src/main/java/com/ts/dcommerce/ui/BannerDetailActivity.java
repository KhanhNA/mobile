package com.ts.dcommerce.ui;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;

import com.ts.dcommerce.R;
import com.ts.dcommerce.model.dto.BannerAlbumDTO;
import com.ts.dcommerce.viewmodel.BannerVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Utils.AlertsUtils;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

public class BannerDetailActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        Toolbar bannerToolbar = findViewById(R.id.bannerToolbar);
        setSupportActionBar(bannerToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        //
        if (getIntent() != null && getIntent().getExtras() != null) {
            BannerAlbumDTO albumDTO = (BannerAlbumDTO) getIntent().getExtras().getSerializable("bannerDetail");
            if (albumDTO != null) {
                HtmlTextView htmlTextView = findViewById(R.id.txtDetail);
                htmlTextView.setHtml(albumDTO.getDescription(),
                        new HtmlHttpImageGetter(htmlTextView));
            }
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_banner_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BannerVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
        AlertsUtils.register(this);
    }

    @Override
    protected void onPause() {
        AlertsUtils.unregister(this);
        super.onPause();
    }
}
