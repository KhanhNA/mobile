package com.ts.dcommerce.model.dto;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MethodDTO extends BaseModel {
    private Long paymentMethodId;
    private String name;
    private String paymentCode;
    private int image;

    @BindingAdapter("imageResource")
    public static void loadImage(ImageView view, int imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).into(view);
    }

    public MethodDTO() {
    }

    public MethodDTO(Long paymentId, String name, String paymentCode, int image) {
        this.name = name;
        this.paymentCode = paymentCode;
        this.image = image;
        this.paymentMethodId = paymentId;
        this.checked = false;
    }
}
