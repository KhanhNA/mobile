package com.ts.dcommerce.adapter;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.ui.fragment.CategoriesFragment;
import com.ts.dcommerce.ui.fragment.MeFragment;
import com.ts.dcommerce.ui.fragment.MerchantFragment;
import com.ts.dcommerce.ui.fragment.ProductFragmentV2;
import com.ts.dcommerce.ui.fragment.NotificationsFragment;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.notification.model.Notification;
import com.ts.notification.service.AdapterClickListener;
import com.ts.notification.ui.NotificationFragment;

import org.greenrobot.greendao.annotation.NotNull;


public class MyPagerAdapter extends FragmentPagerAdapter {
    private long lvType;

    public MyPagerAdapter(FragmentManager fragmentManager, long lv) {
        super(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.lvType = lv;
    }

    @Override
    public int getCount() {
        return lvType == 1 ? 5 : 4;
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        if (lvType == 1) {
            switch (position) {
                case 0:
                    return new ProductFragmentV2();
                case 1:
                    return new CategoriesFragment();
                case 2:
                    return new MerchantFragment();
                case 3:
                    String discussionId = AppController.getCurrentMerchant().getDiscussionId() != null ? AppController.getCurrentMerchant().getDiscussionId() : "";
                    return NotificationFragment.newInstance(discussionId, new AdapterClickListener() {
                        @Override
                        public void onItemClick(Notification notification) {
                            onClickNotification(notification);
                        }
                    });
                case 4:
                    return new MeFragment();
            }
        } else if (lvType == 2) {
            switch (position) {
                case 0:
                    return new ProductFragmentV2();
                case 1:
                    return new CategoriesFragment();
                case 2:
                    return new NotificationsFragment();
                case 3:
                    return new MeFragment();
            }

        }
        return null;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }


    private void onClickNotification(Notification o) {
        if (o.getContent().getPackageName() != null && !"".equals(o.getContent().getPackageName())) {
            if (AppController.getInstance().getPackageName().equalsIgnoreCase(o.getContent().getPackageName())) {
                if (StringUtils.isNotNullAndNotEmpty(o.getContent().getActionClick())) {
                    Intent intent1 = new Intent(o.getContent().getActionClick());
                    intent1.putExtra("actionId", o.getContent().getId());
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    AppController.getInstance().startActivity(intent1);
                }
            } else {
                Intent launchIntent = AppController.getInstance().getPackageManager().getLaunchIntentForPackage(o.getContent().getPackageName());
                if (launchIntent != null) {
                    Bundle bundle = new Bundle();
                    if (!"".equals(o.getContent().getActionClick())) {
                        bundle.putString("actionId", "" + o.getContent().getId());
                        bundle.putString("click_action", o.getContent().getActionClick());
                    }
                    launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    launchIntent.putExtras(bundle);
                    AppController.getInstance().startActivity(launchIntent);
                }
            }
        } else {
            ToastUtils.showToast("No packagename");
        }

    }
}