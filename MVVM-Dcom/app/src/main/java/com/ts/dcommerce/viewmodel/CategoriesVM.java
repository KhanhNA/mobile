package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableLong;
import androidx.annotation.NonNull;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.Category;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.Constants;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoriesVM extends BaseViewModel {
    private RxDao<Demo, Void> noteDao;
    private List<Category> categoryList;
    private ObservableLong numProducts = new ObservableLong();

    public CategoriesVM(@NonNull Application application) {
        super(application);
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        noteDao = daoSession.getDemoDao().rx();
        categoryList = new ArrayList<>();
    }

    public void getCategories(Long id) {
        callApi(HttpHelper.getInstance().getApi().getCategories(id, AppController.languageId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<Category>>() {
                    @Override
                    public void onSuccess(List<Category> categoryServiceResponse) {
                        getCategoriesSuccess(categoryServiceResponse);
                    }

                }));
    }

    private void getCategoriesSuccess(List<Category> data) {
        if (data != null && data.size() > 0) {
            try {
                categoryList = data;
                setData(categoryList);
                view.action("getCategoriesSuccess", null, this, null);
            } catch (AppException e) {
                appException.setValue(e);
            }
        }
    }

    public void getTotalProduct() {
        noteDao.count().subscribe(aLong -> numProducts.set(aLong));
    }

}
