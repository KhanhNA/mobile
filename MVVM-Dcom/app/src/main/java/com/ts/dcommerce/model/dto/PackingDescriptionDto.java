package com.ts.dcommerce.model.dto;

import java.util.Date;

import lombok.Data;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-22 16:05
 */
@Data
public class PackingDescriptionDto {

    /**
     * 
     */

    public Long descriptionId;

    /**
     * 
     */
    public Date dateCreated;

    /**
     * 
     */
    public Date dateModified;

    /**
     * 
     */
    public String updtId;

    /**
     * 
     */
    public String description;

    /**
     * 
     */
    public String packingName;

    /**
     * 
     */
    public String packingTitle;

    /**
     * 
     */
    public String metaDescription;

    /**
     * 
     */
    public String metaKeywords;

    /**
     * 
     */
    public String metaTitle;

    /**
     * 
     */
    public String downloadLnk;

    /**
     * 
     */
    public String productHighlight;

    /**
     * 
     */
    public String sefUrl;

    /**
     * 
     */
    public Integer languageId;

    /**
     * 
     */
    public Long packingProductId;

    /**
     * 
     */
    public String urlImage;
}