
package com.ts.dcommerce.model.dto.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Wallet {

    @SerializedName("accountId")
    @Expose
    private Integer accountId;

    @SerializedName("clientId")
    @Expose
    private Integer clientId;

    @SerializedName("savingsProductId")
    @Expose
    private Integer savingsProductId;

    @SerializedName("savingsProductName")
    @Expose
    private String savingsProductName;

    @SerializedName("accountNo")
    @Expose
    private String accountNo;

    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;

    @SerializedName("currencyName")
    @Expose
    private String currencyName;

    @SerializedName("accountBalance")
    @Expose
    private Double accountBalance;

    @SerializedName("availableBalance")
    @Expose
    private Double availableBalance;


}
