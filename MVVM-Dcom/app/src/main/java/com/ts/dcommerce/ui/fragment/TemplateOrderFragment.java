package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.ChildOrderAdapter;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.viewmodel.OrderVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import lombok.Setter;

@Setter
public class TemplateOrderFragment extends BaseFragment {
    private int currentTab;
    private Snackbar snackbar;
    OrderVM orderVM;
    XRecyclerView rcOrders;
    ChildOrderAdapter adapter;
    private int orderType;
    TextView txtReOrder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreateView(inflater, container, savedInstanceState);
        Intent intent = getBaseActivity().getIntent();
        View v = binding.getRoot();
        orderVM = (OrderVM) viewModel;
        if (intent != null && intent.hasExtra("ORDER_TYPE")) {
            orderType = intent.getIntExtra("ORDER_TYPE", -1);
            orderVM.getTypeList().set(orderType);
            if (intent.hasExtra("POSITION")) {
                currentTab = intent.getIntExtra("POSITION", 0);
            }
        }
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.updating, Snackbar.LENGTH_INDEFINITE);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        assert appCompatActivity != null;
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(getString(R.string.order_template));
        }
        txtReOrder = v.findViewById(R.id.txtReorder);
        rcOrders = v.findViewById(R.id.rcOrdered);
        adapter = new ChildOrderAdapter(R.layout.item_order_template, orderVM, this);
        adapter.setConfigXRecycler(rcOrders, 1);
        rcOrders.setAdapter(adapter);
        rcOrders.getDefaultRefreshHeaderView().setState(2);
        rcOrders.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                adapter.notifyDataSetChanged();
                orderVM.onRefreshTemplateOrder();
            }

            @Override
            public void onLoadMore() {
                orderVM.getOrderMoreTemplate();
            }
        });
        orderVM.init(currentTab);
        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Intent intent = getBaseActivity().getIntent();

        if (intent != null && intent.hasExtra("ORDER_TYPE")) {
            orderType = intent.getIntExtra("ORDER_TYPE", -1);
            ((OrderVM) viewModel).getOrderTemplate();
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "listOrder":
                rcOrders.refreshComplete();
                rcOrders.loadMoreComplete();
                adapter.notifyDataSetChanged();
                break;
            case "noMore":
                rcOrders.setNoMore(true);
                break;
            case "calOrderSuccess":
                closeProcess();
                if (((OrderVM) viewModel).getOrderProductCartDTO() != null) {
                    Intent intent2 = new Intent(getContext(), CommonActivity.class);
                    intent2.putExtra("FRAGMENT", OrderConfirmFragment.class);
                    Bundle bundle2 = new Bundle();
                    bundle2.putSerializable("orderProduct", ((OrderVM) viewModel).getOrderProductCartDTO());
                    intent2.putExtras(bundle2);
                    startActivity(intent2);
                }
                break;
            case "calOrderFail":
                closeProcess();
                break;
            case "updateTemplate":
                snackbar.dismiss();
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_order_chid;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onItemClick(View v, Object o) {
        switch (v.getId()) {
            case R.id.txtReorder:
                showProcessing(R.string.processing);
                OrderDTO orderDTO = (OrderDTO) o;
                if (orderDTO.getOrderId() != null) {
                    orderVM.getOrderDetail(orderDTO.getOrderId());
                }
                break;
            case R.id.llProduct:
            case R.id.llHead:
                Bundle bundle = new Bundle();
                Intent intent = new Intent(getContext(), CommonActivity.class);
                intent.putExtra("FRAGMENT", OrderDetailFragment.class);
                bundle.putSerializable("BASE_MODEL", (Serializable) o);
                intent.putExtras(bundle);
                getBaseActivity().startActivity(intent);
                break;
            case R.id.btnOption:
                showOptionTemplate(v, o);
                break;
        }

    }

    private void showOptionTemplate(View view, Object o) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu_template_order, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(getBaseActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.action_edit:
                    final EditText input = new EditText(getBaseActivity());
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    input.setText(((OrderDTO)o).getOrderName());
                    new AlertDialog.Builder(getBaseActivity())
                            .setTitle(getString(R.string.enter_new_order_name))
                            .setPositiveButton(getString(R.string.ok),  (dialog1, which) -> {
                                ((OrderDTO)o).setOrderName(input.getText().toString());
                                orderVM.editOrderTemplate((OrderDTO) o);
                                snackbar.show();
                            })
                            .setNegativeButton(getString(R.string.cancel),null)
                            .setView(input)
                            .show();
                    break;
                case R.id.action_del:
                    showAlertDialog(R.string.CONFIRM_DELETE, this, (BaseModel) o);
                    break;
            }
            return true;
        });

        menuHelper.show();//showing popup menu
    }


    @Override
    public void adapterAction(View view, BaseModel baseModel) {
        snackbar.show();
        orderVM.deleteOrderTemplate((OrderDTO) baseModel);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
