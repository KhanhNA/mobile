package com.ts.dcommerce.model.dto;


import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder

public class ProductDto extends BaseModel {

    @SerializedName("id")
    private Long productId;
    private Long manufacturerId;
    private String productName;

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(AppController.BASE_URL + "files" + imageUrl).into(view);
    }

    private String urlImage;

    private Float reviewAvg;

    private Integer reviewCount;

    private Double price;

    private Integer quantity;

    private Integer typePromotion;
    private Double orgPrice;
    private Double marketPrice;
    private Double discountPercent;
    private Long packingProductId;
    //quantity ordered
    private Integer quantityOrdered;
    private List<ProductPackingDto> packingProductList;
    private String packingProductCode;

}
