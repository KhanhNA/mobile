package com.ts.dcommerce.model;

public enum NotificationAction {
    ORDER(1), GROUPON(2), INVITE(3);
    private int value;

    NotificationAction(int type) {
        this.value = type;
    }

    public int getValue() {
        return value;
    }
}
