package com.ts.dcommerce.ui;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProviders;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.BR;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.databinding.ActivitySubProductBinding;
import com.ts.dcommerce.model.ProductDetail;
import com.ts.dcommerce.model.dto.ProductDto;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.ui.fragment.CartFragment;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.ProductDetailVM;
import com.ts.dcommerce.viewmodel.SearchVM;
import com.ts.dcommerce.widget.BottomSheetAddToCart;
import com.ts.dcommerce.widget.CircleAnimationUtil;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.Utils.AlertsUtils;

import java.util.List;
import java.util.Objects;


public class SubProductActivity extends BaseActivity<ActivitySubProductBinding> {
    private XRecyclerView rcSearch;
    private BaseAdapterV2 searchAdapter;
    private SearchVM searchVM;
    private String keyWord = "";
    private LinearLayout layoutEmpty;
    private BottomSheetAddToCart sheetBehavior;
    private ProductDetailVM productDetailVM;
    private ProductDto productDto;
    private String preKey = "";
    private FrameLayout frCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchVM = (SearchVM) viewModel;
        productDetailVM = ViewModelProviders.of(this).get(ProductDetailVM.class);
        binding.setVariable(BR.productDetail, productDetailVM);
        productDetailVM.setView(this::processFromVM);
        initView();
        searchVM = (SearchVM) viewModel;

        if (getIntent() != null && getIntent().hasExtra("keyword")) {
            searchVM.searchByKey(getIntent().getStringExtra("keyword"));
        }

        initBottomSheet();
        searchAdapter = new BaseAdapterV2(R.layout.item_product, searchVM, this);
        searchAdapter.setConfigXRecycler(rcSearch, 2);
        rcSearch.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                //refresh data here
                searchAdapter.notifyDataSetChanged();
                searchVM.onRefresh(keyWord);
            }

            @Override
            public void onLoadMore() {
                // load more data here
                searchVM.getSearchMore(keyWord);
            }
        });
        rcSearch.setAdapter(searchAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        AlertsUtils.register(this);
        productDetailVM.getTotalProduct();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initBottomSheet() {
        sheetBehavior = new BottomSheetAddToCart(getBaseActivity(), productDetailVM, R.id.rcPackingDialog);
        ViewDataBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(getBaseActivity()), R.layout.bottom_sheet_quantity_product, null, false);
        mBinding.setVariable(BR.viewHoler2, productDetailVM);
    }

    private void initView() {
        layoutEmpty = binding.layoutEmpty;
        rcSearch = binding.XRecyclerView;
        frCart = findViewById(R.id.frCart);
        frCart.setOnClickListener(view -> {
            Intent i = new Intent(getBaseActivity(), CommonActivity.class);
            i.putExtra("FRAGMENT", CartFragment.class);
            startActivity(i);
        });
        findViewById(R.id.imgHome).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.btnSearch).setOnClickListener(v -> {
            finish();
        });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_sub_product;
    }


    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return SearchVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "noData":
                rcSearch.refreshComplete();
                searchAdapter.notifyDataSetChanged();
                rcSearch.setVisibility(View.GONE);
                layoutEmpty.setVisibility(View.VISIBLE);
                break;
            case "getProductSuccess":
                rcSearch.setVisibility(View.VISIBLE);
                layoutEmpty.setVisibility(View.GONE);
                searchAdapter.notifyDataSetChanged();
                rcSearch.refreshComplete();
                hideKeyBoard();
                break;
            case "addToCart":
                makeFlyAnimation(view, frCart);
                Toast.makeText(getBaseActivity(), getString(R.string.ADD_CART), Toast.LENGTH_SHORT).show();
                sheetBehavior.dismiss();
                break;
            case "noMore":
                rcSearch.setNoMore(true);
                break;
        }
    }

    private void makeFlyAnimation(View fromView, View targetView) {

        new CircleAnimationUtil().attachActivity(SubProductActivity.this).setTargetView(fromView).setMoveDuration(500).setDestView(targetView).setAnimationListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                productDetailVM.getTotalProduct();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_cart) {
            Intent i = new Intent(getBaseActivity(), CommonActivity.class);
            i.putExtra("FRAGMENT", CartFragment.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View v, Object o) {
        switch (v.getId()) {
            case R.id.frCart:
                productDto = ProductDto.builder().build();
                productDto = ((ProductDto) o);
                productDetailVM.getQuantity().set(1);

                try {
                    // set object Product --> ProductDetail
                    ProductDetail productDetail = TsUtils.getData(productDto, ProductDetail.class, null);
                    productDetailVM.getProductDetail().set(productDetail);
                    productDetailVM.arrPackingProduct.setData(productDetail.getPackingProductList());

                    //auto select packing
                    int index = 0;
                    List<ProductPackingDto> productPackingDtos = productDetail.getPackingProductList();
                    for (int i = 0; i < productPackingDtos.size(); i++) {
                        if (productDetail.getQuantity().equals(productPackingDtos.get(i).getQuantity())) {
                            index = i + 1;
                            break;
                        }
                    }
                    if (!sheetBehavior.isAdded()) {
                        sheetBehavior.show(Objects.requireNonNull(getSupportFragmentManager()), sheetBehavior.getTag());
                        productPackingDtos.get(index - 1).index = index;
                        sheetBehavior.onPackingClick(null, productPackingDtos.get(index - 1));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.imgProduct:
            case R.id.txtNameProduct:
            case R.id.txtPrice:
                Bundle bundle = new Bundle();
                bundle.putLong("productId", ((ProductDto) o).getProductId());
                bundle.putLong("packingProductId", ((ProductDto) o).getPackingProductId());
                Intent intent = new Intent(SubProductActivity.this, ProductDetailActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.itemGroupon:
                Bundle bundle1 = new Bundle();
                bundle1.putLong("productId", ((ProductPackingDto) o).getProductId());
                bundle1.putLong("packingProductId", ((ProductPackingDto) o).getPackingProductId());
                bundle1.putBoolean("isGroupon", true);
                Intent intent1 = new Intent(getBaseActivity(), ProductDetailActivity.class);
                intent1.putExtras(bundle1);
                startActivity(intent1);
                break;
        }
    }

    @Override
    protected void onPause() {
        AlertsUtils.unregister(this);
        super.onPause();
    }
}
