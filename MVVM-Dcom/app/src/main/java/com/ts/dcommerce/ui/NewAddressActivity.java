package com.ts.dcommerce.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.ts.dcommerce.R;
import com.ts.dcommerce.databinding.DialogNewAddressBinding;
import com.ts.dcommerce.model.db.Administrative;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.ui.fragment.AdministrativeFragment;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.NewAddressVM;
import com.ts.dcommerce.viewmodel.OrderConfirmVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.Utils.AlertsUtils;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProviders;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class NewAddressActivity extends BaseActivity<DialogNewAddressBinding> {
    private int REQUEST_PROVINCE_CODE = 111;
    private int REQUEST_DISTRICT_CODE = 222;
    private int REQUEST_VILLAGE_CODE = 333;
    private int REQUEST_ACCESS_FINE_LOCATION = 444;
    OrderConfirmVM orderConfirmVM;
    NewAddressVM newAddressVM;
    Toolbar toolbar;
    Switch swAddress;

    Integer provinceId, districtId, villageId;

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_new_address;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbar = findViewById(R.id.toolbar);
        swAddress = findViewById(R.id.swAddress);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.new_address);
        }
        orderConfirmVM = ViewModelProviders.of(getBaseActivity()).get(OrderConfirmVM.class);
        newAddressVM = (NewAddressVM) viewModel;
        Intent intent = getIntent();
        MerchantAddressDto merchantAddress = (MerchantAddressDto) intent.getSerializableExtra("MERCHANT_ADDRESS");
        if(merchantAddress != null){
            newAddressVM.getModel().set(merchantAddress);
        }
    }


    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        Intent i = new Intent(getBaseActivity(), CommonActivity.class);
        i.putExtra("FRAGMENT", AdministrativeFragment.class);
        switch (view.getId()){
            case R.id.txtProvince:
                i.putExtra("ADMINISTRATIVE_ID", -1);
                i.putExtra("ADMINISTRATIVE_ACTION", 1);
                startActivityForResult(i, REQUEST_PROVINCE_CODE);
                break;
            case R.id.txtDistrict:
                if(provinceId != null){
                    i.putExtra("ADMINISTRATIVE_ID", provinceId);
                    i.putExtra("ADMINISTRATIVE_ACTION", 2);
                    startActivityForResult(i, REQUEST_DISTRICT_CODE);
                }else {
                    if(StringUtils.isNullOrEmpty(binding.txtDistrict.getText().toString())) {
                        ToastUtils.showToast(R.string.requite_province);
                    }else {
                        i.putExtra("ADMINISTRATIVE_NAME", binding.txtDistrict.getText().toString());
                        startActivityForResult(i, REQUEST_DISTRICT_CODE);
                    }
                }
                break;
            case R.id.txtVillage:
                if(districtId != null){
                    i.putExtra("ADMINISTRATIVE_ID", districtId);
                    i.putExtra("ADMINISTRATIVE_ACTION", 3);
                    startActivityForResult(i, REQUEST_VILLAGE_CODE);
                }else {
                    if(StringUtils.isNullOrEmpty(binding.txtDistrict.getText().toString())) {
                        ToastUtils.showToast(R.string.requite_district);
                    }else {
                        i.putExtra("ADMINISTRATIVE_NAME", binding.txtDistrict.getText().toString());
                        startActivityForResult(i, REQUEST_DISTRICT_CODE);
                    }

                }

            case R.id.btnGps:
                getCurrentLocation();
                requestPermission();
                turnGPSOn();
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if(data != null){
                Administrative administrative = (Administrative) data.getSerializableExtra("ADMINISTRATIVE");
                if(requestCode == REQUEST_PROVINCE_CODE){
                    provinceId = administrative.getId();
                    binding.txtProvince.setText(administrative.getName());
                }else if(requestCode == REQUEST_DISTRICT_CODE){
                    districtId = administrative.getId();
                    binding.txtDistrict.setText(administrative.getName());
                }else if(requestCode == REQUEST_VILLAGE_CODE){
                    villageId = administrative.getId();
                    binding.txtVillage.setText(administrative.getName());
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AlertsUtils.register(this);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            } else {
                Toast.makeText(NewAddressActivity.this, getString(R.string.permissions_not_granted), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getCurrentLocation() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {
            FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
            client.getLastLocation().addOnSuccessListener(location -> {
                if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Geocoder gcd = new Geocoder(this, Locale.getDefault());
                try {
                    if(location!=null){
                        List<Address> addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                        if (!addresses.isEmpty()) {
                            binding.txtAddress.setText(addresses.get(0).getAddressLine(0));
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if ("saveAddressSuccess".equals(action)) {
            Toast.makeText(getBaseActivity(), getString(R.string.insert_success), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.putExtra("MERCHANT_ADDRESS", newAddressVM.getModelE());
            setResult(Activity.RESULT_OK, intent);
            finish();
        }else if("saveAddressFalse".equals(action)){
            Toast.makeText(getBaseActivity(), getString(R.string.insert_false), Toast.LENGTH_SHORT).show();
        }
    }

    private void turnGPSOn() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (!provider.contains("gps")) { //if gps is disabled
            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }


    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NewAddressVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", true);
        setResult(Activity.RESULT_OK, returnIntent);
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        AlertsUtils.unregister(this);
        super.onPause();
    }
}
