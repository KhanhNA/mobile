package com.ts.dcommerce.adapter;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ns.chat.views.activity.FriendPostsFragment;
import com.ns.chat.views.activity.profile.ProfileFragment;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.ui.fragment.ContactFragment;
import com.ts.dcommerce.ui.fragment.MerchantChildFragment;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MerchantPaperAdapter extends FragmentPagerAdapter {
    private String[] TITLES;
    private MerchantChildFragment merchantChildFragment;

    public MerchantPaperAdapter(FragmentManager fm, String[] title) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        AppController.getCurrentMerchant();
        switch (position) {

            case 0: //contact
                return new ContactFragment();
            case 1: //m2
                merchantChildFragment = new MerchantChildFragment();
                return merchantChildFragment;
            case 2: //new feed
                fragment = FriendPostsFragment.getInstance();
                Bundle bundle = new Bundle();
                bundle.putString(ProfileFragment.USER_ID_EXTRA_KEY, AppController.getInstance().current_user_firebase_id);
                fragment.setArguments(bundle);
                return fragment;

            default:
                return null;

        }

    }


}