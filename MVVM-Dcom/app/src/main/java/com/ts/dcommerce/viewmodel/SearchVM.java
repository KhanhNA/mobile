package com.ts.dcommerce.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.dto.ProductDto;
import com.ts.dcommerce.model.dto.SuggestionSearchDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;
import java.util.List;

public class SearchVM extends BaseViewModel {
    private int totalPage = 0;
    private int currentPage = 0;
    public BaseViewModel arrSuggestion;

    public SearchVM(@NonNull Application application) {
        super(application);
        arrSuggestion = new BaseViewModel(application);
        arrSuggestion.baseModels.set(new ArrayList<>());
    }

    public void searchByKey(String key) {
        callApi(HttpHelper.getInstance().getApi().searchProduct(key, 0, AppController.languageId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<ProductDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<ProductDto> response) {
                        getProductSuccess(response);
                    }

                }));
    }

    public void getSearchSuggestion(String key) {
        callApi(HttpHelper.getInstance().getApi().getSearchSuggestion(key)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<SuggestionSearchDTO>>() {
                    @Override
                    public void onSuccess(List<SuggestionSearchDTO> response) {
                        getSuggestionSuccess(response);
                    }

                }));
    }

    private void getSuggestionSuccess(List<SuggestionSearchDTO> response) {
        arrSuggestion.setData(response);
        try {
            view.action("getSuggestionSuccess", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void getSearchMore(String key) {
        if (totalPage > 0 && currentPage < totalPage) {
            currentPage += 1;
            callApi(HttpHelper.getInstance().getApi().searchProduct(key, currentPage, AppController.languageId)
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<ServiceResponse<ProductDto>>() {
                        @Override
                        public void onSuccess(ServiceResponse<ProductDto> productDtoServiceResponse) {
                            getProductMoreSuccess(productDtoServiceResponse);
                        }

                    }));
        } else {
            sendNoMore();
        }
    }

    private void sendNoMore() {
        try {
            view.action("noMore", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    //invoke func
    public void onRefresh(String keyWord) {
        currentPage = 0;
        getBaseModelsE().clear();
        searchByKey(keyWord);
    }

    private void getProductSuccess(ServiceResponse<ProductDto> dto) {
        if (dto != null) {
            List<ProductDto> productDtos = dto.getArrData();
            totalPage = dto.getTotalPages();
            setData(productDtos, true);
            if (TsUtils.isNotNull(dto.getArrData())) {
                sendCallbackSuccess();
            } else {
                sendCallbackFail();
            }
        } else {
            sendCallbackFail();
        }
    }

    private void getProductMoreSuccess(ServiceResponse<ProductDto> dto) {
        if (dto != null && TsUtils.isNotNull(dto.getArrData())) {
            List<ProductDto> productDtos = dto.getArrData();
            totalPage = dto.getTotalPages();
            setData(productDtos);
            sendCallbackSuccess();
        } else {
            sendNoMore();
        }
    }

    private void sendCallbackSuccess() {
        try {
            view.action("getProductSuccess", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    private void sendCallbackFail() {
        try {
            view.action("noData", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }
}
