package com.ts.dcommerce.model.dto;

import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
public class UserPredictionDTO {
    private Long user;
    private Integer num;
    private Long merchantId;
    private int filterType;
    private List<String> lstPrediction;
}
