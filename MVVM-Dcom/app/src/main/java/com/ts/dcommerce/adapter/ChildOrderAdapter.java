package com.ts.dcommerce.adapter;

import androidx.annotation.NonNull;

import com.ts.dcommerce.viewmodel.OrderVM;
import com.tsolution.base.listener.AdapterListener;

public class ChildOrderAdapter extends BaseAdapterV2 {
    private OrderVM viewModel;

    public ChildOrderAdapter(int item, OrderVM vm, AdapterListener listener) {
        super(item, vm, listener);
        this.viewModel = vm;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        viewHolder.bindWithVM(viewModel);
    }
}
