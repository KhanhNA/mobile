package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.viewmodel.MerchantDetailVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderL2Fragment extends BaseFragment {
    MerchantDto merchantDto;
    MerchantDetailVM merchantDetailVM;
    XRecyclerView rcOrders;
    BaseAdapterV2 adapter;
    TextView txtNoData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreateView(inflater, container, savedInstanceState);
        merchantDetailVM = (MerchantDetailVM) viewModel;
        if (merchantDto != null) {
            merchantDetailVM.setMerchant(merchantDto);
            merchantDetailVM.getOrderL2(merchantDto.getMerchantId());
        }
        View view = binding.getRoot();
        initView(view);
        return view;
    }


    private void initView(View v) {
        txtNoData = v.findViewById(R.id.txtNoData);
        rcOrders = v.findViewById(R.id.rcOrdered);
        rcOrders = v.findViewById(R.id.rcOrdered);
        rcOrders.getDefaultRefreshHeaderView().setState(2);
        adapter = new BaseAdapterV2(R.layout.item_order_l2, merchantDetailVM.arrOrder, this);
        adapter.setConfigXRecycler(rcOrders, 1);
        rcOrders.setAdapter(adapter);
        rcOrders.setLoadingMoreEnabled(false);
        rcOrders.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                adapter.notifyDataSetChanged();
                merchantDetailVM.onRefresh(merchantDto.getMerchantId());
            }

            @Override
            public void onLoadMore() {
            }
        });
    }

    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.item_order) {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(getContext(), CommonActivity.class);
//            OrderDTO orderDTO = OrderDTO.builder()
//                    .orderId(((OrderDTO)o).orderId)
//                    .orderType(1)
//                    .langId(Constants.LANG_VI).build();
            intent.putExtra("FRAGMENT", OrderDetailFragment.class);
            bundle.putSerializable("BASE_MODEL", (OrderDTO) o);
            intent.putExtras(bundle);
            if (getContext() != null) {
                getContext().startActivity(intent);
            }
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case "noData":
                if (txtNoData.getVisibility() == View.GONE) {
                    txtNoData.setVisibility(View.VISIBLE);
                }
                rcOrders.refreshComplete();
                rcOrders.loadMoreComplete();

                break;

            case "getOrderL2":
                if (txtNoData.getVisibility() == View.VISIBLE) {
                    txtNoData.setVisibility(View.GONE);
                }
                rcOrders.loadMoreComplete();
                rcOrders.refreshComplete();
                adapter.notifyDataSetChanged();
        }
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_merchant_orderl2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MerchantDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
