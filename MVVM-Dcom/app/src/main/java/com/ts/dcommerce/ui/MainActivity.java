package com.ts.dcommerce.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.MyPagerAdapter;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.NotificationMerchantDTO;
import com.ts.dcommerce.model.dto.SendEventDTO;
import com.ts.dcommerce.model.dto.TokenFbMerchantDTO;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.viewmodel.MerchantVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Utils.AlertsUtils;
import com.tsolution.base.dto.EventDTO;
import com.tsolution.base.dto.NotificationEventDTO;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.item.BaseTabItem;
import me.majiajie.pagerbottomtabstrip.item.NormalItemView;
import me.majiajie.pagerbottomtabstrip.listener.OnTabItemSelectedListener;

public class MainActivity extends BaseActivity {
    private static final int REQUEST_READ_PHONE_STATE = 9999;
    boolean doubleBackToExitPressedOnce = false;
    public static boolean isLoadNotification = false;
    public static boolean isLoadL2 = false;
    private NavigationController navigationController;
    private MerchantVM merchantVM;
    ViewPager viewPager;
    private AlertDialog.Builder customDialog;
    private boolean isLaunched;

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (getIntent() != null) {
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    String actionOpen = extras.getString("click_action", "");
                    String actionId = extras.getString("actionId", "0");
                    if (StringUtils.isNotNullAndNotEmpty(actionOpen)) {
                        isLaunched = true;
                        Intent intent1 = new Intent(actionOpen);
                        intent1.putExtra("actionId", Long.parseLong(actionId));
                        startActivity(intent1);
                    }
                }
            }
            //
            PageNavigationView tab = findViewById(R.id.tab);
            if (AppController.getCurrentMerchant() == null || AppController.getCurrentMerchant().getMerchantTypeId() == null) {
                startActivity(new Intent(MainActivity.this, SplashActivity.class));
                finish();
            }
            Long typeLv = AppController.getCurrentMerchant().getMerchantTypeId();
            viewPager = findViewById(R.id.main_layout);
            merchantVM = new MerchantVM(getApplication());

            navigationView(tab, typeLv);
            getImeiPhone();
        } catch (Exception e) {
            e.printStackTrace();
            startActivity(new Intent(MainActivity.this, SplashActivity.class));
            finish();
        }
    }

    private void navigationView(PageNavigationView tab, Long typeLv) {
        if (typeLv == 1) {
            navigationController = tab.custom()
                    .addItem(newItem(R.drawable.home, R.drawable.home_active, getString(R.string.menu_product)))
                    .addItem(newItem(R.drawable.categories, R.drawable.categories_active, getString(R.string.menu_categories)))
                    .addItem(newItem(R.drawable.contact, R.drawable.contact_active, getString(R.string.list_agent_lv2)))
                    .addItem(newItem(R.drawable.notification, R.drawable.notification_active, getString(R.string.menu_notification)))
                    .addItem(newItem(R.drawable.user, R.drawable.user_active, getString(R.string.MENU_ME)))
                    .build();
            viewPager.setOffscreenPageLimit(5);
        } else if (typeLv == 2) {
            navigationController = tab.custom()
                    .addItem(newItem(R.drawable.home, R.drawable.home_active, getString(R.string.menu_product)))
                    .addItem(newItem(R.drawable.categories, R.drawable.categories_active, getString(R.string.menu_categories)))
                    .addItem(newItem(R.drawable.notification, R.drawable.notification_active, getString(R.string.menu_notification)))
                    .addItem(newItem(R.drawable.user, R.drawable.user_active, getString(R.string.MENU_ME)))
                    .build();
            viewPager.setOffscreenPageLimit(4);
        }

        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), typeLv);
        viewPager.setAdapter(myPagerAdapter);
        navigationController.setupWithViewPager(viewPager);
        navigationController.addTabItemSelectedListener(new OnTabItemSelectedListener() {
            @Override
            public void onSelected(int index, int old) {
                navigationController.setHasMessage(index, false);
            }

            @Override
            public void onRepeat(int index) {

            }
        });
//        navigationController.setMessageNumber(3, 8);
//        navigationController.setHasMessage(0, true);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    @SuppressLint("HardwareIds")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getImeiPhone() {
        if (ActivityCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_READ_PHONE_STATE);

        } else {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            Log.d("imei", ": " + telephonyManager.getDeviceId());
            processFireBase(telephonyManager.getDeviceId());
        }
    }

    private void processFireBase(String imei) {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    // Luu token
                    AppController.getInstance().getEditor().putString("token_fb", token).apply();
                    TokenFbMerchantDTO fbMerchantDTO = new TokenFbMerchantDTO();
                    fbMerchantDTO.setMerchantId(AppController.getCurrentMerchant().getMerchantId());
                    fbMerchantDTO.setToken(token);
                    fbMerchantDTO.setImei(imei);
                    fbMerchantDTO.setIsDelete(false);
                    merchantVM.updateToken(fbMerchantDTO);
                    // Log and toast
                    Log.d("token_firebase", token);

                });
    }

    private BaseTabItem newItem(int drawable, int checkedDrawable, String text) {
        NormalItemView normalItemView = new NormalItemView(this);
        normalItemView.initialize(drawable, checkedDrawable, text);
        normalItemView.setTextDefaultColor(getResources().getColor(R.color.gray2));
        normalItemView.setTextCheckedColor(getResources().getColor(R.color.da_cam));
        return normalItemView;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_main;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            EventBus.getDefault().unregister(this);
            super.onBackPressed();
            return;
        }
        if (viewPager.getCurrentItem() != 0) {
            viewPager.setCurrentItem(0);
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.message_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Subscribe
    public void onEvent(NotificationMerchantDTO merchantDTO) {
        if (merchantDTO != null) {
            isLoadNotification = true;
            if (AppController.getCurrentMerchant().getMerchantTypeId() == 1) {
                getBaseActivity().runOnUiThread(() -> navigationController.setHasMessage(3, true));
            } else if (AppController.getCurrentMerchant().getMerchantTypeId() == 2) {
                getBaseActivity().runOnUiThread(() -> navigationController.setHasMessage(2, true));
            }

        }
    }

    @Subscribe
    public void onEvent(SendEventDTO sendEventDTO) {
        if (sendEventDTO != null && sendEventDTO.getPosition() != null) {
            isLoadL2 = true;
            if (AppController.getCurrentMerchant().getMerchantTypeId() == 1) {
                getBaseActivity().runOnUiThread(() -> viewPager.setCurrentItem(sendEventDTO.getPosition(), true));
            }

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String actionOpen = "";
        long actionId = 0;
        if (intent != null && !isLaunched) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                actionOpen = extras.getString("click_action", "");
                actionId = extras.getLong("actionId", 0);
                if (StringUtils.isNotNullAndNotEmpty(actionOpen)) {
                    Intent intent1 = new Intent(actionOpen);
                    intent1.putExtra("actionId", actionId);
                    startActivity(intent1);
                }
            }
        }
    }

    @Subscribe
    public void onEvent(NotificationEventDTO event) {
        if (event != null) {
//            showCustomDialog(this, "", event.getSenderId() + " Đã đăng ký mua groupon");
            customDialog = new AlertDialog.Builder(getBaseActivity());
            customDialog.setTitle("Đăng ký mua groupon")
                    .setMessage(event.getSenderId() + " đã đăng ký mua groupon" + "\n"
                            + "Đã đặt: " + event.getTotalQuantity() + "/" + event.getTotalQuantityNext() + "\n" +
                            "Hoa hồng: " + event.getIncentiveAmountNext() + "\n" +
                            "Chiết khấu: " + event.getIncentivePercentNext())
                    .setPositiveButton("Xem chi tiết", (dialog, which) -> {
                        Intent intent = new Intent(MainActivity.this, GrouponDetailActivity.class);
                        startActivity(intent);
                    });
            if (!getBaseActivity().isFinishing()) {
                this.getBaseActivity().runOnUiThread(() -> {
                    customDialog.show();
                });
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventDTO mes) {
        // do something
        if (mes != null && mes.getCode() == 401) {
            if (!com.tsolution.base.Utils.AlertsUtils.isShowTimeout) {
                com.tsolution.base.Utils.AlertsUtils.isShowTimeout = true;
                AlertsUtils.displayError(getApplicationContext(), getString(R.string.token_timeout));
            }

        } else if (mes != null && mes.getCode() == 888) {
            Intent intent = new Intent(MainActivity.this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AlertsUtils.register(this);
    }

    @Override
    protected void onPause() {
        AlertsUtils.unregister(this);
        super.onPause();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_READ_PHONE_STATE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getImeiPhone();
            } else {
                processFireBase("");
            }
        }
    }
}
