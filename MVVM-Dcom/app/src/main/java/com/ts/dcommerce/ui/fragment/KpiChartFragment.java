package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.ChartAdapter;
import com.ts.dcommerce.viewmodel.KpiChartVM;
import com.ts.dcommerce.util.DayAxisValueFormatter;
import com.ts.dcommerce.util.MyValueFormatter;
import com.ts.dcommerce.util.XYMarkerView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.ArrayList;

public class KpiChartFragment extends BaseFragment {
    Toolbar toolbar;
    KpiChartVM kpiChartVM;
    ChartAdapter kipAdapter;
    BarChart barChart;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        kpiChartVM = (KpiChartVM) viewModel;
        View v = binding.getRoot();
        initView(v);
        kpiChartVM.getKpi();
        kipAdapter = new ChartAdapter(getBaseActivity(), new ArrayList<>());
        recyclerView.setAdapter(kipAdapter);


        return v;
    }

    private final RectF onValueSelectedRectF = new RectF();
    private void initView(View v) {
        barChart = v.findViewById(R.id.barChart);

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e == null)
                return;

                RectF bounds = onValueSelectedRectF;
                barChart.getBarBounds((BarEntry) e, bounds);
                MPPointF position = barChart.getPosition(e, YAxis.AxisDependency.LEFT);
                MPPointF.recycleInstance(position);

            }

            @Override
            public void onNothingSelected() {

            }
        });

        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);

        barChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);
        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(15f);


        //định dạng giá trị chiều ngang
        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        //định dạng giá trị theo chiều dọc
        ValueFormatter custom = new MyValueFormatter("");
        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)



        XYMarkerView mv = new XYMarkerView(getBaseActivity(), xAxisFormatter);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart


        setData(12, 50002504531d);
        barChart.animateXY(1500, 1500);
    }

    private void setData(int count, double range) {

        float start = 1f;

        ArrayList<BarEntry> values = new ArrayList<>();

        //dump data
        for (int i = (int) start; i < start + count; i++) {
            float val = (float) (Math.random() * (range + 1));

            if (Math.random() * 100 < 25) {
                values.add(new BarEntry(i, val));
            } else {
                values.add(new BarEntry(i, val));
            }
        }

        BarDataSet set1;
        if (barChart.getData() != null &&
                barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();

        } else {
            set1 = new BarDataSet(values, "Doanh số theo tháng năm 2019");
            set1.setDrawIcons(false);

            int startColor = ContextCompat.getColor(getBaseActivity(), android.R.color.holo_blue_dark);
            int endColor = ContextCompat.getColor(getBaseActivity(), android.R.color.holo_blue_bright);
            set1.setGradientColor(startColor, endColor);
            set1.setDrawValues(false);
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);
            barChart.setData(data);
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if(action.equals("getKpiSuccess")){
            kipAdapter.update(kpiChartVM.getArrKpi());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = getBaseActivity().findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        if (getBaseActivity().getSupportActionBar() != null) {
            getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getBaseActivity().getSupportActionBar().setDisplayShowHomeEnabled(true);
            getBaseActivity().getSupportActionBar().setTitle(R.string.KPI_MONTH);
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        Intent i;
        switch (view.getId()) {
            case R.id.btnConfirmed:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", OrderLv2Fragment.class);
                i.putExtra("ORDER_TYPE", 2);
                i.putExtra("TAB_INDEX", 1);
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }
                break;
            case R.id.btnPending:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", OrderLv2Fragment.class);
                i.putExtra("ORDER_TYPE", 2);
                i.putExtra("TAB_INDEX", 0);
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list_order, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            case R.id.action_chat:
                // TODO: 7/24/2019 mở màn hình chat
                Toast.makeText(getContext(), "NTH", Toast.LENGTH_SHORT).show();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_kpi_chart;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return KpiChartVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcKpi;
    }
}
