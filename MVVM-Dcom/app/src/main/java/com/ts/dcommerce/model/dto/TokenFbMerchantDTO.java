package com.ts.dcommerce.model.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenFbMerchantDTO {
    private Long merchantId;
    private String token;
    private String imei;
    private Date createDate;
    private Boolean isDelete;
}
