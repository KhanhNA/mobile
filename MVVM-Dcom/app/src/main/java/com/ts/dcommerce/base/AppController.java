package com.ts.dcommerce.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.ns.chat.application.ChatApplication;
import com.ts.dcommerce.model.db.DaoMaster;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.util.StringUtils;
import com.tsolution.base.RetrofitClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

public class AppController extends MyApplication {
    private static AppController mInstance;

    public static String LINK_DOWN_APK = "http://dev.nextsolutions.com.vn/download/app.apk";

    public static String DOMAIN_NAME = "http://dev.nextsolutions.com.vn";
    public static String DOMAIN_NAME_MR_BIEN = "http://192.168.1.16";
    public static String DOMAIN_NAME_HTTPS = "http://dev.nextsolutions.com.vn";
    public static final String BASE_URL = DOMAIN_NAME + ":8234/api/v1/";
    //        public static final String BASE_URL = "http://192.168.1.12:8234/api/v1/";
    public static final String BASE_LOGIN_URL = DOMAIN_NAME + ":9999";
    public static final String BASE_LOGISTIC_URL = DOMAIN_NAME + ":8888";
    public static final String BASE_PAYMENT = DOMAIN_NAME_HTTPS + ":41019";

    public static final String MERCHANT = "MERCHANT";

    public static final String CLIENT_ID = "Dcommerce";
    public static final String CLIENT_SECRET = "A31b4c24l3kj35d4AKJQ";
    public static final String APP_INFO = "A31b4c24l3kj35d4AKJQ";
    public static final String DATE_PATTERN = "dd/MM/yyyy";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_PATTERN_GSON = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String BASE_IMAGE = BASE_URL + "files";


    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDate = new SimpleDateFormat(AppController.DATE_PATTERN);

    private SharedPreferences sharedPreferences;
    private DecimalFormat formatter;
    public static Long languageId = 1L;
    HashMap<String, Object> clientCache;
    public static HashMap<String, Object> authens = new HashMap<>();
    private DaoSession daoSession;
    public static String[] LANGUAGE_CODE = {"vi", "en", "my-rMM"};
    private DaoMaster daoMaster;

    public String current_user_firebase_id = "";

    static {
        RetrofitClient.BASE_URL = BASE_URL;
        RetrofitClient.BASE_URL_OAUTH = BASE_LOGIN_URL;
        RetrofitClient.clientId = CLIENT_ID;
        RetrofitClient.clientSecret = CLIENT_SECRET;
        RetrofitClient.DATE_FORMAT = DATE_PATTERN_GSON;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        clientCache = new HashMap<>();
//        new HttpHelper.Builder(AppController.getInstance())
//                .initOkHttp()
//                .createRetrofit(AppController.BASE_URL).createRetrofitLogistic(BASE_LOGISTIC_URL)
//                .build();

        loginPost();
    }

    public void openOtherApplication(String packageName, Activity context) {
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(packageName);
        if (launchIntent != null) {
            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(launchIntent);
        } else {
            // TODO: 1/7/2020 show dialog download chat app
            Toast.makeText(context, "You must download mingalaba chat app", Toast.LENGTH_LONG).show();
        }
    }


    private void loginPost() {
        ChatApplication chatApplication = new ChatApplication(this);
        chatApplication.onCreate();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword("thonv2@gmail.com", "123456").addOnCompleteListener(task -> {

            if (task.isSuccessful()) {
                current_user_firebase_id = mAuth.getCurrentUser().getUid();
            }

        });
    }

    public DaoSession getDaoSession() {
        if (daoSession == null) {
            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "user-db", null);
            daoMaster = new DaoMaster(helper.getWritableDatabase());
            daoSession = daoMaster.newSession();
        }
        return daoSession;
    }

    public DaoMaster getDaoMaster() {
        if (daoMaster == null) {
            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "user-db", null);
            daoMaster = new DaoMaster(helper.getWritableDatabase());
        }
        return daoMaster;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public SharedPreferences getSharePre() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences.edit();
    }

    /**
     * save object vao file
     *
     * @author: PhamBien
     * @return: void
     * @throws:
     */
    public void saveObject(String fileName, Object object) {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), fileName);
            if (!file.exists()) {
                file.mkdirs();
            }
            FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch (Exception e) {
            Log.e("Error: ", "Loi write file: " + e.getMessage());
        }
    }


    /**
     * Kiem tra file ton tai hay khong
     *
     * @author PhamBien
     */
    public static boolean isExistFile(String fileName) {
        try {
            if (!StringUtils.isNullOrEmpty(fileName)) {
                String[] s = AppController.getInstance()
                        .fileList();
                for (String s1 : s) {
                    if (fileName.equals(s1)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            Log.w("File", "" + e.getMessage());
        }
        return false;
    }

    /**
     * Doc file
     *
     * @author: PhamBien
     * @return: void
     * @throws:
     */
    public static Object readObject(String fileName) {
        Object object = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            if (isExistFile(fileName)) {
                fis = AppController.getInstance()
                        .openFileInput(fileName);
                if (fis != null) {// ton tai file
                    ois = new ObjectInputStream(fis);
                    object = ois.readObject();

                }
            }
        } catch (Exception e) {
            Log.e("", "" + e);
            object = null;
            Log.w("", "" + e);
        } finally {
            try {
                if (ois != null) {
                    ois.close();
                }
            } catch (Exception e) {
                Log.e("", "" + e);
            }
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {
                Log.e("", "" + e);
            }
        }
        return object;
    }

    public String formatCurrency(Double money) {
        if (money == null) {
            return "0 đ";
        }
        if (formatter == null) {
            formatter = new DecimalFormat("###,###,###");
        }

        return formatter.format(money) + " đ";
    }

    public String formatCurrencyV2(Double money) {
        if (money == null) {
            return "0 đ";
        }
        if (formatter == null) {
            formatter = new DecimalFormat("###,###,###");
        }

        return formatter.format(money);
    }

    public void putCatche(String key, Object obj) {
        if (clientCache == null) {
            clientCache = new HashMap<>();
        }
        clientCache.put(key, obj);
    }

    public Object getFromCache(String key) {
        if (clientCache == null) {
            return null;
        }
        return clientCache.get(key);
    }

    public static Long getMerchantId() {
        return getCurrentMerchant() != null ? getCurrentMerchant().getMerchantId() : null;
    }

    public static MerchantDto getCurrentMerchant() {
        return (MerchantDto) AppController.getInstance().getFromCache(AppController.MERCHANT);
    }

    @SuppressLint("DefaultLocale")
    public static String formatNumber(double number) {
        if (number >= 1000000000) {
            return String.format("%.1fT", number / 1000000000.0);
        }

        if (number >= 1000000) {
            return String.format("%.1fB", number / 1000000.0);
        }

        if (number >= 100000) {
            return String.format("%.1fM", number / 100000.0);
        }

        if (number >= 1000) {
            return String.format("%.1fK", number / 1000.0);
        }
        return String.valueOf(number);
    }

    public void clearCache() {
        clientCache = new HashMap<>();
    }
}
