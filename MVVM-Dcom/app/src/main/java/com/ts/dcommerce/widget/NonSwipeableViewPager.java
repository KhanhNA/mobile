package com.ts.dcommerce.widget;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.ts.dcommerce.R;

public class NonSwipeableViewPager extends ViewPager {
    private boolean swipeable;
    public NonSwipeableViewPager(Context context) {
        super(context);
    }
    public NonSwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NonSwipeableViewPager);
        try {
            swipeable = a.getBoolean(R.styleable.NonSwipeableViewPager_swipeable, true);
        } finally {
            a.recycle();
        }
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return swipeable && super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return swipeable && super.onTouchEvent(event);
    }

    public void setSwipeable(Boolean disable){
        //When disable = true not work the scroll and when disble = false work the scroll
        this.swipeable = disable;
    }
}