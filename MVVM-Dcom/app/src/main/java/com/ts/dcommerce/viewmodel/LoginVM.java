package com.ts.dcommerce.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.UserInfo;
import com.ts.dcommerce.model.db.Administrative;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.service.event.SingleLiveEvent;
import com.ts.dcommerce.util.ApiResponse;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.IntentConstants;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.RetrofitClient;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;

import java.io.IOException;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Response;

public class LoginVM extends BaseViewModel<UserInfo> {
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    private UserInfo userInfo;
    private SharedPreferences.Editor editor;

    private RxDao<Administrative, Void> noteDao;

    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }

    public UIChangeObservable uc = new UIChangeObservable();


    public class UIChangeObservable {
        public SingleLiveEvent<Boolean> pSwitchEvent = new SingleLiveEvent<>();
    }


    public LoginVM(@NonNull Application application) {
        super(application);
        userInfo = new UserInfo();
        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
        userInfo.setIsSave(sharedPreferences.getBoolean("chkSave", false));
        userInfo.setUserName(sharedPreferences.getString(Constants.USER_NAME, ""));
        userInfo.setPassWord(sharedPreferences.getString(Constants.MK, ""));
        model.set(userInfo);

        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        noteDao = daoSession.getAdministrativeDao().rx();
    }

    public void passwordSwitch() {
        uc.pSwitchEvent.setValue(uc.pSwitchEvent.getValue() == null || !uc.pSwitchEvent.getValue());
    }

    public void requestLogin() {
        if (model.get() != null) {
            UserInfo userInfo = model.get();
            HttpHelper.requestLogin(appException, userInfo.getUserName(), userInfo.getPassWord(), this::loginSuccess);
        }

    }

    public void clearText() {
        Objects.requireNonNull(model.get()).setUserName("");
        model.notifyChange();
    }

    private void loginSuccess(Call call, Response response, Object o, Throwable throwable) {
        if (o != null && !(o instanceof IOException)) {
            Gson gson = new Gson();
            String json = gson.toJson(getModelE());
            SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
            if (!getModelE().getUserName().equals(sharedPreferences.getString(Constants.USER_NAME, ""))) {
                AppController.getInstance().getDaoSession().getContactDBDao().deleteAll();
                AppController.getInstance().getDaoSession().getDemoDao().deleteAll();
                AppController.getInstance().getDaoSession().getDmQuanHuyenDao().deleteAll();
            }
            editor = AppController.getInstance().getEditor();
            editor.putString("UserInfo", json);

            MerchantDto dto = new MerchantDto();
            dto.setUserName(getModelE().getUserName());

            editor.putString(Constants.USER_NAME, getModelE().getUserName());
            if (userInfo.getIsSave() != null && userInfo.getIsSave()) {
                editor.putString(Constants.MK, getModelE().getPassWord());
                editor.putBoolean("chkSave", true);
            } else {
                editor.putBoolean("chkSave", false);
                editor.putString(Constants.MK, "");
            }
            editor.commit();
            getUserInfo();
            if(!AppController.getInstance().getSharePre().getBoolean("isSaveAdministrative", false)){
                getAdministrative();
            }

        } else if (o instanceof IOException) {
            responseLiveData.postValue(ApiResponse.notConnect(new Throwable()));
            RetrofitClient.TOKEN = "";
            HttpHelper.TOKEN_DCOM = "";
        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
            RetrofitClient.TOKEN = "";
            HttpHelper.TOKEN_DCOM = "";
        }

    }

    public void inputCode(String inputCode) {
        callApi(HttpHelper.getInstance().getApi().inputCode(inputCode, "")
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<MerchantDto>() {
                    @Override
                    public void onSuccess(MerchantDto merchantDto) {
                        try {
                            view.action(IntentConstants.INTENT_INPUT_SUCCES, null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        try {
                            view.action(IntentConstants.INTENT_INPUT_FAIL, null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    private void getUserInfo() {
        MerchantDto dto = new MerchantDto();
        dto.setUserName(getModelE().getUserName());
        callApi(HttpHelper.getInstance().getApi().getMerchants(dto, 0, null)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::getUserInfoSuccess, this::handleError));
    }

    private void handleError(Throwable throwable) {
        responseLiveData.postValue(ApiResponse.notConnect(new Throwable()));
        throwable.printStackTrace();
    }

    private void getAdministrative(){
        callApi(HttpHelper.getInstance().getApi().getAdministrative("Administrative")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::getAdministrativeSuccess, this::handleError));
    }

    private void getAdministrativeSuccess(ServiceResponse<Administrative> response) {
        if(response != null && TsUtils.isNotNull(response.getArrData())){
               noteDao.insertOrReplaceInTx(response.getArrData())
                       .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                       .subscribe(demo -> editor.putBoolean("isSaveAdministrative", true).apply());
        }

    }

    private void getUserInfoSuccess(ServiceResponse<MerchantDto> result) {
        if (result != null && TsUtils.isNotNull(result.getArrData())) {
            if (result.getArrData().get(0).getStatus() != 1 || result.getArrData().get(0).getMerchantTypeId() != null && result.getArrData().get(0).getMerchantTypeId() != 1 && result.getArrData().get(0).getMerchantTypeId() != 2) {
                ToastUtils.showToast(R.string.not_permission);
                responseLiveData.postValue(ApiResponse.error(new Throwable()));
            } else if (result.getArrData().get(0).getMerchantTypeId() != null) {
                responseLiveData.postValue(ApiResponse.success(result.getArrData()));
                AppController.getInstance().putCatche(AppController.MERCHANT, result.getArrData().get(0));
            }
        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
        }
    }

    //forget password
    public void sendPassword(){
        // TODO: 21/02/2020 send OTP
        try {
            view.action("sendOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void confirmOTP() {
        // TODO: 21/02/2020 confirm OTP
        try {
            view.action("confirmOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }
}
