package com.ts.dcommerce.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ts.dcommerce.ui.fragment.AddressFragment;
import com.ts.dcommerce.ui.fragment.DeliveryFragment;
import com.ts.dcommerce.ui.fragment.PaymentFragment;

public class ConfirmOrderPagerAdapter extends FragmentPagerAdapter {

    private long typeLV;

    public ConfirmOrderPagerAdapter(FragmentManager fragmentManager, long LV) {
        super(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.typeLV = LV;
    }

    @Override
    public int getCount() {
        return typeLV == 1 ? 2 : 1;
    }

    @Override
    public Fragment getItem(int position) {
        if (typeLV == 1) {
            switch (position) {
//                case 0:
//                    return AddressFragment.getInstance();
                case 0:
                    return DeliveryFragment.getInstance();
                case 1:
                    return PaymentFragment.getInstance();
                default:
                    return null;
            }

        } else {
            if (position == 0) {
                return AddressFragment.getInstance();
            }
            return null;

        }

    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}
