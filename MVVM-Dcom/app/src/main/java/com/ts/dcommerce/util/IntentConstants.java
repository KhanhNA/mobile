package com.ts.dcommerce.util;

public interface IntentConstants {
    public static final String INTENT_START_MAIN = "startMain";
    public static final String INTENT_INPUT_SUCCES = "inputSucess";
    public static final String INTENT_INPUT_FAIL = "inputFail";
    public static final String INTENT_LOGIN_FAIL = "LoginFail";
    public static final String INTENT_INPUT_CODE = "inputCode";
    public static final String INTENT_INTENT_REGISTER_FAIL = "registerFail";
    public static final String INTENT_NOT_INPUT_FULL_NAME = "notInputFullName";
    public static final String INTENT_NOT_INPUT_MOBILE_PHONE = "notInputMobilePhone";
    public static final String INTENT_NOT_INPUT_NAME_LOGIN = "notInputNameLogin";
    public static final String INTENT_NOT_INPUT_PASSWORD = "notInputPassword";
    public static final String INTENT_NOT_INPUT_PASSWORD_SECOND = "notInputPasswordSecond";
    public static final String INTENT_REGISTER_SUCCESS = "registerSuccess";
    public static final String INTENT_PASSWORD_NOT_EQUALS= "passwordNotEqual";


}
