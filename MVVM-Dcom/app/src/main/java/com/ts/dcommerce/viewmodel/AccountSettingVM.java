package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.databinding.ObservableBoolean;
import androidx.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.tsolution.base.BaseViewModel;

import java.io.File;
import java.util.Calendar;

import lombok.Getter;
import lombok.Setter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@Getter
@Setter
public class AccountSettingVM extends BaseViewModel<MerchantDto> {

    ObservableBoolean loadingVisible = new ObservableBoolean();

    public AccountSettingVM(@NonNull Application application) {
        super(application);
    }

    public void init() {
        model.set(AppController.getCurrentMerchant());
    }

    public void setAvatar(String url) {
        MerchantDto merchant = this.model.get();
        if (merchant != null) {
            merchant.setMerchantImgUrl(url);
            model.notifyChange();
        }
    }

    public void setDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        MerchantDto merchant = this.model.get();
        if (merchant != null) {
            merchant.setBirthday(calendar.getTime());
            this.model.notifyChange();
        }
    }

    public void updateMerchant(String filePath) {
        loadingVisible.set(true);
        File file = new File(filePath);
        Gson gson = new Gson();
        model.get().setRegisterDate(null);
        model.get().setActiveDate(null);
        String json = gson.toJson(model.get());
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);

        callApi(HttpHelper.getInstance().getApi().updateMerchant(bodyJson, part)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<MerchantDto>() {
                    @Override
                    public void onSuccess(MerchantDto categoryServiceResponse) {
                        Log.e("upload", "success");
                        loadingVisible.set(false);
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        Log.e("upload", "false");
                        loadingVisible.set(false);
                    }
                }));
    }

}
