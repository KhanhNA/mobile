package com.ts.dcommerce.ui.fragment;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.CategoriesAdapter;
import com.ts.dcommerce.adapter.CategoriesPapersAdapter;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.Category;
import com.ts.dcommerce.ui.SearchActivity;
import com.ts.dcommerce.ui.SubProductActivity;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.CategoriesVM;
import com.ts.dcommerce.widget.IconText;
import com.tsolution.base.BR;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.Objects;

public class CategoriesFragment extends BaseFragment implements DiscreteScrollView.ScrollStateChangeListener<CategoriesAdapter.ViewHolder>,
        DiscreteScrollView.OnItemChangedListener<CategoriesAdapter.ViewHolder> {
    private CategoriesVM categoriesVM;
    private ViewPager vpOrderTab;
    private DiscreteScrollView categoryPicker;
    private CategoriesAdapter categoryAdapter;
    private boolean isLoad = true;
    Intent intent;
    private TextView iconBack;
    @Override
    public void onResume() {
        super.onResume();
        if (isLoad) {
            isLoad = false;
            intent = getBaseActivity().getIntent();
            if (intent.hasExtra("Category")) {
                Category category = (Category) Objects.requireNonNull(intent.getExtras()).getSerializable("Category");
                assert category != null;
                categoriesVM.getCategories(category.getId());
                iconBack.setVisibility(View.VISIBLE);
            }else {
                categoriesVM.getCategories(null);
                iconBack.setVisibility(View.GONE);
            }

        }
        categoriesVM.getTotalProduct();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(CategoriesVM.class);
        viewModel.setView(this::processFromVM);
        categoriesVM = (CategoriesVM) viewModel;
        binding.setVariable(BR.viewModel, viewModel);
        init();


        return binding.getRoot();

    }

    private void init() {
        initSlider(binding.getRoot());
        initView(binding.getRoot());
    }

    private void initSlider(View root) {
        categoryPicker = root.findViewById(R.id.categoryPicker);
        categoryPicker.setSlideOnFling(true);
        categoryAdapter = new CategoriesAdapter(categoriesVM.getCategoryList());
        categoryPicker.setAdapter(categoryAdapter);

        categoryPicker.addOnItemChangedListener(this);
        categoryPicker.addScrollStateChangeListener(this);
        categoryPicker.scrollToPosition(2);
        categoryPicker.setItemTransitionTimeMillis(200);
        categoryPicker.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());
    }

    private void initView(View root) {

        iconBack = root.findViewById(R.id.iconBack);
        iconBack.setOnClickListener(v->getBaseActivity().onBackPressed());

        FrameLayout frCart = root.findViewById(R.id.frCart);
        root.findViewById(R.id.btnChat).setOnClickListener(v-> AppController.getInstance().
                        openOtherApplication(Constants.chatAppId,
                        getActivity()));
        frCart.setOnClickListener(view -> {
            Intent i = new Intent(getBaseActivity(), CommonActivity.class);
            i.putExtra("FRAGMENT", CartFragment.class);
            if (getActivity() != null) {
                getActivity().startActivity(i);
            }
        });
        root.findViewById(R.id.btnSearch).setOnClickListener(view -> {
            startActivity(new Intent(getBaseActivity(), SearchActivity.class));
        });
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if ("getCategoriesSuccess".equalsIgnoreCase(action)) {
            if (TsUtils.isNotNull(viewModel.getBaseModelsE())) {
                initViewPage();
                categoryAdapter.updateData(categoriesVM.getCategoryList());
                int pos = intent.getIntExtra("currentCate", 0);
                categoryPicker.scrollToPosition(pos);
                vpOrderTab.setCurrentItem(pos);
            }

        }

    }

    private void initViewPage() {
        vpOrderTab = binding.getRoot().findViewById(R.id.vpContent);
        CategoriesPapersAdapter myPagerAdapter = new CategoriesPapersAdapter(getChildFragmentManager(), categoriesVM.getCategoryList());
        vpOrderTab.setAdapter(myPagerAdapter);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        vpOrderTab.setPageMargin(pageMargin);
        vpOrderTab.setCurrentItem(0);
        vpOrderTab.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                categoryPicker.smoothScrollToPosition(position);
            }
        });
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_categories;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CategoriesVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onCurrentItemChanged(@Nullable CategoriesAdapter.ViewHolder holder, int position) {
        //viewHolder will never be null, because we never remove items from adapter's list
        if (holder != null) {
            holder.showText();
        }
    }

    @Override
    public void onScrollStart(@NonNull CategoriesAdapter.ViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull CategoriesAdapter.ViewHolder currentItemHolder, int adapterPosition) {
        vpOrderTab.setCurrentItem(adapterPosition);
    }

    @Override
    public void onScroll(
            float position,
            int currentIndex, int newIndex,
            @Nullable CategoriesAdapter.ViewHolder currentHolder,
            @Nullable CategoriesAdapter.ViewHolder newHolder) {

//        if (newIndex >= 0 && newIndex < categoryPicker.getAdapter().getItemCount()) {
//
//        }
    }
}
