package com.ts.dcommerce.viewmodel;

import android.app.Application;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.db.Administrative;
import com.ts.dcommerce.model.db.AdministrativeDao;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.StringUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;
import org.greenrobot.greendao.rx.RxQuery;

import java.util.List;

import androidx.annotation.NonNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewAddressVM extends BaseViewModel<MerchantAddressDto> {

    public NewAddressVM(@NonNull Application application) {
        super(application);
        model.set(new MerchantAddressDto());
    }

    public void saveAddress() {
        MerchantAddressDto merchantAddressDto = model.get();
        if (merchantAddressDto != null) {
            merchantAddressDto.setMerchantId(AppController.getMerchantId());
            if (isValid(merchantAddressDto)) {
                callApi(HttpHelper.getInstance().getApi().saveMerchantAddress(model.get())
                        .compose(RxSchedulers.io_main())
                        .subscribeWith(new RxSubscriber<MerchantAddressDto>() {
                            @Override
                            public void onSuccess(MerchantAddressDto dto) {
                                saveAddressSuccess(dto);
                            }
                        }));
            }
        }
    }

    private boolean isValid(MerchantAddressDto merchantAddressDto) {
        boolean isValid = true;
        if (StringUtils.isNullOrEmpty(merchantAddressDto.getFullName())) {
            isValid = false;
            addError("fullName", R.string.INVALID_FIELD, true);
        } else {
            clearErro("fullName");
        }
        if (StringUtils.isNullOrEmpty(merchantAddressDto.getPhoneNumber())) {
            isValid = false;
            addError("phoneNumber", R.string.INVALID_FIELD, true);
        } else {
            clearErro("phoneNumber");
        }

        if (StringUtils.isNullOrEmpty(merchantAddressDto.getProvince())) {
            isValid = false;
            addError("province", R.string.INVALID_FIELD, true);
        } else {
            clearErro("province");
        }
        if (StringUtils.isNullOrEmpty(merchantAddressDto.getDistrict())) {
            isValid = false;
            addError("district", R.string.INVALID_FIELD, true);
        } else {
            clearErro("district");
        }

        if (StringUtils.isNullOrEmpty(merchantAddressDto.getAddress())) {
            isValid = false;
            addError("address", R.string.INVALID_FIELD, true);
        } else {
            clearErro("address");
        }

        return isValid;
    }


    private void saveAddressSuccess(MerchantAddressDto o) {
        if (o != null) {
            if (o.getId() != null) {
                try {
                    view.action("saveAddressSuccess", null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void changeCheck() {
        MerchantAddressDto merchantAddressDto = model.get();
        if (merchantAddressDto != null) {
            if (merchantAddressDto.getIsSelect() == 1) {
                merchantAddressDto.setIsSelect(0);
            } else {
                merchantAddressDto.setIsSelect(1);
            }

        }

    }

}
