package com.ts.dcommerce.model;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.ts.dcommerce.base.AppController;
import com.tsolution.base.BaseModel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Category extends BaseModel {

    private Long id;
    private String code;
    private String title;
    private String name;
    private Integer sortOrder;
    private String image;
    private String description;
    private Boolean expandable;

    @BindingAdapter("imageUrlCate")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(AppController.BASE_IMAGE + imageUrl).into(view);
    }
}
