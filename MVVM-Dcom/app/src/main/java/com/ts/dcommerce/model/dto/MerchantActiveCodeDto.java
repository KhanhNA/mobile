package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantActiveCodeDto extends BaseModel {
    private String parentMerchantCode;
    private String inviteMobilePhone;
    private Date createDate;
    private String activeCode;
}
