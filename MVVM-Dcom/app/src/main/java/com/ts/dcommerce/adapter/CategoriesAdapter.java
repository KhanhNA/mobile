package com.ts.dcommerce.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.Category;

import java.util.List;

/**
 * @author Good_Boy
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private RecyclerView parentRecycler;
    private List<Category> data;

    public CategoriesAdapter(List<Category> data) {
        this.data = data;
    }

    public void updateData(List<Category> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parentRecycler = recyclerView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_cate, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Category category = data.get(position);
        if (category.getImage() != null) {
//            int index = category.getImage().indexOf("base64,");
//            if (index >= 0) {
//                byte[] decodedString = Base64.decode(category.getImage().substring(index + 7), Base64.DEFAULT);
//                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//                holder.imageView.setImageBitmap(decodedByte);
//            } else {
//
//            }
            Glide.with(holder.itemView.getContext()).load(AppController.BASE_IMAGE + category.getImage()).into(holder.imageView);
        }

        holder.textView.setText(category.getName());
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imgCategory);
            textView = itemView.findViewById(R.id.txtNameCategory);

            imageView.setOnClickListener(this);
        }

        public void showText() {
            int parentHeight = ((View) imageView.getParent()).getHeight();
            float scale = (parentHeight - textView.getHeight()) / (float) imageView.getHeight();
            textView.setSelected(true);
            imageView.setPivotX(imageView.getWidth() * 0.5f);
            imageView.setPivotY(0);
            imageView.animate().scaleX(scale)
                    .withEndAction(() -> textView.setTextColor(imageView.getContext().getResources().getColor(R.color.da_cam)))
                    .scaleY(scale).setDuration(10)
                    .start();
        }

        public void hideText() {
            textView.setSelected(false);
            textView.setTextColor(imageView.getContext().getResources().getColor(R.color.gray2));
            imageView.animate().scaleX(1f).scaleY(1f)
                    .setDuration(50)
                    .start();
        }

        @Override
        public void onClick(View v) {
            parentRecycler.smoothScrollToPosition(getAdapterPosition());
        }
    }


}