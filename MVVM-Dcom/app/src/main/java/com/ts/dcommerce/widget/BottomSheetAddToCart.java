package com.ts.dcommerce.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ts.dcommerce.R;
import com.ts.dcommerce.databinding.BottomSheetQuantityProductBinding;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.ProductDetailVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class BottomSheetAddToCart extends BottomSheetDialogFragment implements View.OnClickListener {
    private Activity activity;
    private BottomSheetQuantityProductBinding binding;
    ProductDetailVM productDetailVM;
    RecyclerView rc;
    int recycleId;
    ImageView hideBottomSheet;
    BaseAdapter adapter;
    private ObservableField<List<ProductPackingDto>> arrPackingGroupon;

    @SuppressLint("ValidFragment")
    public BottomSheetAddToCart(Activity activity, ProductDetailVM productDetailVM, int recycleId) {
        arrPackingGroupon = new ObservableField<>();
        this.activity = activity;
        this.productDetailVM = productDetailVM;
        this.recycleId = recycleId;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_quantity_product, container, false);
        binding.setViewHoler2(productDetailVM);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rc = view.findViewById(recycleId);
        hideBottomSheet = view.findViewById(R.id.hideBottomSheet);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(this.activity, LinearLayoutManager.HORIZONTAL, false);
        rc.setLayoutManager(layoutManager);
        if (productDetailVM.isRegisterCoupon()) {
            if (TsUtils.isNotNull(productDetailVM.arrPackingProduct.getBaseModelsE())) {
                List<ProductPackingDto> temp = new ArrayList<>();
                for (int i = 0; i < productDetailVM.arrPackingProduct.getBaseModelsE().size(); i++) {
                    ProductPackingDto dto = (ProductPackingDto) productDetailVM.arrPackingProduct.getBaseModelsE().get(i);
                    if (dto.checked != null && dto.checked) {
                        temp.add(dto);
                    }
                }
                arrPackingGroupon.set(temp);
                adapter = new BaseAdapter(R.layout.item_packing_product, arrPackingGroupon, this::onPackingClick, (BaseActivity) getContext());
            }
        } else {
            adapter = new BaseAdapter(R.layout.item_packing_product, productDetailVM.arrPackingProduct, this::onPackingClick, (BaseActivity) getContext());
        }
        rc.setAdapter(adapter);
        hideBottomSheet.setOnClickListener(this);
    }


    public void onPackingClick(View view, BaseModel model) {
        productDetailVM.getIndexPacking().set(model.index - 1);
        //set data for productDetail


        if (model.checked == null || !model.checked) {
            if (productDetailVM.getPacking() != null) {
                productDetailVM.getPacking().checked = false;
            }
            model.checked = true;
            productDetailVM.setPacking((ProductPackingDto) model);

        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.hideBottomSheet) {
            this.dismiss();
        }
    }
}
