package com.ts.dcommerce.model.dto;

import com.ts.dcommerce.model.ProductDetail;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductPackingDto extends BaseModel {
    private Long productId;
    private String packingUrl;
    private Long packingProductId;
    private String packingProductCode;
    private Double price;
    private Float discountPercent;
    private Double orgPrice;
    private Double marketPrice;
    private Integer quantity;
    private Integer duration;
    private String productName;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProductDetail){
            ProductDetail item = (ProductDetail)obj;
            return item.getQuantity().equals(this.quantity);
        }

        return false;
    }
}
