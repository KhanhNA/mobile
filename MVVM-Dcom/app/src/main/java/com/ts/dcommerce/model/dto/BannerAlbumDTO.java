package com.ts.dcommerce.model.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BannerAlbumDTO implements Serializable {
    private Long id;
    private String linkTarget;
    private Long languageId;
    private String description;
    private String imageUrl;
}
