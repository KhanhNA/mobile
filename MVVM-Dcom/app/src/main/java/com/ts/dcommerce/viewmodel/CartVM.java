package com.ts.dcommerce.viewmodel;

import android.app.Application;
import android.util.Log;
import android.util.LongSparseArray;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.db.DemoDao;
import com.ts.dcommerce.model.dto.IncentiveDescriptionDTO;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.model.dto.OrderProductCartDTO;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.ui.fragment.CategoriesDetailFragment;
import com.ts.dcommerce.ui.fragment.ProductFragmentV2;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;
import org.greenrobot.greendao.rx.RxQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;
import rx.android.schedulers.AndroidSchedulers;

@Getter
@Setter
public class CartVM extends BaseViewModel {
    ObservableBoolean loadingVisible = new ObservableBoolean(false);
    private RxDao<Demo, Void> noteDao;
    private RxQuery<Demo> cartRxQuery;
    private ObservableField<Long> numProduct = new ObservableField<>();
    private ObservableField<OrderProductCartDTO> orderProductCartDTO = new ObservableField<>();
    private ObservableField<Boolean> edit = new ObservableField<>();
    private ObservableField<Boolean> isEmpty = new ObservableField<>();
    private ObservableField<String> couponCode = new ObservableField<>();
    private String couponErrorMessage = "";
    private List<OrderPackingDTO> incentivePackings;
    private List<IncentiveDescriptionDTO> arrIncentive;

    public CartVM(@NonNull Application application) {
        super(application);
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        cartRxQuery = daoSession.getDemoDao().queryBuilder().rx();
        noteDao = daoSession.getDemoDao().rx();
        incentivePackings = new ArrayList<>();
        arrIncentive = new ArrayList<>();
        edit.set(false);
        isEmpty.set(true);
        couponCode.set("");
    }

    public HashMap<String, Demo> getOrderedProducts() {
        HashMap<String, Demo> hashMap = new HashMap<>();
        List<Demo> temp = noteDao.getDao().queryBuilder().build().list();
        for (Demo demo : temp) {
            hashMap.put(demo.getProductId() + "|" + demo.getQuantity(), demo);
        }
        return hashMap;
    }

    public void getAllFromCart() {
        cartRxQuery.list()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(carts -> {
                    try {
                        if (TsUtils.isNotNull(carts)) {
                            numProduct.set((long) carts.size());
                            setData(carts);
                        } else {
                            setData(new ArrayList<>());
                            numProduct.set(0L);
                        }
                        view.action("updateListCart", null, CartVM.this, null);
                    } catch (AppException e) {
                        appException.setValue(e);
                    }
                });
    }

    private void updateCartToDBLocal() {
        ProductFragmentV2.isChangeProduct = true;
        CategoriesDetailFragment.isChangeProduct = true;
        List<Demo> carts = (List<Demo>) baseModels.get();
        if (carts != null) {
            for (Demo c : carts) {
                noteDao.insertOrReplace(c).observeOn(AndroidSchedulers.mainThread()).subscribe(demo -> {
                });
            }
        }
    }

    public void deleteProduct(Demo demo) {

        noteDao.getDao().queryBuilder().where(DemoDao.Properties.ProductId.eq(demo.getProductId()), DemoDao.Properties.Quantity.eq(demo.getQuantity()))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
        try {
            view.action("deleteCartSuccess", null, this, null);
        } catch (AppException e) {
            appException.setValue(e);
        }

    }

    public void calOrder() {
        loadingVisible.set(true);
        OrderProductCartDTO orderDTO = new OrderProductCartDTO();
        if (AppController.getMerchantId() == null) {
            showMessage(R.string.error_client_not_found, "");
        }
        orderDTO.setMerchantId(AppController.getMerchantId());
        orderDTO.setIsSaveOrder(0);
        if (StringUtils.isNotNullAndNotEmpty(Objects.requireNonNull(couponCode.get()).trim())) {
            orderDTO.coupon.setCode(Objects.requireNonNull(couponCode.get()).trim());
        } else {
            orderDTO.coupon = null;
        }
        List<Long> lstPackingId = new ArrayList<>();
        for (int i = 0; i < getBaseModelsE().size(); i++) {
            Demo demo = (Demo) getBaseModelsE().get(i);
            OrderPackingDTO packingDTO = new OrderPackingDTO();
            packingDTO.setPackingProductCode(demo.getPackingProductCode());
            packingDTO.setQuantity(demo.getOrderQuantity().intValue());
            packingDTO.setPackingProductId(demo.getPackingProductId());
            packingDTO.setPackingUrl(demo.getUrl());
            packingDTO.setPackingName(demo.getProductName());
            orderDTO.addPackingProduct(packingDTO);
            //
            lstPackingId.add(demo.getPackingProductId());
        }
        // Update giá packing hiện tại đang áp dụng
        updatePricePacking(lstPackingId);
        //
        callApiOrder(orderDTO);
    }

    private void callApiOrder(OrderProductCartDTO orderDTO) {
        callApi(HttpHelper.getInstance().getApi().getListProductCartOrder(orderDTO)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<OrderProductCartDTO>() {
                    @Override
                    public void onSuccess(OrderProductCartDTO productCartDTO) {
                        calOrderSuccess(productCartDTO);
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        calOrderFail(msg);
                    }
                }));
    }

    public void getIncentiveFromId(List<Long> ids) {
        callApi(HttpHelper.getInstance().getApi().getIncentiveFromId(ids)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<IncentiveDescriptionDTO>>() {
                    @Override
                    public void onSuccess(List<IncentiveDescriptionDTO> dtos) {
                        getIncentiveSuccess(dtos);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        try {
                            view.action("getIncentiveFromIdError", null, null, null);
                        } catch (AppException ex) {
                            ex.printStackTrace();
                        }
                    }
                }));
    }

    private void calOrderFail(String msg) {
        if (msg.contains("Coupon")) {
            try {
                view.action("couponError", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        } else if (msg.contains("packingProductIsNull")) {
            try {
                view.action("packingProductIsNull", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
        loadingVisible.set(true);
    }

    private void calOrderSuccess(OrderProductCartDTO productCartDTO) {
        try {
            orderProductCartDTO.set(productCartDTO);
            if (StringUtils.isNotNullAndNotEmpty(Objects.requireNonNull(couponCode.get()).trim())) {
                Objects.requireNonNull(orderProductCartDTO.get()).coupon.setCode(Objects.requireNonNull(couponCode.get()).trim());
            } else {
                Objects.requireNonNull(orderProductCartDTO.get()).coupon = null;
            }
            loadingVisible.set(false);
            // Lay san pham khuyen mai kem theo cua don hang
            if (TsUtils.isNotNull(productCartDTO.promotionPacking)) {
                incentivePackings.clear();
                incentivePackings.addAll(productCartDTO.promotionPacking);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void clearCalOrder() {
        orderProductCartDTO.set(null);
    }

    public void addProduct(Demo demo) {
        Double orderQuantity = demo.getOrderQuantity();
        orderQuantity++;
        demo.setOrderQuantity(orderQuantity);
        baseModels.notifyChange();
    }

    public void subProduct(Demo demo) {
        Double orderQuantity = demo.getOrderQuantity();
        orderQuantity--;
        demo.setOrderQuantity(orderQuantity < 0 ? 0 : orderQuantity);
        baseModels.notifyChange();
    }


    public void changeToolBar() {
        Boolean e = edit.get();
        if (e != null) {
            if (e) {
                updateCartToDBLocal();
                if (getBaseModelsE().size() > 0) {
                    calOrder();
                    baseModels.notifyChange();
                }

            }
            edit.set(!e);
        }
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (StringUtils.isNullOrEmpty(s.toString().trim())) {
            isEmpty.set(true);
            if (before > 0) {
                couponCode.set("");
                calOrder();
            }
        } else {
            isEmpty.set(false);
        }
    }

    public void applyCoupon() {
        if (StringUtils.isNotNullAndNotEmpty(Objects.requireNonNull(couponCode.get()).trim())) {
            try {
                calOrder();
                view.action("hideKeyBoard", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }

    private void updatePricePacking(List<Long> lstPackings) {
        callApi(HttpHelper.getInstance().getApi().getPriceForPacking(lstPackings)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<ProductPackingDto>>() {
                    @Override
                    public void onSuccess(List<ProductPackingDto> dtos) {
                        if (TsUtils.isNotNull(dtos)) {
                            getPriceSuccess(dtos);
                        }
                    }
                }));
    }

    private void getPriceSuccess(List<ProductPackingDto> dtos) {
        try {
            String updateQuery = "update " + DemoDao.TABLENAME
                    + " set " + DemoDao.Properties.Price.columnName + "=?"
                    + " where " + DemoDao.Properties.PackingProductId.columnName + "=?";
            for (int i = 0; i < dtos.size(); i++) {
                // Update local DB
                ProductPackingDto dto = dtos.get(i);
                AppController.getInstance().getDaoMaster().getDatabase().execSQL(updateQuery, new Object[]{dto.getPrice(), dto.getPackingProductId()});
                Demo packingDTO = ((Demo) getBaseModelsE().get(i));
                if (packingDTO.getPackingProductId().equals(dto.getPackingProductId())) {
                    ((Demo) getBaseModelsE().get(packingDTO.index - 1)).setPrice(dto.getPrice());
                }
            }
            view.action("updateCartList", null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    private void getIncentiveSuccess(List<IncentiveDescriptionDTO> o) {
        if (o != null) {
            try {
                arrIncentive = o;
                view.action("getIncentiveFromIdSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }

}
