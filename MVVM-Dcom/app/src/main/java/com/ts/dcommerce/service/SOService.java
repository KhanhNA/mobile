package com.ts.dcommerce.service;


import com.ts.dcommerce.base.BaseResponse;
import com.ts.dcommerce.model.Category;
import com.ts.dcommerce.model.ChangingPassword;
import com.ts.dcommerce.model.Count;
import com.ts.dcommerce.model.Language;
import com.ts.dcommerce.model.Manufacturers;
import com.ts.dcommerce.model.ProductDetail;
import com.ts.dcommerce.model.ShipMethod;
import com.ts.dcommerce.model.db.Administrative;
import com.ts.dcommerce.model.dto.BannerAlbumDTO;
import com.ts.dcommerce.model.dto.GrouponRegisterDetailDTO;
import com.ts.dcommerce.model.dto.GrouponRegistryDTO;
import com.ts.dcommerce.model.dto.GrouponRegistryStatusDTO;
import com.ts.dcommerce.model.dto.IncentiveDescriptionDTO;
import com.ts.dcommerce.model.dto.KpiMonth;
import com.ts.dcommerce.model.dto.LoyaltyPointDTO;
import com.ts.dcommerce.model.dto.MerchantActiveCodeDto;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.model.dto.MerchantWareHouseDTO;
import com.ts.dcommerce.model.dto.NotificationMerchantDTO;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.model.dto.OrderDetailDTO;
import com.ts.dcommerce.model.dto.OrderProductCartDTO;
import com.ts.dcommerce.model.dto.OrderTemplateDTO;
import com.ts.dcommerce.model.dto.OverviewL2DTO;
import com.ts.dcommerce.model.dto.ProductCustomDTO;
import com.ts.dcommerce.model.dto.ProductDto;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.model.dto.SuggestionSearchDTO;
import com.ts.dcommerce.model.dto.TokenFbMerchantDTO;
import com.ts.dcommerce.model.dto.UserPredictionDTO;
import com.ts.dcommerce.model.dto.WalletDto;
import com.ts.dcommerce.model.dto.WalletHisDto;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.tsolution.base.exceptionHandle.AndroidCrashLogDto;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SOService {
    //----------------------------Product------------------------
    @GET("products/getPoint")
    Flowable<List<LoyaltyPointDTO>> getPoint(@Query("packingProductId") Long packingProductId);

    @GET("products/getAll")
    Flowable<ServiceResponse<ProductDto>> getProducts(@Query("page") Integer page, @Query("langId") Long langId);

    @POST("products/product-of-merchant")
    Flowable<ProductCustomDTO> getProductsOfMerchant(@Query("page") Integer page, @Body UserPredictionDTO userPredictionDTO);

    @POST("products/prediction")
    Flowable<ProductCustomDTO> getPredictionProduct(@Query("page") Integer page, @Body UserPredictionDTO userPredictionDTO);

    @GET("products/search")
    Flowable<ServiceResponse<ProductDto>> searchProduct(@Query("keyWord") String keyWord, @Query("page") Integer page, @Query("langId") Long langId);

    @GET("products/flashSale")
    Flowable<ServiceResponse<ProductPackingDto>> getFlashSale(@Query("langId") Long langId);

    @GET("products/category")
    Flowable<ServiceResponse<ProductDto>> getProductsCategory(@Query("page") Integer page, @Query("langId") Long langId, @Query("categoryId") Long categoryId);

    @GET("products/{id}")
    Flowable<ProductDetail> getProductById(@Path("id") Long id, @Query("langId") Long langId);

    @GET("products/suggestion")
    Flowable<List<SuggestionSearchDTO>> getSearchSuggestion(@Query("keyWord") String keyWord);

    @GET("products/incentive/{packingProductId}")
    Flowable<ServiceResponse<IncentiveDescriptionDTO>> getIncentivePacking(@Path("packingProductId") Long id);

    @POST("products/getPricePacking")
    Flowable<List<ProductPackingDto>> getPriceForPacking(@Body List<Long> lstPackingId);

    @GET("categories/tree")
    Flowable<List<Category>> getCategories(@Query("id") Long id, @Query("langId") Long langId);

    @GET("manufacturers")
    Flowable<ServiceResponse<Manufacturers>> getManufacturers();

    //---------------------------------ORDERS-------------------------------
    @POST("orders")
    Flowable<ServiceResponse<OrderDTO>> getOrders(@Body OrderDTO order, @Query("page") Integer page);

    @POST("orders/get-template")
    Flowable<ServiceResponse<OrderDTO>> getOrdersTemplate(@Body OrderDTO order, @Query("page") Integer page);

    @GET("orders/del-template")
    Flowable<BaseResponse> deleteOrdersTemplate(@Query("orderTemplateId") Long templateId);

    @GET("orders/edit-template")
    Flowable<OrderDTO> editOrdersTemplate(@Query("orderTemplateId") Long templateId, @Query("name") String name);

    @POST("order/cals")
    Flowable<OrderProductCartDTO> getListProductCartOrder(@Body OrderProductCartDTO order);

    @POST("orders/detail")
    Flowable<ServiceResponse<OrderDetailDTO>> getOrderDetails(@Body OrderDetailDTO order);

    @POST("orders/changeOrder")
    Flowable<ServiceResponse<OrderDetailDTO>> changeOrder(@Body OrderDTO order);

    @POST("orders/changeOrderWithoutNoti")
    Flowable<ServiceResponse<OrderDetailDTO>> changeOrderWithoutNoti(@Body OrderDTO order);

    @POST("orders/transferAmount")
    Flowable<ServiceResponse<OrderDetailDTO>> orderTransferAmount(@Body OrderDTO order);

    @GET("orders/delete")
    Flowable<OrderDTO> deleteOrder(@Query("orderId") Long orderId);

    @POST("orders/save-template")
    Flowable<OrderTemplateDTO> saveTemplate(@Body OrderTemplateDTO orderTemplateDTO);

    //------------------------------
    @POST("crash")
    Flowable<String> appCrash(@Body AndroidCrashLogDto baseDto);

    @POST("common/insert")
    Flowable<String> insert(@Body AndroidCrashLogDto baseDto, @Query("name") String name);

    @POST("common/insert")
    Flowable<String> insert(@Body MerchantDto baseDto, @Query("name") String name);

    @POST("common/list")
    Flowable<ServiceResponse<WalletDto>> getWallet(@Body WalletDto object, @Query("entityName") String entityName, @Query("page") Integer page, @Query("langId") Long langId);

    @GET("get-order-transfer")
    Flowable<List<WalletHisDto>> getWalletHistories(@Query("merchantId") Long merchantId);

    @POST("loadConfig")
    Flowable<HashMap<String, Object>> loadConfig();

    @POST("common/list")
    Flowable<ServiceResponse<Language>> getAllLanguages(@Body Language object, @Query("entityName") String entityName, @Query("page") Integer page, @Query("langId") Integer langId);

    //--------------------------Merchant-------------------------
    @GET("/user/me")
    Maybe<ResponseBody> getUser();

    @POST("merchants/changePass")
    Flowable<ChangingPassword> changePassword(@Body ChangingPassword changingPassword);

    @POST("merchants/update/token")
    Flowable<TokenFbMerchantDTO> updateToken(@Body TokenFbMerchantDTO changingPassword);

    @POST("merchants/update/token")
    Flowable<TokenFbMerchantDTO> deleteToken(@Body TokenFbMerchantDTO fbMerchantDTO);

    @POST("merchants/register")
    Flowable<MerchantDto> registerMerchant(@Body MerchantDto merchant);

    @POST("merchants/deactive")
    Flowable<MerchantDto> deActiveMerchant(@Body MerchantDto merchantDto);

    @POST("merchants/createMerchant")
    Flowable<MerchantDto> createMerchant(@Body MerchantDto merchantDto, @Query("code") String code);

    @GET("merchants/inputCode")
    Flowable<MerchantDto> inputCode(@Query("code") String code, @Query("imei") String imei);

    @POST("merchants")
    Flowable<ServiceResponse<MerchantDto>> getMerchants(@Body MerchantDto merchantDto, @Query("page") Integer pageable, @Query("isPage") Integer isPage);

    @POST("merchants")
    Flowable<ServiceResponse<MerchantDto>> getBaseMerchants(@Body MerchantDto merchantDto, @Query("page") Integer pageable, @Query("isPage") Integer isPage);

    @GET("merchantAddress")
    Flowable<ServiceResponse<MerchantAddressDto>> getMerchantAddress(@Query("merchantId") Long merchantId);

    @POST("newAddress")
    Flowable<MerchantAddressDto> saveMerchantAddress(@Body MerchantAddressDto object);

    @GET("merchants/overview")
    Flowable<OverviewL2DTO> overview(@Query("parentMerchantId") Long parentMerchantId);

    @GET("merchants/overviewL2")
    Flowable<OverviewL2DTO> overviewL2(@Query("merchantId") Long merchantId, @Query("parentMerchantId") Long parentMerchantId);

    @GET("merchants/getOrderL2")
    Flowable<ServiceResponse<OrderDTO>> getOrderL2(@Query("merchantId") Long merchantId, @Query("parentMerchantId") Long parentMerchantId);

    @Multipart
    @POST("merchants/createL1")
    Flowable<MerchantDto> updateMerchant(@Part("body") RequestBody body, @Part MultipartBody.Part files);

    //--------------------------Ship---------------------------------

    @GET("shipmethod/getAll")
    Flowable<List<ShipMethod>> getAllShipMethod();

    @POST("merchants/genActiveCode")
    Flowable<MerchantAddressDto> genActiveCode(@Body MerchantActiveCodeDto dto);

    @POST("merchants/update")
    Flowable<MerchantDto> updateMerchant(@Body MerchantDto dto);

    // Banner
    @GET("banners/getMarketing")
    Flowable<List<BannerAlbumDTO>> getBannerMarketing(@Query("merchantId") Long merchantId);

    //Kpi
    @POST("kpi-month")
    Flowable<List<KpiMonth>> getKpiMonths(@Body KpiMonth object);

    @POST("common/listAll")
    Flowable<ServiceResponse<Administrative>> getAdministrative(@Query("entityName") String entityName);


    //----------------------------Groupon---------------------------------------
    @GET("groupon/registerStatus")
    Flowable<GrouponRegistryStatusDTO> getRegisterStatus(@Query("registerId") Long registerId);

    @GET("groupon/listRegistry")
    Flowable<ServiceResponse<GrouponRegistryDTO>> getListRegistry(@Query("registerId") Long registerId, @Query("page") int page);

    @POST("groupon/register")
    Flowable<GrouponRegisterDetailDTO> grouponRegister(@Body GrouponRegisterDetailDTO grouponRegisterDetailDTO);

    @GET("groupon/listGrouponProgram")
    Flowable<List<GrouponRegisterDetailDTO>> listGrouponProgram(@Query("parentMerchantId") Long parentMerchantId);

    @GET("products/groupon")
    Flowable<ServiceResponse<ProductPackingDto>> getGroupon(@Query("langId") Long langId);

    //---------------------------Notification----------------------------------
    @GET("notification/getList")
    Flowable<ServiceResponse<NotificationMerchantDTO>> getListNotification(@Query("merchantId") Long merchantId, @Query("typeId") Integer typeId, @Query("page") Integer page);

    @GET("notification/count")
    Flowable<Count> getUnreadCount(@Query("merchantId") Long merchantId);

    @POST("notification/update")
    Flowable<NotificationMerchantDTO> updateNotifi(@Body NotificationMerchantDTO notificationMerchant);

    //--------------------------Store--------------------------------------------
    @POST("common/list")
    Flowable<ServiceResponse<MerchantWareHouseDTO>> getStoreOfMerchant(@Body MerchantWareHouseDTO wareHouseDTO, @Query("entityName") String entityName, @Query("page") Integer page);
    //--------------------------Promotion--------------------------------------------

    @POST("incentive/from-id")
    Flowable<List<IncentiveDescriptionDTO>> getIncentiveFromId(@Body List<Long> ids);
}
