package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrouponRegistryStatusDTO  {
    private Long id;
    private double totalAmount;
    private double totalQuantity;
    private double totalAmountNext;
    private long totalQuantityNext;
    private double incentiveAmount;
    private double incentivePercent;
    private double incentiveAmountNext;
    private double incentivePercentNext;
    private long packingProductId;

    private long productId;
    private String productName;
    private double price;
    private String url;
    private int quantity;

}
