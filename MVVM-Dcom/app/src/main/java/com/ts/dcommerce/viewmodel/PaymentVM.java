package com.ts.dcommerce.viewmodel;

import android.app.Application;

import androidx.databinding.ObservableBoolean;
import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;

//import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.model.dto.MethodDTO;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.model.dto.OrderDetailDTO;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.model.dto.OrderProductCartDTO;
import com.ts.dcommerce.model.dto.OrderTemplateDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxQuery;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
import rx.android.schedulers.AndroidSchedulers;

@Getter
@Setter
public class PaymentVM extends BaseViewModel {

    ObservableBoolean isLoading = new ObservableBoolean();
    //    public static PayPalConfiguration config = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(AppController.PAYPAL_CLIENT_ID);
    private OrderConfirmVM orderConfirmVM;
    private OrderProductCartDTO orderInfo;
    private OrderDTO orderDTO;
    ObservableField<Double> totalNotVat;
    ObservableField<Double> totalAmount;
    BaseViewModel paymentMethodVM;
    public MutableLiveData<Boolean> orderSaving = new MutableLiveData<>();
    //truy xuất SQL lite
    DaoSession daoSession = ((AppController) getApplication()).getDaoSession();

    private RxQuery<Demo> cartRxQuery = daoSession.getDemoDao().queryBuilder().rx();

    public PaymentVM(@NonNull Application application) {
        super(application);
        totalNotVat = new ObservableField<>();
        totalAmount = new ObservableField<>();
        totalNotVat.set(0d);
        totalAmount.set(0d);
        paymentMethodVM = new BaseViewModel(application);
        paymentMethodVM.setData(new ArrayList<>());
    }


    public double getTotal() {
        // TODO: 7/26/2019 Tinh toan tong tien thanh toan VND -> USD
        OrderProductCartDTO productCartDTO = orderConfirmVM.getOrderProductCartDTO();
        return productCartDTO == null ? 0 : productCartDTO.getAmount();
    }


    public double getTotalMoneyNotVat() {
        OrderProductCartDTO productCartDTO = orderConfirmVM.getOrderProductCartDTO();
        if (productCartDTO != null) {
            return productCartDTO.getAmount() - productCartDTO.getVat();
        }
        return 0;
    }

    public void initPaymentMethod() {
        ArrayList<MethodDTO> arrPayment = new ArrayList<>();
        arrPayment.add(new MethodDTO(1L, "Ship Cod", Constants.SHIP_COD, R.drawable.ic_shipcod));
        arrPayment.add(new MethodDTO(2L, "mStore Pay", Constants.MSTORE_PAY, R.drawable.banner_app_new));
        paymentMethodVM.setData(arrPayment);
    }


    public void createOrder(boolean isSave, MethodDTO methodDTO, String des, String tokenMifos) {
        isLoading.set(isSave);
        // TODO: 8/1/2019 API tao don hang
        OrderProductCartDTO orderDTO = new OrderProductCartDTO();
        MerchantDto merchantDto = AppController.getCurrentMerchant();
        orderDTO.setMerchantId(merchantDto.getMerchantId());
        orderDTO.setIsSaveOrder(isSave ? 1 : 0);
        // TODO: 8/23/2019 chọn kho nhận hàng
        MethodDTO shipMethod = orderConfirmVM.getOrderProductCartDTO().getShipMethod();
        //&& shipMethod.getPaymentCode().equalsIgnoreCase(Constants.GO_STORE)
        if (shipMethod != null) {
            orderDTO.setMerchantWareHouseCode(orderConfirmVM.getOrderProductCartDTO().getMerchantWareHouseCode());
        }
        //
        orderDTO.setTokenMifos("Basic " + tokenMifos);
        orderDTO.setMerchantName(merchantDto.getUserName());
        orderDTO.setOrderType(1L);
        orderDTO.setDescription(StringUtils.isNotNullAndNotEmpty(des) ? des : "");
        // set địa chỉ
        orderDTO.setRecvLat(orderConfirmVM.getMerchantAddressDto().getLatitude());
        orderDTO.setRecvLong(orderConfirmVM.getMerchantAddressDto().getLongitude());
        orderDTO.setRecvAddr(orderConfirmVM.getMerchantAddressDto().getAddress());
        orderDTO.setMerchantAddressId(orderConfirmVM.getMerchantAddressDto().getId());
        orderDTO.setDeliveryDate(orderConfirmVM.getOrderProductCartDTO().deliveryDate != null ? orderConfirmVM.getOrderProductCartDTO().deliveryDate : "");
        // chọn ship method
        if (orderConfirmVM.getOrderProductCartDTO().getShipMethodId() != null) {
            orderDTO.setShipMethodId(orderConfirmVM.getOrderProductCartDTO().getShipMethodId());
        }
        // Payment Method
        if (methodDTO != null && methodDTO.getPaymentMethodId() != null) {
            orderDTO.setPaymentMethodId(methodDTO.getPaymentMethodId());
        }
        // Set coupon
        if (orderConfirmVM.getOrderProductCartDTO().coupon != null && StringUtils.isNotNullAndNotEmpty(orderConfirmVM.getOrderProductCartDTO().coupon.getCode())) {
            orderDTO.coupon.setCode(orderConfirmVM.getOrderProductCartDTO().coupon.getCode());
        } else {
            orderDTO.coupon = null;
        }
        if (orderConfirmVM.getOrderProductCartDTO().getRegisterGrouponId() != null) {
            // Groupon Order
            orderDTO.orderPackings = orderConfirmVM.getOrderProductCartDTO().orderPackings;
            orderDTO.orderPackings.get(0).setIsDefault(1);
            assert methodDTO != null;
            callOrder(orderDTO, methodDTO.getPaymentCode());
        } else if (TsUtils.isNotNull(orderConfirmVM.arrOrderProduct.getBaseModelsE())) {
            // Template order
            for (int i = 0; i < orderConfirmVM.arrOrderProduct.getBaseModelsE().size(); i++) {
                OrderPackingDTO orderPackingDTO = (OrderPackingDTO) orderConfirmVM.arrOrderProduct.getBaseModelsE().get(i);
                if (i == 0) {
                    orderPackingDTO.setIsDefault(1);
                } else {
                    orderPackingDTO.setIsDefault(0);
                }
                orderDTO.addPackingProduct(orderPackingDTO);
            }
            callOrder(orderDTO, (methodDTO != null && methodDTO.getPaymentCode() != null) ? methodDTO.getPaymentCode() : null);
        } else {
            // Normal order
            cartRxQuery.list()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(carts -> {
                        if (TsUtils.isNotNull(carts)) {
                            for (int i = 0; i < carts.size(); i++) {
                                Demo demo = carts.get(i);
                                OrderPackingDTO packingDTO = new OrderPackingDTO();
                                if (i == 0) {
                                    packingDTO.setIsDefault(1);
                                } else {
                                    packingDTO.setIsDefault(0);
                                }
                                packingDTO.setPackingProductCode(demo.getPackingProductCode());
                                packingDTO.setQuantity(demo.getOrderQuantity().intValue());
                                packingDTO.setPackingProductId(demo.getPackingProductId());
                                packingDTO.setPackingUrl(demo.getUrl());
                                packingDTO.setPackingName(demo.getProductName());
                                orderDTO.addPackingProduct(packingDTO);
                            }
                            callOrder(orderDTO, (methodDTO != null && methodDTO.getPaymentCode() != null) ? methodDTO.getPaymentCode() : null);
                        } else {
                            appException.setValue(new Throwable());
                        }
                    });
        }

    }

    private void callOrder(OrderProductCartDTO orderDTO, String paymentMethodCode) {
        callApi(HttpHelper.getInstance().getApi().getListProductCartOrder(orderDTO)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<OrderProductCartDTO>() {
                    @Override
                    public void onSuccess(OrderProductCartDTO productCartDTO) {
                        if (orderDTO.isSaveOrder == 1) {
                            createOrderSuccess(productCartDTO, paymentMethodCode);
                        } else if (orderDTO.isSaveOrder == 0) {
                            totalNotVat.set(productCartDTO.getAmount() - productCartDTO.getVat());
                            totalAmount.set(productCartDTO.getAmount());
                        }

                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        isLoading.set(false);
                        try {
                            view.action("createOrderFail", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    public void saveTemplate(OrderTemplateDTO templateDTO) {
        callApi(HttpHelper.getInstance().getApi().saveTemplate(templateDTO)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<OrderTemplateDTO>() {
                    @Override
                    public void onSuccess(OrderTemplateDTO dto) {
                        orderSaving.setValue(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        orderSaving.setValue(false);
                    }
                }));
    }

    private void createOrderSuccess(OrderProductCartDTO o, String paymentMethodCode) {
        if (o != null) {
            orderInfo = o;
            isLoading.set(false);
            if (orderInfo.getOrderId() != null) {
                try {
                    orderDTO = OrderDTO.builder()
                            .orderId(orderInfo.orderId)
                            .orderType(1)
                            .merchantId(AppController.getMerchantId())
                            .orderStatus(0)
                            .langId(AppController.languageId).build();
                    deleteAllFromCart();
                    view.action("createOrderSuccess", null, this, null);

                } catch (AppException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("Create order fail");
        }
    }

    public void deleteAllFromCart() {
        // TODO: 8/13/2019 condition to delete product from cart
        daoSession.queryBuilder(Demo.class).buildDelete().executeDeleteWithoutDetachingEntities();
    }

    public void init(OrderConfirmVM orderConfirmVM) {
        this.orderConfirmVM = orderConfirmVM;
        //
        totalNotVat.set(getTotalMoneyNotVat());
        totalAmount.set(getTotal());
    }

    public void orderTransferAmount() {
        if (orderInfo != null) {
            callApi(HttpHelper.getInstance().getApi().orderTransferAmount(OrderDTO.builder().orderId(orderInfo.getOrderId()).orderStatus(0).merchantId(AppController.getMerchantId()).build())
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<ServiceResponse<OrderDetailDTO>>() {
                        @Override
                        public void onSuccess(ServiceResponse<OrderDetailDTO> response) {
                            try {
                                view.action("transferSuccess", null, PaymentVM.this, null);
                            } catch (AppException e) {
                                e.printStackTrace();
                            }
                        }

                    }));
        }
    }

}
