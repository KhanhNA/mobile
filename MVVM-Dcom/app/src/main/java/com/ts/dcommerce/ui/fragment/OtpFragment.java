package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.ts.dcommerce.R;
import com.ts.dcommerce.viewmodel.LoginVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class OtpFragment extends BaseFragment {

    private LoginVM loginVM;
    private TextView txtPhoneNumber, txtTitleOTP;
    private Intent intent;
    private MaterialButton btnConfirmed;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        loginVM = (LoginVM) viewModel;
        assert view != null;
        initView(view);

        return view;
    }

    private void initView(View view) {
        setupToolbar(view);
        findView(view);
        setupView();

    }

    private void setupView() {
        intent = getBaseActivity().getIntent();
        if(intent != null){
            txtPhoneNumber.setText(intent.getStringExtra("PHONE_NUMBER"));
            txtTitleOTP.setText(intent.getStringExtra("TITLE_OTP"));
        }
        btnConfirmed.setOnClickListener(v-> loginVM.confirmOTP());

    }

    private void findView(View view) {
        txtTitleOTP = view.findViewById(R.id.txtTitleOTP);
        txtPhoneNumber = view.findViewById(R.id.txtPhoneNumber);
        btnConfirmed = view.findViewById(R.id.btnConfirmed);
    }

    private void setupToolbar(View v){
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(R.string.enter_phone_number);
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if(action.equals("confirmOTP")){
            openResetPasswordFragment();
        }
    }

    private void openResetPasswordFragment() {
        Intent intent = new Intent(getContext(), CommonActivity.class);
        intent.putExtra("FRAGMENT", ResetPasswordFragment.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_otp;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
