package com.ts.dcommerce.ui.fragment;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.BR;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.adapter.FlashSaleAdapter;
import com.ts.dcommerce.adapter.GrouponAdapter;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.ProductDetail;
import com.ts.dcommerce.model.dto.BannerAlbumDTO;
import com.ts.dcommerce.model.dto.ProductDto;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.ui.BannerDetailActivity;
import com.ts.dcommerce.ui.ProductDetailActivity;
import com.ts.dcommerce.ui.SearchActivity;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.ProductDetailVM;
import com.ts.dcommerce.viewmodel.ProductVM;
import com.ts.dcommerce.widget.BottomSheetAddToCart;
import com.ts.dcommerce.widget.BottomSheetFragmentSortBy;
import com.ts.dcommerce.widget.CircleAnimationUtil;
import com.ts.dcommerce.widget.IconText;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

public class ProductFragmentV2 extends BaseFragment implements BaseSliderView.OnSliderClickListener {
    private XRecyclerView rcProduct;
    private BaseAdapterV2 gridProductAdapter;
    private SliderLayout mDemoSlider;
    private ProductVM productVM;
    private FlashSaleAdapter flashSaleAdapter;
    private GrouponAdapter grouponAdapter;
    private ProductDetailVM productDetailVM;
    private BottomSheetAddToCart sheetAddToCart;
    private BottomSheetFragmentSortBy sheetSortBy;
    private FrameLayout frCart;
    private ProductDto productDto;
    public static Boolean isChangeProduct = false;
    private View headerFlashSale;
    private View headerGroupon;
    private TextView txtDuration;
    private FloatingActionButton fab;
    private int filterType = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        productVM = (ProductVM) viewModel;
        productDetailVM = ViewModelProviders.of(this).get(ProductDetailVM.class);
        binding.setVariable(BR.productDetail, productDetailVM);
        productDetailVM.setView(this::processFromVM);
        View v = binding.getRoot();
        initView(v);
        initProduct(v);
        initBottomSheet();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        productDetailVM.getTotalProduct();
        if (isChangeProduct) {
            productVM.arrProductsVM.setUpdateAdappType(0);
            productVM.setOrderedProduct((List<ProductDto>) productVM.arrProductsVM.baseModels.get());
            isChangeProduct = false;
            gridProductAdapter.notifyDataSetChanged();
        }
    }

    private void initProduct(View v) {
        gridProductAdapter = new BaseAdapterV2(R.layout.item_product, productVM.arrProductsVM, this);
        gridProductAdapter.setConfigXRecycler(rcProduct, 2);

        // Banner
        initBanner(v);
        // Groupon
        initGroupon(v);
        // Flash Sale
        initFlashSale(v);
        // Header Product
        View headerProduct = LayoutInflater.from(getBaseActivity()).inflate(R.layout.hearder_product, v.findViewById(android.R.id.content), false);
        TextView txtFilter = headerProduct.findViewById(R.id.txtFilter);
        txtFilter.setOnClickListener(v1 -> {
            Bundle bundle = new Bundle();
            bundle.putInt("index", filterType);
            sheetSortBy.setArguments(bundle);
            sheetSortBy.show(Objects.requireNonNull(getFragmentManager()), sheetSortBy.getTag());
        });
        rcProduct.addHeaderView(headerProduct);
        rcProduct.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                gridProductAdapter.notifyDataSetChanged();
                productVM.onRefresh();
            }

            @Override
            public void onLoadMore() {
                productVM.getMoreProductsOfMerchant(filterType);
            }
        });
        frCart.setOnClickListener(view -> {
            Intent i = new Intent(getBaseActivity(), CommonActivity.class);
            i.putExtra("FRAGMENT", CartFragment.class);
            if (getActivity() != null) {
                getActivity().startActivity(i);
            }
        });
        rcProduct.setAdapter(gridProductAdapter);
        //
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getBaseActivity()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
        rcProduct.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0 || fab.isShown())
                    fab.hide();
                if (!recyclerView.canScrollVertically(1)) {
                    fab.show();
                }
            }

        });
        fab.setOnClickListener(view -> {
            smoothScroller.setTargetPosition(0);
            Objects.requireNonNull(rcProduct.getLayoutManager()).startSmoothScroll(smoothScroller);
        });
        // init Data
        gridProductAdapter.notifyDataSetChanged();
//        productVM.getProduct();
        productVM.getPredictionProduct();
        productVM.getFlashSale();
        productVM.getGroupon();

    }

    private void initGroupon(View v) {
        headerGroupon = LayoutInflater.from(getBaseActivity()).inflate(R.layout.layout_groupon, v.findViewById(android.R.id.content), false);
        RecyclerView rcGroupon = headerGroupon.findViewById(R.id.rcGroupon);
        GridLayoutManager layoutGroupon =
                new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        rcGroupon.setLayoutManager(layoutGroupon);
        grouponAdapter = new GrouponAdapter(getBaseActivity(), productVM.getArrGroupon(), this);
        rcGroupon.setAdapter(grouponAdapter);
        rcProduct.addHeaderView(headerGroupon);
    }

    private void initBottomSheet() {
        sheetAddToCart = new BottomSheetAddToCart(getBaseActivity(), productDetailVM, R.id.rcPackingDialog);
        sheetSortBy = BottomSheetFragmentSortBy.newInstance(filterType);
        sheetSortBy.setFilterListener(type -> {
            filterType = type;
            productVM.getProductByFilter(type);
        });
    }

    private void initFlashSale(View v) {
        headerFlashSale = LayoutInflater.from(getBaseActivity()).inflate(R.layout.layout_flashsale, v.findViewById(android.R.id.content), false);
        RecyclerView rcFlashSale = headerFlashSale.findViewById(R.id.rcFlashSale);
        txtDuration = headerFlashSale.findViewById(R.id.txtDuration);
        GridLayoutManager layoutFlashSale =
                new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        rcFlashSale.setLayoutManager(layoutFlashSale);
        flashSaleAdapter = new FlashSaleAdapter(getBaseActivity());
        rcFlashSale.setAdapter(flashSaleAdapter);
        rcProduct.addHeaderView(headerFlashSale);
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case "getProductSuccess":
                rcProduct.refreshComplete();
                rcProduct.loadMoreComplete();
                gridProductAdapter.notifyDataSetChanged();
                break;
            case "noMore":
                rcProduct.setNoMore(true);
                break;
            case "getFlashSaleSuccess":
                flashSaleAdapter.updateData(productVM.getArrFlashSale());
                //
                if (TsUtils.isNotNull(productVM.getArrFlashSale())) {
                    int duration = productVM.getArrFlashSale().get(0).getDuration();
                    if (duration != 0) {
                        new CountDownTimer((duration - 1) * 60 * 1000L, 1000) {
                            public void onTick(long millisUntilFinished) {
                                long seconds = millisUntilFinished / 1000;
                                long minutes = seconds / 60;
                                long hours = minutes / 60;
                                long days = hours / 24;
                                String time;
                                if (days > 0) {
                                    time = days + " days " + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
                                } else {
                                    time = hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
                                }
                                txtDuration.setText(time);
                            }

                            public void onFinish() {
                                txtDuration.setText("");
                                rcProduct.removeHeaderView(headerFlashSale);
                            }
                        }.start();
                    }
                }

                break;
            case "getFlashSaleFail":
                rcProduct.removeHeaderView(headerFlashSale);
                break;
            case "getGrouponSuccess":
                grouponAdapter.notifyDataSetChanged();
                break;
            case "getGrouponFail":
//                rcProduct.removeHeaderView(headerGroupon);
                break;
            case "getBannerSuccess":
                HashMap<String, String> url_maps = new HashMap<>();
                if (TsUtils.isNotNull(productVM.getArrBanner())) {
                    for (BannerAlbumDTO bannerAlbumDTO : productVM.getArrBanner()) {
                        url_maps.put("" + bannerAlbumDTO.getId(), AppController.BASE_URL + "files" + bannerAlbumDTO.getImageUrl());
                    }
                }
                sliderCollection(url_maps);
                break;
//            case "noBanner":
//                rcProduct.removeHeaderView(headerBanner);
//                break;
            case "addToCart":
                makeFlyAnimation(view, frCart);
                Toast.makeText(getBaseActivity(), getString(R.string.ADD_CART), Toast.LENGTH_SHORT).show();
                sheetAddToCart.dismiss();
                productVM.setOrderedProduct((List<ProductDto>) productVM.arrProductsVM.baseModels.get(), productDto.index - 1);
                getBaseActivity().runOnUiThread(() -> {
                    rcProduct.notifyItemChanged(productDto.index - 1);
                    productDto = ProductDto.builder().build();
                });

                break;

        }
    }

    private void initView(View v) {
        frCart = v.findViewById(R.id.frCart);
        rcProduct = v.findViewById(R.id.rcAllProduct);
        EditText btnSearch = v.findViewById(R.id.vSearch);
        fab = v.findViewById(R.id.fab);
        IconText btnSms = v.findViewById(R.id.btnSms);
        btnSearch.setOnClickListener(v1 -> startActivity(new Intent(getBaseActivity(), SearchActivity.class)));

        btnSms.setOnClickListener(view -> {
            AppController.getInstance().openOtherApplication(Constants.chatAppId, getBaseActivity());
        });

    }

    private void initBanner(View v) {
        productVM.getBannerMarketing();
        View headerBanner = LayoutInflater.from(getBaseActivity()).inflate(R.layout.layout_banner, v.findViewById(android.R.id.content), false);
        mDemoSlider = headerBanner.findViewById(R.id.slider);
        rcProduct.addHeaderView(headerBanner);
        //

    }

    private void sliderCollection(HashMap<String, String> url_maps) {
        if (mDemoSlider != null) {
            mDemoSlider.removeAllSliders();
        }
        for (String name : url_maps.keySet()) {
            DefaultSliderView textSliderView = new DefaultSliderView(getBaseActivity());
            // initialize a SliderLayout
            textSliderView.description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(2600);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_product;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ProductVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        if (TsUtils.isNotNull(productVM.getArrBanner())) {
            for (BannerAlbumDTO bannerDetail : productVM.getArrBanner()) {
                String id = slider.getBundle().getString("extra");
                if (!StringUtils.isNullOrEmpty(id) && id.equalsIgnoreCase("" + bannerDetail.getId())) {
                    Intent it = new Intent(getBaseActivity(), BannerDetailActivity.class);
                    it.putExtra("bannerDetail", bannerDetail);
                    startActivity(it);
                    break;
                }
            }
        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        switch (v.getId()) {
            case R.id.frCart:
                productDto = ProductDto.builder().build();
                productDto = ((ProductDto) o);
                productDetailVM.getQuantity().set(1);

                try {
                    // set object Product --> ProductDetail
                    ProductDetail productDetail = TsUtils.getData(productDto, ProductDetail.class, null);
                    productDetailVM.getProductDetail().set(productDetail);
                    productDetailVM.arrPackingProduct.setData(productDetail.getPackingProductList());

                    //auto select packing
                    int index = 0;
                    List<ProductPackingDto> productPackingDtos = productDetail.getPackingProductList();
                    for (int i = 0; i < productPackingDtos.size(); i++) {
                        if (productDetail.getQuantity().equals(productPackingDtos.get(i).getQuantity())) {
                            index = i + 1;
                            break;
                        }
                    }
                    if (!sheetAddToCart.isAdded()) {
                        sheetAddToCart.show(Objects.requireNonNull(getFragmentManager()), sheetAddToCart.getTag());
                        productPackingDtos.get(index - 1).index = index;
                        sheetAddToCart.onPackingClick(null, productPackingDtos.get(index - 1));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.imgProduct:
            case R.id.txtNameProduct:
            case R.id.txtPrice:
                Bundle bundle = new Bundle();
                bundle.putLong("productId", ((ProductDto) o).getProductId());
                bundle.putLong("packingProductId", ((ProductDto) o).getPackingProductId());
                Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.itemGroupon:
                Bundle bundle1 = new Bundle();
                bundle1.putLong("productId", ((ProductPackingDto) o).getProductId());
                bundle1.putLong("packingProductId", ((ProductPackingDto) o).getPackingProductId());
                bundle1.putBoolean("isGroupon", true);
                Intent intent1 = new Intent(getBaseActivity(), ProductDetailActivity.class);
                intent1.putExtras(bundle1);
                startActivity(intent1);
                break;
        }
    }

    private void makeFlyAnimation(View fromView, View targetView) {

        new CircleAnimationUtil().attachActivity(getActivity()).setTargetView(fromView).setMoveDuration(500).setDestView(targetView).setAnimationListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                productDetailVM.getTotalProduct();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();


    }
}

