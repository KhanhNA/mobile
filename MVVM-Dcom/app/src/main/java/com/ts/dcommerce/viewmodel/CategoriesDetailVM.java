package com.ts.dcommerce.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.Category;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.dto.ProductDto;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.ui.fragment.ProductFragmentV2;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoriesDetailVM extends BaseViewModel {
    public BaseViewModel arrProductCategories;
    public BaseViewModel arrSubCategories;
    CartVM cartVM = new CartVM(getApplication());
    private int totalPage = 0;
    private int currentPage = 0;

    public CategoriesDetailVM(@NonNull Application application) {
        super(application);
        arrProductCategories = new BaseViewModel(application);
        arrSubCategories = new BaseViewModel(application);
        arrProductCategories.baseModels.set(new ArrayList<>());
    }

    public void getSubCategories(Category category) {
        callApi(HttpHelper.getInstance().getApi().getCategories(category.getId(), AppController.languageId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<Category>>() {
                    @Override
                    public void onSuccess(List<Category> categoryServiceResponse) {
                        getSubCategoriesSuccess(categoryServiceResponse);
                    }

                }));
    }

    private void getSubCategoriesSuccess(List<Category> o) {
        if (o != null) {
            arrSubCategories.setData(o);
            try {
                view.action("getSubCategoriesSuccess", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
    }


    public void getProducts(Category category) {
        callProductByPage(category, 0);
    }

    public void setOrderedProduct(List<ProductDto> list) {
        if (TsUtils.isNotNull(list)) {
            HashMap<String, Demo> hashMap = cartVM.getOrderedProducts();
            for (ProductDto productDto : list) {
                if (productDto == null) {
                    continue;
                }
                productDto.setQuantityOrdered(hashMap.get(productDto.getProductId() + "|" + productDto.getQuantity()) == null ?
                        0 : hashMap.get(productDto.getProductId() + "|" + productDto.getQuantity()).getOrderQuantity().intValue());
            }
            arrProductCategories.baseModels.notifyChange();
        }
    }

    public void setOrderedProduct(List<ProductDto> list, int position) {
        if (TsUtils.isNotNull(list)) {
            HashMap<String, Demo> hashMap = cartVM.getOrderedProducts();
            list.get(position).setQuantityOrdered(hashMap.get(list.get(position).getProductId() + "|" + list.get(position).getQuantity()).getOrderQuantity().intValue());
            arrProductCategories.baseModels.notifyChange();
            ProductFragmentV2.isChangeProduct = true;
        }
    }

    private void getProductsSuccess(ServiceResponse<ProductDto> value) {
        if (value != null) {
            ServiceResponse<ProductDto> dto = value;
            arrProductCategories.addNewPage(dto.getArrData(), dto.getTotalPages());
            setOrderedProduct(dto.getArrData());
            totalPage = dto.getTotalPages();
            try {
                view.action("getProductSuccess", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }

    }

    public void getMoreProduct(Category category) {
        if (totalPage > 0 && currentPage < totalPage) {
            currentPage += 1;
            callProductByPage(category, currentPage);
        } else {
            try {
                view.action("noMore", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
    }

    private void callProductByPage(Category category, int page) {
        callApi(HttpHelper.getInstance().getApi().getProductsCategory(page, AppController.languageId, category.getId())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<ProductDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<ProductDto> categoryServiceResponse) {
                        getProductsSuccess(categoryServiceResponse);
                    }

                }));
    }

    public void onRefresh(Category category) {
        currentPage = 0;
        getProducts(category);
    }

    public void init() {
    }
}
