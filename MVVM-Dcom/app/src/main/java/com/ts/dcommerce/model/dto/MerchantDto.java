package com.ts.dcommerce.model.dto;

import androidx.databinding.BindingAdapter;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.util.StringUtils;
import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MerchantDto extends BaseModel implements Comparable<MerchantDto> {
    private Long merchantId;
    private String merchantCode;
    private String merchantName;
    private Long walletClientId;
    private Long parentMarchantId;
    private Long merchantTypeId;
    private String userName;
    private String password;
    private String fullName;
    private Long defaultStoreId;
    private Double tax;
    private String merchantImgUrl;
    private String mobilePhone;
    private String activeCode;
    private Date activeDate;
    private Date registerDate;
    private Integer activeStatus;
    private Integer status;
    private String address;
    private String passWordSecondTime;
    //properties contact;
    MerchantDto parentMerchant;
    private boolean isRegister;
    private String contactEmail;
    //properties personal
    private Date birthday;
    private String tokenFirebase;
    private String discussionId;
    public String getTaxStr() {
        return "" + (tax == null ? "" : tax);
    }

    public void setTaxStr(String tax) {
        if (StringUtils.isNullOrEmpty(tax)) {
            this.tax = 0D;
        } else {
            this.tax = Double.parseDouble(tax);
        }
    }

    public String getBirthdayStr() {
        return birthday == null ? "" : AppController.formatDate.format(this.birthday);
    }

    @Override
    public int compareTo(MerchantDto other) {
        if(fullName != null && other.fullName != null){
            return fullName.compareTo(other.fullName);
        }
        return 0;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null) {
            return this.mobilePhone.equals(((MerchantDto) obj).mobilePhone);
        }
        return false;
    }
}