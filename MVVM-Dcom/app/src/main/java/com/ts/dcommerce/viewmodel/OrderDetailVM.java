package com.ts.dcommerce.viewmodel;

import android.app.Application;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import androidx.annotation.NonNull;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.model.dto.OrderDetailDTO;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDetailVM extends BaseViewModel<OrderDetailDTO> {
    public BaseViewModel arrOrderProduct;
    public BaseViewModel incentiveProduct;
    private ObservableBoolean canCancel = new ObservableBoolean();
    private List<OrderPackingDTO> product = new ArrayList<>();
    private List<OrderPackingDTO> productIncentive = new ArrayList<>();
    private OrderDTO orderDTO;

    public OrderDetailVM(@NonNull Application application) {
        super(application);
        arrOrderProduct = new BaseViewModel(application);
        incentiveProduct = new BaseViewModel(application);
    }

    public void init(OrderDTO bm) {
        this.orderDTO = bm;
        OrderDetailDTO detail = OrderDetailDTO.builder().orderId(bm.orderId)
                .langId(AppController.languageId)
                .build();
        callApi(HttpHelper.getInstance().getApi().getOrderDetails(detail)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<OrderDetailDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<OrderDetailDTO> response) {
                        getOrderDetailSuccess(response);
                    }

                }));
        // TODO: 7/24/2019 Call API sua thong tin don hang

    }


    private void getOrderDetailSuccess(ServiceResponse<OrderDetailDTO> body) {
        try {

            if (TsUtils.isNotNull(body.getArrData()) && TsUtils.isNotNull(body.getArrData().get(0).getOrderPackings())) {
                OrderDetailDTO detailDTO = body.getArrData().get(0);
                setCanCancelOrder(detailDTO);
                // San pham dat hang
                if (TsUtils.isNotNull(detailDTO.orderPackings)) {
                    product.addAll(detailDTO.orderPackings);
                }
                // San pham khuyen mai
                if (TsUtils.isNotNull(detailDTO.promotionPacking)) {
                    productIncentive.addAll(detailDTO.promotionPacking);
                }

                arrOrderProduct.setFetchSize(product.size());
                arrOrderProduct.addNewPage(product, 1);
                setData(baseModels, (List) product);
                arrOrderProduct.baseModels.notifyChange();

                incentiveProduct.setFetchSize(productIncentive.size());
                incentiveProduct.addNewPage(productIncentive, 1);
                setData(baseModels, (List) productIncentive);
                incentiveProduct.baseModels.notifyChange();
                model.set(detailDTO);
                view.action("getOrderSuccess", null, this, null);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            appException.setValue(e);
        }
    }

    private void setCanCancelOrder(OrderDetailDTO detailDTO) {
        if (!detailDTO.getMerchantId().equals(AppController.getMerchantId())) {
            canCancel.set(detailDTO.orderStatus == 0);
        } else {
            canCancel.set(false);
        }
    }

    public void confirmOrder(String reason, boolean isCancel) {
        if (!isCancel) {
            orderDTO.setOrderStatus(1);
        } else {
            orderDTO.setOrderStatus(-1);
            orderDTO.setReason(reason);
        }

        callApi(HttpHelper.getInstance().getApi().changeOrder(orderDTO)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<OrderDetailDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<OrderDetailDTO> categoryServiceResponse) {
                        try {
                            view.action("updateOrderSuccess", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        super.onFailure(msg, code);
                        try {
                            view.action("updateOrderFail", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }
                }));

    }


}
