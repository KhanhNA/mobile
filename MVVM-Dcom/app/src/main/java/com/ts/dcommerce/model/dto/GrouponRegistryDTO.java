package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrouponRegistryDTO extends BaseModel {
    private Long merchantId;
    private String merchantName;
    private Long registerQuantity;
}
