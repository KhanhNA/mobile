package com.ts.dcommerce.util;

import androidx.databinding.BindingAdapter;

import com.example.myloadingbutton.TsButton;

public class BaseBinding {
    @BindingAdapter("mb_text")
    public static void setTypeFace(TsButton v, String str) {
        v.setButtonLabel(str);
    }
}
