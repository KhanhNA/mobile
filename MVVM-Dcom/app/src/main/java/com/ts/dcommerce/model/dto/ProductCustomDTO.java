package com.ts.dcommerce.model.dto;

import com.ts.dcommerce.model.response.ServiceResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductCustomDTO extends BaseDto {
    ServiceResponse<ProductDto> productDtoPage;
    private List<String> lstPrediction;
}
