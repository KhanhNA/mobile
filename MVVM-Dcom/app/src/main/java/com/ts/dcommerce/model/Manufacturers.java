package com.ts.dcommerce.model;

import com.google.gson.annotations.SerializedName;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Manufacturers extends BaseModel {
    @SerializedName("title")
    private String title;

    @SerializedName("urlImage")
    private String urlImage;

    @SerializedName("id")
    private long manufacturerId;

    @SerializedName("name")
    private String manufacturerName;

    @SerializedName("languageId ")
    private long languageId;

}
