package com.ts.dcommerce.viewmodel;

import android.app.Application;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.db.Administrative;
import com.ts.dcommerce.model.db.AdministrativeDao;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;
import org.greenrobot.greendao.rx.RxQuery;

import java.util.List;

import androidx.annotation.NonNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdministrativeVM extends BaseViewModel{

    private RxDao<Administrative, Void> noteDao;

    public AdministrativeVM(@NonNull Application application) {
        super(application);
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        noteDao = daoSession.getAdministrativeDao().rx();
    }

    public void getAdministrative(Integer parentId){
        List<Administrative> list;
        try{
            list = noteDao.getDao().queryBuilder().where(AdministrativeDao.Properties.ParentId .eq(parentId)).list();
            setData(list);
        }catch (Exception e){
            ToastUtils.showToast(e.getMessage());
        }
    }

    public Administrative getAdministrativeByName(String name){
        List<Administrative> list;
        list = noteDao.getDao().queryBuilder().where(AdministrativeDao.Properties.Name .like(name)).list();
        if(TsUtils.isNotNull(list)) {
            return list.get(0);
        }
        return null;
    }

}
