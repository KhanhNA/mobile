package com.ts.dcommerce.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class OrderTemplateDTO {

    private Long id;

    private Long merchantId;

    private Long orderId;

    private Long packingProductId;

    private Double orderQuantity;

    private Long paymentMethodId;

    private Long shipMethodId;

    private String name;

}