package com.ts.dcommerce.viewmodel;

import android.app.Application;
import android.util.Base64;
import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.dto.WalletDto;
import com.ts.dcommerce.model.dto.WalletHisDto;
import com.ts.dcommerce.model.dto.wallet.AccountPayment;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.HttpsTrustManager;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WalletVM extends BaseViewModel {
    private ObservableField<WalletDto> walletDto = new ObservableField<>();
    private ObservableField<AccountPayment> accountPayment = new ObservableField<>();
    public ObservableBoolean isLoading = new ObservableBoolean();

    public WalletVM(@NonNull Application application) {
        super(application);
    }

    public void initClass() {
        WalletDto walletDto = new WalletDto();
        walletDto.merchantId = AppController.getCurrentMerchant().getMerchantId();
        this.walletDto.set(walletDto);
        setUpdateAdappType(0);
        callApi(HttpHelper.getInstance().getApi().getWallet(this.walletDto.get(), "Wallet", 0, AppController.languageId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<WalletDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<WalletDto> response) {
                        success(response);
                    }

                }));
    }

    private void success(ServiceResponse<WalletDto> o) {
        if (o != null && TsUtils.isNotNull(o.getArrData())) {
            List<WalletDto> serviceResponse = o.getArrData();
            walletDto.set(serviceResponse.get(0));
            getWalletHistory();
        }
    }

    public void getWalletAmount() {
        HttpsTrustManager.allowAllSSL();
        RequestQueue queue = Volley.newRequestQueue(AppController.getInstance());
        final String url = String.format("%s/payment-gateway/api/v1/clients/%s/accounts",
                AppController.BASE_PAYMENT,
                AppController.getCurrentMerchant().getWalletClientId());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {
                    // display response
                    Log.d("Response", response.toString());
                    Gson gson = new Gson();
                    AccountPayment accountPayment = gson.fromJson(response.toString(), AccountPayment.class);
                    getAccountPayment().set(accountPayment);
                },

                error -> Log.d("Error.Response", "")
        ) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put(
                        "Authorization",
                        String.format("Basic %s", Base64.encodeToString(
                                String.format("%s:%s", AppController.getCurrentMerchant().getUserName(), "123113").getBytes(), Base64.DEFAULT)));
                return params;
            }
        };


        queue.add(getRequest);
    }

    public void getWalletHistory() {
        callApi(HttpHelper.getInstance().getApi().getWalletHistories(AppController.getCurrentMerchant().getMerchantId())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<WalletHisDto>>() {
                    @Override
                    public void onSuccess(List<WalletHisDto> response) {
                        successWalletHistory(response);
                    }

                }));
    }

    private void successWalletHistory(List<WalletHisDto> dto) {
        if (dto != null) {
            setData(dto);
            baseModels.notifyChange();
        }
        isLoading.set(false);
    }

    public void onRefresh() {
        isLoading.set(true);
        getWalletHistory();
    }


}
