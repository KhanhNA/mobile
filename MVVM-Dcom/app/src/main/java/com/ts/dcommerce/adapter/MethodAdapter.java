package com.ts.dcommerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ts.dcommerce.R;
import com.ts.dcommerce.model.dto.MethodDTO;

import java.util.List;

public class MethodAdapter extends RecyclerView.Adapter<MethodAdapter.ViewHolderCus> {
    private List<MethodDTO> paymentMethods;
    private ItemClick itemClick;
    private Integer indexSelect;

    public MethodAdapter(List<MethodDTO> dtos) {
        this.paymentMethods = dtos;
        indexSelect = 0;
    }

    public void setListener(ItemClick click) {
        this.itemClick = click;
    }

    @NonNull
    @Override
    public ViewHolderCus onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.method_payment_item, parent, false);
        return new ViewHolderCus(contactView, paymentMethods.get(i).getName());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCus viewHolder, int position) {
        MethodDTO dto = paymentMethods.get(position);
        viewHolder.imageView.setImageResource(dto.getImage());
        viewHolder.nameTextView.setText(dto.getName());
        //
        if (position == indexSelect) {
            viewHolder.linearLayout.setBackgroundResource(R.drawable.border_selected);
        } else {
            viewHolder.linearLayout.setBackgroundResource(R.drawable.border);
        }
    }

    @Override
    public int getItemCount() {
        return paymentMethods == null ? 0 : paymentMethods.size();
    }

    public class ViewHolderCus extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public ImageView imageView;
        public LinearLayout linearLayout;

        public ViewHolderCus(View itemView, String car) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.text);
            imageView = itemView.findViewById(R.id.image1);
            linearLayout = itemView.findViewById(R.id.view);
            linearLayout.setOnClickListener(v -> {
                itemClick.onItemClick(paymentMethods.get(getAdapterPosition()));
                indexSelect = getAdapterPosition();
                notifyDataSetChanged();
            });
        }
    }

    public interface ItemClick {
        void onItemClick(MethodDTO methodDTO);
    }
}
