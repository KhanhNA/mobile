package com.ts.dcommerce.ui.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.XBaseAdapter;
import com.ts.dcommerce.model.dto.OrderPackingDTO;

import java.util.List;
import java.util.Objects;

public class IncentivePackingDialogFragment extends BaseDialogFragment {
    private List<OrderPackingDTO> lstData;

    public IncentivePackingDialogFragment(List<OrderPackingDTO> lstData) {
        this.lstData = lstData;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        assert v != null;
        XRecyclerView rcPacking = v.findViewById(R.id.rcPacking);
        rcPacking.setPullRefreshEnabled(false);
        rcPacking.setLoadingMoreEnabled(false);
        XBaseAdapter packingAdapter = new XBaseAdapter(R.layout.item_order_detail, lstData, this);
        packingAdapter.setConfigXRecycler(rcPacking, 2);
        rcPacking.setAdapter(packingAdapter);
        return v;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()));
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_incentive_packing;
    }

}