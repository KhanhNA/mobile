package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.model.dto.WalletHisDto;
import com.ts.dcommerce.model.dto.wallet.AccountPayment;
import com.ts.dcommerce.network.HttpsTrustManager;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.viewmodel.WalletVM;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class WalletFragment extends BaseFragment {
    WalletVM walletVM;

    Toolbar toolbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return binding.getRoot();
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.layout_item_wallet_his, viewModel, this::onItemClick, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        walletVM = (WalletVM) viewModel;
        walletVM.getWalletAmount();
        walletVM.getWalletHistory();
        walletVM.initClass();

        recyclerView.setNestedScrollingEnabled(true);
        baseAdapter.registerScroll(recyclerView, this::loadData);


    }

    public void loadData() {
        this.walletVM.getWalletHistory();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = getBaseActivity().findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(R.string.WALLET);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_wallet;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return WalletVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.wallet_history;
    }

    public void onItemClick(View view, BaseModel baseModel) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(getContext(), CommonActivity.class);
        OrderDTO orderDTO = OrderDTO.builder()
                .orderId(((WalletHisDto) baseModel).orderId)
                .merchantId(walletVM.getWalletDto().get().merchantId)
                .orderType(1)
//                .orderStatus(0)
                .langId(AppController.languageId).build();
        intent.putExtra("FRAGMENT", OrderDetailFragment.class);
        bundle.putSerializable("BASE_MODEL", orderDTO);
        intent.putExtras(bundle);
        if (getContext() != null) {
            getContext().startActivity(intent);
        }
        closeProcess();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_wallet, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            case R.id.action_setting_wallet:
                // TODO: 7/24/2019 mở màn hình cài đặt ví
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
