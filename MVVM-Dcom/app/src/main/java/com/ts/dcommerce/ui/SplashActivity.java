package com.ts.dcommerce.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.UserInfo;
import com.ts.dcommerce.util.ApiResponse;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.LanguageUtils;
import com.ts.dcommerce.util.NetworkUtils;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.viewmodel.LoginVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class SplashActivity extends BaseActivity {
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageUtils.loadLocale(SplashActivity.this);
        LoginVM loginVM = new LoginVM(getApplication());
        //
        if (getIntent() != null) {
            extras = getIntent().getExtras();
        }
        //
        SharedPreferences preferences = AppController.getInstance().getSharePre();
        if (preferences != null) {
            String userName = preferences.getString(Constants.USER_NAME, "");
            String pass = preferences.getString(Constants.MK, "");
            if (StringUtils.isNotNullAndNotEmpty(userName) && StringUtils.isNotNullAndNotEmpty(pass) && NetworkUtils.isNetworkConnected(SplashActivity.this)) {
                loginVM.getModel().set(UserInfo.builder().userName(userName).passWord(pass).build());
                loginVM.requestLogin();
            } else {
                startLogin();
            }
        } else {
            startLogin();
        }
        loginVM.loginResponse().observe(this, this::consumeResponse);

    }

    private void startLogin() {
        startActivity(new Intent(SplashActivity.this, LoginActivityV2.class));
        // close splash activity
        finish();
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {
            case SUCCESS:
//                if (extras != null) {
//
//                    String actionOpen = "";
//                    long actionId = 0;
//                    actionOpen = extras.getString("click_action", "");
//                    actionId = extras.getLong("actionId", 0);
//                    if (StringUtils.isNotNullAndNotEmpty(actionOpen)) {
//                        Intent intent1 = new Intent(actionOpen);
//                        intent1.putExtra("actionId", actionId);
//                        startActivity(intent1);
//                    }
//                }
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                if (extras != null) {
                    intent.putExtras(extras);
                }
                startActivity(intent);
                finish();
                break;
            case NOT_CONNECT:
            case ERROR:
                startLogin();
                break;

            default:
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
