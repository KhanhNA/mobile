package com.ts.dcommerce.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.databinding.DialogOrderSuccessBinding;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.model.dto.OrderTemplateDTO;
import com.ts.dcommerce.ui.MainActivity;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.PaymentVM;
import com.tsolution.base.CommonActivity;

import java.util.Objects;

@SuppressLint("ValidFragment")
public class OrderSuccessDialog extends DialogFragment implements View.OnClickListener {
    private Activity activity;
    private PaymentVM paymentVM;
    private ProgressDialog pd;

    public OrderSuccessDialog(Activity activity, PaymentVM paymentVM) {
        super();
        this.activity = activity;
        this.paymentVM = paymentVM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogOrderSuccessBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_order_success, container, false);
        binding.setViewHoler2(paymentVM);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView goToHome = view.findViewById(R.id.goToHome);
        TextView trackOrder = view.findViewById(R.id.trackOrder);
        TextView saveTemplate = view.findViewById(R.id.saveTemplate);
        TextView btnCopyCode = view.findViewById(R.id.btnCopyCode);
        ImageView imgQRCode = view.findViewById(R.id.imgSuccess);
        goToHome.setOnClickListener(this);
        trackOrder.setOnClickListener(this);
        btnCopyCode.setOnClickListener(this);
        saveTemplate.setOnClickListener(this);
        //
        pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.processing));
        //set QR Code
        String qrCode = paymentVM.getOrderInfo().logisticQRCode;
        if (!StringUtils.isNullOrEmpty(qrCode)) {
            byte[] decodedString = Base64.decode(qrCode, Base64.DEFAULT);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPurgeable = true;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            Bitmap image = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);
            image = Bitmap.createScaledBitmap(image, width / 2, width / 2, false);
            imgQRCode.setImageBitmap(image);
        }
        //
        paymentVM.orderSaving.observe(this, aBoolean -> {
            if (aBoolean) {
                if (!Objects.requireNonNull(getActivity()).isFinishing()) {
                    ToastUtils.showToast(getString(R.string.save_success));
                    this.getActivity().runOnUiThread(this::dismissPD);
                    dismiss();
                }
            } else {
                ToastUtils.showToast(getString(R.string.save_error));
                dismissPD();
            }
        });
    }

    private void dismissPD() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
            dismiss();
        }
    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        activity.finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.goToHome:
                Intent intent = new Intent(activity, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                break;
            case R.id.trackOrder:
                Bundle bundle = new Bundle();
                Intent in = new Intent(activity, CommonActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra("FRAGMENT", OrderDetailFragment.class);
                bundle.putSerializable("BASE_MODEL", paymentVM.getOrderDTO());
                in.putExtras(bundle);
                activity.startActivity(in);
                break;
            case R.id.btnCopyCode:
                ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(activity.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Good_Boy", paymentVM.getOrderInfo().orderNo);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(activity, getResources().getString(R.string.COPIED_TO_CLIPBOARD), Toast.LENGTH_SHORT).show();
                break;
            case R.id.saveTemplate:
                dialogSaveTemplate();

                break;
        }
    }

    private void saveTemplate(String name) {
        if (paymentVM.getOrderInfo() != null && paymentVM.getOrderInfo().getOrderId() != null) {
            if (pd != null && !pd.isShowing()) {
                pd.show();
            }
            if (TsUtils.isNotNull(paymentVM.getOrderInfo().orderPackings)) {
                paymentVM.saveTemplate(OrderTemplateDTO.builder()
                        .merchantId(AppController.getMerchantId())
                        .orderId(paymentVM.getOrderInfo().getOrderId())
                        .name(name)
                        .shipMethodId(paymentVM.getOrderInfo().getShipMethodId() != null ? paymentVM.getOrderInfo().getShipMethodId() : 0)
                        .paymentMethodId(paymentVM.getOrderInfo().getShipMethodId() != null ? paymentVM.getOrderInfo().getShipMethodId() : 0)
                        .build());

            }

        }
    }

    private void dialogSaveTemplate() {
        final EditText input = new EditText(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.input_name))
                .setPositiveButton(getString(R.string.ok), null)
                .setNegativeButton(getString(R.string.cancel), (dialog1, which) -> {
                })
                .setView(input)
                .show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(v -> {
            if (StringUtils.isNotNullAndNotEmpty(input.getText().toString().trim())) {
                dialog.dismiss();
                saveTemplate(input.getText().toString().trim());
            } else {
                ToastUtils.showToast(getString(R.string.NOT_EMPTY_NAME));
            }
        });
    }
}
