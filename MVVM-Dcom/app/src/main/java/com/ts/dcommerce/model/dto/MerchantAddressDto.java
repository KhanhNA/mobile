package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;


/**
 * @author PhamBien
 */
@Getter
@Setter
public class MerchantAddressDto extends BaseModel {
    private Long id;
    private Long merchantId;
    private String province;
    private String district;
    private String village;
    private String phoneNumber;
    private String fullName;
    private int isSelect;
    private String address;
    private Double latitude;
    private Double longitude;

}