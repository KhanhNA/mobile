package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuggestionSearchDTO extends BaseModel {
    private String productName;
    private Long packingProductId;
    private Long productId;
}