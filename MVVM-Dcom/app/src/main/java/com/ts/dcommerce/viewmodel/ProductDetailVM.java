package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.annotation.NonNull;
import android.view.View;

import com.example.myloadingbutton.TsButton;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.ProductDetail;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.db.DemoDao;
import com.ts.dcommerce.model.dto.GrouponRegisterDetailDTO;
import com.ts.dcommerce.model.dto.IncentiveDescriptionDTO;
import com.ts.dcommerce.model.dto.LoyaltyPointDTO;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;
import rx.android.schedulers.AndroidSchedulers;


@Getter
@Setter
public class ProductDetailVM extends BaseViewModel {
    private int quantityTem = 1;
    public static Boolean isChangeProduct = false;
    public BaseViewModel arrPackingProduct;
    private boolean isFlashSale;
    private ObservableInt indexPacking = new ObservableInt();
    ObservableField<Integer> quantity = new ObservableField<>();
    ObservableField<ProductDetail> productDetail = new ObservableField<>();
    private RxDao<Demo, Void> noteDao;
    ObservableField<Long> numProducts = new ObservableField<>();
    ProductPackingDto packing;
    private Long packingProductId;
    private boolean isRegisterCoupon;
    private ObservableField<Boolean> isGroupon = new ObservableField<>();
    private List<IncentiveDescriptionDTO> arrIncentive;
    private List<LoyaltyPointDTO> arrPointDTO;

    public ProductDetailVM(@NonNull Application application) {
        super(application);
        isRegisterCoupon = false;
        quantity.set(quantityTem);
        arrPackingProduct = new BaseViewModel(application);
        arrIncentive = new ArrayList<>();
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        noteDao = daoSession.getDemoDao().rx();
        isGroupon.set(false);
        arrPointDTO = new ArrayList<>();
    }

    private void getProductDetailSuccess(ProductDetail value) {
        if (value != null) {
            try {
                productDetail.set(value);

                int stt = 0;
                for (ProductPackingDto productPackingDto : Objects.requireNonNull(productDetail.get()).getPackingProductList()) {
                    if (productPackingDto.getPackingProductId().equals(packingProductId)) {
                        productPackingDto.index = stt + 1;
                        packing = productPackingDto;
                    }
                    stt++;
                }
                if (isFlashSale) {
                    // TODO: 9/8/2019 get Duration flashSale
                    packing.setDuration(100);
                    List<ProductPackingDto> temp = new ArrayList<>();
                    temp.add(packing);
                    arrPackingProduct.setData(temp);
                } else {
                    arrPackingProduct.setData(((ProductDetail) value).getPackingProductList());
                }
                view.action("updateProductDes", null, this, null);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public void init(Long packingProductId) {
        this.packingProductId = packingProductId;
    }

    // invoke
    public void addToCart(ProductDetail productDetail, View v) {
        if (v instanceof TsButton) {
            ((TsButton) v).showLoadingButton();
        }
        try {
            if (quantity.get() > 0) {
                if (isRegisterCoupon) {
                    // Register groupon
                    if (TsUtils.isNotNull(arrIncentive)) {
                        for (IncentiveDescriptionDTO dto : arrIncentive) {
                            if (dto.getIncentiveType() == 3) {
                                GrouponRegisterDetailDTO groupon = new GrouponRegisterDetailDTO();
                                groupon.setIncentiveProgramId(dto.getIncentiveProgramId());
                                groupon.setPackingProductId(productDetail.getPackingProductList().get(indexPacking.get()).getPackingProductId());
                                groupon.setMerchantId(AppController.getCurrentMerchant().getMerchantId());
                                groupon.setRegisterQuantity(quantity.get());
                                callApi(HttpHelper.getInstance().getApi().grouponRegister(groupon)
                                        .compose(RxSchedulers.io_main())
                                        .subscribeWith(new RxSubscriber<GrouponRegisterDetailDTO>() {

                                            @Override
                                            public void onSuccess(GrouponRegisterDetailDTO grouponRegisterDetailDTO) {
                                                grouponRegisterSuccess(grouponRegisterDetailDTO);
                                            }

                                            @Override
                                            public void onFailure(String msg, int code) {
                                                if (v instanceof TsButton) {
                                                    ((TsButton) v).showErrorButton();
                                                }
                                            }
                                        }));

                                return;
                            }
                        }
                    }
                } else {
                    // Order
                    if (TsUtils.isObjectNotNull(productDetail)) {
                        Demo cart;
                        try {
                            cart = TsUtils.getData(productDetail, Demo.class, null);
                            cart.setPackingProductCode(productDetail.getPackingProductList().get(indexPacking.get()).getPackingProductCode());
                            cart.setPrice(productDetail.getPackingProductList().get(indexPacking.get()).getPrice());
                            cart.setQuantity(productDetail.getPackingProductList().get(indexPacking.get()).getQuantity());
                            cart.setUrl(productDetail.getPackingProductList().get(indexPacking.get()).getPackingUrl());
                            cart.setPackingProductId(productDetail.getPackingProductList().get(indexPacking.get()).getPackingProductId());
                            List<Demo> arr =
                                    noteDao.getDao().queryBuilder().where(DemoDao.Properties.ProductId.eq(productDetail.getProductId()),
                                            DemoDao.Properties.Quantity.eq(productDetail.getPackingProductList().get(indexPacking.get()).getQuantity())).list();
                            if (TsUtils.isNotNull(arr)) {
                                // San pham da ton tai trong gio hang
                                cart.setOrderQuantity(arr.get(0).getOrderQuantity() + quantity.get());
                            } else {
                                // Chua ton tai trong gio hang
                                cart.setOrderQuantity(Objects.requireNonNull(quantity.get()).doubleValue());
                            }

                            noteDao.insertOrReplace(cart)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(note -> {
                                        // Update UI
                                        try {
                                            view.action("addToCart", v, this, null);
                                        } catch (AppException e) {
                                            appException.setValue(e);
                                        }
                                    });

                        } catch (Exception e) {
                            appException.setValue(e);
                        }
                    }
                }
            } else {
                ToastUtils.showToast(R.string.QUANTITY_ERROR);
                ((TsButton)v).showErrorButton();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void grouponRegisterSuccess(GrouponRegisterDetailDTO detailDTO) {
        // Update UI
        if (detailDTO != null) {
            try {
                view.action("grouponRegisterSuccess", null, this, null);
                // Đăng ký nhận thông báo khi người tham gia nhóm goupon
                // Topic Name: groupon_IncentiveID_GrouponIdRegister
                FirebaseMessaging.getInstance().subscribeToTopic("groupon_" + detailDTO.getIncentiveProgramId() + "_" + detailDTO.getId())
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                System.out.println("Register topic groupon_" + detailDTO.getIncentiveProgramId() + "_" + detailDTO.getId());
                            }
                        });

            } catch (AppException e) {
                appException.setValue(e);
            }
        }
    }


    // invoke
    public void addQuantity() {
        Integer quantity = this.quantity.get();
        if (quantity != null) {
            quantityTem = quantity;
            quantityTem++;
            this.quantity.set(quantityTem);
        }
    }

    // invoke
    public void subQuantity() {
        Integer q = quantity.get();
        if (q != null) {
            quantityTem = q;
            if (quantityTem > 1) {
                quantityTem--;
                quantity.set(quantityTem);
            }
        }
    }


    public void getProductDetailAPI(long id) {
        callApi(HttpHelper.getInstance().getApi().getProductById(id, AppController.languageId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ProductDetail>() {
                    @Override
                    public void onSuccess(ProductDetail productDetail) {
                        getProductDetailSuccess(productDetail);
                    }
                }));
    }

    public void getPoint(long packingProductId) {
        callApi(HttpHelper.getInstance().getApi().getPoint(packingProductId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<LoyaltyPointDTO>>() {
                    @Override
                    public void onSuccess(List<LoyaltyPointDTO> dtos) {
                        getPointSuccess(dtos);
                    }
                }));
    }

    public void getIncentivePacking(long id) {
        callApi(HttpHelper.getInstance().getApi().getIncentivePacking(id)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<IncentiveDescriptionDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<IncentiveDescriptionDTO> productDetail) {
                        getIncentivePackingSuccess(productDetail);
                    }
                }));
    }

    private void getIncentivePackingSuccess(ServiceResponse<IncentiveDescriptionDTO> o) {
        if (o != null) {
            try {
                arrIncentive = o.getArrData();
                view.action("getIncentiveSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }

    private void getPointSuccess(List<LoyaltyPointDTO> arr) {
        arrPointDTO.clear();
        if (TsUtils.isNotNull(arr)) {
            arrPointDTO.addAll(arr);
        }
        try {
            view.action("getPointSuccess", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void getTotalProduct() {
        noteDao.count().subscribe(aLong -> numProducts.set(aLong));
    }

}
