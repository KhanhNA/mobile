package com.ts.dcommerce.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Count {
    private Integer countNumber;
}
