package com.ts.dcommerce.ui.fragment;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.kofigyan.stateprogressbar.StateProgressBar;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.ConfirmOrderPagerAdapter;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.OrderProductCartDTO;
import com.ts.dcommerce.viewmodel.OrderConfirmVM;
import com.ts.dcommerce.widget.NonSwipeableViewPager;
import com.tsolution.base.BR;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

public class OrderConfirmFragment extends BaseFragment {
    protected String[] descriptionDataL1;
    protected String[] descriptionDataL2;
    protected StateProgressBar stateprogressbar;
    private NonSwipeableViewPager vpPager;
    private OrderConfirmVM orderConfirmVM;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreateView(inflater, container, savedInstanceState);
        //
        descriptionDataL1 = new String[]{getString(R.string.delivery), getString(R.string.payment)};
        descriptionDataL2 = new String[]{getString(R.string.address)};
        viewModel = ViewModelProviders.of(getBaseActivity()).get(OrderConfirmVM.class);
        binding.setVariable(BR.viewModel, viewModel);
        orderConfirmVM = (OrderConfirmVM) viewModel;
        //
        if (getBaseActivity().getIntent() != null && getBaseActivity().getIntent().hasExtra("orderProduct")) {
            OrderProductCartDTO orderProductCartDTO = (OrderProductCartDTO) getBaseActivity().getIntent().getExtras().getSerializable("orderProduct");
            ((OrderConfirmVM) viewModel).setOrderProductCartDTO(orderProductCartDTO);
            orderConfirmVM.prepareData();
        }
        View v = binding.getRoot();
        initView(v);
        if (AppController.getCurrentMerchant().getMerchantTypeId() == 2) {
            stateprogressbar.setStateDescriptionData(descriptionDataL2);
            stateprogressbar.setMaxStateNumber(StateProgressBar.StateNumber.ONE);
        } else {
            stateprogressbar.setStateDescriptionData(descriptionDataL1);
            stateprogressbar.setMaxStateNumber(StateProgressBar.StateNumber.TWO);
        }
        stateprogressbar.setAnimationDuration(100);
        vpPager.setOffscreenPageLimit(2);
        ConfirmOrderPagerAdapter myPagerAdapter = new ConfirmOrderPagerAdapter(getBaseActivity().getSupportFragmentManager(), AppController.getCurrentMerchant().getMerchantTypeId());
        vpPager.setAdapter(myPagerAdapter);
        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
                        break;
                    case 1:
                        stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                        break;
                    case 2:
                        stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        return v;
    }

    private void initView(View v) {
        Toolbar toolbar1 = v.findViewById(R.id.toolbar1);
        getBaseActivity().setSupportActionBar(toolbar1);
        Objects.requireNonNull(getBaseActivity().getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getBaseActivity().getSupportActionBar()).setDisplayShowHomeEnabled(true);
        stateprogressbar = v.findViewById(R.id.usage_stateprogressbar);
        vpPager = v.findViewById(R.id.view_pager);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Integer pageSelected) {
        if (pageSelected != null) {
            switch (pageSelected) {
                case 0:
                    vpPager.setCurrentItem(0);
                    stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
                    break;
                case 1:
                    vpPager.setCurrentItem(1);
                    stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                    break;
                case 2:
                    vpPager.setCurrentItem(2);
                    stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                    break;
            }
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.btnPayment) {
        }
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderConfirmVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_order_confirm;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}