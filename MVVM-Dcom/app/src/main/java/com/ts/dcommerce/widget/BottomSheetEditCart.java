package com.ts.dcommerce.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.databinding.ViewDataBinding;

import com.example.myloadingbutton.TsButton;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.ts.dcommerce.BR;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.db.DemoDao;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.ui.fragment.CategoriesDetailFragment;
import com.ts.dcommerce.ui.fragment.DeliveryFragment;
import com.ts.dcommerce.ui.fragment.ProductFragmentV2;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;

import rx.android.schedulers.AndroidSchedulers;

@SuppressLint("ValidFragment")
public class BottomSheetEditCart extends BottomSheetDialogFragment implements View.OnClickListener {
    private ViewDataBinding binding;
    OrderPackingDTO orderPackingDTO;
    ImageView hideBottomSheet;
    private IconText btnAdd, btnSub;
    private EditText edSoLuongSanPham;
    private int quantityTem = 0;
    private TsButton btnUpdate;
    private RxDao<Demo, Void> noteDao;
    private CallbackUpdate callbackUpdate;

    public void setCallbackUpdate(CallbackUpdate update) {
        this.callbackUpdate = update;
    }

    @SuppressLint("ValidFragment")
    public BottomSheetEditCart(OrderPackingDTO dto) {
        DaoSession daoSession = AppController.getInstance().getDaoSession();
        this.orderPackingDTO = dto;
        noteDao = daoSession.getDemoDao().rx();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_edit_quantity, container, false);
        binding.setVariable(BR.viewHolderEdit, orderPackingDTO);
        return binding.getRoot();
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideBottomSheet = view.findViewById(R.id.hideBottomSheet);
        hideBottomSheet.setOnClickListener(this);
        //
        btnAdd = binding.getRoot().findViewById(R.id.btnAdd);
        btnSub = binding.getRoot().findViewById(R.id.btnSub);
        btnUpdate = binding.getRoot().findViewById(R.id.btnPacking);
        edSoLuongSanPham = binding.getRoot().findViewById(R.id.edSoLuongSanPham);
        edSoLuongSanPham.setText("" + orderPackingDTO.getQuantity().intValue());
        btnAdd.setOnClickListener(v -> {
            if (StringUtils.isNotNullAndNotEmpty(edSoLuongSanPham.getText().toString())) {
                quantityTem = Integer.parseInt(edSoLuongSanPham.getText().toString());
                quantityTem++;
                edSoLuongSanPham.setText("" + quantityTem);
            }

        });
        btnSub.setOnClickListener(v -> {
            if (StringUtils.isNotNullAndNotEmpty(edSoLuongSanPham.getText().toString())) {
                quantityTem = Integer.parseInt(edSoLuongSanPham.getText().toString());
                if (quantityTem > 1) {
                    quantityTem--;
                    edSoLuongSanPham.setText("" + quantityTem);
                }
            }
        });
        btnUpdate.setOnClickListener(v -> {
            if (StringUtils.isNotNullAndNotEmpty(edSoLuongSanPham.getText().toString())) {
                Demo demo = noteDao.getDao().queryBuilder().where(DemoDao
                        .Properties
                        .PackingProductId
                        .eq(orderPackingDTO.getPackingProductId())).unique();
                //edit giỏ hàng khi chưa thanh toán
                if(demo != null) {
                    demo.setOrderQuantity(Double.parseDouble(edSoLuongSanPham.getText().toString()));
                    orderPackingDTO.setQuantity(Integer.parseInt(edSoLuongSanPham.getText().toString()));
                    noteDao.insertOrReplace(demo)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(note -> {
                                if (callbackUpdate != null) {
                                    callbackUpdate.callBack(orderPackingDTO);
                                }
                                // Update UI
                                ProductFragmentV2.isChangeProduct = true;
                                CategoriesDetailFragment.isChangeProduct = true;
                                DeliveryFragment.isRefresh = true;
                                ToastUtils.showToast(getString(R.string.update_success));
                                dismiss();
                            });
                //edit đơn mẫu
                }else {
                    orderPackingDTO.setQuantity(Integer.parseInt(edSoLuongSanPham.getText().toString()));
                    if (callbackUpdate != null) {
                        callbackUpdate.callBack(orderPackingDTO);
                    }
                    DeliveryFragment.isRefresh = true;
                    ToastUtils.showToast(getString(R.string.update_success));
                    dismiss();
                }

            }

        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.hideBottomSheet) {
            this.dismiss();
        }
    }

    public interface CallbackUpdate {
        void callBack(OrderPackingDTO dto);
    }
}
