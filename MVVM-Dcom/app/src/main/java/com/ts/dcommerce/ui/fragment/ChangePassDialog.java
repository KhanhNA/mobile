package com.ts.dcommerce.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ts.dcommerce.R;
import com.ts.dcommerce.databinding.DialogChangePassBinding;
import com.ts.dcommerce.viewmodel.SettingVM;

@SuppressLint("ValidFragment")
public class ChangePassDialog extends DialogFragment {
    private SettingVM settingVM;

    public ChangePassDialog(Activity activity, SettingVM settingVM) {
        super();
        this.settingVM = settingVM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogChangePassBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_pass, container, false);
        binding.setViewHoler2(settingVM);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

}
