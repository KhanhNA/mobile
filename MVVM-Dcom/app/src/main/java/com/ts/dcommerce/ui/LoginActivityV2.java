package com.ts.dcommerce.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.lifecycle.Observer;

import com.example.myloadingbutton.TsButton;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.databinding.ActivityLoginV2Binding;
import com.ts.dcommerce.model.UserInfo;
import com.ts.dcommerce.ui.fragment.ForgetPassFragment;
import com.ts.dcommerce.util.ApiResponse;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.IntentConstants;
import com.ts.dcommerce.util.LanguageUtils;
import com.ts.dcommerce.util.NetworkUtils;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.viewmodel.LoginVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.ActionsListener;

import static com.ts.dcommerce.base.AppController.BASE_LOGISTIC_URL;


public class LoginActivityV2 extends BaseActivity<ActivityLoginV2Binding> implements ActionsListener {
    private int FORGET_PASS_CODE = 111;

    private EditText edCode;
    private TextView tvNotEnterCode;
    private LoginVM loginVM;
    private AlertDialog dialog;
    private TsButton btnLogin;
    private AppCompatSpinner spLanguage;
    private int check = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginVM = (LoginVM) viewModel;
        btnLogin = binding.btnLogin;
        spLanguage = binding.spin;
        loginVM.loginResponse().observe(this, this::consumeResponse);
        spLanguage.setSelection(AppController.getInstance().getSharePre().getInt("current_language", 0));
        //
        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (++check > 1) {
                    try {
                        LanguageUtils.changeLanguage(LoginActivityV2.this, AppController.LANGUAGE_CODE[i].toLowerCase());
                        AppController.getInstance().getSharePre().edit().putInt("current_language", i).apply();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loginVM.uc.pSwitchEvent.observe(this, aBoolean -> {
            if (loginVM.uc.pSwitchEvent.getValue()) {
                binding.ivSwichPasswrod.setImageResource(R.drawable.show_psw);
                binding.etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                binding.ivSwichPasswrod.setImageResource(R.drawable.show_psw_press);
                binding.etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });

        binding.btnForgetPass.setOnClickListener(v-> openForgetPassword());

    }

    private void openForgetPassword() {
        Intent intent = new Intent(LoginActivityV2.this, CommonActivity.class);
        intent.putExtra("FRAGMENT", ForgetPassFragment.class);
        startActivityForResult(intent, FORGET_PASS_CODE);
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {
            case LOADING:
//                progressDialog.show();
                break;

            case SUCCESS:
                btnLogin.showDoneButton();
                binding.txtLoginFail.setVisibility(View.GONE);
                startActivity(new Intent(LoginActivityV2.this, MainActivity.class));
                finish();
                break;

            case ERROR:
                binding.txtLoginFail.setVisibility(View.VISIBLE);
                btnLogin.showErrorButton();
                break;
            case NOT_CONNECT:
                Toast.makeText(this, R.string.not_connect_server, Toast.LENGTH_SHORT).show();
                btnLogin.showErrorButton();
                break;

            default:
                break;
        }
    }


    private void showDialogInputCode() {
        // Khoi tao retrofit
        new HttpHelper.Builder(AppController.getInstance())
                .initOkHttp()
                .createRetrofit(AppController.BASE_URL).createRetrofitLogistic(BASE_LOGISTIC_URL)
                .build();
        //
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View input_code = getLayoutInflater().inflate(R.layout.dialog_input_code_login, null);
        edCode = input_code.findViewById(R.id.edCode);
        tvNotEnterCode = input_code.findViewById(R.id.tvNotEnterCode);
        builder.setView(input_code);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
        });
        builder.setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel());
        //overide lại button để hiện thi text loi
        dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (!StringUtils.isNullOrEmpty(edCode.getText().toString())) {
                processError(null, null, null, null);
                loginVM.inputCode(edCode.getText().toString().toUpperCase());
                tvNotEnterCode.setVisibility(View.GONE);
            } else {
                tvNotEnterCode.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case IntentConstants.INTENT_INPUT_SUCCES:
                dialog.dismiss();
                Intent intent = new Intent(LoginActivityV2.this, RegisterMerchantActivity.class);
                intent.putExtra(IntentConstants.INTENT_INPUT_CODE, edCode.getText().toString());
                startActivityForResult(intent, 111);
                break;
            case IntentConstants.INTENT_INPUT_FAIL:
                tvNotEnterCode.setVisibility(View.VISIBLE);
                tvNotEnterCode.setText(getString(R.string.INPUT_CODE_FAIL));
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_login_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        if (R.id.btnLogin == v.getId()) {
            if (isValid()) {
                if (!NetworkUtils.isNetworkConnected(this)) {
                    Toast.makeText(LoginActivityV2.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                } else {
                    btnLogin.showLoadingButton();
                    loginVM.requestLogin();
                }
            }
        }
        if (R.id.tvInputCode == v.getId()) {
            showDialogInputCode();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1111) {
            String result = data.getStringExtra("result_inputCode");
            String userNameParent = data.getStringExtra("result_inputCode_parentName");
            if (StringUtils.isNotNullAndNotEmpty(result)) {
                showDialogInputSuccess(userNameParent);
                loginVM.getModel().set(UserInfo.builder().userName(result).build());
            }
        }
    }

    private void showDialogInputSuccess(String parentName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getBaseActivity());
        View inputSuccess = getLayoutInflater().inflate(R.layout.dialog_input_success, null);
        TextView txtMerchantSuccess = inputSuccess.findViewById(R.id.txtMerchantSuccess);
        txtMerchantSuccess.setText(getString(R.string.REGISTER_MERCHANT) + Constants.SPACE + parentName + Constants.SPACE + getString(R.string.SUCCESS));
        builder.setView(inputSuccess);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    private boolean isValid() {
        if (loginVM.getModelE().getUserName().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_username));
            return false;
        } else if (loginVM.getModelE().getPassWord().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_password));
            return false;
        }

        return true;
    }

}
