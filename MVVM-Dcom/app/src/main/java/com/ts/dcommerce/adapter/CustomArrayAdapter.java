package com.ts.dcommerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.ts.dcommerce.model.dto.MerchantWareHouseDTO;

import java.util.List;

public class CustomArrayAdapter extends ArrayAdapter<String> {
    private final List<MerchantWareHouseDTO> items;
    private final int mResource;

    public CustomArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                              @NonNull List objects) {
        super(context, resource, 0, objects);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), mResource, parent, false);
        bind(binding, items.get(position));
        return binding.getRoot();
    }


    public void bind(ViewDataBinding viewDataBinding, Object obj) {
        viewDataBinding.setVariable(com.ts.dcommerce.BR.viewHolder, obj);
        viewDataBinding.executePendingBindings();
    }
}