package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

public class BaseDto extends BaseModel {

    public String ORDER_BY;
    public Integer langId;
}
