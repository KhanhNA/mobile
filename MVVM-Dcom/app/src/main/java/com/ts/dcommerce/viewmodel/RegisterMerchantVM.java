package com.ts.dcommerce.viewmodel;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.example.myloadingbutton.TsButton;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.IntentConstants;
import com.ts.dcommerce.util.StringUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Response;

public class RegisterMerchantVM extends BaseViewModel<MerchantDto> {
    private String activeCode = "";
    public String userName = "";
    public String parentMerchantName = "";
    private boolean checkBox;
    public boolean checked = false;
    public ObservableField<Boolean> checkRule = new ObservableField<>(false);

    public RegisterMerchantVM(@NonNull Application application) {
        super(application);
        model.set(new MerchantDto());
    }

    public void setActiveCode(String activeCode) {
        this.activeCode = activeCode;
    }

    public void register(View v, MerchantDto merchantDto) {


        String fullName = merchantDto.getFullName();
        String mobilePhone = merchantDto.getMobilePhone();
        String passWord = merchantDto.getPassword();
        String passWordSecondTime = merchantDto.getPassWordSecondTime();
        if (!StringUtils.isNullOrEmpty(fullName) && !StringUtils.isNullOrEmpty(mobilePhone)
                && !StringUtils.isNullOrEmpty(passWord) && !StringUtils.isNullOrEmpty(passWordSecondTime)
                && passWord.equals(passWordSecondTime) && checkRule.get() && passWord.equalsIgnoreCase(passWordSecondTime)) {
            if (v instanceof TsButton) {
                ((TsButton) v).showLoadingButton();
            }
            callApi(HttpHelper.getInstance().getApi().createMerchant(getModelE(), activeCode)
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<MerchantDto>() {
                        @Override
                        public void onSuccess(MerchantDto dto) {
                            try {
                                if (dto != null) {
                                    userName = dto.getUserName();
                                    parentMerchantName = dto.getParentMerchant().getFullName();
                                    view.action("registerSuccess", null, RegisterMerchantVM.this, null);
                                }

                            } catch (AppException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            super.onError(e);
                            if (v instanceof TsButton) {
                                ((TsButton) v).showErrorButton();
                            }
                        }
                    }));
        } else if (StringUtils.isNullOrEmpty(fullName)) {
            try {
                view.action(IntentConstants.INTENT_NOT_INPUT_FULL_NAME, null, this, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else if (StringUtils.isNullOrEmpty(mobilePhone)) {
            try {
                view.action(IntentConstants.INTENT_NOT_INPUT_MOBILE_PHONE, null, this, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else if (StringUtils.isNullOrEmpty(passWord)) {
            try {
                view.action(IntentConstants.INTENT_NOT_INPUT_PASSWORD, null, this, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else if (StringUtils.isNullOrEmpty(passWordSecondTime)) {
            try {
                view.action(IntentConstants.INTENT_NOT_INPUT_PASSWORD_SECOND, null, this, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else if (!passWord.equals(passWordSecondTime)) {
            try {
                view.action(IntentConstants.INTENT_PASSWORD_NOT_EQUALS, null, this, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else if (!checkRule.get()) {
            appException.setValue(new AppException(R.string.RULE_IS_NOT_CHECK, ""));
        } else {
            appException.setValue(new AppException(R.string.REQUIRE_CHECK_NAME_PASSWORD, ""));
        }

    }

    private void registerSuccess(Call call, Response response, Object o, Throwable throwable) {
        //
        if (response != null) {
            if (response.code() == HttpsURLConnection.HTTP_OK) {

                //trung so dien thoai
            } else if (response.code() == HttpsURLConnection.HTTP_CONFLICT) {
                appException.setValue(new AppException(R.string.validate_create_merchant, ""));
            }//trung ten dang nhap
            else if (response.code() == HttpsURLConnection.HTTP_GONE) {
                appException.setValue(new AppException(R.string.VALIDATE_CREATE_NAME_MERCHANT, ""));
            }
        }

    }

    private void loginSuccess(Call call, Response response, Object o, Throwable throwable) {
        try {
            view.action("registerSuccess", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }
}
