package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ns.chat.views.activity.profile.ProfileFragment;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.viewmodel.MerchantDetailVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import lombok.Setter;

//import eu.siacs.conversations.ui.ConversationsActivity;

@Setter
public class MerchantInfoFragment extends BaseFragment {
    MerchantDto merchantDto;
    MerchantDetailVM merchantDetailVM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        merchantDetailVM = (MerchantDetailVM) viewModel;
        if (merchantDto != null) {
            merchantDetailVM.setMerchant(merchantDto);
            merchantDetailVM.getInfoL2(merchantDto.getMerchantId());
        }
        return view;
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        Fragment fragment = new ProfileFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(ProfileFragment.USER_ID_EXTRA_KEY, merchantDto.getUserName());
//        fragment.setArguments(bundle);
//        getChildFragmentManager().beginTransaction()
//
//                .add(R.id.profileInfo, fragment)
//                .commit();
//    }


    @Override
    public void adapterAction(View view, BaseModel baseModel) {
        if (view.getId() == R.id.btnSendSms) {
            Intent launchIntent = AppController.getInstance().getPackageManager().getLaunchIntentForPackage(Constants.chatAppId);
            if (launchIntent != null && !merchantDto.getUserName().isEmpty()) {
                launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //TODO: Fix cung username
                launchIntent.putExtra("INTENT_USER_NAME", merchantDto.getUserName());
                startActivity(launchIntent);
            } else {
                // TODO: 1/7/2020 show dialog download chat app
                ToastUtils.showToast("You must download mingalaba chat app");
            }
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_merchant_info;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MerchantDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
