package com.ts.dcommerce.viewmodel;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import android.net.Uri;
import android.provider.ContactsContract;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.BaseResponse;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.db.ContactDB;
import com.ts.dcommerce.model.db.ContactDBDao;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.model.dto.OrderDetailDTO;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.model.dto.OrderProductCartDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderVM extends BaseViewModel {
    private Application mApp;
    HashMap<String, MerchantDto> contacts;
    private RxDao<ContactDB, Void> noteDao;
    private int totalPage = 0;
    private int currentPage = 0;
    public ObservableField<Integer> recordCount = new ObservableField<>();
    public ObservableField<Integer> typeList = new ObservableField<>();
    public ObservableField<Boolean> isTemplate = new ObservableField<>();
    OrderDTO.OrderDTOBuilder orderDTO;
    public int currentTab;
    OrderProductCartDTO orderProductCartDTO;

    public OrderVM(@NonNull Application application) {
        super(application);
        this.mApp = application;
        recordCount.set(0);
        typeList.set(1);
        isTemplate.set(false);
    }

    public void init(int currentTab) {
        this.currentTab = currentTab;
    }

    //invoke function
    public String getMerchantImg(String merchantCode) {
        if (merchantCode != null) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            noteDao = daoSession.getContactDBDao().rx();
            List<ContactDB> contacts = noteDao.getDao().queryBuilder()
                    .where(ContactDBDao.Properties.ContactId.eq(merchantCode))
                    .list();
            if (TsUtils.isNotNull(contacts)) {
                return contacts.get(0) != null ? contacts.get(0).getUrlImg() : "";
            }
        }
        return "";
    }

    //màu trạng thái đơn hàng
    public int getColorStatus(int orderStatus) {
        switch (orderStatus) {
            case 0:
                return AppController.getInstance().getResources().getColor(R.color.colorAccent_v2);
            case 1:
                return AppController.getInstance().getResources().getColor(R.color.da_cam);
            case 2:
                return AppController.getInstance().getResources().getColor(R.color.green_text);
            case -1:
                return AppController.getInstance().getResources().getColor(R.color.red);
        }
        return Resources.getSystem().getColor(R.color.background_color);
    }

    //màu trạng thái đơn hàng
    public String getTitleStatus(int orderStatus) {
        switch (orderStatus) {
            case 0:
                return AppController.getInstance().getResources().getStringArray(R.array.title_order)[0];
            case 1:
                return AppController.getInstance().getResources().getStringArray(R.array.title_order)[1];
            case 2:
                return AppController.getInstance().getResources().getStringArray(R.array.title_order)[2];
            case -1:
                return AppController.getInstance().getResources().getStringArray(R.array.title_order)[3];
        }
        return "";
    }

    //invoke function
    public void getMoreOrder() {
        if (totalPage > 0 && currentPage < totalPage) {
            currentPage += 1;
            callApi(HttpHelper.getInstance().getApi()
                    .getOrders(orderDTO.build(), currentPage)
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<ServiceResponse<OrderDTO>>() {
                        @Override
                        public void onSuccess(ServiceResponse<OrderDTO> orderDTOServiceResponse) {
                            getOrdersSuccess(orderDTOServiceResponse);
                        }
                    }));
        } else {
            try {
                view.action("noMore", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
    }

    private void getOrderFromApi() {
        callApi(HttpHelper.getInstance().getApi()
                .getOrders(orderDTO.build(), 0)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<OrderDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<OrderDTO> orderDTOServiceResponse) {
                        getOrdersSuccess(orderDTOServiceResponse);
                    }
                }));
    }

    public void getOrderTemplate() {
        OrderDTO dto = OrderDTO.builder()
                .merchantId(AppController.getMerchantId()).build();
        callApi(HttpHelper.getInstance().getApi()
                .getOrdersTemplate(dto, 0)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<OrderDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<OrderDTO> orderDTOServiceResponse) {
                        getOrdersSuccess(orderDTOServiceResponse);
                    }
                }));
        isTemplate.set(true);
    }

    public void getOrderMoreTemplate() {
        if (totalPage > 0 && currentPage < totalPage) {
            currentPage += 1;
            OrderDTO dto = OrderDTO.builder()
                    .merchantId(AppController.getMerchantId()).build();
            callApi(HttpHelper.getInstance().getApi()
                    .getOrdersTemplate(dto, currentPage)
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<ServiceResponse<OrderDTO>>() {
                        @Override
                        public void onSuccess(ServiceResponse<OrderDTO> orderDTOServiceResponse) {
                            getOrdersSuccess(orderDTOServiceResponse);
                        }
                    }));

        } else {
            try {
                view.action("noMore", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
        isTemplate.set(true);
    }

    public void deleteOrderTemplate(OrderDTO orderDTO){
        callApi(HttpHelper.getInstance().getApi()
                .deleteOrdersTemplate(orderDTO.getOrderTemplateId())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<BaseResponse>() {
                    @Override
                    public void onSuccess(BaseResponse response) {
                        try {
                            ((List) Objects.requireNonNull(baseModels.get())).remove(orderDTO.index-1);
                            view.action("updateTemplate", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {

                    }
                }));
    }

    public void editOrderTemplate(OrderDTO orderDTO){
        callApi(HttpHelper.getInstance().getApi()
                .editOrdersTemplate(orderDTO.getOrderTemplateId(), orderDTO.getOrderName())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<OrderDTO>() {
                    @Override
                    public void onSuccess(OrderDTO response) {
                        try {
                            ((List)baseModels.get()).set(orderDTO.index-1, orderDTO);
                            view.action("updateTemplate", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFailure(String msg, int code) {

                    }
                }));
    }

    public void getOrderDetail(Long orderId) {
        OrderDetailDTO detail = OrderDetailDTO.builder().orderId(orderId)
                .langId(AppController.languageId)
                .build();
        callApi(HttpHelper.getInstance().getApi().getOrderDetails(detail)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<OrderDetailDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<OrderDetailDTO> response) {
                        if (TsUtils.isNotNull(response.getArrData()) && TsUtils.isNotNull(response.getArrData().get(0).orderPackings)) {
                            for (OrderPackingDTO packingDTO : response.getArrData().get(0).orderPackings) {
                                packingDTO.setOrderPackingId(null);
                            }
                            calOrder(response.getArrData().get(0).orderPackings);
                        }

                    }

                }));
        // TODO: 7/24/2019 Call API sua thong tin don hang

    }

    private void calOrder(List<OrderPackingDTO> orderPackings) {
        OrderProductCartDTO orderDTO = new OrderProductCartDTO();
        if (AppController.getMerchantId() == null) {
            showMessage(R.string.error_client_not_found, "");
        }
        orderDTO.setMerchantId(AppController.getMerchantId());
        orderDTO.setIsSaveOrder(0);
        orderDTO.setOrderPackings(orderPackings);
        callApiOrder(orderDTO);

    }

    private void callApiOrder(OrderProductCartDTO orderDTO) {
        callApi(HttpHelper.getInstance().getApi().getListProductCartOrder(orderDTO)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<OrderProductCartDTO>() {
                    @Override
                    public void onSuccess(OrderProductCartDTO productCartDTO) {
                        try {
                            orderProductCartDTO = productCartDTO;
                            view.action("calOrderSuccess", null, OrderVM.this, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        try {
                            view.action("calOrderFail", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    public void getContacts(Context context) {
        contacts = new HashMap<>();
        ContentResolver cr = context.getContentResolver();
        // Get the Cursor of all the contacts
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        // Move the cursor to first. Also check whether the cursor is empty or not.
        assert cursor != null;
        if (cursor.moveToFirst()) {
            // Iterate through the cursor
            do {
                // Get the contacts name
                MerchantDto contact = new MerchantDto();
                contact.setMerchantName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                contact.setMobilePhone(number);
                contacts.put(contact.getMobilePhone(), contact);
            } while (cursor.moveToNext());
        }
        cursor.close();

    }

    private void getOrdersSuccess(ServiceResponse<OrderDTO> orders) {
        if (orders != null) {
            totalPage = orders.getTotalPages();
            if (contacts != null && !contacts.isEmpty()) {
                for (OrderDTO orderDTO : orders.getArrData()) {
                    if (!StringUtils.isNullOrEmpty(orderDTO.merchantCode)) {
                        if (contacts.get(orderDTO.merchantCode) != null) {
                            orderDTO.setMerchantName(contacts.get(orderDTO.getMerchantCode()).getMerchantName());
                        }
                    }
                }
            }
            addNewPage(orders.getArrData(), orders.getTotalPages());
            baseModels.notifyChange();
            recordCount.set(getCount());
            try {
                view.action("listOrder", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }

    public void onRefresh() {
        currentPage = 0;
        totalPage = 0;
        getBaseModelsE().clear();
        getOrderFromApi();
    }

    public void onRefreshTemplateOrder() {
        currentPage = 0;
        totalPage = 0;
        getBaseModelsE().clear();
        getOrderTemplate();
    }

    public void getOrder(int orderType) {
        if (orderType == -1) {
            return;
        }
        MerchantDto merchantDto = (MerchantDto) AppController.getInstance().getFromCache(AppController.MERCHANT);
        if (merchantDto == null) {
            appException.setValue(new AppException(R.string.merchantNotExist, ""));
            return;
        }
        orderDTO = OrderDTO.builder().orderType(1).langId(AppController.languageId);
        if (typeList.get() == 1) {
            switch (currentTab) {
                case 0: // đặt hàng
                    orderDTO.orderStatus(0);
                    break;
                case 1: // dang giao
                    orderDTO.orderStatus(1);
                    break;
                case 2: // da nhan
                    orderDTO.orderStatus(2);
                    break;
                case 3: // đã hủy
                    orderDTO.orderStatus(-1);
                    break;
            }
        } else if (typeList.get() == 2) {
            switch (currentTab) {
                case 0: // Chờ duyệt
                    orderDTO.orderStatus(0);
                    break;
                case 1: // Đã duyệt
                    orderDTO.orderStatus(1);
                    break;
                case 2: // đã hủy
                    orderDTO.orderStatus(-1);
            }
        }

        if (orderType == 2) {
            orderDTO.parentMerchantId(merchantDto.getMerchantId());
        } else {
            orderDTO.merchantId(merchantDto.getMerchantId());
        }
        orderDTO.incentiveAmount(null);
        getOrderFromApi();
    }

    public void openDial(String numberPhone) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + numberPhone));
            mApp.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
