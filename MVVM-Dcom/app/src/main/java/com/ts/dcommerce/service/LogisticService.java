package com.ts.dcommerce.service;


import com.ts.dcommerce.model.dto.stockAvailable.CheckInventoryDTO;

import io.reactivex.Flowable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LogisticService {
    //----------------------------Product------------------------
    @POST("/merchant-orders/get-inventory")
    Flowable<CheckInventoryDTO> checkAvailable(@Body CheckInventoryDTO dto);
}
