package com.ts.dcommerce.model;

import com.tsolution.base.BaseModel;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Language extends BaseModel {
    private Integer languageId;
    private String code;
    private String name;

    public Language(Integer languageId, String code, String name) {
        this.languageId = languageId;
        this.code = code;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
