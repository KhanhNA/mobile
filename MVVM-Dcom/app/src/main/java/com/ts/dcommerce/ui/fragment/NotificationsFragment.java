package com.ts.dcommerce.ui.fragment;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.model.NotificationAction;
import com.ts.dcommerce.model.dto.NotificationMerchantDTO;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.model.dto.SendEventDTO;
import com.ts.dcommerce.ui.GrouponDetailActivity;
import com.ts.dcommerce.ui.MainActivity;
import com.ts.dcommerce.viewmodel.NotificationVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import org.greenrobot.eventbus.EventBus;

public class NotificationsFragment extends BaseFragment {
    private XRecyclerView rcNotification;
    private BaseAdapterV2 notificationAdapter;
    private NotificationVM notificationVM;
    private boolean isLoad = true;
    private AppCompatSpinner spNotification;
    private int check = 0;

    @Override
    public void onResume() {
        super.onResume();
        if (MainActivity.isLoadNotification || isLoad) {
            MainActivity.isLoadNotification = false;
            isLoad = false;
            notificationVM.setTypeId(0);
            notificationVM.refresh();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = binding.getRoot();
        notificationVM = (NotificationVM) viewModel;
        initView(v);
        return v;
    }

    private void initView(View v) {
        rcNotification = v.findViewById(R.id.rcNotification);
        notificationAdapter = new BaseAdapterV2(R.layout.item_notification, notificationVM, this);
        notificationAdapter.setConfigXRecycler(rcNotification, 1);
        rcNotification.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                notificationAdapter.notifyDataSetChanged();
                notificationVM.refresh();
            }

            @Override
            public void onLoadMore() {
                notificationVM.getMoreNoti();
            }
        });
        rcNotification.setAdapter(notificationAdapter);

        spNotification = v.findViewById(R.id.spNotification);
        spNotification.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (++check > 1) {
                    Log.d("test_poss", "" + i);
                    notificationVM.setTypeId(i);
                    notificationVM.refresh();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onItemClick(View v, Object o) {
        NotificationMerchantDTO dto = (NotificationMerchantDTO) o;
        switch (v.getId()) {
            case R.id.layoutRoot:
                try {
                    if (dto.getIsSeen() == 0) {
                        dto.setIsSeen(1);
                        ((NotificationMerchantDTO) notificationVM.getBaseModelsE().get(dto.index - 1)).setIsSeen(1);
                        rcNotification.notifyItemChanged(dto.index - 1);
                        notificationVM.update(dto);
                    }
                    sendIntent(dto);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    private void sendIntent(NotificationMerchantDTO dto) {
        Intent i;
        if (dto.getTypeId() == NotificationAction.ORDER.getValue()) {
            OrderDTO orderDTO = OrderDTO.builder()
                    .orderId(dto.getActionId())
                    .orderType(1)
                    .build();
            i = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            i.putExtra("FRAGMENT", OrderDetailFragment.class);
            bundle.putSerializable("BASE_MODEL", orderDTO);
            i.putExtras(bundle);
            getBaseActivity().startActivity(i);
        } else if (dto.getTypeId() == NotificationAction.GROUPON.getValue()) {
            i = new Intent(getBaseActivity(), GrouponDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putLong("actionId", dto.getActionId());
            i.putExtras(bundle);
            getBaseActivity().startActivity(i);
        } else if (dto.getTypeId() == NotificationAction.INVITE.getValue()) {
            EventBus.getDefault().post(SendEventDTO.builder().position(2).build());
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "getListSuccess":
                rcNotification.loadMoreComplete();
                rcNotification.refreshComplete();
                notificationAdapter.notifyDataSetChanged();
                break;
            case "noMore":
                rcNotification.setNoMore(true);
                break;
            case "updateSuccess":
                rcNotification.setNoMore(true);
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_notifications;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


}
