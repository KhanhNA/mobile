package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;

import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.dto.GrouponRegisterDetailDTO;
import com.ts.dcommerce.model.dto.GrouponRegistryDTO;
import com.ts.dcommerce.model.dto.GrouponRegistryStatusDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrouponDetailVM extends BaseViewModel<GrouponRegistryDTO> {
    private ObservableField<GrouponRegistryStatusDTO> registryStatusDTO;
    private ObservableField<String[]> arrGroupon = new ObservableField<>();
    private List<Long> arrRegisterId = new ArrayList<>();
    private int currentPage = 0;
    private int totalPage = 0;

    public GrouponDetailVM(@NonNull Application application) {
        super(application);
        registryStatusDTO = new ObservableField<>();
        registryStatusDTO.set(new GrouponRegistryStatusDTO());
        registryStatusDTO.get();

    }

    public void getGrouponStatus(Long registerId) {
        callApi(HttpHelper.getInstance().getApi().getRegisterStatus(registerId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<GrouponRegistryStatusDTO>() {
                    @Override
                    public void onSuccess(GrouponRegistryStatusDTO response) {
                        getGrouponStatusSuccess(response);
                    }

                }));
    }

    public void getListGroupon(Long parentMerchantId) {
        callApi(HttpHelper.getInstance().getApi().listGrouponProgram(parentMerchantId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<GrouponRegisterDetailDTO>>() {
                    @Override
                    public void onSuccess(List<GrouponRegisterDetailDTO> response) {
                        if (TsUtils.isNotNull(response)) {
                            List<String> arr = new ArrayList<>();
//                            arrGroupon.add(AppController.getInstance().getString(R.string.option_program));
//                            arrRegisterId.add(-1L);
                            for (GrouponRegisterDetailDTO dto : response) {
                                if (StringUtils.isNotNullAndNotEmpty(dto.getPromotionName())) {
                                    arr.add(dto.getPromotionName());
                                    arrRegisterId.add(dto.getId());
                                }
                            }
                            if(TsUtils.isNotNull(arr))
                            {
                                arrGroupon.set(arr.toArray(new String[arr.size()]));
                            }
                            try {
                                view.action("getListGrouponSuccess", null, null, null);
                            } catch (AppException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }));
    }

    public String getIncentiveAmount() {
        if (registryStatusDTO.get() != null) {
            return "";
        }
        return "Hoa hồng: " + registryStatusDTO.get().getIncentiveAmountNext();
    }

    public String getIncentivePercent() {
        if (registryStatusDTO.get() != null) {
            return "";
        }
        return "Chiết khấu: " + registryStatusDTO.get().getIncentivePercentNext();
    }

    public void getListRegistry(Long registerId, int page) {
        callApi(HttpHelper.getInstance().getApi().getListRegistry(registerId, page)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<GrouponRegistryDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<GrouponRegistryDTO> response) {
                        getListRegistrySuccess(response);
                    }
                }));
    }

    private void getListRegistrySuccess(ServiceResponse<GrouponRegistryDTO> o) {
        if (o != null) {
            setBaseModelsE(o.getArrData());
            try {
                view.action("getListRegistry", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else {
            noMore();
        }
    }

    private void getGrouponStatusSuccess(GrouponRegistryStatusDTO o) {
        if (o != null) {
            registryStatusDTO.set(o);
            try {
                view.action("getGrouponStatus", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }

    public void refresh(Long parentMerchantId) {
        arrRegisterId = new ArrayList<>();
        getBaseModelsE().clear();
        getListGroupon(parentMerchantId);
    }

    public void getMoreListRegistry(long id) {
        if (totalPage > 0 && currentPage < totalPage) {
            currentPage += 1;
            getListRegistry(id, currentPage);
        } else {
            noMore();
        }
    }

    private void noMore() {
        try {
            view.action("noMore", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }
}
