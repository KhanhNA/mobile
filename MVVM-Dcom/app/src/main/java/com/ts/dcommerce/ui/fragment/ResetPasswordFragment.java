package com.ts.dcommerce.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ts.dcommerce.R;
import com.ts.dcommerce.viewmodel.LoginVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class ResetPasswordFragment extends BaseFragment {
    private LoginVM loginVM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        loginVM = (LoginVM) viewModel;
        assert view != null;
        initView(view);
        return view;
    }


    private void initView(View view) {
        setupToolbar(view);
    }
    private void setupToolbar(View v){
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(R.string.enter_phone_number);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_reset_passwod;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
