package com.ts.dcommerce.model.dto.stockAvailable;

import com.ts.dcommerce.model.InventoryPacking;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckInventoryDTO {
    private String destinationStore;
    private List<String> stores;
    private List<InventoryPacking> merchantOrderInventoryDetails;
}
