package com.ts.dcommerce.ui.fragment;

import android.animation.Animator;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.BR;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.model.Category;
import com.ts.dcommerce.model.ProductDetail;
import com.ts.dcommerce.model.dto.ProductDto;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.ui.ProductDetailActivity;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.CategoriesDetailVM;
import com.ts.dcommerce.viewmodel.CategoriesVM;
import com.ts.dcommerce.viewmodel.ProductDetailVM;
import com.ts.dcommerce.widget.BottomSheetAddToCart;
import com.ts.dcommerce.widget.CircleAnimationUtil;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.List;

import lombok.Setter;

@Setter
public class CategoriesDetailFragment extends BaseFragment {
    CategoriesVM categoriesVM;
    public static boolean isChangeProduct = false;
    CategoriesDetailVM categoriesDetailVM;
    //    BaseAdapter productAdapter;
    ProductDto productDto;
    Category category;
    FrameLayout frCart;
    XRecyclerView rcProducts;
    RecyclerView rcSubCategories;
    private BottomSheetAddToCart sheetBehavior;
    private ProductDetailVM productDetailVM;
    private int currentPos = 0;
    private BaseAdapterV2 gridProductAdapter, subCategoriesAdapter;

    public CategoriesDetailFragment() {
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isChangeProduct) {
            categoriesDetailVM.setUpdateAdappType(0);
            categoriesDetailVM.setOrderedProduct((List<ProductDto>) categoriesDetailVM.arrProductCategories.getBaseModelsE());
            isChangeProduct = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        categoriesVM = ViewModelProviders.of(getParentFragment()).get(CategoriesVM.class);
        categoriesDetailVM = (CategoriesDetailVM) viewModel;
        try {
            init(binding.getRoot());
        } catch (Exception e) {
            e.printStackTrace();
        }


//
        return binding.getRoot();
    }

    private void initSubCategories(View v) {
        categoriesDetailVM.getSubCategories(category);
        View subCate = LayoutInflater.from(getBaseActivity()).inflate(R.layout.layout_sub_categories, v.findViewById(android.R.id.content), false);
        rcSubCategories = subCate.findViewById(R.id.rcSubCategories);

        subCategoriesAdapter = new BaseAdapterV2(R.layout.item_sub_categories, categoriesDetailVM.arrSubCategories, this);
        GridLayoutManager layoutManager =
                new GridLayoutManager(getBaseActivity(), 2, GridLayoutManager.HORIZONTAL, false);
        rcSubCategories.setLayoutManager(layoutManager);
        rcSubCategories.setAdapter(subCategoriesAdapter);

        rcProducts.addHeaderView(subCate);
        //

    }


    private void init(View v) {
        rcProducts = v.findViewById(R.id.rcProducts);
        gridProductAdapter = new BaseAdapterV2(R.layout.item_product, categoriesDetailVM.arrProductCategories, this);
        gridProductAdapter.setConfigXRecycler(rcProducts, 2);
        rcProducts.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                if (category != null) {
                    gridProductAdapter.notifyDataSetChanged();
                    categoriesDetailVM.onRefresh(category);
                }

            }

            @Override
            public void onLoadMore() {
                if (category != null) {
                    categoriesDetailVM.getMoreProduct(category);
                }
            }
        });
        rcProducts.setAdapter(gridProductAdapter);
        //
        frCart = getParentFragment().getView().findViewById(R.id.frCart);

        productDetailVM = ViewModelProviders.of(this).get(ProductDetailVM.class);
        sheetBehavior = new BottomSheetAddToCart(getBaseActivity(), productDetailVM, R.id.rcPackingDialog);
        ViewDataBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(getBaseActivity()), R.layout.bottom_sheet_quantity_product, null, false);
        mBinding.setVariable(BR.viewHoler2, productDetailVM);
        productDetailVM.setView(this::processFromVM);
        Bundle arguments = getArguments();
        if (arguments != null) {
            currentPos = arguments.getInt("position", -1);
            if (currentPos == -1) {
                return;
            }
            category = categoriesVM.getCategoryList().get(currentPos);
            categoriesDetailVM.getProducts(category);
            initSubCategories(v);
        }
    }

    @Override
    public void onItemClick(View view, Object baseModel) {
        Bundle bundle;
        Intent intent;
        switch (view.getId()) {
            case R.id.item_cate:
                bundle = new Bundle();
                intent = new Intent(getBaseActivity(), CommonActivity.class);
                intent.putExtra("FRAGMENT", CategoriesFragment.class);
                intent.putExtra("currentCate", ((Category) baseModel).index - 1);
                bundle.putSerializable("Category", category);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.imgProduct:
                bundle = new Bundle();
                bundle.putLong("productId", ((ProductDto) baseModel).getProductId());
                bundle.putLong("packingProductId", ((ProductDto) baseModel).getPackingProductId());
                intent = new Intent(getActivity(), ProductDetailActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.frCart:
                productDto = ProductDto.builder().build();
                productDto = (ProductDto) baseModel;
                //reset value ProductVM
                productDetailVM.getQuantity().set(1);
                productDetailVM.getIndexPacking().set(0);
                try {
                    // set object Product --> ProductDetail
                    ProductDetail productDetail = TsUtils.getData(productDto, ProductDetail.class, null);
                    productDetailVM.getProductDetail().set(productDetail);
                    productDetailVM.arrPackingProduct.setData(productDetail.getPackingProductList());
                    //auto select first packing
                    ProductPackingDto productPackingDto = productDetail.getPackingProductList().get(0);
                    productPackingDto.index = 1;
                    sheetBehavior.onPackingClick(null, productPackingDto);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                sheetBehavior.show(getFragmentManager(), sheetBehavior.getTag());
                break;
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case "getProductSuccess":
                rcProducts.refreshComplete();
                rcProducts.loadMoreComplete();
                gridProductAdapter.notifyDataSetChanged();
                break;
            case "addToCart": {
                makeFlyAnimation(view, frCart);
                Toast.makeText(getBaseActivity(), getString(R.string.ADD_CART), Toast.LENGTH_SHORT).show();
                sheetBehavior.dismiss();
                categoriesDetailVM.setOrderedProduct((List<ProductDto>) categoriesDetailVM.arrProductCategories.getBaseModelsE(), productDto.index - 1);
                rcProducts.notifyItemChanged(productDto.index - 1);
                productDto = ProductDto.builder().build();

            }
            case "noMore":
                rcProducts.setNoMore(true);
                break;
            case "getSubCategoriesSuccess":
                subCategoriesAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_categories_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CategoriesDetailVM.class;
    }

    private void makeFlyAnimation(View fromView, View targetView) {

        new CircleAnimationUtil().attachActivity(getActivity()).setTargetView(fromView).setMoveDuration(500).setDestView(targetView).setAnimationListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                categoriesVM.getTotalProduct();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();


    }

}
