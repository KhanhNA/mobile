package com.ts.dcommerce.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Contact implements Serializable {
    private String urlImg;
    private String contact_name;
    private int contact_poit;
    private String contact_phone;
    private String contact_email;
    private int status;
    private boolean isRegister;


    public Contact(String urlImg, String contact_name, int contact_poit, String contact_phone, String email, int status) {
        this.urlImg = urlImg;
        this.contact_name = contact_name;
        this.contact_poit = contact_poit;
        this.contact_phone = contact_phone;
        this.contact_email = contact_email;
        this.contact_email = email;
        this.status = status;
    }

    public Contact(){

    }


}
