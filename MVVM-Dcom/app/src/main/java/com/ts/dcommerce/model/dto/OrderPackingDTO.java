package com.ts.dcommerce.model.dto;


import com.ts.dcommerce.base.AppController;
import com.tsolution.base.BaseModel;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderPackingDTO extends BaseModel implements Serializable {
    public static final int INCENTIVE = 1;
    public static final int SALE = 0;
    private Long orderPackingId;
    private Integer quantity;
    private Double available;
    private Long incentiveProgramId;
    private Double price;
    private Double vat;
    private Long packingProductId;
    private String packingProductCode;
    private Double amount;
    private Long productId;
    private Double incentiveAmount = (double) 0;

    private Double remainQuantity = (double) 0;
    private Double remainAmount = (double) 0;

    private Double incentiveTotal = (double) 0;
//    private Integer isIncentive;
    private double orderPercent;//% dong gop vao don hang

    private Integer quantityPacking;
    private String packingName;
    private String packingUrl;
    private int isDefault;
    private int availableQuantity;

    public int getAvailable() {
        return this.available == null ? 0 : this.available.intValue();
    }
//    public int getQuantity(){
//        return this.quantity == null ? 0 : this.quantity.intValue();
//    }
//
//    public void setQuantity(Double quantity) {
//        this.quantity = quantity;
//    }

    public String getPriceStr() {
        return price == null ? "" : AppController.getInstance().formatCurrency(price);
    }


    public String getIncentiveAmountStr() {
        return AppController.getInstance().formatCurrency(amount);
    }

    public String getTotalPrice() {
        return price == null || quantity == null ? "" : AppController.getInstance().formatCurrency(price * quantity);
    }

}
