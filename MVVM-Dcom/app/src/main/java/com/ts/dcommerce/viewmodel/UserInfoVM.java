package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.UserInfo;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.model.user.Menu;
import com.ts.dcommerce.model.user.Permission;
import com.ts.dcommerce.model.user.Principal;
import com.ts.dcommerce.model.user.Role;
import com.ts.dcommerce.model.user.User;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.ApiResponse;
import com.ts.dcommerce.util.IntentConstants;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.RetrofitClient;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter

public class UserInfoVM extends BaseViewModel<UserInfo> {
    private int countThread = 0;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();

    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }

    public UserInfoVM(@NonNull Application application) {
        super(application);
    }

    public void inputCode(String inputCode, String imei) {
        callApi(HttpHelper.getInstance().getApi().inputCode(inputCode, imei)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<MerchantDto>() {
                    @Override
                    public void onSuccess(MerchantDto merchantDto) {
                        try {
                            view.action(IntentConstants.INTENT_INPUT_SUCCES, null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String msg, int code) {
                        try {
                            view.action(IntentConstants.INTENT_INPUT_FAIL, null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    private void getRole() {
//        RetrofitClient.getAuthClient().create(SOService.class).getUser().enqueue(callBack(this::getRoleSuccess, User.class, null));
//        SOService service = RetrofitClient.getAuthClient().create(SOService.class);
//        Disposable disposable = service.getUser()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(this::handleResponse, this::handleError, this::handleSuccess);
//
//        mCompositeDisposable.add(disposable);

    }

    public void login() {
        UserInfo userInfo = model.get();
        if (userInfo != null) {
            RetrofitClient.requestLogin(appException, userInfo.getUserName(), userInfo.getPassWord(), this::success);
        }
    }

//    public void loginAfterRegister() {
//        UserInfo userInfo = new UserInfo();
//        userInfo.setUserName("phambien");
//        userInfo.setPassWord("abc@123");
//        setModelE(userInfo);
//        RetrofitClient.requestLogin(appException, "phambien", "abc@123", this::success);
//    }

    private synchronized void threadFinish() {
        try {
            countThread += 1;
            if (countThread == 2) {
                processing.setValue(null);
                view.action(IntentConstants.INTENT_START_MAIN, null, null, null);
            }

        } catch (Throwable e) {
            appException.postValue(e);
            processing.postValue(null);
        }
    }

    private void getRoleSuccess(Object body) {
        if (body != null) {
            User u = (User) body;
            Principal principal = u.getUserAuthentication().getPrincipal();
            List<Menu> menus = TsUtils.safe(principal.getMenus());
            List<Role> roles = TsUtils.safe(principal.getRoles());
            List<Permission> permissions;
            AppController.authens.clear();
            for (Menu menu : menus) {
                AppController.authens.put(menu.getCode(), menu);
            }
            for (Role role : roles) {
                menus = TsUtils.safe(role.getMenus());
                for (Menu menu : menus) {
                    AppController.authens.put(menu.getCode(), menu);
                }
                permissions = TsUtils.safe(role.getPermissions());
                for (Permission permission : permissions) {
                    AppController.authens.put(permission.getUrl(), permission);
                }
            }

        }
        threadFinish();
    }

    private void success(Call<Object> call, Response<Object> response, Object body, Throwable throwable) {

//        SharedPreferences.Editor editor = AppController.getInstance().getEditor();
//        editor.putString(USER_NAME, getModelE().getUserName());
//        if (isSave.get() != null && isSave.get()) {
//            editor.putString(MK, getModelE().getPassWord());
//            editor.putBoolean("chkSave", true);
//        } else {
//            editor.putBoolean("chkSave", false);
//            editor.putString(MK, "");
//        }
        if (body != null) {
            //Sucess
            Gson gson = new Gson();

            String json = gson.toJson(getModelE());

            SharedPreferences.Editor editor = AppController.getInstance().getEditor();
            editor.putString("UserInfo", json);
            editor.commit();

            countThread = 0;
            getRole();

            MerchantDto dto = new MerchantDto();
            dto.setUserName(getModelE().getUserName());

//            AppController.getInstance().getSOService().getMerchants(dto, 0, null).enqueue(callBack(this::merchantSuccess));
        } else {//Fail
            try {
                view.action("LoginFail", null, null, null);
//                appException.postValue(throwable);
            } catch (Throwable e) {
                appException.postValue(e);
                processing.postValue(null);
            }

        }

    }

    private void merchantSuccess(Call<Object> call, Response<Object> response, Object body, Throwable throwable) {
        ServiceResponse<MerchantDto> dtos = (ServiceResponse<MerchantDto>) body;
        if (dtos != null && dtos.getArrData() != null && dtos.getArrData().size() > 0) {
            try {
                AppController.getInstance().putCatche(AppController.MERCHANT, dtos.getArrData().get(0));
                view.action(IntentConstants.INTENT_START_MAIN, null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
        //        if (dtos == null || dtos.getArrData() == null || dtos.getArrData().size() == 0) {
//            appException.setValue(new AppException(R.string.merchantNotExist, getModelE().getUserName()));
//            processing.postValue(null);
//            return;
//        }
//        AppController.getInstance().saveObject(AppController.TOKEN,RetrofitClient.TOKEN);
//        threadFinish();

    }

    public void saveModel() {
        UserInfo user = getModelE();

        Gson gson = new Gson();
        String json = gson.toJson(user);
        SharedPreferences.Editor editor = AppController.getInstance().getEditor();
        editor.putString("UserInfo", json);
        editor.commit();
    }

}
