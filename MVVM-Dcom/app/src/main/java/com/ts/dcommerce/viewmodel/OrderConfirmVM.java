package com.ts.dcommerce.viewmodel;

import android.app.Application;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;

import com.ts.dcommerce.model.ShipMethod;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.model.dto.OrderProductCartDTO;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderConfirmVM extends BaseViewModel {
    private OrderProductCartDTO orderProductCartDTO;
    private MerchantAddressDto merchantAddressDto;
    public BaseViewModel arrOrderProduct;
    public BaseViewModel incentiveProduct;

    private ObservableField<Boolean> swipeable = new ObservableField<>(true);

    public OrderConfirmVM(@NonNull Application application) {
        super(application);
        arrOrderProduct = new BaseViewModel(application);
        incentiveProduct = new BaseViewModel(application);
    }

    public void changeAddress(MerchantAddressDto addressDto) {
        merchantAddressDto = addressDto;
        if (addressDto.getLatitude() != null) {
            orderProductCartDTO.setRecvLat(addressDto.getLatitude());
        }
        if (addressDto.getLongitude() != null) {
            orderProductCartDTO.setRecvLong(addressDto.getLongitude());
        }
        if (StringUtils.isNotNullAndNotEmpty(addressDto.getAddress())) {
            orderProductCartDTO.setRecvAddr(addressDto.getAddress());
        }
        if (addressDto.getId() != null) {
            orderProductCartDTO.setMerchantAddressId(addressDto.getId());
        }

    }

    public void prepareData() {
        if (orderProductCartDTO.getOrderPackings() != null) {
            List<OrderPackingDTO> product = new ArrayList<>();
            List<OrderPackingDTO> productIncentive = new ArrayList<>();
            // San pham dat hang
            if (TsUtils.isNotNull(orderProductCartDTO.getOrderPackings())) {
                product.addAll(orderProductCartDTO.getOrderPackings());
            }
            // San pham khuyen mai
            if (TsUtils.isNotNull(orderProductCartDTO.getPromotionPacking())) {
                productIncentive.addAll(orderProductCartDTO.getPromotionPacking());
            }
            arrOrderProduct.setData(product);
            incentiveProduct.setData(productIncentive);
        }
    }

    public void resetAddress() {
        merchantAddressDto = null;
        orderProductCartDTO.setRecvLat(null);
        orderProductCartDTO.setRecvLong(null);
        orderProductCartDTO.setRecvAddr(null);
        orderProductCartDTO.setMerchantAddressId(null);
    }

//    public void changeShipMethod(ShipMethod shipMethod) {
//        orderProductCartDTO.setShipMethodId(shipMethod.getShipMethodId());
//    }

    public void setWareHouse(int selectedWareHousePositon) {
        orderProductCartDTO.setRecvStoreId(Long.valueOf(selectedWareHousePositon));
    }

    public void resetShipMethod() {
        orderProductCartDTO.setShipMethodId(null);
    }

}
