package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.annotation.NonNull;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.NotificationAction;
import com.ts.dcommerce.model.dto.NotificationMerchantDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationVM extends BaseViewModel {
    private int totalPage;
    private int currentPage;
    private int typeId = 0;

    public NotificationVM(@NonNull Application application) {
        super(application);
        baseModels.set(new ArrayList<>());
    }

    public void getList(int page, boolean isClear) {
        callApi(HttpHelper.getInstance().getApi().getListNotification(AppController.getMerchantId(), typeId, page)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<NotificationMerchantDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<NotificationMerchantDTO> response) {
                        getListNotiSuccess(response, isClear);
                    }
                }));
    }

    public void getMoreNoti() {
        if (totalPage > 0 && currentPage < totalPage) {
            currentPage += 1;
            getList(currentPage, false);
        } else {
            noMore();
        }
    }

    private void noMore() {
        try {
            view.action("noMore", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void getListNotiSuccess(ServiceResponse<NotificationMerchantDTO> response, boolean isClear) {
        if (response != null && TsUtils.isNotNull(response.getArrData())) {
            try {
                for (NotificationMerchantDTO dto : response.getArrData()) {
                    if (dto.getTypeId() == NotificationAction.ORDER.getValue()) {
                        dto.setImg(R.drawable.bag);
                    } else if (dto.getTypeId() == NotificationAction.GROUPON.getValue()) {
                        dto.setImg(R.drawable.group_icon);
                    } else if (dto.getTypeId() == NotificationAction.INVITE.getValue()) {
                        dto.setImg(R.drawable.send);
                    }
                }
                setData(response.getArrData(), isClear);
                totalPage = response.getTotalPages();
                view.action("getListSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else {
            noMore();
        }
    }

    public void refresh() {
        currentPage = 0;
        getList(currentPage, true);
    }

    public void update(NotificationMerchantDTO dto) {
        callApi(HttpHelper.getInstance().getApi().updateNotifi(dto)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<NotificationMerchantDTO>() {
                    @Override
                    public void onSuccess(NotificationMerchantDTO response) {
                        if (response != null) {
                            try {
                                view.action("updateSuccess", null, null, null);
                            } catch (AppException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }));
    }
}
