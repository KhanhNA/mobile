package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.ChangingPassword;
import com.ts.dcommerce.model.dto.TokenFbMerchantDTO;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import lombok.Getter;
import lombok.Setter;

import static com.ts.dcommerce.util.StringUtils.isNullOrEmpty;

@Setter
@Getter
public class SettingVM extends BaseViewModel {
    ObservableField<ChangingPassword> changingPassword = new ObservableField<>();
    ObservableBoolean loadingVisible = new ObservableBoolean(false);

    public SettingVM(@NonNull Application application) {
        super(application);
        changingPassword.set(new ChangingPassword());
    }

    public void changePass() {
        if (isValid()) {
            loadingVisible.set(true);
            callApi(HttpHelper.getInstance().getApi().changePassword(changingPassword.get())
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<ChangingPassword>() {
                        @Override
                        public void onSuccess(ChangingPassword categoryServiceResponse) {
                            changePasswordSuccess(categoryServiceResponse);
                        }

                        @Override
                        public void onFailure(String msg, int code) {
                            addError("oldPass", R.string.INCORRECT_CURRENT_PASSWORD, true);
                            loadingVisible.set(false);
                        }
                    }));

        }
    }

    public void deleteToken(TokenFbMerchantDTO fbMerchantDTO) {
        callApi(HttpHelper.getInstance().getApi().deleteToken(fbMerchantDTO)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<TokenFbMerchantDTO>() {
                    @Override
                    public void onSuccess(TokenFbMerchantDTO categoryServiceResponse) {
                    }
                }));

    }

    private void changePasswordSuccess(Object o) {
        if (o != null) {
            clearErro("oldPass");
            try {
                view.action("changePasswordSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
        loadingVisible.set(false);
    }

    private boolean isValid() {
        ChangingPassword temp = changingPassword.get();
        boolean isValid = true;
        if (temp != null) {
            if (isNullOrEmpty(temp.getOldPassword())) {
                isValid = false;
                addError("oldPass", R.string.INVALID_FIELD, true);
            } else {
                clearErro("oldPass");
            }

            if (isNullOrEmpty(temp.getNewPassword())) {
                isValid = false;
                addError("newPass", R.string.INVALID_FIELD, true);
            } else if (isTheSamePassword(temp.getOldPassword(), temp.getNewPassword())) {
                isValid = false;
                addError("newPass", R.string.SAME_PASSWORD, true);
            } else {
                clearErro("newPass");
            }

            if (!isTheSamePassword(temp.getNewPassword(), temp.getNewConfirmPassword())) {
                isValid = false;
                addError("confirmPass", R.string.INCORRECT_PASSWORD, true);
            } else {
                clearErro("confirmPass");
            }
        }
        return isValid;
    }

    private boolean isTheSamePassword(String pass1, String pass2) {
        return !isNullOrEmpty(pass1) && !isNullOrEmpty(pass2) && pass1.equals(pass2);
    }

    public void deleteTokenFireBase() {

    }
}
