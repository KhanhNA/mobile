package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.databinding.ObservableBoolean;
import androidx.annotation.NonNull;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.dto.KpiMonth;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class KpiChartVM extends BaseViewModel<KpiMonth> {
    List<KpiMonth> arrKpi;

    public KpiChartVM(@NonNull Application application) {
        super(application);
        arrKpi = new ArrayList<>();
    }

    public void getKpi() {
        // TODO: 9/21/2019 set month
        KpiMonth kpiMonth = new KpiMonth();
        kpiMonth.setMerchantId(AppController.getMerchantId());
        callApi(HttpHelper.getInstance().getApi().getKpiMonths(kpiMonth)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<KpiMonth>>() {
                    @Override
                    public void onSuccess(List<KpiMonth> kpiMonths) {
                        getKpiSuccess(kpiMonths);
                    }
                }));
    }

    private void getKpiSuccess(Object o) {
        if (o != null) {
            try {
                arrKpi = (List<KpiMonth>) o;
                view.action("getKpiSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }
}
