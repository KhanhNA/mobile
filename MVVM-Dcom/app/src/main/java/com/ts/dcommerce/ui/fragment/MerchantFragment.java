package com.ts.dcommerce.ui.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.MerchantPaperAdapter;
import com.ts.dcommerce.viewmodel.MerchantVM;
import com.ts.dcommerce.widget.SlidingTabLayout;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MerchantFragment extends BaseFragment {
    SlidingTabLayout slidingTabs;
    ViewPager vpTab;
    MerchantPaperAdapter myPagerAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        innitView(binding.getRoot());
        return binding.getRoot();
    }

    private void innitView(View v) {
        slidingTabs = v.findViewById(R.id.slidingTab);
        vpTab = v.findViewById(R.id.vpContent);
        slidingTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        slidingTabs.setDistributeEvenly(true);
        myPagerAdapter = new MerchantPaperAdapter(getChildFragmentManager(), getResources().getStringArray(R.array.title_merchant));
        vpTab.setAdapter(myPagerAdapter);
        vpTab.setOffscreenPageLimit(2);
        vpTab.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        slidingTabs.setViewPager(vpTab);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        vpTab.setPageMargin(pageMargin);
        vpTab.setCurrentItem(0);

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_contact;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MerchantVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
