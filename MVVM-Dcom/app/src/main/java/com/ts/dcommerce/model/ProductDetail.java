package com.ts.dcommerce.model;

import com.google.gson.annotations.SerializedName;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProductDetail extends BaseModel {

    @SerializedName("id")
    private Long productId;

    private String productName;

    private String urlImage;

    private Float reviewAvg;

    private Integer reviewCount;

    @SerializedName("productPrice")
    private Double price;

    @SerializedName("productQuantity")
    private Integer quantity;

    private Integer typePromotion;

    private Long manufacturerId;

    private String manufacturerName;

    private String incentiveDescription;

    private String desProduct;

    private String packingProductCode;

    private String sellerName;

    private String[] imageList;

    private Double orgPrice;

    private List<ProductPackingDto> packingProductList;

    public ProductDetail() {
    }

    public ProductDetail(Long productId, String productName, String urlImage, Float reviewAvg, Integer reviewCount, Double price, Integer quantity, Integer typePromotion, Long manufacturerId, String manufacturerName, String incentiveDescription, String desProduct, String packingProductCode, String sellerName, String[] imageList, Double orgPrice, List<ProductPackingDto> packingProductList) {
        this.productId = productId;
        this.productName = productName;
        this.urlImage = urlImage;
        this.reviewAvg = reviewAvg;
        this.reviewCount = reviewCount;
        this.price = price;
        this.quantity = quantity;
        this.typePromotion = typePromotion;
        this.manufacturerId = manufacturerId;
        this.manufacturerName = manufacturerName;
        this.incentiveDescription = incentiveDescription;
        this.desProduct = desProduct;
        this.packingProductCode = packingProductCode;
        this.sellerName = sellerName;
        this.imageList = imageList;
        this.orgPrice = orgPrice;
        this.packingProductList = packingProductList;
    }
}
