package com.ts.dcommerce.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ts.dcommerce.R;
import com.ts.dcommerce.util.StringUtils;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

public class DetailPromotionFragment extends Fragment {
    private View rootView;
    private String des;

    public DetailPromotionFragment() {
    }

    @SuppressLint("ValidFragment")
    public DetailPromotionFragment(String str) {
        this.des = str;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        HtmlTextView txtDes = rootView.findViewById(R.id.txtDes);
        if (!StringUtils.isNullOrEmpty(des)) {
            txtDes.setHtml(des,
                    new HtmlHttpImageGetter(txtDes));
        }
    }
}
