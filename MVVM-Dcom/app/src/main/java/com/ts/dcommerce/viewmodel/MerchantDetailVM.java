package com.ts.dcommerce.viewmodel;

import android.app.Application;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;
import android.util.Log;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.db.ContactDB;
import com.ts.dcommerce.model.db.DaoSession;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.model.dto.OverviewL2DTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.greenrobot.greendao.rx.RxDao;

import lombok.Getter;
import lombok.Setter;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author Good_Boy
 */
@Getter
@Setter
public class MerchantDetailVM extends BaseViewModel<MerchantDto> {
    private RxDao<ContactDB, Void> noteDao;
    private ObservableField<OverviewL2DTO> overViewL2 = new ObservableField<>();
    public BaseViewModel arrOrder;

    public MerchantDetailVM(@NonNull Application application) {
        super(application);
        arrOrder = new BaseViewModel(application);
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        noteDao = daoSession.getContactDBDao().rx();

    }

    public void setMerchant(MerchantDto merchantDto) {
        model.set(merchantDto);
    }
    public void setMerchant(String merchantName) {
        MerchantDto m =  new MerchantDto();
        m.setUserName(merchantName);
        callApi(HttpHelper.getInstance().getApi()
                .getBaseMerchants(m, 0, 0)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<MerchantDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<MerchantDto>  dto) {
                        try {
                            if(dto == null || dto.getArrData() == null || dto.getArrData().size() == 0){
                                return;
                            }
                            MerchantDto dto1 = dto.getArrData().get(0);
                            model.set(dto1);
                            view.action("getMerchant", null, null, null);
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                }));

    }
    public void getInfoL2(Long merchantId) {
        callApi(HttpHelper.getInstance().getApi()
                .overviewL2(merchantId, AppController.getMerchantId())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<OverviewL2DTO>() {
                    @Override
                    public void onSuccess(OverviewL2DTO dto) {
                        overViewL2.set(dto);
                    }

                }));
    }

    public void getOrderL2(Long merchantId) {
        callApi(HttpHelper.getInstance().getApi()
                .getOrderL2(merchantId, AppController.getMerchantId())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<OrderDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<OrderDTO> arr) {
                        if (TsUtils.isNotNull(arr.getArrData())) {
                            arrOrder.setData(arr.getArrData());
                            try {
                                view.action("getOrderL2", null, arrOrder, null);
                            } catch (AppException e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                view.action("noData", null, arrOrder, null);
                            } catch (AppException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }));
    }

    public void saveImg(ContactDB contactDB) {
        noteDao.insertOrReplace(contactDB).observeOn(AndroidSchedulers.mainThread()).subscribe(contactDB1
                -> Log.e("InsertContact", "Thanh cong"));
    }

    public void onRefresh(Long id) {
        getOrderL2(id);
        arrOrder.getBaseModelsE().clear();
    }

}
