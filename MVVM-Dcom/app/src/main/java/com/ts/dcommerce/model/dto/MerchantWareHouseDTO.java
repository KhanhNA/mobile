package com.ts.dcommerce.model.dto;

import androidx.annotation.NonNull;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MerchantWareHouseDTO {
    private Long id;

    private Long merchantId;

    private String storeCode;

    private String storeName;

    private String storeAddress;

    private String city;

    private String phone;

    private String postalCode;

    private Double lat;

    private Double longitude;

    @NonNull
    @Override
    public String toString() {
        return storeName + " - " + storeAddress;
    }
}
