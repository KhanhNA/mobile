package com.ts.dcommerce.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SendEventDTO {
    Integer position;
}
