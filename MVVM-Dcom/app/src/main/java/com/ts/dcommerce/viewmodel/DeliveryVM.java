package com.ts.dcommerce.viewmodel;

import android.app.Application;
import android.text.format.DateUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.InventoryPacking;
import com.ts.dcommerce.model.ShipMethod;
import com.ts.dcommerce.model.dto.MerchantWareHouseDTO;
import com.ts.dcommerce.model.dto.OrderPackingDTO;
import com.ts.dcommerce.model.dto.stockAvailable.CheckInventoryDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliveryVM extends BaseViewModel {
    private OrderConfirmVM orderConfirmVM;
    ObservableBoolean isLoading = new ObservableBoolean();
    ObservableBoolean isStoreShow = new ObservableBoolean();
    private MutableLiveData<Integer> selectedStorePosition = new MutableLiveData<>();
    private ObservableField<List<MerchantWareHouseDTO>> lstStore = new ObservableField<>();
    private ObservableField<String> receiveDate = new ObservableField<>();

    public DeliveryVM(@NonNull Application application) {
        super(application);
        isLoading.set(false);
        isStoreShow.set(false);
        lstStore.set(new ArrayList<>());
    }

    public void init(OrderConfirmVM orderConfirmVM) {
        this.orderConfirmVM = orderConfirmVM;
    }

    public void getStoreOfMerchant() {
        callApi(HttpHelper.getInstance().getApi().getStoreOfMerchant(MerchantWareHouseDTO.builder().merchantId(AppController.getMerchantId()).build(), "MerchantWareHouse", 0)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<MerchantWareHouseDTO>>() {
                    @Override
                    public void onSuccess(ServiceResponse<MerchantWareHouseDTO> stores) {
                        if (TsUtils.isNotNull(stores.getArrData())) {
                            lstStore.set(stores.getArrData());
                            try {
                                view.action("getStoreSuccess", null, null, null);
                            } catch (AppException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }));
    }

    @Override
    public void init() {
        clearData();
    }

    private void clearData() {
        Objects.requireNonNull(lstStore.get()).clear();
        selectedStorePosition.setValue(0);
    }


    public void checkAvailable(CheckInventoryDTO dto) {
        callApi(HttpHelper.getInstance().getLogisticApi().checkAvailable(dto)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<CheckInventoryDTO>() {
                    @Override
                    public void onSuccess(CheckInventoryDTO checkInventoryDTO) {
                        if (TsUtils.isNotNull(checkInventoryDTO.getMerchantOrderInventoryDetails())) {
                            checkAvailableSuccess(checkInventoryDTO);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                }));
    }

    private void checkAvailableSuccess(CheckInventoryDTO checkInventoryDTO) {
        try {
            view.action("checkAvailableSuccess", null, null, null);
            HashMap<String, Integer> hmInventory = new HashMap<>();
            int maxDayToWait = 0;
            for (int i = 0; i < checkInventoryDTO.getMerchantOrderInventoryDetails().size(); i++) {
                InventoryPacking inventoryPacking = checkInventoryDTO.getMerchantOrderInventoryDetails().get(i);
                hmInventory.put(inventoryPacking.getProductPackingCode(), inventoryPacking.getInventory());
                if (maxDayToWait < inventoryPacking.getMaxDayToWait()) {
                    maxDayToWait = inventoryPacking.getMaxDayToWait();
                }
            }
            List<OrderPackingDTO> orderPackingDTOS = (List<OrderPackingDTO>) orderConfirmVM.arrOrderProduct.baseModels.get();
            if (TsUtils.isNotNull(orderPackingDTOS)) {
                for (int j = 0; j < orderPackingDTOS.size(); j++) {
                    OrderPackingDTO packingDTO = orderPackingDTOS.get(j);
                    if (hmInventory.get(packingDTO.getPackingProductCode()) != null) {
                        orderPackingDTOS.get(j).setAvailableQuantity(hmInventory.get(packingDTO.getPackingProductCode()));
                    }
                }
                orderConfirmVM.arrOrderProduct.baseModels.notifyChange();
            }
            if (maxDayToWait > 0) {
                Date dt = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(dt);
                c.add(Calendar.DATE, maxDayToWait);
                dt = c.getTime();
                String dateConvert = StringUtils.dateToString(dt, AppController.YYYY_MM_DD);
                receiveDate.set(dateConvert);
                orderConfirmVM.getOrderProductCartDTO().setDeliveryDate(dateConvert);
            } else {
                String dateConvert = StringUtils.dateToString(new Date(), AppController.YYYY_MM_DD);
                receiveDate.set(dateConvert);
                orderConfirmVM.getOrderProductCartDTO().setDeliveryDate(dateConvert);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

}
