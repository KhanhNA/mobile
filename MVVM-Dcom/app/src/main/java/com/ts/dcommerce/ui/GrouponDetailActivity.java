package com.ts.dcommerce.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.dto.GrouponRegistryStatusDTO;
import com.ts.dcommerce.ui.fragment.CartFragment;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.GrouponDetailVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.Utils.AlertsUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GrouponDetailActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private GrouponDetailVM grouponDetailVM;
    private XRecyclerView rcRegistry;
    private BaseAdapterV2 grouponRegistryAdapter;
    private Toolbar toolbar;
    private Button btnBuyNow;
    private boolean isBuy;
    private int postionSelected;
    private Spinner spGroupon;
    private int check = 0;
    private SwipeRefreshLayout swGroupon;
    private long actionId = -1L;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_groupon_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return GrouponDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        grouponDetailVM = (GrouponDetailVM) viewModel;
        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                actionId = extras.getLong("actionId", -1L);
            }
        }
        initView();
        btnBuyNow.setOnClickListener(view -> {
            // Get info groupon register
            if (TsUtils.isNotNull(grouponDetailVM.getArrRegisterId())) {
                isBuy = true;
                grouponDetailVM.getGrouponStatus(grouponDetailVM.getArrRegisterId().get(postionSelected));
            }

        });
        spGroupon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (++check > 1) {
                    if ( TsUtils.isNotNull(grouponDetailVM.getArrRegisterId())) {
                        btnBuyNow.setVisibility(View.GONE);
                        postionSelected = i;
                        // Get data
                        grouponDetailVM.getGrouponStatus(grouponDetailVM.getArrRegisterId().get(i));
                        grouponDetailVM.getListRegistry(grouponDetailVM.getArrRegisterId().get(i), 0);
                    }
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        grouponDetailVM.getListGroupon(getParentMerchantId());
    }

    private void initView() {
        // Toolbar
        swGroupon = findViewById(R.id.swGroupon);
        swGroupon.setOnRefreshListener(this);
        isBuy = false;
        btnBuyNow = findViewById(R.id.btnBuyNow);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.registered);
        }
        // Groupom
        spGroupon = findViewById(R.id.spGroupon);
        rcRegistry = findViewById(R.id.rcRegistry);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1,
                StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcRegistry.setLayoutManager(layoutManager);
        rcRegistry.setPullRefreshEnabled(false);
        rcRegistry.setLoadingMoreProgressStyle(ProgressStyle.BallRotate);
        rcRegistry.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {

            }

            @Override
            public void onLoadMore() {
                if (TsUtils.isNotNull(grouponDetailVM.getArrRegisterId())) {
                    grouponDetailVM.getMoreListRegistry(grouponDetailVM.getArrRegisterId().get(postionSelected));
                }
            }
        });
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        swGroupon.setRefreshing(false);
        switch (action) {
            case "getGrouponStatus":
                if (AppController.getCurrentMerchant().getMerchantTypeId() == 1) {
                    //
                    btnBuyNow.setVisibility(View.VISIBLE);
                } else {
                    btnBuyNow.setVisibility(View.GONE);
                }
                if (isBuy) {
                    Demo packingDTO = new Demo();
                    GrouponRegistryStatusDTO statusDTO = grouponDetailVM.getRegistryStatusDTO().get();
                    if (statusDTO != null) {
                        packingDTO.setOrderQuantity(statusDTO.getTotalQuantity());
                        packingDTO.setPackingProductId(statusDTO.getPackingProductId());
                        packingDTO.setProductId(statusDTO.getProductId());
                        packingDTO.setPrice(statusDTO.getPrice());
                        packingDTO.setUrl(statusDTO.getUrl());
                        packingDTO.setQuantity(statusDTO.getQuantity());
                        packingDTO.setProductName(statusDTO.getProductName());
                    }
                    //
//                    private Long packingProductId;
                    // Open Order
                    List<Demo> packingDTOS = new ArrayList<>();
                    packingDTOS.add(packingDTO);
                    Intent intent = new Intent(GrouponDetailActivity.this, CommonActivity.class);
                    intent.putExtra("FRAGMENT", CartFragment.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("packingDTOS", (Serializable) packingDTOS);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    //
                    isBuy = false;
                }
                break;
            case "getListRegistry":
                grouponRegistryAdapter = new BaseAdapterV2(R.layout.item_groupon_registry, grouponDetailVM, this);
                grouponRegistryAdapter.setConfigXRecycler(rcRegistry, 1);
                rcRegistry.setAdapter(grouponRegistryAdapter);
                break;
            case "getListGrouponSuccess":
                if (actionId != -1) {
                    if (TsUtils.isNotNull(grouponDetailVM.getArrRegisterId())) {
                        for (int i = 0; i < grouponDetailVM.getArrRegisterId().size(); i++) {
                            if (grouponDetailVM.getArrRegisterId().get(i) == actionId) {
                                spGroupon.setSelection(i);
                                postionSelected = i;
                                grouponDetailVM.getGrouponStatus(grouponDetailVM.getArrRegisterId().get(i));
                                grouponDetailVM.getListRegistry(grouponDetailVM.getArrRegisterId().get(i), 0);
                                break;
                            }
                        }
                    }
                }
                break;
            case "noMore":
                rcRegistry.setNoMore(true);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View v, Object o) {

    }

    @Override
    public void onRefresh() {
        swGroupon.setRefreshing(true);
        grouponDetailVM.refresh(getParentMerchantId());
    }

    private Long getParentMerchantId() {
        if (AppController.getCurrentMerchant().getMerchantTypeId() == 1) {
            return AppController.getMerchantId();
        } else if (AppController.getCurrentMerchant().getMerchantTypeId() == 2) {
            return AppController.getCurrentMerchant().getParentMarchantId();
        }
        return -1L;
    }
    @Override
    protected void onResume() {
        super.onResume();
        AlertsUtils.register(this);
    }

    @Override
    protected void onPause() {
        AlertsUtils.unregister(this);
        super.onPause();
    }
}
