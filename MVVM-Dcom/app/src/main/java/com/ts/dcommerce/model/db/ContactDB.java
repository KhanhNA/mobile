package com.ts.dcommerce.model.db;

import com.tsolution.base.BaseModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;

@Entity(
        indexes = {
                @Index(value = "contactId DESC", unique = true)
        }
)
public class ContactDB extends BaseModel {
    private String contactId;
    private String urlImg;
@Generated(hash = 1728945074)
public ContactDB(String contactId, String urlImg) {
    this.contactId = contactId;
    this.urlImg = urlImg;
}
@Generated(hash = 1972887847)
public ContactDB() {
}
public String getContactId() {
    return this.contactId;
}
public void setContactId(String contactId) {
    this.contactId = contactId;
}
public String getUrlImg() {
    return this.urlImg;
}
public void setUrlImg(String urlImg) {
    this.urlImg = urlImg;
}
}
