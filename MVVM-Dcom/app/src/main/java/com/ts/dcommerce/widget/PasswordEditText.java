package com.ts.dcommerce.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;

import com.ts.dcommerce.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordEditText extends EditText {
    Drawable eye, eyeStrike;
    Boolean visible = false;
    Boolean useStrike = false;
    Boolean useValidate = false;
    Drawable drawable;
    int ALPHA = (int) (255 * .70f);
    String MATCHER_PATTERN = "((?=.*\\d)(?=.*[A-Z])(?=.*[a-z]).{6,20})"; // (?=.*\d)
    Pattern pattern;
    Matcher matcher;

    public PasswordEditText(final Context context) {
        super(context);
        create(null);
    }

    public PasswordEditText(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        create(attrs);
    }

    public PasswordEditText(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PasswordEditText(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        create(attrs);
    }

    private void create(AttributeSet attrs) {
        this.pattern = Pattern.compile(MATCHER_PATTERN);
        //neu thuoc tinh nguoi dung truyen vao khac null
        if (attrs != null) {//ng dung truyen vao usetrie
            TypedArray array = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.PasswordEditText, 0, 0);
            useStrike = array.getBoolean(R.styleable.PasswordEditText_useStrike, false);
            this.useValidate = array.getBoolean(R.styleable.PasswordEditText_useValidate, false);
        }
        if (attrs != null)
            //mutate cho ra man hinh
            eye = ContextCompat.getDrawable(getContext(), R.drawable.ic_visibility_black_24dp).mutate();
        eyeStrike = ContextCompat.getDrawable(getContext(), R.drawable.ic_visibility_off_black_24dp).mutate();

//        if(this.useValidate){
//            setOnFocusChangeListener(new OnFocusChangeListener() {
//                @Override
//                public void onFocusChange(View view, boolean b) {
//                    if(!b) {
//                        String chuoi = getText().toString();
//                        TextInputLayout textInputLayout = (TextInputLayout) view.getParent();
//                        matcher = pattern.matcher(chuoi);
//                        if(!matcher.matches()){
//                            textInputLayout.setErrorEnabled(true);
//                            textInputLayout.setError("Mật khẩu phải bao gồm 6 ký tự và một chữ hoa");
//                        }else{
//                            textInputLayout.setErrorEnabled(false);
//                            textInputLayout.setError("");
//                        }
//
//
//                    }
//                }
//            });
//        }
        setting();
    }

    private void setting() {
        //mac dinh truyen vao visible = true thi dang pass con khong thi hien chu
        setInputType(InputType.TYPE_CLASS_TEXT | (visible ? InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : InputType.TYPE_TEXT_VARIATION_PASSWORD));
        Drawable[] drawables = getCompoundDrawables();//lay ra cac gia tri vi tri drawable, trai, phai, tren, duoi
        drawable = useStrike && !visible ? eyeStrike : eye;
        drawable.setAlpha(ALPHA);
        setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawable, drawables[3]);
    }

    //ke thua de click nut an hien
    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        // bat su kien nhan vao man hinh
        //lay chieu dai cua su kien click > đe tinh toa do click con mat
        if (event.getAction() == MotionEvent.ACTION_UP && event.getX() >= (getRight() - drawable.getBounds().width())) {
            visible = !visible;
            setting();
            invalidate();//phuong thuc kiem tra  lai man hinh
        }
        return super.onTouchEvent(event);
    }
}
