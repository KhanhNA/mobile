package com.ts.dcommerce.service.listener;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;

public interface ActionDialogFragmentListener {
    @LayoutRes
    int getLayoutRes();
}
