package com.ts.dcommerce.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author PhamBien
 * Create at 2019-06-21 09:39
 */
@Getter
@Setter
public class GrouponRegisterDetailDTO {
    private Long id;
    private Long incentiveProgramId;

    private Long merchantId;

    private Long grouponRegisterId;

    private Long packingProductId;

    private Integer registerQuantity;

    private Double registerAmount;

    private String merchantName;
    private String promotionName;

}
