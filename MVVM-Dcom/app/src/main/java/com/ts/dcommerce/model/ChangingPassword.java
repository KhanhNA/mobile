package com.ts.dcommerce.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangingPassword extends BaseModel {
    String oldPassword;
    String newPassword;
    String newConfirmPassword;
}
