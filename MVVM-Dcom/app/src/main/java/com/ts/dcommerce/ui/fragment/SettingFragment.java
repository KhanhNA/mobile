package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.ChangingPassword;
import com.ts.dcommerce.model.dto.TokenFbMerchantDTO;
import com.ts.dcommerce.ui.LoginActivityV2;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.viewmodel.SettingVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

public class SettingFragment extends BaseFragment {
    Toolbar toolbar;
    SettingVM settingVM;
    ChangePassDialog dialogChangePass;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreateView(inflater, container, savedInstanceState);
        settingVM = (SettingVM) viewModel;

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = getBaseActivity().findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = getBaseActivity();
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void action(View v, BaseViewModel viewModel) {
        Intent i;
        switch (v.getId()) {
            case R.id.btnUserInformation:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", AccountSettingFragment.class);
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }
                break;
            case R.id.btnAddress:
                i = new Intent(getActivity(), CommonActivity.class);
                i.putExtra("FRAGMENT", AddressSettingFragment.class);
                if (getActivity() != null) {
                    getActivity().startActivity(i);
                }
                break;
            case R.id.bankingAccount:
                Toast.makeText(getContext(), "bankingAccount", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnLanguage:
                Toast.makeText(getContext(), "btnLanguage", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnChangePass:
                showDialogChangePass();
                break;
            case R.id.btnLogout:
                showDialogLogout();
                break;
        }
        closeProcess();
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case "changePasswordSuccess":
                //reset value settingVM
                settingVM.getChangingPassword().set(new ChangingPassword());
                if (dialogChangePass != null) {
                    dialogChangePass.dismiss();
                }
                Toast.makeText(getContext(), "change Success", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void showDialogChangePass() {
        dialogChangePass = new ChangePassDialog(getBaseActivity(), settingVM);
        dialogChangePass.show(getBaseActivity().getFragmentManager(), dialogChangePass.getTag());

    }

    private void showDialogLogout() {
        new AlertDialog.Builder(getBaseActivity()).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.message_logout)
                .setPositiveButton(R.string.OK, (dialog, which) -> {
                    // Delete token firebase
                    TokenFbMerchantDTO fbMerchantDTO = new TokenFbMerchantDTO();
                    fbMerchantDTO.setMerchantId(AppController.getCurrentMerchant().getMerchantId());
                    fbMerchantDTO.setToken(AppController.getInstance().getSharePre().getString("token_fb", ""));
                    fbMerchantDTO.setIsDelete(true);
                    settingVM.deleteToken(fbMerchantDTO);
                    // Delete user & pass
                    AppController.getInstance().getSharePre().edit().putString(Constants.USER_NAME, "").apply();
                    AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
                    // Delete cache merchant
                    AppController.getInstance().clearCache();
                    // Delete local db
                    AppController.getInstance().getDaoSession().getContactDBDao().deleteAll();
                    AppController.getInstance().getDaoSession().getDemoDao().deleteAll();
                    AppController.getInstance().getDaoSession().getDmQuanHuyenDao().deleteAll();
                    // Intent loginActivity
                    Intent intent = new Intent(getBaseActivity(), LoginActivityV2.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                })
                .setNegativeButton(R.string.CANCEL, null)
                .show();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_setting;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return SettingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

}
