package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.OrderPaperAdapter;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.widget.SlidingTabLayout;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class OrderLv2Fragment extends BaseFragment {

    SlidingTabLayout slidingTabs;
    ViewPager vpOrderTab;
    Toolbar toolbar;
    OrderPaperAdapter myPagerAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreateView(inflater, container, savedInstanceState);
        innitView(binding.getRoot());

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = getBaseActivity().findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        assert appCompatActivity != null;
        if (appCompatActivity.getSupportActionBar() != null) {
            toolbar.setTitle(getString(R.string.menu_order_list));
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void innitView(View v) {
        slidingTabs = v.findViewById(R.id.slidingTab);
        vpOrderTab = v.findViewById(R.id.vpContent);
        slidingTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        slidingTabs.setDistributeEvenly(true);
        myPagerAdapter = new OrderPaperAdapter(getFragmentManager(), getResources().getStringArray(R.array.title_order_lv2));
        vpOrderTab.setAdapter(myPagerAdapter);
        vpOrderTab.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        slidingTabs.setViewPager(vpOrderTab);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        vpOrderTab.setPageMargin(pageMargin);

        Intent intent = getBaseActivity().getIntent();
        int tab = intent.getIntExtra("TAB_INDEX", 0);
        vpOrderTab.setCurrentItem(tab);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list_order, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            case R.id.action_chat:
                AppController.getInstance().openOtherApplication(Constants.chatAppId, getActivity());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_list_order;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


}
