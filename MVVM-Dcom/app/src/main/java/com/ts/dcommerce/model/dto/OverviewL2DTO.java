package com.ts.dcommerce.model.dto;

import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OverviewL2DTO extends BaseModel {
    private int totalRegister;
    private int totalOrderPending;
    private int totalOrderSuccess;
    private int totalActive;
    private long merchantId;
    private Date activeDate;
    private double totalAmount;
}