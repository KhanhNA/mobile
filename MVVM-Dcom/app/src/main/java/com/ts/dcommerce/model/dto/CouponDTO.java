package com.ts.dcommerce.model.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponDTO  implements Serializable {
    String code;
}
