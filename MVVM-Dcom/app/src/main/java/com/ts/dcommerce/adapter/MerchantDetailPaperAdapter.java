package com.ts.dcommerce.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.ui.fragment.MerchantInfoFragment;
import com.ts.dcommerce.ui.fragment.OrderL2Fragment;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MerchantDetailPaperAdapter extends FragmentPagerAdapter {
    private String[] TITLES;
    private MerchantInfoFragment infoFragment;
    private OrderL2Fragment l2Fragment;
    private MerchantDto dto;

    public MerchantDetailPaperAdapter(FragmentManager fm, String[] title, MerchantDto merchantDto) {
        super(fm);
        this.TITLES = title;
        this.dto = merchantDto;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                infoFragment = new MerchantInfoFragment();
                infoFragment.setMerchantDto(dto);
                return infoFragment;
            case 1:
                l2Fragment = new OrderL2Fragment();
                l2Fragment.setMerchantDto(dto);
                return  l2Fragment;
        }
        return null;
    }

}