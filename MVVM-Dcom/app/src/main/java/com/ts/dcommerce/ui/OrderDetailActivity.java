package com.ts.dcommerce.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.ts.dcommerce.R;
import com.ts.dcommerce.model.dto.OrderDTO;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.viewmodel.OrderDetailVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.Utils.AlertsUtils;
import com.tsolution.base.listener.AdapterActionsListener;

public class OrderDetailActivity extends BaseActivity implements View.OnClickListener, AdapterActionsListener {
    private String[] descriptionData = {"Tạo đơn", "Giao hàng", "Nhận hàng"};
    OrderDetailVM orderDetailVM;
    Toolbar toolbar;
    Button btnCancelOrder, btnConfirmOrder;
    ImageView imgQRCode;
    StateProgressBar progressBar;
    protected RecyclerView recyclerView;
    private ShimmerFrameLayout mShimmerViewContainer;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("actionId")) {
            mShimmerViewContainer.startShimmer();
            long orderId = intent.getLongExtra("actionId", 0);
            orderDetailVM.init(OrderDTO.builder().orderId(orderId).build());
        }

        // RecyclerView 1 sản phẩm
        BaseAdapter adapterProduct = new BaseAdapter(R.layout.item_order_detail, orderDetailVM.arrOrderProduct, getBaseActivity());
        recyclerView.setAdapter(adapterProduct);
        GridLayoutManager lmProduct =
                new GridLayoutManager(getBaseActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(lmProduct);

        // RecyclerView 2 sản phẩm khuyến mãi
        RecyclerView recyclerViewIncentive = findViewById(R.id.rcIncentiveProduct);
        GridLayoutManager lmIncentive =
                new GridLayoutManager(getBaseActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerViewIncentive.setLayoutManager(lmIncentive);
        BaseAdapter adapterProductPromotion = new BaseAdapter(R.layout.item_order_detail, orderDetailVM.incentiveProduct, getBaseActivity());
        recyclerViewIncentive.setAdapter(adapterProductPromotion);

    }

    @Override
    protected void onPause() {
        AlertsUtils.unregister(this);
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    private void initView() {
        progressDialog = new ProgressDialog(OrderDetailActivity.this);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        recyclerView = findViewById(R.id.recyclerSanPham);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        imgQRCode = findViewById(R.id.imgSuccess);
        progressBar = findViewById(R.id.process_delivery);
        btnCancelOrder = findViewById(R.id.btnCancelOrder);
        btnConfirmOrder = findViewById(R.id.btnConfirmOrder);
        toolbar = findViewById(R.id.toolbar);
        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(2f);
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        btnCancelOrder.setOnClickListener(this);
        btnConfirmOrder.setOnClickListener(this);
        orderDetailVM = (OrderDetailVM) viewModel;

    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case "getOrderSuccess":
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                // TODO: 8/27/2019 trạng thái đơn hàng
                progressBar.setStateDescriptionData(descriptionData);
                progressBar.setStateDescriptionTypeface("fonts/arial.ttf");
                progressBar.setStateNumberTypeface("fonts/arial.ttf");
                //set status state
                switch (orderDetailVM.getModelE().orderStatus) {
                    case 0:
                        progressBar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
                        break;
                    case 1:
                        progressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                        break;
                    case 2:
                        progressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                        break;
                    default:
                        progressBar.setVisibility(View.GONE);
                }

                //set QR Code
                String qrCode = orderDetailVM.getModelE().logisticQRCode;
                if (!StringUtils.isNullOrEmpty(qrCode)) {
                    byte[] decodedString = Base64.decode(qrCode, Base64.DEFAULT);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPurgeable = true;
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getBaseActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int width = displayMetrics.widthPixels;
                    Bitmap image = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);
                    image = Bitmap.createScaledBitmap(image, width / 2, width / 2, false);
                    imgQRCode.setImageBitmap(image);
                }
                break;
            case "updateOrderSuccess":
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                ToastUtils.showToast(getResources().getString(R.string.update_order_success));
                getBaseActivity().finish();
                break;
            case "updateOrderFail":
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            case R.id.action_chat:
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_list_order, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_order_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancelOrder:
                dialogCancelOrder();
                break;
            case R.id.btnConfirmOrder:
                showAlertDialog(R.string.APPROVE_ORDER, this, orderDetailVM.getOrderDTO());
                break;
        }
    }

    @Override
    public void adapterAction(View view, BaseModel baseModel) {
        confirmOrder("", false);
    }

    private void confirmOrder(String mess, Boolean isCancel) {
        progressDialog.setMessage(getString(R.string.waiting));
        progressDialog.show();
        orderDetailVM.confirmOrder(mess, isCancel);
    }

    public void dialogCancelOrder() {
        final EditText input = new EditText(getBaseActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        final AlertDialog dialog = new AlertDialog.Builder(getBaseActivity())
                .setTitle(getString(R.string.CONFIRM))
                .setMessage(getString(R.string.reason_cancel_order))
                .setPositiveButton(getString(R.string.ok), null)
                .setNegativeButton(getString(R.string.cancel), (dialog1, which) -> {
                })
                .setView(input)
                .show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(v -> {
            if (StringUtils.isNotNullAndNotEmpty(input.getText().toString().trim())) {
                confirmOrder(input.getText().toString().trim(), true);
                dialog.dismiss();
            } else {
                ToastUtils.showToast(getString(R.string.NOT_EMPTY_REASON));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AlertsUtils.register(this);
    }

}
