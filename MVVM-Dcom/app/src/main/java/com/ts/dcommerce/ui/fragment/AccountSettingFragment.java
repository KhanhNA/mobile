package com.ts.dcommerce.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.ts.dcommerce.R;
import com.ts.dcommerce.util.ImageUtils;
import com.ts.dcommerce.viewmodel.AccountSettingVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.Calendar;

public class AccountSettingFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    private int SELECT_IMAGE = 9999;
    private final int REQUEST_READ_EXTERNAL_STORAGE = 101;
    AccountSettingVM accountSettingVM;
    Toolbar toolbar;
    String picturePath;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreateView(inflater, container, savedInstanceState);
        accountSettingVM = (AccountSettingVM) viewModel;
        accountSettingVM.init();
        return binding.getRoot();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = getBaseActivity().findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = getBaseActivity();
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }




    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnAvatar:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getContext()!=null && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE);

                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_READ_EXTERNAL_STORAGE);
                    } else {
                        intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                    }
                }
                break;

            case R.id.txtBirthday:
                if (getContext() != null) {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, year, month, day);
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
                break;
            case R.id.btnUpdate:
                accountSettingVM.updateMerchant(picturePath);
                break;
        }
        closeProcess();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && getContext() != null) {
                    int buildVertion = Build.VERSION.SDK_INT;
                    if (buildVertion < Build.VERSION_CODES.KITKAT && buildVertion > Build.VERSION_CODES.HONEYCOMB) {
                        picturePath = ImageUtils.getRealPathFromURI_API11to18(getContext(), data.getData());
                    } else {
                        picturePath = ImageUtils.getRealPathFromURI_API19(getContext(),  data.getData());
                    }
                    accountSettingVM.setAvatar(picturePath);
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getContext(), getString(R.string.canceled), Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
            } else {
                //If user presses deny
                Toast.makeText(getContext(), getString(R.string.permissions_not_granted), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_account_setting;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return AccountSettingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        accountSettingVM.setDate(year, month, day);
    }
}
