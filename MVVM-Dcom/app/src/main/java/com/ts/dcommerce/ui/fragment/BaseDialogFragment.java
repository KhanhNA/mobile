package com.ts.dcommerce.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.ts.dcommerce.service.listener.ActionDialogFragmentListener;
import com.tsolution.base.listener.AdapterListener;

public abstract class BaseDialogFragment extends DialogFragment implements AdapterListener, ActionDialogFragmentListener {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @Override
    public void onItemClick(View view, Object o) {

    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }


}
