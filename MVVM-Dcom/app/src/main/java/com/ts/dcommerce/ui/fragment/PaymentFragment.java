package com.ts.dcommerce.ui.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ns.paymentv5.models.PaymentDetail;
import com.ns.paymentv5.ui.views.activities.PaymentLoginActivity;
import com.ns.paymentv5.utils.PaymentConfiguration;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.MethodAdapter;
import com.ts.dcommerce.model.dto.MethodDTO;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.viewmodel.OrderConfirmVM;
import com.ts.dcommerce.viewmodel.PaymentVM;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.math.BigDecimal;
import java.util.ArrayList;

public class PaymentFragment extends BaseFragment {
    private MethodDTO methodDTO;
    private static PaymentFragment mInstance;
    private final int BAKING_REQUEST_CODE = 8171;
    private PaymentVM paymentVM;
    private OrderConfirmVM orderConfirmVM;
    private BaseAdapter adapterProduct, adapterProductPromotion;
    private OrderSuccessDialog dialog;
    private ProgressDialog pd;

    public static PaymentFragment getInstance() {
        if (mInstance == null) {
            mInstance = new PaymentFragment();
        }
        return mInstance;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        paymentVM = (PaymentVM) viewModel;
        methodDTO = new MethodDTO();
        orderConfirmVM = ViewModelProviders.of(getBaseActivity()).get(OrderConfirmVM.class);
        paymentVM.init(orderConfirmVM);
        //
        pd = new ProgressDialog(getBaseActivity());
        pd.setMessage(getString(R.string.processing));
        methodPayment(binding.getRoot());
        // RecyclerView 1 sản phẩm
        adapterProduct = new BaseAdapter(R.layout.item_order_review, orderConfirmVM.arrOrderProduct, getBaseActivity());
        GridLayoutManager lmProduct =
                new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(lmProduct);
        recyclerView.setAdapter(adapterProduct);

        // RecyclerView 2 sản phẩm khuyến mãi
        RecyclerView recyclerViewIncentive = binding.getRoot().findViewById(R.id.rcIncentiveProduct);
        GridLayoutManager lmIncentive =
                new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerViewIncentive.setLayoutManager(lmIncentive);
        adapterProductPromotion = new BaseAdapter(R.layout.item_order_review, orderConfirmVM.incentiveProduct, getBaseActivity());
        recyclerViewIncentive.setAdapter(adapterProductPromotion);
        return binding.getRoot();
    }

    private void methodPayment(View v) {
        ArrayList<MethodDTO> arrPayment = new ArrayList<>();
        arrPayment.add(new MethodDTO(1L, "Ship Cod", Constants.SHIP_COD, R.drawable.ic_shipcod));
        arrPayment.add(new MethodDTO(2L, "mStore Pay", Constants.MSTORE_PAY, R.drawable.banner_app_new));
        // Default Payment Method
        this.methodDTO = arrPayment.get(0);
        //
        RecyclerView recyclerView = v.findViewById(R.id.rcPayment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        MethodAdapter mAdapter = new MethodAdapter(arrPayment);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        //
        mAdapter.setListener(methodDTO -> {
            this.methodDTO = methodDTO;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DeliveryFragment.isRefresh) {
            paymentVM.createOrder(false, null, "", "");
            adapterProduct.notifyDataSetChanged();
            adapterProductPromotion.notifyDataSetChanged();
            DeliveryFragment.isRefresh = false;
        }


    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view == null) {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(getContext(), CommonActivity.class);
            intent.putExtra("FRAGMENT", OrderDetailFragment.class);
            bundle.putSerializable("BASE_MODEL", paymentVM.getOrderDTO());
            intent.putExtras(bundle);
            if (getContext() != null) {
                getContext().startActivity(intent);
            }
            return;
        }
        if (view.getId() == R.id.cash) {
            if (methodDTO != null && StringUtils.isNullOrEmpty(methodDTO.getPaymentCode())) {
                ToastUtils.showToast(getString(R.string.not_select_payment_method));
                return;
            }
            if (methodDTO != null && Constants.SHIP_COD.equalsIgnoreCase(methodDTO.getPaymentCode())) {
                paymentVM.createOrder(true, methodDTO, "", "");
            } else if (methodDTO != null && Constants.MSTORE_PAY.equalsIgnoreCase(methodDTO.getPaymentCode())) {
                processBaking();
            }
        }
    }

    private void processBaking() {
        if (methodDTO != null && methodDTO.getPaymentMethodId() != null) {
            if (Constants.MSTORE_PAY.equalsIgnoreCase(methodDTO.getPaymentCode())) {
                // TODO:Dang nhap vi
                Intent intent = new Intent(getContext(), PaymentLoginActivity.class);
                PaymentDetail paymentDetail = new PaymentDetail(new BigDecimal(paymentVM.getTotal()));
                intent.putExtra(PaymentConfiguration.PAYMENT_DETAIL, paymentDetail);
                startActivityForResult(intent, BAKING_REQUEST_CODE);
            } else {
                paymentVM.createOrder(true, methodDTO, "", "");
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BAKING_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    String des = extras.getString(PaymentConfiguration.PAYMENT_RESULT, "");
                    String tokenMifos = extras.getString(PaymentConfiguration.PAYMENT_TOKEN, "");

                    // Transfer amount and update order status
                    if (!getBaseActivity().isFinishing()) {
                        this.getBaseActivity().runOnUiThread(() -> {
                            if (pd != null && !pd.isShowing()) {
                                pd.show();
                            }
                        });
                    }
                    paymentVM.createOrder(true, methodDTO, des, tokenMifos);
                }
            }
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case "createOrderSuccess":
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                ProductFragmentV2.isChangeProduct = true;
                dialog = new OrderSuccessDialog(getActivity(), (PaymentVM) viewModel);
                dialog.show(getBaseActivity().getSupportFragmentManager(), dialog.getTag());
                break;
            case "transferSuccess":
                paymentVM.deleteAllFromCart();
                ProductFragmentV2.isChangeProduct = true;
                dialog = new OrderSuccessDialog(getActivity(), (PaymentVM) viewModel);
                dialog.show(getBaseActivity().getSupportFragmentManager(), dialog.getTag());
                break;
            case "createOrderFail":
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_payment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PaymentVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.recyclerSanPham;
    }
}
