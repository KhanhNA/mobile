package com.ts.dcommerce.ui;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.IncentiveDescriptionDTO;
import com.ts.dcommerce.model.dto.LoyaltyPointDTO;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.ui.fragment.CartFragment;
import com.ts.dcommerce.ui.fragment.CategoriesDetailFragment;
import com.ts.dcommerce.ui.fragment.ProductFragmentV2;
import com.ts.dcommerce.ui.fragment.PromotionDialogFragment;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.ProductDetailVM;
import com.ts.dcommerce.widget.BottomSheetAddToCart;
import com.ts.dcommerce.widget.CircleAnimationUtil;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.Utils.AlertsUtils;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.HashMap;
import java.util.Objects;

public class ProductDetailActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener, View.OnClickListener {
    private SliderLayout mDemoSlider;
    ProductDetailVM productDetailVM;
    //    CartVM cartVM;
    HtmlTextView txtDes;
    BottomSheetAddToCart sheetBehavior;
    LinearLayout bottomSheet;
    Button btnAddToCart, btnAddToCart2, btnRegisterGroupon;
    FrameLayout frCart;
    //toolbar
    LinearLayout toolbar;
    TextView iconCart, iconBack, txtTitle;
    ImageView imgShare, imgPromotion, imgPoint;
    NestedScrollView scrollView;
    BaseAdapter packingAdapter;
    TextView txtDuration;
    //share
    public static final String FACEBOOK_PACKAGE_NAME = "com.facebook";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        onScroll();
        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                if (getIntent().hasExtra("productId")) {
                    productDetailVM.getProductDetailAPI(extras.getLong("productId", -1));
                }
                if (getIntent().hasExtra("packingProductId")) {
                    long packingProductId = extras.getLong("packingProductId", -1);
                    productDetailVM.init(packingProductId);
                    productDetailVM.getPoint(packingProductId);
                }
                if (getIntent().hasExtra("discountPercent")) {
                    productDetailVM.setFlashSale(extras.getBoolean("discountPercent"));
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AlertsUtils.register(this);
        productDetailVM.getTotalProduct();
    }

    private void initBanner(String[] listImg) {
        HashMap<String, String> url_maps = new HashMap<>();
        for (int i = 0; i < listImg.length; i++) {
            url_maps.put(i + 1 + "/" + listImg.length, AppController.BASE_URL + "files" + listImg[i]);
        }
        sliderCollection(url_maps);
    }

    private void sliderCollection(HashMap<String, String> url_maps) {
        for (String name : url_maps.keySet()) {
            DefaultSliderView textSliderView = new DefaultSliderView(ProductDetailActivity.this);
            // initialize a SliderLayout
            textSliderView.description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);
            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(3600);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
    }

    private void initView() {
        imgPromotion = findViewById(R.id.imgPromotion);
        mDemoSlider = findViewById(R.id.slider);
        scrollView = findViewById(R.id.scroll);
        toolbar = findViewById(R.id.toolbar);
        txtTitle = findViewById(R.id.txtTitle);
        iconBack = findViewById(R.id.iconBack);
        iconCart = findViewById(R.id.iconCart);
        imgShare = findViewById(R.id.imgShare);
        imgPoint = findViewById(R.id.imgPoint);
        bottomSheet = findViewById(R.id.bottom_sheet);
        txtDuration = findViewById(R.id.txtDuration);
        RecyclerView rcPackingProduct = findViewById(R.id.rcBackingProduct);
        frCart = findViewById(R.id.frCart);
        txtDes = findViewById(R.id.txtDes);
        btnAddToCart = findViewById(R.id.btnAddToCart);
        btnAddToCart2 = findViewById(R.id.btnAddToCart2);
        btnRegisterGroupon = findViewById(R.id.btnRegisterGroupon);
        productDetailVM = (ProductDetailVM) viewModel;

        //set binding to dialog
        sheetBehavior = new BottomSheetAddToCart(getBaseActivity(), productDetailVM, R.id.rcPackingDialog);

        //list packingProduct
        GridLayoutManager layoutManager =
                new GridLayoutManager(ProductDetailActivity.this, 1, GridLayoutManager.HORIZONTAL, false);
        rcPackingProduct.setLayoutManager(layoutManager);
        packingAdapter = new BaseAdapter(R.layout.item_packing_product, productDetailVM.arrPackingProduct, this::onPackingClick, getBaseActivity());
        rcPackingProduct.setAdapter(packingAdapter);


        //set onClick to element
        btnAddToCart.setOnClickListener(this);
        btnAddToCart2.setOnClickListener(this);
        btnRegisterGroupon.setOnClickListener(this);
        frCart.setOnClickListener(this);
        iconBack.setOnClickListener(this);
        imgShare.setOnClickListener(this);
        imgPoint.setOnClickListener(this);
        imgPromotion.setOnClickListener(this);
    }

    private void onPackingClick(View view, BaseModel model) {
        productDetailVM.getIndexPacking().set(model.index - 1);
        if (model.checked == null || !model.checked) {
            if (productDetailVM.getPacking() != null) {
                productDetailVM.getPacking().checked = false;
            }
            model.checked = true;
            productDetailVM.setPacking((ProductPackingDto) model);

        }
        // Get promotion program by productPackingId
        productDetailVM.getIsGroupon().set(false);
        imgPromotion.setVisibility(View.GONE);
        productDetailVM.getIncentivePacking(productDetailVM.getPacking().getPackingProductId());
        productDetailVM.getPoint(productDetailVM.getPacking().getPackingProductId());
        // Update adapter
        packingAdapter.notifyDataSetChanged();
    }

    /**
     * animation fly to cart
     */
    private void makeFlyAnimation(View fromView, View tagetView) {

        new CircleAnimationUtil().attachActivity(ProductDetailActivity.this).setTargetView(fromView).setMoveDuration(500).setDestView(tagetView).setAnimationListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                productDetailVM.getTotalProduct();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();


    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_product_detail_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ProductDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case "updateProductDes":
                String des = Objects.requireNonNull(((ProductDetailVM) viewModel).getProductDetail().get()).getDesProduct();
                if (!StringUtils.isNullOrEmpty(des)) {
                    txtDes.setHtml(des,
                            new HtmlHttpImageGetter(txtDes));
                }
                String[] listImg = Objects.requireNonNull(((ProductDetailVM) viewModel).getProductDetail().get()).getImageList();
                onPackingClick(null, productDetailVM.getPacking());
                initBanner(listImg);
                //flash sale
                if (productDetailVM.getPacking().getDuration() != null) {
//                    new CountDownTimer((productDetailVM.getPacking().getDuration() - 1) * 60 * 1000L, 1000) {
//                        public void onTick(long millisUntilFinished) {
//                            long seconds = millisUntilFinished / 1000;
//                            long minutes = seconds / 60;
//                            long hours = minutes / 60;
//                            long days = hours / 24;
//                            String time;
//                            if (days > 0) {
//                                time = days + " days " + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
//                            } else {
//                                time = hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
//                            }
//                            txtDuration.setText(time);
//                        }
//
//                        @SuppressLint("SetTextI18n")
//                        public void onFinish() {
//                            txtDuration.setText("Time up!");
//                        }
//                    }.start();
                }

                break;

            case "addToCart":
                Toast.makeText(ProductDetailActivity.this, getString(R.string.ADD_CART), Toast.LENGTH_SHORT).show();
                makeFlyAnimation(view, frCart);
                sheetBehavior.dismiss();
                ProductFragmentV2.isChangeProduct = true;
                CategoriesDetailFragment.isChangeProduct = true;
                break;
            case "getIncentiveSuccess":
                if (TsUtils.isNotNull(productDetailVM.getArrIncentive())) {
                    imgPromotion.setVisibility(View.VISIBLE);
                    for (IncentiveDescriptionDTO dto : productDetailVM.getArrIncentive()) {
                        // Kiem tra km groupon
                        if (dto.getIncentiveType() == 3) {
                            productDetailVM.getIsGroupon().set(true);
                            return;
                        }
                    }
                    productDetailVM.getIsGroupon().set(false);
                } else {
                    productDetailVM.getIsGroupon().set(false);
                    imgPromotion.setVisibility(View.GONE);
                }
                break;
            case "grouponRegisterSuccess":
                try {
                    Toast.makeText(ProductDetailActivity.this, getString(R.string.GROUPON_REGISTER_SUCCESS), Toast.LENGTH_SHORT).show();
                    sheetBehavior.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case "getPointSuccess":
                if (TsUtils.isNotNull(productDetailVM.getArrPointDTO())) {
                    imgPoint.setVisibility(View.VISIBLE);
                } else {
                    imgPoint.setVisibility(View.GONE);
                }
                break;
        }
    }

    /**
     * animation change color toolbar
     */
    private void onScroll() {
        scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (nestedScrollView, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY == 0) {
                toolbar.setBackgroundColor(Color.parseColor("#00ffffff"));
                iconBack.setBackground(getResources().getDrawable(R.drawable.circle_v1));
                iconCart.setBackground(getResources().getDrawable(R.drawable.circle_v1));
                iconBack.setTextColor(Color.parseColor("#ffffff"));
                iconCart.setTextColor(Color.parseColor("#ffffff"));
                txtTitle.setTextColor(Color.parseColor("#00000000"));

            } else if (scrollY > 50 && scrollY < 100) {
                toolbar.setBackgroundColor(Color.parseColor("#33ffffff"));
                iconBack.setBackground(getResources().getDrawable(R.drawable.circle_v2));
                iconCart.setBackground(getResources().getDrawable(R.drawable.circle_v2));
                iconBack.setTextColor(Color.parseColor("#ccffffff"));
                iconCart.setTextColor(Color.parseColor("#ccffffff"));
                txtTitle.setTextColor(Color.parseColor("#33000000"));
            } else if (scrollY > 100 && scrollY < 200) {
                toolbar.setBackgroundColor(Color.parseColor("#88ffffff"));
                iconBack.setBackground(getResources().getDrawable(R.drawable.circle_v3));
                iconCart.setBackground(getResources().getDrawable(R.drawable.circle_v3));
                iconCart.setTextColor(Color.parseColor("#ccffffff"));
                iconBack.setTextColor(Color.parseColor("#ccffffff"));
                txtTitle.setTextColor(Color.parseColor("#88000000"));
            } else if (scrollY > 200 && scrollY < 300) {
                iconBack.setBackground(getResources().getDrawable(R.drawable.circle_v3));
                iconCart.setBackground(getResources().getDrawable(R.drawable.circle_v3));
                toolbar.setBackgroundColor(Color.parseColor("#ccffffff"));
                iconBack.setTextColor(Color.parseColor("#77B20024"));
                iconCart.setTextColor(Color.parseColor("#77B20024"));
                txtTitle.setTextColor(Color.parseColor("#882d2d2d"));
            } else if (scrollY > 300 && scrollY < 500) {
                toolbar.setBackgroundColor(Color.parseColor("#ffffffff"));
                iconBack.setBackgroundColor(Color.WHITE);
                iconCart.setBackgroundColor(Color.WHITE);
                iconBack.setTextColor(getResources().getColor(R.color.da_cam));
                iconCart.setTextColor(getResources().getColor(R.color.da_cam));
                txtTitle.setTextColor(Color.parseColor("#2d2d2d"));
            }
        });
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iconBack:
                onBackPressed();
                break;
            case R.id.btnAddToCart:
            case R.id.btnAddToCart2:
                productDetailVM.setRegisterCoupon(false);
                productDetailVM.getQuantity().set(1);
                sheetBehavior.show(getSupportFragmentManager(), sheetBehavior.getTag());
                break;
            case R.id.frCart:
                Intent intent = new Intent(ProductDetailActivity.this, CommonActivity.class);
                intent.putExtra("FRAGMENT", CartFragment.class);
                startActivity(intent);
                break;
            case R.id.btnRegisterGroupon:
                productDetailVM.setRegisterCoupon(true);
                productDetailVM.getQuantity().set(1);
                sheetBehavior.show(getSupportFragmentManager(), sheetBehavior.getTag());
                break;
            case R.id.imgShare:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, FACEBOOK_PACKAGE_NAME);
                    String sAux = "Chia sẻ với ";
                    // TODO: 8/14/2019 thay bằng link sản phẩm.
                    sAux = sAux + "https://play.google.com/store/apps/details?id=Orion.Soft \n\n";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (android.content.ActivityNotFoundException e) {
                    Toast.makeText(this, "app have not been installed.", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.imgPromotion:
                if (TsUtils.isNotNull(productDetailVM.getArrIncentive())) {
                    FragmentManager fm = getSupportFragmentManager();
                    PromotionDialogFragment editNameDialogFragment = new PromotionDialogFragment(productDetailVM.getArrIncentive());
                    editNameDialogFragment.show(fm, "fragment_webview");
                }
                break;
            case R.id.imgPoint:
                if (TsUtils.isNotNull(productDetailVM.getArrPointDTO())) {
                    showDialogPoint();
                }
                break;
        }
    }

    private void showDialogPoint() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(ProductDetailActivity.this);
        builderSingle.setIcon(R.drawable.piggy_bank);
        builderSingle.setTitle(getString(R.string.loyal_point));
        StringBuilder message;
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ProductDetailActivity.this, android.R.layout.select_dialog_item);
        for (LoyaltyPointDTO dto : productDetailVM.getArrPointDTO()) {
            message = new StringBuilder();
            if (dto.getMinAmount() != null && dto.getMinAmount() != 0) {
                message.append("Số tiền: ").append(dto.getMinAmount());
            }
            if (dto.getMinQuantity() != null && dto.getMinQuantity() != 0) {
                message.append("SLTT: ").append(dto.getMinQuantity());
            }
            if (dto.getLoyaltyCoin() != null && dto.getLoyaltyCoin() != 0) {
                message.append(" -- Coin: ").append(dto.getLoyaltyCoin());
            }
            if (dto.getLoyaltyPoint() != null && dto.getLoyaltyPoint() != 0) {
                message.append(" -- Point: ").append(dto.getLoyaltyPoint());
            }
            arrayAdapter.add(message.toString());
        }

        builderSingle.setNegativeButton("cancel", (dialog, which) -> dialog.dismiss());
        builderSingle.setAdapter(arrayAdapter, (dialog, which) -> {
            LoyaltyPointDTO pointDTO = productDetailVM.getArrPointDTO().get(which);
            if (pointDTO != null && pointDTO.getMinQuantity() != null && pointDTO.getMinQuantity() != 0) {
                productDetailVM.setRegisterCoupon(false);
                productDetailVM.getQuantity().set(Integer.parseInt("" + pointDTO.getMinQuantity()));
                sheetBehavior.show(getSupportFragmentManager(), sheetBehavior.getTag());
            }
        });
        builderSingle.show();
    }


    @Override
    protected void onPause() {
        AlertsUtils.unregister(this);
        super.onPause();
    }
}
