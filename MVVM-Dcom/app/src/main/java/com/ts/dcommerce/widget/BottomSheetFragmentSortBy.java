package com.ts.dcommerce.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.ts.dcommerce.R;
import com.ts.dcommerce.databinding.BottomSheetFragmentSortByBinding;
import com.ts.dcommerce.databinding.BottomSheetQuantityProductBinding;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.ProductDetailVM;
import com.ts.dcommerce.viewmodel.ProductVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

@SuppressLint("ValidFragment")
public class BottomSheetFragmentSortBy extends BottomSheetDialogFragment implements View.OnClickListener {
    private BottomSheetFragmentSortByBinding binding;
    private ProductVM productVM;
    private int index;
    private TextView lastChecked;
    private final int ic_sort_black_24dp = R.drawable.ic_sort_black_24dp;
    private final int ic_favorites_20dp = R.drawable.ic_favorites_20dp;
    private final int ic_ascending_20dp = R.drawable.ic_ascending_20dp;
    private final int ic_decrease_20dp = R.drawable.ic_decrease_20dp;
    private FilterTypeListener listener;

    public interface FilterTypeListener {
        void typeCallBack(int type);
    }

    public static BottomSheetFragmentSortBy newInstance(int num) {
        BottomSheetFragmentSortBy f = new BottomSheetFragmentSortBy();
        Bundle args = new Bundle();
        args.putInt("index", num);
        f.setArguments(args);
        return f;
    }

    @SuppressLint("ValidFragment")
    public BottomSheetFragmentSortBy() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_fragment_sort_by, container, false);
        lastChecked = binding.all;
        assert getArguments() != null;
        setSelection(getArguments().getInt("index"));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.hideBottomSheet.setOnClickListener(this);
        binding.sortByAscending.setOnClickListener(this);
        binding.sortByDecrease.setOnClickListener(this);
        binding.sortByName.setOnClickListener(this);
        binding.sortByRating.setOnClickListener(this);
        binding.all.setOnClickListener(this);
    }

    private void checkSelection(TextView textView, @DrawableRes int startDrawable, int filterType, boolean isDismiss) {
        lastChecked.setCompoundDrawablesWithIntrinsicBounds(lastChecked.getCompoundDrawables()[0], null, null, null);
        textView.setCompoundDrawablesWithIntrinsicBounds(startDrawable, 0, R.drawable.ic_check, 0);
        lastChecked = textView;

        if (isDismiss) {
            if (listener != null) {
                listener.typeCallBack(filterType);
            }
            dismiss();
        }

    }

    private void setSelection(int index) {
        switch (index) {
            case 0:
                checkSelection(binding.all, 0, 0, false);
                break;
            case 1:
                checkSelection(binding.sortByName, ic_sort_black_24dp, 1, false);
                break;
            case 2:
                checkSelection(binding.sortByRating, ic_favorites_20dp, 2, false);
                break;
            case 3:
                checkSelection(binding.sortByAscending, ic_ascending_20dp, 3, false);
                break;
            case 4:
                checkSelection(binding.sortByDecrease, ic_decrease_20dp, 4, false);
                break;
        }
    }

    public void setFilterListener(FilterTypeListener filterListener) {
        this.listener = filterListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.hideBottomSheet:
                this.dismiss();
                break;
            case R.id.all:
                checkSelection(binding.all, 0, 0, true);
                break;
            case R.id.sort_by_name:
                checkSelection(binding.sortByName, ic_sort_black_24dp, 1, true);
                break;
            case R.id.sort_by_rating:
                checkSelection(binding.sortByRating, ic_favorites_20dp, 2, true);
                break;
            case R.id.sort_by_ascending:
                checkSelection(binding.sortByAscending, ic_ascending_20dp, 3, true);
                break;
            case R.id.sort_by_decrease:
                checkSelection(binding.sortByDecrease, ic_decrease_20dp, 4, true);
                break;
        }
    }
}
