
package com.ts.dcommerce.model.dto.stockAvailable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvailableStock {

    @SerializedName("store")
    @Expose
    private Store store;
    @SerializedName("productPacking")
    @Expose
    private ProductPacking productPacking;
    @SerializedName("totalQuantity")
    @Expose
    private Integer totalQuantity;

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

}
