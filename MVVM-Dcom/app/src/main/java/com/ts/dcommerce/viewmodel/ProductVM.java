package com.ts.dcommerce.viewmodel;

import android.app.Application;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.base.http.HttpHelper;
import com.ts.dcommerce.base.http.rx.RxSchedulers;
import com.ts.dcommerce.model.db.Demo;
import com.ts.dcommerce.model.dto.BannerAlbumDTO;
import com.ts.dcommerce.model.dto.ProductCustomDTO;
import com.ts.dcommerce.model.dto.ProductDto;
import com.ts.dcommerce.model.dto.ProductPackingDto;
import com.ts.dcommerce.model.dto.UserPredictionDTO;
import com.ts.dcommerce.model.response.ServiceResponse;
import com.ts.dcommerce.network.rx.RxSubscriber;
import com.ts.dcommerce.util.Constants;
import com.ts.dcommerce.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductVM extends BaseViewModel {
    public BaseViewModel arrProductsVM;
    private List<ProductPackingDto> arrFlashSale;
    private List<ProductPackingDto> arrGroupon;
    private List<BannerAlbumDTO> arrBanner;
    private int totalPage = 0;
    private int currentPage = 0;
    CartVM cartVM = new CartVM(getApplication());
    private List<String> lstPrediction;

    ObservableBoolean isLoading = new ObservableBoolean();

    public ProductVM(@NonNull Application application) {
        super(application);
        arrFlashSale = new ArrayList<>();
        arrBanner = new ArrayList<>();
        arrGroupon = new ArrayList<>();
        arrProductsVM = new BaseViewModel(application);
        arrProductsVM.baseModels.set(new ArrayList<>());
        lstPrediction = new ArrayList<>();
    }

    public void setOrderedProduct(List<ProductDto> list) {
        if (TsUtils.isNotNull(list)) {
            HashMap<String, Demo> hashMap = cartVM.getOrderedProducts();
            for (ProductDto productDto : list) {
                if (productDto == null) {
                    continue;
                }
                productDto.setQuantityOrdered(hashMap.get(productDto.getProductId() + "|" + productDto.getQuantity()) == null ?
                        0 : hashMap.get(productDto.getProductId() + "|" + productDto.getQuantity()).getOrderQuantity().intValue());
            }
            arrProductsVM.baseModels.notifyChange();
        }
    }

    public void setOrderedProduct(List<ProductDto> list, int position) {
        if (TsUtils.isNotNull(list)) {
            HashMap<String, Demo> hashMap = cartVM.getOrderedProducts();
            list.get(position).setQuantityOrdered(hashMap.get(list.get(position).getProductId() + "|" + list.get(position).getQuantity()).getOrderQuantity().intValue());
            arrProductsVM.baseModels.notifyChange();
        }
    }

    public void getMoreProduct() {
        if (totalPage > 0 && currentPage < totalPage) {
            currentPage += 1;
            callApi(HttpHelper.getInstance().getApi().getProducts(currentPage, AppController.languageId)
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<ServiceResponse<ProductDto>>() {
                        @Override
                        public void onSuccess(ServiceResponse<ProductDto> productDtoServiceResponse) {
                            getProductSuccess(productDtoServiceResponse);
                        }

                    }));
        } else {
            try {
                view.action("noMore", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
    }

    public void getMoreProductsOfMerchant(int filterType) {
        if (totalPage > 0 && currentPage < totalPage) {
            callApi(HttpHelper.getInstance().getApi().getProductsOfMerchant(currentPage, UserPredictionDTO.builder()
                    .merchantId(AppController.getMerchantId())
                    .lstPrediction(lstPrediction)
                    .filterType(filterType)
                    .build())
                    .compose(RxSchedulers.io_main())
                    .subscribeWith(new RxSubscriber<ProductCustomDTO>() {
                        @Override
                        public void onSuccess(ProductCustomDTO productDtoServiceResponse) {
                            currentPage += 1;
                            getPredictionProductSuccess(productDtoServiceResponse);
                        }

                    }));
        } else {
            try {
                view.action("noMore", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
    }

    public void getProductByFilter(int filterType) {
        currentPage = 0;
        lstPrediction.clear();
        arrProductsVM.getBaseModelsE().clear();
        getMoreProductsOfMerchant(filterType);
    }

    public void getProduct() {
        callApi(HttpHelper.getInstance().getApi().getProducts(0, AppController.languageId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<ProductDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<ProductDto> productDtoServiceResponse) {
                        getProductSuccess(productDtoServiceResponse);
                    }

                }));
    }

    public void getPredictionProduct() {
        callApi(HttpHelper.getInstance().getApi().getPredictionProduct(0, UserPredictionDTO.builder()
                .merchantId(AppController.getMerchantId())
                .lstPrediction(null)
                .build())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ProductCustomDTO>() {
                    @Override
                    public void onSuccess(ProductCustomDTO productDtoServiceResponse) {
                        getPredictionProductSuccess(productDtoServiceResponse);
                    }

                }));
    }

    public void getBannerMarketing() {
        callApi(HttpHelper.getInstance().getApi().getBannerMarketing(AppController.getMerchantId())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<List<BannerAlbumDTO>>() {
                    @Override
                    public void onSuccess(List<BannerAlbumDTO> dtos) {
                        getBannerSuccess(dtos);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        try {
                            view.action("noBanner", null, null, null);
                        } catch (AppException ex) {
                            ex.printStackTrace();
                        }
                    }
                }));
    }

    private void getBannerSuccess(List<BannerAlbumDTO> o) {
        arrBanner = new ArrayList<>();
        if (TsUtils.isNotNull(o)) {
            arrBanner = o;
            try {
                view.action("getBannerSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else {
            try {
                view.action("noBanner", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
    }


    public void getFlashSale() {
        arrFlashSale.clear();
        callApi(HttpHelper.getInstance().getApi().getFlashSale(AppController.languageId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<ProductPackingDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<ProductPackingDto> response) {
                        getFlashSaleSuccess(response);
                    }

                }));
    }

    public void getGroupon() {
        arrGroupon.clear();
        callApi(HttpHelper.getInstance().getApi().getGroupon(AppController.languageId)
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<ServiceResponse<ProductPackingDto>>() {
                    @Override
                    public void onSuccess(ServiceResponse<ProductPackingDto> response) {
                        getGrouponSuccess(response);
                    }

                }));
    }

    private void getFlashSaleSuccess(ServiceResponse<ProductPackingDto> o) {
        if (o != null && TsUtils.isNotNull(o.getArrData())) {
            arrFlashSale.addAll(o.getArrData());
            try {
                view.action("getFlashSaleSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else {
            try {
                view.action("getFlashSaleFail", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
        isLoading.set(false);
    }

    private void getGrouponSuccess(ServiceResponse<ProductPackingDto> dto) {
        if (dto != null && TsUtils.isNotNull(dto.getArrData())) {
            arrGroupon.addAll(dto.getArrData());
            try {
                view.action("getGrouponSuccess", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        } else {
            try {
                view.action("getGrouponFail", null, null, null);
            } catch (AppException e) {
                e.printStackTrace();
            }
        }
        isLoading.set(false);
    }


    private void getProductSuccess(ServiceResponse<ProductDto> dto) {
        if (dto != null) {
            List<ProductDto> productDtos = dto.getArrData();
            totalPage = dto.getTotalPages();
            arrProductsVM.addNewPage(productDtos, dto.getTotalPages());
            setOrderedProduct(productDtos);

            try {
                view.action("getProductSuccess", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }

        }
        isLoading.set(false);
    }

    private void getPredictionProductSuccess(ProductCustomDTO dto) {
        if (dto != null) {
            List<ProductDto> lstProductDTO = dto.getProductDtoPage().getArrData();
            if (TsUtils.isNotNull(lstProductDTO)) {
                totalPage = dto.getProductDtoPage().getTotalPages();
                arrProductsVM.addNewPage(dto.getProductDtoPage().getArrData(), dto.getProductDtoPage().getTotalPages());
                setOrderedProduct(dto.getProductDtoPage().getArrData());
                if (TsUtils.isNotNull(dto.getLstPrediction())) {
                    lstPrediction = dto.getLstPrediction();
                }
            }
            try {
                view.action("getProductSuccess", null, null, null);
            } catch (AppException e) {
                appException.postValue(e);
            }
        }
        isLoading.set(false);
    }

    //invoke func
    public void onRefresh() {
        isLoading.set(true);
        currentPage = 0;
        arrProductsVM.getBaseModelsE().clear();
//        getProduct();
        getPredictionProduct();
        getFlashSale();
        getGroupon();
        getBannerMarketing();
    }
}
