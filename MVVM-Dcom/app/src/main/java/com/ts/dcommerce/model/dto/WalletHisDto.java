package com.ts.dcommerce.model.dto;


import java.util.Date;

import lombok.Builder;

/**
 * - khi thực hiện order thành công thì sẽ đẩy tiền chiết khấu sang ví
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
@Builder
public class WalletHisDto extends BaseDto{

    public Long walletHisId;

    /**
     *
     */
    public Long orderId;

    /**
     *
     */
    public Double amount;

    /**
     *
     */
    public Date createDate;

    /**
     *
     */
    public Long walletId;
}