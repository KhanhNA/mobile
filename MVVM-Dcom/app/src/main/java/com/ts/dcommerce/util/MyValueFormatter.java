package com.ts.dcommerce.util;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.ts.dcommerce.base.AppController;

public class MyValueFormatter extends ValueFormatter {
    private String suffix;

    public MyValueFormatter(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String getFormattedValue(float value) {
        return AppController.formatNumber(value) + " " + suffix;
    }

}