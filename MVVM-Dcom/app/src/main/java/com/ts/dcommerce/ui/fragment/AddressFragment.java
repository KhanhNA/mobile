package com.ts.dcommerce.ui.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.ts.dcommerce.R;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.MerchantAddressDto;
import com.ts.dcommerce.ui.NewAddressActivity;
import com.ts.dcommerce.util.ToastUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.AddressMerchantVM;
import com.ts.dcommerce.viewmodel.OrderConfirmVM;
import com.ts.dcommerce.viewmodel.PaymentVM;
import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class AddressFragment extends BaseFragment {
    private static AddressFragment mInstance;
    OrderConfirmVM orderConfirmVM;
    BaseAdapter baseAdapter;
    AddressMerchantVM addressMerchantVM;
    PaymentVM paymentVM;
    ProgressDialog pd;

    public static AddressFragment getInstance() {
        if (mInstance == null) {
            mInstance = new AddressFragment();
        }
        return mInstance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        pd = new ProgressDialog(getBaseActivity());
        pd.setMessage(getString(R.string.processing));
        orderConfirmVM = ViewModelProviders.of(getBaseActivity()).get(OrderConfirmVM.class);
        if (AppController.getCurrentMerchant().getMerchantTypeId() == 2) {
            paymentVM = ViewModelProviders.of(this).get(PaymentVM.class);
            paymentVM.setView(this::processFromVM);
            paymentVM.init(orderConfirmVM);

        }
        addressMerchantVM = (AddressMerchantVM) viewModel;

        baseAdapter = new BaseAdapter(R.layout.item_address, addressMerchantVM, this::onItemClick, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);


        addressMerchantVM.init(orderConfirmVM);
        addressMerchantVM.getMerchantAddress();
        return binding.getRoot();
    }

    public void onItemClick(View view, BaseModel model) {
        if (!model.checked) {
            List baseModelsE = addressMerchantVM.getBaseModelsE();
            if (TsUtils.isNotNull(baseModelsE)) {
                model.checked = true;
                if (orderConfirmVM.getMerchantAddressDto() != null) {
                    orderConfirmVM.getMerchantAddressDto().checked = false;
                }
                orderConfirmVM.changeAddress((MerchantAddressDto) model);
                List<MerchantAddressDto> dtos = (List<MerchantAddressDto>) addressMerchantVM.baseModels.get();
                if (dtos != null) {
                    baseAdapter.notifyItemChanged(dtos.indexOf(orderConfirmVM.getMerchantAddressDto()) - 1);
                }

                addressMerchantVM.baseModels.notifyChange();
            }
            orderConfirmVM.getSwipeable().set(true);

        } else {
            model.checked = false;
            orderConfirmVM.resetAddress();
            orderConfirmVM.getSwipeable().set(false);
        }

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        switch (view.getId()) {
            case R.id.btnNext:
                try {
                    if (AppController.getCurrentMerchant().getMerchantTypeId() == 1) {
                        if (orderConfirmVM.getMerchantAddressDto() != null) {
                            EventBus.getDefault().post(1);
                        }
                    } else if (AppController.getCurrentMerchant().getMerchantTypeId() == 2) {
                        if (orderConfirmVM.getMerchantAddressDto() != null) {
                            if (pd != null && !pd.isShowing()) {
                                pd.show();
                            }
                            paymentVM.createOrder(true,null,"","");
                        } else {
                            ToastUtils.showToast(getString(R.string.no_address));
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.lnAddAddress:
                Intent intent = new Intent(getBaseActivity(), NewAddressActivity.class);
                startActivityForResult(intent, 1);
                break;
        }
    }


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_address;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return AddressMerchantVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcAddress;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                addressMerchantVM.getMerchantAddress();

            }
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if ("createOrderSuccess".equals(action)) {
            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }
            ToastUtils.showToast(getString(R.string.SEND_RESQUEST_ORDER_SUCCESS));
            ProductFragmentV2.isChangeProduct = true;
            OrderSuccessDialog dialog = new OrderSuccessDialog(getActivity(), (PaymentVM) viewModel);
            dialog.show(getBaseActivity().getSupportFragmentManager(), dialog.getTag());
        }
    }
}
