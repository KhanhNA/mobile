package com.ts.dcommerce.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

import com.ts.dcommerce.R;

/**
 * @author Good_Boy
 */
public class IconText extends androidx.appcompat.widget.AppCompatTextView {

    public IconText(Context context) {
        super(context);
    }

    public IconText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public IconText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.IconText);
        String customFont = a.getString(R.styleable.IconText_typefaceAsset);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public void setCustomFont(Context ctx, String asset) {
        Typeface typeface;
        try {
            typeface = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {
            Log.e("TextView", "Unable to load typeface: "+e.getMessage());
            return;
        }

        setTypeface(typeface);
    }
}
