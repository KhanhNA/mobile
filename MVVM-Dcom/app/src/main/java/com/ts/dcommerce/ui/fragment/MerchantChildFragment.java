package com.ts.dcommerce.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ns.chat.views.activity.profile.ProfileFragment;
import com.ns.chat.views.manager.ProfileManager;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.MerchantVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import static androidx.core.content.ContextCompat.checkSelfPermission;

@Getter
@Setter
public class MerchantChildFragment extends BaseFragment {
    BaseAdapterV2 contactAdapter;
    MerchantVM merchantVM;
    private int CHANGE_MERCHANT = 8888;

    SearchView searchView;
    LinearLayout searchField;
    private boolean isLoad = true;
    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        merchantVM = (MerchantVM) viewModel;
        contactAdapter = new BaseAdapterV2(R.layout.item_contact, merchantVM.getArrMerchant(), this);
        recyclerView.setAdapter(contactAdapter);
        innitView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isLoad) {
            isLoad = false;
            merchantVM.getTotalRegisters();
            merchantVM.getMerchants();
        }
        searchView.clearFocus();
    }

    private void innitView(View v) {
        searchView = v.findViewById(R.id.searchBarSearchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                List<Object> rs = new ArrayList<>();
                if (TsUtils.isNotNull(merchantVM.getListMerchant())) {
                    for (Object temp : merchantVM.getListMerchant()) {
                        if (StringUtils.isNullOrEmpty(((MerchantDto) temp).getFullName())) {
                            rs.add(temp);
                        } else if (((MerchantDto) temp).getFullName().toUpperCase().contains(newText.toUpperCase()) || ((MerchantDto) temp).getMobilePhone().contains(newText)) {
                            rs.add(temp);
                        }
                    }
                    contactAdapter.update(rs);
                    contactAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });
        searchView.onActionViewExpanded();
        searchView.clearFocus();
    }

    @Override
    public void action(View v, BaseViewModel viewModel) {
//        showProcessing(R.string.wait);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onItemClick(View view, Object baseModel) {
        if (view.getId() == R.id.itemContact) {
            MerchantDto inviteMerchant = (MerchantDto) baseModel;
            if (inviteMerchant.isRegister()) {
                Intent intent = new Intent(getContext(), CommonActivity.class);
                intent.putExtra("FRAGMENT", MerchantDetailFragment.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("contact", inviteMerchant);
                intent.putExtras(bundle);
                startActivityForResult(intent, CHANGE_MERCHANT);
            } else {
                // Show dialog send code
                AlertDialog.Builder builder = new AlertDialog.Builder(getBaseActivity());
                View loginFormView = getLayoutInflater().inflate(R.layout.dialog_input_code_login, null);
                builder.setView(loginFormView);
                TextView txtMessage = loginFormView.findViewById(R.id.txtMessage);
                EditText txtSMS = loginFormView.findViewById(R.id.txtSMS);
                txtSMS.setVisibility(View.VISIBLE);

                txtMessage.setText(getString(R.string.message_sms_invite) + " " + inviteMerchant.getMerchantName());
                (loginFormView.findViewById(R.id.edCode)).setVisibility(View.GONE);
                (loginFormView.findViewById(R.id.txtYourCode)).setVisibility(View.GONE);
                builder.setCancelable(false);
                builder.setPositiveButton("Send message", (dialog, which) -> {
                    try {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setType("vnd.android-dir/mms-sms");
                        smsIntent.putExtra("address", inviteMerchant.getMobilePhone());
                        String message_share = "MStore\n" +
                                String.format(getResources().getString(R.string.msg_invite_l2), AppController.getCurrentMerchant().getFullName()
                                , AppController.LINK_DOWN_APK, AppController.getCurrentMerchant().getActiveCode(), txtSMS.getText());
                        smsIntent.putExtra("sms_body", message_share);
                        startActivity(smsIntent);
                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                    }
                });
                builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
                builder.show();
            }
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if ("getMerchants".equals(action)) {
            getContacts();
            contactAdapter.notifyDataSetChanged();
        }
    }


    public void getContacts() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getBaseActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            merchantVM.getContacts(getBaseActivity());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHANGE_MERCHANT && resultCode == Activity.RESULT_OK) {
            merchantVM.getMerchants();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //get contact từ danh bạ
                getContacts();
                contactAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(getContext(), "Until you grant the permission, we cannot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_merchant_chid;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MerchantVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcContact;
    }

}
