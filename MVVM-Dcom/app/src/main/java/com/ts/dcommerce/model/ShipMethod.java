package com.ts.dcommerce.model;

import com.tsolution.base.BaseModel;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipMethod extends BaseModel implements Serializable {

    private Long shipMethodId;
    private String shipMethodName;
}
