package com.ts.dcommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.ts.dcommerce.R;
import com.ts.dcommerce.viewmodel.LoginVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class ForgetPassFragment extends BaseFragment {

    private LoginVM loginVM;
    private MaterialButton btnForgetPass;
    private TextView txtPhoneNumber;
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        loginVM = (LoginVM) viewModel;
        initView(view);
        return view;
    }

    private void initView(View view) {
        setupToolbar(view);
        btnForgetPass = view.findViewById(R.id.btnForgetPass);
        txtPhoneNumber = view.findViewById(R.id.txtPhoneNumber);

        btnForgetPass.setOnClickListener(v-> loginVM.sendPassword());
    }

    private void setupToolbar(View v){
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(R.string.login);
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if(action.equals("sendOTP")){
            openOtpFragment();
        }
    }

    private void openOtpFragment() {
        Intent intent = new Intent(getContext(), CommonActivity.class);
        intent.putExtra("FRAGMENT", OtpFragment.class);
        intent.putExtra("PHONE_NUMBER", txtPhoneNumber.getText().toString());
        intent.putExtra("TITLE_OTP", getString(R.string.RESET_PASSWORD));
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_forget_pass;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
