package com.ts.dcommerce.model.dto;

import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.PaymentMethod;
import com.ts.dcommerce.model.ShipMethod;
import com.tsolution.base.BaseModel;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OrderDetailDTO extends BaseModel {

    /**
     *
     */
    public Long orderId;

    /**
     * Mã đơn hàng
     */
    public String orderNo;

    /**
     *
     */
    public Long merchantId;
    public Long merchantTypeId;
    /**
     *
     */
    public String merchantName;

    /**
     *
     */
    public PaymentMethod paymentMethod;

    /**
     *
     */
    public Long orderInvoiceId;

    /**
     *
     */
    public ShipMethod shipMethod;

    /**
     *
     */
    public Double shipCharge;

    /**
     *
     */
    public Long shpCartId;

    /**
     *
     */
    public Long recvStoreId;

    /**
     *
     */
    public Double amount;

    /**
     *
     */
    public Double vat;

    /**
     *
     */
    public Double incentiveAmount;

    /**
     *
     */
    public Double quantity;

    /**
     *
     */
    public Double recvLat;

    /**
     *
     */
    public Double recvLong;

    /**
     *
     */
    public String recvAddr;

    /**
     *
     */
    public Integer orderStatus;

    public Double remainQuantity;
    public Double remainAmount;
    public Boolean isSuccess;
    public Integer orderType;
    public Long langId;
    public Long parentMerchantId;
    public Double walletAmount;
    public MerchantAddressDto merchantAddress;
//    public Date orderDate;


    /**
     * Đã thanh toán chưa
     */

    public Integer paymentStatus;

    /**
     * Đơn hàng đã giao cho khách hàng
     */

    public Integer deliveryStatus;

    /**
     * Ngày thanh toán
     */

    public Date paymentDate;

    /**
     * Ngày giao hàng
     */

    /**
     * QR Code
     */
    public String logisticCode;
    public String logisticQRCode;
    public String reason;
    public Date deliveryDate;
    public Date orderDate;
    public String merchantImgUrl;
    public String paymentMethodName;
    public List<OrderPackingDTO> orderPackings;
    public HashMap<String, PackingDescriptionDto> packingDescriptions;
    public List<OrderPackingDTO> promotionPacking;
    private String merchantWareHouseCode;
    private Long shipMethodId;

    public String getAmountStr() {
        return amount == null ? "" : AppController.getInstance().formatCurrency(amount);
    }

    public String getWalletAmountStr() {
        return walletAmount == null ? "" : AppController.getInstance().formatCurrency(walletAmount);
    }
}
