package com.ts.dcommerce.model.db;

import com.tsolution.base.BaseModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Index;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import org.greenrobot.greendao.annotation.Generated;

@Getter
@Setter
@Entity(
        indexes = {
                @Index(value = "id DESC", unique = true)
        }
)
public class DmQuanHuyen extends BaseModel {
    private Long id;
    private String ma ;
    private String ten ;
    private String moTa ;
    private String maTinh;
    private String cap;
    private Integer trangThai;
    private Date ngayTao;
    private Long nguoiTaoId ;
    private Date ngaySua;
    private Long nguoiSuaId ;
@Generated(hash = 1819411918)
public DmQuanHuyen(Long id, String ma, String ten, String moTa, String maTinh,
        String cap, Integer trangThai, Date ngayTao, Long nguoiTaoId,
        Date ngaySua, Long nguoiSuaId) {
    this.id = id;
    this.ma = ma;
    this.ten = ten;
    this.moTa = moTa;
    this.maTinh = maTinh;
    this.cap = cap;
    this.trangThai = trangThai;
    this.ngayTao = ngayTao;
    this.nguoiTaoId = nguoiTaoId;
    this.ngaySua = ngaySua;
    this.nguoiSuaId = nguoiSuaId;
}
@Generated(hash = 2117181724)
public DmQuanHuyen() {
}
public Long getId() {
    return this.id;
}
public void setId(Long id) {
    this.id = id;
}
public String getMa() {
    return this.ma;
}
public void setMa(String ma) {
    this.ma = ma;
}
public String getTen() {
    return this.ten;
}
public void setTen(String ten) {
    this.ten = ten;
}
public String getMoTa() {
    return this.moTa;
}
public void setMoTa(String moTa) {
    this.moTa = moTa;
}
public String getMaTinh() {
    return this.maTinh;
}
public void setMaTinh(String maTinh) {
    this.maTinh = maTinh;
}
public String getCap() {
    return this.cap;
}
public void setCap(String cap) {
    this.cap = cap;
}
public Integer getTrangThai() {
    return this.trangThai;
}
public void setTrangThai(Integer trangThai) {
    this.trangThai = trangThai;
}
public Date getNgayTao() {
    return this.ngayTao;
}
public void setNgayTao(Date ngayTao) {
    this.ngayTao = ngayTao;
}
public Long getNguoiTaoId() {
    return this.nguoiTaoId;
}
public void setNguoiTaoId(Long nguoiTaoId) {
    this.nguoiTaoId = nguoiTaoId;
}
public Date getNgaySua() {
    return this.ngaySua;
}
public void setNgaySua(Date ngaySua) {
    this.ngaySua = ngaySua;
}
public Long getNguoiSuaId() {
    return this.nguoiSuaId;
}
public void setNguoiSuaId(Long nguoiSuaId) {
    this.nguoiSuaId = nguoiSuaId;
}
}
