package com.ts.dcommerce.ui.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.ChildOrderAdapter;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.viewmodel.OrderVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.io.Serializable;

import lombok.Setter;

import static androidx.core.content.ContextCompat.checkSelfPermission;

@Setter
public class OrderChildFragment extends BaseFragment {
    private int currentTab;
    OrderVM orderVM;
    XRecyclerView rcOrders;
    ChildOrderAdapter adapter;
    private int orderType;
    private boolean isLoad = true;

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isLoad) {
            isLoad = false;

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Intent intent = getBaseActivity().getIntent();
        View v = binding.getRoot();
        orderVM = (OrderVM) viewModel;
        if (intent != null && intent.hasExtra("ORDER_TYPE")) {
            orderType = intent.getIntExtra("ORDER_TYPE", -1);
            orderVM.getTypeList().set(orderType);
        }
        getContacts();
        rcOrders = v.findViewById(R.id.rcOrdered);
        adapter = new ChildOrderAdapter(R.layout.item_ordered, orderVM, this);
        adapter.setConfigXRecycler(rcOrders, 1);
        rcOrders.setAdapter(adapter);
        rcOrders.getDefaultRefreshHeaderView().setState(2);
        rcOrders.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                adapter.notifyDataSetChanged();
                orderVM.onRefresh();
            }

            @Override
            public void onLoadMore() {
                orderVM.getMoreOrder();
            }
        });
        orderVM.init(currentTab);
        return v;
    }

    public void getContacts() {
        if (AppController.getCurrentMerchant().getMerchantTypeId() == 1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getBaseActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (OrderFragment.isShowPermission) {
                    OrderFragment.isShowPermission = false;
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
                }
            } else {
                orderVM.getContacts(getBaseActivity());
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Intent intent = getBaseActivity().getIntent();

        if (intent != null && intent.hasExtra("ORDER_TYPE")) {
            orderType = intent.getIntExtra("ORDER_TYPE", -1);
//            orderVM.getTypeList().set(orderType);
            ((OrderVM) viewModel).getOrder(orderType);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            OrderFragment.isShowPermission = true;
            if ((grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getContacts();
            } else {
                Toast.makeText(getContext(), "Until you grant the permission, we cannot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "listOrder":
                rcOrders.refreshComplete();
                rcOrders.loadMoreComplete();
                adapter.notifyDataSetChanged();
                break;
            case "noMore":
                rcOrders.setNoMore(true);
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_order_chid;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onItemClick(View v, Object o) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(getContext(), CommonActivity.class);
        intent.putExtra("FRAGMENT", OrderDetailFragment.class);
        bundle.putSerializable("BASE_MODEL", (Serializable) o);
        intent.putExtras(bundle);
        getBaseActivity().startActivity(intent);
    }
}
