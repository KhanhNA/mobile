package com.ts.dcommerce.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.BaseAdapterV2;
import com.ts.dcommerce.databinding.ActivitySearchBinding;
import com.ts.dcommerce.model.dto.ProductDto;
import com.ts.dcommerce.model.dto.SuggestionSearchDTO;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.util.TsUtils;
import com.ts.dcommerce.viewmodel.SearchVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import java.util.List;
import java.util.Objects;

public class SearchActivity extends BaseActivity<ActivitySearchBinding> {
    SearchVM searchVM;
    private EditText searchView;
    private XRecyclerView rcSuggest;
    private String preKey = "";
    private BaseAdapterV2 dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchVM = (SearchVM) viewModel;
        Handler handler = new Handler();
//        handler.postDelayed(() -> searchView.showSearch(), 100);
        searchView = binding.vSearch;
        rcSuggest = binding.rcSuggest;
        //
        dataAdapter = new BaseAdapterV2(R.layout.item_suggest, searchVM.arrSuggestion, this);
        dataAdapter.setConfigXRecycler(rcSuggest, 1);
        rcSuggest.setAdapter(dataAdapter);
        rcSuggest.setLoadingMoreEnabled(false);
        rcSuggest.setPullRefreshEnabled(false);
        searchView.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence newText, int start,
                                      int before, int count) {
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(() -> {
                    preKey = newText.toString();
                    searchVM.getSearchSuggestion(newText.toString());
                }, 350);
            }
        });
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if ("getSuggestionSuccess".equalsIgnoreCase(action)) {
            rcSuggest.refreshComplete();
//            hideKeyBoard();
            dataAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_search;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return SearchVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        return true;
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
//        Intent intent = new Intent(SearchActivity.this, SubProductActivity.class);
//        intent.putExtra("keyword", ((SuggestionSearchDTO) o).getProductName());
//        startActivity(intent);

        Bundle bundle = new Bundle();
        bundle.putLong("productId", ((SuggestionSearchDTO) o).getProductId());
        bundle.putLong("packingProductId", ((SuggestionSearchDTO) o).getPackingProductId());
        Intent intent = new Intent(SearchActivity.this, ProductDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
