package com.ts.dcommerce.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ts.dcommerce.R;
import com.ts.dcommerce.adapter.MerchantDetailPaperAdapter;
import com.ts.dcommerce.base.AppController;
import com.ts.dcommerce.model.db.ContactDB;
import com.ts.dcommerce.model.dto.MerchantDto;
import com.ts.dcommerce.util.ImageUtils;
import com.ts.dcommerce.util.StringUtils;
import com.ts.dcommerce.viewmodel.MerchantDetailVM;
import com.ts.dcommerce.widget.SlidingTabLayout;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.Objects;

public class MerchantDetailFragment extends BaseFragment {
    MerchantDto merchantDto;
    private static final int REQUEST_READ_EXTERNAL_STORAGE = 101;
    private int SELECT_IMAGE = 9999;
    MerchantDetailVM merchantDetailVM;
    Intent intent;
    Toolbar toolbar;
    SlidingTabLayout slidingTabs;
    ViewPager vpTab;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        merchantDetailVM = (MerchantDetailVM) viewModel;
        intent = Objects.requireNonNull(getActivity()).getIntent();
        if (intent != null && intent.hasExtra("contact")) {
            merchantDto = (MerchantDto) Objects.requireNonNull(intent.getExtras()).getSerializable("contact");
            if (merchantDto != null) {
                merchantDto.setParentMarchantId(AppController.getMerchantId());
                merchantDetailVM.setMerchant(merchantDto);
                initView(view);
            }
        }
        if(intent.hasExtra("accountName")){
            String merchantName = intent.getStringExtra("accountName");
            merchantDetailVM.setMerchant(merchantName);
        }
        if(merchantDto == null){
            merchantDto = new MerchantDto();

        }
        return view;
    }

    private void initView(View v) {
        slidingTabs = v.findViewById(R.id.slidingTab);
        vpTab = v.findViewById(R.id.vpContent);
        slidingTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        slidingTabs.setDistributeEvenly(true);
        MerchantDetailPaperAdapter myPagerAdapter = new MerchantDetailPaperAdapter(getChildFragmentManager(), getResources().getStringArray(R.array.title_merchant_detail), merchantDto);
        vpTab.setAdapter(myPagerAdapter);
        vpTab.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        slidingTabs.setViewPager(vpTab);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        vpTab.setPageMargin(pageMargin);
        vpTab.setCurrentItem(0);



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = getBaseActivity().findViewById(R.id.toolbar);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        assert appCompatActivity != null;
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(merchantDto.getFullName());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            case R.id.action_edit:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        }
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_READ_EXTERNAL_STORAGE);
                    } else {
                        intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                    }
                }
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                } else {
                    //If user presses deny
                    Toast.makeText(getContext(), getString(R.string.permissions_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Objects.requireNonNull(getActivity()).setResult(Activity.RESULT_OK, intent);
                        Uri selectedImageUri = data.getData();
                        //Bitmap img = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
                        String picturePath;
                        int buildVertion = Build.VERSION.SDK_INT;
                        if (buildVertion < Build.VERSION_CODES.KITKAT && buildVertion > Build.VERSION_CODES.HONEYCOMB) {
                            picturePath = ImageUtils.getRealPathFromURI_API11to18(getContext(), selectedImageUri);
                        } else {
                            picturePath = ImageUtils.getRealPathFromURI_API19(getContext(), selectedImageUri);
                        }
                        if (!StringUtils.isNullOrEmpty(picturePath)) {
                            ContactDB contactDB = new ContactDB(merchantDetailVM.getModel().get().getMobilePhone(),
                                    picturePath);
                            merchantDetailVM.saveImg(contactDB);
                            //modelContact.insertImg(txtPhone.getText().toString(), picturePath);
                            merchantDetailVM.getModel().get().setMerchantImgUrl(picturePath);
                            merchantDetailVM.getModel().notifyChange();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getContext(), getString(R.string.canceled), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if("getMerchant".equals(action)){
            merchantDto = merchantDetailVM.getModel().get();
            this.initView(this.binding.getRoot());
            AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
            appCompatActivity.getSupportActionBar().setTitle(merchantDto.getFullName());
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_merchant_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MerchantDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

}
