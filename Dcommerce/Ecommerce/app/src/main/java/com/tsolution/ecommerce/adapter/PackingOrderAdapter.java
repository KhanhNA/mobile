package com.tsolution.ecommerce.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.dto.OrderPackingDTO;
import com.tsolution.ecommerce.utils.UtilsTs;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PackingOrderAdapter extends RecyclerView.Adapter<PackingOrderAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<OrderPackingDTO> mData;

    public PackingOrderAdapter(Context mContext, ArrayList<OrderPackingDTO> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (UtilsTs.isNotNull(mData)) {
            OrderPackingDTO dto = mData.get(i);
            viewHolder.txtTile.setText(dto.getPackingName());
            viewHolder.txtAmount.setText("Tổng: "+AppController.getInstance().formatCurrency(dto.getAmount()));
            viewHolder.txtQuantity.setText(String.valueOf(dto.getQuantity()));
            viewHolder.txtPrice.setText(AppController.getInstance().formatCurrency(dto.getPrice()));
//            if (dto.getIncentiveAmount() != null && dto.getIncentiveAmount() > 0) {
                viewHolder.txtIncentiveAmount.setText("( "+String.valueOf(mContext.getString(R.string.DISCOUNT)) + "-"+AppController.getInstance().formatCurrency(20000d)+" )");

//            }
            Picasso.with(mContext).load(dto.getPackingUrl()).placeholder(R.drawable.logots).into(viewHolder.imgOrder);
        }
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgOrder)
        ImageView imgOrder;
        @BindView(R.id.txtTile)
        TextView txtTile;
        @BindView(R.id.txtAmount)
        TextView txtAmount;
        @BindView(R.id.txtQuantity)
        TextView txtQuantity;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtIncentiveAmount)
        TextView txtIncentiveAmount;

        ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
