package com.tsolution.ecommerce.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class ModelContact {
    SQLiteDatabase database;

    public void getConnectToSQL(Context context) {
        DataBaseSqlLite contact = new DataBaseSqlLite(context);
        database = contact.getWritableDatabase();
    }

    public void insertImg(String id, String img) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseSqlLite.TB_CONTACT_ID, id);
        contentValues.put(DataBaseSqlLite.TB_CONTACT_IMG, img);

        long check = database.insert(DataBaseSqlLite.TB_CONTACT, null, contentValues);
        if (check <= 0) {
            database.update(DataBaseSqlLite.TB_CONTACT, contentValues, DataBaseSqlLite.TB_CONTACT_ID + " = '" + id + "' ", null);

        }
    }


    public String getImage(String phoneNumber) {
        ArrayList<String> params = new ArrayList<>();
        String qu = "SELECT CONTACT_IMG FROM CONTACT WHERE CONTACT_ID = ?";
        params.add(phoneNumber);
        Cursor cur = database.rawQuery(qu, params.toArray(new String[params.size()]));
        if (cur.moveToFirst()) {
            String img = cur.getString(0);
            cur.close();
            return img;
        }
        if (cur != null && !cur.isClosed()) {
            cur.close();
        }

        return null;
    }

    public HashMap<String, String> getImages() {
        HashMap<String, String> imgs = new HashMap<>();
        String qu = "SELECT * FROM CONTACT ";
        try {
            Cursor cur = database.rawQuery(qu, null);
            if (cur != null) {
                cur.moveToFirst();
                do {
                    String id = cur.getString(cur.getColumnIndex(DataBaseSqlLite.TB_CONTACT_ID));
                    String img = cur.getString(cur.getColumnIndex(DataBaseSqlLite.TB_CONTACT_IMG));
                    imgs.put(id, img);
                } while (cur.moveToNext());
                cur.close();
            }
        } catch (Exception e){
            Log.e("aaa", e + "");
        }
//        cur.close();
//        if (cur != null && !cur.isClosed()) {
//            cur.close();
//        }
        return imgs;
    }
}
