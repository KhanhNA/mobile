package com.tsolution.ecommerce.db;

import android.content.*;
import android.database.sqlite.*;

public class DataBaseSqlLite extends SQLiteOpenHelper {

    public static String TB_GIOHANG = "CART";
    public static String TB_GIOHANG_MASP = "PRODUCT_ID";
    public static String TB_GIOHANG_TENSP = "PRODUCT_NAME";
    public static String TB_GIOHANG_GIATIEN = "PRICE";
    public static String TB_GIOHANG_HINHANH = "IMAGE";
    public static String TB_GIOHANG_SOLUONG = "QUANTITY";
    public static String TB_GIOHANG_SOLUONG_SANPHAM = "QUANTITY_OF_PRODUCT";
    public static String TB_CONTACT = "CONTACT";
    public static String TB_CONTACT_ID = "CONTACT_ID";
    public static String TB_CONTACT_IMG = "CONTACT_IMG";
    public static String TB_YEUTHICH = "YEUTHICH";
    public static String TB_YEUTHICH_MASP = "MASP";
    public static String TB_YEUTHICH_TENSP = "TENSP";
    public static String TB_YEUTHICH_GIATIEN = "GIATIEN";
    public static String TB_YEUTHICH_HINHANH = "HINHANH";

    public DataBaseSqlLite(Context context ) {
        super(context, "SQLLITE", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //giỏ hàng
        String tbGioHang = "CREATE TABLE " + TB_GIOHANG + " (" + TB_GIOHANG_MASP + " INTEGER PRIMARY KEY , "
                + TB_GIOHANG_TENSP + " TEXT, " + TB_GIOHANG_GIATIEN + " REAL, " +TB_GIOHANG_HINHANH + "  TEXT, "
                + TB_GIOHANG_SOLUONG_SANPHAM +" INTEGER, "
                + TB_GIOHANG_SOLUONG + " INTEGER);";

        //yêu thích
        String tbYeuThich = "CREATE TABLE " + TB_YEUTHICH + " (" + TB_YEUTHICH_MASP + " INTEGER PRIMARY KEY , "
                + TB_YEUTHICH_TENSP + " TEXT, " + TB_YEUTHICH_GIATIEN + " REAL, " +TB_YEUTHICH_HINHANH + "  BLOB);";

        //contact
        String tbContact = "CREATE TABLE " + TB_CONTACT + " ("+
                TB_CONTACT_ID + " TEXT PRIMARY KEY, " +
                TB_CONTACT_IMG + " TEXT);";

        db.execSQL(tbContact);
        db.execSQL(tbGioHang);
        db.execSQL(tbYeuThich);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
