package com.tsolution.ecommerce.widget;

public interface ILoadMore {
    void onLoadMore();
}
