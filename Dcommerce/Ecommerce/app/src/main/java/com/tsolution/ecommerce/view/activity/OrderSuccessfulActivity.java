package com.tsolution.ecommerce.view.activity;

import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.dto.*;
import com.tsolution.ecommerce.service.listerner.*;

import butterknife.*;

public class OrderSuccessfulActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.txtIdOrder)
    TextView txtIdOrder;
    @BindView(R.id.btnHome)
    Button btnHome;

    private long orderId;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_order_success);
        ButterKnife.bind(this);
        btnHome.setOnClickListener(this);
        Intent i = getIntent();
        orderId = i.getLongExtra(IntentConstans.INTENT_ORDER_SUCCESS,0);
        if (  orderId > 0)
            txtIdOrder.setText(String.valueOf(orderId));
    }

    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.btnHome) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
