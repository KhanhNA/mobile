/**
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.tsolution.ecommerce.utils;

import android.content.*;
import android.os.*;
import android.util.*;


import java.text.*;
import java.util.*;

/**
 * Chua cac ham util ve date
 *
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class DateUtils {
	//15 phut
	public static final long DISTANCE_TIME_ROUTING = 15 * 60 * 1000;
	/** The Constant SECOND. */
	public static final long SECOND = 1000;

	/** The Constant MINUTE. */
	public static final long MINUTE = SECOND * 60;

	/** The Constant HOUR. */
	public static final long HOUR = MINUTE * 60;
	// chua kiem tra duoc thoi gian
	public final static int TIME_NOT_CHECK = -1;
	// thoi gian khong hop le
	public final static int TIME_INVALID = 0;
	// thoi gian hop le
	public final static int TIME_VALID = 1;

	public static final String DATE_FORMAT_FILE_DEFAULT = "yyyy_MM_dd_HH_mm_ss";
	public static final String DATE_FORMAT_DEFAULT = "dd_MM_yyyy_HH_mm_ss";
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_STRING_DD_MM_YYYY = "dd/MM/yyyy";
	public static final String DATE_STRING_YYYY_MM_DD = "yyyy-MM-dd";
	//public static SimpleDateFormat defaultDateFormat_2 = new SimpleDateFormat(DATE_FORMAT_NOW);
	public static final String defaultDateFormat_3 = new String("dd/MM/yyyy HH:mm");
	//public static SimpleDateFormat defaultSqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	//public static SimpleDateFormat defaultSqlFullDateFormat = new SimpleDateFormat(DATE_FORMAT_NOW);
	public static final String DATE_FORMAT_ATTENDANCE = "yyyy-MM-dd HH:mm";
	public static final String DATE_FORMAT_HOUR_MINUTE = "HH:mm";
	//public static SimpleDateFormat defaultHourMinute = new SimpleDateFormat(DATE_FORMAT_HOUR_MINUTE);
	public static final String DATE_FORMAT_DATE="yyyy-MM-dd";
	public static final String DATE_FORMAT_DATE_PAY_RECEIVED="ddMMyy";
	public static final String DATE_TIME_FORMAT_VN = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_TIME_FORMAT_TIME = "HH:mm:ss";
	public static final String DATE_FORMAT_FILE_EXPORT = "yyyyMMddHHmmss";
	public static final String DATE_FORMAT_DATE_VN="dd/MM/yyyy";
	//public static SimpleDateFormat defaultDateFormat = new SimpleDateFormat(DATE_FORMAT_DATE_VN);
	public static final String DATE_FORMAT_YEAR_MONTH_DATE="yyyy/MM/dd";

	public static final String DATE_FORMAT_MONTH_YEAR="yyyy-MM";
	public static final String FORMAT_MONTH_YEAR = "MM/yyyy";
	public static final String FORMAT_DAY = "dd";
	public static final String FORMAT_MONTH = "MM";
	public static final String FORMAT_YEAR = "yyyy";

	public static final long ONE_MINUTE_IN_MILLIS = 60000;// millisecs

	public static final int RIGHT_TIME = 0;
	public static final int WRONG_DATE = 1;
	public static final int WRONG_TIME = 2;
	public static final int WRONG_TIME_BOOT_REASON = 3;

	public static final int GMT7Offset = 60 * 60 * 1000 * 7;

	/**
	 * Lay thoi gian hien tai theo: yyyy-MM-dd HH:mm:SS
	 * @author : BangHN since : 12:00:32 PM
	 */
	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}

	public static Date nowDate() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		try {
			return sdf.parse(sdf.format(cal.getTime()));
		} catch (ParseException e) {
			return new Date();
		}
	}

	/**
	 * Convert date sang mot dinh dang truyen vao
	 *
	 * @author : BangHN since : 1.0
	 */
	public static String convertDateTimeWithFormat(Date date, String format) {
		if(date != null){
			if (StringUtils.isNullOrEmpty(format)) {
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
				return sdf.format(date);
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				return sdf.format(date);
			}
		}else{
			return null;
		}
	}

	/**
	 * Lay thoi gian theo format truyen vao
	 *
	 * @author : BangHN since : 11:57:44 AM
	 */
	public static String getCurrentDateTimeWithFormat(String strFormat) {
		Date currentDateTime = new Date();
		SimpleDateFormat format;
		if (StringUtils.isNullOrEmpty(strFormat)) {
			format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
		} else {
			format = new SimpleDateFormat(strFormat);
		}
		return format.format(currentDateTime);
	}

	/**
	 * getCurrentDate
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public static int getCurrentDay() {
		Date d = new Date(System.currentTimeMillis());
		Calendar.getInstance().setTime(d);
		int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		int indexDay = -1;
		switch (day) {
		case Calendar.MONDAY:
			indexDay = 0;
			break;
		case Calendar.TUESDAY:
			indexDay = 1;
			break;
		case Calendar.WEDNESDAY:
			indexDay = 2;
			break;
		case Calendar.THURSDAY:
			indexDay = 3;
			break;
		case Calendar.FRIDAY:
			indexDay = 4;
			break;
		case Calendar.SATURDAY:
			indexDay = 5;
			break;
		case Calendar.SUNDAY:
			indexDay = 6;
			break;
		}
		return indexDay;
	}

	/**
	 *
	 * Lay tuyen hien tai(T2-->T7)
	 *
	 * @author : DoanDM since : 11:08:34 AM
	 */
//	public static String getCurrentLine() {
//		String rs = "";
//		Date d = new Date(System.currentTimeMillis());
//		Calendar.getInstance().setTime(d);
//		int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
//		boolean isVI = true;
//		if(!GlobalInfo.getInstance().loadLanguages().getValue().contains("vi"))
//		{
//			isVI = false;
//		}
//		switch (day) {
//		case Calendar.MONDAY:
//			rs = Constants.getDayLine2()[0];
//			if (!isVI)
//				rs = "MO";
//
//			break;
//		case Calendar.TUESDAY:
//			rs = Constants.getDayLine()[1];
//			if (!isVI)
//				rs = "TU";
//			break;
//		case Calendar.WEDNESDAY:
//			rs = Constants.getDayLine()[2];
//			if (!isVI)
//				rs = "WE";
//			break;
//		case Calendar.THURSDAY:
//			rs = Constants.getDayLine()[3];
//			if (!isVI)
//				rs = "TH";
//			break;
//		case Calendar.FRIDAY:
//			rs = Constants.getDayLine()[4];
//			if (!isVI)
//				rs = "FR";
//			break;
//		case Calendar.SATURDAY:
//			rs = Constants.getDayLine()[5];
//			if (!isVI)
//				rs = "SA";
//			break;
//		case Calendar.SUNDAY:
//			rs = Constants.getDayLine()[6];
//			if (!isVI)
//				rs = "SU";
//			break;
//		}
//		return rs;
//	}

	/**
	 * getCurrentDate
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
//	public static String getToday() {
//		String today = "";
//		Date d = new Date(System.currentTimeMillis());
//		Calendar.getInstance().setTime(d);
//		int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
//		switch (day) {
//		case Calendar.MONDAY:
//			today = Constants.getToday()[0];
//			break;
//		case Calendar.TUESDAY:
//			today = Constants.getToday()[1];
//			break;
//		case Calendar.WEDNESDAY:
//			today = Constants.getToday()[2];
//			break;
//		case Calendar.THURSDAY:
//			today = Constants.getToday()[3];
//			break;
//		case Calendar.FRIDAY:
//			today = Constants.getToday()[4];
//			break;
//		case Calendar.SATURDAY:
//			today = Constants.getToday()[5];
//			break;
//		case Calendar.SUNDAY:
//			today = Constants.getToday()[6];
//			break;
//		}
//		return today;
//	}

	// hàm này không translate
	public static String getNameDayOfWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		switch (c.get(Calendar.DAY_OF_WEEK)) {
			case Calendar.SUNDAY:
				return "SUNDAY";
			case Calendar.MONDAY:
				return "MONDAY";
			case Calendar.TUESDAY:
				return "TUESDAY";
			case Calendar.WEDNESDAY:
				return "WEDNESDAY";
			case Calendar.THURSDAY:
				return "THURSDAY";
			case Calendar.FRIDAY:
				return "FRIDAY";
			case Calendar.SATURDAY:
				return "SATURDAY";
			default:
				break;
		}
		return "";
	}

	/**
	 *
	 * Lay chuoi ngay thang nam yyyy-MM-dd HH:mm:ss tu dd/MM/yyyy HH:mm
	 *
	 * @author: Nguyen Thanh Dung
	 * @param strDate
	 * @param strTime
	 * @return
	 * @return: String
	 * @throws:
	 */
	public static String getDateTimeStringFromDateAndTime(String strDate, String strTime) {
		String[] days = strDate.split("/");
		StringBuilder sbDateTime = new StringBuilder();

		if (days.length >= 3) {
			sbDateTime.append(days[2]);
			sbDateTime.append("-");
			sbDateTime.append(days[1]);
			sbDateTime.append("-");
			sbDateTime.append(days[0]);

			sbDateTime.append(" ");
		}

		if (StringUtils.isNullOrEmpty(strTime)) {
			sbDateTime.append("00:00");
		} else{
			sbDateTime.append(strTime);
		}
		//add second data
		sbDateTime.append(":00");

		return sbDateTime.toString().trim();
	}

	/**
	 * Lay ngay lam viec giua 2 ngay
	 *
	 * @author : ? since : 1.0
	 */
	public static int getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
		SimpleDateFormat sfd = new SimpleDateFormat("yyyyMMdd");
		try {
			startDate = sfd.parse(sfd.format(startDate));
			endDate = sfd.parse(sfd.format(endDate));
		} catch (Exception e) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		Calendar startCal;
		Calendar endCal;
		startCal = Calendar.getInstance();
		startCal.setTime(startDate);
		endCal = Calendar.getInstance();
		endCal.setTime(endDate);
		int workDays = 1;
		// Return 0 if start and end are the same
		if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
			startCal.setTime(endDate);
			endCal.setTime(startDate);
		}
//		if (startCal.getTimeInMillis() == endCal.getTimeInMillis()
//				&& startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
//			return 0;
//		}
		if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
			return 1;
		}
		do {
//			if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				++workDays;
//			}
			startCal.add(Calendar.DAY_OF_MONTH, 1);
		} while (startCal.getTimeInMillis() < endCal.getTimeInMillis());
		return workDays;
	}

	/**
	 * Lay ngay dau tien trong tahang
	 *
	 */
	public static Date getStartTimeOfMonth(Date date) {
		SimpleDateFormat sfd = new SimpleDateFormat("yyyy/MM");
		try {
			return sfd.parse(sfd.format(date));
		} catch (Exception e) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return null;
	}

	/**
	 * Lay ngay dau tien trong tahang
	 *
	 * @author : ? since : 1.0
	 */
	public static Date getStartTimeOfDay(Date date) {
		SimpleDateFormat sfd = new SimpleDateFormat("yyyy/MM/dd");
		try {
			return sfd.parse(sfd.format(date));
		} catch (Exception e) {
			Log.e("aaa","aaaa", e);
		}
		return null;
	}

	/**
	 *
	 * compare one Date with today . -1 : strDay < today , 0: strDay = today, 1:
	 * strDay > today
	 *
	 * @param strDay
	 * @return
	 * @return: int
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 8, 2012
	 */
	public static int isCompareWithToDate(String strDay) {
		int kq = 0;
		SimpleDateFormat sfd = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date dayFormat = sfd.parse(strDay);
			Date toDay = sfd.parse(getCurrentDate());
			kq = dayFormat.compareTo(toDay);
		} catch (Exception e) {
			Log.e("", "",e);
		}
		return kq;
	}

	/**
	 * kiem tra mot ngay phai la ngay chu nhat?
	 *
	 * @author : ? since : 1.0
	 */
	public static boolean isSunday(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
	}

	/**
	 * Lay nam hien tai
	 *
	 * @author : ? since : 1.0
	 */
	public static int getYear(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.YEAR);
	}

	/**
	 * Lay thang hien tai
	 *
	 * @author : ? since : 1.0
	 */
	public static int getMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MONTH);
	}

	/**
	 * Lay ngay hien tai
	 *
	 * @author : ? since : 1.0
	 */
	public static int getDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DAY_OF_MONTH);
	}

//	public static String getDayOfWeek(Date date) {
//		Calendar c = Calendar.getInstance();
//		c.setTime(date);
//		switch (c.get(Calendar.DAY_OF_WEEK)) {
//		case Calendar.SUNDAY:
//			return StringUtils.getString(R.string.TEXT_SUNDAY);
//		case Calendar.MONDAY:
//			return StringUtils.getString(R.string.TEXT_MONDAY);
//		case Calendar.TUESDAY:
//			return StringUtils.getString(R.string.TEXT_TUESDAY);
//		case Calendar.WEDNESDAY:
//			return StringUtils.getString(R.string.TEXT_WEDNESDAY);
//		case Calendar.THURSDAY:
//			return StringUtils.getString(R.string.TEXT_THURSDAY);
//		case Calendar.FRIDAY:
//			return StringUtils.getString(R.string.TEXT_FRIDAY);
//		case Calendar.SATURDAY:
//			return StringUtils.getString(R.string.TEXT_SATURDAY);
//		default:
//			break;
//		}
//		return "";
//	}

	/**
	 * Parse date tu chuoi date trong sqlLite
	 *
	 * @author: TruongHN
	 * @param dateSql
	 * @return
	 * @return: String
	 * @throws:
	 */
	public static String parseDateFromSqlLite(String dateSql) {
		String date = "";
		if (!StringUtils.isNullOrEmpty(dateSql)) {
			// 2012-08-01 14:09:21.0

			int index = dateSql.indexOf(".");
			if (index > -1) {
				date = dateSql.substring(0, index);
			} else {
				date = dateSql;
			}
		}
		return date;
	}

	/**
	 * getVisitPlan
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
//	public static String getVisitPlan(String date) {
//		String d = "";
////		if (date.equals(Constants.getArrayLineChoose()[0])) {
////			d = Constants.getDayLine()[0];
////		} else if (date.equals(Constants.getArrayLineChoose()[1])) {
////			d = Constants.getDayLine()[1];
////		} else if (date.equals(Constants.getArrayLineChoose()[2])) {
////			d = Constants.getDayLine()[2];
////		} else if (date.equals(Constants.getArrayLineChoose()[3])) {
////			d = Constants.getDayLine()[3];
////		} else if (date.equals(Constants.getArrayLineChoose()[4])) {
////			d = Constants.getDayLine()[4];
////		} else if (date.equals(Constants.getArrayLineChoose()[5])) {
////			d = Constants.getDayLine()[5];
////		} else if (date.equals(Constants.getArrayLineChoose()[6])) {
////			d = Constants.getDayLine()[6];
////		} else if (date.equals(Constants.getArrayLineChoose()[7])) {
////			d = "";
////		}
//
//		if ("Thứ 2".contains(date) || "Monday".contains(date) ){
//			d = "T2";
//		} else if ("Thứ 3".contains(date) || "Tuesday".contains(date)) {
//			d = "T3";
//		} else if ("Thứ 4".contains(date) || "Wednesday".contains(date)) {
//			d = "T4";
//		} else if ("Thứ 5".contains(date) || "Thursday".contains(date)) {
//			d = "T5";
//		} else if ("Thứ 6".contains(date) || "Friday".contains(date)) {
//			d = "T6";
//		} else if ("Thứ 7".contains(date) || "Saturday".contains(date)) {
//			d = "T7";
//		} else if ("Chủ nhật".contains(date) || "Sunday".contains(date)) {
//			d = "CN";
//		} else if (date.equals(Constants.getArrayLineChoose()[7])) {
//			d = "";
//		}
//		return d;
//	}

	public static String convertFormatDate(String strDate, String fromFormat, String toFormat) {
		String strToDate = strDate;
		SimpleDateFormat fromFM = new SimpleDateFormat(fromFormat);
		SimpleDateFormat toFM = new SimpleDateFormat(toFormat);
		try {
			Date dateFrom = fromFM.parse(strDate);
			strToDate = toFM.format(dateFrom);
		} catch (ParseException e) {
			Log.w("", e);
		}
		return strToDate;
	}

	/**
	 * Kiem tra thoi gian hop le cua client voi server
	 *
	 * @author BangHN
	 * @param createTime
	 * @return
	 */
//	public static int checkTimeClientAndServer(String strServerDate) {
//		// kiem tra thoi gian hop le khong
//		SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
//
//		Date dateTimeServer;
//		Date dateTimeClient;
//		int validTime = TIME_NOT_CHECK; // mac dinh la chua check duoc
//
//		try {

//			if (!StringUtilss.isNullOrEmpty(strServerDate)) {
//				// thoi gian hien tai
//				String currentDateClient = DateUtils.now();
//
//				dateTimeServer = formatterDateTime.parse(strServerDate);
//				dateTimeClient = formatterDateTime.parse(currentDateClient);
//
//				Date dateServer = formatterDate.parse(strServerDate);
//				Date dateClient = formatterDate.parse(currentDateClient);
//
//				long secs = (dateTimeServer.getTime() - dateTimeClient.getTime()) / 1000;
//				int hours = (int) secs / 3600;
//
//				// neu gio cach biet nhau 1 h -> fail
//				// neu ngay khac ngay hien tai -> fail
//				if (Math.abs(hours) >= GlobalInfo.getInstance().getTimeTestOrder()
//						|| dateClient.compareTo(dateServer) != 0) {
//					// thoi gian khong hop le
//					validTime = TIME_INVALID;
//				} else {
//					// thoi gian hop le
//					validTime = TIME_VALID;
//				}
//			}
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
//		}
//		return validTime;
//	}

	/**
	 * Check thoi gian sau khi relogin hay nhan cap nhat du lieu Neu sau mot
	 * ngay login thi yeu cau ra man hinh login de cap nhat du lieu
	 *
	 * @author banghn
	 */
//	public static boolean checkTimeToShowLogin(String serverDate) {
//		boolean isValid = true;
//		SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
//		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
//		String dateBefore = sharedPreferences.getString(LoginView.VNM_SV_DATE, "");
//
//		Date dateAfterRelogin = null;
//		Date dateBeforeRelogin = null;
//		try {
//			if (!StringUtils.isNullOrEmpty(serverDate)) {
//				dateAfterRelogin = formatterDate.parse(serverDate);
//			}
//			if (!StringUtils.isNullOrEmpty(dateBefore)) {
//				dateBeforeRelogin = formatterDate.parse(dateBefore);
//			}
//			if (dateAfterRelogin != null && dateBeforeRelogin != null
//					&& dateAfterRelogin.compareTo(dateBeforeRelogin) != 0) {
//				isValid = false;
//			}
//		} catch (Exception e1) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
//		}
//		return isValid;
//	}

	public static String getCurrentDate() {
		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		return getDefaultDateFormat().format(cal.getTime());
	}

	/**
	 *
	 * Lay gia tri hien tai cua 1 loai thoi gian nao do
	 *
	 * @author: Nguyen Thanh Dung
	 * @param timeType
	 * @return
	 * @return: int
	 * @throws:
	 */
	public static int getCurrentTimeByTimeType(int timeType) {
		int currentTime = 0;
		Date now = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);

		switch (timeType) {
		case Calendar.MONTH: {
			currentTime = calendar.get(Calendar.MONTH) + 1;
			break;
		}
		case Calendar.YEAR: {
			currentTime = calendar.get(Calendar.YEAR);
			break;
		}
		case Calendar.HOUR_OF_DAY: {
			currentTime = calendar.get(Calendar.HOUR_OF_DAY);
			break;
		}

		default:
			break;
		}

		return currentTime;
	}

	/**
	 *
	 * Convert 1 chuoi ngay tu 1 format sang 1 format khac
	 *
	 * @author: Nguyen Thanh Dung
	 * @param date
	 * @param fromFormat
	 * @param toFormat
	 * @return
	 */
	public static String convertDateOneFromFormatToAnotherFormat(String date, String fromFormat, String toFormat) {
		Date tn;
		String result = "";
		if (!StringUtils.isNullOrEmpty(date)) {
			try {
				tn = StringUtils.stringToDate(date, fromFormat);
				result = StringUtils.dateToString(tn, toFormat);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.w("", e);
			}
		}

		return result;
	}

	/**
	 * lay ngay cuoi cung cua so thang truoc
	*
	* @author: trungnt56
	* @param: @return
	* @return: String
	* @throws:
	 */
	public static String getLastDateOfNumberPreviousMonthWithFormat(String strFormat, int numberPreviousMonth) {
		Calendar cal= Calendar.getInstance();
		SimpleDateFormat format;
		if (StringUtils.isNullOrEmpty(strFormat)) {
			format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
		} else {
			format = new SimpleDateFormat(strFormat);
		}
		cal.add(Calendar.MONTH, numberPreviousMonth);
		cal.set(Calendar.DATE, Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
		//cal.add(Calendar.DATE, -1);
		return format.format(cal.getTime());
	}

	/**
	 * Lay so ngay giua 2 ngay bat ky
	 *
	 * @param d1
	 *            : ngay dau
	 * @param d2
	 *            : ngay sau
	 * @author banghn
	 * @return
	 */
	public static int daysBetween(Date d1, Date d2) {
		if ((d1 != null) && (d2 == null)) {
			return -1;
		} else if ((d1 == null) && (d2 != null)) {
			return 1;
		} else if ((d1 == null)) {
			return 0;
		}
		return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * get num minutes two time in date theo temple HH:mm
	 * @param strTime1
	 * @param strTime2
	 * @return: long
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 22, 2013
	 */
	public static long getDistanceMinutesFrom2Hours(String strTime1, String strTime2) {
		long distance = 0;
		DateFormat formatter = new SimpleDateFormat("HH:mm");
		try {
			Date date1 = formatter.parse(strTime1);
			Date date2 = formatter.parse(strTime2);

			int hours1 = date1.getHours();
			int hours2 = date2.getHours();

			long distanceHours = hours2 - hours1;
			long distanceMinutes = date2.getMinutes() - date1.getMinutes();
			if (distanceHours >= 0) {
				distance = distanceHours * 60 + distanceMinutes;
			}
		} catch (Exception e) {
			Log.w("", e);
		}
		return distance;
	}


	/**
	 * lay so phut giua thoi gian theo dang yyyy-MM-dd HH:mm:ss
	 * @author: BANGHN
	 * @param strDate1 : ngay gio bat dau
	 * @param strDate2 : ngay gio ket thuc
	 * @return : so phut (minutes)
	 */
	public static long getDistanceMinutesFrom2Date(String strDate1, String strDate2) {
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_FORMAT_NOW);
		Date startTimeDate = new Date();
		try {
			startTimeDate = sdf.parse(strDate1);
		} catch (Exception e) {
			Log.w("", e);
		}
		Date endTimeDate = new Date();
		try {
			endTimeDate = sdf.parse(strDate2);
		} catch (Exception e) {
			Log.w("", e);
		}
		long diff = endTimeDate.getTime() - startTimeDate.getTime();
		long seconds = diff / 1000;
		long minutes = seconds / 60;

		//seconds = seconds % 60;
		minutes = minutes % 60;

		return minutes;
	}

	/**
	 * lay so giay giua thoi gian theo dang yyyy-MM-dd HH:mm:ss
	 * @author: BANGHN
	 * @param strDate1 : ngay gio bat dau
	 * @param strDate2 : ngay gio ket thuc
	 * @return : so phut (minutes)
	 */
	public static long getDistanceSecondFrom2Date(String strDate1, String strDate2) {
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_FORMAT_NOW);
		Date startTimeDate = new Date();
		try {
			startTimeDate = sdf.parse(strDate1);
		} catch (Exception e) {
			Log.w("",e);
		}
		Date endTimeDate = new Date();
		try {
			endTimeDate = sdf.parse(strDate2);
		} catch (Exception e) {
			Log.w("", e);
		}
		long diff = endTimeDate.getTime() - startTimeDate.getTime();
		long seconds = diff / 1000;

		return seconds;
	}

	/**
	 * Kiem tra co phai hien tai nam trong thoi gian cham cong hay khong
	 *
	 * @author banghn
	 * @return: Co hay khong
	 */
//	public static boolean isInAttendaceTime() {
//		boolean isIn = false;
//		String dateNow = DateUtils.now();
//		SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.DATE_FORMAT_ATTENDANCE);
//		Date currentDate = new Date();
//		Date startDate = new Date();
//		Date endDate = new Date();
//
//		try {
//			// starttime
//			String startTime = dateNow.split(" ")[0];
//			if(!StringUtilss.isNullOrEmpty(GlobalInfo.getInstance().getCcStartTime())) {
//				startTime += " " + GlobalInfo.getInstance().getCcStartTime();
//			} else {
//				startTime += " 00:00";
//			}
//
//			// endtime
//			String endTime = dateNow.split(" ")[0];
//			if(!StringUtils.isNullOrEmpty(GlobalInfo.getInstance().getCcEndTime())) {
//				endTime += " " + GlobalInfo.getInstance().getCcEndTime();
//			} else {
//				endTime += " 00:00";
//			}
//
//			currentDate = formatter.parse(dateNow);
//			startDate = formatter.parse(startTime);
//			endDate = formatter.parse(endTime);
//		} catch (Exception e) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		}
//
//		// Trong thoi gian cham cong
//		if (currentDate.compareTo(startDate) >= 0 && currentDate.compareTo(endDate) <= 0) {
//			isIn = true;
//		}
//		return isIn;
//	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @return
	 * @return: booleanvoid
	 * @throws Exception
	 * @throws:
	 */
	public static boolean isIn30Min(String t) throws Exception {
		boolean isValid;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, -30);
		Date time30 = calendar.getTime();

		DateFormat formatter = new SimpleDateFormat(DateUtils.DATE_FORMAT_NOW);
		Date time = formatter.parse(t);

		if (time30.after(time)) {
			isValid = false;
		} else {
			isValid = true;
		}

		return isValid;
	}

	public static String parseTime(String time) {
		String t = "";
		try {
			Date date = new SimpleDateFormat(DateUtils.DATE_FORMAT_NOW).parse(time);
			t = DateUtils.convertDateTimeWithFormat(date, "HH:mm");
		} catch (Exception e) {
			Log.w("", e);
		}
		return t;
	}

	public static Date parseDateFromString(String st, String format) {
		Date date = null;
		SimpleDateFormat sfs = new SimpleDateFormat(format);
		try {
			date = sfs.parse(st);
		} catch (ParseException e) {
            Log.w("", e);
		}
		return date;
	}

	/*
	 * HieuNH so sanh ngay voi ngay hien tai
	 */
	public static int compareWithNow(String sDate1, String sDateFormat) {
		if (StringUtils.isNullOrEmpty(sDateFormat))
			return -1;
		Date now = now(sDateFormat);
		if ((StringUtils.isNullOrEmpty(sDate1))) {
			return 1;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(sDateFormat);
		Date date1 = null;
		long time1 = 0, time2 = 0;
		try {
			date1 = sdf.parse(sDate1);
			time1 = date1.getTime() / HOUR;
			time2 = now.getTime() / HOUR;
		} catch (Exception e) {
			Log.w("", e);
		}
		if (time1 == time2) {
			return 0;
		} else if (time1 < time2) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * lay ngay dau tien cua so thang truoc
	*
	* @author: trungnt56
	* @param: @return
	* @return: String
	* @throws:
	 */
	public static String getFirstDateOfNumberPreviousMonthWithFormat(String strFormat, int numberPreviousMonth) {
		Calendar cal= Calendar.getInstance();
		SimpleDateFormat format;
		if (StringUtils.isNullOrEmpty(strFormat)) {
			format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
		} else {
			format = new SimpleDateFormat(strFormat);
		}
		cal.add(Calendar.MONTH, numberPreviousMonth);
		cal.set(Calendar.DATE, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
		return format.format(cal.getTime());
	}

	/**
	 * So sanh ngay
	 *
	 * @author: TamPQ
	 * @return
	 * @return: intvoid
	 * @throws:
	 */
	public static int compareDate(String date1, String date2) {
		int result = -2;
		try {
			date1 = date1.substring(0, 10);
			date2 = date2.substring(0, 10);

			Date date11 = getDefaultSqlDateFormat().parse(date1);
			Date date22 = getDefaultSqlDateFormat().parse(date2);

			if (date11.before(date22)) {
				result = -1;
			} else if (date11.after(date22)) {
				result = 1;
			} else {
				result = 0;
			}
		} catch (Exception e) {
			Log.w("", e);
		}
		return result;
	}

	/**
	 * HieuNH Compare to.
	 *
	 * @param date1
	 *            the date1
	 * @param date2
	 *            the date2
	 * @return the int
	 */
	public static int compare(Date date1, Date date2) {

		if ((date1 != null) && (date2 == null)) {
			return -1;
		} else if ((date1 == null) && (date2 != null)) {
			return 1;
		} else if ((date1 == null)) {
			return 0;
		}

		long time1 = date1.getTime() / HOUR;
		long time2 = date2.getTime() / HOUR;

		if (time1 == time2) {
			return 0;
		} else if (time1 < time2) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * HieuNH
	 *
	 * @param formatDate
	 *            the format date
	 * @return the date
	 */
	public static Date now(String formatDate) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
		try {
			return sdf.parse(sdf.format(cal.getTime()));
		} catch (ParseException e) {
			return new Date();
		}
	}

	/*
	 * HieuNH so sanh 2 ngay
	 */
	public static int compare(String sDate1, String sDate2, String sDateFormat) {
		if ((!StringUtils.isNullOrEmpty(sDate1)) && (StringUtils.isNullOrEmpty(sDate2))) {
			return -1;
		} else if ((StringUtils.isNullOrEmpty(sDate1)) && (!StringUtils.isNullOrEmpty(sDate2))) {
			return 1;
		} else if ((StringUtils.isNullOrEmpty(sDate1)) && (StringUtils.isNullOrEmpty(sDate2))) {
			return 0;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(sDateFormat);
		Date date1 = null;
		Date date2 = null;
		long time1 = 0, time2 = 0;
		try {
			date1 = sdf.parse(sDate1);
			date2 = sdf.parse(sDate2);
			time1 = date1.getTime() / HOUR;
			time2 = date2.getTime() / HOUR;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.w("", e);
		}
		if (time1 == time2) {
			return 0;
		} else if (time1 < time2) {
			return -1;
		} else {
			return 1;
		}
	}


	/**
	 * TamPQ
	 *            the format date
	 * @return the date
	 */
	public static Date parseDateFromString(String st, SimpleDateFormat format) {
		Date date = null;
		SimpleDateFormat sfs = format;
		try {
			date = sfs.parse(st);
		} catch (ParseException e) {
		}
		return date;
	}

	/**
	 * Lay thu tu tuan thu may trong nam cua 1 ngay
	 *
	 * @author: TamPQ
	 * @return
	 * @return: intvoid
	 * @throws:
	 */
	public static int getWeekFromDate(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.WEEK_OF_YEAR);
	}



	/**
	 * parse Hour Minute
	 * @author: TamPQ
	 * @param visit_start_time
	 * @return
	 * @return: Datevoid
	 * @throws:
	 */
	public static Date parseHourMinute(String visit_start_time) {
		Date date = null;
		DateFormat formatter = new SimpleDateFormat("HH:mm");
		try {
			date = formatter.parse(visit_start_time);
		} catch (Exception e) {
			Log.w("", e);
		}
		return date;
	}


	/**
	 * Ktra thoi gian trong vong 30p so voi hien tai
	 * @author: TamPQ
	 * @return
	 * @return: booleanvoid
	 * @throws ParseException
	 * @throws:
	 */
	public static boolean isIn30Min(Date t) throws ParseException {
		boolean isValid;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, -30);
		Date time30 = calendar.getTime();

		if (time30.after(t)) {
			isValid = false;
		} else {
			isValid = true;
		}

		return isValid;
	}

	/**
	 * Cong thoi gian
	 *
	 */
	public static String plusTime(String time, int valueInMinute) {
		Date d = null;
		SimpleDateFormat format = new SimpleDateFormat(DateUtils.DATE_FORMAT_NOW);
		try {
			d = format.parse(time);
			d = new Date(d.getTime() + valueInMinute * ONE_MINUTE_IN_MILLIS);
		} catch (ParseException e) {
			// 			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}

		return format.format(d);
	}

	/**
	 * check ngay thang trong khoang may thang
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	public static boolean checkDateInOffsetMonth(String sDay, String sMonth, int year, int offsetMonth) {
		boolean isValid = false;
		try {
			String strDate = sDay + "/" + sMonth + "/" + year;
			Date date = DateUtils.getDefaultDateFormat().parse(strDate);
			Date firstDateOfPrevious2Month = getFirstDateOfOffsetMonth(offsetMonth);

			if (!date.before(firstDateOfPrevious2Month)) {
				isValid = true;
			}

		} catch (ParseException e) {
			isValid = false;
		}
		return isValid;
	}

	/**
	 * lay ngay dau tien cua 1 offset thang
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	public static Date getFirstDateOfOffsetMonth(int offsetMonth) {
		Date firstDateOfPreviousMonth = null;
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.MONTH, offsetMonth);
		try {
			firstDateOfPreviousMonth = DateUtils.getDefaultDateFormat().parse(DateUtils.getDefaultDateFormat().format(aCalendar
					.getTime()));
		} catch (ParseException e) {
			Log.w("", e);
		}
		return firstDateOfPreviousMonth;
	}

//	/**
//	 * Lay chuoi ngay gio giua 2 ngay
//	 *
//	 * @author: Nguyen Thanh Dung
//	 * @param startTime
//	 * @param endTime
//	 * @return
//	 */
//	public static String getVisitTime(String startTime) {
//		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_FORMAT_NOW);
//		Date startTimeDate = new Date();
//		try {
//			startTimeDate = sdf.parse(startTime);
//		} catch (ParseException e) {
//		}
//		Date endTimeDate = DateUtils.now(DateUtils.DATE_FORMAT_NOW);
//		StringBuilder time = new StringBuilder();
//		long diff = endTimeDate.getTime() - startTimeDate.getTime();
//		long seconds = diff / 1000;
//		long minutes = seconds / 60;
//		long hours = minutes / 60;
//
//		seconds = seconds % 60;
//		minutes = minutes % 60;
//
//		if (hours > 0) {
//			time.append(hours + " gio ");
//		}
//
//		time.append(minutes + " phut ");
//		time.append(seconds + " giay");
//
//		return time.toString();
//	}

	/**
	 * Doi thoi gian ghe tham da dang h gio m phut s giay
	 * @author: BANGHN
	 * @param startTime : thoi gian bat dau
	 * @param endTime : thoi gian ket thuc
	 * @return
	 */
//	public static String getVisitTime(String startTime, String endTime){
//		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_FORMAT_NOW);
//		Date startTimeDate = new Date();
//		try {
//			startTimeDate = sdf.parse(startTime);
//		} catch (Exception e) {
//			Log.w("", e);
//		}
//		Date endTimeDate = new Date();
//		try {
//			endTimeDate = sdf.parse(endTime);
//		} catch (Exception e) {
//			Log.w("", e);
//		}
//		StringBuilder time = new StringBuilder();
//		long diff = endTimeDate.getTime() - startTimeDate.getTime();
//		long seconds = diff / 1000;
//		long minutes = seconds / 60;
//		long hours = minutes / 60;
//
//		seconds = seconds % 60;
//		minutes = minutes % 60;
//
//		if (hours > 0) {
//			time.append(hours + " " + StringUtils.getString(R.string.TEXT_HOUR) + " ");
//		}
//
//		time.append(minutes +  " " + StringUtils.getString(R.string.TEXT_MINUTE) + " ");
//		time.append(seconds +  " " + StringUtils.getString(R.string.TEXT_SECOND) + " ");
//
//		return time.toString();
//	}


	/**
	 * Tinh thoi gian ghe tham cua khach hang
	 * @author: banghn
	 * @param cus
	 * @return
	 */
//	public static String getVisitEndTime(CustomerDTO cus) {
//		try{
//			double saleLat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
//					.getLastLatitude();
//			double saleLng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
//					.getLastLongtitude();
//			LatLng salePoint = new LatLng(saleLat, saleLng);
//			LatLng shopPoint = new LatLng(cus.lat, cus.lng);
//			double distance = GlobalUtil.getDistanceBetween(salePoint, shopPoint);
//			double distanceAllow = GlobalInfo.getInstance()
//					.getDistanceAllowFinish();
//			String dateNow = DateUtils.now();
//			ActionLogDTO al = GlobalInfo.getInstance().getProfile()
//					.getActionLogVisitCustomer();
//			String startTime = al.startTime;
//			//neu thoi gian ghe tham nho hon X(phut : 20) thi khoi check k.c
//			if (getDistanceSecondFrom2Date(startTime, dateNow) <= GlobalInfo
//					.getInstance().getVisitTimeAllow()) {
//				return dateNow;
//			} else if (distance <= distanceAllow) {
//				// Khoang cach luc nhan ket thuc cua NVBH so voi KH <= 300m
//				return dateNow;
//			} else {
//				int intervalVisitime = GlobalInfo.getInstance()
//						.getVisitTimeInterval();
//				//Lay tu sqlite danh sach vi tri nhan vien tu thoi diem start-time
//				//ArrayList<StaffPositionLogDTO> listPosition = SQLUtils.getInstance().getListLog(startTime);
//				//Lay tu globalinfo (luu file)
//				ArrayList<StaffPositionLogDTO> listPosition = GlobalInfo.getInstance().getStaffPosition(startTime);
//
//				// TH : ton tai log gan nhat voi thoi diem nhan ket thuc < 300m
//				for (StaffPositionLogDTO temp : listPosition) {
//					LatLng point = new LatLng(temp.lat, temp.lng);
//					double d = GlobalUtil.getDistanceBetween(shopPoint, point);
//					if (d <= distanceAllow) {
//						String endTimeTemp = DateUtils.plusTime(temp.createDate,
//								intervalVisitime);
//						if (DateUtils.compareFullDate(endTimeTemp, dateNow) >= 0) {
//							return dateNow;
//						} else {
//							return endTimeTemp;
//						}
//					}
//				}
//
//				// TH : Khong co log nao < 300m
//				// Lay log gan nhat voi thoi diem bat dau ghe tham > 300m
//				int size = listPosition.size();
//				if(size > 0){
//					StaffPositionLogDTO temp = listPosition.get(size - 1);
//					// Log gan nhat voi thoi diem ghe tham > 300m
//					String endTimeTemp = DateUtils.plusTime(temp.createDate,
//							intervalVisitime);
//					if (DateUtils.compareFullDate(endTimeTemp, dateNow) >= 0) {
//						return dateNow;
//					} else {
//						return endTimeTemp;
//					}
//				}
//			}
//			//Khong tim duoc truong hop nao, lay thoi gian hien tai
//			return dateNow;
//		}catch(Exception e){
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//			return DateUtils.now();
//		}
//	}

	/**
	 * So sanh ngay day du : gio, phut, giay
	 *
	 * @author: TRUNGHQM
	 * @return
	 * @return: intvoid
	 * @throws:
	 */
	public static int compareFullDate(String date1, String date2) {
		int result = -2;
		try {
			Date date11 = getDefaultSqlFullDateFormat().parse(date1);
			Date date22 = getDefaultSqlFullDateFormat().parse(date2);

			if (date11.before(date22)) {
				result = -1;
			} else if (date11.after(date22)) {
				result = 1;
			} else {
				result = 0;
			}
		} catch (Exception e) {
			Log.w("", e);
		}
		return result;
	}


	/**
	 * Lay chuoi ngay gio giua 2 ngay
	 *
	 * @author: Nguyen Thanh Dung
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	//public static String getVisitTime(String startTime) {
	//	return getVisitTime(startTime, now());
	//}

	/**
	 * lay so phut giua thoi gian theo dang yyyy-MM-dd HH:mm:ss
	 * @author: DungNX
	 * @param strDate1 : ngay gio bat dau
	 * @param strDate2 : ngay gio ket thuc
	 * @return : so phut (minutes)
	 */
	public static long getDistanceMinutesFrom2DateCheckAttendance(String strDate1, String strDate2) {
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_FORMAT_NOW);
		Date startTimeDate = new Date();
		try {
			startTimeDate = sdf.parse(strDate1);
		} catch (Exception e) {
			Log.w("", e);
		}
		Date endTimeDate = new Date();
		try {
			endTimeDate = sdf.parse(strDate2);
		} catch (Exception e) {
			Log.w("", e);
		}
		long diff = endTimeDate.getTime() - startTimeDate.getTime();
		long seconds = diff / 1000;
		long minutes = seconds / 60;

		return minutes;
	}

	static long lastBootUpdateRightTime = 0;
	static long TIME_ALLOW_UPDATE_RIGHT_TIME = 1 * 60 * 1000;
	/**
	 * Cap nhat thoi gian dua vao thoi gian dung, dong thoi hien dialog thong bao neu sai gio
	 * @author: duongdt3
	 * @since: 09:17:07 8 Apr 2014
	 * @return: void
	 * @throws:  
	 */
//	public static void updateRightTime(String rightDate) {
//
//		long bootNow = SystemClock.elapsedRealtime();
//		long timeUpdate = Math.abs(bootNow - lastBootUpdateRightTime);
//		//>= 15 phut moi cap nhat thoi gian, tranh cap nhat qua nhieu lan
//		if (timeUpdate >= TIME_ALLOW_UPDATE_RIGHT_TIME) {
//			if (!StringUtils.isNullOrEmpty(rightDate) && DateUtils.checkDateInFormat(rightDate, DATE_FORMAT_NOW)) {
//				Log.d("updateRightTime", "updateRightTime: " + rightDate + " now: " + DateUtils.now());
//				//update last boot update right time
//				lastBootUpdateRightTime = bootNow;
//
//				RightTimeInfo curRightInfo = GlobalInfo.getInstance().getRightTimeInfo().clone();
//				//luu lai thoi gian cuoi cung login online server, neu chua co
//				if (StringUtilss.isNullOrEmpty(curRightInfo.getLastTimeOnlineLogin())) {
//					curRightInfo.setLastTimeOnlineLogin(rightDate);
//				}
//				//cap nhat thoi gian dung cuoi cung
//				curRightInfo.setLastRightTime(rightDate);
//				//cap nhat thoi gian tu khi boot
//				curRightInfo.setLastRightTimeSinceBoot(bootNow);
//				//luu thoi gian dung moi nhat
//				GlobalInfo.getInstance().setRightTimeInfo(curRightInfo);
//			} else{
//				MyLog.e("updateRightTime", "not updateRightTime: not in format " + rightDate);
//			}
//		} else{
//			MyLog.d("updateRightTime", "not updateRightTime: " + rightDate
//					+ " now: " + DateUtils.now() + " timeUpdate: " + timeUpdate);
//		}
//	}
	
	private static boolean checkDateInFormat(String date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		boolean result = false;
		try {
			sdf.parse(date);
			result = true;
		} catch (ParseException e) {
			result = false;
		}
		return result;
	}


	/**
	 * Lay thoi gian dung tuong doi, tinh toan bang lastRightTime, tra ra Calendar
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 17:24:38 20 Jun 2014
	 * @return: Calendar
	 * @throws:  
	 * @return
	 */
//	public static Calendar getRightCalTimeNow(){
//		Calendar result = Calendar.getInstance();
//		try {
//			RightTimeInfo rightTimeInfo = GlobalInfo.getInstance().getRightTimeInfo();
//			String lastRightTime = rightTimeInfo.getLastRightTime();
//			if (!StringUtilss.isNullOrEmpty(lastRightTime) && !"null".equals(lastRightTime)) {
//				long lastTimeOnlineFromBoot = rightTimeInfo.getLastRightTimeSinceBoot();
//				long timeSinceBootNow = SystemClock.elapsedRealtime();
//				long dis = (timeSinceBootNow - lastTimeOnlineFromBoot);
//				// bo qua thoi gian tat may, khoi dong lai may
//				if (dis < 0) {
//					dis = 0;
//				}
//				SimpleDateFormat sfs = new SimpleDateFormat(DATE_FORMAT_NOW);
//				sfs.setTimeZone(TimeZone.getTimeZone("GMT+7"));
//				result = Calendar.getInstance(TimeZone.getTimeZone("GMT+7"));
//				Date rightTime = sfs.parse(lastRightTime.toString());
//				result.setTimeInMillis(rightTime.getTime());
//				result.add(Calendar.MILLISECOND, (int)dis);
//			} else {
//				ServerLogger.sendLog( "TIME getRightCalTimeNow lastRightTime: " + lastRightTime + " lastRightTimeSinceBoot " + rightTimeInfo.getLastRightTimeSinceBoot(), TabletActionLogDTO.LOG_CLIENT);
//			}
//		} catch (Exception e) {
//			ServerLogger.sendLog("TIME getRightCalTimeNow " + MyLog.printStackTrace(e), TabletActionLogDTO.LOG_CLIENT);
//			result = Calendar.getInstance();
//		}
//		return result;
//	}
	/**
	 * Kiem tra thoi gian hien tai cua tablet, co chenh lech qua 50 phut hay khong 
	 * @author: duongdt3
	 * @since: 11:46:03 24 Mar 2014
	 * @return: int {DateUtils.RIGHT_TIME, DateUtils.WRONG_DATE, DateUtils.WRONG_TIME, DateUtils.WRONG_TIME_BOOT_REASON}
	 * @throws:  
	 * @param isCheckTimeFromBoot
	 * @return
	 */
//	private static int checkTabletRightTime(boolean isCheckTimeFromBoot) {
//
//		RightTimeInfo rightTimeInfo = GlobalInfo.getInstance().getRightTimeInfo();
//		String lastRightTime = isCheckTimeFromBoot ? rightTimeInfo.getLastRightTime() : rightTimeInfo.getLastTimeOnlineLogin();
//		//mac dinh, danh cho truong hop ko co lastTime hoac co loi khi thao tac
//		int isOnlineInDate = DateUtils.RIGHT_TIME;
//		try {
//			if (!StringUtilss.isNullOrEmpty(lastRightTime) && !"null".equals(lastRightTime)) {
//				String now = DateUtils.now();
//				long lastTimeOnlineFromBoot = rightTimeInfo.getLastRightTimeSinceBoot();
//				Date dFromLastLogIn = DateUtils.parseDateFromString(lastRightTime, DateUtils.DATE_FORMAT_NOW);
//				Date dnow = new Date();
//				long timeSinceBootNow = SystemClock.elapsedRealtime();
//
//				//xac dinh ngay co sai hay khong
//				boolean isWrongDate = true;
//				String dateLastRightTime = lastRightTime.substring(0, 10);
//				String dateNow = now.substring(0, 10);
//				//dung ngay
//				if (dateLastRightTime.equals(dateNow)) {
//					isWrongDate = false;
//				}
//
//				//neu can check dung gio
//				if (isCheckTimeFromBoot) {
//					long dis = (timeSinceBootNow - lastTimeOnlineFromBoot);
//					boolean isWrongBootTime = false;
//					//bo qua thoi gian tat may, khoi dong lai may
//					if (dis < 0) {
//						dis = 0;
//						isWrongBootTime = true;
//					}
//					//
//                    if(dFromLastLogIn!=null && Math.abs((dFromLastLogIn.getTime() + dis) - dnow.getTime()) > GlobalInfo.getInstance().getMaxTimeWrong()){
//                        if (isWrongBootTime) {
//                            isOnlineInDate = DateUtils.WRONG_TIME_BOOT_REASON;
//                        }else{
//                            isOnlineInDate = DateUtils.WRONG_TIME;
//                        }
//                    }
//					else { //dung thoi gian
////					//
////					long deltaTime = Math.abs((dFromLastLogIn.getTime() + dis) - dnow.getTime());
////					//sai gio, qua 50 phut
////					if(dFromLastLogIn == null){
////						isOnlineInDate = DateUtils.WRONG_TIME;
////					} else if(deltaTime > GlobalInfo.getInstance().getMaxTimeWrong()
////							){
////						if (isWrongBootTime) {
////							isOnlineInDate = DateUtils.WRONG_TIME_BOOT_REASON;
////						}else{
////							isOnlineInDate = DateUtils.WRONG_TIME;
////						}
////					}
//                        //thi tra ve gia tri la ngay hom nay chua login online
//						if (isWrongDate) {
//							isOnlineInDate = DateUtils.WRONG_DATE;
//						} else {
//							//dung ngay dung gio
//							isOnlineInDate = DateUtils.RIGHT_TIME;
//						}
//					}
//				} else { //chi can check dung ngay
//					//neu sai ngay bao sai
//					if(isWrongDate){
//						isOnlineInDate = DateUtils.WRONG_DATE;
//					} else{
//						isOnlineInDate = DateUtils.RIGHT_TIME;
//					}
//				}
//			} else{
//				isOnlineInDate = DateUtils.WRONG_TIME;
//			}
//		} catch (Exception e) {
//			isOnlineInDate = DateUtils.WRONG_TIME;
//			ServerLogger.sendLog("TIME checkTabletRightTime " + MyLog.printStackTrace(e), TabletActionLogDTO.LOG_CLIENT);
//		}
//		return isOnlineInDate;
//	}


	/**
	 * check TimeZone hien tai co phai la GMT+7 hay khong
	 * @author : duongdt
	 */
	public static boolean isGMT7TimeZone(){
		boolean isRight = false;
		if (GMT7Offset == TimeZone.getDefault().getRawOffset()) {
			isRight = true;
		}
		return isRight;
	}

	public static String nowVN() {
		Calendar cal = Calendar.getInstance();
		String result = formatDate(cal.getTime(), DATE_TIME_FORMAT_VN);
		return result;
	}

	public static String formatDate(Date date, String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String result = sdf.format(date);
		return result;
	}

	public static String formatNow(String format){
		return formatDate(Calendar.getInstance().getTime(), format);
	}

	/**
	 * Lay thoi gian ngay hom sau theo format truyen vao
	 *
	 * @author : ThangNV31
	 */
	public static String getTomorrowDateTimeWithFormat(String strFormat) {
		Date currentDateTime = new Date();
		Date tomorrowDateTime;
		Calendar c = Calendar.getInstance();
		c.setTime(currentDateTime);
		c.add(Calendar.DATE, 1);
		tomorrowDateTime = c.getTime();
		SimpleDateFormat format;
		if (StringUtils.isNullOrEmpty(strFormat)) {
			format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
		} else {
			format = new SimpleDateFormat(strFormat);
		}
		return format.format(tomorrowDateTime);
	}

	/**
	 * lay 4 thang
	 *
	 * @author: dungdq3
	 * @since: 4:40:59 PM Nov 10, 2014
	 * @return: String
	 * @throws:
	 * @return
	 */
	public static ArrayList<String[]> get4Month(){
		SimpleDateFormat format = new SimpleDateFormat("MM_yyyy");
        ArrayList<String[]> listCalendar = new ArrayList<String[]>();
        String[] arrCalendar;
        for(int i = -2; i <= 1; i++){
        	Calendar cal = Calendar.getInstance();
              cal.add(Calendar.MONTH, i);
              cal.set(Calendar.DATE, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
              arrCalendar = format.format(cal.getTime()).split("_");
              listCalendar.add(arrCalendar);
        }
        return listCalendar;
	}

	/**
	 * lay 3 thang: 2 thang truoc va hien tai
	 *
	 * @author: dungdq3
	 * @since: 4:40:59 PM Nov 10, 2014
	 * @return: String
	 * @throws:
	 * @return
	 */
	public static ArrayList<String[]> get3Month(){
		SimpleDateFormat format = new SimpleDateFormat("MM_yyyy");
		ArrayList<String[]> listCalendar = new ArrayList<String[]>();
		String[] arrCalendar;
		for(int i = -2; i <= 0; i++){
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, i);
			cal.set(Calendar.DATE, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
			arrCalendar = format.format(cal.getTime()).split("_");
			listCalendar.add(arrCalendar);
		}
		return listCalendar;
	}

	/**
	 * Lay ngay dau tien cua thang , nam truyen vao
	 * @author: hoanpd1
	 * @since: 13:31:51 18-11-2014
	 * @return: Date
	 * @throws:
	 * @param offsetMonth
	 * @param offsetYear
	 * @return
	 */
	public static Date getFirstDateOfOffsetMonthYear(int offsetMonth, int offsetYear) {
		Date firstDateOfPreviousMonth = null;
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.MONTH, offsetMonth);
		aCalendar.set(Calendar.YEAR, offsetYear);
		try {
			firstDateOfPreviousMonth = DateUtils.getDefaultDateFormat().parse(DateUtils.getDefaultDateFormat().format(aCalendar
					.getTime()));
		} catch (Exception e) {
			Log.e("getFirstDateOfOffset", "fail", e);
		}
		return firstDateOfPreviousMonth;
	}

	/**
	 * Lay ngay cuoi cung thang truoc tuong ung voi ngay truyen vao
	 *
	 * @author: hoanpd1
	 * @since: 16:58:18 14-01-2015
	 * @return: String
	 * @throws:
	 * @param strFormat
	 * @param date
	 * @param numberPreviousMonth
	 * @return
	 */
	public static String getLastDateOfNumberPreviousMonthWithFormat(String strFormat, Date date, int numberPreviousMonth) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String lastDateOfMonth = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			cal.add(Calendar.MONTH, numberPreviousMonth);
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			lastDateOfMonth= format.format(cal.getTime());
		} catch (Exception e) {
			Log.w("getLastDateOfNumber", "fail", e);
		}
		return lastDateOfMonth;
	}

	/**
	 * Ngay dau tien cua thang ung voi ngay truyen vao
	 *
	 * @author: hoanpd1
	 * @since: 10:51:10 15-01-2015
	 * @return: String
	 * @throws:
	 * @param strFormat
	 * @param date
	 * @param numberPreviousMonth
	 * @return
	 */
	public static String getFirstDateOfNumberPreviousMonthWithFormat(String strFormat, Date date, int numberPreviousMonth) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String firstDateOfMonth = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			cal.add(Calendar.MONTH, numberPreviousMonth);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			firstDateOfMonth= format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("getFirstDateOf", "fail", e);
		}
		return firstDateOfMonth;
	}

	public static Date parseDateFromString(String strFormat, Date date1, int numberPreviousMonth) {
		Date date = date1;
		Calendar cal = Calendar.getInstance();
		//SimpleDateFormat format = null;
		//String firstDateOfMonth = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				//format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				//format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			cal.add(Calendar.MONTH, numberPreviousMonth);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			//firstDateOfMonth= format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("parseDateFromString", "fail", e);
		}
//		return firstDateOfMonth
		return date;
	}

	/**
	 * Lay ngay dau tien cua nam ung voi ngay truyen vao theo format
	 * @author: hoanpd1
	 * @since: 17:06:36 19-01-2015
	 * @return: String
	 * @throws:
	 * @param strFormat
	 * @param date
	 * @param : numPreviousYear : so nam truoc nam hien tai
	 * @return
	 */
	public static String getFirstDateOfYear(String strFormat, Date date, int numPreviousYear) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String firstDateOfYear = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			cal.add(Calendar.YEAR, numPreviousYear);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.DAY_OF_MONTH,1);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			firstDateOfYear= format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("getFirstDateOfYear", "fail", e);
		}
		return firstDateOfYear;
	}

	/**
	 * Lay ngay cuoi cung cua nam
	 * @author: hoanpd1
	 * @since: 17:09:39 19-01-2015
	 * @return: String
	 * @throws:
	 * @param strFormat
	 * @param date
	 * @return
	 */
	public static String getLastDateOfYear(String strFormat, Date date) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String lastDateOfYear = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			//them 1 nam
			cal.add(Calendar.YEAR, 1);

			//tru 1 ngay
			// => ngay cuoi cung nam cu
			cal.add(Calendar.DAY_OF_MONTH, -1);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			lastDateOfYear= format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("getLastDateOfYear", "fail", e);
		}
		return lastDateOfYear;
	}

	/**
	 * ngay dau tien cua quy ung voi ngay truyen vao theo format
	 * @author: hoanpd1
	 * @since: 17:17:24 19-01-2015
	 * @return: String
	 * @throws:
	 * @param strFormat
	 * @param date
	 * @param numberPreviousQuarter: so quy bat dau tu quy hien tai tro ve truoc 'numberPreviousQuarter' quy
	 * @return
	 */
	public static String getFirstDateOfQuarter(String strFormat, Date date, int numberPreviousQuarter) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String firstDateOfQuarter = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			cal.add(Calendar.MONTH, numberPreviousQuarter * 3);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			firstDateOfQuarter = format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("getFirstDateOfQuarter", "fail", e);
		}
		return firstDateOfQuarter;

	}

	/**
	 * Ngay cuoi cua quy ung voi ngay truyen vao theo format
	 * @author: hoanpd1
	 * @since: 17:18:14 19-01-2015
	 * @return: String
	 * @throws:
	 * @param strFormat
	 * @param date
	 * @return
	 */
	public static String getLastDateOfQuarter(String strFormat, Date date) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String lastDateOfQuarter = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			int month = cal.get(Calendar.MONTH);
			int quarter = month/3 ;
			cal.set(Calendar.MONTH, quarter*3);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.add(Calendar.MONTH, 3);
			cal.add(Calendar.DAY_OF_MONTH, -1);
			lastDateOfQuarter = format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("getLastDateOfQuarter", "fail", e);
		}
		return lastDateOfQuarter;
	}


	 /**
	 * Lay ngay dau tien cua tuan hien tai theo ngay trong tuan hien tai truyen vao
	 * @author: Tuanlt11
	 * @param strFormat
	 * @param date
	 * @param numberPreviousWeek
	 * @return
	 * @return: String
	 * @throws:
	*/
	public static String getFirstDateOfWeek(String strFormat, Date date, int numberPreviousWeek) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String firstDateOfWeek = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			cal.add(Calendar.WEEK_OF_YEAR, numberPreviousWeek);
			//cal.setFirstDayOfWeek(Calendar.MONDAY);
			int today = cal.get(Calendar.DAY_OF_WEEK);
			if(today == Calendar.SUNDAY){
				//set chu nhat = 8
				today = 8;
			}
			cal.add(Calendar.DAY_OF_MONTH, - today + Calendar.MONDAY);
			firstDateOfWeek= format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("getFirstDateOfWeek", "fail", e);
		}
		return firstDateOfWeek;
	}

	/**
	 * Lay ngay cuoi cua tuan hien tai theo ngay trong tuan hien tai truyen
	 * vao
	 *
	 * @author: Tuanlt11
	 * @param strFormat
	 * @param date
	 * @return
	 * @return: String
	 * @throws:
	 */
	public static String getLastDateOfWeek(String strFormat, Date date) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String lastDateOfWeek = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			int currentDay = cal.get(Calendar.DAY_OF_WEEK);
			if(currentDay == Calendar.SUNDAY){
				//set chu nhat = 8
				currentDay = 8;
			}
			int leftDays= Calendar.SATURDAY - currentDay + 1;
			cal.add(Calendar.DAY_OF_MONTH, leftDays);
			lastDateOfWeek = format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("getLastDateOfWeek", "fail", e);
		}
		return lastDateOfWeek;
	}

	 /**
	 * Lay quy cua ngay truyen vao
	 * @author: Tuanlt11
	 * @param date
	 * @return
	 * @return: int
	 * @throws:
	*/
	public static int getQuarter(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MONTH)/ 3 + 1;
	}

	/**
	 * Lay thu tu tuan thu may trong thang
	 *
	 * @author: TamPQ
	 * @param fDate
	 * @return
	 * @return: intvoid
	 * @throws:
	 */
//	public static int getWeekOfMonth(Date date) {
//		// hien tai xu li theo ngon ngu de lay tuan bat dau cho chinh xac
//		Calendar c = Calendar.getInstance(GlobalInfo.getInstance().getBaseContext().getResources().getConfiguration().locale);
////		Calendar c = Calendar.getInstance();
//		c.setTime(date);
//		return c.get(Calendar.WEEK_OF_MONTH);
//	}

	 /**
	 * Lay tu tu tuan cua nam
	 * @author: Tuanlt11
	 * @param date
	 * @return
	 * @return: int
	 * @throws:
	*/
	public static int getWeekOfYear(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.WEEK_OF_YEAR);
	}


	 /**
	 * Lay ngay tiep theo hay ngay truoc
	 * @author: Tuanlt11
	 * @param strFormat
	 * @param date
	 * @param numPreviousYear
	 * @return
	 * @return: String
	 * @throws:
	*/
	public static String getOffsetDay(String strFormat, Date date, int numPreviousDay) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = null;
		String nextDay = null;
		try {
			if (StringUtils.isNullOrEmpty(strFormat)) {
				format = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
			} else {
				format = new SimpleDateFormat(strFormat);
			}
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_MONTH,numPreviousDay);
			nextDay= format.format(cal.getTime());
		} catch (Exception e) {
			Log.e("getOffsetDay", "fail", e);
		}
		return nextDay;
	}

	/**
	 * format from string to date
	 *
	 * @author: dungdq3
	 * @since: 2:08:22 PM Feb 14, 2015
	 * @return: Date
	 * @throws:
	 * @param stringDate
	 * @param formatString
	 * @return
	 */
	public static Date convertToDate(String stringDate, String formatString){
		SimpleDateFormat format = null;
		Date date = null;
		try {
			if (StringUtils.isNullOrEmpty(formatString)) {
				format = getDefaultSqlFullDateFormat();
			} else {
				format = new SimpleDateFormat(formatString);
			}
			if (!StringUtils.isNullOrEmpty(stringDate)) {
				date = format.parse(stringDate);
			}
		} catch (Exception e) {
			Log.e("convertToDate", "fail", e);
		}
		return date;
	}

	/**
	 * compare with loai input
	 *
	 * @author: dungdq3
	 * @since: 11:40:15 AM Apr 7, 2015
	 * @return: int
	 * @throws:
	 * @param date1
	 * @param date2
	 * @param typeCompare
	 * @return
	 */
	public static int compare(Date date1, Date date2, long typeCompare) {

		if ((date1 != null) && (date2 == null)) {
			return -1;
		} else if ((date1 == null) && (date2 != null)) {
			return 1;
		} else if ((date1 == null)) {
			return 0;
		}

		long time1 = date1.getTime() / typeCompare;
		long time2 = date2.getTime() / typeCompare;

		if (time1 == time2) {
			return 0;
		} else if (time1 < time2) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * get Num Mili second
	 * @return: int
	 * @throws:
	 * @param strDay
	 * @param dateFormat
	 */
	public static long getNumMilisecond(String strDay, String dateFormat) {
		long res = 0;
		try {
			SimpleDateFormat sfd = new SimpleDateFormat(dateFormat);
			Date dayFormat = sfd.parse(strDay);
			res = dayFormat.getTime();
		} catch (Exception e) {
			Log.w("getNumMilisecond", e);
		}
		return res;
	}

	/**
	 *
	 * @author TamPQ
	 */

	public static String[] getCurrentAnd3MonthAgoYM(){
		String[] yearMonth = new String[2];
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
		Date currentDate = new Date();
		yearMonth[0] = dateFormat.format(currentDate);

		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.MONTH, -3);
		yearMonth[1] = dateFormat.format(c.getTime());
		
		return yearMonth;
	}



	/**
	 *	con thoi gian them days ngay
	 */
	public static Date plusDays(Date time, int days) {
		Date d = new Date(time.getTime() + days * ONE_MINUTE_IN_MILLIS * 60 * 24);
		return d;
	}

	public static SimpleDateFormat getDefaultDateFormat_2() {
		return new SimpleDateFormat(DATE_FORMAT_NOW);
	}

	public static SimpleDateFormat getDefaultDateFormat() {
		return new SimpleDateFormat(DATE_FORMAT_DATE_VN);
	}

	public static SimpleDateFormat getDefaultSqlFullDateFormat() {
		return new SimpleDateFormat(DATE_FORMAT_NOW);
	}

	public static SimpleDateFormat getDefaultHourMinute() {
		return new SimpleDateFormat(DATE_FORMAT_HOUR_MINUTE);
	}

	public static SimpleDateFormat getDefaultSqlDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}
}
