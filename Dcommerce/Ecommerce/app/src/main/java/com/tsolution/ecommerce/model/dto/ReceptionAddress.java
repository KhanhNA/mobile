package com.tsolution.ecommerce.model.dto;

import java.io.*;

import lombok.*;

@Data
public class ReceptionAddress  implements Serializable {
    public ReceptionAddress() {
    }

    String name;
    String phone;
    String addres;
}
