package com.tsolution.ecommerce.presenter;

import com.tsolution.ecommerce.model.dto.MerchantDto;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.utils.Constants;

public class ContactPresenter extends BasePresenter {
    private BaseView baseView;


    public ContactPresenter(BaseView baseView) {
        super(baseView);
        this.baseView = baseView;
    }

    public void registerMerchant(MerchantDto merchantDto) {
        baseView.loading("Registering. Please wait...");
        get(this, "registerMerchant", "handleResponseRegisterSuccessful", merchantDto);

    }

    public void getMerchants(MerchantDto merchantDto) {
        get(this, "getMerchants", "handleResponseMerchantsSuccessful", merchantDto, 0, 1);
    }

    public void handleResponseMerchantsSuccessful(ServiceResponse serviceResponse) {
        baseView.handleResponseSuccessful(Constants.GET_DATA_MERCHANT, new ResponseInfo<>(serviceResponse, Constants.CODE.SUCCESS));
    }

    public void handleResponseRegisterSuccessful(ServiceResponse serviceResponse) {
        baseView.handleResponseSuccessful(Constants.REGISTER_MERCHANT, new ResponseInfo<>(serviceResponse, Constants.CODE.SUCCESS));
    }

}
