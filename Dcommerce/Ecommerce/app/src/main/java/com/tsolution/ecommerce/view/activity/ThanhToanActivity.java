package com.tsolution.ecommerce.view.activity;

import android.app.*;
import android.app.AlertDialog;
import android.content.*;
import android.os.*;

import androidx.annotation.Nullable;
import androidx.appcompat.app.*;
import androidx.appcompat.widget.*;
import android.view.*;
import android.widget.*;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tsolution.ecommerce.*;
import com.tsolution.ecommerce.adapter.*;
import com.tsolution.ecommerce.db.*;
import com.tsolution.ecommerce.model.*;
import com.tsolution.ecommerce.model.dto.*;
import com.tsolution.ecommerce.presenter.*;
import com.tsolution.ecommerce.service.listerner.*;
import com.tsolution.ecommerce.utils.*;
import com.tsolution.ecommerce.view.application.*;

import java.util.*;

public class ThanhToanActivity extends BaseActivity implements View.OnClickListener {
    Spinner spPhuongThucVanChuyen,spChonKho;
    TextView txtdiaChiNhanHang,txtMethodPayment,tvWalletMoney,tvToTalMoney,tvmoneyDicount;
    LinearLayout lnThanhToan;
    List<Products> products = new ArrayList<Products>();
    OrderProductCartDTO orderedResponse = new OrderProductCartDTO();
    Double totalMoney;//tong tien thanh toan
    Double walletMoney;//tien vao vi
    Double moneyDiscount;//tien chiet khau
    RecyclerView recyclerSanPham;
    Button btnThanhToan;
    private BasePresenter paytPresenter;
    OrderProductCartDTO orderDTO = new OrderProductCartDTO();


    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_thanh_toan_new);
        paytPresenter = new BasePresenter(this);
        getIntentFromCartActivity();
        init();
        setAdapterListProduct();
    }

    private void getIntentFromCartActivity() {
        Intent i = getIntent();
        products = (List<Products>) i.getSerializableExtra(IntentConstans.INTENT_LIST_PRODUCT);
        totalMoney = i.getDoubleExtra(IntentConstans.INTENT_TOTAL_MONEY,0.0);
        walletMoney = i.getDoubleExtra(IntentConstans.INTENT_MONEY_TO_WALLET,0.0);
        moneyDiscount = i.getDoubleExtra(IntentConstans.INTENT_MONEY_DISCOUNT,0.0);
    }

    private void setAdapterListProduct() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        AdapterGioHang adapterGioHang = new AdapterGioHang(this,products,null);
        recyclerSanPham.setLayoutManager(layoutManager);
        recyclerSanPham.setAdapter(adapterGioHang);
    }

    public void init(){
        spChonKho = findViewById(R.id.spChonKho);
        spPhuongThucVanChuyen = findViewById(R.id.spPhuongThucVanChuyen);
        txtdiaChiNhanHang = findViewById(R.id.txtdiaChiNhanHang);
        txtdiaChiNhanHang.setOnClickListener(this);
        txtMethodPayment = findViewById(R.id.txtMethodPayment);
        btnThanhToan = findViewById(R.id.btnThanhToan);
        btnThanhToan.setOnClickListener(this);
        tvToTalMoney = findViewById(R.id.tvToTalMoney);
        if (totalMoney>0.0){
            tvToTalMoney.setText(AppController.getInstance().formatCurrency(totalMoney));
        }


        tvWalletMoney = findViewById(R.id.tvWalletMoney);
        if (walletMoney >0.0){
            tvWalletMoney.setText(AppController.getInstance().formatCurrency(walletMoney));
        }

        tvmoneyDicount = findViewById(R.id.tvmoneyDicount);
        if (moneyDiscount > 0.0){
            tvmoneyDicount.setText(AppController.getInstance().formatCurrency(moneyDiscount));
        }

        recyclerSanPham = findViewById(R.id.recyclerSanPham);
        lnThanhToan=findViewById(R.id.lnThanhToan);
        lnThanhToan.setOnClickListener(this);



        //spChonKho
        List<String> list = new ArrayList<>();
        list.add("Kho DC1");
        list.add("Kho DC2");
        list.add("Kho DC3");
        list.add("Kho DC4");
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spChonKho.setAdapter(adapter);
        spChonKho.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //
        List<String> list2 = new ArrayList<>();
        list2.add("Lấy hàng trực tiếp");
        list2.add("Ship");
        ArrayAdapter<String> adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item,list2);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPhuongThucVanChuyen.setAdapter(adapter2);
        spPhuongThucVanChuyen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.txtdiaChiNhanHang:
                Intent i = new Intent(ThanhToanActivity.this, LocationActivity.class);
                startActivityForResult(i, Constants.INTENT_GET_RECEPTION_ADDRESS);
                break;
            case R.id.lnThanhToan:
                Intent intent = new Intent(ThanhToanActivity.this,PhuongThucThanhToanActivity.class);
                startActivityForResult(intent,Constants.INTENT_GET_METHOD_PAYMENT);
                break;
            case R.id.btnThanhToan:
                showConfirmDialog();
//                setDTO();
//                paytPresenter.get(this, "getListProductCartOrder", "handleResponseRegisterSuccessful",orderDTO );
                break;
        }
    }

    private void showConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.CONFIRM_ORDER));
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.CANCEL), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(getString(R.string.CONFIRM), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setDTO();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void setDTO() {
        orderDTO = new OrderProductCartDTO();
        orderDTO.setMerchantId(1L);
        orderDTO.setPaymentMethodId(1L);
        orderDTO.setShipMethodId(1L);
        orderDTO.setShipCharge(10000D);
        orderDTO.setRecvStoreId(1L);
        orderDTO.setRecvLat(1.2D);
        orderDTO.setRecvLong(2.3D);
        orderDTO.setIsSaveOrder(1);
        orderDTO.setRecvAddr(txtdiaChiNhanHang.getText().toString());
        for (int i = 0; i < products.size(); i++) {
            OrderPackingDTO packingDTO = new OrderPackingDTO();
            packingDTO.setQuantity(products.get(i).getQuantity());
            packingDTO.setPackingProductId(products.get(i).getProductId());
            orderDTO.addPackingProduct(packingDTO);
        }
        //push data len server
        paytPresenter.get(this, "getListProductCartOrder", "handleResponseRegisterSuccessful",orderDTO );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.INTENT_GET_RECEPTION_ADDRESS) {
            if (resultCode == Activity.RESULT_OK){
                ReceptionAddress receptionAddress = (ReceptionAddress) data.getSerializableExtra("receptionAddress");
                if (receptionAddress != null) {
                    txtdiaChiNhanHang.setText(receptionAddress.getName() + " | (+84)"+receptionAddress.getPhone() + "\n"+ receptionAddress.getAddres());
                }

            }
        }
        if (requestCode == Constants.INTENT_GET_METHOD_PAYMENT) {
            if (resultCode == Activity.RESULT_OK){
                int methodPayment = data.getIntExtra("methodPayment",3);
                if (methodPayment == Constants.METHOD_WALLET){
                    txtMethodPayment.setText(R.string.WALLET_NEXT);
                }else if (methodPayment == Constants.METHOD_CARD){
                    txtMethodPayment.setText(R.string.PAY_CARD);
                }else if (methodPayment == Constants.METHOD_DIRECTORY){
                    txtMethodPayment.setText(R.string.PAY_DIRECTORY);
                }
            }
        }
    }
    public void handleResponseRegisterSuccessful(OrderProductCartDTO orderDTO){

        //Xoa san pham da dat
        ModelGioHang modelGioHang = new ModelGioHang();
        modelGioHang.MoKetNoiSQL(this);
        for (int i = 0; i < products.size(); i++) {
            modelGioHang.XoaSanPhamTrongGioHang(products.get(i).getProductId());
        }
        //
        Intent intent = new Intent(ThanhToanActivity.this,OrderSuccessfulActivity.class);
        intent.putExtra(IntentConstans.INTENT_ORDER_SUCCESS,orderDTO.getOrderId());
        //clear all activity before
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        ThanhToanActivity.this.finish();
    }

    @Override
    public void handleResponseError(String serviceName, Constants.CODE code) {
        super.handleResponseError(serviceName, code);
        switch (serviceName) {
            case "getListProductCartOrder":
                Toast.makeText(this,"Faellllllllllllllllllll",Toast.LENGTH_LONG).show();
                break;
        }
    }
}
