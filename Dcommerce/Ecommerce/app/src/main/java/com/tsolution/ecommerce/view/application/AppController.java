package com.tsolution.ecommerce.view.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;

import com.tsolution.ecommerce.service.RetrofitClient;
import com.tsolution.ecommerce.service.listerner.SOService;
import com.tsolution.ecommerce.utils.StringUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.util.HashMap;

import static com.tsolution.ecommerce.utils.Constants.APP_INFO;

public class AppController extends Application {
    private static AppController mInstance;

    public static final String BASE_URL = "http://192.168.1.4:8234/api/v1/";
    public static final String BASE_LOGIN_URL = "http://123.16.66.14:9999/oauth/token";
    public static final String MERCHANT = "MERCHANT";

    private SharedPreferences sharedPreferences;
    private DecimalFormat formatter;
    private AlertDialog alertDialog;

    HashMap<String,Object> clientCache;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //
        sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        clientCache = new HashMap<>();
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }
    public SOService getLoginService() {
        return RetrofitClient.getClient(BASE_LOGIN_URL).create(SOService.class);
    }
    public SharedPreferences getSharePre() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences.edit();
    }

    /**
     * save object vao file
     * a
     *
     * @param object
     * @param fileName
     * @author: PhamBien
     * @return: void
     * @throws:
     */
    public void saveObject(Object object, String fileName) {
        try {
            FileOutputStream fos = AppController.getInstance().openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch (Exception e) {
            Log.e("Error: ", "Loi write file: " + e.getMessage());

        }
    }


    /**
     * Doc file
     *
     * @param fileName
     * @author: PhamBien
     * @return: void
     * @throws:
     */
    public static Object readObject(String fileName) {
        Object object = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            if (isExistFile(fileName)) {
                fis = AppController.getInstance()
                        .openFileInput(fileName);
                if (fis != null) {// ton tai file
                    ois = new ObjectInputStream(fis);
                    object = ois.readObject();

                }
            }
        } catch (Exception e) {
            Log.e("", "" + e);
            object = null;
            Log.w("", "" + e);
        } finally {
            try {
                if (ois != null) {
                    ois.close();
                }
            } catch (Exception e) {
                Log.e("", "" + e);
            }
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {
                Log.e("", "" + e);
            }
        }
        return object;
    }

    /**
     * Kiem tra file ton tai hay khong
     *
     * @param fileName
     * @return
     * @author PhamBien
     */
    public static boolean isExistFile(String fileName) {
        try {
            if (!StringUtils.isNullOrEmpty(fileName)) {
                String[] s = AppController.getInstance()
                        .fileList();
                for (int i = 0, size = s.length; i < size; i++) {
                    if (fileName.equals(s[i].toString())) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            Log.w("", "" + e.getMessage());
        }
        return false;
    }

    public String formatCurrency(Double money) {
        if (money == null) {
            return "0 đ";
        }
        if (formatter == null) {
            formatter = new DecimalFormat("###,###,###");
        }

        return formatter.format(money) + " đ";
    }
    public void putCatche(String key, Object obj){
        if(clientCache == null){
            clientCache = new HashMap<>();
        }
        clientCache.put(key,obj);
    }
    public Object getFromCatche(String key){
        if(clientCache == null){
            return null;
        }
        return clientCache.get(key);
    }
}
