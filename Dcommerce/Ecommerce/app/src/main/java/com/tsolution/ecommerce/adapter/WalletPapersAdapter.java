package com.tsolution.ecommerce.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.tsolution.ecommerce.view.fragment.UserHistoryFragment;
import com.tsolution.ecommerce.view.fragment.WalletHisFragment;


public class WalletPapersAdapter extends FragmentPagerAdapter {
    private String[] TITLES;
    public WalletHisFragment walletHisFragment;
    public UserHistoryFragment userHistoryFragment;

    public WalletPapersAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                walletHisFragment = new WalletHisFragment().getInstance();
                return walletHisFragment;
            case 1:
                userHistoryFragment = new UserHistoryFragment();
                return userHistoryFragment;

        }
        return null;
    }


}
