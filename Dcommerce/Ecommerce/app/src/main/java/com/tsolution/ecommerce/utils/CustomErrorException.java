package com.tsolution.ecommerce.utils;

import android.util.Log;

import java.util.HashMap;

public class CustomErrorException extends Exception {

    public static final String API_ERROR="apiError";
    public static final String USER_ERROR="userError";

    private String code;
    private String error;

    public CustomErrorException(String code, String error) {
        this.code = code;
        this.error = error;
    }
    public HashMap toMessage(){
        HashMap<String, String> msg = new HashMap<>();
        msg.put(code, error);
        Log.d("Error TS: ",msg.toString());
        return msg;
    }
}
