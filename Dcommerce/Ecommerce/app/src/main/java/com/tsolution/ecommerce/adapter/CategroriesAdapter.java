package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Category;
import com.tsolution.ecommerce.view.activity.CategoryDetailActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CategroriesAdapter extends RecyclerView.Adapter<CategroriesAdapter.MyViewHolder>  {
    private ArrayList<Category> arrData;
    private Context mContext;

    public CategroriesAdapter(Context mContext, ArrayList<Category> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }


    public void updateCategories(ArrayList<Category> items) {
        arrData = items;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cate, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategroriesAdapter.MyViewHolder holder, int i) {
        if(arrData!=null && arrData.size()>0)
        {
            Category movie = arrData.get(i);
            if(movie.getUrlImage() != null && !"".equals(movie.getUrlImage())){
                Picasso.with(mContext).load(movie.getUrlImage()).into(holder.imgCategory);
            }
            holder.nameCategory.setText(movie.getTitle());
        }

    }

    @Override
    public int getItemCount() {
        if(arrData != null && arrData.size() > 0){
            return arrData.size();
        }
        return 0;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nameCategory;
        public CircleImageView imgCategory;
        public MyViewHolder(View view) {
            super(view);
            imgCategory = view.findViewById(R.id.imgCategory);
            nameCategory =  view.findViewById(R.id.txtNameCategory);

            imgCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, CategoryDetailActivity.class);
                    i.putExtra("categoryId", arrData.get(getAdapterPosition()).getCategoryId() + "");
                    mContext.startActivity(i);
                }
            });


        }
    }
}
