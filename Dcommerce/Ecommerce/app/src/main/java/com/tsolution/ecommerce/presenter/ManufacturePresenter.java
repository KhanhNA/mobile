package com.tsolution.ecommerce.presenter;

import android.util.Log;

import com.tsolution.ecommerce.model.Manufacturers;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.view.application.AppController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManufacturePresenter {
    private BaseView baseView;

    public ManufacturePresenter(BaseView base) {
        this.baseView = base;
    }

    public void getManufacturers() {
        AppController.getInstance().getSOService().getManufacturers().enqueue(new Callback<ServiceResponse<Manufacturers>>() {
            @Override
            public void onResponse(Call<ServiceResponse<Manufacturers>> call, Response<ServiceResponse<Manufacturers>> response) {
                if (response.isSuccessful()) {
                    baseView.handleResponseSuccessful(Constants.GET_DATA_MANUFACTURE,new ResponseInfo<ServiceResponse<Manufacturers>>(Constants.GET_DATA_MANUFACTURE,Constants.CODE.SUCCESS,response.body()));
                    Log.e("Manufacturer-onResponse", "load success");
                } else {
                    Log.e("Manufacturer-onResponse", "load fail");
                    baseView.handleResponseError(Constants.GET_DATA_MANUFACTURE,Constants.CODE.ERROR_INTERNAL);
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse<Manufacturers>> call, Throwable t) {
                Log.e("Manufacturer-onFailure", "" + t);
                baseView.handleResponseError(Constants.GET_DATA_MANUFACTURE,Constants.CODE.ERROR_INTERNAL);
            }
        });
    }


}
