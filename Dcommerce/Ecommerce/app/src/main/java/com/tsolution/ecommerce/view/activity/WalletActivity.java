package com.tsolution.ecommerce.view.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.WalletPapersAdapter;
import com.tsolution.ecommerce.model.dto.WalletDto;
import com.tsolution.ecommerce.model.dto.WalletHisDto;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.view.application.AppController;
import com.tsolution.ecommerce.widget.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletActivity extends BaseActivity {
    public Long walletId;
    BasePresenter presenter;
    WalletDto walletDto;
    Long merchantId;
    Intent intent;
    WalletPapersAdapter myPagerAdapter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabLayout)
    SlidingTabLayout slidingTabs;
    @BindView(R.id.pager)
    ViewPager vpOrderTab;
    @BindView(R.id.txtAmount)
    TextView txtAmount;
    @BindView(R.id.pullToReFresh)
    SwipeRefreshLayout pullToReFresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        presenter = new BasePresenter(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.WALLET));
        }
        refresh();
        initView();
        intent = getIntent();
        merchantId = intent.getLongExtra(Constants.MERCHANT_ID, -1);
        if (merchantId != -1) {
            walletDto = new WalletDto();
            walletDto.merchantId = merchantId;
            presenter.get(WalletActivity.this, "commonList", "getWalletSuccessful", walletDto, "Wallet", 0, Constants.LANG_VI);
        } else {
            showAlertDialog("Fail to get token. Please login again.");
            //show dialog comfirm đăng nhập lại
        }

    }

    public void getWalletSuccessful(ServiceResponse<WalletDto> walletDto) {
        if (walletDto != null) {
            ArrayList<WalletDto> walletList = walletDto.getArrData();
            WalletDto wallet = walletList.get(0);
            if (wallet != null && wallet.amount != null) {
                txtAmount.setText(AppController.getInstance().formatCurrency(wallet.amount));
                WalletHisDto walletHisDto = new WalletHisDto();
                this.walletId = wallet.walletId;
                walletHisDto.walletId = wallet.walletId;
                walletHisDto.ORDER_BY = "createDate desc";
                myPagerAdapter.walletHisFragment.getWalletHisList(walletHisDto, 0);
                myPagerAdapter.walletHisFragment.setCurrentPage(0);
                pullToReFresh.setRefreshing(false);
                closeProcess();
            }
        }

    }



    @Override
    public void handleResponseError(String funcName, Constants.CODE code) {
        super.handleResponseError(funcName, code);
        switch (funcName){
            case "commonList":
                closeProcess();
                pullToReFresh.setRefreshing(false);
                break;
        }
    }

    private void initView() {

        slidingTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        slidingTabs.setDistributeEvenly(true);
        myPagerAdapter = new WalletPapersAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.title_wallet));
        vpOrderTab.setAdapter(myPagerAdapter);
        vpOrderTab.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        slidingTabs.setViewPager(vpOrderTab);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        vpOrderTab.setPageMargin(pageMargin);
        vpOrderTab.setCurrentItem(0);
    }

    private void refresh() {
        pullToReFresh.setRefreshing(false);
        pullToReFresh.setSize(50);
        pullToReFresh.setColorSchemeColors(getResources().getColor(R.color.accent), getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent_v2), getResources().getColor(R.color.main_color));
        pullToReFresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToReFresh.setRefreshing(true);
                merchantId = intent.getLongExtra("merchantId", -1);
                presenter.get(WalletActivity.this, "commonList", "getWalletSuccessful", walletDto, "Wallet", 0, Constants.LANG_VI);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wallet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_setting_wallet:
                Intent i = new Intent(WalletActivity.this, WalletSettingActivity.class);
                startActivity(i);
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
