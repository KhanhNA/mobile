package com.tsolution.ecommerce.model;

import android.database.*;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tsolution.ecommerce.db.*;

import java.io.*;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Products implements Serializable {
    @SerializedName("id")
    @Expose
    public Long productId;

    @SerializedName("productName")
    @Expose
    public String productName;

    @SerializedName("urlImage")
    @Expose
    public String url;

    @SerializedName("reviewAvg")
    @Expose
    public float reviewAvg;

    @SerializedName("reviewCount")
    @Expose
    public int reviewCount;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("quantity")
    @Expose
    public double quantity;

    @SerializedName("typePromotion")
    @Expose
    public int typePromotion;

    @SerializedName("number")
    public int currentPage;

    @SerializedName("totalPages")
    public int totalPages;

    public double quantityOfProduct;

    public void initFromCursor(Cursor cursor) {
        productId = cursor.getLong(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_MASP));
        productName = cursor.getString(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_TENSP));
        price = cursor.getInt(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_GIATIEN));
        url = cursor.getString(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_HINHANH));
        quantity = cursor.getDouble(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_SOLUONG));

    }
}
