package com.tsolution.ecommerce.model.response;

import com.google.gson.annotations.SerializedName;
import com.tsolution.ecommerce.model.dto.OrderDTO;

import java.util.ArrayList;

import lombok.Data;

@Data
public class ServiceResponse<T> {
    @SerializedName("content")
    private ArrayList<T> arrData;

    @SerializedName("totalPages")
    private Integer totalPages;
}
