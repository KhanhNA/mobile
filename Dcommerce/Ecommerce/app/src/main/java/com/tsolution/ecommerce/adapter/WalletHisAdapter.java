package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.dto.WalletHisDto;
import com.tsolution.ecommerce.utils.DateUtils;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHisAdapter extends RecyclerView.Adapter<WalletHisAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<WalletHisDto> arrWalletHis;


    public WalletHisAdapter(Context mContext, ArrayList<WalletHisDto> arrWalletHis) {
        this.arrWalletHis = arrWalletHis;
        this.mContext = mContext;
    }

    public void update(ArrayList<WalletHisDto> arrWalletHis, boolean isLoad){
        if(isLoad){
            this.arrWalletHis.addAll(arrWalletHis);
        }
        else {
            this.arrWalletHis = arrWalletHis;
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item_wallet_his, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder view, int i) {
        WalletHisDto walletHisDto = arrWalletHis.get(i);
        if(walletHisDto.walletId != null){
            view.txtOrderID1.setText(String.valueOf(walletHisDto.orderId));
        }
        if (walletHisDto.createDate != null) {
            view.txtOrderDate1.setText(DateUtils.formatDate(walletHisDto.createDate,DateUtils.DATE_STRING_DD_MM_YYYY));
        }
        if(walletHisDto.amount != null){
            view.txtAmount1.setText(AppController.getInstance().formatCurrency(walletHisDto.amount));
        }
    }


    @Override
    public int getItemCount() {
        return arrWalletHis != null ? arrWalletHis.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtOrderDate1)
        TextView txtOrderDate1;
        @BindView(R.id.txtOrderID1)
        TextView txtOrderID1;
        @BindView(R.id.txtAmount1)
        TextView txtAmount1;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            Paint p = new Paint();
            p.setColor(ContextCompat.getColor(mContext,R.color.colorAccent_v2));
            p.setFlags(Paint.UNDERLINE_TEXT_FLAG);
            txtOrderID1.setPaintFlags(p.getFlags());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Show history detail", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


}
