package com.tsolution.ecommerce.view.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.UserHistoryAdapter;
import com.tsolution.ecommerce.model.Order;
import com.tsolution.ecommerce.model.UserHistory;

import java.util.ArrayList;
import java.util.List;

public class UserHistoryFragment extends BaseFragment {
    RecyclerView rcUserHistory;
    private View rootView;
    private UserHistoryAdapter userHistoryAdapter;
    ArrayList<String> img;
    ArrayList<String> title;
    List<UserHistory> arrData = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView= inflater.inflate(R.layout.layout_list_user_history_transfer, container, false);
        return rootView;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rcUserHistory = getView().findViewById(R.id.rcUserHistory);


        genData();
        userHistoryAdapter = new UserHistoryAdapter(getContext(), arrData);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcUserHistory.setLayoutManager(layoutManager);
        rcUserHistory.setAdapter(userHistoryAdapter);



    }
    private void genData() {

        img = new ArrayList<>();
        title = new ArrayList<>();
        img.add("https://cf.shopee.vn/file/de50ba8b024d839e3cc4d746d5852e89_tn");
        img.add("https://www.brandsvietnam.com/upload/newsPics2/2015/04/migoi5_6607.jpg");
        img.add("https://cf.shopee.vn/file/07a993c25b004acb8ca4bb68992fcc6c_tn");
        img.add("https://cf.shopee.vn/file/2eb9f2c993babb863facd9156bdae34d_tn");
        img.add("https://s.meta.com.vn/img/thumb.ashx/data/image/2016/04/01/mi-chinsu-ca-hoi-500.jpg");
        img.add("https://cf.shopee.vn/file/209e7a1d6c5f279a90066c4bc6eb31f0_tn");
        img.add("https://cf.shopee.vn/file/3bda99e21aef90e300df0a2ced51a189_tn");
        img.add("https://cf.shopee.vn/file/ec95c51882de3b68883b52d9ae8e5cd1_tn");
        img.add("https://s.meta.com.vn/img/thumb.ashx/data/image/2016/04/01/mi-chinsu-ca-hoi-500.jpg");
        img.add("https://pbs.twimg.com/media/DBv4eOzXcAEl3Ik.jpg");
        img.add("https://cf.shopee.vn/file/2eb9f2c993babb863facd9156bdae34d_tn");
        img.add("https://cdn.tuoitre.vn/zoom/1200_630/2018/4/18/photo1524031947023-15240319470231548578060.jpg");
        img.add("https://masanfood-cms-production.s3-ap-southeast-1.amazonaws.com/iblock/e57/e57699706080016298db02befbbe4526.jpg");
        img.add("https://cf.shopee.vn/file/c58b5ca95f32416d4b3f6b2188f1ed6b_tn");
        img.add("https://cf.shopee.vn/file/cf8b21e16959cca617e509d0e22c6d9c_tn");
        img.add("https://cf.shopee.vn/file/de50ba8b024d839e3cc4d746d5852e89_tn");
        img.add("https://cf.shopee.vn/file/3bda99e21aef90e300df0a2ced51a189_tn");
        img.add("https://cdn.tuoitre.vn/zoom/1200_630/2018/4/18/photo1524031947023-15240319470231548578060.jpg");

        title.add("");


        for(int i = 0; i < img.size(); i ++){
            title.add("sản phẩm" + i);
            arrData.add(new UserHistory(new Order(img.get(i),
                     title.get(i),
                    (long)40000,
                    "tsolution",
                    "https://www.brandsvietnam.com/upload/news/480px/2014/Masan_1392600440.JPG",
                    i + 1,
                    1), 235, "06/072019", "Giao hàng vào giờ hành chính." )) ;
        }
    }

}
