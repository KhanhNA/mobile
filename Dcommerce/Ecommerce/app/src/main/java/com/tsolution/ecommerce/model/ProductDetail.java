package com.tsolution.ecommerce.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDetail {

    @SerializedName("id")
    @Expose
    private Long productId;

    @SerializedName("productName")
    @Expose
    private String productName;

    @SerializedName("urlImage")
    @Expose
    private String url;

    @SerializedName("reviewAvg")
    @Expose
    private float reviewAvg;

    @SerializedName("reviewCount")
    @Expose
    private int reviewCount;

    @SerializedName("productPrice")
    @Expose
    private double price;

    @SerializedName("productQuantity")
    @Expose
    private int quantity;

    @SerializedName("typePromotion")
    @Expose
    private int typePromotion;

    @SerializedName("manufacturerId")
    @Expose
    private long manufacturerId;
    @SerializedName("manufacturerName")
    @Expose
    private String manufacturerName;

    @SerializedName("incentiveDescription")
    @Expose
    private String incentiveDescription;

    @SerializedName("desProduct")
    @Expose
    private String desProduct;

    @SerializedName("productShip")
    @Expose
    private boolean productShip;

    @SerializedName("imageList")
    @Expose
    private String[] imageList;


}
