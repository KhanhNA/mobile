package com.tsolution.ecommerce.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ContactAdapter;
import com.tsolution.ecommerce.db.ModelContact;
import com.tsolution.ecommerce.model.Contact;
import com.tsolution.ecommerce.model.dto.MerchantDto;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.ContactPresenter;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.StringUtils;
import com.tsolution.ecommerce.view.activity.ContactDetailActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class ContactFragment extends BaseFragment implements ContactAdapter.ItemClickListener {
    ModelContact modelContact;
    ContactAdapter adapter;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout pullToRefresh;
    @BindView(R.id.imgDimisSelect)
    ImageView imgDimisSelect;
    @BindView(R.id.contacts_list)
    RecyclerView recyclerView;
    @BindView((R.id.checkContact))
    LinearLayout checkContact;
    @BindView(R.id.chkListContact)
    CheckBox chkCheckAll;
    @BindView(R.id.dialogContact)
    CardView dialogContact;
    View rootView;
    HashMap<String, MerchantDto> hashMapMerchantList;


    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private List<Contact> contacts;
    private ServiceResponse<MerchantDto> merchantDtoList;
    private MerchantDto merchantDto;
    private ContactPresenter contactPresenter;

    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_contact, container, false);
        ButterKnife.bind(this, rootView);
        modelContact = new ModelContact();
        modelContact.getConnectToSQL(mContext);
        contactPresenter = new ContactPresenter(this);
        hashMapMerchantList = new HashMap<>();
        merchantDtoList = new ServiceResponse<>();
        merchantDto = new MerchantDto();
        //adapter contact
        contacts = new ArrayList<>();
        adapter = new ContactAdapter(getActivity(), contacts);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
        adapter.setItemClickListener(this);

        refresh();
        //Get merchant từ database
        //thay băng id user đăng nhập
        merchantDto.setParentMarchantId((long)1);
        merchantDto.setStatus(1);
        contactPresenter.getMerchants(merchantDto);

        imgDimisSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setShow(false);
                adapter.notifyDataSetChanged();
                checkContact.setVisibility(View.GONE);
                dialogContact.setVisibility(View.GONE);
            }
        });
        chkCheckAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                   @Override
                                                   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                       for(int i = 0; i < contacts.size(); i++){
                                                           contacts.get(i).setStatus(isChecked ? 1 : 0);
                                                       }
                                                       adapter.notifyDataSetChanged();
                                                   }
                                               }
        );
        return rootView;
    }
    private void refresh() {
        pullToRefresh.setRefreshing(false);
        pullToRefresh.setSize(50);
        pullToRefresh.setColorSchemeColors(getResources().getColor(R.color.accent), getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent_v2), getResources().getColor(R.color.main_color));
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh.setRefreshing(true);
                contactPresenter.getMerchants(merchantDto);
            }


        });

    }

    /**
     * @author Good_Boy
     * @return
     */
    private List<Contact> getContactNames() {
        ArrayList<Contact> temp = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            // Get the ContentResolver
            ContentResolver cr = getContext().getContentResolver();
            //get img contact from sqlLite
            HashMap<String, String> imgs = modelContact.getImages();
            // Get the Cursor of all the contacts
            Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            // Move the cursor to first. Also check whether the cursor is empty or not.
            if (cursor.moveToFirst()) {
                // Iterate through the cursor
                do {
                    // Get the contacts name
                    Contact contact = new Contact();
                    contact.setContact_name(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                    String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    contact.setContact_phone(number);
                    String img = imgs.get(number);
                    if(!StringUtils.isNullOrEmpty(img)){
                        contact.setUrlImg(img);
                    }
                    contact.setContact_email(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DISPLAY_NAME)));
                    contact.setRegister(hashMapMerchantList.get(contact.getContact_phone()) != null);
                    temp.add(contact);
                } while (cursor.moveToNext());
            }
            // Close the curosor
            cursor.close();
        }
        return temp;
    }

    private boolean showContact() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            }
            return false;
        }
        //get contact từ danh bạ
        contacts = getContactNames();
        //set contact cho adapter
        adapter.update(contacts);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
                }
                //get contact từ danh bạ
                contacts = getContactNames();
                //set contact cho adapter
                adapter.update(contacts);
            } else {
                Toast.makeText(getContext(), "Until you grant the permission, we cannot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
            if(isLongClick)
            {
                adapter.setShow(true);
                adapter.notifyDataSetChanged();
                checkContact.setVisibility(View.VISIBLE);
                dialogContact.setVisibility(View.VISIBLE);
            }
            else
            {
                final Contact contact = contacts.get(position);
                Intent intent = new Intent(getContext(), ContactDetailActivity.class);
                intent.putExtra("phoneNumber",contact.getContact_phone());
                intent.putExtra("point", contact.getContact_poit());
                intent.putExtra("name", contact.getContact_name());
                intent.putExtra("isRegister", contact.isRegister());
                intent.putExtra("index", position);
                intent.putExtra("img", contact.getUrlImg());
                startActivityForResult(intent, 1);
            }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String phoneNumber = data.getStringExtra("result");
                MerchantDto temp = new MerchantDto();
                temp.setMobilePhone(phoneNumber);
                int index = data.getIntExtra("index", -1);
                if(index != -1 && data.getBooleanExtra("isActive", false)){
                    contacts.get(index).setRegister(true);
                    adapter.notifyDataSetChanged();
                }
            }

        }
    }

    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {
        super.handleResponseError(actionSender, code);
        switch (actionSender) {
            case Constants.GET_DATA_MERCHANT:
                Toast.makeText(getActivity(), "Get fail merchants",Toast.LENGTH_SHORT).show();
                pullToRefresh.setRefreshing(false);
                break;
        }
    }

    @Override
    public void handleResponseSuccessful(int actionSender, ResponseInfo response) {
        super.handleResponseSuccessful(actionSender, response);
        pullToRefresh.setRefreshing(false);
        switch (actionSender) {
            case Constants.GET_DATA_MERCHANT:
                if(response != null && response.responseData != null){
                    merchantDtoList = (ServiceResponse<MerchantDto>) response.responseData;
                    for (MerchantDto merchantDto : merchantDtoList.getArrData()) {
                        hashMapMerchantList.put(merchantDto.getMobilePhone(), merchantDto);
                    }
                    showContact();



                }
                break;
        }
    }

}
