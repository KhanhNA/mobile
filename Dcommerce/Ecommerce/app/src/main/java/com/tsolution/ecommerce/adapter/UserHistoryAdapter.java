package com.tsolution.ecommerce.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.db.chiTietSanPham.PresenterLogicChiTietSanPham;
import com.tsolution.ecommerce.model.UserHistory;
import com.tsolution.ecommerce.utils.FontManager;
import com.tsolution.ecommerce.view.activity.ProductDetailActivity;
import com.tsolution.ecommerce.view.activity.TranferPoitActivity;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.List;

public class UserHistoryAdapter extends RecyclerView.Adapter<UserHistoryAdapter.MyViewHolder> {
    private List<UserHistory> arrData;
    private Context mContext;

    PresenterLogicChiTietSanPham presenterLogicChiTietSanPham;

    public UserHistoryAdapter(Context mContext, List<UserHistory> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_user_history_transfer, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        if (arrData != null && arrData.size() > 0) {
            UserHistory userHistory = arrData.get(i);
            if (mContext.getClass().equals(TranferPoitActivity.class))
                Picasso.with(mContext).load(userHistory.getOrder().getUrlProduct()).into(holder.imgProduct);
            holder.txtNameProduct.setText(userHistory.getOrder().getProductName());
            holder.txtQuantity.setText("x " + userHistory.getOrder().getQuantity());
            if (mContext.getClass().equals(TranferPoitActivity.class)){
                holder.btnCart.setVisibility(View.VISIBLE);
                holder.txtPrice.setText(AppController.getInstance().formatCurrency((double) userHistory.getOrder().getQuantity() * userHistory.getOrder().getPrice()));
            }else {
                holder.btnCart.setVisibility(View.GONE);
                holder.txtPrice.setText("-" + AppController.getInstance().formatCurrency((double) userHistory.getOrder().getQuantity() * userHistory.getOrder().getPrice()));
            }
            holder.txtDescription.setText(userHistory.getDescription());

        }

    }


    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNameProduct, txtQuantity, txtPrice, txtDate, txtDescription, btnCart;
        public ImageView imgProduct;

        public MyViewHolder(View view) {
            super(view);
            Typeface iconFont = FontManager.getTypeface(mContext, FontManager.FONTAWESOME_PRO);
            btnCart = view.findViewById(R.id.btnCart);
            FontManager.markAsIconContainer(btnCart, iconFont);
            txtNameProduct = view.findViewById(R.id.txtNameProduct);
            txtQuantity = view.findViewById(R.id.txtQuantity);
            txtPrice = view.findViewById(R.id.txtPrice);
            imgProduct = view.findViewById(R.id.imgProduct);
            txtDate = view.findViewById(R.id.txtDate);
            txtDescription = view.findViewById(R.id.txtDescription);

            imgProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, ProductDetailActivity.class);
                    i.putExtra("product_id", (long)10);
                    mContext.startActivity(i);
                }
            });
            btnCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Thêm vào giỏ hàng ?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Toast.makeText(mContext, "Sản phẩm đã được thêm vào giỏ hàng", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

        }
    }

}
