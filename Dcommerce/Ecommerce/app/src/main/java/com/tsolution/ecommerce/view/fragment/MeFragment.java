package com.tsolution.ecommerce.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.view.activity.AccountSettingActivity;
import com.tsolution.ecommerce.view.activity.AgentLv1Activity;
import com.tsolution.ecommerce.view.activity.ListOrderedActivity;
import com.tsolution.ecommerce.view.activity.ListOrderedLv2Activity;
import com.tsolution.ecommerce.view.activity.SettingActivity;
import com.tsolution.ecommerce.view.activity.TranferPoitActivity;
import com.tsolution.ecommerce.view.activity.WalletActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

public class MeFragment extends Fragment implements View.OnClickListener {
    @BindView(R.id.backdrop)
    ImageView backdrop;
    @BindView(R.id.contact_img)
    CircleImageView contactImg;
    @BindView(R.id.contact_name)
    TextView contactName;
    @BindView(R.id.contact_poit)
    TextView contactPoit;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.btnDonMua)
    TextView btnDonMua;
    @BindView(R.id.btnDonMuaCap2)
    TextView btnDonMuaCap2;
    @BindView(R.id.btnDoiDiem)
    TextView btnDoiDiem;
    @BindView(R.id.btnVi)
    TextView btnVi;
    @BindView(R.id.btnShareFB)
    TextView btnShareFB;
    @BindView(R.id.btnSetting)
    TextView btnSetting;
    @BindView(R.id.btnHelp)
    TextView btnHelp;
    @BindView(R.id.btnAgentLv1)
    TextView btnAgentLv1;
    Unbinder unbinder;
    @BindView(R.id.btnUserInfo)
    LinearLayout btnUserInfo;
    private View rootView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_me, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //create control

        //set onclick
        btnDonMua.setOnClickListener(this);
        btnDonMuaCap2.setOnClickListener(this);
        btnDoiDiem.setOnClickListener(this);
        btnVi.setOnClickListener(this);
        btnShareFB.setOnClickListener(this);
        btnSetting.setOnClickListener(this);
        btnHelp.setOnClickListener(this);
        btnAgentLv1.setOnClickListener(this);
        btnUserInfo.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.btnUserInfo:
                i = new Intent(getActivity(), AccountSettingActivity.class);
                //thay bằng id user
                i.putExtra(Constants.MERCHANT_ID, (long)1);
                getContext().startActivity(i);
                break;
            case R.id.btnDonMua:
                i = new Intent(getActivity(), ListOrderedActivity.class);
                getContext().startActivity(i);
                break;
            case R.id.btnDonMuaCap2:
                i = new Intent(getActivity(), ListOrderedLv2Activity.class);
                getContext().startActivity(i);
                break;
            case R.id.btnDoiDiem:
                i = new Intent(getActivity(), TranferPoitActivity.class);
                //thay bằng id user
                i.putExtra(Constants.MERCHANT_ID, (long)1);
                getContext().startActivity(i);
                break;
            case R.id.btnVi:
                i = new Intent(getActivity(), WalletActivity.class);
                //thay bằng id user khi đăng nhập
                i.putExtra(Constants.MERCHANT_ID, (long) 1);
                getContext().startActivity(i);
                break;
            case R.id.btnShareFB:
                break;
            case R.id.btnSetting:
                i = new Intent(getActivity(), SettingActivity.class);
                //thay bằng id user khi đăng nhập
                i.putExtra(Constants.MERCHANT_ID, (long) 1);
                getContext().startActivity(i);
                break;
            case R.id.btnHelp:
                break;
            case R.id.btnAgentLv1:
                i = new Intent(getActivity(), AgentLv1Activity.class);
                i.putExtra("index", 1);
                getContext().startActivity(i);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
