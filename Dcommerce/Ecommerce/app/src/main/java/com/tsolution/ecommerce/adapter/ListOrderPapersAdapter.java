package com.tsolution.ecommerce.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.tsolution.ecommerce.model.dto.OrderDTO;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.view.fragment.ListOrderPendingFragment;
import com.tsolution.ecommerce.view.fragment.ListOrderRejectFragment;
import com.tsolution.ecommerce.view.fragment.ListOrderedFragment;


public class ListOrderPapersAdapter extends FragmentPagerAdapter {
    private String[] TITLES;
    private ListOrderedFragment listOrderedFragment;
    private ListOrderPendingFragment listOrderPendingFragment;
    private ListOrderRejectFragment listOrderRejectFragment;

    public ListOrderPapersAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        //Show Fragment OrderDTO order
        OrderDTO order = OrderDTO.builder().langId(Constants.LANG_VI).merchantId((long)1).orderType(1).build();
        switch (position) {
            case 0:
                listOrderPendingFragment = new ListOrderPendingFragment();
                order.setOrderStatus(0);
                listOrderPendingFragment.setOrderDTO(order);
                return listOrderPendingFragment;
            case 1:
                listOrderedFragment = new ListOrderedFragment();
                order.setOrderStatus(1);
                listOrderedFragment.setOrderDTO(order);
                return listOrderedFragment;
            case 2:
                listOrderRejectFragment = new ListOrderRejectFragment();
                order.setOrderStatus(2);
                listOrderRejectFragment.setOrderDTO(order);
                return listOrderRejectFragment;

        }
        return null;
    }
}
