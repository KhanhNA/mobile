package com.tsolution.ecommerce.view.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingActivity extends BaseActivity implements View.OnClickListener {
    BasePresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnUserInformation)
    TextView btnUserInformation;
    @BindView(R.id.btnAddress)
    TextView btnAddress;
    @BindView(R.id.btnPhoneNumber)
    TextView btnPhoneNumber;
    @BindView(R.id.btnIdCard)
    TextView btnIdCard;
    @BindView(R.id.btnLanguage)
    TextView btnLanguage;
    @BindView(R.id.btnChangePass)
    TextView btnChangePass;
    @BindView(R.id.btnLogout)
    Button btnLogout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        presenter = new BasePresenter(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.action_settings));
        }

        //setOnclick
        btnUserInformation.setOnClickListener(this);
        btnAddress.setOnClickListener(this);
        btnChangePass.setOnClickListener(this);
        btnIdCard.setOnClickListener(this);
        btnLanguage.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnPhoneNumber.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.btnUserInformation:
                i = new Intent(SettingActivity.this, AccountSettingActivity.class);
                i.putExtra(Constants.MERCHANT_ID, (long)1);
                startActivity(i);
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_chat:
                Intent i = new Intent(SettingActivity.this, ChatActivity.class);
                startActivity(i);
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}
