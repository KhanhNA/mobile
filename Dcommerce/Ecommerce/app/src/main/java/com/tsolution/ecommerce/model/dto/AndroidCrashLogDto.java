package com.tsolution.ecommerce.model.dto;


import java.util.Date;

/**
 * 
 * @author ts-client01
 * Create at 2019-06-27 15:57
 */

public class AndroidCrashLogDto {

    /**
     * 
     */
    public Long id;

    /**
     * 
     */
    public Long merchantId;

    /**
     * 
     */
    public String error;
    public String errorDetail;

    /**
     * 
     */
    public Date createDate;

    public AndroidCrashLogDto(Long merchantId, String error) {

        this.merchantId = merchantId;
        this.error = error;

    }
}