package com.tsolution.ecommerce.db.chiTietSanPham;

import android.content.*;

import com.tsolution.ecommerce.db.*;
import com.tsolution.ecommerce.model.*;
import com.tsolution.ecommerce.model.dto.*;

import java.util.*;

public class PresenterLogicChiTietSanPham implements IPresenterChiTietSanPham {
    ModelGioHang modelGioHang;
    ViewChiTietSanPham viewChiTietSanPham;

    public PresenterLogicChiTietSanPham() {
        modelGioHang = new ModelGioHang();
    }

    public PresenterLogicChiTietSanPham(ViewChiTietSanPham viewChiTietSanPham) {
        this.viewChiTietSanPham = viewChiTietSanPham;
        modelGioHang = new ModelGioHang();
    }

    @Override
    public void ThemGioHang(Products sanPham, Context context) {
        modelGioHang.MoKetNoiSQL(context);
        boolean kiemtra = modelGioHang.ThemGioHang(sanPham);
        if (kiemtra) {
            viewChiTietSanPham.ThemGioHangThanhCong();
        } else {
            viewChiTietSanPham.ThemGiohangThatBai();
        }
    }

    public int DemSanPhamCoTrongGioHang(Context context){
        modelGioHang.MoKetNoiSQL(context);
        List<Products> sanPhamList =modelGioHang.LayDanhSachSanPhamTrongGioHang();
        int dem =sanPhamList.size();
        return dem;
    }

    public double getQuantityProduct(Context context, ProductDto dto){
        modelGioHang.MoKetNoiSQL(context);
        double quantityProduct = modelGioHang.getQuantiTyProduct(dto);
        return quantityProduct;
    }


}
