package com.tsolution.ecommerce.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.tsolution.ecommerce.view.fragment.ContactFragment;
import com.tsolution.ecommerce.view.fragment.UserHistoryFragment;


public class TransferPoitPapersAdapter extends FragmentPagerAdapter {
    private String[] TITLES;
    private UserHistoryFragment sampleFragment;
    private ContactFragment contact_fragment;

    public TransferPoitPapersAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        //Show Fragment
        switch (position) {
            case 0:
               sampleFragment = new UserHistoryFragment();
               return sampleFragment;
            case 1:
                contact_fragment = new ContactFragment();
                return contact_fragment;

        }
        return null;
    }
}
