package com.tsolution.ecommerce.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ProductAdapterHorizontal;
import com.tsolution.ecommerce.db.chiTietSanPham.PresenterLogicChiTietSanPham;
import com.tsolution.ecommerce.db.chiTietSanPham.ViewChiTietSanPham;
import com.tsolution.ecommerce.model.ProductDetail;
import com.tsolution.ecommerce.model.Products;
import com.tsolution.ecommerce.model.dto.ProductDto;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.ProductsPresenter;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.FontManager;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProductDetailActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener, ViewChiTietSanPham, ViewPagerEx.OnPageChangeListener, View.OnClickListener {
    private static ProductDetailActivity mInstance;
    private long productId;
    private ProductAdapterHorizontal productAdapter;
    ProductDetail productDetail;
    ProductsPresenter productsPresenter;
    @BindView(R.id.slider)
    SliderLayout mDemoSlider;
    @BindView(R.id.iconBack)
    TextView iconBack;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.scroll)
    NestedScrollView scrollView;
    @BindView(R.id.toolbar)
    LinearLayout toolbar;
    @BindView(R.id.rcRelativeProduct)
    RecyclerView rcRelativeProduct;
    @BindView(R.id.txtQuantity)
    TextView txtQuantity;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.txtNameProduct)
    TextView name;
    @BindView(R.id.txtPrice)
    TextView price;
    @BindView(R.id.number_rate)
    TextView numberRating;
    @BindView(R.id.txtPriceSale)
    TextView priceSale;
    @BindView(R.id.ratingCount)
    TextView ratingCount;
    @BindView(R.id.imgCart)
    ImageView imgCart;
    @BindView(R.id.txtQuantityProductCart)
    TextView txtQuantityProductCart;
    @BindView(R.id.frCart)
    FrameLayout frCart;
    @BindView(R.id.iconCart)
    TextView iconCart;
    @BindView(R.id.bottom_sheet_quantity_product)
    LinearLayout bottom_sheet_quantity_product;
    @BindView(R.id.btnAddToCart)
    Button btnAddToCart;
    @BindView(R.id.edSoLuongSanPham)
    EditText edSoLuongSanPham;
    @BindView(R.id.imGiamSoLuongSPTrongGioHang)
    ImageView imGiamSoLuongSPTrongGioHang;
    @BindView(R.id.imTangSoLuongSPTrongGioHang)
    ImageView imTangSoLuongSPTrongGioHang;
    @BindView(R.id.txtBuy)
    TextView txtBuy;
    @BindView(R.id.imgShare)
    ImageView imgShare;
    //bottom_sheet
    BottomSheetBehavior sheetBehavior;
    ExpandableTextView expTv1;

    public static final String FACEBOOK_PACKAGE_NAME = "com.facebook.katana";
    public static final String TWITTER_PACKAGE_NAME = "com.twitter.android";
    public static final String INSTAGRAM_PACKAGE_NAME = "com.instagram.android";
    public static final String PINTEREST_PACKAGE_NAME = "com.pinterest";
    public static final String WHATS_PACKAGE_NAME = "com.whatsapp";

    boolean onPause = false;
    boolean isBuyNow = false;

    PresenterLogicChiTietSanPham presenterLogicChiTietSanPham;


    public ProductDetailActivity getInstance() {
        if (mInstance == null) {
            mInstance = new ProductDetailActivity();
        }
        return mInstance;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_product_detail_v2);
        ButterKnife.bind(this);
        productsPresenter = new ProductsPresenter(this);
        imGiamSoLuongSPTrongGioHang.setOnClickListener(this);
        imTangSoLuongSPTrongGioHang.setOnClickListener(this);
        isBuyNow = false;
        txtBuy.setOnClickListener(this);
        btnAddToCart.setOnClickListener(this);
        imgCart.setOnClickListener(this);
        frCart.setOnClickListener(this);
        imgShare.setOnClickListener(this);
        initToolbar();
        onScroll();
        presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham(this);
        //bottom_sheet_dialog
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet_quantity_product);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.layout_product_detail_v2);
            ButterKnife.bind(this);
            productsPresenter = new ProductsPresenter(this);
            initToolbar();
            onScroll();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void initToolbar() {
        Typeface iconFont = FontManager.getTypeface(ProductDetailActivity.this, FontManager.FONTAWESOME_PRO);
        FontManager.markAsIconContainer(iconBack, iconFont);
        FontManager.markAsIconContainer(iconCart, iconFont);


        Intent intent = getIntent();
        expTv1 = findViewById(R.id.sample1)
                .findViewById(R.id.expand_text_view);
        priceSale.setPaintFlags(priceSale.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        productId = intent.getLongExtra("product_id", -1);
        if (productId != -1) {
            productsPresenter.getProductById(productId);
        }
        productsPresenter.getProducts(0);
    }

    private void onScroll() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView nestedScrollView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == 0) {
                    toolbar.setBackgroundColor(Color.parseColor("#00ffffff"));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        iconBack.setBackground(getResources().getDrawable(R.drawable.circle_v1));
                        iconCart.setBackground(getResources().getDrawable(R.drawable.circle_v1));
                    }
                    iconBack.setTextColor(Color.parseColor("#ffffff"));
                    iconCart.setTextColor(Color.parseColor("#ffffff"));
                    txtTitle.setTextColor(Color.parseColor("#00000000"));

                } else if (scrollY > 50 && scrollY < 100) {
                    toolbar.setBackgroundColor(Color.parseColor("#33ffffff"));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        iconBack.setBackground(getResources().getDrawable(R.drawable.circle_v2));
                        iconCart.setBackground(getResources().getDrawable(R.drawable.circle_v2));
                    }
                    iconBack.setTextColor(Color.parseColor("#ccffffff"));
                    iconCart.setTextColor(Color.parseColor("#ccffffff"));
                    txtTitle.setTextColor(Color.parseColor("#33000000"));
                } else if (scrollY > 100 && scrollY < 200) {
                    toolbar.setBackgroundColor(Color.parseColor("#88ffffff"));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        iconBack.setBackground(getResources().getDrawable(R.drawable.circle_v3));
                        iconCart.setBackground(getResources().getDrawable(R.drawable.circle_v3));
                    }
                    iconCart.setTextColor(Color.parseColor("#ccffffff"));
                    iconBack.setTextColor(Color.parseColor("#ccffffff"));
                    txtTitle.setTextColor(Color.parseColor("#88000000"));
                } else if (scrollY > 200 && scrollY < 300) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        iconBack.setBackground(getResources().getDrawable(R.drawable.circle_v3));
                        iconCart.setBackground(getResources().getDrawable(R.drawable.circle_v3));
                    }
                    toolbar.setBackgroundColor(Color.parseColor("#ccffffff"));
                    iconBack.setTextColor(Color.parseColor("#77B20024"));
                    iconCart.setTextColor(Color.parseColor("#77B20024"));
                    txtTitle.setTextColor(Color.parseColor("#882d2d2d"));
                } else if (scrollY > 300 && scrollY < 500) {
                    toolbar.setBackgroundColor(Color.parseColor("#ffffffff"));
                    iconBack.setBackgroundColor(Color.WHITE);
                    iconCart.setBackgroundColor(Color.WHITE);
                    iconBack.setTextColor(getResources().getColor(R.color.main_color));
                    iconCart.setTextColor(getResources().getColor(R.color.main_color));
                    txtTitle.setTextColor(Color.parseColor("#2d2d2d"));
                }
            }
        });
    }


    // sliderCollection
    private void sliderCollection(HashMap<String, String> url_maps) {
        for (String title : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(ProductDetailActivity.this);
            // initialize a SliderLayout
            textSliderView
                    .description(title)
                    .image(url_maps.get(title))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", title);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(36000);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
    }

    @OnClick(R.id.iconBack)
    public void onBack() {
        super.onBackPressed();
    }

    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {
        super.handleResponseError(actionSender, code);
        switch (actionSender) {
            case Constants.GET_PRODUCT_DETAIL:
                closeProcess();
                break;
        }
    }

    @Override
    public void handleResponseSuccessful(int actionSender, ResponseInfo response) {
        super.handleResponseSuccessful(actionSender, response);
        switch (actionSender) {
            case Constants.GET_PRODUCT_DETAIL:
                if (response != null && response.responseData != null) {
                    productDetail = (ProductDetail) response.responseData;
                    HashMap<String, String> url_maps = new HashMap();
                    //set data for view
                    if (productDetail.getProductName() != null && !"".equals(productDetail.getProductName())) {
                        txtTitle.setText(productDetail.getProductName());
                        name.setText(productDetail.getProductName());
                    }
                    if (productDetail.getDesProduct() != null && !"".equals(productDetail.getDesProduct())) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            expTv1.setText(Html.fromHtml(productDetail.getDesProduct(), Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            expTv1.setText(Html.fromHtml(productDetail.getDesProduct()));
                        }
                    }
                    ratingBar.setRating(productDetail.getReviewAvg());
                    ratingCount.setText(productDetail.getReviewCount() + "");
                    numberRating.setText(productDetail.getReviewAvg() + "");
                    price.setText(AppController.getInstance().formatCurrency(productDetail.getPrice()));
                    txtQuantity.setText(productDetail.getQuantity() + "");
//                    if (productDetail.getImageList() != null && productDetail.getImageList().length > 0) {
//                        int length = productDetail.getImageList().length;
//                        for (int i = 0; i < length; i++) {
//                            url_maps.put((i + 1) + "/" + (length + 1), productDetail.getImageList()[i]);
//                        }
//
//                    }

                    url_maps = new HashMap();
                    url_maps.put("Masan", "https://cf.shopee.vn/file/0e993556e72aeca9e62e278bc1e5990f");
                    url_maps.put("chisu", "https://cf.shopee.vn/file/2f03d279dbb459517841a52b51012793");
                    url_maps.put("fast food", "https://cf.shopee.vn/file/f8b77000882a94362860fb7eb0be285f");
                    url_maps.put("B'Fast", "https://masanfood-cms-production.s3-ap-southeast-1.amazonaws.com/iblock/623/62326d58273e62c4692f773c9892fd0e.jpg");
                    sliderCollection(url_maps);
                }
                closeProcess();
                break;
            case Constants.GET_DATA_PRODUCT:
                if (response != null && response.responseData != null) {
                    ServiceResponse<ProductDto> productsResponse = (ServiceResponse<ProductDto>) response.responseData;
                    productAdapter = new ProductAdapterHorizontal(this, productsResponse.getArrData(), null);
                    rcRelativeProduct.setAdapter(productAdapter);
                    GridLayoutManager layoutProducts
                            = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);
                    rcRelativeProduct.setLayoutManager(layoutProducts);

                }
                break;
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        PresenterLogicChiTietSanPham presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();
        if (presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this) > 0) {
            txtQuantityProductCart.setVisibility(View.VISIBLE);
            txtQuantityProductCart.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
        } else {
            txtQuantityProductCart.setVisibility(View.GONE);
        }
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        onPause = true;
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.imgCart:
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                break;
            case R.id.btnAddToCart:
                addProductToCart();
            case R.id.imGiamSoLuongSPTrongGioHang:
                double soluong = Double.parseDouble(edSoLuongSanPham.getText().toString());
                if (soluong > 1) {
                    soluong--;
                }
                edSoLuongSanPham.setText(String.valueOf(soluong));
                break;
            case R.id.imTangSoLuongSPTrongGioHang:
                double soluong1 = Double.parseDouble(edSoLuongSanPham.getText().toString());
                soluong1++;
                edSoLuongSanPham.setText(String.valueOf(soluong1));
//                modelGioHang.CapNhatSoLuongSanPhamGioHang(sanPham.getProductId(),soluong);
                break;
            case R.id.txtBuy:
                isBuyNow = true;
                addProductToCart();
                break;
            case R.id.frCart:
                Intent intent = new Intent(ProductDetailActivity.this, CartActivity.class);
                startActivity(intent);
                break;
            case R.id.imgShare:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, FACEBOOK_PACKAGE_NAME);
                    String sAux = "Chia sẻ với";
                    sAux = sAux + "https://play.google.com/store/apps/details?id=Orion.Soft \n\n";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (android.content.ActivityNotFoundException e) {
                    Toast.makeText(this, "app have not been installed.", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void addProductToCart() {
        Products sanPhamGioHang = Products.builder().build();
        sanPhamGioHang.setProductId(productDetail.getProductId());
        sanPhamGioHang.setProductName(productDetail.getProductName());
        sanPhamGioHang.setPrice(productDetail.getPrice());
        sanPhamGioHang.setUrl(productDetail.getUrl());
        sanPhamGioHang.setQuantity(Double.parseDouble(edSoLuongSanPham.getText().toString()));
        presenterLogicChiTietSanPham.ThemGioHang(sanPhamGioHang, this);
    }


    @Override
    public void ThemGioHangThanhCong() {
        Toast.makeText(this, "Sản phẩm đã được thêm vào giỏ hàng !", Toast.LENGTH_SHORT).show();
        if (presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this) > 0) {
            txtQuantityProductCart.setVisibility(View.VISIBLE);
            txtQuantityProductCart.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(this)));
        } else {
            txtQuantityProductCart.setVisibility(View.GONE);
        }
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
        if (isBuyNow) {
            Intent intent = new Intent(ProductDetailActivity.this, CartActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void ThemGiohangThatBai() {
        if (isBuyNow) {
            Intent intent = new Intent(ProductDetailActivity.this, CartActivity.class);
            startActivity(intent);
        } else
            Toast.makeText(this, "Sản phẩm đã tồn tại trong giỏ hàng!", Toast.LENGTH_SHORT).show();
    }
}
