package com.tsolution.ecommerce.db.GioHang;

import com.tsolution.ecommerce.model.*;

import java.util.*;

public interface ViewGioHang {
    void HienThiDanhSachSanPhamTrongGioHang(List<Products> sanPhamList);
}
