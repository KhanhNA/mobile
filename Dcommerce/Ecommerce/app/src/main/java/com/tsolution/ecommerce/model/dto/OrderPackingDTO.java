package com.tsolution.ecommerce.model.dto;


import lombok.Data;

@Data
public class OrderPackingDTO {
    public static final int INCENTIVE = 1;
    public static final int SALE = 0;
    private Long orderPackingId;
    private Double quantity;
    private Long incentiveProgramId;
    private Double price;
    private Double vat;
    private Long packingProductId;
    private Double amount;
    private Long productId;
    private Double incentiveAmount = (double) 0;

    private Double remainQuantity = (double) 0;
    private Double remainAmount = (double) 0;

    private Double incentiveTotal = (double) 0;
    private Integer isIncentive;
    private double orderPercent;//% dong gop vao don hang

    private String packingName;
    private String packingUrl;


}
