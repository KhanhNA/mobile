package com.tsolution.ecommerce.view.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.PackingOrderAdapter;
import com.tsolution.ecommerce.model.dto.OrderDetailDTO;
import com.tsolution.ecommerce.model.dto.OrderPackingDTO;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.UtilsTs;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderDetailActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rcPacking)
    RecyclerView rcPacking;
    @BindView(R.id.temCal)
    TextView temCal;
    @BindView(R.id.vat)
    TextView vat;
    @BindView(R.id.moneySave)
    TextView moneySave;
    @BindView(R.id.totalMoney)
    TextView totalMoney;
    @BindView(R.id.rcIncentiveProduct)
    RecyclerView rcIncentiveProduct;
    @BindView(R.id.layoutIncentive)
    LinearLayout layoutIncentive;
    @BindView(R.id.txtOrderNumber)
    TextView txtOrderNumber;
    @BindView(R.id.txtMerchantName)
    TextView txtMerchantName;
    @BindView(R.id.txtNumberPhone)
    TextView txtNumberPhone;
    @BindView(R.id.txtOrderDate)
    TextView txtOrderDate;
    @BindView(R.id.btnConfirm)
    Button btnConfirm;
    @BindView(R.id.btnReject)
    Button btnReject;
    @BindView(R.id.layoutSubmit)
    LinearLayout layoutSubmit;
    private BasePresenter basePresenter;
    private PackingOrderAdapter packingOrderAdapter;
    private PackingOrderAdapter incentivePorductAdapter;
    private ArrayList<OrderPackingDTO> mData;
    private ArrayList<OrderPackingDTO> arrIncentiveProduct;
    private OrderDetailDTO orderDTO;
    private int oldStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(4);
        }
        basePresenter = new BasePresenter(this);
        mData = new ArrayList<>();
        arrIncentiveProduct = new ArrayList<>();
        layoutIncentive.setVisibility(View.GONE);
        rcIncentiveProduct.setNestedScrollingEnabled(false);
        rcPacking.setNestedScrollingEnabled(false);
        packingOrderAdapter = new PackingOrderAdapter(OrderDetailActivity.this, mData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OrderDetailActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcPacking.setLayoutManager(linearLayoutManager);
        rcPacking.setAdapter(packingOrderAdapter);


        incentivePorductAdapter = new PackingOrderAdapter(OrderDetailActivity.this, arrIncentiveProduct);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(OrderDetailActivity.this);
        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        rcIncentiveProduct.setLayoutManager(linearLayoutManager2);
        rcIncentiveProduct.setAdapter(incentivePorductAdapter);

        if (getIntent().hasExtra("orderDTO")) {
            orderDTO = (OrderDetailDTO) getIntent().getSerializableExtra("orderDTO");
            basePresenter.get(this, "getOrderDetails", "handleResponseOrderDetailSuccessful", orderDTO);
            if (orderDTO.getOrderStatus() != null)
                oldStatus = orderDTO.getOrderStatus();
        }
        btnConfirm.setVisibility(View.GONE);
        if (UtilsTs.isObjectNotNull(orderDTO) && orderDTO.getOrderStatus() != null) {
            if (orderDTO.getOrderStatus() == Constants.ORDER.APPROVE.getCode()) {
                btnConfirm.setVisibility(View.GONE);
            } else if (orderDTO.getOrderStatus() == Constants.ORDER.REJECT.getCode()) {
                btnConfirm.setVisibility(View.GONE);
            }
            if (orderDTO.getOrderStatus() == Constants.ORDER.PENDING.getCode()) {
                btnConfirm.setVisibility(View.VISIBLE);
                btnConfirm.setText(getString(R.string.APPROVE));
            }
        }
    }

    @OnClick(R.id.btnConfirm)
    public void btnConfirm() {
        if (UtilsTs.isObjectNotNull(orderDTO) && orderDTO.getOrderStatus() != null) {
            if (orderDTO.getOrderStatus() == Constants.ORDER.PENDING.getCode()) {
                showProcessing("Đang xử lý...");
                orderDTO.setOrderStatus(Constants.ORDER.APPROVE.getCode());
                basePresenter.get(this, "changeOrder", "handleResponseSuccessfulChangeOrder", orderDTO);
                layoutSubmit.setVisibility(View.GONE);
            }
        }
    }

    @OnClick(R.id.btnReject)
    public void btnReject() {
        if (UtilsTs.isObjectNotNull(orderDTO) && orderDTO.getOrderStatus() != null) {
            if (orderDTO.getOrderStatus() == Constants.ORDER.PENDING.getCode()) {
                showProcessing("Đang xử lý...");
                orderDTO.setOrderStatus(Constants.ORDER.REJECT.getCode());
                basePresenter.get(this, "changeOrder", "handleResponseSuccessfulChangeOrder", orderDTO);
                layoutSubmit.setVisibility(View.GONE);
            }
        }
    }

    public void handleResponseSuccessfulChangeOrder(ServiceResponse response) {
        Toast.makeText(OrderDetailActivity.this, getString(R.string.CHANGE_STATUS_ORDER_SUCCESS), Toast.LENGTH_LONG).show();
    }


    public void handleResponseOrderDetailSuccessful(ServiceResponse<OrderDetailDTO> response) {
        if (UtilsTs.isNotNull(response.getArrData()) && UtilsTs.isNotNull(response.getArrData().get(0).getOrderPackings())) {
            OrderDetailDTO orderDTO = (OrderDetailDTO) response.getArrData().get(0);
            temCal.setText(AppController.getInstance().formatCurrency(orderDTO.getAmount() - orderDTO.getVat()));
            vat.setText(AppController.getInstance().formatCurrency(orderDTO.getVat()));
            moneySave.setText(AppController.getInstance().formatCurrency(orderDTO.getWalletAmount()));
            totalMoney.setText(AppController.getInstance().formatCurrency(orderDTO.getAmount()));
            txtOrderNumber.setText(getString(R.string.order) + " " + orderDTO.getOrderNo());
            txtMerchantName.setText(orderDTO.getMerchantName());
            for (OrderPackingDTO dto : orderDTO.getOrderPackings()) {
                if (dto.getIsIncentive() != null && dto.getIsIncentive() == Constants.INCENTIVE) {
                    arrIncentiveProduct.add(dto);
                } else {
                    mData.add(dto);
                }
            }
            if (UtilsTs.isNotNull(arrIncentiveProduct)) {
                layoutIncentive.setVisibility(View.VISIBLE);
                incentivePorductAdapter.notifyDataSetChanged();
            } else {
                layoutIncentive.setVisibility(View.GONE);
            }

            packingOrderAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void handleResponseError(String funcName, Constants.CODE code) {
        super.handleResponseError(funcName, code);
        switch (funcName) {
            case "changeOrder":
                closeProcess();
                layoutSubmit.setVisibility(View.VISIBLE);
                orderDTO.setOrderStatus(oldStatus);
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_chat:
                Intent i = new Intent(OrderDetailActivity.this, ChatActivity.class);
                startActivity(i);

                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
