package com.tsolution.ecommerce.presenter;

import com.tsolution.ecommerce.model.ProductDetail;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.utils.Constants;

public class ProductsPresenter  extends BasePresenter  {
    private BaseView baseView;

    public ProductsPresenter(BaseView base) {
        super(base);
        this.baseView = base;
    }

    public void getProducts(int page) {
        get(this,"getProducts","handleResponseProductSuccessful", page,Constants.LANG_VI );
    }
    public void getProductById(long id) {
        get(this,"getProductById","handleResponseSuccessfulDetail", id, Constants.LANG_VI);
    }
    public void handleResponseProductSuccessful(ServiceResponse response) {
        baseView.handleResponseSuccessful(Constants.GET_DATA_PRODUCT, new ResponseInfo<>(response, Constants.CODE.SUCCESS));
    }
    public void handleResponseSuccessfulDetail(ProductDetail response) {
        baseView.handleResponseSuccessful(Constants.GET_PRODUCT_DETAIL, new ResponseInfo<>(response, Constants.CODE.SUCCESS));
    }
}
