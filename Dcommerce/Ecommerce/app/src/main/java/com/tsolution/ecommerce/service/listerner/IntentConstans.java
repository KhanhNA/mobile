package com.tsolution.ecommerce.service.listerner;

public interface IntentConstans {
    public static final String INTENT_LIST_PRODUCT = "ListProducts";
    public static final String INTENT_TOTAL_MONEY ="totalMoney" ;
    public static final String INTENT_MONEY_TO_WALLET ="walletMoney" ;
    public static final String INTENT_MONEY_DISCOUNT ="moneyDiscount" ;
    public static final String INTENT_METHOD_PAYMENT ="methodPayment";
    public static final String INTENT_ORDER_SUCCESS ="orderSuccess";
}
