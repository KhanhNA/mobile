package com.tsolution.ecommerce.model.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 *
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Data
public class MerchantDto {
    private Long merchantId;
    private String merchantCode;
    private String merchantName;
    private Long parentMarchantId;
    private Long merchantTypeId;
    private String userName;
    private String password;
    private String fullName;
    private Long defaultStoreId;
    private Double tax;
    private String merchantImgUrl;
    private String mobilePhone;
    private String activeCode;
    private Date activeDate;
    private Date registerDate;
    private Integer activeStatus;
    private Integer status;
    private String address;

}