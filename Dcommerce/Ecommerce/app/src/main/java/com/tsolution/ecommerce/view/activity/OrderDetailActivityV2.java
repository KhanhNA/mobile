package com.tsolution.ecommerce.view.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.IncentiveProductAdapter;
import com.tsolution.ecommerce.adapter.PackingOrderAdapter;
import com.tsolution.ecommerce.model.dto.OrderDetailDTO;
import com.tsolution.ecommerce.model.dto.OrderPackingDTO;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.UtilsTs;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderDetailActivityV2 extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.spPhuongThucVanChuyen)
    Spinner spPhuongThucVanChuyen;
    @BindView(R.id.txtMethodPayment)
    TextView txtMethodPayment;
    @BindView(R.id.lnThanhToan)
    LinearLayout lnThanhToan;
    @BindView(R.id.txtdiaChiNhanHang)
    TextView txtdiaChiNhanHang;
    @BindView(R.id.spChonKho)
    Spinner spChonKho;
    @BindView(R.id.recyclerSanPham)
    RecyclerView recyclerSanPham;
    @BindView(R.id.rcIncentiveProduct)
    RecyclerView rcIncentiveProduct;
    @BindView(R.id.tvWalletMoney)
    TextView tvWalletMoney;
    @BindView(R.id.tvmoneyDicount)
    TextView tvmoneyDicount;
    @BindView(R.id.tvToTalMoney)
    TextView tvToTalMoney;
    @BindView(R.id.layoutIncentive)
    LinearLayout layoutIncentive;
    @BindView(R.id.layoutSubmit)
    LinearLayout layoutSubmit;
    @BindView(R.id.btnConfirm)
    Button btnConfirm;
    @BindView(R.id.btnReject)
    Button btnReject;

    private BasePresenter basePresenter;
    private OrderDetailDTO orderDTO;
    private int oldStatus;
    private ArrayList<OrderPackingDTO> arrIncentiveProduct;
    private ArrayList<OrderPackingDTO> arrPackingProduct;
    private IncentiveProductAdapter incentivePorductAdapter;
    private PackingOrderAdapter packingOrderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_his_order);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(4);
        }
        arrPackingProduct = new ArrayList<>();
        arrIncentiveProduct = new ArrayList<>();
        basePresenter = new BasePresenter(this);
        packingOrderAdapter = new PackingOrderAdapter(OrderDetailActivityV2.this, arrPackingProduct);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OrderDetailActivityV2.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerSanPham.setLayoutManager(linearLayoutManager);
        recyclerSanPham.setAdapter(packingOrderAdapter);


        incentivePorductAdapter = new IncentiveProductAdapter(OrderDetailActivityV2.this, arrIncentiveProduct);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(OrderDetailActivityV2.this);
        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        rcIncentiveProduct.setLayoutManager(linearLayoutManager2);
        rcIncentiveProduct.setAdapter(incentivePorductAdapter);

        rcIncentiveProduct.setNestedScrollingEnabled(false);
        recyclerSanPham.setNestedScrollingEnabled(false);

        if (getIntent().hasExtra("orderDTO")) {
            orderDTO = (OrderDetailDTO) getIntent().getSerializableExtra("orderDTO");
            basePresenter.get(this, "getOrderDetails", "getOrderSuccessFul", orderDTO);
            if (orderDTO.getOrderStatus() != null)
                oldStatus = orderDTO.getOrderStatus();
        }
        btnConfirm.setVisibility(View.GONE);
        if (UtilsTs.isObjectNotNull(orderDTO) && orderDTO.getOrderStatus() != null) {
            if (orderDTO.getOrderStatus().equals(Constants.ORDER.APPROVE.getCode())) {
                layoutSubmit.setVisibility(View.GONE);
            } else if (orderDTO.getOrderStatus().equals(Constants.ORDER.REJECT.getCode())) {
                layoutSubmit.setVisibility(View.GONE);
            }
            if (orderDTO.getOrderStatus().equals(Constants.ORDER.PENDING.getCode())) {
                btnConfirm.setText(getString(R.string.APPROVE));
                btnConfirm.setVisibility(View.VISIBLE);
                btnReject.setVisibility(View.VISIBLE);
                layoutSubmit.setVisibility(View.VISIBLE);
            }
        }
        //

        List<String> list2 = new ArrayList<>();
        list2.add("Lấy hàng trực tiếp");
        list2.add("Ship");
        ArrayAdapter<String> adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item,list2);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPhuongThucVanChuyen.setAdapter(adapter2);
        spPhuongThucVanChuyen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //spChonKho
        List<String> list = new ArrayList<>();
        list.add("Kho DC1");
        list.add("Kho DC2");
        list.add("Kho DC3");
        list.add("Kho DC4");
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spChonKho.setAdapter(adapter);
        spChonKho.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void hideView() {
        btnConfirm.setVisibility(View.GONE);
        btnReject.setVisibility(View.GONE);
        layoutSubmit.setVisibility(View.GONE);
    }

    private void showView() {
        btnConfirm.setVisibility(View.VISIBLE);
        btnReject.setVisibility(View.VISIBLE);
        layoutSubmit.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btnConfirm)
    public void btnConfirm() {
        if (UtilsTs.isObjectNotNull(orderDTO) && orderDTO.getOrderStatus() != null) {
            if (orderDTO.getOrderStatus().equals(Constants.ORDER.PENDING.getCode())) {
                showProcessing("Đang xử lý...");
                orderDTO.setOrderStatus(Constants.ORDER.APPROVE.getCode());
                basePresenter.get(this, "changeOrder", "changeOrderSuccessFul", orderDTO);
                hideView();
            }
        }
    }

    @OnClick(R.id.btnReject)
    public void btnReject() {
        if (UtilsTs.isObjectNotNull(orderDTO) && orderDTO.getOrderStatus() != null) {
            if (orderDTO.getOrderStatus().equals(Constants.ORDER.PENDING.getCode())) {
                showProcessing("Đang xử lý...");

                orderDTO.setOrderStatus(Constants.ORDER.REJECT.getCode());
                basePresenter.get(this, "changeOrder", "changeOrderSuccessFul", orderDTO);

                hideView();
            }
        }
    }

    @Override
    public void handleResponseError(String funcName, Constants.CODE code) {
        super.handleResponseError(funcName, code);
        switch (funcName) {
            case "changeOrder":
                closeProcess();
                showView();
                orderDTO.setOrderStatus(oldStatus);
                break;

        }
    }

    public void changeOrderSuccessFul(ServiceResponse response) {
        Constants.IS_REFRESH_ORDER_PENDING = true;
        Constants.IS_REFRESH_ORDERED = true;
        Constants.IS_REFRESH_ORDER_REJECT = true;
        closeProcess();
        Toast.makeText(OrderDetailActivityV2.this, getString(R.string.CHANGE_STATUS_ORDER_SUCCESS), Toast.LENGTH_LONG).show();
    }

    public void getOrderSuccessFul(ServiceResponse<OrderDetailDTO> response) {
        if (UtilsTs.isNotNull(response.getArrData()) && UtilsTs.isNotNull(response.getArrData().get(0).getOrderPackings())) {
            OrderDetailDTO detailDTO = response.getArrData().get(0);
//            txtdiaChiNhanHang.setText(detailDTO.getRecvAddr());

            tvWalletMoney.setText(AppController.getInstance().formatCurrency(detailDTO.walletAmount));
            tvmoneyDicount.setText(AppController.getInstance().formatCurrency(detailDTO.incentiveAmount));
            tvToTalMoney.setText(AppController.getInstance().formatCurrency(detailDTO.amount));
            if (UtilsTs.isNotNull(detailDTO.getOrderPackings())) {
                for (OrderPackingDTO dto : detailDTO.getOrderPackings()) {
                    if (dto.getIsIncentive() != null && dto.getIsIncentive() == Constants.INCENTIVE) {
                        arrIncentiveProduct.add(dto);
                    } else {
                        arrPackingProduct.add(dto);
                    }
                }
            }
            packingOrderAdapter.notifyDataSetChanged();
            if (UtilsTs.isNotNull(arrIncentiveProduct)) {
                layoutIncentive.setVisibility(View.VISIBLE);
                incentivePorductAdapter.notifyDataSetChanged();
            } else {
                layoutIncentive.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
