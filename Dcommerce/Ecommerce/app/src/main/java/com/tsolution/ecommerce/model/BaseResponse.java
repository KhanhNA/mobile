package com.tsolution.ecommerce.model;

import java.util.ArrayList;

import lombok.Data;

@Data
public class BaseResponse<T> {

    ArrayList<T> content;
}
