package com.tsolution.ecommerce.utils;

import java.util.Collections;
import java.util.List;

public class UtilsTs {
    public static List safe(List other) {

        return other == null ? Collections.EMPTY_LIST : other;
    }

    public static boolean isNotNull(List other) {
        if (other != null && other.size() > 0) {
            return true;
        }
        return false;
    }

    public static boolean isObjectNotNull(Object other) {
        if (other != null) {
            return true;
        }
        return false;
    }
}
