package com.tsolution.ecommerce.exceptionHandle;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tsolution.ecommerce.model.dto.AndroidCrashLogDto;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.view.activity.MainActivity;
import com.tsolution.ecommerce.view.application.AppController;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class ApplicationCrashHandler implements Thread.UncaughtExceptionHandler {
    /**
     * Storage for the original default crash handler.
     */
    private Thread.UncaughtExceptionHandler defaultHandler;
    static Activity activity;
    /**
     * Simple string for category log
     */
    private static final String TAG = "ApplicationCrashHandler";

    /**
     * Installs a new exception handler.
     */
    public static void installHandler(Activity a, Bundle bundle) {
        activity = a;
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof ApplicationCrashHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new ApplicationCrashHandler(bundle));
        }
    }

    private ApplicationCrashHandler(Bundle bundle) {

        this.defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    }
    private void restartApp(){
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("crash", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(AppController.getInstance().getBaseContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr = (AlarmManager) AppController.getInstance().getBaseContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);
        activity.finish();
        System.exit(2);
    }
    /**
     * Called when there is an uncaught exception elsewhere in the code.
     * @param t the thread that caused the error
     * @param e the exception that caused the error
     */
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        // Place a breakpoint here to catch application crashes
        Log.wtf(TAG, String.format("Exception: %s\n%s", e.toString(), getStackTrace(e)));

        messageBox("doStuff", e.getMessage());
        // Call the default handler
        defaultHandler.uncaughtException(t, e);
    }
    //*********************************************************
//generic dialog, takes in the method name and error message
//*********************************************************
    private void messageBox(String method, String message)
    {

        AndroidCrashLogDto dto = new AndroidCrashLogDto(null, method + ":" + message);
        BasePresenter presenter = new BasePresenter(null);
        presenter.get(this, "appCrash","",dto);



    }
    /**
     * Convert an exception into a printable stack trace.
     * @param e the exception to convert
     * @return the stack trace
     */
    private String getStackTrace(Throwable e) {
        final Writer sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);

        e.printStackTrace(pw);
        String stacktrace = sw.toString();
        pw.close();

        return stacktrace;
    }
}
