package com.tsolution.ecommerce.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputLayout;
import androidx.loader.content.CursorLoader;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.db.ModelContact;
import com.tsolution.ecommerce.model.dto.MerchantDto;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.presenter.ContactPresenter;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.FontManager;
import com.tsolution.ecommerce.utils.StringUtils;

import java.io.File;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactDetailActivity extends BaseActivity implements View.OnClickListener {
    private static final int REQUEST_READ_EXTERNAL_STORAGE = 101;
    ModelContact modelContact;
    @BindView(R.id.backdrop)
    ImageView backdrop;
    @BindView(R.id.txt_contact_poit)
    TextView txtContactPoit;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.txtRegister)
    TextView txtRegister;
    private BasePresenter contactPresenter;
    private int SELECT_IMAGE = 9999;
    private MerchantDto merchantDto;
    private StringBuilder activeCode;
    @BindView(R.id.txtNumberPhone)
    TextView txtPhone;
    @BindView(R.id.btnRegister)
    RelativeLayout btnRegister;
    @BindView(R.id.iconRegister)
    TextView iconRegister;
    @BindView(R.id.btnCall)
    TextView btnCall;
    @BindView(R.id.btnSendSms)
    TextView btnSendSms;
    @BindView(R.id.txtParentMerchantId)
    TextView txtMerchantId;
    @BindView(R.id.txtMerchantTax)
    TextInputLayout txtMerchantTax;
    @BindView(R.id.txtMerchantAddress)
    TextInputLayout txtMerchantAddress;
    @BindView(R.id.txtSms)
    TextInputLayout txtSms;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Typeface iconType;
    Typeface iconTypePro;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conttact_detail);
        ButterKnife.bind(this);
        contactPresenter = new ContactPresenter(this);
        modelContact = new ModelContact();
        modelContact.getConnectToSQL(ContactDetailActivity.this);
        intent = getIntent();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(intent.getStringExtra("name"));
        }

        //thay bằng id user
        txtMerchantId.setText(1 + "");

        //set kiểu icon cho Textview
        iconType = FontManager.getTypeface(this, FontManager.FONTAWESOME);
        iconTypePro = FontManager.getTypeface(this, FontManager.FONTAWESOME_PRO);

        if (intent.getBooleanExtra("isRegister", false)) {
            FontManager.markAsIconContainer(iconRegister, iconType);
            btnRegister.setEnabled(false);
        } else {
            FontManager.markAsIconContainer(iconRegister, iconTypePro);
        }


        FontManager.markAsIconContainer(btnCall, iconTypePro);
        FontManager.markAsIconContainer(btnSendSms, iconTypePro);


        txtPhone.setText(intent.getStringExtra("phoneNumber"));
        btnRegister.setOnClickListener(this);
        String img = intent.getStringExtra("img");
        if(!StringUtils.isNullOrEmpty(img)){
            File file = new File(img);
            Picasso.with(ContactDetailActivity.this).load(file).into(backdrop);
        }


        btnCall.setOnClickListener(this);
        btnSendSms.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:
                //btnRegister.setEnabled(false);
                activeCode = new StringBuilder();
                String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                Random rd = new Random();
                for (int i = 0; i < 6; i++) {
                    char letter = abc.charAt(rd.nextInt(abc.length()));
                    activeCode.append(letter);
                }
                merchantDto = new MerchantDto();
                //thay bằng id user khi đăng nhập
                merchantDto.setParentMarchantId((long) 1);
                merchantDto.setMobilePhone(txtPhone.getText().toString());
                merchantDto.setActiveCode(activeCode.toString());

                //validate input
                if (validateInput()) {
                    String address = txtMerchantAddress.getEditText().getText().toString();
                    String tax = txtMerchantTax.getEditText().getText().toString();
                    loading("Registering. Please wait...");
                    merchantDto.setTax(Double.parseDouble(tax));
                    merchantDto.setAddress(address);
                    contactPresenter.get(this, "registerMerchant", "handleResponseRegisterSuccessful", merchantDto);
                    intent.putExtra("result", merchantDto.getMobilePhone());
                    setResult(Activity.RESULT_OK, intent);
                }
                break;
        }
    }

    public boolean validateInput() {
        if (txtMerchantTax.getEditText().getText().toString().isEmpty()) {
            txtMerchantTax.setError("Field can't be empty");
            return false;
        } else {
            txtMerchantTax.setError(null);
        }
        if (txtMerchantAddress.getEditText().getText().toString().isEmpty()) {
            txtMerchantAddress.setError("Field can't be empty");
            return false;
        } else {
            txtMerchantAddress.setError(null);
        }
        return true;
    }

    public void handleResponseRegisterSuccessful(MerchantDto merchantDto) {
        closeProcess();
        StringBuilder sms = new StringBuilder();
        intent.putExtra("isActive", true);
        FontManager.markAsIconContainer(btnRegister, iconType);
        sms.append("your active code: " + activeCode.toString() + "\n");
        sms.append("type this code to login\n");
        sms.append("-----------------------------");
        sms.append(txtSms.getEditText().getText().toString());

        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", txtPhone.getText().toString());
        smsIntent.putExtra("sms_body", sms.toString());
        startActivity(smsIntent);
    }

    @Override
    public void handleResponseError(String serviceName, Constants.CODE code) {
        super.handleResponseError(serviceName, code);
        switch (serviceName) {
            case "registerMerchant":
                closeProcess();
                btnRegister.setEnabled(true);
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_edit:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (shouldShowRequestPermissionRationale(
                                Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        }
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_READ_EXTERNAL_STORAGE);
                    }else {
                        intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                    }
                }
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit, menu);
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                } else {
                    //If user presses deny
                    Toast.makeText(ContactDetailActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }


    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API19(Context context, Uri uri){
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = { MediaStore.Images.Media.DATA };

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{ id }, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if(cursor != null){
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Uri selectedImageUri = data.getData( );
                        Bitmap img = MediaStore.Images.Media.getBitmap(ContactDetailActivity.this.getContentResolver(), data.getData());
                        backdrop.setImageBitmap(img);
                        String picturePath;
                        int buildVertion = Build.VERSION.SDK_INT;
                        if(buildVertion < Build.VERSION_CODES.KITKAT && buildVertion > Build.VERSION_CODES.HONEYCOMB){
                            picturePath = getRealPathFromURI_API11to18(ContactDetailActivity.this, selectedImageUri);
                        }
                        else {
                            picturePath = getRealPathFromURI_API19(ContactDetailActivity.this, selectedImageUri);
                        }
                        if(!StringUtils.isNullOrEmpty(picturePath)){
                            modelContact.insertImg(txtPhone.getText().toString(), picturePath);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(ContactDetailActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
