package com.tsolution.ecommerce.view.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.utils.StringUtils;

import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountSettingActivity extends BaseActivity implements View.OnClickListener {
    BasePresenter presenter;
    @BindView(R.id.txtBirthday)
    TextView txtBirthday;
    @BindView(R.id.txtAddress)
    EditText txtAddress;
    private int SELECT_IMAGE = 9999;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imgAvatar)
    CircleImageView imgAvatar;
    @BindView(R.id.btnAvatar)
    LinearLayout btnAvatar;
    @BindView(R.id.txtUserName)
    EditText txtUserName;
    @BindView(R.id.txtFullName)
    EditText txtFullName;
    @BindView(R.id.txtPhoneNumber)
    EditText txtPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setting);
        ButterKnife.bind(this);

        presenter = new BasePresenter(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.ACCOUNT_SETTING));
        }
        btnAvatar.setOnClickListener(this);
        txtBirthday.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btnAvatar:
                intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                break;
            case R.id.txtBirthday:
                showDatePicker();
                break;
        }
    }

    private void showDatePicker() {
        DatePickerDialog.OnDateSetListener callback=new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                //Mỗi lần thay đổi ngày tháng năm thì cập nhật lại TextView Date
                txtBirthday.setText(
                        (dayOfMonth) +"/"+(monthOfYear+1)+"/"+year);
                //Lưu vết lại biến ngày hoàn thành
                //cal.set(year, monthOfYear, dayOfMonth);

            }
        };
        //các lệnh dưới này xử lý ngày giờ trong DatePickerDialog

        String s = txtBirthday.getText()+"";
        int ngay, thang, nam;
        if(!StringUtils.isNullOrEmpty(s)){

            String strArrtmp[]=s.split("/");
            ngay= Integer.parseInt(strArrtmp[0]);
            thang = Integer.parseInt(strArrtmp[1])-1;
            nam = Integer.parseInt(strArrtmp[2]);
        }
        else {
            Calendar c = Calendar.getInstance();
            nam = c.get(Calendar.YEAR);
            thang = c.get(Calendar.MONTH);
            ngay = c.get(Calendar.DAY_OF_MONTH);
        }
        DatePickerDialog pic=new DatePickerDialog(
                AccountSettingActivity.this,
                callback, nam, thang, ngay);
        pic.setTitle(getString(R.string.SELECT_BIRTHDAY));
        pic.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(AccountSettingActivity.this.getContentResolver(), data.getData());
                        imgAvatar.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(AccountSettingActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
