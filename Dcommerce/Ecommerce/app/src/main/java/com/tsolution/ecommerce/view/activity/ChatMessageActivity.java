package com.tsolution.ecommerce.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.MessageAdapter;
import com.tsolution.ecommerce.model.Contact;
import com.tsolution.ecommerce.model.Message;
import com.tsolution.ecommerce.utils.StringUtils;

import java.io.File;

import butterknife.BindView;

public class ChatMessageActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private EditText editText;
    Contact contact;

    private MessageAdapter messageAdapter;
    private ListView messagesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        setContentView(R.layout.activity_chat_message);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(i.getStringExtra("title_chat"));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ActionBar actionBar = getSupportActionBar();

        messagesView = findViewById(R.id.messages_view);
        editText = (EditText) findViewById(R.id.editText);
        editText.setText(i.getStringExtra("sms_body"));
        messageAdapter = new MessageAdapter(this);
        messagesView.setAdapter(messageAdapter);
        initToolbar();

    }

    private void initToolbar() {


        Intent i = getIntent();
        toolbar.setTitleTextColor(Color.GRAY);
        contact = (Contact) i.getSerializableExtra("contact");

//        setTitle(i.getStringExtra("name"));
        if (contact != null) {

            TextView poit = findViewById(R.id.txt_contact_poit);
            poit.setText("$ " + contact.getContact_poit());

            
            ImageView img = findViewById(R.id.backdrop);
            if(!StringUtils.isNullOrEmpty(contact.getUrlImg())){
                File file = new File(contact.getUrlImg());
                Picasso.with(ChatMessageActivity.this).load(file).into(img);
            }

        }
    }

    public void sendMessage(View view) {
        String message = editText.getText().toString();
        if (message.length() > 0) {
            messageAdapter.add(new Message(message, true));
            messageAdapter.add(new Message("Masan........", false));
            editText.getText().clear();
            messagesView.setSelection(messageAdapter.getCount() - 1);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}


