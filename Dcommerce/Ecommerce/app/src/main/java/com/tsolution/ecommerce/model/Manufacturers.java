package com.tsolution.ecommerce.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;

@Data
public class Manufacturers {
    @SerializedName("title")
    private String title;

    @SerializedName("urlImage")
    private String urlImage;

    @SerializedName("id")
    private long manufacturerId;

    @SerializedName("name")
    private String manufacturerName;

    @SerializedName("languageId ")
    private long languageId;

}
