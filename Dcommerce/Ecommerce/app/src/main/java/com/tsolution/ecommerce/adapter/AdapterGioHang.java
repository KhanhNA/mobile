package com.tsolution.ecommerce.adapter;

import android.app.*;
import android.content.*;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.*;
import android.view.*;
import android.widget.*;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.*;
import com.tsolution.ecommerce.*;
import com.tsolution.ecommerce.db.*;
import com.tsolution.ecommerce.model.*;
import com.tsolution.ecommerce.service.listerner.*;

import java.text.*;
import java.util.*;

public class AdapterGioHang extends RecyclerView.Adapter<AdapterGioHang.ViewHolderGioHang> {
    Context context;
    List<Products> sanPhamList;
    ModelGioHang modelGioHang;
    private Dialog dialog;
    private RecyclerViewClickListener mListener;


    public AdapterGioHang(Context context, List<Products> sanPhamList,RecyclerViewClickListener listener){
        this.context = context;
        mListener = listener;
        this.sanPhamList = sanPhamList;
        modelGioHang = new ModelGioHang();
        modelGioHang.MoKetNoiSQL(context);

    }

    public class ViewHolderGioHang extends RecyclerView.ViewHolder {
        //liet ke du lieu can
        TextView txtTenTieuDeGioHang,txtGiaTienGioHang,txtSoLuongSanPham,txtsoluong,txtChinhSua;
        ImageView imHinhGioHang,imXoaSanPhamGioHang;
        ImageButton imTangSoLuongSPGioHang,imGiamSoLuongSPGioHang;
        private RecyclerViewClickListener mListener;
        View view1,view2;
        public ViewHolderGioHang(View itemView,RecyclerViewClickListener listener) {
            super(itemView);
            mListener = listener;
            txtTenTieuDeGioHang = (TextView) itemView.findViewById(R.id.txtTieuDeGioHang);
            txtGiaTienGioHang = (TextView) itemView.findViewById(R.id.txtGiaGioHang);
            txtSoLuongSanPham = (TextView) itemView.findViewById(R.id.txtSoLuongSanPham);
            txtChinhSua = itemView.findViewById(R.id.txtChinhSua);
            imHinhGioHang = (ImageView) itemView.findViewById(R.id.imHinhGioHang);
            imXoaSanPhamGioHang = (ImageView) itemView.findViewById(R.id.imXoaSanPhamGioHang);
            imXoaSanPhamGioHang.setVisibility(View.GONE);
            imGiamSoLuongSPGioHang = (ImageButton) itemView.findViewById(R.id.imGiamSoLuongSPTrongGioHang);
            imTangSoLuongSPGioHang = (ImageButton) itemView.findViewById(R.id.imTangSoLuongSPTrongGioHang);
            view1 = itemView.findViewById(R.id.view1);
            view2 = itemView.findViewById(R.id.view2);
            txtsoluong = itemView.findViewById(R.id.txtsoluong);

        }
    }
    @NonNull
    @Override
    public ViewHolderGioHang onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout_giohang,parent,false);
        ViewHolderGioHang viewHolderGioHang= new ViewHolderGioHang(view,mListener);
        return viewHolderGioHang;
    }

    @Override
    public void onBindViewHolder( final ViewHolderGioHang holder, final int position) {
        Products sanPham =sanPhamList.get(position);
        holder.txtTenTieuDeGioHang.setText(sanPham.getProductName());

        NumberFormat numberFormat = new DecimalFormat("###,###");
        String gia = numberFormat.format(sanPham.getPrice()).toString();
        holder.txtGiaTienGioHang.setText(gia + " VNĐ ");

        //convert byte -> image
//        byte[] hinhsanpham = sanPham.getHinhgiohang();
//        Bitmap bitmapHinhGioHang = BitmapFactory.decodeByteArray(hinhsanpham,0,hinhsanpham.length);
//        holder.imHinhGioHang.setImageBitmap(bitmapHinhGioHang);
        if(sanPham.getUrl() != null) {
            Picasso.with(context).load(sanPham.getUrl()).into(holder.imHinhGioHang);
        }

        holder.imXoaSanPhamGioHang.setTag(sanPham.getProductId());

        holder.imXoaSanPhamGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModelGioHang modelGioHang = new ModelGioHang();
                modelGioHang.MoKetNoiSQL(context);
                modelGioHang.XoaSanPhamTrongGioHang((long)v.getTag());
                sanPhamList.remove(position);
                notifyDataSetChanged();
                mListener.onClick(v, sanPham);
            }
        });

        holder.txtSoLuongSanPham.setText(String.valueOf(sanPham.getQuantity()));
        holder.imTangSoLuongSPGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double soluong = Double.parseDouble(holder.txtSoLuongSanPham.getText().toString());
                soluong++;
                holder.txtSoLuongSanPham.setText(String.valueOf(soluong) );
                modelGioHang.CapNhatSoLuongSanPhamGioHang(sanPham.getProductId(),soluong);

            }
        });
        holder.imGiamSoLuongSPGioHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double soluong = Double.parseDouble(holder.txtSoLuongSanPham.getText().toString());
                if(soluong > 1){
                    soluong--;
                }
                modelGioHang.CapNhatSoLuongSanPhamGioHang(sanPham.getProductId(),soluong);
                holder.txtSoLuongSanPham.setText(String.valueOf(soluong));
            }
        });

        holder.txtChinhSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (holder.txtChinhSua.getText().toString().equals("Hoàn Thành"))
                {
                    holder.txtChinhSua.setText("Sửa");
                    holder.view1.setVisibility(View.GONE);
                    holder.view2.setVisibility(View.GONE);
                    holder.txtsoluong.setVisibility(View.VISIBLE);
                    holder.imTangSoLuongSPGioHang.setVisibility(View.GONE);
                    holder.imGiamSoLuongSPGioHang.setVisibility(View.GONE);
                    holder.imXoaSanPhamGioHang.setVisibility(View.GONE);

                    mListener.onClick(v, sanPham);
                    //add product to cart
                }else if (holder.txtChinhSua.getText().toString().equals("Sửa")){
                    holder.txtChinhSua.setText("Hoàn Thành");
                    holder.view1.setVisibility(View.VISIBLE);
                    holder.view2.setVisibility(View.VISIBLE);
                    holder.txtsoluong.setVisibility(View.GONE);
                    holder.imTangSoLuongSPGioHang.setVisibility(View.VISIBLE);
                    holder.imGiamSoLuongSPGioHang.setVisibility(View.VISIBLE);
                    holder.imXoaSanPhamGioHang.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    public void showDialog() {
        dialog = new Dialog(context);
        dialog.setTitle("Thông tin chi tiết");
        dialog.setContentView(R.layout.dialog_chuong_trinh_khuyen_mai);
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return sanPhamList.size();
    }


}
