package com.tsolution.ecommerce.model;

import android.graphics.Bitmap;

import java.io.Serializable;

import lombok.Data;

@Data
public class Contact implements Serializable {
    private String urlImg;
    private String contact_name;
    private int contact_poit;
    private String contact_phone;
    private String contact_email;
    private int status;
    private boolean isRegister;

}
