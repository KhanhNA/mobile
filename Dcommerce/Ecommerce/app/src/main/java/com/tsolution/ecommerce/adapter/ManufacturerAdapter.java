package com.tsolution.ecommerce.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Manufacturers;
import java.util.ArrayList;

public class ManufacturerAdapter extends RecyclerView.Adapter<ManufacturerAdapter.MyViewHolder>  {
    private ArrayList<Manufacturers> arrData;
    private Context mContext;

    public ManufacturerAdapter(Context mContext, ArrayList<Manufacturers> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }

    public void updateManufacturer(ArrayList<Manufacturers> items) {
        arrData = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_brand, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ManufacturerAdapter.MyViewHolder holder, int i) {
        if(arrData!=null && arrData.size()>0)
        {
            Manufacturers movie = arrData.get(i);
            if(movie.getUrlImage() != null && !"".equals(movie.getUrlImage())) {
                Picasso.with(mContext).load(movie.getUrlImage()).into(holder.imgManufacturers);
            }
            if(movie.getManufacturerName() != null && !"".equals(movie.getManufacturerName())) {
                holder.txtNameManufacturers.setText(movie.getManufacturerName());
            }
//            if(movie.getTitle() != null && !"".equals(movie.getTitle())){
//                holder.txtTitle.setText(movie.getTitle());
//            }

        }

    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNameManufacturers, txtTitle;
        public ImageView imgManufacturers;
        public MyViewHolder(View view) {
            super(view);
            imgManufacturers = view.findViewById(R.id.imgManufacturers);
            txtNameManufacturers =  view.findViewById(R.id.txtNameManufacturers);
           //  txtTitle = view.findViewById(R.id.txtTitle);

        }
    }
}
