package com.tsolution.ecommerce.view.activity;

import android.app.*;
import android.content.*;
import android.os.*;

import androidx.annotation.Nullable;
import androidx.appcompat.app.*;
import android.view.*;
import android.widget.*;

import com.tsolution.ecommerce.*;
import com.tsolution.ecommerce.service.listerner.*;
import com.tsolution.ecommerce.utils.*;

public class PhuongThucThanhToanActivity extends AppCompatActivity implements View.OnClickListener {
    CheckedTextView ctvWallet;
    CheckedTextView ctvCard;
    CheckedTextView ctvDirectPayment;
    Button btnAgree;


    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_method_payment);
        initView();
    }

    private void initView() {
        ctvWallet = findViewById(R.id.ctvWallet);
        ctvWallet.setOnClickListener(this);
        ctvCard = findViewById(R.id.ctvCard);
        ctvCard.setOnClickListener(this);
        ctvDirectPayment = findViewById(R.id.ctvDirectPayment);
        ctvDirectPayment.setOnClickListener(this);
        btnAgree = findViewById(R.id.btnAgree);
        btnAgree.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.ctvWallet:
                if (ctvWallet.isChecked()){
                    ctvWallet.setCheckMarkDrawable(null);
                    ctvWallet.setChecked(false);
                }else {
                    ctvWallet.setCheckMarkDrawable(R.drawable.icon_check);
                    ctvWallet.setChecked(true);
                    ctvDirectPayment.setCheckMarkDrawable(null);
                    ctvDirectPayment.setChecked(false);
                    ctvCard.setCheckMarkDrawable(null);
                    ctvCard.setChecked(false);
                }
                break;
            case R.id.ctvCard:
                if (ctvCard.isChecked()){
                    ctvCard.setCheckMarkDrawable(null);
                    ctvCard.setChecked(false);

                }else {
                    ctvCard.setCheckMarkDrawable(R.drawable.icon_check);
                    ctvCard.setChecked(true);
                    ctvWallet.setCheckMarkDrawable(null);
                    ctvWallet.setChecked(false);
                    ctvDirectPayment.setCheckMarkDrawable(null);
                    ctvDirectPayment.setChecked(false);
                }
                break;
            case R.id.ctvDirectPayment:
                if (ctvDirectPayment.isChecked()){
                    ctvDirectPayment.setCheckMarkDrawable(null);
                    ctvDirectPayment.setChecked(false);
                }else {
                    ctvDirectPayment.setCheckMarkDrawable(R.drawable.icon_check);
                    ctvDirectPayment.setChecked(true);
                    ctvWallet.setCheckMarkDrawable(null);
                    ctvWallet.setChecked(false);
                    ctvCard.setCheckMarkDrawable(null);
                    ctvCard.setChecked(false);
                }
                break;
            case R.id.btnAgree:
                int methodPayment= 0;
                if (ctvWallet.isChecked()){
                    methodPayment = Constants.METHOD_WALLET;
                }else if (ctvCard.isChecked()){
                    methodPayment = Constants.METHOD_CARD;
                }else if (ctvDirectPayment.isChecked()){
                    methodPayment = Constants.METHOD_DIRECTORY;
                }
                if (methodPayment != 0){
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(IntentConstans.INTENT_METHOD_PAYMENT,methodPayment );
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }

        }
    }
}
