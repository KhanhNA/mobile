package com.tsolution.ecommerce.service.listerner;

import android.view.*;

public interface RecyclerViewClickListener {
    void onClick(View view, Object object);
}
