package com.tsolution.ecommerce.service.listerner;

import com.tsolution.ecommerce.model.dto.*;
import com.tsolution.ecommerce.model.response.*;
import com.tsolution.ecommerce.model.Category;
import com.tsolution.ecommerce.model.Manufacturers;
import com.tsolution.ecommerce.model.ProductDetail;
import com.tsolution.ecommerce.model.Products;
import com.tsolution.ecommerce.model.TokenInfo;
import com.tsolution.ecommerce.model.dto.AndroidCrashLogDto;
import com.tsolution.ecommerce.model.dto.MerchantDto;
import com.tsolution.ecommerce.model.dto.OrderDTO;
import com.tsolution.ecommerce.model.dto.OrderDetailDTO;
import com.tsolution.ecommerce.model.response.ServiceResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SOService {
    @Headers({"Content-Type: application/json"})
    @POST("private/login")
    Call<TokenInfo> requestLogin(@Body HashMap<String, String> user);

    @GET("products")
    Call<ServiceResponse<ProductDto>> getProducts(@Query("page") Integer page, @Query("langId") Integer langId);

    @GET("products/{id}")
    Call<ProductDetail> getProductById(@Path("id") Long id, @Query("langId") Integer langId);

    @GET("categories")
    Call<ServiceResponse<Category>> getCategories();

    @GET("manufacturers")
    Call<ServiceResponse<Manufacturers>> getManufacturers();

    @POST("orders")
    Call<ServiceResponse<OrderDTO>> getOrders(@Body OrderDTO order);

    @POST("order/cals")
    Call<OrderProductCartDTO> getListProductCartOrder(@Body OrderProductCartDTO order);

    @POST("orders/detail")
    Call<ServiceResponse<OrderDetailDTO>> getOrderDetails(@Body OrderDetailDTO order);

    @POST("orders/changeOrder")
    Call<ServiceResponse<OrderDetailDTO>> changeOrder(@Body OrderDetailDTO order);

    @POST("merchants/register")
    Call<MerchantDto> registerMerchant(@Body MerchantDto merchant);

    @POST("merchants/deactive")
    Call<MerchantDto> deActiveMerchant(@Body MerchantDto merchantDto);

    @POST("merchants")
    Call<ServiceResponse<MerchantDto>> getMerchants(@Body MerchantDto merchantDto, @Query("page") Integer pageable, @Query("isPage") Integer isPage);

    @POST("crash")
    Call<String> appCrash(@Body AndroidCrashLogDto baseDto);

    @POST("common/insert")
    Call<String> insert(@Body AndroidCrashLogDto baseDto, @Query("name")String name);
    @POST("common/insert")
    Call<String> insert(@Body MerchantDto baseDto, @Query("name")String name);

    @POST("common/list")
    Call<ServiceResponse<WalletDto>> commonList(@Body WalletDto object,@Query("entityName") String entityName , @Query("page") Integer page, @Query("langId") Integer langId);

    @POST("common/list")
    Call<ServiceResponse<WalletHisDto>> getWalletHistories(@Body WalletHisDto object,@Query("entityName") String entityName , @Query("page") Integer page, @Query("langId") Integer langId);
    @POST("loadConfig")
    Call<HashMap<String,Object>> loadConfig();
}
