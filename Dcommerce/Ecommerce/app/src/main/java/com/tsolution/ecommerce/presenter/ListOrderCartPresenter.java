package com.tsolution.ecommerce.presenter;

import android.util.*;

import com.google.gson.*;
import com.tsolution.ecommerce.model.dto.*;
import com.tsolution.ecommerce.model.response.*;
import com.tsolution.ecommerce.service.*;
import com.tsolution.ecommerce.service.listerner.*;
import com.tsolution.ecommerce.utils.*;
import com.tsolution.ecommerce.view.application.*;

import retrofit2.*;

public class ListOrderCartPresenter {

    private BaseView baseView;

    public ListOrderCartPresenter(BaseView base) {
        this.baseView = base;
    }


    public void getListOrdersProductCart(OrderProductCartDTO orderDTO){
        Gson gson = new Gson();
        String tt =gson.toJson(orderDTO);
        AppController.getInstance().getSOService().getListProductCartOrder(orderDTO).enqueue(new Callback<OrderProductCartDTO>() {
            @Override
            public void onResponse(Call<OrderProductCartDTO> call, Response<OrderProductCartDTO> response) {
                if(response.isSuccessful()){
                    baseView.handleResponseSuccessful(Constants.GET_DATA_LIST_PRODUCT_ORDER_CART, new ResponseInfo<>(response.body(), Constants.CODE.SUCCESS));
                }else {
                    Log.e("Response-ListOrder", "load fail" + response.message());
                    baseView.handleResponseError(Constants.GET_DATA_LIST_PRODUCT_ORDER_CART,Constants.CODE.ERROR_INTERNAL);
                }
            }

            @Override
            public void onFailure(Call<OrderProductCartDTO> call, Throwable t) {
                Log.e("Failure-ListOrder", "404:" + t);
                baseView.handleResponseError(Constants.GET_DATA_LIST_PRODUCT_ORDER_CART,Constants.CODE.ERROR_INTERNAL);
            }
        });

    }

}
