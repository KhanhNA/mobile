package com.tsolution.ecommerce.db;

import android.content.*;
import android.database.*;
import android.database.sqlite.*;
import android.util.*;

import com.tsolution.ecommerce.model.*;
import com.tsolution.ecommerce.model.dto.*;

import java.util.*;

public class ModelGioHang {
    SQLiteDatabase database;

    public void MoKetNoiSQL(Context context){
        DataBaseSqlLite dataSanPham = new DataBaseSqlLite(context);
        database = dataSanPham.getWritableDatabase();
    }

    public boolean XoaSanPhamTrongGioHang(long masp){

        int kiemtra = database.delete(DataBaseSqlLite.TB_GIOHANG, DataBaseSqlLite.TB_GIOHANG_MASP + " = " + masp,null);
        if(kiemtra > 0){
            return true;
        }else{
            return false;
        }
    }

    public boolean CapNhatSoLuongSanPhamGioHang(long masp,double soluong){

        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseSqlLite.TB_GIOHANG_SOLUONG,soluong);

        int id = database.update(DataBaseSqlLite.TB_GIOHANG,contentValues, DataBaseSqlLite.TB_GIOHANG_MASP + " = " + masp,null);
        if(id > 0){
            return true;
        }else{
            return false;
        }
    }

    public boolean ThemGioHang(Products sanPham){

        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseSqlLite.TB_GIOHANG_MASP,sanPham.getProductId());
        contentValues.put(DataBaseSqlLite.TB_GIOHANG_TENSP,sanPham.getProductName());
        contentValues.put(DataBaseSqlLite.TB_GIOHANG_GIATIEN,sanPham.getPrice());
        contentValues.put(DataBaseSqlLite.TB_GIOHANG_HINHANH,sanPham.getUrl());
        contentValues.put(DataBaseSqlLite.TB_GIOHANG_SOLUONG,sanPham.getQuantity());
        contentValues.put(DataBaseSqlLite.TB_GIOHANG_SOLUONG_SANPHAM,sanPham.getQuantityOfProduct());
//        contentValues.put(DataBaseSqlLite.TB_GIOHANG_SOLUONGTONKHO,sanPham.getSOLUONGTONKHO());

        long id = database.insert(DataBaseSqlLite.TB_GIOHANG,null,contentValues);
        if(id > 0){
            return true;
        }else{
            return false;
        }

    }

    public List<Products> LayDanhSachSanPhamTrongGioHang(){
        List<Products> sanPhamList = new ArrayList<>();

        String truyvan = "SELECT * FROM " + DataBaseSqlLite.TB_GIOHANG;
        Cursor cursor = database.rawQuery(truyvan,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Products products = Products.builder().build();
            products.initFromCursor(cursor);
            sanPhamList.add(products);
            cursor.moveToNext();
        }

        return sanPhamList;
    }

    public double  getQuantiTyProduct(ProductDto dto){
        double quantity =0;
        ArrayList<String> params = new ArrayList<String>();
        String abc;
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM CART " );
        sql.append(" where PRODUCT_ID = ? " );
        params.add(dto.getId() +"");
        sql.append(" AND QUANTITY_OF_PRODUCT = ?");//vi product get theo position nen quantity of product = 0
        params.add(String.valueOf(dto.getQuantity()));
        sql.append(" AND PRICE = ?");
        params.add(String.valueOf(dto.getPrice()));

        Cursor cursor = null;
        try {
            cursor = database.rawQuery(sql.toString(),params.toArray(new String[params.size()]));
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                quantity = cursor.getDouble(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_SOLUONG));
                cursor.moveToNext();
            }
        }catch (Exception ex){
            Log.e("",""+ ex);
            throw ex;
        }
        return quantity;
    }
}
