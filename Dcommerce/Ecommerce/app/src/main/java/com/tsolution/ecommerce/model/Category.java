package com.tsolution.ecommerce.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Category  {
    @SerializedName("name")
    private String title;
    @SerializedName("urlImage")
    private String urlImage;
    @SerializedName("categoryId")
    private long categoryId;
    @SerializedName("descriptionId ")
    private long descriptionId;
    @SerializedName("languageId ")
    private long languageId;
}
