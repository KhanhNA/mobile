package com.tsolution.ecommerce.presenter;

import com.tsolution.ecommerce.model.dto.OrderDTO;
import com.tsolution.ecommerce.model.dto.OrderDetailDTO;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.utils.Constants;

public class OrderPresenter extends BasePresenter {
    private BaseView baseView;

    public OrderPresenter(BaseView base) {
        super(base);
        this.baseView = base;

    }

    public void getOrders(OrderDTO orderDTO) {
        get(this, "getOrders", "handleResponseOrdersSuccessful", orderDTO);
    }

    public void getOrdersDetail(OrderDetailDTO orderDTO) {
        get(this, "getOrderDetails", "handleResponseOrderDetailSuccessful", orderDTO);
    }

    public void changeOrder(OrderDetailDTO orderDTO) {

        try {
            get(this, "changeOrder", "handleResponseSuccessfulChangeOrder", orderDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleResponseOrdersSuccessful(ServiceResponse response) {
        baseView.handleResponseSuccessful(Constants.GET_DATA_LIST_ORDER, new ResponseInfo<>(response, Constants.CODE.SUCCESS));
    }

    public void handleResponseOrderDetailSuccessful(ServiceResponse response) {
        baseView.handleResponseSuccessful(Constants.GET_DATE_ORDER_DETAIL, new ResponseInfo<>(response, Constants.CODE.SUCCESS));
    }

    public void handleResponseSuccessfulChangeOrder(ServiceResponse response) {
        baseView.handleResponseSuccessful(Constants.GET_DATE_ORDER_DETAIL, new ResponseInfo<>(response, Constants.CODE.SUCCESS));
    }

}
