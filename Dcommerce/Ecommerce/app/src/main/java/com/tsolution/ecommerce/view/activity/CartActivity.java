package com.tsolution.ecommerce.view.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.AdapterGioHang;
import com.tsolution.ecommerce.db.GioHang.PresenterLogicGioHang;
import com.tsolution.ecommerce.db.GioHang.ViewGioHang;
import com.tsolution.ecommerce.model.Products;
import com.tsolution.ecommerce.model.dto.OrderPackingDTO;
import com.tsolution.ecommerce.model.dto.OrderProductCartDTO;
import com.tsolution.ecommerce.presenter.ListOrderCartPresenter;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.IntentConstans;
import com.tsolution.ecommerce.service.listerner.RecyclerViewClickListener;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.StringUtils;
import com.tsolution.ecommerce.view.application.AppController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartActivity extends BaseActivity implements ViewGioHang, View.OnClickListener {

    ListOrderCartPresenter listOrderCartPresenter;

    OrderProductCartDTO orderedResponse;

    PresenterLogicGioHang presenterLogicGioHang;

    List<Products> sanPhamList = new ArrayList<Products>();

    BottomSheetBehavior sheetBehavior;

    @BindView(R.id.lnPromotionApllies)
    LinearLayout lnPromotionApllies;
    @BindView(R.id.lnPromotionProduct)
    LinearLayout lnPromotionProduct;
    @BindView(R.id.lnMoneyToWallet)
    LinearLayout lnMoneyToWallet;
    @BindView(R.id.lnDisCount)
    LinearLayout lnDisCount;
    @BindView(R.id.toolbar1)
    Toolbar toolbar;
    @BindView(R.id.recyclerGioHang)
    RecyclerView recyclerGioHang;
    @BindView(R.id.txtKhuyenMaiApDung)
    TextView txtKhuyenMaiApDung;
    @BindView(R.id.txtChuongTrinhKhuyenMai)
    TextView txtChuongTrinhKhuyenMai;
    @BindView(R.id.txtKMSanPham)
    TextView txtKMSanPham;
    @BindView(R.id.txtSanPhamKM)
    TextView txtSanPhamKM;
    @BindView(R.id.txtTienVaoVi)
    TextView txtTienVaoVi;
    @BindView(R.id.txtChietKhau)
    TextView txtChietKhau;
    @BindView(R.id.txtTongTien)
    TextView txtTongTien;
    @BindView(R.id.btnThanhToan)
    Button btnThanhToan;
    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_giohang);
        ButterKnife.bind(this);
        //DU
        listOrderCartPresenter = new ListOrderCartPresenter(this);
        init();
        getListProductFromCart();

    }

    private void getListProductFromCart() {
        presenterLogicGioHang = new PresenterLogicGioHang(this);
        presenterLogicGioHang.LayDanhSachSanPhamTrongGioHang(this);
    }

    private void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setElevation(4);
            getSupportActionBar().setTitle(getResources().getString(R.string.menu_cart));
        }
        btnThanhToan = findViewById(R.id.btnThanhToan);
        btnThanhToan.setOnClickListener(this);

        txtChuongTrinhKhuyenMai = findViewById(R.id.txtChuongTrinhKhuyenMai);
        layoutBottomSheet = findViewById(R.id.bottom_sheet);
        layoutBottomSheet.setOnClickListener(this);
        setBottomSheet();


    }

    private void setBottomSheet() {
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        sheetBehavior.setHideable(false);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        lnDisCount.setVisibility(View.VISIBLE);
                        lnMoneyToWallet.setVisibility(View.VISIBLE);
                        lnPromotionApllies.setVisibility(View.VISIBLE);
                        lnPromotionProduct.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        lnDisCount.setVisibility(View.GONE);
                        lnMoneyToWallet.setVisibility(View.GONE);
                        lnPromotionApllies.setVisibility(View.GONE);
                        lnPromotionProduct.setVisibility(View.GONE);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull final View view, final float v) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void HienThiDanhSachSanPhamTrongGioHang(List<Products> sanPhamList) {
        this.sanPhamList = sanPhamList;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(final View view, final Object object) {
                getListProductFromCart();
            }
        };
        AdapterGioHang adapterGioHang = new AdapterGioHang(this, sanPhamList,listener);
        recyclerGioHang.setLayoutManager(layoutManager);
        recyclerGioHang.setAdapter(adapterGioHang);
        postListProductCart(sanPhamList);
    }

    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {
        super.handleResponseError(actionSender, code);
        switch (actionSender) {
            case Constants.GET_DATA_LIST_PRODUCT_ORDER_CART:
                Toast.makeText(this, "Faillllllllllllllllllllllllll", Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void postListProductCart(List<Products> sanPhamList) {
        if (sanPhamList.size() > 0) {
            OrderProductCartDTO orderDTO = new OrderProductCartDTO();
            orderDTO.setMerchantId(1L);
            orderDTO.setIsSaveOrder(0);
            for (int i = 0; i < sanPhamList.size(); i++) {
                OrderPackingDTO packingDTO = new OrderPackingDTO();
                packingDTO.setQuantity(sanPhamList.get(i).getQuantity());
                packingDTO.setPackingProductId(sanPhamList.get(i).getProductId());
                orderDTO.addPackingProduct(packingDTO);
            }
            listOrderCartPresenter.getListOrdersProductCart(orderDTO);
        }
    }

    @Override
    public void handleResponseSuccessful(final int actionSender, final ResponseInfo response) {
        super.handleResponseSuccessful(actionSender, response);
        switch (actionSender) {
            case Constants.GET_DATA_LIST_PRODUCT_ORDER_CART:
                if (response.responseData != null) {
                    orderedResponse = (OrderProductCartDTO) response.responseData;
//                    Toast.makeText(this,"Successssssssssssss",Toast.LENGTH_LONG).show();
                    setOrder(orderedResponse);

                }
                break;
        }
    }

    private void setOrder(OrderProductCartDTO orderedResponse) {
        if (orderedResponse.getAmount() > 0.0) {
            txtTongTien.setText(AppController.getInstance().formatCurrency(orderedResponse.getAmount()));
        }
        if (orderedResponse.getWalletAmount() > 0.0) {
            txtTienVaoVi.setText(AppController.getInstance().formatCurrency(orderedResponse.getWalletAmount()));
        } else
            txtTienVaoVi.setText("0đ");

        if (orderedResponse.getIncentiveAmount() > 0.0) {
            txtChietKhau.setText(AppController.getInstance().formatCurrency(orderedResponse.getIncentiveAmount()));
        } else
            txtChietKhau.setText("0đ");

        StringBuilder builder = new StringBuilder();
        boolean isFirst = true;
        boolean isbuilder = false;
        for (int i = 0; i < orderedResponse.orderPackings.size(); i++) {
            if (orderedResponse.orderPackings.get(i).getIsIncentive() != 1) {
                txtChuongTrinhKhuyenMai.setText(R.string.NO);

            } else {
                if (isFirst) {
                    isFirst = false;
                    builder.append("CTTB");
                } else {
                    builder.append(",CTTB");
                }
            }
        }
        if (StringUtils.isNullOrEmpty(builder.toString())) {
            txtChuongTrinhKhuyenMai.setText(R.string.NO);
            txtSanPhamKM.setText(R.string.NO);
        } else {
            txtChuongTrinhKhuyenMai.setText(builder);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_list_order,menu);
        return true;
//        getMenuInflater().inflate(R.menu.edit, menu);
//        MenuItem iGioHang =menu.findItem(R.id.actin_edit);
//        View giaoDienCustomGioHang = MenuItemCompat.getActionView(iGioHang);
//        txtEdit =(TextView) giaoDienCustomGioHang.findViewById(R.id.txtEdit);
//        txtEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View v) {
//                if (txtEdit.getText().toString().equals(R.string.COMPLETE))
//                {
//                    txtEdit.setText(R.string.EDIT);
//                }else if (txtEdit.getText().toString().equals(R.string.EDIT)){
//                    txtEdit.setText(R.string.COMPLETE);
//                }
//            }
//        });

    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()){
            case R.id.home:
                onBackPressed();
                break;
            case R.id.action_chat:
                Intent intent = new Intent(CartActivity.this, ChatActivity.class);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btnThanhToan:
                if (sanPhamList.size() > 0) {
                    Intent intent = new Intent(CartActivity.this, ThanhToanActivity.class);
                    intent.putExtra(IntentConstans.INTENT_LIST_PRODUCT, (Serializable) sanPhamList);
                    intent.putExtra(IntentConstans.INTENT_MONEY_TO_WALLET, orderedResponse.getWalletAmount());
                    intent.putExtra(IntentConstans.INTENT_TOTAL_MONEY, orderedResponse.getAmount());
                    intent.putExtra(IntentConstans.INTENT_MONEY_DISCOUNT, orderedResponse.getIncentiveAmount());
                    startActivity(intent);
                } else
                    Toast.makeText(this, "Bạn chưa thêm sản phẩm vào giỏ hàng", Toast.LENGTH_LONG).show();
                break;
            case R.id.bottom_sheet:
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                break;
        }
    }

    //    public void showBottomSheetDialog() {
//        View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_dialog_khuyen_mai, null);
//        btnThanhToan = view.findViewById(R.id.btnThanhToan);
//        btnThanhToan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View v) {
//                Intent intent = new Intent(CartActivity.this, ThanhToanActivity.class);
//                startActivity(intent);
//            }
//        });
//        BottomSheetDialog dialog = new BottomSheetDialog(this);
//        dialog.setContentView(view);
//        dialog.show();
//    }
}
