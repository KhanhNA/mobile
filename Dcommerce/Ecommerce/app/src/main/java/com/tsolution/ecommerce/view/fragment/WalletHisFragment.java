package com.tsolution.ecommerce.view.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.WalletHisAdapterV2;
import com.tsolution.ecommerce.model.dto.WalletHisDto;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.view.activity.WalletActivity;
import com.tsolution.ecommerce.widget.ILoadMore;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class WalletHisFragment extends BaseFragment {
    BasePresenter presenter;
    private View rootView;
    WalletHisAdapterV2 adapter;
    ArrayList<WalletHisDto> walletHisDtos;
    private static WalletHisFragment mInstance;
    @BindView(R.id.rcWalletHis)
    RecyclerView rcWalletHis;
    boolean isLoad;
    Unbinder unbinder;
    int visibleThreshold=1;
    int lastVisibleItem,totalItemCount;
    int currentPage = 0, totalPage;

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public WalletHisFragment getInstance() {
        if (mInstance == null) {
            mInstance = new WalletHisFragment();
        }
        return mInstance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_his_wallet, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        presenter = new BasePresenter(this);
        walletHisDtos = new ArrayList<>();
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcWalletHis.setLayoutManager(linearLayoutManager);
        adapter = new WalletHisAdapterV2(rcWalletHis,getActivity(), walletHisDtos);
        rcWalletHis.setAdapter(adapter);
//        rcWalletHis.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                totalItemCount = linearLayoutManager.getItemCount(); // Lấy tổng số lượng item đang có
//                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition(); // Lấy vị trí của item cuối cùng
//                if(totalItemCount <= (lastVisibleItem+visibleThreshold)) // Nếu không phải trạng thái loading và tổng số lượng item bé hơn hoặc bằng vị trí item cuối + số lượng item tối đa hiển thị
//                {
//                     if(currentPage < totalPage && !isLoad) {
//                        WalletHisDto walletHisDto = new WalletHisDto();
//                        WalletActivity mActivity = (WalletActivity) getActivity();
//                        Long walletId = mActivity.walletId;
//                        walletHisDto.walletId = walletId;
//                        walletHisDto.ORDER_BY = "createDate asc";
//                        getWalletHisList(walletHisDto, ++currentPage);
//                        isLoad = true;
//                    }
//                }
//
//            }
//        });
        //Set Load more event
        adapter.setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                if(currentPage < totalPage && !isLoad) {
                    walletHisDtos.add(null);
                    adapter.notifyItemInserted(walletHisDtos.size() - 1);
                    WalletHisDto walletHisDto = new WalletHisDto();
                    WalletActivity mActivity = (WalletActivity) getActivity();
                    Long walletId = mActivity.walletId;
                    walletHisDto.walletId = walletId;
                    walletHisDto.ORDER_BY = "createDate desc";
                    getWalletHisList(walletHisDto, ++currentPage);
                    isLoad = true;
                }
            }
        });
        return rootView;
    }

    public void getWalletHisList(WalletHisDto walletHisDto, int currentPage){

        presenter.get(this, "getWalletHistories", "getWalletHistoriesSuccessful", walletHisDto,"WalletHis", currentPage, Constants.LANG_VI);

    }

    public void getWalletHistoriesSuccessful(ServiceResponse<WalletHisDto> listWalletHis){

        if(listWalletHis != null && listWalletHis.getArrData() != null){
            if(walletHisDtos != null && walletHisDtos.size() > 0){
                walletHisDtos.remove(walletHisDtos.size()-1);
            }
            if(isLoad){
                walletHisDtos.addAll(listWalletHis.getArrData());
            }
            else {
                walletHisDtos = listWalletHis.getArrData();
            }
            adapter.update(walletHisDtos);
            totalPage = listWalletHis.getTotalPages();
            isLoad = false;
           //closeProcess();
            adapter.notifyDataSetChanged();
            adapter.setLoaded();

        }
    }

    @Override
    public void handleResponseError(String serviceName, Constants.CODE code) {
        super.handleResponseError(serviceName, code);
        switch (serviceName){
            case "getWalletHistories":
                isLoad = false;
                //closeProcess();
                adapter.setLoaded();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
