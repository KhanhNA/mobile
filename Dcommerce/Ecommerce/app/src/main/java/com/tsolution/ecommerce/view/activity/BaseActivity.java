package com.tsolution.ecommerce.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.tsolution.ecommerce.exceptionHandle.ApplicationCrashHandler;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.dto.AndroidCrashLogDto;
import com.tsolution.ecommerce.model.dto.MerchantDto;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.TsLog;
import com.tsolution.ecommerce.view.application.AppController;

public class BaseActivity extends AppCompatActivity implements BaseView {
    ProgressDialog pd;
    private AlertDialog alertDialog;

    public BaseActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ApplicationCrashHandler.installHandler(this, savedInstanceState);
//        Thread thread = Thread.currentThread();
//        thread.setUncaughtExceptionHandler((v, exc) ->
//        {
//            handleError(this,this.getLocalClassName(),Constants.CODE.FATAL_ERROR,exc);
//        });
    }

    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {

    }

    @Override
    public void handleResponseError(String funcName, Constants.CODE code) {

    }

    @Override
    public void handleResponseSuccessful(int actionSender, ResponseInfo response) {

    }

    @Override
    public void loading(String msg) {
        showProcessing(msg);
    }

    public void showProcessing(String msg) {
        if (pd == null) {
            pd = new ProgressDialog(this);
        }
        if (!pd.isShowing()) {
            pd.setCancelable(false);
            pd.setMessage(msg);
            pd.show();
        }

    }

    @Override
    public void showAlertDialog(String msg) {
        try {
            if (alertDialog == null) {
                alertDialog = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Dialog_Alert).create();
            }
            if (!alertDialog.isShowing()) {
                alertDialog.setMessage(msg);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.CLOSE),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if (!isFinishing()) {
                    alertDialog.show();
                }

            }
        } catch (Exception e) {
            TsLog.e("Error Alert dialog: ", "", e);
        }
    }

    public static void handleError(Object activity, String serviceName, Constants.CODE code, final Throwable ex) {
        ex.printStackTrace();

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder((Context) activity);
        builder.setTitle("Error");
        MerchantDto merchantDto = (MerchantDto) AppController.getInstance().getFromCatche(AppController.MERCHANT);
        Long merchantId = null;
        if (merchantDto != null) {
            merchantId = merchantDto.getMerchantId();
        }
        if (code == Constants.CODE.FATAL_ERROR) {

            AndroidCrashLogDto dto = new AndroidCrashLogDto(merchantId, serviceName + ":" + TsLog.printStackTrace(ex));
            BasePresenter presenter = new BasePresenter(null);
            presenter.get(activity, "appCrash", "", dto);
            return;
        }
        if (code == Constants.CODE.USER_DEFIEND) {
            builder.setMessage(ex.getMessage());
        } else {
            builder.setMessage(TsLog.printStackTrace(ex));
            // add the buttons
            builder.setPositiveButton("Send Error", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    AndroidCrashLogDto dto = new AndroidCrashLogDto(merchantDto.getMerchantId(), TsLog.printStackTrace(ex));

                    new BasePresenter(null).get(null, "appCrash", "", dto);
                }
            });
        }


        builder.setNegativeButton("OK", null);
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void closeAlertDialog() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    public void closeProcess() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }

}
