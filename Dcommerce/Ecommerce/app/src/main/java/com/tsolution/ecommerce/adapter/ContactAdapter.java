package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.Contact;
import com.tsolution.ecommerce.utils.FontManager;
import com.tsolution.ecommerce.utils.StringUtils;

import java.io.File;
import java.util.List;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyContactListViewHolder> {

    private List<Contact> contactList;
    private Context context;
    private CheckBox chkContact, chkCheckAll;
    private boolean isShow = false;
    private boolean isCheckedAll = true;
    private ItemClickListener itemClickListener;


    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public ContactAdapter(Context context, List<Contact> mainInfo) {
        this.contactList = mainInfo;
        this.context = context;
    }
    public void update(List<Contact> contactList){
        this.contactList = contactList;
        notifyDataSetChanged();
    }

    public boolean isCheckedAll() {
        return isCheckedAll;
    }

    public void setCheckedAll(boolean isCheckedAll) {
        this.isCheckedAll = isCheckedAll;

    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public class MyContactListViewHolder extends RecyclerView.ViewHolder  {

        ImageView imgContact;
        TextView txt_contact_name;
        TextView txtContactPhone;
        TextView txt_contact_poit;
        TextView labelRegister;
        CheckBox checkBox;
        public MyContactListViewHolder(View itemView) {
            super(itemView);
            this.setIsRecyclable(false);

            imgContact = itemView.findViewById(R.id.contact_img);
            txt_contact_name = itemView.findViewById(R.id.txt_contact_name);
            checkBox = itemView.findViewById(R.id.chkContact);
            txtContactPhone =  itemView.findViewById(R.id.txt_contact_phone);
            txt_contact_poit =  itemView.findViewById(R.id.txt_contact_poit);
            labelRegister = itemView.findViewById(R.id.labelRegister);
            Typeface iconType = FontManager.getTypeface(itemView.getContext(), FontManager.FONTAWESOME);
            FontManager.markAsIconContainer(labelRegister, iconType);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListener != null) {
                        itemClickListener.onClick(v, getAdapterPosition(), false);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(itemClickListener != null) {
                        itemClickListener.onClick(v, getAdapterPosition(), true);
                    }
                    return false;
                }
            });
            chkContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.e("positon", getAdapterPosition()+ "");
                    contactList.get(getAdapterPosition()).setStatus(isChecked ? 1 : 0);
                }
            });

        }
//        public void bind(int position){
//            int checked = contactList.get(position).getStatus();
//            if(checked == 1){
//                chkContact.setChecked(true);
//            }else{
//                chkContact.setChecked(false);
//            }
//        }

    }


    @NonNull
    @Override
    public MyContactListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_contact, viewGroup, false);
        chkContact = view.findViewById(R.id.chkContact);
        return new MyContactListViewHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(MyContactListViewHolder holder, final int position) {
        //holder.bind(position);
        final Contact contact = contactList.get(position);

        if (!StringUtils.isNullOrEmpty(contact.getUrlImg())) {
            File file = new File(contact.getUrlImg());
            Picasso.with(context).load(file).resize(100, 100).into(holder.imgContact);
        }
        if(contact.isRegister()){
            holder.labelRegister.setVisibility(View.VISIBLE);
        }
        holder.txt_contact_name.setText(contact.getContact_name());
        holder.txtContactPhone.setText(contactList.get(position).getContact_phone());
        //holder.txt_contact_poit.setText(( contactList.get(position).getContact_poit());

        if(isShow) {
            holder.checkBox.setVisibility(View.VISIBLE);

        } else {
            holder.checkBox.setVisibility(View.GONE);
        }

        holder.checkBox.setChecked(contact.getStatus() == 1);



    }

    @Override
    public int getItemCount() {
        if(contactList == null){
            return 0;
        }
        return contactList.size();
    }

//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        contactList.clear();
//        if (charText.length() == 0) {
//            contactList.addAll(arraylist);
//        } else {
//            for (Contact wp : arraylist) {
//                if (wp.getContact_name().toLowerCase(Locale.getDefault())
//                        .contains(charText)) {
//                    contactList.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }
}
