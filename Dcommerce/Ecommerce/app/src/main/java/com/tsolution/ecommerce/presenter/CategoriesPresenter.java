package com.tsolution.ecommerce.presenter;

import android.util.Log;

import com.tsolution.ecommerce.model.Category;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.view.application.AppController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriesPresenter {

    private BaseView baseView;

    public CategoriesPresenter(BaseView base) {
        this.baseView = base;

    }

    public void getDataCategories() {
        AppController.getInstance().getSOService().getCategories().enqueue(new Callback<ServiceResponse<Category>>() {
            @Override
            public void onResponse(Call<ServiceResponse<Category>> call, Response<ServiceResponse<Category>> response) {
                if (response.isSuccessful()) {
                    baseView.handleResponseSuccessful(Constants.GET_DATA_CATEGORIES,new ResponseInfo<>(response.body(), Constants.CODE.SUCCESS));
                } else {
                    Log.e("onResponse", "load fail");
                    baseView.handleResponseError(Constants.GET_DATA_CATEGORIES,Constants.CODE.ERROR_INTERNAL);
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse<Category>> call, Throwable t) {
                Log.e("onFailure", "404:" + t);
                baseView.handleResponseError(Constants.GET_DATA_CATEGORIES,Constants.CODE.ERROR_INTERNAL);
            }
        });

    }

}
