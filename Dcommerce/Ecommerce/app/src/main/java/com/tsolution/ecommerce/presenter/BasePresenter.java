package com.tsolution.ecommerce.presenter;

import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.service.listerner.SOService;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.StringUtils;
import com.tsolution.ecommerce.utils.TsLog;
import com.tsolution.ecommerce.view.activity.BaseActivity;
import com.tsolution.ecommerce.view.application.AppController;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BasePresenter {
    private BaseView baseView;

    public BasePresenter(BaseView baseView) {
        this.baseView = baseView;
    }

    public void get(final Object parent, final String serviceName, final String functionName, Object... params) {


        Callback<Object> responseCallback = new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    if(StringUtils.isNotNullAndNotEmpty(functionName)) {
                        try {

                            Method m = parent.getClass().getMethod(functionName, response.body().getClass());
                            m.invoke(parent, response.body());
                        } catch (Exception e) {
                            handleResponseError(serviceName, Constants.CODE.ERROR_INTERNAL, e);
                        }
                    }

                } else {
                    handleResponseError(serviceName, Constants.CODE.ERROR_INTERNAL, response.body());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                BaseActivity.handleError(parent, serviceName, Constants.CODE.ERROR_INTERNAL, t);
            }
        };
        SOService s = AppController.getInstance().getSOService();
        Method m;
        Class[] arg = null;
        Object[] value = null;
        if (params != null) {

            arg = new Class[params.length];
            for (int i = 0; i < params.length; i++) {
                arg[i] = params[i].getClass();
            }
        }
        try {
            m = s.getClass().getMethod(serviceName, arg);
            ((Call) m.invoke(s, params)).enqueue(responseCallback);
        } catch (Exception e) {
            BaseActivity.handleError(parent, serviceName, Constants.CODE.ERROR_INTERNAL, e);
        }


    }

    void handleResponseError(String funcName, Constants.CODE code, Object err) {
        baseView.handleResponseError(funcName, code);
        TsLog.e("Call API Error: ", funcName + " Error code: " + String.valueOf(code) + ": " + String.valueOf(err));
        baseView.showAlertDialog(" Error code: " + String.valueOf(code) + String.valueOf(err));

    }

    void handleResponseError(String funcName, Constants.CODE code, Throwable throwable) {
        baseView.handleResponseError(funcName, code);
        TsLog.e("Call API Error: ", funcName + " Error code: " + String.valueOf(code) + ": " + String.valueOf(throwable));
        baseView.showAlertDialog(" Error code: " + String.valueOf(code) + String.valueOf(throwable));
    }

}
