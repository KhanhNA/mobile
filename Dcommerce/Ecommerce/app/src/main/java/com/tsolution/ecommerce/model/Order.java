package com.tsolution.ecommerce.model;

import com.google.gson.annotations.SerializedName;
import com.tsolution.ecommerce.model.dto.OrderPackingDTO;

import java.util.ArrayList;
import java.util.HashMap;

import lombok.Data;

@Data
public class Order {
    @SerializedName("orderId")
    private long id;
    @SerializedName("merchantId")
    private long merchantId;
    @SerializedName("merchantName")
    private String merchantName;
    @SerializedName("parentMerchantId")
    private long parentMerchantId;
    @SerializedName("quantity")
    private int quantity;

    @SerializedName("orderPackings")
    ArrayList<OrderPackingDTO> orderlist;
    @SerializedName("packingDescriptions")
    HashMap<String, OrderPackingDTO> orderDescription;
    private String createDate;
    private int status;
    private String urlProduct;
    private String productName;
    private long price;
    private String providerName;
    private String urlProviderImg;

    public Order(String urlProduct, String productName, Long price, String providerName, String urlProviderImg, int quantity, int status){
        this.urlProduct = urlProduct;
        this.productName = productName;
        this.price = price;
        this.providerName = providerName;
        this.urlProviderImg = urlProviderImg;
        this.quantity = quantity;
        this.status = status;

    }
}
