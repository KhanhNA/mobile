package com.tsolution.ecommerce.view.activity;

import android.app.*;
import android.content.*;
import android.os.*;

import androidx.annotation.Nullable;
import androidx.appcompat.app.*;
import android.view.*;
import android.widget.*;

import com.tsolution.ecommerce.*;
import com.tsolution.ecommerce.model.dto.*;
import com.tsolution.ecommerce.utils.*;

public class LocationActivity extends AppCompatActivity implements View.OnClickListener {
    EditText edNameReceive;
    EditText edPhone;
    EditText edAddress;
    Button btnPay;
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_reception_address);
        init();

    }

    private void init() {
        edNameReceive = findViewById(R.id.edNameReceive);
        edPhone = findViewById(R.id.edPhone);
        edAddress = findViewById(R.id.edAddress);
        btnPay = findViewById(R.id.btnPay);
        btnPay.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.btnPay){
            ReceptionAddress receptionAddress = new ReceptionAddress();
            if (!StringUtils.isNullOrEmpty(edNameReceive.getText().toString())){
                receptionAddress.setName(edNameReceive.getText().toString());
            }else
                Toast.makeText(this,"Vui lòng nhập tên!",Toast.LENGTH_LONG).show();

            if (!StringUtils.isNullOrEmpty(edAddress.getText().toString())){
                receptionAddress.setAddres(edAddress.getText().toString());
            }else
                Toast.makeText(this,"Vui lòng nhập địa chỉ!",Toast.LENGTH_LONG).show();

            if (!StringUtils.isNullOrEmpty(edPhone.getText().toString())){
                receptionAddress.setPhone(edPhone.getText().toString());
            }else
                Toast.makeText(this,"Vui lòng nhập số điện thoại!",Toast.LENGTH_LONG).show();

            if (!StringUtils.isNullOrEmpty(edNameReceive.getText().toString()) && !StringUtils.isNullOrEmpty(edAddress.getText().toString()) && !StringUtils.isNullOrEmpty(edPhone.getText().toString())){
                Intent returnIntent = new Intent();
                returnIntent.putExtra("receptionAddress", receptionAddress);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }else
                Toast.makeText(this,"Vui lòng nhập đầy đủ các trường!",Toast.LENGTH_LONG).show();

        }
    }

}
