package com.tsolution.ecommerce.view.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.MainPagerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import devlight.io.library.ntb.NavigationTabBar;

public class MainActivity extends BaseActivity {

    @BindView(R.id.main_layout)
    ViewPager viewPager;
    boolean doubleBackToExitPressedOnce = false;
    private String TAG= MainActivity.class.getName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initUI();

    }

    private void initViewPager() {
        // View Pager
        MainPagerAdapter myPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.arr_title));
        viewPager.setAdapter(myPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
        viewPager.setCurrentItem(0);
    }

    @SuppressLint("ResourceType")
    private void initUI() {
        initViewPager();



        final NavigationTabBar navigationTabBar = findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_first),
                        Color.parseColor(getResources().getString(R.color.main_color)))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_sixth))
                        .title(getResources().getString(R.string.menu_product))
                        .badgeTitle(null)
                        .build()
        );
        models.get(0).hideBadge();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_second),
                        Color.parseColor(getResources().getString(R.color.main_color)))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title(getResources().getString(R.string.menu_management_agent))
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_notifications_active_black_24dp),
                        Color.parseColor(getResources().getString(R.color.main_color)))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title(getResources().getString(R.string.menu_notification))
                        .badgeTitle("50")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_third),
                        Color.parseColor(getResources().getString(R.color.main_color)))
                        .selectedIcon(getResources().getDrawable(R.drawable.ic_seventh))
                        .title(getResources().getString(R.string.MENU_ME))
                        .badgeTitle("state")
                        .build()
        );




        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 0);

        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });
        models.get(0).hideBadge();
        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_chat:
//                Intent i = new Intent(MainActivity.this, ChatActivity.class);
//                startActivity(i);
                break;
            case R.id.action_cart:
//                startActivity(new Intent(MainActivity.this, CartActivity.class));
                break;


        }
        return super.onOptionsItemSelected(item);


    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
