package com.tsolution.ecommerce.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsolution.ecommerce.adapter.DateDeserializer;
import com.tsolution.ecommerce.view.application.AppController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;

    public static Retrofit getClient(String baseUrl) {
        if(retrofit == null){
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))

                    .build();
        }
        return retrofit;
    }

    public static Retrofit init(String baseUrl, final String accessToken) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        Request.Builder newRequest = request.newBuilder().addHeader("Authorization", "BEARER " + accessToken.trim())
                                .addHeader("Content-Type", "application/json");
                        return chain.proceed(newRequest.build());
                    }
                });
        //Add the interceptor to the client builder.
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        retrofit = new Retrofit.Builder()
                .client(okHttpClientBuilder.build())
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))

                .build();

        return retrofit;
    }


    public static boolean refreshToken()//String url, String refresh, String cid, String csecret)
            throws IOException {

        URL refreshUrl = new URL(AppController.BASE_LOGIN_URL);
        HttpURLConnection urlConnection = (HttpURLConnection) refreshUrl.openConnection();
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        urlConnection.setUseCaches(false);
        String userRefreshToken = "oauth/token";
        String cid = "Dcommerce";
        String csecret = "A31b4c24l3kj35d4AKJQ";
        urlConnection.setRequestProperty("grant_type", "password");
        urlConnection.setRequestProperty("username", "admin");
        urlConnection.setRequestProperty("password", "jwtpass");
        urlConnection.addRequestProperty("client_secret", "A31b4c24l3kj35d4AKJQ");
        urlConnection.addRequestProperty("client_id", "Dcommerce");
        urlConnection.setRequestProperty("scope", "read write");
        String urlParameters = "grant_type=password&clientId="
                + cid
                + "&clientSecret="
                + csecret
//                + "&refresh_token="
//                + refresh;
                + "&username=admin"
                + "&password=jwtpass";
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.connect();
        String msg = urlConnection.getResponseMessage();
        int responseCode = urlConnection.getResponseCode();

        if (responseCode == 200) {
            BufferedReader in =
                    new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return true;
        } else {
            return false;
        }
    }
}
