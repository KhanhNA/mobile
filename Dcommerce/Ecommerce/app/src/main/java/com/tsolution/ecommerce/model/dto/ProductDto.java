package com.tsolution.ecommerce.model.dto;

import android.database.Cursor;

import com.tsolution.ecommerce.db.DataBaseSqlLite;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDto {
    private Long id;
    private Long manufacturerId;
    private String productName;

    private String urlImage;

    private Double reviewAvg;

    private Integer reviewCount;

    private Double price;

    private Integer quantity;

    private Integer typePromotion;
    private Double orgPrice;
    private Double marketPrice;
    private Double discountPercent;
    public void initFromCursor(Cursor cursor) {
        id = cursor.getLong(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_MASP));
        productName = cursor.getString(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_TENSP));
        price = cursor.getDouble(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_GIATIEN));
        urlImage = cursor.getString(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_HINHANH));
        quantity = cursor.getInt(cursor.getColumnIndex(DataBaseSqlLite.TB_GIOHANG_SOLUONG));

    }
}
