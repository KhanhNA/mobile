package com.tsolution.ecommerce.adapter;

import android.app.Activity;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.dto.WalletHisDto;
import com.tsolution.ecommerce.utils.DateUtils;
import com.tsolution.ecommerce.view.application.AppController;
import com.tsolution.ecommerce.widget.ILoadMore;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;



public class WalletHisAdapterV2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM=0,VIEW_TYPE_LOADING=1;
    ILoadMore loadMore;
    boolean isLoading;
    Activity activity;
    private ArrayList<WalletHisDto> arrWalletHis;
    int visibleThreshold=2;
    int lastVisibleItem,totalItemCount;

    public WalletHisAdapterV2(RecyclerView recyclerView,Activity activity, ArrayList<WalletHisDto> arrWalletHis) {
        this.activity = activity;
        this.arrWalletHis = arrWalletHis;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if(!isLoading && totalItemCount <= (lastVisibleItem+visibleThreshold))
                {
                    if(loadMore != null)
                        loadMore.onLoadMore();
                    isLoading = true;
                }

            }
        });
    }

    public void update(ArrayList<WalletHisDto> arrWalletHis){
        this.arrWalletHis = arrWalletHis;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return arrWalletHis.get(position) == null ? VIEW_TYPE_LOADING:VIEW_TYPE_ITEM;
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_ITEM)
        {
            View view = LayoutInflater.from(activity)
                    .inflate(R.layout.layout_item_wallet_his,parent,false);
            return new ViewHolder(view);
        }
        else if(viewType == VIEW_TYPE_LOADING)
        {
            View view = LayoutInflater.from(activity)
                    .inflate(R.layout.item_loading,parent,false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder view, int position) {

        if(view instanceof  ViewHolder)
        {
            ViewHolder viewHolder = (ViewHolder) view;
            WalletHisDto walletHisDto = arrWalletHis.get(position);
            if(walletHisDto.walletId != null){
                viewHolder.txtOrderID1.setText(String.valueOf(walletHisDto.orderId));
            }
            if (walletHisDto.createDate != null) {
                viewHolder.txtOrderDate1.setText(DateUtils.formatDate(walletHisDto.createDate,DateUtils.DATE_STRING_DD_MM_YYYY));
            }
            if(walletHisDto.amount != null){
                viewHolder.txtAmount1.setText(AppController.getInstance().formatCurrency(walletHisDto.amount));
            }
        }
        else if(view instanceof LoadingViewHolder)
        {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)view;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }
    @Override
    public int getItemCount() {
        if(arrWalletHis != null && arrWalletHis.size() > 0){
            return arrWalletHis.size();
        }
        return 0;
    }
    public void setLoaded() {
        isLoading = false;
    }
    class LoadingViewHolder extends RecyclerView.ViewHolder
    {
        public ProgressBar progressBar;
        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtOrderDate1)
        TextView txtOrderDate1;
        @BindView(R.id.txtOrderID1)
        TextView txtOrderID1;
        @BindView(R.id.txtAmount1)
        TextView txtAmount1;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            Paint p = new Paint();
            p.setColor(ContextCompat.getColor(activity,R.color.colorAccent_v2));
            p.setFlags(Paint.UNDERLINE_TEXT_FLAG);
            txtOrderID1.setPaintFlags(p.getFlags());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, "Show history detail", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

