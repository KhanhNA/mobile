package com.tsolution.ecommerce.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.tsolution.ecommerce.view.fragment.ContactFragment;
import com.tsolution.ecommerce.view.fragment.NotificationFragment;
import com.tsolution.ecommerce.view.fragment.ProductFragment;
import com.tsolution.ecommerce.view.fragment.MeFragment;

/**
 * Created by KaKa on 5/16/2017.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    private MeFragment mefragment;
    private ContactFragment contactfragment;
    private ProductFragment productFragment;
    private NotificationFragment notificationFragment;
    private String[] TITLES;

    public MainPagerAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.TITLES = title;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        //Show Fragment
        switch (position) {
            case 0:
                productFragment = new ProductFragment().getInstance();
                return productFragment;

            case 1:
                contactfragment = new ContactFragment();
                return contactfragment;

            case 2:
                notificationFragment = new NotificationFragment();
                return notificationFragment;

            case 3:
                mefragment = new MeFragment();
                return mefragment;
        }
        return null;
    }
}
