package com.tsolution.ecommerce.model.response;


import com.google.gson.annotations.*;
import com.tsolution.ecommerce.model.*;
import com.tsolution.ecommerce.model.dto.*;

import java.util.*;

import lombok.*;

@Data
public class OrderListProductCartResponse {
    private OrderProductCartDTO OrderCart;
}
