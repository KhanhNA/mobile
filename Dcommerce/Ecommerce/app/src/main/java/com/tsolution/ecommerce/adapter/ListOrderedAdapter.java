package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.dto.OrderDTO;
import com.tsolution.ecommerce.model.dto.OrderDetailDTO;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.DateUtils;
import com.tsolution.ecommerce.view.activity.OrderDetailActivityV2;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.List;

public class ListOrderedAdapter extends RecyclerView.Adapter<ListOrderedAdapter.MyViewHolder> {
    private List<OrderDTO> arrData;
    private Context mContext;

    public ListOrderedAdapter(Context mContext, List<OrderDTO> arrData) {
        this.arrData = arrData;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ordered, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ListOrderedAdapter.MyViewHolder holder, int i) {
        if (arrData != null && arrData.size() > 0) {
            OrderDTO order = arrData.get(i);
            holder.txtOrderNumber.setText(mContext.getString(R.string.ORDER) + " " + order.getOrderId());
            holder.txtOrderDate.setText("Ngày đặt hàng: " + DateUtils.getCurrentDate());
//            holder.txtTile.setText(mContext.getString(R.string.orderer) + " " + order.merchantName);
            holder.txtTile.setText(mContext.getString(R.string.orderer) + " Merchant Name " );
            Picasso.with(mContext).load(order.getMerchantImgUrl()).placeholder(R.drawable.logots).into(holder.imgOrder);
            holder.txtAmount.setText(AppController.getInstance().formatCurrency(order.getAmount()));
        }

    }


    @Override
    public int getItemCount() {
        return arrData != null ? arrData.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTile, txtAmount, txtOrderNumber, txtOrderDate;
        ImageView imgOrder;
        LinearLayout layoutRoot;

        MyViewHolder(View view) {
            super(view);
            txtTile = view.findViewById(R.id.txtTile);
            txtAmount = view.findViewById(R.id.txtAmount);
            txtOrderNumber = view.findViewById(R.id.txtOrderNumber);
            txtOrderDate = view.findViewById(R.id.txtOrderDate);
            imgOrder = view.findViewById(R.id.imgOrder);
            layoutRoot = view.findViewById(R.id.layoutRoot);

            layoutRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("orderDTO", OrderDetailDTO.builder().orderId(arrData.get(getAdapterPosition()).orderId)
                            .langId(Constants.LANG_VI)
                            .merchantId(Constants.MERCHANT_ID_FAKE)
                            .orderType(arrData.get(getAdapterPosition()).orderType)
                            .orderStatus(arrData.get(getAdapterPosition()).orderStatus)
                            .build());
                    Intent intent = new Intent(mContext, OrderDetailActivityV2.class);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }
    }

}
