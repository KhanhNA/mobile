package com.tsolution.ecommerce.service.listerner;

import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.utils.Constants;

public interface BaseView {

    void handleResponseError(int actionSender, Constants.CODE code);

    void handleResponseError(String servicesName, Constants.CODE code);

    void handleResponseSuccessful(int actionSender, ResponseInfo response);

    void loading(String msg);

    void showAlertDialog(String msg);

    void closeAlertDialog();
}
