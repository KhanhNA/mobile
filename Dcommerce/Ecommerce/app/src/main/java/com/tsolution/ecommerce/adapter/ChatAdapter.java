package com.tsolution.ecommerce.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.view.activity.ChatMessageActivity;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder>{
    private ArrayList<Integer> icons;
    private ArrayList<String> titles;
    private ArrayList<String> description;
    private Activity a;
    public ChatAdapter(ArrayList<Integer> icons, ArrayList<String> titles, ArrayList<String> description, Activity a){
        this.icons = icons;
        this.titles = titles;
        this.description = description;
        this.a = a;
    }
    public ChatAdapter(){

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.chat_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        String title = this.titles.get(i);
        String description = this.description.get(i);
        viewHolder.icon.setImageResource(icons.get(i));
        viewHolder.title.setText(title);
        viewHolder.description.setText(description);
        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i = new Intent(a, ChatMessageActivity.class);
                i.putExtra("title_chat", viewHolder.title.getText().toString());
                a.startActivity(i);
            }
        });



    }

    @Override
    public int getItemCount() {
        return titles.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        ImageView icon;
        TextView title;
        TextView description;
        private ItemClickListener itemClickListener;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            icon = view.findViewById(R.id.icon);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
            view.setOnClickListener(this);


        }
        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition());
        }
    }
    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
