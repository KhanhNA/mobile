package com.tsolution.ecommerce.model.dto;

import com.google.gson.annotations.*;

import java.util.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class OrderDTO implements Serializable {
    public Integer isSaveOrder ;
    /**
     *
     */
    public Long orderId;

    /**
     * Mã đơn hàng
     */
    public String orderNo;

    /**
     *
     */
    public Long merchantId;
    public Long merchantTypeId;
    /**
     *
     */
    public String merchantName;

    /**
     *
     */
    public Long paymentMethodId;

    /**
     *
     */
    public Long orderInvoiceId;

    /**
     *
     */
    public Long shipMethodId;

    /**
     *
     */
    public Double shipCharge;

    /**
     *
     */
    public Long shpCartId;

    /**
     *
     */
    public Long recvStoreId;

    /**
     *
     */
    public Double amount;

    /**
     *
     */
    public Double vat;

    /**
     *
     */
    public Double incentiveAmount;

    /**
     *
     */
    public Double quantity;

    /**
     *
     */
    public Double recvLat;

    /**
     *
     */
    public Double recvLong;

    /**
     *
     */
    public String recvAddr;
    public Date orderDate;

    /**
     *
     */
    public Integer orderStatus;

    public Double remainQuantity;
    public Double remainAmount;
    public Boolean isSuccess;
    public Integer orderType;
    public Integer langId;
    public Long parentMerchantId;
    public Double walletAmount;

//    public Date orderDate;


    /**
     * Đã thanh toán chưa
     */

    public Integer paymentStatus;

    /**
     * Đơn hàng đã giao cho khách hàng
     */

    public Integer deliveryStatus;

    /**
     * Ngày thanh toán
     */

    public Date paymentDate;

    /**
     * Ngày giao hàng
     */

    public Date deliveryDate;
    public String merchantImgUrl;

    public List<OrderPackingDTO> orderPackings;
    public HashMap<String, PackingDescriptionDto> packingDescriptions;
    public List<OrderPackingDTO> promotionPacking;

    public void addPackingProduct(OrderPackingDTO packingProduct) {
        if(this.orderPackings == null) {
            this.orderPackings = new ArrayList<>();
        }
        this.orderPackings.add(packingProduct);
    }
}
