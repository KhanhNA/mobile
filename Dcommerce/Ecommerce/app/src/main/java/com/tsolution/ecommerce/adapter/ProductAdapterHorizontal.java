package com.tsolution.ecommerce.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.db.chiTietSanPham.PresenterLogicChiTietSanPham;
import com.tsolution.ecommerce.model.dto.ProductDto;
import com.tsolution.ecommerce.service.listerner.RecyclerViewClickListener;
import com.tsolution.ecommerce.utils.FontManager;
import com.tsolution.ecommerce.view.activity.ProductDetailActivity;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapterHorizontal extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ProductDto> arrData;
    private Context mContext;
    private RecyclerViewClickListener mListener;


    public ProductAdapterHorizontal(Context context, ArrayList<ProductDto> data, RecyclerViewClickListener listener) {
        arrData = data;
        mContext = context;
        mListener = listener;

    }
    public void update(List<ProductDto> arrData){
        this.arrData = arrData;
        notifyDataSetChanged();
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_horizontal, parent, false);
        return new ProductAdapterHorizontal.CustomViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CustomViewHolder) {
            ProductDto item = arrData.get(position);
            CustomViewHolder viewHolder = (CustomViewHolder) holder;
            if (item.getProductName() != null && !"".equals(item.getProductName())) {
                viewHolder.name.setText(item.getProductName());
            }
            if (item.getPrice() != null) {
                viewHolder.price.setText("Giá: " + AppController.getInstance().formatCurrency(item.getPrice()));
            }
            if (item.getQuantity() != null) {
                viewHolder.txtQuantity.setText("/" + item.getQuantity());
            }
            if(item.getReviewAvg() != null){
                viewHolder.rating.setRating(item.getReviewAvg().floatValue());
            }
            String img = item.getUrlImage();
            if (img != null) {
                Picasso.with(mContext).load(img).into(viewHolder.image);
            }
            if(item.getOrgPrice() != null){
                viewHolder.txtOriginPrice.setText(AppController.getInstance().formatCurrency(item.getOrgPrice()));
            }
            if(item.getDiscountPercent() != null){
                viewHolder.txtOriginPrice.setText(String.valueOf(item.getDiscountPercent()));
            }
            if (item.getTypePromotion() != null && item.getTypePromotion() == 2) {
                viewHolder.imgProductSale.setVisibility(View.VISIBLE);
            } else {
                viewHolder.imgProductSale.setVisibility(View.GONE);
            }
            PresenterLogicChiTietSanPham presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();
            if (presenterLogicChiTietSanPham.getQuantityProduct(mContext, item) > 0) {
                viewHolder.txtQuantityProductCart.setVisibility(View.VISIBLE);
                viewHolder.txtQuantityProductCart.setText(String.valueOf(presenterLogicChiTietSanPham.getQuantityProduct(mContext, item)));
            } else {
                viewHolder.txtQuantityProductCart.setVisibility(View.GONE);
            }

            viewHolder.setIndex(position);
//            if(cart.get(item.getProductId()) != null){
//                viewHolder.txtOrderQuantity.setVisibility(View.VISIBLE);
//                viewHolder.txtOrderQuantity.setText(cart.get(item.getProductId()).toString());
//            }


        }


    }

    @Override
    public int getItemCount() {
        return arrData == null ? 0 : arrData.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView name, price, txtQuantity, iconCart, txtOriginPrice, txtDiscountPercent;
        ImageView image, imgProductSale;
        ScrollView layoutRoot;
        FrameLayout frCart;
        private int index;
        RatingBar rating;
        TextView txtQuantityProductCart;
        private RecyclerViewClickListener mListener;
//        BottomSheetBehavior sheetBehavior;
//        LinearLayout bottom_sheet_quantity_product;

        CustomViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mListener = listener;
            txtQuantity = itemView.findViewById(R.id.txtQuantity);
            iconCart = itemView.findViewById(R.id.iconCart);
            name = itemView.findViewById(R.id.txtNameProduct);
            price = itemView.findViewById(R.id.txtPrice);
            image = itemView.findViewById(R.id.imgProduct);
            layoutRoot = itemView.findViewById(R.id.layoutRoot);
            rating = itemView.findViewById(R.id.ratingBar);
            imgProductSale = itemView.findViewById(R.id.imgProductSale);
            frCart = itemView.findViewById(R.id.frCart);
            txtOriginPrice = itemView.findViewById(R.id.txtOriginPrice);
            txtDiscountPercent = itemView.findViewById(R.id.txtDiscountPercent);
            txtQuantityProductCart = itemView.findViewById(R.id.txtQuantityProductCart);
            //bottom sheet
//            bottom_sheet_quantity_product = itemView.findViewById(R.id.bottom_sheet_quantity_product);
//            sheetBehavior = BottomSheetBehavior.from(bottom_sheet_quantity_product);
//            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//            imgCartSubProduct = itemView.findViewById(R.id.imgCartSubProduct);
//            txtOrderQuantity = itemView.findViewById(R.id.txtOrderQuantity);
            Typeface iconFont = FontManager.getTypeface(mContext, FontManager.FONTAWESOME_PRO);
            FontManager.markAsIconContainer(iconCart, iconFont);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, ProductDetailActivity.class);
                    i.putExtra("product_id", arrData.get(index).getId());
                    notifyDataSetChanged();

                    mContext.startActivity(i);
                }
            });
            frCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ProductDto item = arrData.get(getAdapterPosition());
                    mListener.onClick(v, item);
                }
            });

        }


        private void setIndex(int index) {
            this.index = index;
        }
    }

}



