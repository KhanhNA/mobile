package com.tsolution.ecommerce.presenter;

import android.content.*;
import android.util.Log;

import com.tsolution.ecommerce.db.*;
import com.tsolution.ecommerce.db.chiTietSanPham.*;
import com.tsolution.ecommerce.model.*;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.view.application.AppController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetialPresent {
    private BaseView baseView;
    public ProductDetialPresent(BaseView baseView){
        this.baseView = baseView;
    }

    public void getProductById(long productID, int langId){
        AppController.getInstance().getSOService().getProductById(productID, langId).enqueue(new Callback<ProductDetail>() {
            @Override
            public void onResponse(Call<ProductDetail> call, Response<ProductDetail> response) {
                if (response.isSuccessful()) {
                    baseView.handleResponseSuccessful(Constants.GET_PRODUCT_DETAIL,new ResponseInfo<>(response.body(), Constants.CODE.SUCCESS));
                } else {
                    Log.e("Response_PrDetail", "load fail");
                    baseView.handleResponseError(Constants.GET_PRODUCT_DETAIL,Constants.CODE.ERROR_INTERNAL);
                }
            }

            @Override
            public void onFailure(Call<ProductDetail> call, Throwable t) {
                Log.e("Failure_PrDetail", "404:" + t);
                baseView.handleResponseError(Constants.GET_PRODUCT_DETAIL,Constants.CODE.ERROR_INTERNAL);
            }
        });
    }


}
