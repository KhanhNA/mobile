package com.tsolution.ecommerce.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.BaseView;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.TsLog;

public abstract class BaseFragment extends Fragment implements BaseView {
    ProgressDialog pd;
    AlertDialog alertDialog;
    protected Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {

    }

    @Override
    public void handleResponseError(String funcName, Constants.CODE code) {

    }

    @Override
    public void handleResponseSuccessful(int actionSender, ResponseInfo response) {

    }

    @Override
    public void loading(String msg) {
        pd = new ProgressDialog(getContext());
        pd.setCancelable(false);
        pd.setMessage(msg);
        pd.show();
    }

    @Override
    public void showAlertDialog(String msg) {
        try {
            if (mContext != null) {
                if (alertDialog == null) {
                    alertDialog = new AlertDialog.Builder(mContext, R.style.Theme_AppCompat_Dialog_Alert).create();
                }
                if (!alertDialog.isShowing()) {
                    alertDialog.setMessage(msg);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Đóng",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        }catch (Exception e)
        {
            TsLog.e("Error Alert dialog: ", "", e);
        }

    }

    @Override
    public void closeAlertDialog() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    public void closeProcess() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }


}
