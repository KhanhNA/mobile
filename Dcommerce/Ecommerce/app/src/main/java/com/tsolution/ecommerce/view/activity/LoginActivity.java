package com.tsolution.ecommerce.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.dto.MerchantDto;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.service.RetrofitClient;
import com.tsolution.ecommerce.view.application.AppController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.mimic.oauth2library.OAuth2Client;
import ca.mimic.oauth2library.OAuthResponse;
import ca.mimic.oauth2library.OAuthResponseCallback;

import static com.tsolution.ecommerce.utils.Constants.MK;
import static com.tsolution.ecommerce.utils.Constants.TOKEN_INFO;
import static com.tsolution.ecommerce.utils.Constants.USER_NAME;

public class LoginActivity extends BaseActivity {
    @BindView(R.id.txtUsername)
    TextInputLayout txtUsername;
    @BindView(R.id.txtPassword)
    TextInputLayout txtPassword;
    @BindView(R.id.txtLoginFail)
    TextView txtLoginFail;
    @BindView(R.id.chkSave)
    CheckBox checkBox;
    private String TAG = "LoginActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        txtUsername.getEditText().setText(AppController.getInstance().getSharePre().getString(USER_NAME, ""));
        txtPassword.getEditText().setText(AppController.getInstance().getSharePre().getString(MK, ""));
        checkBox.setChecked(AppController.getInstance().getSharePre().getBoolean("chkSave", false));


    }

    public boolean validateInput() {
        if (txtUsername.getEditText().getText().toString().isEmpty()) {
            txtUsername.setError("Field can't be empty");
            return false;
        } else {
            txtUsername.setError(null);
        }
        if (txtPassword.getEditText().getText().toString().isEmpty()) {
            txtPassword.setError("Field can't be empty");
            return false;
        } else {
            txtPassword.setError(null);
        }
        return true;
    }


    @OnClick(R.id.btnLogin)
    public void login() {



        if (validateInput()) {
            String userName = txtUsername.getEditText().getText().toString();
            String password = txtPassword.getEditText().getText().toString();
            requestLogin(this, userName, password);

        }
    }

    public void requestLogin(final Activity activity, final String user, final String pass) {

        loading("Login...");

        OAuth2Client.Builder builder = new OAuth2Client.Builder("Dcommerce", "A31b4c24l3kj35d4AKJQ", AppController.BASE_LOGIN_URL)
                .grantType("password")
                .scope("read write")
                .username(user)
                .password(pass);

        builder.build().requestAccessToken(new OAuthResponseCallback() {
            @Override
            public void onResponse(OAuthResponse response) {
                if (response.isSuccessful()) {
                    String token = response.getAccessToken();
                    Log.e("NTH", token);
                    AppController.getInstance().saveObject(token, TOKEN_INFO);
                    txtLoginFail.setVisibility(View.INVISIBLE);
                    //
                    SharedPreferences.Editor editor = AppController.getInstance().getEditor();
                    editor.putString(USER_NAME, user);
                    editor.putString(TOKEN_INFO, token);
                    if (checkBox.isChecked()) {
                        editor.putString(MK, pass);
                        editor.putBoolean("chkSave", true);
                    } else {
                        editor.putBoolean("chkSave", false);
                        editor.putString(MK, "");
                    }
                    //xác nhận lưu trữ
                    editor.commit();
                    RetrofitClient.init(AppController.BASE_URL, token);
                    MerchantDto dto = new MerchantDto();
                    dto.setUserName(user);
                    dto.setStatus(1);
                    new BasePresenter(null).get(activity,"getMerchants","startMainActivity", dto, 1, 0);


                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtLoginFail.setVisibility(View.VISIBLE);
                            closeProcess();
                        }
                    });

                }

            }
        });

    }
    public void startMainActivity(ServiceResponse<MerchantDto> dtos){
      closeProcess();


        if(dtos != null && dtos.getArrData() != null && !dtos.getArrData().isEmpty()) {
            AppController.getInstance().putCatche("MERCHANT", dtos.getArrData().get(0));
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }else{
            System.out.println("loi");
        }
        finish();
    }
}
