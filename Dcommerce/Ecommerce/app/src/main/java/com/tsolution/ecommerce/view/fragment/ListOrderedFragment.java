package com.tsolution.ecommerce.view.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ListOrderedAdapter;
import com.tsolution.ecommerce.model.dto.OrderDTO;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.OrderPresenter;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListOrderedFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    OrderPresenter listOrderPresenter;
    @BindView(R.id.rcOrdered)
    RecyclerView rcOrderList;
    @BindView(R.id.swOrdered)
    SwipeRefreshLayout swOrdered;
    private View rootView;
    private ListOrderedAdapter listOrderedAdapter;
    OrderDTO orderDTO;
    private boolean isLoad = true;
    public void setOrderDTO(OrderDTO orderDTO) {
        this.orderDTO = orderDTO;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if((isVisibleToUser && orderDTO!=null && isLoad )|| Constants.IS_REFRESH_ORDERED)
        {
            listOrderPresenter = new OrderPresenter(this);
            getOrders(orderDTO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_list_ordered_fragment, container, false);

        ButterKnife.bind(this, rootView);

        swOrdered.setOnRefreshListener(this);
        swOrdered.setRefreshing(true);
        return rootView;
    }

    private void getOrders(OrderDTO orderDTO) {
        if(orderDTO!=null)
        {
            listOrderPresenter.getOrders(orderDTO);
        }

    }

    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {
        super.handleResponseError(actionSender, code);
        swOrdered.setRefreshing(false);
        Constants.IS_REFRESH_ORDERED = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Constants.IS_REFRESH_ORDERED && getUserVisibleHint())
        {
            getOrders(orderDTO);
        }

    }

    @Override
    public void handleResponseSuccessful(int actionSender, ResponseInfo response) {
        super.handleResponseSuccessful(actionSender, response);
        switch (actionSender) {
            case Constants.GET_DATA_LIST_ORDER:
                Constants.IS_REFRESH_ORDERED = false;
                swOrdered.setRefreshing(false);
                if (response.responseData != null) {
                    isLoad = false;
                    ServiceResponse orderedResponse = (ServiceResponse) response.responseData;
                    listOrderedAdapter = new ListOrderedAdapter(getContext(), orderedResponse.getArrData());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    rcOrderList.setLayoutManager(layoutManager);
                    rcOrderList.setAdapter(listOrderedAdapter);
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        isLoad = false;
        swOrdered.setRefreshing(true);
        getOrders(orderDTO);
    }
}
