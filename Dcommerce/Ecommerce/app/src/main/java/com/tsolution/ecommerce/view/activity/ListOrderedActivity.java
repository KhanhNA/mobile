package com.tsolution.ecommerce.view.activity;


import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ListOrderedAdapter;
import com.tsolution.ecommerce.model.dto.OrderDTO;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.presenter.OrderPresenter;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.UtilsTs;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListOrderedActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rcOrdered)
    RecyclerView rcOrderList;
    private BasePresenter listOrderPresenter;
    private ListOrderedAdapter listOrderedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_list_order);
            ButterKnife.bind(this);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setElevation(4);
            }
            listOrderPresenter = new OrderPresenter(this);
            OrderDTO orderDTO = OrderDTO.builder().orderType(1).merchantId(1l).langId(1).build();
            listOrderPresenter.get(this, "getOrders", "getOrdersSuccess", orderDTO);

        }catch(Exception ex){
            handleError(this, "ListOrderedActivity.onCreate", Constants.CODE.ERROR_INTERNAL, ex);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_chat:
                Intent i = new Intent(ListOrderedActivity.this, ChatActivity.class);
                startActivity(i);

                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getOrdersSuccess(ServiceResponse<OrderDTO> response) {
        if (UtilsTs.isNotNull(response.getArrData())) {
            listOrderedAdapter = new ListOrderedAdapter(ListOrderedActivity.this, response.getArrData());
            LinearLayoutManager layoutManager = new LinearLayoutManager(ListOrderedActivity.this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rcOrderList.setLayoutManager(layoutManager);
            rcOrderList.setAdapter(listOrderedAdapter);
        }
    }




}
