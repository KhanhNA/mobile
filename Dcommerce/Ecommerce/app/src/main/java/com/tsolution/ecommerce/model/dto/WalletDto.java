package com.tsolution.ecommerce.model.dto;

import java.util.List;

/**
 * - Mỗi đại lý (merchant) sẽ có 1 ví (wallet)
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
public class WalletDto {

    /**
     *
     */
    public Long walletId;

    /**
     *
     */
    public Long merchantId;

    /**
     *
     */
    public Double amount;

    //public List<WalletHisDto> walletHisDtos;




}