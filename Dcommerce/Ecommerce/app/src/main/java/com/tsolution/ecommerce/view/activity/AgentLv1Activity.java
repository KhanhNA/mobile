package com.tsolution.ecommerce.view.activity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.model.dto.MerchantDto;
import com.tsolution.ecommerce.presenter.BasePresenter;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.FontManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AgentLv1Activity extends BaseActivity implements View.OnClickListener {
    private MerchantDto merchantDto;
    private BasePresenter basePresenter;

    @BindView(R.id.btnDeActive)
    TextView btnDeActive;
    @BindView(R.id.scroll)
    NestedScrollView scrollView;
    @BindView(R.id.toolbar)
    LinearLayout toolbar;
    @BindView(R.id.btnCart)
    TextView btnCart;
    @BindView(R.id.btnChat)
    TextView btnChat;
    @BindView(R.id.iconBack)
    TextView iconBack;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_lv1);
        ButterKnife.bind(this);
        Typeface iconFont = FontManager.getTypeface(AgentLv1Activity.this, FontManager.FONTAWESOME_PRO);
        //set type face
        FontManager.markAsIconContainer(iconBack, iconFont);
        FontManager.markAsIconContainer(btnCart, iconFont);
        FontManager.markAsIconContainer(btnChat, iconFont);

        //khởi tạo đối tượng
        merchantDto = new MerchantDto();
        basePresenter = new BasePresenter(this);


        //set onclick
        iconBack.setOnClickListener(this);
        btnDeActive.setOnClickListener(this);
        onScroll();

    }

    private void onScroll() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView nestedScrollView, int i, int scrollY, int i2, int i3) {
                if (scrollY == 0) {
                    toolbar.setBackgroundColor(Color.parseColor("#00ffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.white));
                    btnChat.setTextColor(getResources().getColor(R.color.white));
                    iconBack.setTextColor(Color.parseColor("#ffffff"));
                    txtTitle.setTextColor(Color.parseColor("#00000000"));
                } else if (scrollY > 50 && scrollY < 100) {
                    toolbar.setBackgroundColor(Color.parseColor("#33ffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.primaryPressed));
                    btnChat.setTextColor(getResources().getColor(R.color.primaryPressed));
                    iconBack.setTextColor(Color.parseColor("#ccffffff"));
                    txtTitle.setTextColor(Color.parseColor("#33000000"));
                } else if (scrollY > 100 && scrollY < 200) {
                    toolbar.setBackgroundColor(Color.parseColor("#88ffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.accent));
                    btnChat.setTextColor(getResources().getColor(R.color.accent));
                    iconBack.setTextColor(Color.parseColor("#33ffffff"));
                    txtTitle.setTextColor(Color.parseColor("#88000000"));
                } else if (scrollY > 200 && scrollY < 300) {
                    toolbar.setBackgroundColor(Color.parseColor("#ccffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.da_cam));
                    btnChat.setTextColor(getResources().getColor(R.color.da_cam));
                    iconBack.setTextColor(Color.parseColor("#77B20024"));
                    txtTitle.setTextColor(Color.parseColor("#cc000000"));
                } else if (scrollY > 300 && scrollY < 500) {
                    toolbar.setBackgroundColor(Color.parseColor("#ffffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.da_cam));
                    btnChat.setTextColor(getResources().getColor(R.color.da_cam));
                    iconBack.setTextColor(Color.parseColor("#B20024"));
                    txtTitle.setTextColor(Color.parseColor("#000000"));
                }

            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDeActive:
                //thêm xác nhận
                //thay bằng id user đăng nhập
                merchantDto.setMerchantId((long) 5);
                loading("DeActiving . Please wait....");
                basePresenter.get(this, "deActiveMerchant", "deActiveMerchantSuccessFul", merchantDto);
                break;
            case R.id.iconBack:
                onBackPressed();
                break;

        }
    }

    public void deActiveMerchantSuccessFul(MerchantDto merchantDto){
        closeProcess();
        //xác nhận bằng code mới dược deactive
        showAlertDialog("DeActive success!");
//        Intent i = new Intent(AgentLv1Activity.this, LoginActivity.class);
//        startActivity(i);
//
    }

    @Override
    public void handleResponseError(String serviceName, Constants.CODE code) {
        super.handleResponseError(serviceName, code);
        switch (serviceName){
            case "deActiveMerchant":
                closeProcess();
                showAlertDialog("DeActive Error! Please try again.");
                break;
        }
    }
}
