package com.tsolution.ecommerce.view.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.CategroriesAdapter;
import com.tsolution.ecommerce.adapter.ManufacturerAdapter;
import com.tsolution.ecommerce.adapter.ProductAdapterHorizontal;
import com.tsolution.ecommerce.adapter.ProductAdapter;
import com.tsolution.ecommerce.db.chiTietSanPham.PresenterLogicChiTietSanPham;
import com.tsolution.ecommerce.db.chiTietSanPham.ViewChiTietSanPham;
import com.tsolution.ecommerce.model.Category;
import com.tsolution.ecommerce.model.Manufacturers;
import com.tsolution.ecommerce.model.Products;
import com.tsolution.ecommerce.model.dto.ProductDto;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.CategoriesPresenter;
import com.tsolution.ecommerce.presenter.ManufacturePresenter;
import com.tsolution.ecommerce.presenter.ProductsPresenter;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.service.listerner.RecyclerViewClickListener;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.utils.FontManager;
import com.tsolution.ecommerce.utils.StringUtils;
import com.tsolution.ecommerce.view.activity.CartActivity;
import com.tsolution.ecommerce.view.application.AppController;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductFragment extends BaseFragment implements BaseSliderView.OnSliderClickListener, ViewChiTietSanPham, ViewPagerEx.OnPageChangeListener, View.OnClickListener {
    private static ProductFragment mInstance;
    private ArrayList<ProductDto> arrProducts;
    private View rootView;
    @BindView(R.id.btnSeeMoreHighlight)
    TextView btnSeeMoreHighlight;
    @BindView(R.id.btnSeeMorePopular)
    TextView btnSeeMorePopular;
    @BindView(R.id.btnSeeMoreSale)
    TextView btnSeeMoreSale;
    @BindView(R.id.scroll_list_product)
    NestedScrollView scrollView;
    @BindView(R.id.toolbar)
    LinearLayout toolbar;
    @BindView(R.id.btnCart)
    TextView btnCart;
    @BindView(R.id.btnChat)
    TextView btnChat;
    @BindView(R.id.rcDanhMucSp)
    RecyclerView rcDanhMucSp;
    @BindView(R.id.rcBrand)
    RecyclerView rcBrand;
    @BindView(R.id.rcAllProduct)
    RecyclerView rcAllProduct;
    @BindView(R.id.rcHighlightProducts)
    RecyclerView rcHighlightProducts;
    @BindView(R.id.rcPopularProduct)
    RecyclerView rcPopularProduct;
    @BindView(R.id.rcSaleProducts)
    RecyclerView rcSaleProducts;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout pullToRefresh;
    @BindView(R.id.txtQuantityProductCart)
    TextView txtQuantityProductCart;
    @BindView(R.id.frCart)
    FrameLayout frCart;
    @BindView(R.id.bottom_sheet_main_activity)  LinearLayout bottom_sheet_quantity_product;
    @BindView(R.id.slider) SliderLayout mDemoSlider;
    @BindView(R.id.txtProductName) TextView txtProductName;
    @BindView(R.id.txtProductPriceAndNumer) TextView txtProductPriceAndNumer;
    @BindView(R.id.imReduceNumberProduct) ImageView imReduceNumberProduct;
    @BindView(R.id.edNumber_Product)
    EditText edNumber_Product;
    @BindView(R.id.imgAddProduct)
    ImageView imgAddProduct;
    @BindView(R.id.btnAddToCartMainActivity)
    Button btnAddToCartMainActivity;

    //bottom_sheet
    BottomSheetBehavior sheetBehavior;

    //adapter
    private CategroriesAdapter categroriesAdapter;
    private ManufacturerAdapter manufacturerAdapter;
    private ProductAdapter productAdapter;
    private ProductAdapterHorizontal productAdapterHorizontal;
    private CategoriesPresenter categoriesPresenter;
    private ManufacturePresenter manufacturePresenter;
    private ProductsPresenter productsPresenter;

    ProductDto products;
    PresenterLogicChiTietSanPham presenterLogicChiTietSanPham;
    int currentPage = 0;
    int totalPage;

    boolean onPause = false;
    //xử lý load thêm dữ liệu
    boolean isLoading = false;
    public ProductFragment getInstance() {
        if (mInstance == null) {
            mInstance = new ProductFragment();
        }
        return mInstance;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list_product, container, false);
        ButterKnife.bind(this, rootView);
        arrProducts = new ArrayList<>();
        //set kiểu icon cho Textview
        Typeface iconType = FontManager.getTypeface(mContext, FontManager.FONTAWESOME_PRO);
        FontManager.markAsIconContainer(btnCart, iconType);
        FontManager.markAsIconContainer(btnChat, iconType);

        //set onclick
        btnAddToCartMainActivity.setOnClickListener(this);
        imgAddProduct.setOnClickListener(this);
        imReduceNumberProduct.setOnClickListener(this);
        bottom_sheet_quantity_product.setClickable(false);
        frCart.setOnClickListener(this);

        //khoi tao
        categoriesPresenter = new CategoriesPresenter(this);
        manufacturePresenter = new ManufacturePresenter(this);
        productsPresenter = new ProductsPresenter(this);
        presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();

        //truyen event onclick to fragment
        presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham(this);
        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(final View view, final Object object) {
                //bottom_sheet_dialog
//                            sheetBehavior = BottomSheetBehavior.from(bottom_sheet_quantity_product);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                products = ProductDto.builder().build();
                products = (ProductDto) object;
                if (!StringUtils.isNullOrEmpty(products.getProductName()))
                    txtProductName.setText(products.getProductName());
                if (products.getPrice() >0 && products.getQuantity() >0)
                    txtProductPriceAndNumer.setText(AppController.getInstance().formatCurrency(products.getPrice()) +"/"+products.getQuantity());

            }
        };
        productAdapter = new ProductAdapter(getActivity(), new ArrayList<ProductDto>(), listener);
        productAdapterHorizontal = new ProductAdapterHorizontal(getActivity(), new ArrayList<ProductDto>(), listener);
        rcAllProduct.setAdapter(productAdapter);
        GridLayoutManager layoutProducts
                = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        rcAllProduct.setLayoutManager(layoutProducts);

        if (presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(getContext())>0) {
            txtQuantityProductCart.setVisibility(View.VISIBLE);
            txtQuantityProductCart.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(getContext())));
        }else
            txtQuantityProductCart.setVisibility(View.GONE);

        sheetBehavior = BottomSheetBehavior.from(bottom_sheet_quantity_product);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        //sliding banner
        HashMap<String, String> url_maps = new HashMap();
        url_maps.put("Masan", "https://cf.shopee.vn/file/0e993556e72aeca9e62e278bc1e5990f");
        url_maps.put("chisu", "https://cf.shopee.vn/file/2f03d279dbb459517841a52b51012793");
        url_maps.put("fast food", "https://cf.shopee.vn/file/f8b77000882a94362860fb7eb0be285f");
        url_maps.put("B'Fast", "https://masanfood-cms-production.s3-ap-southeast-1.amazonaws.com/iblock/623/62326d58273e62c4692f773c9892fd0e.jpg");
        sliderCollection(url_maps);
        onScroll();
        refresh();
        getCategories();
        getManufacturers();
        getHighlightProduct();
        getAllProducts(currentPage);

        return rootView;

    }

    @Override
    public void onPause() {
        super.onPause();
        onPause = true;//khi chuyen trang thi nhay vao onPause
    }

    @Override
    public void onResume() {
        super.onResume();
        if(onPause){
            txtQuantityProductCart.setVisibility(View.VISIBLE);
            PresenterLogicChiTietSanPham presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();
            txtQuantityProductCart.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(getContext())));
        }
        int countProduct = Integer.parseInt(txtQuantityProductCart.getText().toString());
        if (countProduct == 0)
            txtQuantityProductCart.setVisibility(View.GONE);

    }

    private void onScroll() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView nestedScrollView, int i, int scrollY, int i2, int i3) {
                if(scrollY == 0){
                    toolbar.setBackgroundColor(Color.parseColor("#00ffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.white));
                    btnChat.setTextColor(getResources().getColor(R.color.white));
                }
                else if(scrollY > 50 && scrollY < 100){
                    toolbar.setBackgroundColor(Color.parseColor("#33ffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.primaryPressed));
                    btnChat.setTextColor(getResources().getColor(R.color.primaryPressed));
                }
                else if(scrollY > 100 && scrollY < 200){
                    toolbar.setBackgroundColor(Color.parseColor("#88ffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.accent));
                    btnChat.setTextColor(getResources().getColor(R.color.accent));
                }
                else if(scrollY > 200 && scrollY < 300){
                    toolbar.setBackgroundColor(Color.parseColor("#ccffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.da_cam));
                    btnChat.setTextColor(getResources().getColor(R.color.da_cam));
                }
                else if(scrollY > 300 && scrollY < 500){
                    toolbar.setBackgroundColor(Color.parseColor("#ffffffff"));
                    btnCart.setTextColor(getResources().getColor(R.color.da_cam));
                    btnChat.setTextColor(getResources().getColor(R.color.da_cam));
                }

                View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
                // if diff is zero, then the bottom has been reached
                if (diff == 0) {
                    if (currentPage < totalPage && !isLoading) {
                        isLoading = true;
                        getAllProducts(++currentPage);
                    }
                }
            }
        });


    }


    private void refresh() {
        pullToRefresh.setRefreshing(false);
        pullToRefresh.setSize(50);
        pullToRefresh.setSlingshotDistance(200);
        pullToRefresh.setColorSchemeColors(getResources().getColor(R.color.accent), getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent_v2), getResources().getColor(R.color.main_color));
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh.setRefreshing(true);
                getCategories();
                getManufacturers();
                arrProducts.clear();
                getAllProducts(currentPage = 0);

            }


        });

    }

    private void getManufacturers() {
        manufacturePresenter.getManufacturers();
    }
    private void getAllProducts(int page) {
        try {
            productsPresenter.getProducts(page);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void getCategories() {
        categoriesPresenter.getDataCategories();
    }
    private void getHighlightProduct() {
    }
    // sliderCollection
    private void sliderCollection(HashMap<String, String> url_maps) {
        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(2600);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
    }

    @Override
    public void onStop() {
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }
    @Override
    public void onPageSelected(int position) {

    }
    @Override
    public void onPageScrollStateChanged(int state) {

    }
    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {
        super.handleResponseError(actionSender, code);
        switch (actionSender) {
            case Constants.GET_DATA_MANUFACTURE:
                pullToRefresh.setRefreshing(false);
                break;
            case Constants.GET_DATA_CATEGORIES:
                pullToRefresh.setRefreshing(false);
                break;
            case Constants.GET_DATA_PRODUCT:
                pullToRefresh.setRefreshing(false);
                isLoading = false;
                break;
        }
    }
    @Override
    public void handleResponseSuccessful(int actionSender, ResponseInfo response) {
        super.handleResponseSuccessful(actionSender, response);
        switch (actionSender) {
            case Constants.GET_DATA_MANUFACTURE:
                pullToRefresh.setRefreshing(false);
                if(response.responseData!=null)
                {
                    ServiceResponse<Manufacturers> manufactureResponse = (ServiceResponse<Manufacturers>) response.responseData;
                    manufacturerAdapter = new ManufacturerAdapter(getActivity(), manufactureResponse.getArrData());
                    GridLayoutManager layoutBrand = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
                    rcBrand.setLayoutManager(layoutBrand);
                    rcBrand.setAdapter(manufacturerAdapter);
                }
                break;
            case Constants.GET_DATA_CATEGORIES:
                pullToRefresh.setRefreshing(false);
                if (response != null && response.responseData != null) {
                    ServiceResponse<Category> categoriesResponse = (ServiceResponse<Category>) response.responseData;
                    categroriesAdapter = new CategroriesAdapter(getActivity(), categoriesResponse.getArrData());
                    rcDanhMucSp.setAdapter(categroriesAdapter);
                    GridLayoutManager layoutCategory
                            = new GridLayoutManager(getActivity(), 2, GridLayoutManager.HORIZONTAL, false);
                    rcDanhMucSp.setLayoutManager(layoutCategory);
                }
                break;
            case Constants.GET_DATA_PRODUCT:
                pullToRefresh.setRefreshing(false);
                if (response != null && response.responseData != null) {
                    ServiceResponse<ProductDto> productsResponse = (ServiceResponse<ProductDto>) response.responseData;
                    arrProducts = productsResponse.getArrData();

                    totalPage = productsResponse.getTotalPages();
                    productAdapter.update(arrProducts, isLoading);
                    //----------------
                    productAdapterHorizontal.update(arrProducts);
                    rcHighlightProducts.setAdapter(productAdapterHorizontal);
                    rcPopularProduct.setAdapter(productAdapterHorizontal);
                    rcSaleProducts.setAdapter(productAdapterHorizontal);
                    GridLayoutManager layout
                            = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
                    rcSaleProducts.setLayoutManager(layout);
                    GridLayoutManager layout1
                            = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
                    rcPopularProduct.setLayoutManager(layout1);
                    GridLayoutManager layout2
                            = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
                    rcHighlightProducts.setLayoutManager(layout2);
//
                    isLoading = false;

                }
                break;

        }
    }

    private void addProductToCart() {
        Products sanPhamGioHang = Products.builder().build();
        sanPhamGioHang.setProductId(products.getId());
        sanPhamGioHang.setProductName(products.getProductName());
        sanPhamGioHang.setPrice(products.getPrice());
        sanPhamGioHang.setUrl(products.getUrlImage());
        sanPhamGioHang.setQuantityOfProduct(products.getQuantity());
        sanPhamGioHang.setQuantity(Double.parseDouble(edNumber_Product.getText().toString()));
        presenterLogicChiTietSanPham.ThemGioHang(sanPhamGioHang, getContext());
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frCart:
                Intent iCart = new Intent(getContext(), CartActivity.class);
                startActivity(iCart);
                break;
            case R.id.btnSeeMoreHighlight:
                break;
            case R.id.btnSeeMorePopular:
                break;
            case R.id.btnSeeMoreSale:
                break;
            case R.id.btnAddToCartMainActivity:
                addProductToCart();
                break;
            case R.id.imgAddProduct:
                double soluong1 = Double.parseDouble(edNumber_Product.getText().toString());
                soluong1++;
                edNumber_Product.setText(String.valueOf(soluong1) );
                break;
            case R.id.imReduceNumberProduct:
                double soluong = Double.parseDouble(edNumber_Product.getText().toString());
                if(soluong > 1){
                    soluong--;
                }
                edNumber_Product.setText(String.valueOf(soluong));
                break;
        }
    }

    @Override
    public void ThemGioHangThanhCong() {
        Toast.makeText(getContext(),"Sản phẩm đã được thêm vào giỏ hàng !",Toast.LENGTH_SHORT).show();
        PresenterLogicChiTietSanPham presenterLogicChiTietSanPham = new PresenterLogicChiTietSanPham();
        if (presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(getContext()) > 0) {
            txtQuantityProductCart.setVisibility(View.VISIBLE);
            txtQuantityProductCart.setText(String.valueOf(presenterLogicChiTietSanPham.DemSanPhamCoTrongGioHang(getContext())));
        }else {
            txtQuantityProductCart.setVisibility(View.GONE);
        }
        productAdapter.notifyDataSetChanged();
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
    }

    @Override
    public void ThemGiohangThatBai() {
        Toast.makeText(getContext(),"Sản phẩm đã tồn tại trong giỏ hàng!",Toast.LENGTH_SHORT).show();
    }
}
