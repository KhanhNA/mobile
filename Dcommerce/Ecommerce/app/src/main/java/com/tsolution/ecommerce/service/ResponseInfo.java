package com.tsolution.ecommerce.service;

import com.tsolution.ecommerce.utils.Constants;

/**
 * Mo ta class or interface
 * Created by: Phung Dang Hoan. HoanPD1.
 * Create time: 1/11/2017
 * Update time: 1/11/2017
 * Version: 1.0
 */

public class ResponseInfo<T> {

    public int actionSender;
    public Constants.CODE errorCode;
    public T responseData;

    public ResponseInfo() {

    }

    public ResponseInfo(T responseData,Constants.CODE errorCode) {
        this.responseData = responseData;
    }


    public ResponseInfo(int actionSender, T responseData) {
        this.actionSender = actionSender;
        this.responseData = responseData;
    }

    public ResponseInfo(int actionSender, Constants.CODE errorCode) {
        this.actionSender = actionSender;
        this.errorCode = errorCode;
    }

    public ResponseInfo(int actionSender, Constants.CODE errorCode, T responseData) {
        this.actionSender = actionSender;
        this.errorCode = errorCode;
        this.responseData = responseData;
    }

}
