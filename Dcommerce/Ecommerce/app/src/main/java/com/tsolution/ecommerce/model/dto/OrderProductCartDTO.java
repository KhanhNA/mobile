package com.tsolution.ecommerce.model.dto;

import com.tsolution.ecommerce.model.*;

import java.io.*;
import java.util.*;

import lombok.*;


@Data
public class OrderProductCartDTO implements Serializable {
    public Long orderId;
    /**
     * Mã đơn hàng
     */
    public String orderNo;
    public Long merchantId;
    public Long merchantTypeId;
    public String merchantName;
    /**
     *
     */
    public Integer isSaveOrder ;
    /**
     *
     */
    public Double amount;
    /**
     *
     */
    public Long vat;

    /**
     *
     */
    public Double incentiveAmount;
    /**
     *
     */
    public Double recvLong;
    /**
     *
     */
    public String recvAddr;
    /**
     *
     */
    public Long recvStoreId;
    /**
     *
     */
    public Double recvLat;
    /**
     *
     */
    public Double shipCharge;

    public Long shipMethodId;
    /**
     *
     */
    public Long paymentMethodId;

    public Double quantity;
    public Integer orderStatus;

    public Double remainQuantity;
    public Double remainAmount;
    public Double walletAmount;
    public Double tax;
    /**
     * Đã thanh toán chưa
     */

    public Integer paymentStatus;
    /**
     * Đơn hàng đã giao cho khách hàng
     */

    public Integer deliveryStatus;
    public List<OrderPackingDTO> orderPackings;
    public void addPackingProduct(OrderPackingDTO packingProduct) {
        if(this.orderPackings == null) {
            this.orderPackings = new ArrayList<>();
        }
        this.orderPackings.add(packingProduct);
    }
}
