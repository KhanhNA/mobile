package com.tsolution.ecommerce.view.activity;


import android.content.Intent;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ListOrderPapersAdapter;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.utils.Constants;
import com.tsolution.ecommerce.widget.SlidingTabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListOrderedLv2Activity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.sliding_tabs)
    SlidingTabLayout slidingTabs;
    @BindView(R.id.vpOrderTab)
    ViewPager vpOrderTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_lv2);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setElevation(4);
        }
        intTabLayout();
    }
    private void intTabLayout()
    {
        slidingTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        slidingTabs.setDistributeEvenly(true);
        ListOrderPapersAdapter myPagerAdapter = new ListOrderPapersAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.arr_order_tab));
        vpOrderTab.setAdapter(myPagerAdapter);
        vpOrderTab.setOffscreenPageLimit(2);
        vpOrderTab.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        slidingTabs.setViewPager(vpOrderTab);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        vpOrderTab.setPageMargin(pageMargin);
        vpOrderTab.setCurrentItem(0);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_chat:
                Intent i = new Intent(ListOrderedLv2Activity.this, ChatActivity.class);
                startActivity(i);

                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {
        super.handleResponseError(actionSender, code);

    }

    @Override
    public void handleResponseSuccessful(int actionSender, ResponseInfo response) {
        super.handleResponseSuccessful(actionSender, response);
    }
}
