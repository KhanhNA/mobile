package com.tsolution.ecommerce.view.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tsolution.ecommerce.R;
import com.tsolution.ecommerce.adapter.ProductAdapter;
import com.tsolution.ecommerce.model.dto.ProductDto;
import com.tsolution.ecommerce.model.response.ServiceResponse;
import com.tsolution.ecommerce.presenter.ProductsPresenter;
import com.tsolution.ecommerce.service.ResponseInfo;
import com.tsolution.ecommerce.utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;



public class SubProductFragment extends BaseFragment   {
    private static SubProductFragment mInstance;
    ArrayList<ProductDto> data;
    @BindView(R.id.rcSubProduct)
    RecyclerView rcSubProduct;
    private View rootView;
    private ProductAdapter productAdapter;
    int currentPage = 0;
    int totalPage;


    // create boolean for fetching data
    private boolean loaded = false;
    //xử lý load thêm dữ liệu
    boolean isLoading = false;
    int lastVisibleItem, totalItemCount;
    private ProductsPresenter productsPresenter;

    public SubProductFragment getInstance() {
        if (mInstance == null) {
            mInstance = new SubProductFragment();
        }
        return mInstance;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_sub_product_fragment, container, false);
        EventBus.getDefault().register(this);
        return rootView;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(getActivity());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        try {
            super.setUserVisibleHint(isVisibleToUser);
            if (!loaded && isVisibleToUser) {
                productsPresenter = new ProductsPresenter(this);
                Log.e("isVisibleToUser", isVisibleToUser + "");
                getProduct(currentPage);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        try {
            super.onViewCreated(view, savedInstanceState);
            ButterKnife.bind(this, rootView);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
            rcSubProduct.setLayoutManager(mLayoutManager);
            rcSubProduct.setItemAnimator(new DefaultItemAnimator());
            data = new ArrayList<>();
            productAdapter = new ProductAdapter(getContext(), data, null);

            rcSubProduct.setAdapter(productAdapter);
            //xử lý load thêm dữ liệu
            loadMoreProducts();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void loadMoreProducts() throws Exception{

        rcSubProduct.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                try {
                    super.onScrolled(recyclerView, dx, dy);
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + 1)) {
                        if (currentPage < totalPage) {
                            Log.e("loadmore", true + "");
                            isLoading = true;
                            getProduct(++currentPage);
                        }
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });
    }

    private void getProduct(int page) throws Exception {
        loaded = true;
        productsPresenter.getProducts(page);
    }
    @Override
    public void handleResponseError(int actionSender, Constants.CODE code) {
        super.handleResponseError(actionSender, code);
        switch (actionSender) {
            case Constants.GET_DATA_PRODUCT:
                break;
        }
    }

    @Override
    public void handleResponseSuccessful(int actionSender, ResponseInfo response) {
        super.handleResponseSuccessful(actionSender, response);
        switch (actionSender) {
            case Constants.GET_DATA_PRODUCT:
                ServiceResponse<ProductDto> productsResponse = (ServiceResponse<ProductDto>) response.responseData;
                data.addAll(productsResponse.getArrData());
                productAdapter.notifyDataSetChanged();
                break;
        }
    }


}
